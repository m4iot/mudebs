#!/bin/sh

rm -rf /tmp/muDEBS-deploy
mkdir /tmp/muDEBS-deploy
cd sources/muDEBS/
mvn clean install deploy
cd ../..
