#!/bin/bash

MUDEBS_HOME_SCRIPTS="$(cd ../../../muDEBS/scripts/ && pwd)"

echo
echo "Hit return when you want to send termination detection to broker B1"
read x
echo "Setting logger overlay to debug for B1"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2001/B1 --command setlog --name overlay --level debug
sleep 1
echo "Setting logger overlay to debug for B2"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2002/B2 --command setlog --name overlay --level debug
sleep 1
echo "Setting logger overlay to debug for B3"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2003/B3 --command setlog --name overlay --level debug
sleep 1
echo "Setting logger overlay to debug for B4"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2004/B4 --command setlog --name overlay --level debug
sleep 1
echo "Setting logger overlay to debug for B5"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2005/B5 --command setlog --name overlay --level debug
sleep 10
echo
echo On B313_01_B1, termination detection
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2001/B1 --command term_detection
sleep 1
