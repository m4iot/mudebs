#!/bin/bash

MUDEBS_HOME_SCRIPTS="$(cd ../../../muDEBS/scripts/ && pwd)"

echo 'Architecture of the example'
echo ''
echo '  C2        B2            C3'
echo '   \      /|\           /'
echo '    \    / | \         /'
echo '     \  /  |  \       /'
echo '      B3   |   B1---B5'
echo '     /     |  /       \'
echo '    /      | /         \'
echo '   /       |/           \'
echo '  C0        B4            C1'
echo ''

echo
echo "Hit return when you want to start brokers"
read x
echo "Starting B1"
${MUDEBS_HOME_SCRIPTS}/startbroker --uri mudebs://localhost:2001/B1 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performance_example.config
echo "Starting B2"
${MUDEBS_HOME_SCRIPTS}/startbroker --uri mudebs://localhost:2002/B2 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performance_example.config 
echo "Starting B3"
${MUDEBS_HOME_SCRIPTS}/startbroker --uri mudebs://localhost:2003/B3 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performance_example.config 
echo "Starting B4"
${MUDEBS_HOME_SCRIPTS}/startbroker --uri mudebs://localhost:2004/B4 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performance_example.config
echo "Starting B5"
${MUDEBS_HOME_SCRIPTS}/startbroker --uri mudebs://localhost:2005/B5 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performance_example.config

echo
echo "Hit return when you want to connect brokers"
read x
echo "Starting B2 connected to B1"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2002/B2 --command connect --neigh mudebs://localhost:2001/B1
echo "Starting B3 connected to B2"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2003/B3 --command connect --neigh mudebs://localhost:2002/B2
echo "Starting B4 connected to B4 and B1"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2004/B4 --command connect --neigh mudebs://localhost:2001/B1 
echo "Starting B4 connected to B4 and B1"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2004/B4 --command connect --neigh mudebs://localhost:2002/B2
echo "Starting B5 connected to B1"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2005/B5 --command connect --neigh mudebs://localhost:2001/B1
sleep 1

echo
echo "Hit return to start join scope calls"
read x

echo
echo On B1, join scope \(es,top\)
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2001/B1 --command joinscope --dimension mudebs://localhost:membership --subscope es --superscope top
sleep 1

echo
echo On B2, join scope \(us,es\)
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2002/B2 --command joinscope --dimension mudebs://localhost:membership --subscope us --superscope es --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
sleep 1

echo
echo On B3, join scope \(ls,us\)
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2003/B3 --command joinscope --dimension mudebs://localhost:membership --subscope ls --superscope us --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
sleep 1
echo
echo On B1, join scope \(fs,es\)
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2001/B1 --command joinscope --dimension mudebs://localhost:membership --subscope fs --superscope es --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
sleep 1
echo
echo On B5, join scope \(is,fs\)
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2005/B5 --command joinscope --dimension mudebs://localhost:membership --subscope is --superscope fs --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
sleep 1
echo
echo On B5, join scope \(ir,top\)
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2005/B5 --command joinscope --dimension mudebs://localhost:membership --subscope ir --superscope top
sleep 1
echo
echo On B5, join scope \(is,ir\)
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2005/B5 --command joinscope --dimension mudebs://localhost:membership --subscope is --superscope ir --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
sleep 1

echo
echo On B2, join scope \(eu,top\)
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2002/B2 --command joinscope --dimension mudebs://localhost:admin_area --subscope eu --superscope top
sleep 1
echo
echo On B2, join scope \(fr,eu\)
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2002/B2 --command joinscope --dimension mudebs://localhost:admin_area --subscope fr --superscope eu --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
sleep 1
echo
echo On B3, join scope \(bo,fr\)
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2003/B3 --command joinscope --dimension mudebs://localhost:admin_area --subscope bo --superscope fr --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
sleep 1
echo
echo On B3, join scope \(ch,bo\)
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2003/B3 --command joinscope --dimension mudebs://localhost:admin_area --subscope ch --superscope bo --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
sleep 1
echo
echo On B1, join scope \(uk,eu\)
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2001/B1 --command joinscope --dimension mudebs://localhost:admin_area --subscope uk --superscope eu --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
sleep 1
echo
echo On B5, join scope \(lo,uk\)
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2005/B5 --command joinscope --dimension mudebs://localhost:admin_area --subscope lo --superscope uk --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
sleep 1

echo
echo On B4, join scope \(pi,top\)
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2004/B4 --command joinscope --dimension mudebs://localhost:network_range --subscope pi --superscope top
sleep 1
echo
echo On B5, join scope \(tm,pi\)
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2005/B5 --command joinscope --dimension mudebs://localhost:network_range --subscope tm --superscope pi --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
sleep 1
echo
echo On B2, join scope \(ti,pi\)
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2002/B2 --command joinscope --dimension mudebs://localhost:network_range --subscope ti --superscope pi --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
sleep 1
echo
echo On B3, join scope \(bw,ti\)
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2003/B3 --command joinscope --dimension mudebs://localhost:network_range --subscope bw --superscope ti --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
sleep 1

echo
echo "Hit return to start clients"
read x
echo "Starting C2 connected to B3"
${MUDEBS_HOME_SCRIPTS}/startperfclient --uri mudebs://localhost:2103/C2 --broker mudebs://localhost:2003/B3 --log ROUTING.INFO
echo "Starting C0 connected to B3"
${MUDEBS_HOME_SCRIPTS}/startperfclient --uri mudebs://localhost:2101/C0 --broker mudebs://localhost:2003/B3 --log ROUTING.INFO
echo "Starting C1 connected to B5"
${MUDEBS_HOME_SCRIPTS}/startperfclient --uri mudebs://localhost:2102/C1 --broker mudebs://localhost:2005/B5 --log ROUTING.INFO
echo "Starting C3 connected to B5"
${MUDEBS_HOME_SCRIPTS}/startperfclient --uri mudebs://localhost:2104/C3 --broker mudebs://localhost:2005/B5 --log ROUTING.INFO


echo
echo "Hit return to start the verification"
read x
echo
echo "Start advertising and subscribing"
echo
echo C2 advertising filter \'startperffirst\' with scopes \'ls\' and \'ch\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2103/C2 --command advertise --id 'startperffirst' --file ./ressources/advertisement.mudebs --dimension mudebs://localhost:membership --scope ls --dimension mudebs://localhost:admin_area --scope ch
sleep 1
echo
echo C0 advertising filter \'startperfsecond\' with scopes \'ls\', \'ch\' and bot
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2101/C0 --command advertise --id 'startperfsecond' --file ./ressources/advertisement.mudebs --dimension mudebs://localhost:membership --scope ls --dimension mudebs://localhost:admin_area --scope ch --scope-bottom
echo
sleep 1
echo C1 subscribing filter \'startperfthird\' with scopes \'is\', \'lo\' and \'tm\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2102/2 --command subscribe --id 'startperfthird' --file ./ressources/subscription.mudebs --dimension mudebs://localhost:membership --scope is --dimension mudebs://localhost:admin_area --scope lo --dimension mudebs://localhost:network_range --scope tm
echo
sleep 1
echo C3 subscribing filter \'startperffourth\' with scopes \'is\', \'tm\' and top
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2104/C3 --command subscribe --id 'startperffourth' --file ./ressources/subscription.mudebs --dimension mudebs://localhost:membership --scope is --dimension mudebs://localhost:network_range --scope tm --scope-top
sleep 5

echo
echo "Start publishing"
echo 
echo C2 publishing with adv. filter \'startperffirst\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2103/C2 --command publish --id 'startperffirst' --content '<foo>helloWorldIAmC2</foo>'
sleep 1
echo
echo C0 publishing with adv. filter \'startperfsecond\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2101/C0 --command publish --id 'startperfsecond' --content '<foo>helloWorldIAmC0</foo>'
sleep 1


echo
echo "Hit return when you want clients to unsubscribe and unadvertise all filters"
read x 
echo C0 unsubscribing all the filters
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2101/C0 --command unsubscribe
echo C0 unadvertising all the filters
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2101/C0 --command unadvertise
echo
echo C1 unsubscribing all the filters
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2102/2 --command unsubscribe
echo C1 unadvertising all the filters
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2102/2 --command unadvertise
sleep 1
echo
echo C3 unsubscribing all the filters
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2104/C3 --command unsubscribe
echo C3 unadvertising all the filters
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2104/C3 --command unadvertise
sleep 1
echo
echo C2 unsubscribing all the filters
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2103/C2 --command unsubscribe
echo C2 unadvertising all the filters
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2103/C2 --command unadvertise
sleep 5

echo
echo "Hit return when you want to send termination detection to broker B1"
read x
echo "Setting logger overlay to debug for B1"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2001/B1 --command setlog --name overlay --level debug
sleep 1
echo "Setting logger overlay to debug for B2"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2002/B2 --command setlog --name overlay --level debug
sleep 1
echo "Setting logger overlay to debug for B3"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2003/B3 --command setlog --name overlay --level debug
sleep 1
echo "Setting logger overlay to debug for B4"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2004/B4 --command setlog --name overlay --level debug
sleep 1
echo "Setting logger overlay to debug for B5"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2005/B5 --command setlog --name overlay --level debug
sleep 10
echo
echo On B313_01_B1, termination detection
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2001/B1 --command term_detection
sleep 1

echo
echo "Hit return when you want to send terminate broker B1"
read x
echo On B1, terminate
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2001/B1 --command terminate_all
sleep 1

echo
echo "Hit return when you want to stop the example"
read x
echo "Stopping all the clients"
${MUDEBS_HOME_SCRIPTS}/stopclient C2
${MUDEBS_HOME_SCRIPTS}/stopclient C0
${MUDEBS_HOME_SCRIPTS}/stopclient C1
${MUDEBS_HOME_SCRIPTS}/stopclient C3
echo "Stopping all the brokers"
${MUDEBS_HOME_SCRIPTS}/stopbroker B1
${MUDEBS_HOME_SCRIPTS}/stopbroker B2
${MUDEBS_HOME_SCRIPTS}/stopbroker B3
${MUDEBS_HOME_SCRIPTS}/stopbroker B4
${MUDEBS_HOME_SCRIPTS}/stopbroker B5
