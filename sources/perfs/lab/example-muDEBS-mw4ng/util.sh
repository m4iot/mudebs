#!/bin/bash

MUDEBS_HOME_SCRIPTS="$(cd ../../../muDEBS/scripts && pwd)"

B313_01_B1=mudebs://127.0.0.1:3001/B313_01_B1
B313_02_B2=mudebs://127.0.0.1:3002/B313_02_B2
B313_03_B3=mudebs://127.0.0.1:3003/B313_03_B3
B313_04_B4=mudebs://127.0.0.1:3004/B313_04_B4
B313_05_B5=mudebs://127.0.0.1:3005/B313_05_B5

CW=mudebs://127.0.0.1:2001/CW
CX=mudebs://127.0.0.1:2002/CX
CY=mudebs://127.0.0.1:2003/CY
CZ=mudebs://127.0.0.1:2004/CZ

MEMBERSHIP=mudebs://localhost:membership
ADMIN_AREA=mudebs://localhost:admin_area
NETWORK_RANGE=mudebs://localhost:network_range
UP_FILTER=./ressources/mapupfilter.mudebs
DOWN_FILTER=./ressources/mapdownfilter.mudebs
ADV_FILTER=./ressources/advertisement.mudebs
SUB_FILTER=./ressources/subscription.mudebs
