#!/bin/bash

MUDEBS_HOME_SCRIPTS="$(cd ../../../muDEBS/scripts/ && pwd)"

echo 'Architecture of the example'
echo ''
echo '  C2       B2            C3'
echo '   \      /|\           /'
echo '    \    / | \         /'
echo '     \  /  |  \       /'
echo '      B3   |   B1---B5'
echo '     /     |  /       \'
echo '    /      | /         \'
echo '   /       |/           \'
echo '  C0       B4            C1'
echo ''

echo "Starting B1"
${MUDEBS_HOME_SCRIPTS}/startbroker --uri mudebs://localhost:2001/B1 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performance_example.config
echo "Starting B2"
${MUDEBS_HOME_SCRIPTS}/startbroker --uri mudebs://localhost:2002/B2 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performance_example.config 
#echo "Starting B3"
#${MUDEBS_HOME_SCRIPTS}/startbroker --uri mudebs://localhost:2003/B3 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performance_example.config 
echo "Starting B4"
${MUDEBS_HOME_SCRIPTS}/startbroker --uri mudebs://localhost:2004/B4 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performance_example.config
#echo "Starting B5"
#${MUDEBS_HOME_SCRIPTS}/startbroker --uri mudebs://localhost:2005/B5 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performance_example.config
sleep 2

echo "Starting C0 connected to B3"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2101/C0 --broker mudebs://localhost:2003/B3 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performance_example.config #--log ROUTING.INFO
echo "Starting C1 connected to B5"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2102/C1 --broker mudebs://localhost:2005/B5 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performance_example.config #--log ROUTING.INFO
echo "Starting C2 connected to B3"
${MUDEBS_HOME_SCRIPTS}/startperfclient --uri mudebs://localhost:2103/C2 --broker mudebs://localhost:2003/B3 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performance_example.config #--log ROUTING.INFO
echo "Starting C3 connected to B5"
${MUDEBS_HOME_SCRIPTS}/startperfclient --uri mudebs://localhost:2104/C3 --broker mudebs://localhost:2005/B5 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performance_example.config #--log ROUTING.INFO
sleep 2

echo "Starting B2 connected to B1"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2002/B2 --command connect --neigh mudebs://localhost:2001/B1
echo "Starting B3 connected to B2"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2003/B3 --command connect --neigh mudebs://localhost:2002/B2
echo "Starting B4 connected to B4 and B1"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2004/B4 --command connect --neigh mudebs://localhost:2001/B1 
echo "Starting B4 connected to B4 and B1"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2004/B4 --command connect --neigh mudebs://localhost:2002/B2
echo "Starting B5 connected to B1"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2005/B5 --command connect --neigh mudebs://localhost:2001/B1
sleep 2

echo
echo On B1, join scope \(es,top\)
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2001/B1 --command joinscope --dimension mudebs://localhost:membership --subscope es --superscope top
sleep 0.7
echo On B2, join scope \(us,es\)
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2002/B2 --command joinscope --dimension mudebs://localhost:membership --subscope us --superscope es --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
sleep 0.7
echo On B3, join scope \(ls,us\)
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2003/B3 --command joinscope --dimension mudebs://localhost:membership --subscope ls --superscope us --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
sleep 0.7
echo On B1, join scope \(fs,es\)
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2001/B1 --command joinscope --dimension mudebs://localhost:membership --subscope fs --superscope es --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
sleep 0.7
echo On B5, join scope \(is,fs\)
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2005/B5 --command joinscope --dimension mudebs://localhost:membership --subscope is --superscope fs --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
sleep 0.7
echo On B5, join scope \(ir,top\)
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2005/B5 --command joinscope --dimension mudebs://localhost:membership --subscope ir --superscope top
sleep 0.7
echo On B5, join scope \(is,ir\)
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2005/B5 --command joinscope --dimension mudebs://localhost:membership --subscope is --superscope ir --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
sleep 0.7
echo On B2, join scope \(eu,top\)
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2002/B2 --command joinscope --dimension mudebs://localhost:admin_area --subscope eu --superscope top
sleep 0.7
echo On B2, join scope \(fr,eu\)
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2002/B2 --command joinscope --dimension mudebs://localhost:admin_area --subscope fr --superscope eu --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
sleep 0.7
echo On B3, join scope \(bo,fr\)
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2003/B3 --command joinscope --dimension mudebs://localhost:admin_area --subscope bo --superscope fr --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
sleep 1
echo On B3, join scope \(ch,bo\)
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2003/B3 --command joinscope --dimension mudebs://localhost:admin_area --subscope ch --superscope bo --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
sleep 0.7
echo On B1, join scope \(uk,eu\)
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2001/B1 --command joinscope --dimension mudebs://localhost:admin_area --subscope uk --superscope eu --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
sleep 0.7
echo On B5, join scope \(lo,uk\)
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2005/B5 --command joinscope --dimension mudebs://localhost:admin_area --subscope lo --superscope uk --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
sleep 0.7
echo On B4, join scope \(pi,top\)
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2004/B4 --command joinscope --dimension mudebs://localhost:network_range --subscope pi --superscope top
sleep 0.7
echo On B5, join scope \(tm,pi\)
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2005/B5 --command joinscope --dimension mudebs://localhost:network_range --subscope tm --superscope pi --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
sleep 0.7
echo On B2, join scope \(ti,pi\)
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2002/B2 --command joinscope --dimension mudebs://localhost:network_range --subscope ti --superscope pi --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
sleep 0.7
echo On B3, join scope \(bw,ti\)
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2003/B3 --command joinscope --dimension mudebs://localhost:network_range --subscope bw --superscope ti --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo
sleep 2

echo "Start publishing"
echo C0 advertising filter \'startperfsubscribing\'
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2101/C0 --command advertise --id 'startperfsubscribing' --file ./ressources/advertisement.mudebs 
sleep 1
echo C0 publishing subscribing command with advertising filter \'subscribing\'
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2101/C0 --command publish --id 'startperfsubscribing' --content '<command><startsubscribing>w4w-C0-</startsubscribing></command>'
sleep 1
echo
echo "Please wait..."

echo
echo "Hit return when you want clients to unsubscribe and unadvertise all filters"
read x 
echo C0 unsubscribing all the filters
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2101/C0 --command unsubscribe
echo sleep 30
sleep 30
echo C0 unadvertising all the filters
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2101/C0 --command unadvertise
echo sleep 30
sleep 30
echo C1 unsubscribing all the filters
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2102/2 --command unsubscribe
echo sleep 30
sleep 30
echo C1 unadvertising all the filters
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2102/2 --command unadvertise
echo sleep 30
sleep 30
echo C2 unsubscribing all the filters
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2103/C2 --command unsubscribe
echo sleep 30
sleep 30
echo C2 unadvertising all the filters
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2103/C2 --command unadvertise
echo sleep 30
sleep 30
echo C3 unsubscribing all the filters
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2104/C3 --command unsubscribe
echo sleep 30
sleep 30
echo C3 unadvertising all the filters
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2104/C3 --command unadvertise
echo sleep 30
sleep 30

echo
echo "Hit return when you want to send termination detection to broker B1"
read x
echo "Setting logger overlay to debug for B1"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2001/B1 --command setlog --name overlay --level debug
sleep 1
echo "Setting logger overlay to debug for B2"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2002/B2 --command setlog --name overlay --level debug
sleep 1
echo "Setting logger overlay to debug for B3"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2003/B3 --command setlog --name overlay --level debug
sleep 1
echo "Setting logger overlay to debug for B4"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2004/B4 --command setlog --name overlay --level debug
sleep 1
echo "Setting logger overlay to debug for B5"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2005/B5 --command setlog --name overlay --level debug
sleep 10
echo
echo On B1, termination detection
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2001/B1 --command term_detection
sleep 1

echo
echo "Hit return when you want to send terminate broker B1"
read x
echo On B1, terminate
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2001/B1 --command terminate_all
sleep 1

echo
echo "Hit return when you want to stop the example"
read x
echo "Stopping all the clients"
${MUDEBS_HOME_SCRIPTS}/stopclient C2
${MUDEBS_HOME_SCRIPTS}/stopclient C0
${MUDEBS_HOME_SCRIPTS}/stopclient C1
${MUDEBS_HOME_SCRIPTS}/stopclient C3
echo "Stopping all the brokers"
${MUDEBS_HOME_SCRIPTS}/stopbroker B1
${MUDEBS_HOME_SCRIPTS}/stopbroker B2
${MUDEBS_HOME_SCRIPTS}/stopbroker B3
${MUDEBS_HOME_SCRIPTS}/stopbroker B4
${MUDEBS_HOME_SCRIPTS}/stopbroker B5
