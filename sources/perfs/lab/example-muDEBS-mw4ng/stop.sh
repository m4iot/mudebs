#!/bin/bash

MUDEBS_HOME_SCRIPTS="$(cd ../../../muDEBS/scripts/ && pwd)"

echo ${MUDEBS_HOME_SCRIPTS}

echo "Stopping all the clients"
${MUDEBS_HOME_SCRIPTS}/stopclient C0
${MUDEBS_HOME_SCRIPTS}/stopclient C1
${MUDEBS_HOME_SCRIPTS}/stopclient C2
${MUDEBS_HOME_SCRIPTS}/stopclient C3
echo "Stopping all the brokers"
${MUDEBS_HOME_SCRIPTS}/stopbroker B1
${MUDEBS_HOME_SCRIPTS}/stopbroker B2
${MUDEBS_HOME_SCRIPTS}/stopbroker B3
${MUDEBS_HOME_SCRIPTS}/stopbroker B4
${MUDEBS_HOME_SCRIPTS}/stopbroker B5
