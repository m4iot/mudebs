#!/bin/bash

MUDEBS_HOME_SCRIPTS="$(cd $(dirname "$0")/../../../../muDEBS/scripts/ && pwd)"
${MUDEBS_HOME_SCRIPTS}/utils.sh

M0=mudebs://localhost
M1=mudebs://localhost
M2=mudebs://localhost
B0_URI=$M0:3000/B0
B1_URI=$M1:3001/B1
C0_URI=$M2:2000/C0
C1_URI=$M2:2001/C1
period_between_pubs=1000
np_pub=1000
nb_attributesXACML=13 #+3
nb_attributesABAC=16
perfmode=XACML_A
#the first policy is used
id_adv_privacy=1
config_file=./resources/xacml_a_1000-pub_1-sub_16-attr.config

echo 'Architecture of 2 brokers'
echo sleep 0.5
sleep 0.5

#==============================================================================
# the following code should be executed at the machine M0 hosting B0                                                            
#==============================================================================
echo
echo "Starting broker B0"
${MUDEBS_HOME_SCRIPTS}/startbroker --uri $B0_URI --log PERFORMANCE.PERF --perfconfigfile $config_file #--log ROUTING.TRACE 
echo sleep 0.5
sleep 0.5

#==============================================================================
# the following code should be executed at the machine M1 hosting B1  
# Note2: M1 et M2 should have the same capacities in terms of computation 
# memory...
#==============================================================================
echo "Starting broker B1"
${MUDEBS_HOME_SCRIPTS}/startbroker --uri $B1_URI --log PERFORMANCE.PERF --perfconfigfile $config_file #--log ROUTING.TRACE
echo sleep 0.5
sleep 0.5
echo
echo "Connecting broker B1 to broker B0"
${MUDEBS_HOME_SCRIPTS}/broker --uri $B1_URI --command connect --neigh $B0_URI 
echo sleep 0.5
sleep 0.5

#==============================================================================
# the following code should be executed at the machine M2 hosting C0 and C1                                                            
#==============================================================================
# performance mode:                                                            
# - CBC 		= context-based filters with constraints
# - QOCMCCC	= QoC-based multi-constraints with crierion constraints
# - QOCMCVC	= QoC-based multi-constraints with value constraints
# - QOCMMCC	= QoC-based multi-metadata with criterion constraint
# - QOCMMVC	= QoC-based multi-metadata with value constraints
# - DEFAULT	= none of the above modes is applied
# Note2: only one of both "--nb_constraints" and "--nb_indidcators" is used.
# Note3: a filter whose identifier begins with "startperf" and its associated 
# messages... are not taken into account in the results.
#===============================================================================
echo 
echo "Starting client C0 connected to broker B0"
${MUDEBS_HOME_SCRIPTS}/startqocimperfclient --uri $C0_URI --broker $B0_URI --eval_perf_filters_mode $perfmode --nb_attributes $nb_attributesXACML --eval_perf_nb_pub $np_pub --time_between_two_pub $period_between_pubs
echo sleep 0.5
sleep 0.5
echo "Starting client C1 connected to broker B1"
${MUDEBS_HOME_SCRIPTS}/startqocimperfclient --uri $C1_URI --broker $B1_URI --eval_perf_filters_mode $perfmode --nb_attributes $nb_attributesABAC --eval_perf_nb_sub 1
echo sleep 0.5
sleep 0.5


echo "Please wait..."
echo "Start publishing"
echo C0 advertising filter \'startperfsubscribing\'
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2000/C0 --command advertise --id 'startperfsubscribing' --file ./resources/advertisement.mudebs 
sleep 1
echo C0 publishing subscribing command with advertising filter \'startperfsubscribing\'
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2000/C0 --command publish --id 'startperfsubscribing' --content '<command><noscoping><startsubscribing>-C1-</startsubscribing></noscoping></command>'
sleep 1
echo


echo
echo "Hit return to let clients to unsubscribe and unadvertise to all filters"
read x
echo "Client C0 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri $C0_URI --command unsubscribe
echo sleep 1
sleep 1
echo "Client C0 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri $C0_URI --command unadvertise
echo sleep 1
sleep 1
echo "Client C1 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri $C1_URI --command unsubscribe
echo sleep 1
sleep 1
echo "Client C1 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri $C1_URI --command unadvertise
echo sleep 1
sleep 1


echo
echo "Hit return to set logger overlay to debug for all brokers"
read x
echo
echo "Setting logger overlay to debug for B0"
${MUDEBS_HOME_SCRIPTS}/broker --uri $B0_URI --command setlog --name overlay --level debug
echo sleep 1
sleep 1
echo "Setting logger overlay to debug for B1"
${MUDEBS_HOME_SCRIPTS}/broker --uri $B1_URI --command setlog --name overlay --level debug
echo sleep 1
sleep 1


echo
echo "Hit return to send termination detection to broker B0"
read x
echo On B0, termination detection
${MUDEBS_HOME_SCRIPTS}/broker --uri $B0_URI --command term_detection
echo sleep 1
sleep 1


echo
echo "Hit return to send terminate to broker B0"
read x
echo On B0, terminate
${MUDEBS_HOME_SCRIPTS}/broker --uri $B0_URI --command terminate_all
sleep 5

echo
echo "Hit return to force the termination of all clients and brokers"
read x
echo "Stopping all the clients"
${MUDEBS_HOME_SCRIPTS}/stopclient C0
${MUDEBS_HOME_SCRIPTS}/stopclient C1
echo "Stopping all the brokers"
${MUDEBS_HOME_SCRIPTS}/stopbroker B0
${MUDEBS_HOME_SCRIPTS}/stopbroker B1
