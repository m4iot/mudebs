MUDEBS_HOME_SCRIPTS="$(cd $(dirname "$0")/../../../../muDEBS/scripts/ && pwd)"
${MUDEBS_HOME_SCRIPTS}/utils.sh

echo
echo "Hit return when you want to start the brokers"
read x
echo "Starting BrokerA"
${MUDEBS_HOME_SCRIPTS}/startbroker --uri mudebs://localhost:2000/BrokerA --log PERFORMANCE.PERF --perfconfigfile ./resources/testXACML.confg #--log ROUTING.DEBUG
#sleep 1
echo "Starting BrokerB"
${MUDEBS_HOME_SCRIPTS}/startbroker --uri mudebs://localhost:2001/BrokerB --log PERFORMANCE.PERF --perfconfigfile ./resources/testXACML.confg #--log ROUTING.TRACE

echo "Broker B connects to BrokerA"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2001/BrokerB --command connect --neigh mudebs://localhost:2000/BrokerA


echo 
echo "Starting clientA connected to BrokerA"
${MUDEBS_HOME_SCRIPTS}/startclient --uri mudebs://localhost:2100/ClientA --broker mudebs://localhost:2000/BrokerA
sleep 1
echo "Starting clientB connected to BrokerB"
${MUDEBS_HOME_SCRIPTS}/startclient --uri mudebs://localhost:2101/ClientB --broker mudebs://localhost:2001/BrokerB
sleep 1


echo
echo "Hit return when you want to start the test"
read x
echo ClientA subscribing to filter \'sixth\' in \'./resources/subscription.mudebs\' with ABAC info \'category1,id1,INCOME:category2,id2,PersonalUse:category3,id3,abcd:category4,id4,access\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2100/ClientA --command subscribe --id 'sixth' --file './resources/subscription.mudebs' --abac 'category1,id1,INCOME:category2,id2,PersonalUse:category3,id3,abcd:category4,id4,access'
sleep 1
echo
echo ClientA advertising filter \'sixth\' in \'./resources/advertisement.mudebs\' with XAMCL policy in \'./resources/xacmlpolicy.xml\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2100/ClientA --command advertise --id 'sixth' --file './resources/advertisement.mudebs' --policy './resources/xacmlpolicy.xml'
sleep 1
echo ClientA advertising filter \'other\' in \'./resources/advertisement.mudebs\' with XAMCL policy in \'./resources/policySimple.xml\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2100/ClientA --command advertise --id 'other' --file './resources/advertisement.mudebs' --policy './resources/xacmlpolicy.xml'
sleep 1
echo
echo ClientA publishing \'apublicationfromA\' with adv. filter \'sixth\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2100/ClientA --command publish --id 'sixth' --content '<foo>apublicationfromA</foo>'
sleep 1
echo
echo ClientB advertising filter \'sixth\' in \'./resources/advertisement.mudebs\' with policy in \'./resources/xacmlpolicy.xml\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2101/ClientB --command advertise --id 'sixth' --file './resources/advertisement.mudebs' --policy './resources/xacmlpolicy.xml'
sleep 1
echo
echo ClientB publishing \'apublicationfromB\' with adv. filter \'sixth\'
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2101/ClientB --command publish --id 'sixth' --content '<foo>apublicationfromB</foo>'
echo
sleep 5

echo
echo "Hit return when you want to stop the demonstration"
read x
echo "Stopping all the clients"
${MUDEBS_HOME_SCRIPTS}/stopclient ClientA
${MUDEBS_HOME_SCRIPTS}/stopclient ClientB

echo "Stopping all the brokers"
${MUDEBS_HOME_SCRIPTS}/stopbroker BrokerA
${MUDEBS_HOME_SCRIPTS}/stopbroker BrokerB
