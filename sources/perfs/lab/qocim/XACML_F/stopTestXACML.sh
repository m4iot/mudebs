MUDEBS_HOME_SCRIPTS="$(cd $(dirname "$0")/../../../../muDEBS/scripts/ && pwd)"
${MUDEBS_HOME_SCRIPTS}/utils.sh

echo "Stopping all the clients"
${MUDEBS_HOME_SCRIPTS}/stopclient ClientA
${MUDEBS_HOME_SCRIPTS}/stopclient ClientB

echo "Stopping all the brokers"
${MUDEBS_HOME_SCRIPTS}/stopbroker BrokerA
${MUDEBS_HOME_SCRIPTS}/stopbroker BrokerB
