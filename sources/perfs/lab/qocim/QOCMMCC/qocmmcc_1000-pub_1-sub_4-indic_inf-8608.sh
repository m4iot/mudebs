#!/bin/bash

MUDEBS_HOME_SCRIPTS="$(cd $(dirname "$0")/../../../../muDEBS/scripts/ && pwd)"
${MUDEBS_HOME_SCRIPTS}/utils.sh

M0=mudebs://inf-8661
M1=mudebs://inf-8608
M2=mudebs://inf-8941
B0_URI=$M0:3000/B0
B1_URI=$M1:3001/B1
C0_URI=$M2:2000/C0
C1_URI=$M2:2001/C1
perfmode=QOCMMCC
nb_indicators=4
config_file="./ressources/qocmmcc_1000-pub_1-sub_${nb_indicators}-indic.config"

echo 'Architecture of 2 brokers'
echo sleep 0.5
sleep 0.5

#==============================================================================
# the following code should be executed at the machine M1 hosting B1  
# Note1: M1 et M2 should have the same capacities in terms of computation 
# memory...
#==============================================================================
echo "Starting broker B1"
${MUDEBS_HOME_SCRIPTS}/startbroker --uri $B1_URI --log PERFORMANCE.PERF --perfconfigfile $config_file #--log ROUTING.TRACE
echo sleep 0.5
sleep 0.5
echo
echo "Connecting broker B1 to broker B0"
${MUDEBS_HOME_SCRIPTS}/broker --uri $B1_URI --command connect --neigh $B0_URI 
echo sleep 0.5
sleep 0.5

echo
echo "Hit return to force the termination of all clients and brokers"
read x
echo "Stopping all the clients"
${MUDEBS_HOME_SCRIPTS}/stopclient C0
${MUDEBS_HOME_SCRIPTS}/stopclient C1
echo "Stopping all the brokers"
${MUDEBS_HOME_SCRIPTS}/stopbroker B0
${MUDEBS_HOME_SCRIPTS}/stopbroker B1
