import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GetStatsAfter500Pub {
	public static void main(String[] args) throws IOException {
		List<Double> list = new ArrayList<Double>();
		List<Double> truncatedList = new ArrayList<Double>();
		BufferedReader br = new BufferedReader(new FileReader(args[0]));
		String line;
		while (null != (line = br.readLine())) {
			String[] line_parts = line.split(" ");
			if (line_parts.length > 1) {
				StringBuilder desc = new StringBuilder(line_parts[1]);
				for (int i = 2; i < line_parts.length; i++) {
					desc.append(line_parts[i]);
				}
				double executionTime;
				if (!desc.toString().contains("E")) {
					executionTime = Double.parseDouble(desc.toString());
				} else {
					System.out.println();
					String beforeE = desc.substring(0, desc.lastIndexOf("E"));
					String afterE = desc.substring(desc.lastIndexOf("E") + 1);
					System.out.println("beforeE" + " = " + beforeE);
					System.out.println("afterE" + " = " + afterE);
					double number = Double.parseDouble(beforeE);
					double power = Double.parseDouble(afterE);
					executionTime = number * Math.pow(10, power);
					System.out.println("number" + " = " + number);
					System.out.println("power" + " = " + power);
					System.out.println("executionTime" + " = " + executionTime);
					System.out.println();
				}
				list.add(executionTime);
			}
		}
		br.close();
		for (int i = 0; i < list.size(); i++) {
			System.out.println(i + " : " + list.get(i));
		}
		truncatedList = getTruncatedListOfExecutionTime(list, 500);
		System.out.println("truncatedList.size() = " + truncatedList.size());
		System.out.println("truncatedList.get(0) = " + truncatedList.get(0));
		System.out.println("truncatedList.get(truncatedList.size()-1) = " + truncatedList.get(truncatedList.size()-1));
		System.out.println("mean = "
				+ getMeanFromListOfExecutionTime(truncatedList));
		System.out.println("stdv = "
				+ getStandardDeviationFromListOfExecutionTime(truncatedList));
	}

	public static List<Double> getTruncatedListOfExecutionTime(
			final List<Double> listOfExecutionTime, final int n) {
		List<Double> truncatedList = new ArrayList<Double>();
		if (listOfExecutionTime.size() > 100) {
			for (int i = n; i < listOfExecutionTime.size(); i++) {
				truncatedList.add(listOfExecutionTime.get(i));
			}
		} else {
			return listOfExecutionTime;
		}
		return truncatedList;
	}

	public static double getMeanFromListOfExecutionTime(
			final List<Double> listOfExecutionTime) {
		double averageExecutiontime = 0;
		double sumExecutiontime = 0;
		if (!listOfExecutionTime.isEmpty()) {
			for (double execTime : listOfExecutionTime) {
				sumExecutiontime += execTime;
			}
			averageExecutiontime = sumExecutiontime
					/ listOfExecutionTime.size();
		}
		return averageExecutiontime;
	}

	private static double getVarianceFromListOfExecutionTime(
			final List<Double> listOfExecutionTime) {
		double varianceExecutionTime = 0;
		double tmp = 0;
		double averageExecutionTime = getMeanFromListOfExecutionTime(listOfExecutionTime);
		if (!listOfExecutionTime.isEmpty()) {
			for (double execTime : listOfExecutionTime) {
				tmp += (execTime - averageExecutionTime)
						* (execTime - averageExecutionTime);
			}
			varianceExecutionTime = tmp / listOfExecutionTime.size();
		}
		return varianceExecutionTime;
	}

	public static double getStandardDeviationFromListOfExecutionTime(
			final List<Double> listOfExecutionTime) {
		double stdv = 0;
		stdv = Math
				.sqrt(getVarianceFromListOfExecutionTime(listOfExecutionTime));
		return stdv;
	}
}
