#!/bin/bash

MUDEBS_HOME_SCRIPTS="$(cd $(dirname "$0")/../../../../muDEBS/scripts/ && pwd)"
${MUDEBS_HOME_SCRIPTS}/utils.sh

M0=mudebs://inf-8661
M1=mudebs://inf-8608
M2=mudebs://inf-8941
B0_URI=$M0:3000/B0
B1_URI=$M1:3001/B1
C0_URI=$M2:2000/C0
C1_URI=$M2:2001/C1
perfmode=QOCMCVC
nb_constraints=1
config_file="./ressources/qocmcvc_1000-pub_1-sub_${nb_constraints}-const.config"

echo 'Architecture of 2 brokers'
echo sleep 0.5
sleep 0.5


#==============================================================================
# The following code should be executed at the machine M2 hosting C0 and C1                                                            
#==============================================================================
# performance mode:                                                            
# - CBC 		= context-based filters with constraints
# - QOCMCCC	= QoC-based multi-constraints with crierion constraints
# - QOCMCVC	= QoC-based multi-constraints with value constraints
# - QOCMMCC	= QoC-based multi-metadata with criterion constraint
# - QOCMMVC	= QoC-based multi-metadata with value constraints
# - DEFAULT	= none of the above modes is applied
# Note2: only one of both "--nb_constraints" and "--nb_indidcators" is used.
# Note3: a filter whose identifier begins with "startperf" and its associated 
# messages... are not taken into account in the results.
#===============================================================================
echo 
echo "Starting client C0 connected to broker B0"
${MUDEBS_HOME_SCRIPTS}/startqocimperfclient --uri $C0_URI --broker $B0_URI --eval_perf_filters_mode $perfmode --nb_constraints $nb_constraints --eval_perf_nb_pub 1100 --time_between_two_pub 1000
echo sleep 0.5
sleep 0.5
echo "Starting client C1 connected to broker B1"
${MUDEBS_HOME_SCRIPTS}/startqocimperfclient --uri $C1_URI --broker $B1_URI --eval_perf_filters_mode $perfmode --nb_constraints $nb_constraints --eval_perf_nb_sub 1
echo sleep 0.5
sleep 0.5


echo "Please wait..."
echo "Start publishing"
echo C0 advertising filter \'startperfsubscribing\'
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2000/C0 --command advertise --id 'startperfsubscribing' --file ./ressources/advertisement.mudebs 
sleep 1
echo C0 publishing subscribing command with advertising filter \'startperfsubscribing\'
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2000/C0 --command publish --id 'startperfsubscribing' --content '<command><noscoping><startsubscribing>-C1-</startsubscribing></noscoping></command>'
sleep 1
echo


echo
echo "Hit return to let clients to unsubscribe and unadvertise to all filters"
read x
echo "Client C0 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri $C0_URI --command unsubscribe
echo sleep 1
sleep 1
echo "Client C0 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri $C0_URI --command unadvertise
echo sleep 1
sleep 1
echo "Client C1 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri $C1_URI --command unsubscribe
echo sleep 1
sleep 1
echo "Client C1 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri $C1_URI --command unadvertise
echo sleep 1
sleep 1


echo
echo "Hit return to set logger overlay to debug for all brokers"
read x
echo
echo "Setting logger overlay to debug for B0"
${MUDEBS_HOME_SCRIPTS}/broker --uri $B0_URI --command setlog --name overlay --level debug
echo sleep 1
sleep 1
echo "Setting logger overlay to debug for B1"
${MUDEBS_HOME_SCRIPTS}/broker --uri $B1_URI --command setlog --name overlay --level debug
echo sleep 1
sleep 1


echo
echo "Hit return to send termination detection to broker B0"
read x
echo On B0, termination detection
${MUDEBS_HOME_SCRIPTS}/broker --uri $B0_URI --command term_detection
echo sleep 1
sleep 1


echo
echo "Hit return to send terminate to broker B0"
read x
echo On B0, terminate
${MUDEBS_HOME_SCRIPTS}/broker --uri $B0_URI --command terminate_all
sleep 5

echo
echo "Hit return to force the termination of all clients and brokers"
read x
echo "Stopping all the clients"
${MUDEBS_HOME_SCRIPTS}/stopclient C0
${MUDEBS_HOME_SCRIPTS}/stopclient C1
echo "Stopping all the brokers"
${MUDEBS_HOME_SCRIPTS}/stopbroker B0
${MUDEBS_HOME_SCRIPTS}/stopbroker B1
