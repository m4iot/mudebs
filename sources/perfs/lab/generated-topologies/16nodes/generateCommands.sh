#!/bin/bash

. "$(cd $(dirname "$0") && pwd)"/util.sh

#echo ''
#echo '#identifiers of brokers'
#generateidbroker $STARTING_BROKER $ENDING_BROKER
#echo ''
#echo '#uri of brokers'
#generateuribroker $STARTING_BROKER $ENDING_BROKER $HOSTNAME
#echo ''
#echo '#identifiers of clients'
#generateidclient $STARTING_BROKER $ENDING_BROKER
#echo ''
#echo '#uri of clients'
#generateuriclient $STARTING_BROKER $ENDING_BROKER $HOSTNAME
#echo ''
#echo '#starting brokers'
#generatestartbroker $STARTING_BROKER $ENDING_BROKER
#echo ''
#echo '#starting clients'
#generatestartclient $STARTING_BROKER $ENDING_BROKER
#echo ''
#echo '#starting connections' 
#generatestarconnectingbrokers 16nodes.gtitm-0.tcl
#generatestarjoinscopecalls joinscopeCalls.input

generateunsubpuball $STARTING_BROKER $ENDING_BROKER