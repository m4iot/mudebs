#!/bin/bash

MUDEBS_HOME_SCRIPTS="$(cd ../../../../muDEBS/scripts/ && pwd)"

CONFIG_FILE=./ressources/performance_16nodes.config

HOSTNAME=localhost
STARTING_PORT_BROKER=3000
STARTING_PORT_CLIENT=2000
STARTING_BROKER=0
ENDING_BROKER=15
MAPDOWN_FILTER=./ressources/mapdownfilter.mudebs
MAPUP_FILTER=./ressources/mapupfilter.mudebs
ADV_FILTER=./ressources/advertisement.mudebs
SUB_FILTER=./ressources/subscription.mudebs


# dimensions
DS=DS
DT=DT
DR=DR

# scopes of dimension R
R1=R1
R2=R2
R3=R3
R4=R4
R5=R5
R6=R6
R7=R7
R8=R8
R9=R9
R10=R10
R11=R11
R12=R12
R13=R13
R14=R14

# scopes of dimension S
S1=S1
S2=S2
S3=S3
S4=S4
S5=S5
S6=S6
S7=S7
S8=S8
S9=S9
S10=S10
S11=S11
S12=S12
S13=S13
S14=S14

# scopes of dimension T
T1=T1
T2=T2
T3=T3
T4=T4
T5=T5
T6=T6
T7=T7
T8=T8
T9=T9
T10=T10
T11=T11
T12=T12
T13=T13
T14=T14

#generated identifiers and uri
#------------------------------------------------------------------------------
#identifiers of brokers
B0=B0
B1=B1
B2=B2
B3=B3
B4=B4
B5=B5
B6=B6
B7=B7
B8=B8
B9=B9
B10=B10
B11=B11
B12=B12
B13=B13
B14=B14
B15=B15

#uri of brokers
B0_URI=mudebs://localhost:3000/B0
B1_URI=mudebs://localhost:3001/B1
B2_URI=mudebs://localhost:3002/B2
B3_URI=mudebs://localhost:3003/B3
B4_URI=mudebs://localhost:3004/B4
B5_URI=mudebs://localhost:3005/B5
B6_URI=mudebs://localhost:3006/B6
B7_URI=mudebs://localhost:3007/B7
B8_URI=mudebs://localhost:3008/B8
B9_URI=mudebs://localhost:3009/B9
B10_URI=mudebs://localhost:3010/B10
B11_URI=mudebs://localhost:3011/B11
B12_URI=mudebs://localhost:3012/B12
B13_URI=mudebs://localhost:3013/B13
B14_URI=mudebs://localhost:3014/B14
B15_URI=mudebs://localhost:3015/B15

#identifiers of clients
C0=C0
C1=C1
C2=C2
C3=C3
C4=C4
C5=C5
C6=C6
C7=C7
C8=C8
C9=C9
C10=C10
C11=C11
C12=C12
C13=C13
C14=C14
C15=C15

#uri of clients
C0_URI=mudebs://localhost:2000/C0
C1_URI=mudebs://localhost:2001/C1
C2_URI=mudebs://localhost:2002/C2
C3_URI=mudebs://localhost:2003/C3
C4_URI=mudebs://localhost:2004/C4
C5_URI=mudebs://localhost:2005/C5
C6_URI=mudebs://localhost:2006/C6
C7_URI=mudebs://localhost:2007/C7
C8_URI=mudebs://localhost:2008/C8
C9_URI=mudebs://localhost:2009/C9
C10_URI=mudebs://localhost:2010/C10
C11_URI=mudebs://localhost:2011/C11
C12_URI=mudebs://localhost:2012/C12
C13_URI=mudebs://localhost:2013/C13
C14_URI=mudebs://localhost:2014/C14
C15_URI=mudebs://localhost:2015/C15
#------------------------------------------------------------------------------


WHITE="\033[0m"
RED="\033[31m"
BLUE="\033[34m"
PINK="\033[35m"
CYAN="\033[36m"
YELLOW="\033[33m"
GREEN="\033[42m"

colorbroker(){
  broker=$1
  echo -e $CYAN""$broker$WHITE
}

colorclient(){
  client=$1
  echo -e $YELLOW""$client$WHITE
}

colordimension(){
  dimension=$1
  echo -e $RED""$dimension$WHITE
}

colorscope(){
  scope=$1
  echo -e $PINK""$scope$WHITE
}

colorfilter(){
  filter=$1
  echo -e $GREEN""$filter$WHITE
}

generateidbroker(){
  for (( i = $1; i <= $2; i++ ))
  do
    echo "B$i=B$i"
  done
}

generateuribroker(){
  for (( i = $1; i <= $2; i++ ))
  do
    echo 'B'$i'_URI=mudebs://'$3':'`expr $STARTING_PORT_BROKER + $i`'/B'$i
  done
}

generateidclient(){
  for (( i = $1; i <= $2; i++ ))
  do
    echo "C$i=C$i"
  done
}

generateuriclient(){
  for (( i = $1; i <= $2; i++ ))
  do
    echo 'C'$i'_URI=mudebs://'$3':'`expr $STARTING_PORT_CLIENT + $i`'/C'$i
  done
}

generatestartbroker(){
for (( i = $1; i <= $2; i++ ))
  do
    broker="B${i}"
    broker_uri="B${i}_URI"
    echo 'echo "Starting broker '${!broker}'"'
    echo '${MUDEBS_HOME_SCRIPTS}/startbroker --uri '${!broker_uri}' --log PERFORMANCE.PERF --perfconfigfile' $CONFIG_FILE
done
}

generatestartclient(){
for (( i = $1; i <= $2; i++ ))
  do
    client="C${i}"
    client_uri="C${i}_URI"
    broker_uri="B${i}_URI"
    echo 'echo "Starting client '${!client}' connected to broker 'B${i}'"'
    echo '${MUDEBS_HOME_SCRIPTS}/startperfclient --uri '${!client_uri}' --broker' ${!broker_uri} '--log ROUTING.INFO'
done
}

generatestarconnectingbrokers(){
while read line; do 
   case "$line" in \#*) continue ;; esac
   [[ $line =~ '$ns duplex-link $n('([0-9]+)') $n('([0-9]+) ]]
   first_number=${BASH_REMATCH[1]}
   second_number=${BASH_REMATCH[2]}
   if [ -n "$first_number" ]; then      
      first_broker="B${first_number}"
      second_broker="B${second_number}"
      first_broker_uri="B${first_number}_URI"
      second_broker_uri="B${second_number}_URI"         
      echo 'echo "Connecting broker '$first_broker' to broker '$second_broker'"'
      echo '${MUDEBS_HOME_SCRIPTS}/broker --uri '${!first_broker_uri}' --command connect --neigh '${!second_broker_uri}
   fi
done < $1
}

#/broker's identifier/dimension/subscope/superscope
generatestarjoinscopecalls(){
positiveInteger='[0-9]+$'
while read line; do 
   case "$line" in \#*) continue ;; esac
   broker_id=$(cut -d/ -f2 <<<"${line}")
   dimension=$(cut -d/ -f3 <<<"${line}")
   subscope=$(cut -d/ -f4 <<<"${line}")
   superscope=$(cut -d/ -f5 <<<"${line}")
   if [[ -n "$broker_id" ]]; then 
      broker="B${broker_id}"      
      broker_uri="B${broker_id}_URI"      
      echo -e 'echo "On '${!broker}', join scope ('${subscope}','${superscope}')"'
      echo -e '${MUDEBS_HOME_SCRIPTS}/broker --uri '${!broker_uri}' --command joinscope --dimension '${dimension}' --subscope '${subscope}' --superscope' ${superscope} '--mapupfile' $MAPUP_FILTER '--mapdownfile' $MAPDOWN_FILTER
      echo 'sleep 3'
   fi
done < $1
}

generateunsubpuball(){
for (( i = $1; i <= $2; i++ ))
  do
    client="C${i}"      
    client_uri="C${i}_URI" 
    echo -e 'echo "Client '${!client}' unsubscribe all filters"'
    echo '${MUDEBS_HOME_SCRIPTS}/client --uri '${!client_uri}' --command unsubscribe'
    echo -e 'echo "Client '${!client}' unadvertise all filters"'
    echo -e '${MUDEBS_HOME_SCRIPTS}/client --uri '${!client_uri}' --command unadvertise'
done
}

stopclient(){
for (( i = $1; i <= $2; i++ ))
  do
    client="C${i}"
    echo -e "Stoping client ${!client}"
    ${MUDEBS_HOME_SCRIPTS}/stopclient ${!client}
done
}

stopbroker(){
for (( i = $1; i <= $2; i++ ))
  do
    broker="B${i}"
    echo -e "Stoping broker ${!broker}"
    ${MUDEBS_HOME_SCRIPTS}/stopbroker ${!broker}
done
}

unsubpuball(){
for (( i = $1; i <= $2; i++ ))
  do
    client="C${i}"      
    client_uri="C${i}_URI" 
    echo -e "Client ${!client} unsubscribe all filters"
    ${MUDEBS_HOME_SCRIPTS}/client --uri ${!client_uri} --command unsubscribe
    echo
    echo -e "Client ${!client} unadvertise all filters"
    ${MUDEBS_HOME_SCRIPTS}/client --uri ${!client_uri} --command unadvertise
done
}