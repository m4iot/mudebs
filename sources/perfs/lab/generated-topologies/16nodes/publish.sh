#!/bin/bash

. "$(cd $(dirname "$0") && pwd)"/util.sh

echo
echo "Hit return when you want to send command startpublishing to all clients"
read x
echo 
echo CX advertising filter \'startperfpublishing\'
${MUDEBS_HOME_SCRIPTS}/client --uri $CX --command advertise --ack local --id 'startperfpublishing' --file $ADV_FILTER 
sleep 1
echo CX publishing startpublishing command with advertising filter \'startperfpublishing\' with global acknowledgement
${MUDEBS_HOME_SCRIPTS}/client --uri $CX --command publish --ack global --id 'startperfpublishing' --content '<command><startpublishing>1</startpublishing></command>'
sleep 1
echo "Please wait..."
