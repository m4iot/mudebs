#!/bin/bash

. "$(cd $(dirname "$0") && pwd)"/util.sh

echo 'Architecture of 16 brokers'
echo sleep 1
sleep 1

echo
echo "Starting client C0 connected to broker B0"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2500/C0 --broker mudebs://localhost:3500/B0 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_16nodes.config #--log ROUTING.INFO 
echo sleep 1
sleep 1
echo "Starting client C1 connected to broker B1"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2501/C1 --broker mudebs://localhost:3501/B1 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_16nodes.config #--log ROUTING.INFO
echo sleep 1
sleep 1
echo "Starting client C2 connected to broker B2"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2502/C2 --broker mudebs://localhost:3502/B2 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_16nodes.config #--log ROUTING.INFO 
echo sleep 1
sleep 1
echo "Starting client C3 connected to broker B3"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2503/C3 --broker mudebs://localhost:3503/B3 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_16nodes.config #--log ROUTING.INFO 
echo sleep 1
sleep 1
echo "Starting client C4 connected to broker B4"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2504/C4 --broker mudebs://localhost:3504/B4 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_16nodes.config #--log ROUTING.INFO 
echo sleep 1
sleep 1
echo "Starting client C5 connected to broker B5"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2505/C5 --broker mudebs://localhost:3505/B5 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_16nodes.config #--log ROUTING.INFO 
echo sleep 1
sleep 1
echo "Starting client C6 connected to broker B6"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2506/C6 --broker mudebs://localhost:3506/B6 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_16nodes.config #--log ROUTING.INFO 
echo sleep 1
sleep 1
echo "Starting client C7 connected to broker B7"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2507/C7 --broker mudebs://localhost:3507/B7 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_16nodes.config #--log ROUTING.INFO 
echo sleep 1
sleep 1
echo "Starting client C8 connected to broker B8"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2508/C8 --broker mudebs://localhost:3508/B8 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_16nodes.config #--log ROUTING.INFO 
echo sleep 1
sleep 1
echo "Starting client C9 connected to broker B9"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2509/C9 --broker mudebs://localhost:3509/B9 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_16nodes.config #--log ROUTING.INFO 
echo sleep 1
sleep 1
echo "Starting client C10 connected to broker B10"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2510/C10 --broker mudebs://localhost:3510/B10 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_16nodes.config #--log ROUTING.INFO 
echo sleep 1
sleep 1
echo "Starting client C11 connected to broker B11"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2511/C11 --broker mudebs://localhost:3511/B11 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_16nodes.config #--log ROUTING.INFO 
echo sleep 1
sleep 1
echo "Starting client C12 connected to broker B12"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2512/C12 --broker mudebs://localhost:3512/B12 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_16nodes.config #--log ROUTING.INFO 
echo sleep 1
sleep 1
echo "Starting client C13 connected to broker B13"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2513/C13 --broker mudebs://localhost:3513/B13 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_16nodes.config #--log ROUTING.INFO 
echo sleep 1
sleep 1
echo "Starting client C14 connected to broker B14"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2514/C14 --broker mudebs://localhost:3514/B14 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_16nodes.config #--log ROUTING.INFO 
echo sleep 1
sleep 1
echo "Starting client C15 connected to broker B15"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2515/C15 --broker mudebs://localhost:3515/B15 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_16nodes.config #--log ROUTING.INFO 
sleep 20

echo
echo "Connecting broker B0 to broker B5"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3500/B0 --command connect --neigh mudebs://localhost:3505/B5
echo sleep 1
sleep 1
echo "Connecting broker B0 to broker B4"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3500/B0 --command connect --neigh mudebs://localhost:3504/B4
echo sleep 1
sleep 1
echo "Connecting broker B0 to broker B1"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3500/B0 --command connect --neigh mudebs://localhost:3501/B1
echo sleep 1
sleep 1
echo "Connecting broker B0 to broker B2"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3500/B0 --command connect --neigh mudebs://localhost:3502/B2
echo sleep 1
sleep 1
echo "Connecting broker B0 to broker B3"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3500/B0 --command connect --neigh mudebs://localhost:3503/B3
echo sleep 1
sleep 1
echo "Connecting broker B1 to broker B9"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3501/B1 --command connect --neigh mudebs://localhost:3509/B9
echo sleep 1
sleep 1
echo "Connecting broker B1 to broker B8"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3501/B1 --command connect --neigh mudebs://localhost:3508/B8
echo sleep 1
sleep 1
echo "Connecting broker B1 to broker B7"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3501/B1 --command connect --neigh mudebs://localhost:3507/B7
echo sleep 1
sleep 1
echo "Connecting broker B1 to broker B6"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3501/B1 --command connect --neigh mudebs://localhost:3506/B6
echo sleep 1
sleep 1
echo "Connecting broker B1 to broker B2"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3501/B1 --command connect --neigh mudebs://localhost:3502/B2
echo sleep 1
sleep 1
echo "Connecting broker B1 to broker B3"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3501/B1 --command connect --neigh mudebs://localhost:3503/B3
echo sleep 1
sleep 1
echo "Connecting broker B2 to broker B12"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3502/B2 --command connect --neigh mudebs://localhost:3512/B12
echo sleep 1
sleep 1
echo "Connecting broker B2 to broker B11"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3502/B2 --command connect --neigh mudebs://localhost:3511/B11
echo sleep 1
sleep 1
echo "Connecting broker B2 to broker B10"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3502/B2 --command connect --neigh mudebs://localhost:3510/B10
echo sleep 1
sleep 1
echo "Connecting broker B2 to broker B3"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3502/B2 --command connect --neigh mudebs://localhost:3503/B3
echo sleep 1
sleep 1
echo "Connecting broker B3 to broker B15"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3503/B3 --command connect --neigh mudebs://localhost:3515/B15
echo sleep 1
sleep 1
echo "Connecting broker B3 to broker B14"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3503/B3 --command connect --neigh mudebs://localhost:3514/B14
echo sleep 1
sleep 1
echo "Connecting broker B3 to broker B13"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3503/B3 --command connect --neigh mudebs://localhost:3513/B13
echo sleep 1
sleep 20

echo
echo "On B0, join scope (s1,top)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3500/B0 --command joinscope --dimension s --subscope s1 --superscope top --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 10
sleep 10
echo "On B2, join scope (s2,s1)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3502/B2 --command joinscope --dimension s --subscope s2 --superscope s1 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 10
sleep 10
echo "On B11, join scope (s4,s2)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3511/B11 --command joinscope --dimension s --subscope s4 --superscope s2 --mapupfile ./ressources/visibility-filter-always-false.mudebs --mapdownfile ./ressources/visibility-filter-always-false.mudebs
echo sleep 10
sleep 10
echo "On B11, join scope (s8,s4)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3511/B11 --command joinscope --dimension s --subscope s8 --superscope s4 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 10
sleep 10
echo "On B11, join scope (s9,s4)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3511/B11 --command joinscope --dimension s --subscope s9 --superscope s4 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 10
sleep 10
echo "On B10, join scope (s5,s2)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3510/B10 --command joinscope --dimension s --subscope s5 --superscope s2 --mapupfile ./ressources/visibility-filter-always-false.mudebs --mapdownfile ./ressources/visibility-filter-always-false.mudebs
echo sleep 10
sleep 10
echo "On B10, join scope (s10,s5)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3510/B10 --command joinscope --dimension s --subscope s10 --superscope s5 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 10
sleep 10
echo "On B10, join scope (s11,s5)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3510/B10 --command joinscope --dimension s --subscope s11 --superscope s5 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 10
sleep 10
echo "On B3, join scope (s3,s1)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3503/B3 --command joinscope --dimension s --subscope s3 --superscope s1 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 10
sleep 10
echo "On B14, join scope (s6,s3)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3514/B14 --command joinscope --dimension s --subscope s6 --superscope s3 --mapupfile ./ressources/visibility-filter-always-false.mudebs --mapdownfile ./ressources/visibility-filter-always-false.mudebs
echo sleep 10
sleep 10
echo "On B14, join scope (s12,s6)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3514/B14 --command joinscope --dimension s --subscope s12 --superscope s6 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 10
sleep 10
echo "On B14, join scope (s13,s6)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3514/B14 --command joinscope --dimension s --subscope s13 --superscope s6 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 10
sleep 10
echo "On B13, join scope (s7,s3)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3513/B13 --command joinscope --dimension s --subscope s7 --superscope s3 --mapupfile ./ressources/visibility-filter-always-false.mudebs --mapdownfile ./ressources/visibility-filter-always-false.mudebs
echo sleep 10
sleep 10
echo "On B13, join scope (s14,s7)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3513/B13 --command joinscope --dimension s --subscope s14 --superscope s7 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 10
sleep 10
echo "On B13, join scope (s15,s7)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3513/B13 --command joinscope --dimension s --subscope s15 --superscope s7 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 10
sleep 10


echo
echo "Please wait..."
echo "C0 advertising filter 'startperfsubscribing'"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2500/C0 --command advertise --id 'startperfsubscribing' --file ./ressources/advertisement.mudebs
echo sleep 5
sleep 5
echo "C0 publishing subscribing command with advertising filter 'startperfsubscribing'"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2500/C0 --command publish --id 'startperfsubscribing' --content '<command><startsubscribing>w16w-C0-</startsubscribing></command>'


echo
echo "Hit return when you want clients to unsubscribe to all filters"
read x
echo "Client C0 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2500/C0 --command unsubscribe
echo sleep 5
sleep 5
echo "Client C0 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2500/C0 --command unadvertise
echo sleep 5
sleep 5
echo "Client C1 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2501/C1 --command unsubscribe
echo sleep 5
sleep 5
echo "Client C1 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2501/C1 --command unadvertise
echo sleep 5
sleep 5
echo "Client C2 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2502/C2 --command unsubscribe
echo sleep 5
sleep 5
echo "Client C2 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2502/C2 --command unadvertise
echo sleep 5
sleep 5
echo "Client C3 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2503/C3 --command unsubscribe
echo sleep 5
sleep 5
echo "Client C3 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2503/C3 --command unadvertise
echo sleep 5
sleep 5
echo "Client C4 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2504/C4 --command unsubscribe
echo sleep 5
sleep 5
echo "Client C4 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2504/C4 --command unadvertise
echo sleep 5
sleep 5
echo "Client C5 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2505/C5 --command unsubscribe
echo sleep 5
sleep 5
echo "Client C5 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2505/C5 --command unadvertise
echo sleep 5
sleep 5
echo "Client C6 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2506/C6 --command unsubscribe
echo sleep 5
sleep 5
echo "Client C6 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2506/C6 --command unadvertise
echo sleep 5
sleep 5
echo "Client C7 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2507/C7 --command unsubscribe
echo sleep 5
sleep 5
echo "Client C7 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2507/C7 --command unadvertise
echo sleep 5
sleep 5
echo "Client C8 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2508/C8 --command unsubscribe
echo sleep 5
sleep 5
echo "Client C8 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2508/C8 --command unadvertise
echo sleep 5
sleep 5
echo "Client C9 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2509/C9 --command unsubscribe
echo sleep 5
sleep 5
echo "Client C9 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2509/C9 --command unadvertise
echo sleep 5
sleep 5
echo "Client C10 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2510/C10 --command unsubscribe
echo sleep 5
sleep 5
echo "Client C10 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2510/C10 --command unadvertise
echo sleep 5
sleep 5
echo "Client C11 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2511/C11 --command unsubscribe
echo sleep 5
sleep 5
echo "Client C11 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2511/C11 --command unadvertise
echo sleep 5
sleep 5
echo "Client C12 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2512/C12 --command unsubscribe
echo sleep 5
sleep 5
echo "Client C12 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2512/C12 --command unadvertise
echo sleep 5
sleep 5
echo "Client C13 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2513/C13 --command unsubscribe
echo sleep 5
sleep 5
echo "Client C13 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2513/C13 --command unadvertise
echo sleep 5
sleep 5
echo "Client C14 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2514/C14 --command unsubscribe
echo sleep 5
sleep 5
echo "Client C14 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2514/C14 --command unadvertise
echo sleep 5
sleep 5
echo "Client C15 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2515/C15 --command unsubscribe
echo sleep 5
sleep 5
echo "Client C15 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2515/C15 --command unadvertise
sleep 60


echo
echo "Hit return when you want to set logger overlay to debug for all brokers"
read x
echo
echo "Setting logger overlay to debug for B0"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3500/B0 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B1"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3501/B1 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B2"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3502/B2 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B3"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3503/B3 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B4"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3504/B4 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B5"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3505/B5 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B6"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3506/B6 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B7"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3507/B7 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B8"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3508/B8 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B9"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3509/B9 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B10"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3510/B10 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B11"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3511/B11 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B12"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3512/B12 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B13"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3513/B13 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B14"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3514/B14 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B15"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3515/B15 --command setlog --name overlay --level debug
echo sleep 5
sleep 5

echo
echo "Hit return when you want to send termination detection to broker B0"
read x
echo On B0, termination detection
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3500/B0 --command term_detection
sleep 60

echo
echo "Hit return when you want to send terminate to broker B0"
read x
echo On B0, terminate
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3500/B0 --command terminate_all
