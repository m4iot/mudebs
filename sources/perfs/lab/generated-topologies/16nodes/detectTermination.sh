#!/bin/bash

. "$(cd $(dirname "$0") && pwd)"/util.sh

echo
echo "Hit return when you want to send termination detection to broker $1"
read x
echo

echo
echo On $1, termination detection
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri $1 --command term_detection

