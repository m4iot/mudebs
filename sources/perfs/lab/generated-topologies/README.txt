Content of this file :
- Installation of itm, sgb2ns, ns, nam
- Generating graphs from GT-ITM topology generator
- Conversion of GT-ITM output to ns-2 format with sgb2ns
- Visualisation of the generated topology with ns-2 and nam
- Makefile

===============================================================================

Installation of itm, sgb2ns, ns, nam:
-------------------------------------
- In sgb2ns.c file
	- Replace "duplex-link-of-interfaces" by "duplex-link"
	- Also change "upvar $node" n to "global ns n"

- Then, follow this note : 
http://www.ict.csiro.au/staff/ren.liu/ns-2/ns-2InstallationNote.htm
- If the following problem occurs when installing: 
--- begin problem ----
In file included from linkstate/ls.cc:67:0:
linkstate/ls.h: In instantiation of ‘void LsMap<Key, T>::eraseAll() 
[with Key = int; T = LsIdSeq]’:
linkstate/ls.cc:396:28:   required from here
linkstate/ls.h:137:58: error: ‘erase’ was not declared in this scope, and no 
declarations were found by argument-dependent lookup at the point of 
instantiation [-fpermissive]
  void eraseAll() { erase(baseMap::begin(), baseMap::end()); }
[...]
                                                         ^
--- end problem ----

Chnge the following line in ns-2.35/linkstate/ls.h file:
Replace 

void eraseAll() {erase(baseMap::begin(), baseMap::end()); } 

with 

void eraseAll() { baseMap::erase(baseMap::begin(), baseMap::end()); } 

$ ./validate  
Most of tests should be validated.

Generating graphs from GT-ITM topology generator:
-------------------------------------------------
The topologies of the overlay IP network of brokers are constructed using the 
GT-ITM Tool [1] to generate Internet topology. 

# itm <spec-file0> <spec-file1> ....

Conversion of GT-ITM output to ns-2 format with sgb2ns:
-------------------------------------------------------
sgb2ns [2] is used to convert Geogia Tech's topology generator GT-ITM's 
output to ns-2 format.

# sgb2ns <sgfile> <nsfile>


Makefile:
--------

The generation process (including all the compilation steps) is written
in the Makefile. There are two parameters that can be configured:
- BASE: name of the gtitm input file (ex. example for example.gtitm); and
- NUMBER: number of gb generate file (ex. 0 for example.gtitm-0.gb)




[1] G. T. College of Computing. http://www.cc.gatech.edu/projects/gtitm
[2] https://stuff.mit.edu/afs/sipb/project/sipbnet/src/ns-allinone-2.1b4a/gt-itm/sgb2ns/README
