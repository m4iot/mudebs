#!/bin/bash

MUDEBS_HOME_SCRIPTS="$(cd ../../../../muDEBS/scripts/ && pwd)"

echo 'Architecture of 100 brokers'
sleep 1

echo
echo "Starting client C0 connected to broker B0"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2000/C0 --broker mudebs://localhost:3000/B0 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C1 connected to broker B1"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2001/C1 --broker mudebs://localhost:3001/B1 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C2 connected to broker B2"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2002/C2 --broker mudebs://localhost:3002/B2 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C3 connected to broker B3"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2003/C3 --broker mudebs://localhost:3003/B3 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C4 connected to broker B4"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2004/C4 --broker mudebs://localhost:3004/B4 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C5 connected to broker B5"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2005/C5 --broker mudebs://localhost:3005/B5 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C6 connected to broker B6"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2006/C6 --broker mudebs://localhost:3006/B6 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C7 connected to broker B7"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2007/C7 --broker mudebs://localhost:3007/B7 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C8 connected to broker B8"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2008/C8 --broker mudebs://localhost:3008/B8 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C9 connected to broker B9"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2009/C9 --broker mudebs://localhost:3009/B9 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C10 connected to broker B10"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2010/C10 --broker mudebs://localhost:3010/B10 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C11 connected to broker B11"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2011/C11 --broker mudebs://localhost:3011/B11 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C12 connected to broker B12"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2012/C12 --broker mudebs://localhost:3012/B12 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C13 connected to broker B13"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2013/C13 --broker mudebs://localhost:3013/B13 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C14 connected to broker B14"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2014/C14 --broker mudebs://localhost:3014/B14 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C15 connected to broker B15"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2015/C15 --broker mudebs://localhost:3015/B15 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C16 connected to broker B16"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2016/C16 --broker mudebs://localhost:3016/B16 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C17 connected to broker B17"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2017/C17 --broker mudebs://localhost:3017/B17 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C18 connected to broker B18"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2018/C18 --broker mudebs://localhost:3018/B18 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C19 connected to broker B19"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2019/C19 --broker mudebs://localhost:3019/B19 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C20 connected to broker B20"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2020/C20 --broker mudebs://localhost:3020/B20 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C21 connected to broker B21"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2021/C21 --broker mudebs://localhost:3021/B21 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C22 connected to broker B22"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2022/C22 --broker mudebs://localhost:3022/B22 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C23 connected to broker B23"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2023/C23 --broker mudebs://localhost:3023/B23 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C24 connected to broker B24"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2024/C24 --broker mudebs://localhost:3024/B24 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C25 connected to broker B25"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2025/C25 --broker mudebs://localhost:3025/B25 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C26 connected to broker B26"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2026/C26 --broker mudebs://localhost:3026/B26 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C27 connected to broker B27"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2027/C27 --broker mudebs://localhost:3027/B27 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C28 connected to broker B28"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2028/C28 --broker mudebs://localhost:3028/B28 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C29 connected to broker B29"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2029/C29 --broker mudebs://localhost:3029/B29 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C30 connected to broker B30"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2030/C30 --broker mudebs://localhost:3030/B30 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C31 connected to broker B31"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2031/C31 --broker mudebs://localhost:3031/B31 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C32 connected to broker B32"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2032/C32 --broker mudebs://localhost:3032/B32 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C33 connected to broker B33"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2033/C33 --broker mudebs://localhost:3033/B33 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C34 connected to broker B34"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2034/C34 --broker mudebs://localhost:3034/B34 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C35 connected to broker B35"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2035/C35 --broker mudebs://localhost:3035/B35 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C36 connected to broker B36"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2036/C36 --broker mudebs://localhost:3036/B36 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C37 connected to broker B37"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2037/C37 --broker mudebs://localhost:3037/B37 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C38 connected to broker B38"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2038/C38 --broker mudebs://localhost:3038/B38 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C39 connected to broker B39"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2039/C39 --broker mudebs://localhost:3039/B39 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C40 connected to broker B40"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2040/C40 --broker mudebs://localhost:3040/B40 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C41 connected to broker B41"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2041/C41 --broker mudebs://localhost:3041/B41 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C42 connected to broker B42"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2042/C42 --broker mudebs://localhost:3042/B42 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C43 connected to broker B43"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2043/C43 --broker mudebs://localhost:3043/B43 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C44 connected to broker B44"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2044/C44 --broker mudebs://localhost:3044/B44 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C45 connected to broker B45"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2045/C45 --broker mudebs://localhost:3045/B45 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C46 connected to broker B46"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2046/C46 --broker mudebs://localhost:3046/B46 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C47 connected to broker B47"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2047/C47 --broker mudebs://localhost:3047/B47 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C48 connected to broker B48"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2048/C48 --broker mudebs://localhost:3048/B48 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C49 connected to broker B49"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2049/C49 --broker mudebs://localhost:3049/B49 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C50 connected to broker B50"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2050/C50 --broker mudebs://localhost:3050/B50 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C51 connected to broker B51"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2051/C51 --broker mudebs://localhost:3051/B51 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C52 connected to broker B52"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2052/C52 --broker mudebs://localhost:3052/B52 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C53 connected to broker B53"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2053/C53 --broker mudebs://localhost:3053/B53 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C54 connected to broker B54"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2054/C54 --broker mudebs://localhost:3054/B54 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C55 connected to broker B55"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2055/C55 --broker mudebs://localhost:3055/B55 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C56 connected to broker B56"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2056/C56 --broker mudebs://localhost:3056/B56 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C57 connected to broker B57"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2057/C57 --broker mudebs://localhost:3057/B57 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C58 connected to broker B58"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2058/C58 --broker mudebs://localhost:3058/B58 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C59 connected to broker B59"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2059/C59 --broker mudebs://localhost:3059/B59 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C60 connected to broker B60"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2060/C60 --broker mudebs://localhost:3060/B60 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C61 connected to broker B61"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2061/C61 --broker mudebs://localhost:3061/B61 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C62 connected to broker B62"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2062/C62 --broker mudebs://localhost:3062/B62 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C63 connected to broker B63"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2063/C63 --broker mudebs://localhost:3063/B63 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C64 connected to broker B64"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2064/C64 --broker mudebs://localhost:3064/B64 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C65 connected to broker B65"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2065/C65 --broker mudebs://localhost:3065/B65 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C66 connected to broker B66"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2066/C66 --broker mudebs://localhost:3066/B66 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C67 connected to broker B67"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2067/C67 --broker mudebs://localhost:3067/B67 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C68 connected to broker B68"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2068/C68 --broker mudebs://localhost:3068/B68 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C69 connected to broker B69"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2069/C69 --broker mudebs://localhost:3069/B69 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C70 connected to broker B70"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2070/C70 --broker mudebs://localhost:3070/B70 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C71 connected to broker B71"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2071/C71 --broker mudebs://localhost:3071/B71 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C72 connected to broker B72"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2072/C72 --broker mudebs://localhost:3072/B72 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C73 connected to broker B73"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2073/C73 --broker mudebs://localhost:3073/B73 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C74 connected to broker B74"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2074/C74 --broker mudebs://localhost:3074/B74 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C75 connected to broker B75"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2075/C75 --broker mudebs://localhost:3075/B75 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C76 connected to broker B76"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2076/C76 --broker mudebs://localhost:3076/B76 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C77 connected to broker B77"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2077/C77 --broker mudebs://localhost:3077/B77 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C78 connected to broker B78"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2078/C78 --broker mudebs://localhost:3078/B78 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C79 connected to broker B79"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2079/C79 --broker mudebs://localhost:3079/B79 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C80 connected to broker B80"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2080/C80 --broker mudebs://localhost:3080/B80 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C81 connected to broker B81"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2081/C81 --broker mudebs://localhost:3081/B81 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C82 connected to broker B82"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2082/C82 --broker mudebs://localhost:3082/B82 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C83 connected to broker B83"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2083/C83 --broker mudebs://localhost:3083/B83 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C84 connected to broker B84"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2084/C84 --broker mudebs://localhost:3084/B84 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C85 connected to broker B85"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2085/C85 --broker mudebs://localhost:3085/B85 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C86 connected to broker B86"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2086/C86 --broker mudebs://localhost:3086/B86 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C87 connected to broker B87"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2087/C87 --broker mudebs://localhost:3087/B87 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C88 connected to broker B88"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2088/C88 --broker mudebs://localhost:3088/B88 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C89 connected to broker B89"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2089/C89 --broker mudebs://localhost:3089/B89 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C90 connected to broker B90"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2090/C90 --broker mudebs://localhost:3090/B90 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C91 connected to broker B91"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2091/C91 --broker mudebs://localhost:3091/B91 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C92 connected to broker B92"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2092/C92 --broker mudebs://localhost:3092/B92 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C93 connected to broker B93"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2093/C93 --broker mudebs://localhost:3093/B93 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C94 connected to broker B94"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2094/C94 --broker mudebs://localhost:3094/B94 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C95 connected to broker B95"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2095/C95 --broker mudebs://localhost:3095/B95 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C96 connected to broker B96"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2096/C96 --broker mudebs://localhost:3096/B96 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C97 connected to broker B97"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2097/C97 --broker mudebs://localhost:3097/B97 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C98 connected to broker B98"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2098/C98 --broker mudebs://localhost:3098/B98 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 1
sleep 1
echo "Starting client C99 connected to broker B99"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2099/C99 --broker mudebs://localhost:3099/B99 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_100nodes.config
echo sleep 20
sleep 20


echo
echo "Connecting broker B0 to broker B19"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3000/B0 --command connect --neigh mudebs://localhost:3019/B19
echo sleep 1
sleep 1
echo "Connecting broker B0 to broker B12"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3000/B0 --command connect --neigh mudebs://localhost:3012/B12
echo sleep 1
sleep 1
echo "Connecting broker B0 to broker B1"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3000/B0 --command connect --neigh mudebs://localhost:3001/B1
echo sleep 1
sleep 1
echo "Connecting broker B0 to broker B3"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3000/B0 --command connect --neigh mudebs://localhost:3003/B3
echo sleep 1
sleep 1
echo "Connecting broker B0 to broker B51"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3000/B0 --command connect --neigh mudebs://localhost:3051/B51
echo sleep 1
sleep 1
echo "Connecting broker B1 to broker B45"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3001/B1 --command connect --neigh mudebs://localhost:3045/B45
echo sleep 1
sleep 1
echo "Connecting broker B1 to broker B28"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3001/B1 --command connect --neigh mudebs://localhost:3028/B28
echo sleep 1
sleep 1
echo "Connecting broker B1 to broker B23"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3001/B1 --command connect --neigh mudebs://localhost:3023/B23
echo sleep 1
sleep 1
echo "Connecting broker B1 to broker B2"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3001/B1 --command connect --neigh mudebs://localhost:3002/B2
echo sleep 1
sleep 1
echo "Connecting broker B2 to broker B72"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3002/B2 --command connect --neigh mudebs://localhost:3072/B72
echo sleep 1
sleep 1
echo "Connecting broker B2 to broker B63"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3002/B2 --command connect --neigh mudebs://localhost:3063/B63
echo sleep 1
sleep 1
echo "Connecting broker B2 to broker B53"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3002/B2 --command connect --neigh mudebs://localhost:3053/B53
echo sleep 1
sleep 1
echo "Connecting broker B2 to broker B3"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3002/B2 --command connect --neigh mudebs://localhost:3003/B3
echo sleep 1
sleep 1
echo "Connecting broker B3 to broker B95"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3003/B3 --command connect --neigh mudebs://localhost:3095/B95
echo sleep 1
sleep 1
echo "Connecting broker B3 to broker B86"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3003/B3 --command connect --neigh mudebs://localhost:3086/B86
echo sleep 1
sleep 1
echo "Connecting broker B3 to broker B81"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3003/B3 --command connect --neigh mudebs://localhost:3081/B81
echo sleep 1
sleep 1
echo "Connecting broker B12 to broker B9"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3012/B12 --command connect --neigh mudebs://localhost:3009/B9
echo sleep 1
sleep 1
echo "Connecting broker B9 to broker B8"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3009/B9 --command connect --neigh mudebs://localhost:3008/B8
echo sleep 1
sleep 1
echo "Connecting broker B8 to broker B5"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3008/B8 --command connect --neigh mudebs://localhost:3005/B5
echo sleep 1
sleep 1
echo "Connecting broker B5 to broker B11"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3005/B5 --command connect --neigh mudebs://localhost:3011/B11
echo sleep 1
sleep 1
echo "Connecting broker B11 to broker B7"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3011/B11 --command connect --neigh mudebs://localhost:3007/B7
echo sleep 1
sleep 1
echo "Connecting broker B7 to broker B6"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3007/B7 --command connect --neigh mudebs://localhost:3006/B6
echo sleep 1
sleep 1
echo "Connecting broker B6 to broker B10"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3006/B6 --command connect --neigh mudebs://localhost:3010/B10
echo sleep 1
sleep 1
echo "Connecting broker B10 to broker B4"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3010/B10 --command connect --neigh mudebs://localhost:3004/B4
echo sleep 1
sleep 1
echo "Connecting broker B4 to broker B12"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3004/B4 --command connect --neigh mudebs://localhost:3012/B12
echo sleep 1
sleep 1
echo "Connecting broker B19 to broker B13"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3019/B19 --command connect --neigh mudebs://localhost:3013/B13
echo sleep 1
sleep 1
echo "Connecting broker B13 to broker B15"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3013/B13 --command connect --neigh mudebs://localhost:3015/B15
echo sleep 1
sleep 1
echo "Connecting broker B15 to broker B16"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3015/B15 --command connect --neigh mudebs://localhost:3016/B16
echo sleep 1
sleep 1
echo "Connecting broker B16 to broker B17"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3016/B16 --command connect --neigh mudebs://localhost:3017/B17
echo sleep 1
sleep 1
echo "Connecting broker B17 to broker B14"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3017/B17 --command connect --neigh mudebs://localhost:3014/B14
echo sleep 1
sleep 1
echo "Connecting broker B14 to broker B18"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3014/B14 --command connect --neigh mudebs://localhost:3018/B18
echo sleep 1
sleep 1
echo "Connecting broker B18 to broker B19"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3018/B18 --command connect --neigh mudebs://localhost:3019/B19
echo sleep 1
sleep 1
echo "Connecting broker B23 to broker B26"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3023/B23 --command connect --neigh mudebs://localhost:3026/B26
echo sleep 1
sleep 1
echo "Connecting broker B26 to broker B24"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3026/B26 --command connect --neigh mudebs://localhost:3024/B24
echo sleep 1
sleep 1
echo "Connecting broker B24 to broker B20"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3024/B24 --command connect --neigh mudebs://localhost:3020/B20
echo sleep 1
sleep 1
echo "Connecting broker B20 to broker B21"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3020/B20 --command connect --neigh mudebs://localhost:3021/B21
echo sleep 1
sleep 1
echo "Connecting broker B21 to broker B22"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3021/B21 --command connect --neigh mudebs://localhost:3022/B22
echo sleep 1
sleep 1
echo "Connecting broker B22 to broker B25"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3022/B22 --command connect --neigh mudebs://localhost:3025/B25
echo sleep 1
sleep 1
echo "Connecting broker B25 to broker B23"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3025/B25 --command connect --neigh mudebs://localhost:3023/B23
echo sleep 1
sleep 1
echo "Connecting broker B28 to broker B34"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3028/B28 --command connect --neigh mudebs://localhost:3034/B34
echo sleep 1
sleep 1
echo "Connecting broker B34 to broker B33"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3034/B34 --command connect --neigh mudebs://localhost:3033/B33
echo sleep 1
sleep 1
echo "Connecting broker B33 to broker B31"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3033/B33 --command connect --neigh mudebs://localhost:3031/B31
echo sleep 1
sleep 1
echo "Connecting broker B31 to broker B27"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3031/B31 --command connect --neigh mudebs://localhost:3027/B27
echo sleep 1
sleep 1
echo "Connecting broker B27 to broker B32"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3027/B27 --command connect --neigh mudebs://localhost:3032/B32
echo sleep 1
sleep 1
echo "Connecting broker B32 to broker B35"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3032/B32 --command connect --neigh mudebs://localhost:3035/B35
echo sleep 1
sleep 1
echo "Connecting broker B35 to broker B30"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3035/B35 --command connect --neigh mudebs://localhost:3030/B30
echo sleep 1
sleep 1
echo "Connecting broker B30 to broker B37"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3030/B30 --command connect --neigh mudebs://localhost:3037/B37
echo sleep 1
sleep 1
echo "Connecting broker B37 to broker B29"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3037/B37 --command connect --neigh mudebs://localhost:3029/B29
echo sleep 1
sleep 1
echo "Connecting broker B29 to broker B38"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3029/B29 --command connect --neigh mudebs://localhost:3038/B38
echo sleep 1
sleep 1
echo "Connecting broker B38 to broker B36"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3038/B38 --command connect --neigh mudebs://localhost:3036/B36
echo sleep 1
sleep 1
echo "Connecting broker B36 to broker B28"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3036/B36 --command connect --neigh mudebs://localhost:3028/B28
echo sleep 1
sleep 1
echo "Connecting broker B45 to broker B44"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3045/B45 --command connect --neigh mudebs://localhost:3044/B44
echo sleep 1
sleep 1
echo "Connecting broker B44 to broker B43"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3044/B44 --command connect --neigh mudebs://localhost:3043/B43
echo sleep 1
sleep 1
echo "Connecting broker B43 to broker B39"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3043/B43 --command connect --neigh mudebs://localhost:3039/B39
echo sleep 1
sleep 1
echo "Connecting broker B39 to broker B40"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3039/B39 --command connect --neigh mudebs://localhost:3040/B40
echo sleep 1
sleep 1
echo "Connecting broker B40 to broker B42"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3040/B40 --command connect --neigh mudebs://localhost:3042/B42
echo sleep 1
sleep 1
echo "Connecting broker B42 to broker B41"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3042/B42 --command connect --neigh mudebs://localhost:3041/B41
echo sleep 1
sleep 1
echo "Connecting broker B41 to broker B45"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3041/B41 --command connect --neigh mudebs://localhost:3045/B45
echo sleep 1
sleep 1
echo "Connecting broker B51 to broker B49"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3051/B51 --command connect --neigh mudebs://localhost:3049/B49
echo sleep 1
sleep 1
echo "Connecting broker B49 to broker B48"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3049/B49 --command connect --neigh mudebs://localhost:3048/B48
echo sleep 1
sleep 1
echo "Connecting broker B48 to broker B50"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3048/B48 --command connect --neigh mudebs://localhost:3050/B50
echo sleep 1
sleep 1
echo "Connecting broker B50 to broker B47"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3050/B50 --command connect --neigh mudebs://localhost:3047/B47
echo sleep 1
sleep 1
echo "Connecting broker B47 to broker B46"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3047/B47 --command connect --neigh mudebs://localhost:3046/B46
echo sleep 1
sleep 1
echo "Connecting broker B46 to broker B51"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3046/B46 --command connect --neigh mudebs://localhost:3051/B51
echo sleep 1
sleep 1
echo "Connecting broker B53 to broker B55"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3053/B53 --command connect --neigh mudebs://localhost:3055/B55
echo sleep 1
sleep 1
echo "Connecting broker B55 to broker B58"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3055/B55 --command connect --neigh mudebs://localhost:3058/B58
echo sleep 1
sleep 1
echo "Connecting broker B58 to broker B56"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3058/B58 --command connect --neigh mudebs://localhost:3056/B56
echo sleep 1
sleep 1
echo "Connecting broker B56 to broker B57"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3056/B56 --command connect --neigh mudebs://localhost:3057/B57
echo sleep 1
sleep 1
echo "Connecting broker B57 to broker B54"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3057/B57 --command connect --neigh mudebs://localhost:3054/B54
echo sleep 1
sleep 1
echo "Connecting broker B54 to broker B60"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3054/B54 --command connect --neigh mudebs://localhost:3060/B60
echo sleep 1
sleep 1
echo "Connecting broker B60 to broker B52"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3060/B60 --command connect --neigh mudebs://localhost:3052/B52
echo sleep 1
sleep 1
echo "Connecting broker B52 to broker B59"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3052/B52 --command connect --neigh mudebs://localhost:3059/B59
echo sleep 1
sleep 1
echo "Connecting broker B59 to broker B53"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3059/B59 --command connect --neigh mudebs://localhost:3053/B53
echo sleep 1
sleep 1
echo "Connecting broker B63 to broker B61"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3063/B63 --command connect --neigh mudebs://localhost:3061/B61
echo sleep 1
sleep 1
echo "Connecting broker B61 to broker B67"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3061/B61 --command connect --neigh mudebs://localhost:3067/B67
echo sleep 1
sleep 1
echo "Connecting broker B67 to broker B65"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3067/B67 --command connect --neigh mudebs://localhost:3065/B65
echo sleep 1
sleep 1
echo "Connecting broker B65 to broker B62"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3065/B65 --command connect --neigh mudebs://localhost:3062/B62
echo sleep 1
sleep 1
echo "Connecting broker B62 to broker B64"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3062/B62 --command connect --neigh mudebs://localhost:3064/B64
echo sleep 1
sleep 1
echo "Connecting broker B64 to broker B68"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3064/B64 --command connect --neigh mudebs://localhost:3068/B68
echo sleep 1
sleep 1
echo "Connecting broker B68 to broker B66"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3068/B68 --command connect --neigh mudebs://localhost:3066/B66
echo sleep 1
sleep 1
echo "Connecting broker B66 to broker B63"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3066/B66 --command connect --neigh mudebs://localhost:3063/B63
echo sleep 1
sleep 1
echo "Connecting broker B72 to broker B75"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3072/B72 --command connect --neigh mudebs://localhost:3075/B75
echo sleep 1
sleep 1
echo "Connecting broker B75 to broker B74"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3075/B75 --command connect --neigh mudebs://localhost:3074/B74
echo sleep 1
sleep 1
echo "Connecting broker B74 to broker B69"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3074/B74 --command connect --neigh mudebs://localhost:3069/B69
echo sleep 1
sleep 1
echo "Connecting broker B69 to broker B70"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3069/B69 --command connect --neigh mudebs://localhost:3070/B70
echo sleep 1
sleep 1
echo "Connecting broker B70 to broker B73"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3070/B70 --command connect --neigh mudebs://localhost:3073/B73
echo sleep 1
sleep 1
echo "Connecting broker B73 to broker B71"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3073/B73 --command connect --neigh mudebs://localhost:3071/B71
echo sleep 1
sleep 1
echo "Connecting broker B71 to broker B72"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3071/B71 --command connect --neigh mudebs://localhost:3072/B72
echo sleep 1
sleep 1
echo "Connecting broker B81 to broker B80"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3081/B81 --command connect --neigh mudebs://localhost:3080/B80
echo sleep 1
sleep 1
echo "Connecting broker B80 to broker B82"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3080/B80 --command connect --neigh mudebs://localhost:3082/B82
echo sleep 1
sleep 1
echo "Connecting broker B82 to broker B76"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3082/B82 --command connect --neigh mudebs://localhost:3076/B76
echo sleep 1
sleep 1
echo "Connecting broker B76 to broker B79"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3076/B76 --command connect --neigh mudebs://localhost:3079/B79
echo sleep 1
sleep 1
echo "Connecting broker B79 to broker B78"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3079/B79 --command connect --neigh mudebs://localhost:3078/B78
echo sleep 1
sleep 1
echo "Connecting broker B78 to broker B77"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3078/B78 --command connect --neigh mudebs://localhost:3077/B77
echo sleep 1
sleep 1
echo "Connecting broker B77 to broker B81"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3077/B77 --command connect --neigh mudebs://localhost:3081/B81
echo sleep 1
sleep 1
echo "Connecting broker B86 to broker B84"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3086/B86 --command connect --neigh mudebs://localhost:3084/B84
echo sleep 1
sleep 1
echo "Connecting broker B84 to broker B85"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3084/B84 --command connect --neigh mudebs://localhost:3085/B85
echo sleep 1
sleep 1
echo "Connecting broker B85 to broker B89"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3085/B85 --command connect --neigh mudebs://localhost:3089/B89
echo sleep 1
sleep 1
echo "Connecting broker B89 to broker B91"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3089/B89 --command connect --neigh mudebs://localhost:3091/B91
echo sleep 1
sleep 1
echo "Connecting broker B91 to broker B87"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3091/B91 --command connect --neigh mudebs://localhost:3087/B87
echo sleep 1
sleep 1
echo "Connecting broker B87 to broker B90"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3087/B87 --command connect --neigh mudebs://localhost:3090/B90
echo sleep 1
sleep 1
echo "Connecting broker B90 to broker B88"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3090/B90 --command connect --neigh mudebs://localhost:3088/B88
echo sleep 1
sleep 1
echo "Connecting broker B88 to broker B92"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3088/B88 --command connect --neigh mudebs://localhost:3092/B92
echo sleep 1
sleep 1
echo "Connecting broker B92 to broker B83"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3092/B92 --command connect --neigh mudebs://localhost:3083/B83
echo sleep 1
sleep 1
echo "Connecting broker B83 to broker B86"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3083/B83 --command connect --neigh mudebs://localhost:3086/B86
echo sleep 1
sleep 1
echo "Connecting broker B95 to broker B94"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3095/B95 --command connect --neigh mudebs://localhost:3094/B94
echo sleep 1
sleep 1
echo "Connecting broker B94 to broker B93"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3094/B94 --command connect --neigh mudebs://localhost:3093/B93
echo sleep 1
sleep 1
echo "Connecting broker B93 to broker B99"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3093/B93 --command connect --neigh mudebs://localhost:3099/B99
echo sleep 1
sleep 1
echo "Connecting broker B99 to broker B97"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3099/B99 --command connect --neigh mudebs://localhost:3097/B97
echo sleep 1
sleep 1
echo "Connecting broker B97 to broker B96"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3097/B97 --command connect --neigh mudebs://localhost:3096/B96
echo sleep 1
sleep 1
echo "Connecting broker B96 to broker B98"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3096/B96 --command connect --neigh mudebs://localhost:3098/B98
echo sleep 1
sleep 1
echo "Connecting broker B98 to broker B95"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3098/B98 --command connect --neigh mudebs://localhost:3095/B95
echo sleep 20
sleep 20


echo
echo "On B0, join scope (s1,top)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3000/B0 --command joinscope --dimension s --subscope s1 --superscope top --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B1, join scope (s2,s1)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3001/B1 --command joinscope --dimension s --subscope s2 --superscope s1 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B23, join scope (s4,s2)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3023/B23 --command joinscope --dimension s --subscope s4 --superscope s2 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B25, join scope (s8,s4)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3025/B25 --command joinscope --dimension s --subscope s8 --superscope s4 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B26, join scope (s9,s4)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3026/B26 --command joinscope --dimension s --subscope s9 --superscope s4 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B28, join scope (s5,s2)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3028/B28 --command joinscope --dimension s --subscope s5 --superscope s2 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B34, join scope (s10,s5)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3034/B34 --command joinscope --dimension s --subscope s10 --superscope s5 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B36, join scope (s11,s5)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3036/B36 --command joinscope --dimension s --subscope s11 --superscope s5 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B3, join scope (s3,s1)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3003/B3 --command joinscope --dimension s --subscope s3 --superscope s1 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B95, join scope (s6,s3)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3095/B95 --command joinscope --dimension s --subscope s6 --superscope s3 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B94, join scope (s12,s6)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3094/B94 --command joinscope --dimension s --subscope s12 --superscope s6 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B98, join scope (s13,s6)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3098/B98 --command joinscope --dimension s --subscope s13 --superscope s6 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B81, join scope (s7,s3)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3081/B81 --command joinscope --dimension s --subscope s7 --superscope s3 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B80, join scope (s14,s7)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3080/B80 --command joinscope --dimension s --subscope s14 --superscope s7 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B77, join scope (s15,s7)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3077/B77 --command joinscope --dimension s --subscope s15 --superscope s7 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B1, join scope (r1,top)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3001/B1 --command joinscope --dimension r --subscope r1 --superscope top --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B0, join scope (r2,r1)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3000/B0 --command joinscope --dimension r --subscope r2 --superscope r1 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B51, join scope (r4,r2)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3051/B51 --command joinscope --dimension r --subscope r4 --superscope r2 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B46, join scope (r8,r4)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3046/B46 --command joinscope --dimension r --subscope r8 --superscope r4 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B49, join scope (r9,r4)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3049/B49 --command joinscope --dimension r --subscope r9 --superscope r4 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B19, join scope (r5,r2)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3019/B19 --command joinscope --dimension r --subscope r5 --superscope r2 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B13, join scope (r10,r5)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3013/B13 --command joinscope --dimension r --subscope r10 --superscope r5 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B18, join scope (r11,r5)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3018/B18 --command joinscope --dimension r --subscope r11 --superscope r5 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B2, join scope (r3,r1)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3002/B2 --command joinscope --dimension r --subscope r3 --superscope r1 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B63, join scope (r6,r3)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3063/B63 --command joinscope --dimension r --subscope r6 --superscope r3 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B61, join scope (r12,r6)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3061/B61 --command joinscope --dimension r --subscope r12 --superscope r6 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B66, join scope (r13,r6)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3066/B66 --command joinscope --dimension r --subscope r13 --superscope r6 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B72, join scope (r7,r3)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3072/B72 --command joinscope --dimension r --subscope r7 --superscope r3 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B71, join scope (r14,r7)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3071/B71 --command joinscope --dimension r --subscope r14 --superscope r7 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B75, join scope (r15,r7)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3075/B75 --command joinscope --dimension r --subscope r15 --superscope r7 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B2, join scope (t1,top)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3002/B2 --command joinscope --dimension t --subscope t1 --superscope top --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B1, join scope (t2,t1)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3001/B1 --command joinscope --dimension t --subscope t2 --superscope t1 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B45, join scope (t4,t2)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3045/B45 --command joinscope --dimension t --subscope t4 --superscope t2 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B44, join scope (t8,t4)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3044/B44 --command joinscope --dimension t --subscope t8 --superscope t4 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B41, join scope (t9,t4)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3041/B41 --command joinscope --dimension t --subscope t9 --superscope t4 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B28, join scope (t5,t2)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3028/B28 --command joinscope --dimension t --subscope t5 --superscope t2 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B34, join scope (t10,t5)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3034/B34 --command joinscope --dimension t --subscope t10 --superscope t5 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B36, join scope (t11,t5)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3036/B36 --command joinscope --dimension t --subscope t11 --superscope t5 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B3, join scope (t3,t1)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3003/B3 --command joinscope --dimension t --subscope t3 --superscope t1 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B81, join scope (t6,t3)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3081/B81 --command joinscope --dimension t --subscope t6 --superscope t3 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B80, join scope (t12,t6)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3080/B80 --command joinscope --dimension t --subscope t12 --superscope t6 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B77, join scope (t13,t6)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3077/B77 --command joinscope --dimension t --subscope t13 --superscope t6 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B95, join scope (t7,t3)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3095/B95 --command joinscope --dimension t --subscope t7 --superscope t3 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B94, join scope (t14,t7)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3094/B94 --command joinscope --dimension t --subscope t14 --superscope t7 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B98, join scope (t15,t7)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3098/B98 --command joinscope --dimension t --subscope t15 --superscope t7 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20


echo "Please wait..."
echo "Start publishing"
echo C0 advertising filter \'startperfsubscribing\'
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2000/C0 --command advertise --id 'startperfsubscribing' --file ./ressources/advertisement.mudebs 
sleep 1
echo C0 publishing subscribing command with advertising filter \'startperfsubscribing\'
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2000/C0 --command publish --id 'startperfsubscribing' --content '<command><startsubscribing>w100w-C0-</startsubscribing></command>'
sleep 1
echo

echo
echo "Hit return when you want clients to unsubscribe to all filters"
read x
echo "Client C0 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2000/C0 --command unsubscribe
echo "Client C0 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2000/C0 --command unadvertise
echo sleep 30
sleep 30
echo "Client C1 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2001/C1 --command unsubscribe
echo "Client C1 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2001/C1 --command unadvertise
echo sleep 30
sleep 30
echo "Client C2 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2002/C2 --command unsubscribe
echo "Client C2 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2002/C2 --command unadvertise
echo sleep 30
sleep 30
echo "Client C3 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2003/C3 --command unsubscribe
echo "Client C3 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2003/C3 --command unadvertise
echo sleep 30
sleep 30
echo "Client C4 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2004/C4 --command unsubscribe
echo "Client C4 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2004/C4 --command unadvertise
echo sleep 30
sleep 30
echo "Client C5 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2005/C5 --command unsubscribe
echo "Client C5 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2005/C5 --command unadvertise
echo sleep 30
sleep 30
echo "Client C6 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2006/C6 --command unsubscribe
echo "Client C6 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2006/C6 --command unadvertise
echo sleep 30
sleep 30
echo "Client C7 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2007/C7 --command unsubscribe
echo "Client C7 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2007/C7 --command unadvertise
echo sleep 30
sleep 30
echo "Client C8 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2008/C8 --command unsubscribe
echo "Client C8 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2008/C8 --command unadvertise
echo sleep 30
sleep 30
echo "Client C9 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2009/C9 --command unsubscribe
echo "Client C9 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2009/C9 --command unadvertise
echo sleep 30
sleep 30
echo "Client C10 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2010/C10 --command unsubscribe
echo "Client C10 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2010/C10 --command unadvertise
echo sleep 30
sleep 30
echo "Client C11 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2011/C11 --command unsubscribe
echo "Client C11 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2011/C11 --command unadvertise
echo sleep 30
sleep 30
echo "Client C12 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2012/C12 --command unsubscribe
echo "Client C12 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2012/C12 --command unadvertise
echo sleep 30
sleep 30
echo "Client C13 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2013/C13 --command unsubscribe
echo "Client C13 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2013/C13 --command unadvertise
echo sleep 30
sleep 30
echo "Client C14 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2014/C14 --command unsubscribe
echo "Client C14 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2014/C14 --command unadvertise
echo sleep 30
sleep 30
echo "Client C15 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2015/C15 --command unsubscribe
echo "Client C15 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2015/C15 --command unadvertise
echo sleep 30
sleep 30
echo "Client C16 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2016/C16 --command unsubscribe
echo "Client C16 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2016/C16 --command unadvertise
echo sleep 30
sleep 30
echo "Client C17 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2017/C17 --command unsubscribe
echo "Client C17 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2017/C17 --command unadvertise
echo sleep 30
sleep 30
echo "Client C18 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2018/C18 --command unsubscribe
echo "Client C18 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2018/C18 --command unadvertise
echo sleep 30
sleep 30
echo "Client C19 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2019/C19 --command unsubscribe
echo "Client C19 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2019/C19 --command unadvertise
echo sleep 30
sleep 30
echo "Client C20 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2020/C20 --command unsubscribe
echo "Client C20 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2020/C20 --command unadvertise
echo sleep 30
sleep 30
echo "Client C21 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2021/C21 --command unsubscribe
echo "Client C21 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2021/C21 --command unadvertise
echo sleep 30
sleep 30
echo "Client C22 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2022/C22 --command unsubscribe
echo "Client C22 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2022/C22 --command unadvertise
echo sleep 30
sleep 30
echo "Client C23 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2023/C23 --command unsubscribe
echo "Client C23 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2023/C23 --command unadvertise
echo sleep 30
sleep 30
echo "Client C24 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2024/C24 --command unsubscribe
echo "Client C24 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2024/C24 --command unadvertise
echo sleep 30
sleep 30
echo "Client C25 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2025/C25 --command unsubscribe
echo "Client C25 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2025/C25 --command unadvertise
echo sleep 30
sleep 30
echo "Client C26 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2026/C26 --command unsubscribe
echo "Client C26 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2026/C26 --command unadvertise
echo sleep 30
sleep 30
echo "Client C27 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2027/C27 --command unsubscribe
echo "Client C27 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2027/C27 --command unadvertise
echo sleep 30
sleep 30
echo "Client C28 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2028/C28 --command unsubscribe
echo "Client C28 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2028/C28 --command unadvertise
echo sleep 30
sleep 30
echo "Client C29 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2029/C29 --command unsubscribe
echo "Client C29 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2029/C29 --command unadvertise
echo sleep 30
sleep 30
echo "Client C30 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2030/C30 --command unsubscribe
echo "Client C30 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2030/C30 --command unadvertise
echo sleep 30
sleep 30
echo "Client C31 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2031/C31 --command unsubscribe
echo "Client C31 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2031/C31 --command unadvertise
echo sleep 30
sleep 30
echo "Client C32 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2032/C32 --command unsubscribe
echo "Client C32 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2032/C32 --command unadvertise
echo sleep 30
sleep 30
echo "Client C33 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2033/C33 --command unsubscribe
echo "Client C33 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2033/C33 --command unadvertise
echo sleep 30
sleep 30
echo "Client C34 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2034/C34 --command unsubscribe
echo "Client C34 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2034/C34 --command unadvertise
echo sleep 30
sleep 30
echo "Client C35 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2035/C35 --command unsubscribe
echo "Client C35 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2035/C35 --command unadvertise
echo sleep 30
sleep 30
echo "Client C36 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2036/C36 --command unsubscribe
echo "Client C36 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2036/C36 --command unadvertise
echo sleep 30
sleep 30
echo "Client C37 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2037/C37 --command unsubscribe
echo "Client C37 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2037/C37 --command unadvertise
echo sleep 30
sleep 30
echo "Client C38 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2038/C38 --command unsubscribe
echo "Client C38 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2038/C38 --command unadvertise
echo sleep 30
sleep 30
echo "Client C39 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2039/C39 --command unsubscribe
echo "Client C39 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2039/C39 --command unadvertise
echo sleep 30
sleep 30
echo "Client C40 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2040/C40 --command unsubscribe
echo "Client C40 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2040/C40 --command unadvertise
echo sleep 30
sleep 30
echo "Client C41 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2041/C41 --command unsubscribe
echo "Client C41 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2041/C41 --command unadvertise
echo sleep 30
sleep 30
echo "Client C42 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2042/C42 --command unsubscribe
echo "Client C42 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2042/C42 --command unadvertise
echo sleep 30
sleep 30
echo "Client C43 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2043/C43 --command unsubscribe
echo "Client C43 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2043/C43 --command unadvertise
echo sleep 30
sleep 30
echo "Client C44 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2044/C44 --command unsubscribe
echo "Client C44 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2044/C44 --command unadvertise
echo sleep 30
sleep 30
echo "Client C45 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2045/C45 --command unsubscribe
echo "Client C45 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2045/C45 --command unadvertise
echo sleep 30
sleep 30
echo "Client C46 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2046/C46 --command unsubscribe
echo "Client C46 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2046/C46 --command unadvertise
echo sleep 30
sleep 30
echo "Client C47 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2047/C47 --command unsubscribe
echo "Client C47 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2047/C47 --command unadvertise
echo sleep 30
sleep 30
echo "Client C48 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2048/C48 --command unsubscribe
echo "Client C48 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2048/C48 --command unadvertise
echo sleep 30
sleep 30
echo "Client C49 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2049/C49 --command unsubscribe
echo "Client C49 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2049/C49 --command unadvertise
echo sleep 30
sleep 30
echo "Client C50 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2050/C50 --command unsubscribe
echo "Client C50 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2050/C50 --command unadvertise
echo sleep 30
sleep 30
echo "Client C51 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2051/C51 --command unsubscribe
echo "Client C51 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2051/C51 --command unadvertise
echo sleep 30
sleep 30
echo "Client C52 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2052/C52 --command unsubscribe
echo "Client C52 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2052/C52 --command unadvertise
echo sleep 30
sleep 30
echo "Client C53 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2053/C53 --command unsubscribe
echo "Client C53 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2053/C53 --command unadvertise
echo sleep 30
sleep 30
echo "Client C54 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2054/C54 --command unsubscribe
echo "Client C54 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2054/C54 --command unadvertise
echo sleep 30
sleep 30
echo "Client C55 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2055/C55 --command unsubscribe
echo "Client C55 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2055/C55 --command unadvertise
echo sleep 30
sleep 30
echo "Client C56 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2056/C56 --command unsubscribe
echo "Client C56 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2056/C56 --command unadvertise
echo sleep 30
sleep 30
echo "Client C57 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2057/C57 --command unsubscribe
echo "Client C57 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2057/C57 --command unadvertise
echo sleep 30
sleep 30
echo "Client C58 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2058/C58 --command unsubscribe
echo "Client C58 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2058/C58 --command unadvertise
echo sleep 30
sleep 30
echo "Client C59 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2059/C59 --command unsubscribe
echo "Client C59 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2059/C59 --command unadvertise
echo sleep 30
sleep 30
echo "Client C60 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2060/C60 --command unsubscribe
echo "Client C60 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2060/C60 --command unadvertise
echo sleep 30
sleep 30
echo "Client C61 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2061/C61 --command unsubscribe
echo "Client C61 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2061/C61 --command unadvertise
echo sleep 30
sleep 30
echo "Client C62 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2062/C62 --command unsubscribe
echo "Client C62 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2062/C62 --command unadvertise
echo sleep 30
sleep 30
echo "Client C63 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2063/C63 --command unsubscribe
echo "Client C63 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2063/C63 --command unadvertise
echo sleep 30
sleep 30
echo "Client C64 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2064/C64 --command unsubscribe
echo "Client C64 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2064/C64 --command unadvertise
echo sleep 30
sleep 30
echo "Client C65 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2065/C65 --command unsubscribe
echo "Client C65 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2065/C65 --command unadvertise
echo sleep 30
sleep 30
echo "Client C66 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2066/C66 --command unsubscribe
echo "Client C66 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2066/C66 --command unadvertise
echo sleep 30
sleep 30
echo "Client C67 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2067/C67 --command unsubscribe
echo "Client C67 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2067/C67 --command unadvertise
echo sleep 30
sleep 30
echo "Client C68 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2068/C68 --command unsubscribe
echo "Client C68 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2068/C68 --command unadvertise
echo sleep 30
sleep 30
echo "Client C69 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2069/C69 --command unsubscribe
echo "Client C69 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2069/C69 --command unadvertise
echo sleep 30
sleep 30
echo "Client C70 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2070/C70 --command unsubscribe
echo "Client C70 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2070/C70 --command unadvertise
echo sleep 30
sleep 30
echo "Client C71 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2071/C71 --command unsubscribe
echo "Client C71 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2071/C71 --command unadvertise
echo sleep 30
sleep 30
echo "Client C72 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2072/C72 --command unsubscribe
echo "Client C72 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2072/C72 --command unadvertise
echo sleep 30
sleep 30
echo "Client C73 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2073/C73 --command unsubscribe
echo "Client C73 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2073/C73 --command unadvertise
echo sleep 30
sleep 30
echo "Client C74 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2074/C74 --command unsubscribe
echo "Client C74 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2074/C74 --command unadvertise
echo sleep 30
sleep 30
echo "Client C75 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2075/C75 --command unsubscribe
echo "Client C75 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2075/C75 --command unadvertise
echo sleep 30
sleep 30
echo "Client C76 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2076/C76 --command unsubscribe
echo "Client C76 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2076/C76 --command unadvertise
echo sleep 30
sleep 30
echo "Client C77 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2077/C77 --command unsubscribe
echo "Client C77 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2077/C77 --command unadvertise
echo sleep 30
sleep 30
echo "Client C78 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2078/C78 --command unsubscribe
echo "Client C78 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2078/C78 --command unadvertise
echo sleep 30
sleep 30
echo "Client C79 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2079/C79 --command unsubscribe
echo "Client C79 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2079/C79 --command unadvertise
echo sleep 30
sleep 30
echo "Client C80 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2080/C80 --command unsubscribe
echo "Client C80 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2080/C80 --command unadvertise
echo sleep 30
sleep 30
echo "Client C81 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2081/C81 --command unsubscribe
echo "Client C81 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2081/C81 --command unadvertise
echo sleep 30
sleep 30
echo "Client C82 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2082/C82 --command unsubscribe
echo "Client C82 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2082/C82 --command unadvertise
echo sleep 30
sleep 30
echo "Client C83 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2083/C83 --command unsubscribe
echo "Client C83 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2083/C83 --command unadvertise
echo sleep 30
sleep 30
echo "Client C84 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2084/C84 --command unsubscribe
echo "Client C84 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2084/C84 --command unadvertise
echo sleep 30
sleep 30
echo "Client C85 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2085/C85 --command unsubscribe
echo "Client C85 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2085/C85 --command unadvertise
echo sleep 30
sleep 30
echo "Client C86 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2086/C86 --command unsubscribe
echo "Client C86 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2086/C86 --command unadvertise
echo sleep 30
sleep 30
echo "Client C87 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2087/C87 --command unsubscribe
echo "Client C87 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2087/C87 --command unadvertise
echo sleep 30
sleep 30
echo "Client C88 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2088/C88 --command unsubscribe
echo "Client C88 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2088/C88 --command unadvertise
echo sleep 30
sleep 30
echo "Client C89 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2089/C89 --command unsubscribe
echo "Client C89 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2089/C89 --command unadvertise
echo sleep 30
sleep 30
echo "Client C90 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2090/C90 --command unsubscribe
echo "Client C90 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2090/C90 --command unadvertise
echo sleep 30
sleep 30
echo "Client C91 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2091/C91 --command unsubscribe
echo "Client C91 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2091/C91 --command unadvertise
echo sleep 30
sleep 30
echo "Client C92 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2092/C92 --command unsubscribe
echo "Client C92 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2092/C92 --command unadvertise
echo sleep 30
sleep 30
echo "Client C93 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2093/C93 --command unsubscribe
echo "Client C93 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2093/C93 --command unadvertise
echo sleep 30
sleep 30
echo "Client C94 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2094/C94 --command unsubscribe
echo "Client C94 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2094/C94 --command unadvertise
echo sleep 30
sleep 30
echo "Client C95 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2095/C95 --command unsubscribe
echo "Client C95 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2095/C95 --command unadvertise
echo sleep 30
sleep 30
echo "Client C96 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2096/C96 --command unsubscribe
echo "Client C96 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2096/C96 --command unadvertise
echo sleep 30
sleep 30
echo "Client C97 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2097/C97 --command unsubscribe
echo "Client C97 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2097/C97 --command unadvertise
echo sleep 30
sleep 30
echo "Client C98 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2098/C98 --command unsubscribe
echo "Client C98 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2098/C98 --command unadvertise
echo sleep 30
sleep 30
echo "Client C99 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2099/C99 --command unsubscribe
echo "Client C99 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2099/C99 --command unadvertise
echo sleep 30
sleep 30


echo
echo "Hit return when you want to set logger overlay to debug for all brokers"
read x
echo "Setting logger overlay to debug for B0"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3000/B0 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B1"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3001/B1 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B2"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3002/B2 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B3"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3003/B3 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B4"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3004/B4 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B5"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3005/B5 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B6"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3006/B6 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B7"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3007/B7 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B8"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3008/B8 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B9"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3009/B9 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B10"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3010/B10 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B11"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3011/B11 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B12"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3012/B12 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B13"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3013/B13 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B14"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3014/B14 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B15"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3015/B15 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B16"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3016/B16 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B17"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3017/B17 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B18"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3018/B18 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B19"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3019/B19 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B20"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3020/B20 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B21"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3021/B21 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B22"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3022/B22 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B23"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3023/B23 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B24"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3024/B24 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B25"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3025/B25 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B26"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3026/B26 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B27"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3027/B27 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B28"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3028/B28 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B29"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3029/B29 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B30"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3030/B30 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B31"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3031/B31 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B32"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3032/B32 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B33"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3033/B33 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B34"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3034/B34 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B35"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3035/B35 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B36"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3036/B36 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B37"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3037/B37 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B38"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3038/B38 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B39"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3039/B39 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B40"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3040/B40 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B41"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3041/B41 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B42"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3042/B42 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B43"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3043/B43 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B44"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3044/B44 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B45"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3045/B45 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B46"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3046/B46 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B47"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3047/B47 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B48"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3048/B48 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B49"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3049/B49 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B50"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3050/B50 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B51"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3051/B51 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B52"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3052/B52 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B53"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3053/B53 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B54"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3054/B54 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B55"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3055/B55 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B56"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3056/B56 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B57"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3057/B57 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B58"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3058/B58 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B59"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3059/B59 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B60"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3060/B60 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B61"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3061/B61 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B62"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3062/B62 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B63"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3063/B63 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B64"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3064/B64 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B65"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3065/B65 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B66"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3066/B66 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B67"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3067/B67 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B68"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3068/B68 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B69"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3069/B69 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B70"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3070/B70 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B71"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3071/B71 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B72"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3072/B72 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B73"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3073/B73 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B74"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3074/B74 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B75"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3075/B75 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B76"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3076/B76 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B77"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3077/B77 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B78"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3078/B78 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B79"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3079/B79 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B80"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3080/B80 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B81"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3081/B81 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B82"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3082/B82 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B83"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3083/B83 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B84"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3084/B84 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B85"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3085/B85 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B86"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3086/B86 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B87"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3087/B87 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B88"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3088/B88 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B89"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3089/B89 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B90"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3090/B90 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B91"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3091/B91 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B92"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3092/B92 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B93"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3093/B93 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B94"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3094/B94 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B95"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3095/B95 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B96"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3096/B96 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B97"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3097/B97 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B98"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3098/B98 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B99"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3099/B99 --command setlog --name overlay --level debug
echo sleep 5
sleep 5


echo
echo "Hit return when you want to send termination detection to broker B0"
read x
echo On B0, termination detection
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3000/B0 --command term_detection
sleep 5


echo
echo "Hit return when you want to send terminate to broker B0"
read x
echo On B0, terminate
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3000/B0 --command terminate_all