#!/bin/bash

MUDEBS_HOME_SCRIPTS="$(cd ../../../../muDEBS/scripts/ && pwd)"

CONFIG_FILE=./ressources/performancemultiscoping_52nodes.config

HOSTNAME=localhost
STARTING_PORT_BROKER=3000
STARTING_PORT_CLIENT=2000
STARTING_BROKER=0
ENDING_BROKER=51
MAPDOWN_FILTER=./ressources/mapdownfilter.mudebs
MAPUP_FILTER=./ressources/mapupfilter.mudebs
ADV_FILTER=./ressources/advertisement.mudebs
SUB_FILTER=./ressources/subscription.mudebs


# dimensions
DS=DS
DT=DT
DR=DR

# scopes of dimension R
R1=R1
R2=R2
R3=R3
R4=R4
R5=R5
R6=R6
R7=R7
R8=R8
R9=R9
R10=R10
R11=R11
R12=R12
R13=R13
R14=R14

# scopes of dimension S
S1=S1
S2=S2
S3=S3
S4=S4
S5=S5
S6=S6
S7=S7
S8=S8
S9=S9
S10=S10
S11=S11
S12=S12
S13=S13
S14=S14

# scopes of dimension T
T1=T1
T2=T2
T3=T3
T4=T4
T5=T5
T6=T6
T7=T7
T8=T8
T9=T9
T10=T10
T11=T11
T12=T12
T13=T13
T14=T14

#generated identifiers and uri
#------------------------------------------------------------------------------
#identifiers of brokers
B0=B0
B1=B1
B2=B2
B3=B3
B4=B4
B5=B5
B6=B6
B7=B7
B8=B8
B9=B9
B10=B10
B11=B11
B12=B12
B13=B13
B14=B14
B15=B15
B16=B16
B17=B17
B18=B18
B19=B19
B20=B20
B21=B21
B22=B22
B23=B23
B24=B24
B25=B25
B26=B26
B27=B27
B28=B28
B29=B29
B30=B30
B31=B31
B32=B32
B33=B33
B34=B34
B35=B35
B36=B36
B37=B37
B38=B38
B39=B39
B40=B40
B41=B41
B42=B42
B43=B43
B44=B44
B45=B45
B46=B46
B47=B47
B48=B48
B49=B49
B50=B50
B51=B51

#uri of brokers
B0_URI=mudebs://localhost:3000/B0
B1_URI=mudebs://localhost:3001/B1
B2_URI=mudebs://localhost:3002/B2
B3_URI=mudebs://localhost:3003/B3
B4_URI=mudebs://localhost:3004/B4
B5_URI=mudebs://localhost:3005/B5
B6_URI=mudebs://localhost:3006/B6
B7_URI=mudebs://localhost:3007/B7
B8_URI=mudebs://localhost:3008/B8
B9_URI=mudebs://localhost:3009/B9
B10_URI=mudebs://localhost:3010/B10
B11_URI=mudebs://localhost:3011/B11
B12_URI=mudebs://localhost:3012/B12
B13_URI=mudebs://localhost:3013/B13
B14_URI=mudebs://localhost:3014/B14
B15_URI=mudebs://localhost:3015/B15
B16_URI=mudebs://localhost:3016/B16
B17_URI=mudebs://localhost:3017/B17
B18_URI=mudebs://localhost:3018/B18
B19_URI=mudebs://localhost:3019/B19
B20_URI=mudebs://localhost:3020/B20
B21_URI=mudebs://localhost:3021/B21
B22_URI=mudebs://localhost:3022/B22
B23_URI=mudebs://localhost:3023/B23
B24_URI=mudebs://localhost:3024/B24
B25_URI=mudebs://localhost:3025/B25
B26_URI=mudebs://localhost:3026/B26
B27_URI=mudebs://localhost:3027/B27
B28_URI=mudebs://localhost:3028/B28
B29_URI=mudebs://localhost:3029/B29
B30_URI=mudebs://localhost:3030/B30
B31_URI=mudebs://localhost:3031/B31
B32_URI=mudebs://localhost:3032/B32
B33_URI=mudebs://localhost:3033/B33
B34_URI=mudebs://localhost:3034/B34
B35_URI=mudebs://localhost:3035/B35
B36_URI=mudebs://localhost:3036/B36
B37_URI=mudebs://localhost:3037/B37
B38_URI=mudebs://localhost:3038/B38
B39_URI=mudebs://localhost:3039/B39
B40_URI=mudebs://localhost:3040/B40
B41_URI=mudebs://localhost:3041/B41
B42_URI=mudebs://localhost:3042/B42
B43_URI=mudebs://localhost:3043/B43
B44_URI=mudebs://localhost:3044/B44
B45_URI=mudebs://localhost:3045/B45
B46_URI=mudebs://localhost:3046/B46
B47_URI=mudebs://localhost:3047/B47
B48_URI=mudebs://localhost:3048/B48
B49_URI=mudebs://localhost:3049/B49
B50_URI=mudebs://localhost:3050/B50
B51_URI=mudebs://localhost:3051/B51

#identifiers of clients
C0=C0
C1=C1
C2=C2
C3=C3
C4=C4
C5=C5
C6=C6
C7=C7
C8=C8
C9=C9
C10=C10
C11=C11
C12=C12
C13=C13
C14=C14
C15=C15
C16=C16
C17=C17
C18=C18
C19=C19
C20=C20
C21=C21
C22=C22
C23=C23
C24=C24
C25=C25
C26=C26
C27=C27
C28=C28
C29=C29
C30=C30
C31=C31
C32=C32
C33=C33
C34=C34
C35=C35
C36=C36
C37=C37
C38=C38
C39=C39
C40=C40
C41=C41
C42=C42
C43=C43
C44=C44
C45=C45
C46=C46
C47=C47
C48=C48
C49=C49
C50=C50
C51=C51

#uri of clients
C0_URI=mudebs://localhost:2000/C0
C1_URI=mudebs://localhost:2001/C1
C2_URI=mudebs://localhost:2002/C2
C3_URI=mudebs://localhost:2003/C3
C4_URI=mudebs://localhost:2004/C4
C5_URI=mudebs://localhost:2005/C5
C6_URI=mudebs://localhost:2006/C6
C7_URI=mudebs://localhost:2007/C7
C8_URI=mudebs://localhost:2008/C8
C9_URI=mudebs://localhost:2009/C9
C10_URI=mudebs://localhost:2010/C10
C11_URI=mudebs://localhost:2011/C11
C12_URI=mudebs://localhost:2012/C12
C13_URI=mudebs://localhost:2013/C13
C14_URI=mudebs://localhost:2014/C14
C15_URI=mudebs://localhost:2015/C15
C16_URI=mudebs://localhost:2016/C16
C17_URI=mudebs://localhost:2017/C17
C18_URI=mudebs://localhost:2018/C18
C19_URI=mudebs://localhost:2019/C19
C20_URI=mudebs://localhost:2020/C20
C21_URI=mudebs://localhost:2021/C21
C22_URI=mudebs://localhost:2022/C22
C23_URI=mudebs://localhost:2023/C23
C24_URI=mudebs://localhost:2024/C24
C25_URI=mudebs://localhost:2025/C25
C26_URI=mudebs://localhost:2026/C26
C27_URI=mudebs://localhost:2027/C27
C28_URI=mudebs://localhost:2028/C28
C29_URI=mudebs://localhost:2029/C29
C30_URI=mudebs://localhost:2030/C30
C31_URI=mudebs://localhost:2031/C31
C32_URI=mudebs://localhost:2032/C32
C33_URI=mudebs://localhost:2033/C33
C34_URI=mudebs://localhost:2034/C34
C35_URI=mudebs://localhost:2035/C35
C36_URI=mudebs://localhost:2036/C36
C37_URI=mudebs://localhost:2037/C37
C38_URI=mudebs://localhost:2038/C38
C39_URI=mudebs://localhost:2039/C39
C40_URI=mudebs://localhost:2040/C40
C41_URI=mudebs://localhost:2041/C41
C42_URI=mudebs://localhost:2042/C42
C43_URI=mudebs://localhost:2043/C43
C44_URI=mudebs://localhost:2044/C44
C45_URI=mudebs://localhost:2045/C45
C46_URI=mudebs://localhost:2046/C46
C47_URI=mudebs://localhost:2047/C47
C48_URI=mudebs://localhost:2048/C48
C49_URI=mudebs://localhost:2049/C49
C50_URI=mudebs://localhost:2050/C50
C51_URI=mudebs://localhost:2051/C51

#------------------------------------------------------------------------------


WHITE="\033[0m"
RED="\033[31m"
BLUE="\033[34m"
PINK="\033[35m"
CYAN="\033[36m"
YELLOW="\033[33m"
GREEN="\033[42m"

colorbroker(){
  broker=$1
  echo -e $CYAN""$broker$WHITE
}

colorclient(){
  client=$1
  echo -e $YELLOW""$client$WHITE
}

colordimension(){
  dimension=$1
  echo -e $RED""$dimension$WHITE
}

colorscope(){
  scope=$1
  echo -e $PINK""$scope$WHITE
}

colorfilter(){
  filter=$1
  echo -e $GREEN""$filter$WHITE
}

generateidbroker(){
  for (( i = $1; i <= $2; i++ ))
  do
    echo "B$i=B$i"
  done
}

generateuribroker(){
  for (( i = $1; i <= $2; i++ ))
  do
    echo 'B'$i'_URI=mudebs://'$3':'`expr $STARTING_PORT_BROKER + $i`'/B'$i
  done
}

generateidclient(){
  for (( i = $1; i <= $2; i++ ))
  do
    echo "C$i=C$i"
  done
}

generateuriclient(){
  for (( i = $1; i <= $2; i++ ))
  do
    echo 'C'$i'_URI=mudebs://'$3':'`expr $STARTING_PORT_CLIENT + $i`'/C'$i
  done
}

generatestartbroker(){
for (( i = $1; i <= $2; i++ ))
  do
    broker="B${i}"
    broker_uri="B${i}_URI"
    echo 'echo "Starting broker '${!broker}'"'
    echo '${MUDEBS_HOME_SCRIPTS}/startbroker --uri '${!broker_uri}' --log PERFORMANCE.PERF --perfconfigfile' $CONFIG_FILE
done
}

generatestartclient(){
for (( i = $1; i <= $2; i++ ))
  do
    client="C${i}"
    client_uri="C${i}_URI"
    broker_uri="B${i}_URI"
    echo 'echo "Starting client '${!client}' connected to broker 'B${i}'"'
    echo '${MUDEBS_HOME_SCRIPTS}/startperfclient --uri '${!client_uri}' --broker' ${!broker_uri}
done
}

generatestarconnectingbrokers(){
while read line; do 
   case "$line" in \#*) continue ;; esac
   [[ $line =~ '$ns duplex-link $n('([0-9]+)') $n('([0-9]+) ]]
   first_number=${BASH_REMATCH[1]}
   second_number=${BASH_REMATCH[2]}
   if [ -n "$first_number" ]; then      
      first_broker="B${first_number}"
      second_broker="B${second_number}"
      first_broker_uri="B${first_number}_URI"
      second_broker_uri="B${second_number}_URI"         
      echo 'echo "Connecting broker '$first_broker' to broker '$second_broker'"'
      echo '${MUDEBS_HOME_SCRIPTS}/broker --uri '${!first_broker_uri}' --command connect --neigh '${!second_broker_uri}
      echo 'sleep 1'
   fi
done < $1
}

#/broker's identifier/dimension/subscope/superscope
generatestarjoinscopecalls(){
positiveInteger='[0-9]+$'
while read line; do 
   case "$line" in \#*) continue ;; esac
   broker_id=$(cut -d/ -f2 <<<"${line}")
   dimension=$(cut -d/ -f3 <<<"${line}")
   subscope=$(cut -d/ -f4 <<<"${line}")
   superscope=$(cut -d/ -f5 <<<"${line}")
   if [[ -n "$broker_id" ]]; then 
      broker="B${broker_id}"      
      broker_uri="B${broker_id}_URI"      
      echo -e 'echo "On '${!broker}', join scope ('${subscope}','${superscope}')"'
      echo -e '${MUDEBS_HOME_SCRIPTS}/broker --uri '${!broker_uri}' --command joinscope --dimension '${dimension}' --subscope '${subscope}' --superscope' ${superscope} '--mapupfile' $MAPUP_FILTER '--mapdownfile' $MAPDOWN_FILTER
      echo 'echo sleep 20'
      echo 'sleep 20'
   fi
done < $1
}

generateunsubpuball(){
for (( i = $1; i <= $2; i++ ))
  do
    client="C${i}"      
    client_uri="C${i}_URI" 
    echo -e 'echo "Client '${!client}' unsubscribe all filters"'
    echo '${MUDEBS_HOME_SCRIPTS}/client --uri '${!client_uri}' --command unsubscribe'
    echo -e 'echo "Client '${!client}' unadvertise all filters"'
    echo -e '${MUDEBS_HOME_SCRIPTS}/client --uri '${!client_uri}' --command unadvertise'
    echo 'sleep 1'
done
}

stopclient(){
for (( i = $1; i <= $2; i++ ))
  do
    client="C${i}"
    echo -e "Stoping client ${!client}"
    ${MUDEBS_HOME_SCRIPTS}/stopclient ${!client}
done
}

stopbroker(){
for (( i = $1; i <= $2; i++ ))
  do
    broker="B${i}"
    echo -e "Stoping broker ${!broker}"
    ${MUDEBS_HOME_SCRIPTS}/stopbroker ${!broker}
done
}

unsubpuball(){
for (( i = $1; i <= $2; i++ ))
  do
    client="C${i}"      
    client_uri="C${i}_URI" 
    echo -e "Client ${!client} unsubscribe all filters"
    ${MUDEBS_HOME_SCRIPTS}/client --uri ${!client_uri} --command unsubscribe
    echo
    echo -e "Client ${!client} unadvertise all filters"
    ${MUDEBS_HOME_SCRIPTS}/client --uri ${!client_uri} --command unadvertise
done
}