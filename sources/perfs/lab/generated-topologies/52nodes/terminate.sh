#!/bin/bash

. "$(cd $(dirname "$0") && pwd)"/util.sh

echo
echo "Hit return when you want to send terminate to broker $1"
read x
echo

echo On $1, terminate
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri $1 --command terminate_all