#!/bin/bash

MUDEBS_HOME_SCRIPTS="$(cd ../../../../muDEBS/scripts/ && pwd)"

echo 'Architecture of 52 brokers'
sleep 1

echo "Starting client C0 connected to broker B0"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2000/C0 --broker mudebs://localhost:3000/B0 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_52nodes.config #--log ROUTING.INFO
echo sleep 1
sleep 1
echo "Starting client C1 connected to broker B1"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2001/C1 --broker mudebs://localhost:3001/B1 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_52nodes.config #--log ROUTING.INFO
echo sleep 1
sleep 1
echo "Starting client C2 connected to broker B2"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2002/C2 --broker mudebs://localhost:3002/B2 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_52nodes.config #--log ROUTING.INFO
echo sleep 1
sleep 1
echo "Starting client C3 connected to broker B3"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2003/C3 --broker mudebs://localhost:3003/B3 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_52nodes.config #--log ROUTING.INFO
echo sleep 1
sleep 1
echo "Starting client C4 connected to broker B4"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2004/C4 --broker mudebs://localhost:3004/B4 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_52nodes.config #--log ROUTING.INFO
echo sleep 1
sleep 1
echo "Starting client C5 connected to broker B5"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2005/C5 --broker mudebs://localhost:3005/B5 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_52nodes.config #--log ROUTING.INFO
echo sleep 1
sleep 1
echo "Starting client C6 connected to broker B6"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2006/C6 --broker mudebs://localhost:3006/B6 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_52nodes.config #--log ROUTING.INFO
echo sleep 1
sleep 1
echo "Starting client C7 connected to broker B7"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2007/C7 --broker mudebs://localhost:3007/B7 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_52nodes.config #--log ROUTING.INFO
echo sleep 1
sleep 1
echo "Starting client C8 connected to broker B8"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2008/C8 --broker mudebs://localhost:3008/B8 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_52nodes.config #--log ROUTING.INFO
echo sleep 1
sleep 1
echo "Starting client C9 connected to broker B9"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2009/C9 --broker mudebs://localhost:3009/B9 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_52nodes.config #--log ROUTING.INFO
echo sleep 1
sleep 1
echo "Starting client C10 connected to broker B10"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2010/C10 --broker mudebs://localhost:3010/B10 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_52nodes.config #--log ROUTING.INFO
echo sleep 1
sleep 1
echo "Starting client C11 connected to broker B11"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2011/C11 --broker mudebs://localhost:3011/B11 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_52nodes.config #--log ROUTING.INFO
echo sleep 1
sleep 1
echo "Starting client C12 connected to broker B12"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2012/C12 --broker mudebs://localhost:3012/B12 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_52nodes.config #--log ROUTING.INFO
echo sleep 1
sleep 1
echo "Starting client C13 connected to broker B13"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2013/C13 --broker mudebs://localhost:3013/B13 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_52nodes.config #--log ROUTING.INFO
echo sleep 1
sleep 1
echo "Starting client C14 connected to broker B14"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2014/C14 --broker mudebs://localhost:3014/B14 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_52nodes.config #--log ROUTING.INFO
echo sleep 1
sleep 1
echo "Starting client C15 connected to broker B15"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2015/C15 --broker mudebs://localhost:3015/B15 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_52nodes.config #--log ROUTING.INFO
echo sleep 1
sleep 1
echo "Starting client C16 connected to broker B16"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2016/C16 --broker mudebs://localhost:3016/B16 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_52nodes.config #--log ROUTING.INFO
echo sleep 1
sleep 1
echo "Starting client C17 connected to broker B17"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2017/C17 --broker mudebs://localhost:3017/B17  --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_52nodes.config #--log ROUTING.INFO
echo sleep 1
sleep 1
echo "Starting client C18 connected to broker B18"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2018/C18 --broker mudebs://localhost:3018/B18 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_52nodes.config #--log ROUTING.INFO
echo sleep 1
sleep 1
echo "Starting client C19 connected to broker B19"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2019/C19 --broker mudebs://localhost:3019/B19 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_52nodes.config #--log ROUTING.INFO
echo sleep 1
sleep 1
echo "Starting client C20 connected to broker B20"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2020/C20 --broker mudebs://localhost:3020/B20 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_52nodes.config #--log ROUTING.INFO
echo sleep 1
sleep 1
echo "Starting client C21 connected to broker B21"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2021/C21 --broker mudebs://localhost:3021/B21 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_52nodes.config #--log ROUTING.INFO
echo sleep 1
sleep 1
echo "Starting client C22 connected to broker B22"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2022/C22 --broker mudebs://localhost:3022/B22 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_52nodes.config #--log ROUTING.INFO
echo sleep 1
sleep 1
echo "Starting client C23 connected to broker B23"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2023/C23 --broker mudebs://localhost:3023/B23 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_52nodes.config #--log ROUTING.INFO
echo sleep 1
sleep 1
echo "Starting client C24 connected to broker B24"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2024/C24 --broker mudebs://localhost:3024/B24 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_52nodes.config #--log ROUTING.INFO
echo sleep 1
sleep 1
echo "Starting client C25 connected to broker B25"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2025/C25 --broker mudebs://localhost:3025/B25 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_52nodes.config #--log ROUTING.INFO
echo sleep 1
sleep 1
echo "Starting client C26 connected to broker B26"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2026/C26 --broker mudebs://localhost:3026/B26 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_52nodes.config #--log ROUTING.INFO
echo sleep 1
sleep 1
echo "Starting client C27 connected to broker B27"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2027/C27 --broker mudebs://localhost:3027/B27 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_52nodes.config #--log ROUTING.INFO
echo sleep 1
sleep 1
echo "Starting client C28 connected to broker B28"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2028/C28 --broker mudebs://localhost:3028/B28 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_52nodes.config #--log ROUTING.INFO
echo sleep 1
sleep 1
echo "Starting client C29 connected to broker B29"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2029/C29 --broker mudebs://localhost:3029/B29 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_52nodes.config #--log ROUTING.INFO
echo sleep 1
sleep 1
echo "Starting client C30 connected to broker B30"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2030/C30 --broker mudebs://localhost:3030/B30 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_52nodes.config #--log ROUTING.INFO
echo sleep 1
sleep 1
echo "Starting client C31 connected to broker B31"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2031/C31 --broker mudebs://localhost:3031/B31 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_52nodes.config #--log ROUTING.INFO
echo sleep 1
sleep 1
echo "Starting client C32 connected to broker B32"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2032/C32 --broker mudebs://localhost:3032/B32 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_52nodes.config #--log ROUTING.INFO
echo sleep 1
sleep 1
echo "Starting client C33 connected to broker B33"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2033/C33 --broker mudebs://localhost:3033/B33 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_52nodes.config #--log ROUTING.INFO
echo sleep 1
sleep 1
echo "Starting client C34 connected to broker B34"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2034/C34 --broker mudebs://localhost:3034/B34 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_52nodes.config #--log ROUTING.INFO
echo sleep 1
sleep 1
echo "Starting client C35 connected to broker B35"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2035/C35 --broker mudebs://localhost:3035/B35 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_52nodes.config #--log ROUTING.INFO
echo sleep 1
sleep 1
echo "Starting client C36 connected to broker B36"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2036/C36 --broker mudebs://localhost:3036/B36 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_52nodes.config #--log ROUTING.INFO
echo sleep 1
sleep 1
echo "Starting client C37 connected to broker B37"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2037/C37 --broker mudebs://localhost:3037/B37 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_52nodes.config #--log ROUTING.INFO
echo sleep 1
sleep 1
echo "Starting client C38 connected to broker B38"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2038/C38 --broker mudebs://localhost:3038/B38 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_52nodes.config #--log ROUTING.INFO
echo sleep 1
sleep 1
echo "Starting client C39 connected to broker B39"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2039/C39 --broker mudebs://localhost:3039/B39 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_52nodes.config #--log ROUTING.INFO
echo sleep 1
sleep 1
echo "Starting client C40 connected to broker B40"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2040/C40 --broker mudebs://localhost:3040/B40 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_52nodes.config #--log ROUTING.INFO
echo sleep 1
sleep 1
echo "Starting client C41 connected to broker B41"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2041/C41 --broker mudebs://localhost:3041/B41 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_52nodes.config #--log ROUTING.INFO
echo sleep 1
sleep 1
echo "Starting client C42 connected to broker B42"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2042/C42 --broker mudebs://localhost:3042/B42 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_52nodes.config #--log ROUTING.INFO
echo sleep 1
sleep 1
echo "Starting client C43 connected to broker B43"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2043/C43 --broker mudebs://localhost:3043/B43 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_52nodes.config #--log ROUTING.INFO
echo sleep 1
sleep 1
echo "Starting client C44 connected to broker B44"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2044/C44 --broker mudebs://localhost:3044/B44 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_52nodes.config #--log ROUTING.INFO
echo sleep 1
sleep 1
echo "Starting client C45 connected to broker B45"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2045/C45 --broker mudebs://localhost:3045/B45 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_52nodes.config #--log ROUTING.INFO
echo sleep 1
sleep 1
echo "Starting client C46 connected to broker B46"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2046/C46 --broker mudebs://localhost:3046/B46 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_52nodes.config #--log ROUTING.INFO
echo sleep 1
sleep 1
echo "Starting client C47 connected to broker B47"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2047/C47 --broker mudebs://localhost:3047/B47 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_52nodes.config #--log ROUTING.INFO
echo sleep 1
sleep 1
echo "Starting client C48 connected to broker B48"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2048/C48 --broker mudebs://localhost:3048/B48 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_52nodes.config #--log ROUTING.INFO
echo sleep 1
sleep 1
echo "Starting client C49 connected to broker B49"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2049/C49 --broker mudebs://localhost:3049/B49 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_52nodes.config #--log ROUTING.INFO
echo sleep 1
sleep 1
echo "Starting client C50 connected to broker B50"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2050/C50 --broker mudebs://localhost:3050/B50 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_52nodes.config #--log ROUTING.INFO
echo sleep 1
sleep 1
echo "Starting client C51 connected to broker B51"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2051/C51 --broker mudebs://localhost:3051/B51 --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_52nodes.config #--log ROUTING.INFO
echo sleep 30
sleep 30

echo
echo "Connecting broker B0 to broker B11"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3000/B0 --command connect --neigh mudebs://localhost:3011/B11
echo sleep 1
sleep 1
echo "Connecting broker B0 to broker B7"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3000/B0 --command connect --neigh mudebs://localhost:3007/B7
echo sleep 1
sleep 1
echo "Connecting broker B0 to broker B2"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3000/B0 --command connect --neigh mudebs://localhost:3002/B2
echo sleep 1
sleep 1
echo "Connecting broker B0 to broker B3"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3000/B0 --command connect --neigh mudebs://localhost:3003/B3
echo sleep 1
sleep 1
echo "Connecting broker B1 to broker B27"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3001/B1 --command connect --neigh mudebs://localhost:3027/B27
echo sleep 1
sleep 1
echo "Connecting broker B1 to broker B24"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3001/B1 --command connect --neigh mudebs://localhost:3024/B24
echo sleep 1
sleep 1
echo "Connecting broker B1 to broker B16"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3001/B1 --command connect --neigh mudebs://localhost:3016/B16
echo sleep 1
sleep 1
echo "Connecting broker B1 to broker B12"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3001/B1 --command connect --neigh mudebs://localhost:3012/B12
echo sleep 1
sleep 1
echo "Connecting broker B1 to broker B2"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3001/B1 --command connect --neigh mudebs://localhost:3002/B2
echo sleep 1
sleep 1
echo "Connecting broker B1 to broker B3"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3001/B1 --command connect --neigh mudebs://localhost:3003/B3
echo sleep 1
sleep 1
echo "Connecting broker B2 to broker B39"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3002/B2 --command connect --neigh mudebs://localhost:3039/B39
echo sleep 1
sleep 1
echo "Connecting broker B2 to broker B36"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3002/B2 --command connect --neigh mudebs://localhost:3036/B36
echo sleep 1
sleep 1
echo "Connecting broker B2 to broker B30"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3002/B2 --command connect --neigh mudebs://localhost:3030/B30
echo sleep 1
sleep 1
echo "Connecting broker B3 to broker B49"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3003/B3 --command connect --neigh mudebs://localhost:3049/B49
echo sleep 1
sleep 1
echo "Connecting broker B3 to broker B43"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3003/B3 --command connect --neigh mudebs://localhost:3043/B43
echo sleep 1
sleep 1
echo "Connecting broker B3 to broker B40"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3003/B3 --command connect --neigh mudebs://localhost:3040/B40
echo sleep 1
sleep 1
echo "Connecting broker B4 to broker B6"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3004/B4 --command connect --neigh mudebs://localhost:3006/B6
echo sleep 1
sleep 1
echo "Connecting broker B5 to broker B6"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3005/B5 --command connect --neigh mudebs://localhost:3006/B6
echo sleep 1
sleep 1
echo "Connecting broker B5 to broker B7"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3005/B5 --command connect --neigh mudebs://localhost:3007/B7
echo sleep 1
sleep 1
echo "Connecting broker B6 to broker B7"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3006/B6 --command connect --neigh mudebs://localhost:3007/B7
echo sleep 1
sleep 1
echo "Connecting broker B7 to broker B8"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3007/B7 --command connect --neigh mudebs://localhost:3008/B8
echo sleep 1
sleep 1
echo "Connecting broker B9 to broker B10"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3009/B9 --command connect --neigh mudebs://localhost:3010/B10
echo sleep 1
sleep 1
echo "Connecting broker B9 to broker B11"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3009/B9 --command connect --neigh mudebs://localhost:3011/B11
echo sleep 1
sleep 1
echo "Connecting broker B10 to broker B11"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3010/B10 --command connect --neigh mudebs://localhost:3011/B11
echo sleep 1
sleep 1
echo "Connecting broker B12 to broker B14"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3012/B12 --command connect --neigh mudebs://localhost:3014/B14
echo sleep 1
sleep 1
echo "Connecting broker B12 to broker B15"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3012/B12 --command connect --neigh mudebs://localhost:3015/B15
echo sleep 1
sleep 1
echo "Connecting broker B13 to broker B14"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3013/B13 --command connect --neigh mudebs://localhost:3014/B14
echo sleep 1
sleep 1
echo "Connecting broker B13 to broker B15"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3013/B13 --command connect --neigh mudebs://localhost:3015/B15
echo sleep 1
sleep 1
echo "Connecting broker B16 to broker B17"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3016/B16 --command connect --neigh mudebs://localhost:3017/B17
echo sleep 1
sleep 1
echo "Connecting broker B16 to broker B18"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3016/B16 --command connect --neigh mudebs://localhost:3018/B18
echo sleep 1
sleep 1
echo "Connecting broker B17 to broker B19"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3017/B17 --command connect --neigh mudebs://localhost:3019/B19
echo sleep 1
sleep 1
echo "Connecting broker B18 to broker B20"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3018/B18 --command connect --neigh mudebs://localhost:3020/B20
echo sleep 1
sleep 1
echo "Connecting broker B19 to broker B20"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3019/B19 --command connect --neigh mudebs://localhost:3020/B20
echo sleep 1
sleep 1
echo "Connecting broker B21 to broker B22"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3021/B21 --command connect --neigh mudebs://localhost:3022/B22
echo sleep 1
sleep 1
echo "Connecting broker B21 to broker B23"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3021/B21 --command connect --neigh mudebs://localhost:3023/B23
echo sleep 1
sleep 1
echo "Connecting broker B21 to broker B25"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3021/B21 --command connect --neigh mudebs://localhost:3025/B25
echo sleep 1
sleep 1
echo "Connecting broker B22 to broker B24"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3022/B22 --command connect --neigh mudebs://localhost:3024/B24
echo sleep 1
sleep 1
echo "Connecting broker B23 to broker B24"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3023/B23 --command connect --neigh mudebs://localhost:3024/B24
echo sleep 1
sleep 1
echo "Connecting broker B26 to broker B27"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3026/B26 --command connect --neigh mudebs://localhost:3027/B27
echo sleep 1
sleep 1
echo "Connecting broker B28 to broker B29"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3028/B28 --command connect --neigh mudebs://localhost:3029/B29
echo sleep 1
sleep 1
echo "Connecting broker B28 to broker B30"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3028/B28 --command connect --neigh mudebs://localhost:3030/B30
echo sleep 1
sleep 1
echo "Connecting broker B29 to broker B30"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3029/B29 --command connect --neigh mudebs://localhost:3030/B30
echo sleep 1
sleep 1
echo "Connecting broker B31 to broker B34"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3031/B31 --command connect --neigh mudebs://localhost:3034/B34
echo sleep 1
sleep 1
echo "Connecting broker B31 to broker B35"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3031/B31 --command connect --neigh mudebs://localhost:3035/B35
echo sleep 1
sleep 1
echo "Connecting broker B32 to broker B33"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3032/B32 --command connect --neigh mudebs://localhost:3033/B33
echo sleep 1
sleep 1
echo "Connecting broker B32 to broker B36"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3032/B32 --command connect --neigh mudebs://localhost:3036/B36
echo sleep 1
sleep 1
echo "Connecting broker B33 to broker B34"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3033/B33 --command connect --neigh mudebs://localhost:3034/B34
echo sleep 1
sleep 1
echo "Connecting broker B35 to broker B36"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3035/B35 --command connect --neigh mudebs://localhost:3036/B36
echo sleep 1
sleep 1
echo "Connecting broker B37 to broker B38"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3037/B37 --command connect --neigh mudebs://localhost:3038/B38
echo sleep 1
sleep 1
echo "Connecting broker B37 to broker B39"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3037/B37 --command connect --neigh mudebs://localhost:3039/B39
echo sleep 1
sleep 1
echo "Connecting broker B38 to broker B39"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3038/B38 --command connect --neigh mudebs://localhost:3039/B39
echo sleep 1
sleep 1
echo "Connecting broker B40 to broker B41"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3040/B40 --command connect --neigh mudebs://localhost:3041/B41
echo sleep 1
sleep 1
echo "Connecting broker B40 to broker B42"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3040/B40 --command connect --neigh mudebs://localhost:3042/B42
echo sleep 1
sleep 1
echo "Connecting broker B41 to broker B42"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3041/B41 --command connect --neigh mudebs://localhost:3042/B42
echo sleep 1
sleep 1
echo "Connecting broker B44 to broker B45"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3044/B44 --command connect --neigh mudebs://localhost:3045/B45
echo sleep 1
sleep 1
echo "Connecting broker B44 to broker B50"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3044/B44 --command connect --neigh mudebs://localhost:3050/B50
echo sleep 1
sleep 1
echo "Connecting broker B45 to broker B48"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3045/B45 --command connect --neigh mudebs://localhost:3048/B48
echo sleep 1
sleep 1
echo "Connecting broker B46 to broker B49"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3046/B46 --command connect --neigh mudebs://localhost:3049/B49
echo sleep 1
sleep 1
echo "Connecting broker B47 to broker B48"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3047/B47 --command connect --neigh mudebs://localhost:3048/B48
echo sleep 1
sleep 1
echo "Connecting broker B48 to broker B51"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3048/B48 --command connect --neigh mudebs://localhost:3051/B51
echo sleep 1
sleep 1
echo "Connecting broker B49 to broker B50"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3049/B49 --command connect --neigh mudebs://localhost:3050/B50
echo sleep 1
sleep 1
echo "Connecting broker B49 to broker B51"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3049/B49 --command connect --neigh mudebs://localhost:3051/B51
echo sleep 30
sleep 30

echo
echo "On B0, join scope (s1,top)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3000/B0 --command joinscope --dimension s --subscope s1 --superscope top --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B2, join scope (s2,s1)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3002/B2 --command joinscope --dimension s --subscope s2 --superscope s1 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B39, join scope (s4,s2)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3039/B39 --command joinscope --dimension s --subscope s4 --superscope s2 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B37, join scope (s8,s4)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3037/B37 --command joinscope --dimension s --subscope s8 --superscope s4 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B39, join scope (s9,s4)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3039/B39 --command joinscope --dimension s --subscope s9 --superscope s4 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B30, join scope (s5,s2)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3030/B30 --command joinscope --dimension s --subscope s5 --superscope s2 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B28, join scope (s10,s5)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3028/B28 --command joinscope --dimension s --subscope s10 --superscope s5 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B29, join scope (s11,s5)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3029/B29 --command joinscope --dimension s --subscope s11 --superscope s5 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B3, join scope (s3,s1)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3003/B3 --command joinscope --dimension s --subscope s3 --superscope s1 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B40, join scope (s6,s3)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3040/B40 --command joinscope --dimension s --subscope s6 --superscope s3 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B41, join scope (s12,s6)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3041/B41 --command joinscope --dimension s --subscope s12 --superscope s6 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B42, join scope (s13,s6)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3042/B42 --command joinscope --dimension s --subscope s13 --superscope s6 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B49, join scope (s7,s3)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3049/B49 --command joinscope --dimension s --subscope s7 --superscope s3 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B50, join scope (s14,s7)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3050/B50 --command joinscope --dimension s --subscope s14 --superscope s7 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B51, join scope (s15,s7)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3051/B51 --command joinscope --dimension s --subscope s15 --superscope s7 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B1, join scope (r1,top)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3001/B1 --command joinscope --dimension r --subscope r1 --superscope top --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B2, join scope (r2,r1)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3002/B2 --command joinscope --dimension r --subscope r2 --superscope r1 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B36, join scope (r4,r2)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3036/B36 --command joinscope --dimension r --subscope r4 --superscope r2 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B32, join scope (r8,r4)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3032/B32 --command joinscope --dimension r --subscope r8 --superscope r4 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B35, join scope (r9,r4)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3035/B35 --command joinscope --dimension r --subscope r9 --superscope r4 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B39, join scope (r5,r2)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3039/B39 --command joinscope --dimension r --subscope r5 --superscope r2 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B37, join scope (r10,r5)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3037/B37 --command joinscope --dimension r --subscope r10 --superscope r5 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B38, join scope (r11,r5)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3038/B38 --command joinscope --dimension r --subscope r11 --superscope r5 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B3, join scope (r3,r1)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3003/B3 --command joinscope --dimension r --subscope r3 --superscope r1 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B40, join scope (r6,r3)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3040/B40 --command joinscope --dimension r --subscope r6 --superscope r3 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B41, join scope (r12,r6)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3041/B41 --command joinscope --dimension r --subscope r12 --superscope r6 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B42, join scope (r13,r6)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3042/B42 --command joinscope --dimension r --subscope r13 --superscope r6 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B49, join scope (r7,r3)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3049/B49 --command joinscope --dimension r --subscope r7 --superscope r3 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B46, join scope (r14,r7)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3046/B46 --command joinscope --dimension r --subscope r14 --superscope r7 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B51, join scope (r15,r7)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3051/B51 --command joinscope --dimension r --subscope r15 --superscope r7 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B2, join scope (t1,top)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3002/B2 --command joinscope --dimension t --subscope t1 --superscope top --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B1, join scope (t2,t1)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3001/B1 --command joinscope --dimension t --subscope t2 --superscope t1 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B12, join scope (t4,t2)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3012/B12 --command joinscope --dimension t --subscope t4 --superscope t2 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B14, join scope (t8,t4)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3014/B14 --command joinscope --dimension t --subscope t8 --superscope t4 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B15, join scope (t9,t4)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3015/B15 --command joinscope --dimension t --subscope t9 --superscope t4 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B24, join scope (t5,t2)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3024/B24 --command joinscope --dimension t --subscope t5 --superscope t2 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B22, join scope (t10,t5)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3022/B22 --command joinscope --dimension t --subscope t10 --superscope t5 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B23, join scope (t11,t5)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3023/B23 --command joinscope --dimension t --subscope t11 --superscope t5 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B0, join scope (t3,t1)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3000/B0 --command joinscope --dimension t --subscope t3 --superscope t1 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B7, join scope (t6,t3)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3007/B7 --command joinscope --dimension t --subscope t6 --superscope t3 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B8, join scope (t12,t6)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3008/B8 --command joinscope --dimension t --subscope t12 --superscope t6 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B5, join scope (t13,t6)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3005/B5 --command joinscope --dimension t --subscope t13 --superscope t6 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B11, join scope (t7,t3)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3011/B11 --command joinscope --dimension t --subscope t7 --superscope t3 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B9, join scope (t14,t7)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3009/B9 --command joinscope --dimension t --subscope t14 --superscope t7 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B10, join scope (t15,t7)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3010/B10 --command joinscope --dimension t --subscope t15 --superscope t7 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20

echo "Start publishing"
echo "Please wait..."
echo
echo C0 advertising filter \'startperfsubscribing\'
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2000/C0 --command advertise --id 'startperfsubscribing' --file ./ressources/advertisement.mudebs 
sleep 1
echo C0 publishing subscribing command with advertising filter \'startperfsubscribing\'
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2000/C0 --command publish --id 'startperfsubscribing' --content '<command><startsubscribing>w52w-C0-</startsubscribing></command>'
sleep 1
echo

echo
echo "Hit return when you want clients to unsubscribe to all filters"
read x
echo "Client C0 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2000/C0 --command unsubscribe
echo sleep 45
sleep 45
echo "Client C0 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2000/C0 --command unadvertise
echo sleep 45
sleep 45
echo "Client C1 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2001/C1 --command unsubscribe
echo sleep 45
sleep 45
echo "Client C1 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2001/C1 --command unadvertise
echo sleep 45
sleep 45
echo "Client C2 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2002/C2 --command unsubscribe
echo sleep 45
sleep 45
echo "Client C2 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2002/C2 --command unadvertise
echo sleep 45
sleep 45
echo "Client C3 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2003/C3 --command unsubscribe
echo sleep 45
sleep 45
echo "Client C3 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2003/C3 --command unadvertise
echo sleep 45
sleep 45
echo "Client C4 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2004/C4 --command unsubscribe
echo sleep 45
sleep 45
echo "Client C4 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2004/C4 --command unadvertise
echo sleep 45
sleep 45
echo "Client C5 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2005/C5 --command unsubscribe
echo sleep 45
sleep 45
echo "Client C5 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2005/C5 --command unadvertise
echo sleep 45
sleep 45
echo "Client C6 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2006/C6 --command unsubscribe
echo sleep 45
sleep 45
echo "Client C6 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2006/C6 --command unadvertise
echo sleep 45
sleep 45
echo "Client C7 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2007/C7 --command unsubscribe
echo sleep 45
sleep 45
echo "Client C7 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2007/C7 --command unadvertise
echo sleep 45
sleep 45
echo "Client C8 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2008/C8 --command unsubscribe
echo sleep 45
sleep 45
echo "Client C8 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2008/C8 --command unadvertise
echo sleep 45
sleep 45
echo "Client C9 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2009/C9 --command unsubscribe
echo sleep 45
sleep 45
echo "Client C9 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2009/C9 --command unadvertise
echo sleep 45
sleep 45
echo "Client C10 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2010/C10 --command unsubscribe
echo sleep 45
sleep 45
echo "Client C10 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2010/C10 --command unadvertise
echo sleep 45
sleep 45
echo "Client C11 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2011/C11 --command unsubscribe
echo sleep 45
sleep 45
echo "Client C11 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2011/C11 --command unadvertise
echo sleep 45
sleep 45
echo "Client C12 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2012/C12 --command unsubscribe
echo sleep 45
sleep 45
echo "Client C12 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2012/C12 --command unadvertise
echo sleep 45
sleep 45
echo "Client C13 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2013/C13 --command unsubscribe
echo sleep 45
sleep 45
echo "Client C13 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2013/C13 --command unadvertise
echo sleep 45
sleep 45
echo "Client C14 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2014/C14 --command unsubscribe
echo sleep 45
sleep 45
echo "Client C14 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2014/C14 --command unadvertise
echo sleep 45
sleep 45
echo "Client C15 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2015/C15 --command unsubscribe
echo sleep 45
sleep 45
echo "Client C15 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2015/C15 --command unadvertise
echo sleep 45
sleep 45
echo "Client C16 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2016/C16 --command unsubscribe
echo sleep 45
sleep 45
echo "Client C16 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2016/C16 --command unadvertise
echo sleep 45
sleep 45
echo "Client C17 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2017/C17 --command unsubscribe
echo sleep 45
sleep 45
echo "Client C17 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2017/C17 --command unadvertise
echo sleep 45
sleep 45
echo "Client C18 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2018/C18 --command unsubscribe
echo sleep 45
sleep 45
echo "Client C18 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2018/C18 --command unadvertise
echo sleep 45
sleep 45
echo "Client C19 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2019/C19 --command unsubscribe
echo sleep 45
sleep 45
echo "Client C19 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2019/C19 --command unadvertise
echo sleep 45
sleep 45
echo "Client C20 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2020/C20 --command unsubscribe
echo sleep 45
sleep 45
echo "Client C20 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2020/C20 --command unadvertise
echo sleep 45
sleep 45
echo "Client C21 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2021/C21 --command unsubscribe
echo sleep 45
sleep 45
echo "Client C21 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2021/C21 --command unadvertise
echo sleep 45
sleep 45
echo "Client C22 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2022/C22 --command unsubscribe
echo sleep 45
sleep 45
echo "Client C22 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2022/C22 --command unadvertise
echo sleep 45
sleep 45
echo "Client C23 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2023/C23 --command unsubscribe
echo sleep 45
sleep 45
echo "Client C23 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2023/C23 --command unadvertise
echo sleep 45
sleep 45
echo "Client C24 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2024/C24 --command unsubscribe
echo sleep 45
sleep 45
echo "Client C24 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2024/C24 --command unadvertise
echo sleep 45
sleep 45
echo "Client C25 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2025/C25 --command unsubscribe
echo sleep 45
sleep 45
echo "Client C25 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2025/C25 --command unadvertise
echo sleep 45
sleep 45
echo "Client C26 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2026/C26 --command unsubscribe
echo sleep 45
sleep 45
echo "Client C26 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2026/C26 --command unadvertise
echo sleep 45
sleep 45
echo "Client C27 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2027/C27 --command unsubscribe
echo sleep 45
sleep 45
echo "Client C27 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2027/C27 --command unadvertise
echo sleep 45
sleep 45
echo "Client C28 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2028/C28 --command unsubscribe
echo sleep 45
sleep 45
echo "Client C28 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2028/C28 --command unadvertise
echo sleep 45
sleep 45
echo "Client C29 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2029/C29 --command unsubscribe
echo sleep 45
sleep 45
echo "Client C29 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2029/C29 --command unadvertise
echo sleep 45
sleep 45
echo "Client C30 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2030/C30 --command unsubscribe
echo sleep 45
sleep 45
echo "Client C30 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2030/C30 --command unadvertise
echo sleep 45
sleep 45
echo "Client C31 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2031/C31 --command unsubscribe
echo sleep 45
sleep 45
echo "Client C31 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2031/C31 --command unadvertise
echo sleep 45
sleep 45
echo "Client C32 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2032/C32 --command unsubscribe
echo sleep 45
sleep 45
echo "Client C32 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2032/C32 --command unadvertise
echo sleep 45
sleep 45
echo "Client C33 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2033/C33 --command unsubscribe
echo sleep 45
sleep 45
echo "Client C33 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2033/C33 --command unadvertise
echo sleep 45
sleep 45
echo "Client C34 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2034/C34 --command unsubscribe
echo sleep 45
sleep 45
echo "Client C34 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2034/C34 --command unadvertise
echo sleep 45
sleep 45
echo "Client C35 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2035/C35 --command unsubscribe
echo sleep 45
sleep 45
echo "Client C35 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2035/C35 --command unadvertise
echo sleep 45
sleep 45
echo "Client C36 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2036/C36 --command unsubscribe
echo sleep 45
sleep 45
echo "Client C36 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2036/C36 --command unadvertise
echo sleep 45
sleep 45
echo "Client C37 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2037/C37 --command unsubscribe
echo sleep 45
sleep 45
echo "Client C37 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2037/C37 --command unadvertise
echo sleep 45
sleep 45
echo "Client C38 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2038/C38 --command unsubscribe
echo sleep 45
sleep 45
echo "Client C38 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2038/C38 --command unadvertise
echo sleep 45
sleep 45
echo "Client C39 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2039/C39 --command unsubscribe
echo sleep 45
sleep 45
echo "Client C39 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2039/C39 --command unadvertise
echo sleep 45
sleep 45
echo "Client C40 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2040/C40 --command unsubscribe
echo sleep 45
sleep 45
echo "Client C40 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2040/C40 --command unadvertise
echo sleep 45
sleep 45
echo "Client C41 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2041/C41 --command unsubscribe
echo sleep 45
sleep 45
echo "Client C41 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2041/C41 --command unadvertise
echo sleep 45
sleep 45
echo "Client C42 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2042/C42 --command unsubscribe
echo sleep 45
sleep 45
echo "Client C42 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2042/C42 --command unadvertise
echo sleep 45
sleep 45
echo "Client C43 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2043/C43 --command unsubscribe
echo sleep 45
sleep 45
echo "Client C43 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2043/C43 --command unadvertise
echo sleep 45
sleep 45
echo "Client C44 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2044/C44 --command unsubscribe
echo sleep 45
sleep 45
echo "Client C44 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2044/C44 --command unadvertise
echo sleep 45
sleep 45
echo "Client C45 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2045/C45 --command unsubscribe
echo sleep 45
sleep 45
echo "Client C45 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2045/C45 --command unadvertise
echo sleep 45
sleep 45
echo "Client C46 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2046/C46 --command unsubscribe
echo sleep 45
sleep 45
echo "Client C46 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2046/C46 --command unadvertise
echo sleep 45
sleep 45
echo "Client C47 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2047/C47 --command unsubscribe
echo sleep 45
sleep 45
echo "Client C47 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2047/C47 --command unadvertise
echo sleep 45
sleep 45
echo "Client C48 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2048/C48 --command unsubscribe
echo sleep 45
sleep 45
echo "Client C48 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2048/C48 --command unadvertise
echo sleep 45
sleep 45
echo "Client C49 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2049/C49 --command unsubscribe
echo sleep 45
sleep 45
echo "Client C49 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2049/C49 --command unadvertise
echo sleep 45
sleep 45
echo "Client C50 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2050/C50 --command unsubscribe
echo sleep 45
sleep 45
echo "Client C50 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2050/C50 --command unadvertise
echo sleep 45
sleep 45
echo "Client C51 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2051/C51 --command unsubscribe
echo sleep 45
sleep 45
echo "Client C51 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2051/C51 --command unadvertise
echo sleep 45
sleep 45

echo
echo "Hit return when you want to set logger overlay to debug for all brokers"
read x
echo "Setting logger overlay to debug for B0"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3000/B0 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B1"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3001/B1 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B2"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3002/B2 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B3"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3003/B3 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B4"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3004/B4 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B5"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3005/B5 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B6"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3006/B6 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B7"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3007/B7 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B8"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3008/B8 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B9"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3009/B9 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B10"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3010/B10 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B11"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3011/B11 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B12"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3012/B12 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B13"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3013/B13 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B14"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3014/B14 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B15"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3015/B15 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B16"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3016/B16 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B17"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3017/B17 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B18"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3018/B18 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B19"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3019/B19 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B20"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3020/B20 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B21"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3021/B21 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B22"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3022/B22 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B23"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3023/B23 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B24"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3024/B24 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B25"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3025/B25 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B26"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3026/B26 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B27"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3027/B27 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B28"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3028/B28 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B29"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3029/B29 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B30"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3030/B30 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B31"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3031/B31 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B32"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3032/B32 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B33"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3033/B33 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B34"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3034/B34 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B35"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3035/B35 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B36"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3036/B36 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B37"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3037/B37 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B38"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3038/B38 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B39"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3039/B39 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B40"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3040/B40 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B41"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3041/B41 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B42"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3042/B42 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B43"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3043/B43 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B44"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3044/B44 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B45"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3045/B45 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B46"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3046/B46 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B47"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3047/B47 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B48"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3048/B48 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B49"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3049/B49 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B50"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3050/B50 --command setlog --name overlay --level debug
echo sleep 5
sleep 5
echo "Setting logger overlay to debug for B51"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3051/B51 --command setlog --name overlay --level debug
echo sleep 5
sleep 5

echo
echo "Hit return when you want to send termination detection to broker B0"
read x
echo On B0, termination detection
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3000/B0 --command term_detection
echo sleep 5
sleep 5

echo
echo "Hit return when you want to send terminate to broker B0"
read x
echo On B0, terminate
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3000/B0 --command terminate_all
