#Make a NS simulator 
set ns [new Simulator]        

# Create the trace file that will be the input file four NAM
set namfile [open 52nodes.gtitm-0.nam w]

# Tell NS to write NAM network events to this trace file
$ns namtrace-all $namfile

# Define a 'finish' procedure
proc finish {} {
   global ns namfile

   $ns flush-trace     ;# flush trace files
   close $namfile      ;# close trace file

   exit 0
}

# Path the topology file generated using sgb2ns: change example.gtitm-0.tcl------------
# the desired path generated file
source 52nodes.gtitm-0.tcl
# "ns" and "n" are mandatory, but not 1 in Mb: for example, 2Mb can be used
create-topology ns n 1Mb         

# Run simulation !!!!
$ns run
