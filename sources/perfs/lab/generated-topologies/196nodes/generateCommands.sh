#!/bin/bash

. "$(cd $(dirname "$0") && pwd)"/util.sh

echo ''
echo '#identifiers of brokers'
generateidbroker $STARTING_BROKER $ENDING_BROKER
echo ''
echo ''
echo ''
echo ''
echo '#uri of brokers'
generateuribroker $STARTING_BROKER $ENDING_BROKER $HOSTNAME
echo ''
echo ''
echo ''
echo ''
echo '#identifiers of clients'
generateidclient $STARTING_BROKER $ENDING_BROKER
echo ''
echo ''
echo ''
echo ''
echo '#uri of clients'
generateuriclient $STARTING_BROKER $ENDING_BROKER $HOSTNAME
echo ''
echo ''
echo ''
echo ''
echo '#starting brokers'
generatestartbroker $STARTING_BROKER $ENDING_BROKER
echo ''
echo ''
echo ''
echo '#starting clients'
generatestartclient $STARTING_BROKER $ENDING_BROKER
echo ''
echo ''
echo ''
echo ''
echo '#starting connections' 
generatestarconnectingbrokers _196nodes.gtitm-0_adjusted.tcl
echo ''
echo ''
echo ''
echo ''
generatestarjoinscopecalls joinscopeCalls.input
echo ''
echo ''
echo ''
echo ''
echo '#starting unsubcribing and advertising'
generateunsubpuball $STARTING_BROKER $ENDING_BROKER
echo ''
echo ''
echo ''
echo ''
echo '#set log overlay'
generatesetlog $STARTING_BROKER $ENDING_BROKER