#!/bin/bash

. "$(cd $(dirname "$0") && pwd)"/util.sh

echo 'Architecture of 196 brokers'
echo sleep 1
sleep 1

startperfclientwithlocalbroker $STARTING_BROKER $ENDING_BROKER
echo 
echo sleep 5
sleep 5
echo 

connectbrokers _196nodes.gtitm-0_adjusted.tcl
echo 
echo sleep 5 
sleep 5
echo 

echo "On B0, join scope (s1,top)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3000/B0 --command joinscope --dimension s --subscope s1 --superscope top --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B1, join scope (s2,s1)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3001/B1 --command joinscope --dimension s --subscope s2 --superscope s1 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapupfilter.mudebs
echo sleep 20
sleep 20
echo "On B60, join scope (s4,s2)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3060/B60 --command joinscope --dimension s --subscope s4 --superscope s2 --mapupfile ./ressources/visibility-filter-always-false.mudebs --mapdownfile ./ressources/visibility-filter-always-false.mudebs
echo sleep 20
sleep 20
echo "On B54, join scope (s8,s4)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3054/B54 --command joinscope --dimension s --subscope s8 --superscope s4 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B61, join scope (s9,s4)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3061/B61 --command joinscope --dimension s --subscope s9 --superscope s4 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B66, join scope (s5,s2)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3066/B66 --command joinscope --dimension s --subscope s5 --superscope s2 --mapupfile ./ressources/visibility-filter-always-false.mudebs --mapdownfile ./ressources/visibility-filter-always-false.mudebs
echo sleep 20
sleep 20
echo "On B83, join scope (s10,s5)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3083/B83 --command joinscope --dimension s --subscope s10 --superscope s5 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B71, join scope (s11,s5)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3071/B71 --command joinscope --dimension s --subscope s11 --superscope s5 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B3, join scope (s3,s1)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3003/B3 --command joinscope --dimension s --subscope s3 --superscope s1 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapupfilter.mudebs
echo sleep 20
sleep 20
echo "On B181, join scope (s6,s3)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3181/B181 --command joinscope --dimension s --subscope s6 --superscope s3 --mapupfile ./ressources/visibility-filter-always-false.mudebs --mapdownfile ./ressources/visibility-filter-always-false.mudebs
echo sleep 20
sleep 20
echo "On B189, join scope (s12,s6)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3189/B189 --command joinscope --dimension s --subscope s12 --superscope s6 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B195, join scope (s13,s6)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3195/B195 --command joinscope --dimension s --subscope s13 --superscope s6 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B171, join scope (s7,s3)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3171/B171 --command joinscope --dimension s --subscope s7 --superscope s3 --mapupfile ./ressources/visibility-filter-always-false.mudebs --mapdownfile ./ressources/visibility-filter-always-false.mudebs
echo sleep 20
sleep 20
echo "On B168, join scope (s14,s7)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3168/B168 --command joinscope --dimension s --subscope s14 --superscope s7 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20
echo "On B172, join scope (s15,s7)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3172/B172 --command joinscope --dimension s --subscope s15 --superscope s7 --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 20
sleep 20


echo
echo "Please wait..."
echo "C0 advertising filter 'startperfsubscribing'"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2000/C0 --command advertise --id 'startperfsubscribing' --file ./ressources/advertisement.mudebs
echo sleep 5
sleep 5
echo "C0 publishing subscribing command with advertising filter 'startperfsubscribing'"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2000/C0 --command publish --id 'startperfsubscribing' --content '<command><startsubscribing>w196w-C0-</startsubscribing></command>'
echo sleep 20
sleep 20

echo
echo "Hit return when you want clients to unsubscribe to all filters"
read x
unsubpuball $STARTING_BROKER $ENDING_BROKER 
echo sleep 20
sleep 20


echo
echo "Hit return when you want to set logger overlay to debug for all brokers"
read x
setlog $STARTING_BROKER $ENDING_BROKER
echo sleep 20
sleep 20

echo
echo "Hit return when you want to send termination detection to broker B0"
read x
echo On B0, termination detection
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3000/B0 --command term_detection
echo sleep 20
sleep 20


echo
echo "Hit return when you want to send terminate to broker B0"
read x
echo On B0, terminate
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3000/B0 --command terminate_all
echo sleep 20
sleep 20


echo
echo "Hit return when you want to stop all clients and brokers"
read x
stopperfclientwithlocalbroker $STARTING_BROKER $ENDING_BROKER