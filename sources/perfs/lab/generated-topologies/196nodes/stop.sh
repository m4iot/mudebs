#!/bin/bash

. "$(cd $(dirname "$0") && pwd)"/util.sh

echo
echo "Hit return to stop brokers and clients"
read x
echo
stopbroker $STARTING_BROKER $ENDING_BROKER
stopclient $STARTING_BROKER $ENDING_BROKER