#!/bin/bash

MUDEBS_HOME_SCRIPTS="$(cd ../../../../muDEBS/scripts/ && pwd)"

CONFIG_FILE=./ressources/performancemultiscoping_196nodes.config

HOSTNAME=localhost
STARTING_PORT_BROKER=3000
STARTING_PORT_CLIENT=2000
STARTING_BROKER=0
ENDING_BROKER=195
MAPDOWN_FILTER=./ressources/mapdownfilter.mudebs
MAPUP_FILTER=./ressources/mapupfilter.mudebs
ADV_FILTER=./ressources/advertisement.mudebs
SUB_FILTER=./ressources/subscription.mudebs


# dimensions
DS=DS
DT=DT
DR=DR

# scopes of dimension R
R1=R1
R2=R2
R3=R3
R4=R4
R5=R5
R6=R6
R7=R7
R8=R8
R9=R9
R10=R10
R11=R11
R12=R12
R13=R13
R14=R14

# scopes of dimension S
S1=S1
S2=S2
S3=S3
S4=S4
S5=S5
S6=S6
S7=S7
S8=S8
S9=S9
S10=S10
S11=S11
S12=S12
S13=S13
S14=S14

# scopes of dimension T
T1=T1
T2=T2
T3=T3
T4=T4
T5=T5
T6=T6
T7=T7
T8=T8
T9=T9
T10=T10
T11=T11
T12=T12
T13=T13
T14=T14

#generated identifiers and uri
#------------------------------------------------------------------------------
B0=B0
B1=B1
B2=B2
B3=B3
B4=B4
B5=B5
B6=B6
B7=B7
B8=B8
B9=B9
B10=B10
B11=B11
B12=B12
B13=B13
B14=B14
B15=B15
B16=B16
B17=B17
B18=B18
B19=B19
B20=B20
B21=B21
B22=B22
B23=B23
B24=B24
B25=B25
B26=B26
B27=B27
B28=B28
B29=B29
B30=B30
B31=B31
B32=B32
B33=B33
B34=B34
B35=B35
B36=B36
B37=B37
B38=B38
B39=B39
B40=B40
B41=B41
B42=B42
B43=B43
B44=B44
B45=B45
B46=B46
B47=B47
B48=B48
B49=B49
B50=B50
B51=B51
B52=B52
B53=B53
B54=B54
B55=B55
B56=B56
B57=B57
B58=B58
B59=B59
B60=B60
B61=B61
B62=B62
B63=B63
B64=B64
B65=B65
B66=B66
B67=B67
B68=B68
B69=B69
B70=B70
B71=B71
B72=B72
B73=B73
B74=B74
B75=B75
B76=B76
B77=B77
B78=B78
B79=B79
B80=B80
B81=B81
B82=B82
B83=B83
B84=B84
B85=B85
B86=B86
B87=B87
B88=B88
B89=B89
B90=B90
B91=B91
B92=B92
B93=B93
B94=B94
B95=B95
B96=B96
B97=B97
B98=B98
B99=B99
B100=B100
B101=B101
B102=B102
B103=B103
B104=B104
B105=B105
B106=B106
B107=B107
B108=B108
B109=B109
B110=B110
B111=B111
B112=B112
B113=B113
B114=B114
B115=B115
B116=B116
B117=B117
B118=B118
B119=B119
B120=B120
B121=B121
B122=B122
B123=B123
B124=B124
B125=B125
B126=B126
B127=B127
B128=B128
B129=B129
B130=B130
B131=B131
B132=B132
B133=B133
B134=B134
B135=B135
B136=B136
B137=B137
B138=B138
B139=B139
B140=B140
B141=B141
B142=B142
B143=B143
B144=B144
B145=B145
B146=B146
B147=B147
B148=B148
B149=B149
B150=B150
B151=B151
B152=B152
B153=B153
B154=B154
B155=B155
B156=B156
B157=B157
B158=B158
B159=B159
B160=B160
B161=B161
B162=B162
B163=B163
B164=B164
B165=B165
B166=B166
B167=B167
B168=B168
B169=B169
B170=B170
B171=B171
B172=B172
B173=B173
B174=B174
B175=B175
B176=B176
B177=B177
B178=B178
B179=B179
B180=B180
B181=B181
B182=B182
B183=B183
B184=B184
B185=B185
B186=B186
B187=B187
B188=B188
B189=B189
B190=B190
B191=B191
B192=B192
B193=B193
B194=B194
B195=B195




#uri of brokers
B0_URI=mudebs://localhost:3000/B0
B1_URI=mudebs://localhost:3001/B1
B2_URI=mudebs://localhost:3002/B2
B3_URI=mudebs://localhost:3003/B3
B4_URI=mudebs://localhost:3004/B4
B5_URI=mudebs://localhost:3005/B5
B6_URI=mudebs://localhost:3006/B6
B7_URI=mudebs://localhost:3007/B7
B8_URI=mudebs://localhost:3008/B8
B9_URI=mudebs://localhost:3009/B9
B10_URI=mudebs://localhost:3010/B10
B11_URI=mudebs://localhost:3011/B11
B12_URI=mudebs://localhost:3012/B12
B13_URI=mudebs://localhost:3013/B13
B14_URI=mudebs://localhost:3014/B14
B15_URI=mudebs://localhost:3015/B15
B16_URI=mudebs://localhost:3016/B16
B17_URI=mudebs://localhost:3017/B17
B18_URI=mudebs://localhost:3018/B18
B19_URI=mudebs://localhost:3019/B19
B20_URI=mudebs://localhost:3020/B20
B21_URI=mudebs://localhost:3021/B21
B22_URI=mudebs://localhost:3022/B22
B23_URI=mudebs://localhost:3023/B23
B24_URI=mudebs://localhost:3024/B24
B25_URI=mudebs://localhost:3025/B25
B26_URI=mudebs://localhost:3026/B26
B27_URI=mudebs://localhost:3027/B27
B28_URI=mudebs://localhost:3028/B28
B29_URI=mudebs://localhost:3029/B29
B30_URI=mudebs://localhost:3030/B30
B31_URI=mudebs://localhost:3031/B31
B32_URI=mudebs://localhost:3032/B32
B33_URI=mudebs://localhost:3033/B33
B34_URI=mudebs://localhost:3034/B34
B35_URI=mudebs://localhost:3035/B35
B36_URI=mudebs://localhost:3036/B36
B37_URI=mudebs://localhost:3037/B37
B38_URI=mudebs://localhost:3038/B38
B39_URI=mudebs://localhost:3039/B39
B40_URI=mudebs://localhost:3040/B40
B41_URI=mudebs://localhost:3041/B41
B42_URI=mudebs://localhost:3042/B42
B43_URI=mudebs://localhost:3043/B43
B44_URI=mudebs://localhost:3044/B44
B45_URI=mudebs://localhost:3045/B45
B46_URI=mudebs://localhost:3046/B46
B47_URI=mudebs://localhost:3047/B47
B48_URI=mudebs://localhost:3048/B48
B49_URI=mudebs://localhost:3049/B49
B50_URI=mudebs://localhost:3050/B50
B51_URI=mudebs://localhost:3051/B51
B52_URI=mudebs://localhost:3052/B52
B53_URI=mudebs://localhost:3053/B53
B54_URI=mudebs://localhost:3054/B54
B55_URI=mudebs://localhost:3055/B55
B56_URI=mudebs://localhost:3056/B56
B57_URI=mudebs://localhost:3057/B57
B58_URI=mudebs://localhost:3058/B58
B59_URI=mudebs://localhost:3059/B59
B60_URI=mudebs://localhost:3060/B60
B61_URI=mudebs://localhost:3061/B61
B62_URI=mudebs://localhost:3062/B62
B63_URI=mudebs://localhost:3063/B63
B64_URI=mudebs://localhost:3064/B64
B65_URI=mudebs://localhost:3065/B65
B66_URI=mudebs://localhost:3066/B66
B67_URI=mudebs://localhost:3067/B67
B68_URI=mudebs://localhost:3068/B68
B69_URI=mudebs://localhost:3069/B69
B70_URI=mudebs://localhost:3070/B70
B71_URI=mudebs://localhost:3071/B71
B72_URI=mudebs://localhost:3072/B72
B73_URI=mudebs://localhost:3073/B73
B74_URI=mudebs://localhost:3074/B74
B75_URI=mudebs://localhost:3075/B75
B76_URI=mudebs://localhost:3076/B76
B77_URI=mudebs://localhost:3077/B77
B78_URI=mudebs://localhost:3078/B78
B79_URI=mudebs://localhost:3079/B79
B80_URI=mudebs://localhost:3080/B80
B81_URI=mudebs://localhost:3081/B81
B82_URI=mudebs://localhost:3082/B82
B83_URI=mudebs://localhost:3083/B83
B84_URI=mudebs://localhost:3084/B84
B85_URI=mudebs://localhost:3085/B85
B86_URI=mudebs://localhost:3086/B86
B87_URI=mudebs://localhost:3087/B87
B88_URI=mudebs://localhost:3088/B88
B89_URI=mudebs://localhost:3089/B89
B90_URI=mudebs://localhost:3090/B90
B91_URI=mudebs://localhost:3091/B91
B92_URI=mudebs://localhost:3092/B92
B93_URI=mudebs://localhost:3093/B93
B94_URI=mudebs://localhost:3094/B94
B95_URI=mudebs://localhost:3095/B95
B96_URI=mudebs://localhost:3096/B96
B97_URI=mudebs://localhost:3097/B97
B98_URI=mudebs://localhost:3098/B98
B99_URI=mudebs://localhost:3099/B99
B100_URI=mudebs://localhost:3100/B100
B101_URI=mudebs://localhost:3101/B101
B102_URI=mudebs://localhost:3102/B102
B103_URI=mudebs://localhost:3103/B103
B104_URI=mudebs://localhost:3104/B104
B105_URI=mudebs://localhost:3105/B105
B106_URI=mudebs://localhost:3106/B106
B107_URI=mudebs://localhost:3107/B107
B108_URI=mudebs://localhost:3108/B108
B109_URI=mudebs://localhost:3109/B109
B110_URI=mudebs://localhost:3110/B110
B111_URI=mudebs://localhost:3111/B111
B112_URI=mudebs://localhost:3112/B112
B113_URI=mudebs://localhost:3113/B113
B114_URI=mudebs://localhost:3114/B114
B115_URI=mudebs://localhost:3115/B115
B116_URI=mudebs://localhost:3116/B116
B117_URI=mudebs://localhost:3117/B117
B118_URI=mudebs://localhost:3118/B118
B119_URI=mudebs://localhost:3119/B119
B120_URI=mudebs://localhost:3120/B120
B121_URI=mudebs://localhost:3121/B121
B122_URI=mudebs://localhost:3122/B122
B123_URI=mudebs://localhost:3123/B123
B124_URI=mudebs://localhost:3124/B124
B125_URI=mudebs://localhost:3125/B125
B126_URI=mudebs://localhost:3126/B126
B127_URI=mudebs://localhost:3127/B127
B128_URI=mudebs://localhost:3128/B128
B129_URI=mudebs://localhost:3129/B129
B130_URI=mudebs://localhost:3130/B130
B131_URI=mudebs://localhost:3131/B131
B132_URI=mudebs://localhost:3132/B132
B133_URI=mudebs://localhost:3133/B133
B134_URI=mudebs://localhost:3134/B134
B135_URI=mudebs://localhost:3135/B135
B136_URI=mudebs://localhost:3136/B136
B137_URI=mudebs://localhost:3137/B137
B138_URI=mudebs://localhost:3138/B138
B139_URI=mudebs://localhost:3139/B139
B140_URI=mudebs://localhost:3140/B140
B141_URI=mudebs://localhost:3141/B141
B142_URI=mudebs://localhost:3142/B142
B143_URI=mudebs://localhost:3143/B143
B144_URI=mudebs://localhost:3144/B144
B145_URI=mudebs://localhost:3145/B145
B146_URI=mudebs://localhost:3146/B146
B147_URI=mudebs://localhost:3147/B147
B148_URI=mudebs://localhost:3148/B148
B149_URI=mudebs://localhost:3149/B149
B150_URI=mudebs://localhost:3150/B150
B151_URI=mudebs://localhost:3151/B151
B152_URI=mudebs://localhost:3152/B152
B153_URI=mudebs://localhost:3153/B153
B154_URI=mudebs://localhost:3154/B154
B155_URI=mudebs://localhost:3155/B155
B156_URI=mudebs://localhost:3156/B156
B157_URI=mudebs://localhost:3157/B157
B158_URI=mudebs://localhost:3158/B158
B159_URI=mudebs://localhost:3159/B159
B160_URI=mudebs://localhost:3160/B160
B161_URI=mudebs://localhost:3161/B161
B162_URI=mudebs://localhost:3162/B162
B163_URI=mudebs://localhost:3163/B163
B164_URI=mudebs://localhost:3164/B164
B165_URI=mudebs://localhost:3165/B165
B166_URI=mudebs://localhost:3166/B166
B167_URI=mudebs://localhost:3167/B167
B168_URI=mudebs://localhost:3168/B168
B169_URI=mudebs://localhost:3169/B169
B170_URI=mudebs://localhost:3170/B170
B171_URI=mudebs://localhost:3171/B171
B172_URI=mudebs://localhost:3172/B172
B173_URI=mudebs://localhost:3173/B173
B174_URI=mudebs://localhost:3174/B174
B175_URI=mudebs://localhost:3175/B175
B176_URI=mudebs://localhost:3176/B176
B177_URI=mudebs://localhost:3177/B177
B178_URI=mudebs://localhost:3178/B178
B179_URI=mudebs://localhost:3179/B179
B180_URI=mudebs://localhost:3180/B180
B181_URI=mudebs://localhost:3181/B181
B182_URI=mudebs://localhost:3182/B182
B183_URI=mudebs://localhost:3183/B183
B184_URI=mudebs://localhost:3184/B184
B185_URI=mudebs://localhost:3185/B185
B186_URI=mudebs://localhost:3186/B186
B187_URI=mudebs://localhost:3187/B187
B188_URI=mudebs://localhost:3188/B188
B189_URI=mudebs://localhost:3189/B189
B190_URI=mudebs://localhost:3190/B190
B191_URI=mudebs://localhost:3191/B191
B192_URI=mudebs://localhost:3192/B192
B193_URI=mudebs://localhost:3193/B193
B194_URI=mudebs://localhost:3194/B194
B195_URI=mudebs://localhost:3195/B195




#identifiers of clients
C0=C0
C1=C1
C2=C2
C3=C3
C4=C4
C5=C5
C6=C6
C7=C7
C8=C8
C9=C9
C10=C10
C11=C11
C12=C12
C13=C13
C14=C14
C15=C15
C16=C16
C17=C17
C18=C18
C19=C19
C20=C20
C21=C21
C22=C22
C23=C23
C24=C24
C25=C25
C26=C26
C27=C27
C28=C28
C29=C29
C30=C30
C31=C31
C32=C32
C33=C33
C34=C34
C35=C35
C36=C36
C37=C37
C38=C38
C39=C39
C40=C40
C41=C41
C42=C42
C43=C43
C44=C44
C45=C45
C46=C46
C47=C47
C48=C48
C49=C49
C50=C50
C51=C51
C52=C52
C53=C53
C54=C54
C55=C55
C56=C56
C57=C57
C58=C58
C59=C59
C60=C60
C61=C61
C62=C62
C63=C63
C64=C64
C65=C65
C66=C66
C67=C67
C68=C68
C69=C69
C70=C70
C71=C71
C72=C72
C73=C73
C74=C74
C75=C75
C76=C76
C77=C77
C78=C78
C79=C79
C80=C80
C81=C81
C82=C82
C83=C83
C84=C84
C85=C85
C86=C86
C87=C87
C88=C88
C89=C89
C90=C90
C91=C91
C92=C92
C93=C93
C94=C94
C95=C95
C96=C96
C97=C97
C98=C98
C99=C99
C100=C100
C101=C101
C102=C102
C103=C103
C104=C104
C105=C105
C106=C106
C107=C107
C108=C108
C109=C109
C110=C110
C111=C111
C112=C112
C113=C113
C114=C114
C115=C115
C116=C116
C117=C117
C118=C118
C119=C119
C120=C120
C121=C121
C122=C122
C123=C123
C124=C124
C125=C125
C126=C126
C127=C127
C128=C128
C129=C129
C130=C130
C131=C131
C132=C132
C133=C133
C134=C134
C135=C135
C136=C136
C137=C137
C138=C138
C139=C139
C140=C140
C141=C141
C142=C142
C143=C143
C144=C144
C145=C145
C146=C146
C147=C147
C148=C148
C149=C149
C150=C150
C151=C151
C152=C152
C153=C153
C154=C154
C155=C155
C156=C156
C157=C157
C158=C158
C159=C159
C160=C160
C161=C161
C162=C162
C163=C163
C164=C164
C165=C165
C166=C166
C167=C167
C168=C168
C169=C169
C170=C170
C171=C171
C172=C172
C173=C173
C174=C174
C175=C175
C176=C176
C177=C177
C178=C178
C179=C179
C180=C180
C181=C181
C182=C182
C183=C183
C184=C184
C185=C185
C186=C186
C187=C187
C188=C188
C189=C189
C190=C190
C191=C191
C192=C192
C193=C193
C194=C194
C195=C195




#uri of clients
C0_URI=mudebs://localhost:2000/C0
C1_URI=mudebs://localhost:2001/C1
C2_URI=mudebs://localhost:2002/C2
C3_URI=mudebs://localhost:2003/C3
C4_URI=mudebs://localhost:2004/C4
C5_URI=mudebs://localhost:2005/C5
C6_URI=mudebs://localhost:2006/C6
C7_URI=mudebs://localhost:2007/C7
C8_URI=mudebs://localhost:2008/C8
C9_URI=mudebs://localhost:2009/C9
C10_URI=mudebs://localhost:2010/C10
C11_URI=mudebs://localhost:2011/C11
C12_URI=mudebs://localhost:2012/C12
C13_URI=mudebs://localhost:2013/C13
C14_URI=mudebs://localhost:2014/C14
C15_URI=mudebs://localhost:2015/C15
C16_URI=mudebs://localhost:2016/C16
C17_URI=mudebs://localhost:2017/C17
C18_URI=mudebs://localhost:2018/C18
C19_URI=mudebs://localhost:2019/C19
C20_URI=mudebs://localhost:2020/C20
C21_URI=mudebs://localhost:2021/C21
C22_URI=mudebs://localhost:2022/C22
C23_URI=mudebs://localhost:2023/C23
C24_URI=mudebs://localhost:2024/C24
C25_URI=mudebs://localhost:2025/C25
C26_URI=mudebs://localhost:2026/C26
C27_URI=mudebs://localhost:2027/C27
C28_URI=mudebs://localhost:2028/C28
C29_URI=mudebs://localhost:2029/C29
C30_URI=mudebs://localhost:2030/C30
C31_URI=mudebs://localhost:2031/C31
C32_URI=mudebs://localhost:2032/C32
C33_URI=mudebs://localhost:2033/C33
C34_URI=mudebs://localhost:2034/C34
C35_URI=mudebs://localhost:2035/C35
C36_URI=mudebs://localhost:2036/C36
C37_URI=mudebs://localhost:2037/C37
C38_URI=mudebs://localhost:2038/C38
C39_URI=mudebs://localhost:2039/C39
C40_URI=mudebs://localhost:2040/C40
C41_URI=mudebs://localhost:2041/C41
C42_URI=mudebs://localhost:2042/C42
C43_URI=mudebs://localhost:2043/C43
C44_URI=mudebs://localhost:2044/C44
C45_URI=mudebs://localhost:2045/C45
C46_URI=mudebs://localhost:2046/C46
C47_URI=mudebs://localhost:2047/C47
C48_URI=mudebs://localhost:2048/C48
C49_URI=mudebs://localhost:2049/C49
C50_URI=mudebs://localhost:2050/C50
C51_URI=mudebs://localhost:2051/C51
C52_URI=mudebs://localhost:2052/C52
C53_URI=mudebs://localhost:2053/C53
C54_URI=mudebs://localhost:2054/C54
C55_URI=mudebs://localhost:2055/C55
C56_URI=mudebs://localhost:2056/C56
C57_URI=mudebs://localhost:2057/C57
C58_URI=mudebs://localhost:2058/C58
C59_URI=mudebs://localhost:2059/C59
C60_URI=mudebs://localhost:2060/C60
C61_URI=mudebs://localhost:2061/C61
C62_URI=mudebs://localhost:2062/C62
C63_URI=mudebs://localhost:2063/C63
C64_URI=mudebs://localhost:2064/C64
C65_URI=mudebs://localhost:2065/C65
C66_URI=mudebs://localhost:2066/C66
C67_URI=mudebs://localhost:2067/C67
C68_URI=mudebs://localhost:2068/C68
C69_URI=mudebs://localhost:2069/C69
C70_URI=mudebs://localhost:2070/C70
C71_URI=mudebs://localhost:2071/C71
C72_URI=mudebs://localhost:2072/C72
C73_URI=mudebs://localhost:2073/C73
C74_URI=mudebs://localhost:2074/C74
C75_URI=mudebs://localhost:2075/C75
C76_URI=mudebs://localhost:2076/C76
C77_URI=mudebs://localhost:2077/C77
C78_URI=mudebs://localhost:2078/C78
C79_URI=mudebs://localhost:2079/C79
C80_URI=mudebs://localhost:2080/C80
C81_URI=mudebs://localhost:2081/C81
C82_URI=mudebs://localhost:2082/C82
C83_URI=mudebs://localhost:2083/C83
C84_URI=mudebs://localhost:2084/C84
C85_URI=mudebs://localhost:2085/C85
C86_URI=mudebs://localhost:2086/C86
C87_URI=mudebs://localhost:2087/C87
C88_URI=mudebs://localhost:2088/C88
C89_URI=mudebs://localhost:2089/C89
C90_URI=mudebs://localhost:2090/C90
C91_URI=mudebs://localhost:2091/C91
C92_URI=mudebs://localhost:2092/C92
C93_URI=mudebs://localhost:2093/C93
C94_URI=mudebs://localhost:2094/C94
C95_URI=mudebs://localhost:2095/C95
C96_URI=mudebs://localhost:2096/C96
C97_URI=mudebs://localhost:2097/C97
C98_URI=mudebs://localhost:2098/C98
C99_URI=mudebs://localhost:2099/C99
C100_URI=mudebs://localhost:2100/C100
C101_URI=mudebs://localhost:2101/C101
C102_URI=mudebs://localhost:2102/C102
C103_URI=mudebs://localhost:2103/C103
C104_URI=mudebs://localhost:2104/C104
C105_URI=mudebs://localhost:2105/C105
C106_URI=mudebs://localhost:2106/C106
C107_URI=mudebs://localhost:2107/C107
C108_URI=mudebs://localhost:2108/C108
C109_URI=mudebs://localhost:2109/C109
C110_URI=mudebs://localhost:2110/C110
C111_URI=mudebs://localhost:2111/C111
C112_URI=mudebs://localhost:2112/C112
C113_URI=mudebs://localhost:2113/C113
C114_URI=mudebs://localhost:2114/C114
C115_URI=mudebs://localhost:2115/C115
C116_URI=mudebs://localhost:2116/C116
C117_URI=mudebs://localhost:2117/C117
C118_URI=mudebs://localhost:2118/C118
C119_URI=mudebs://localhost:2119/C119
C120_URI=mudebs://localhost:2120/C120
C121_URI=mudebs://localhost:2121/C121
C122_URI=mudebs://localhost:2122/C122
C123_URI=mudebs://localhost:2123/C123
C124_URI=mudebs://localhost:2124/C124
C125_URI=mudebs://localhost:2125/C125
C126_URI=mudebs://localhost:2126/C126
C127_URI=mudebs://localhost:2127/C127
C128_URI=mudebs://localhost:2128/C128
C129_URI=mudebs://localhost:2129/C129
C130_URI=mudebs://localhost:2130/C130
C131_URI=mudebs://localhost:2131/C131
C132_URI=mudebs://localhost:2132/C132
C133_URI=mudebs://localhost:2133/C133
C134_URI=mudebs://localhost:2134/C134
C135_URI=mudebs://localhost:2135/C135
C136_URI=mudebs://localhost:2136/C136
C137_URI=mudebs://localhost:2137/C137
C138_URI=mudebs://localhost:2138/C138
C139_URI=mudebs://localhost:2139/C139
C140_URI=mudebs://localhost:2140/C140
C141_URI=mudebs://localhost:2141/C141
C142_URI=mudebs://localhost:2142/C142
C143_URI=mudebs://localhost:2143/C143
C144_URI=mudebs://localhost:2144/C144
C145_URI=mudebs://localhost:2145/C145
C146_URI=mudebs://localhost:2146/C146
C147_URI=mudebs://localhost:2147/C147
C148_URI=mudebs://localhost:2148/C148
C149_URI=mudebs://localhost:2149/C149
C150_URI=mudebs://localhost:2150/C150
C151_URI=mudebs://localhost:2151/C151
C152_URI=mudebs://localhost:2152/C152
C153_URI=mudebs://localhost:2153/C153
C154_URI=mudebs://localhost:2154/C154
C155_URI=mudebs://localhost:2155/C155
C156_URI=mudebs://localhost:2156/C156
C157_URI=mudebs://localhost:2157/C157
C158_URI=mudebs://localhost:2158/C158
C159_URI=mudebs://localhost:2159/C159
C160_URI=mudebs://localhost:2160/C160
C161_URI=mudebs://localhost:2161/C161
C162_URI=mudebs://localhost:2162/C162
C163_URI=mudebs://localhost:2163/C163
C164_URI=mudebs://localhost:2164/C164
C165_URI=mudebs://localhost:2165/C165
C166_URI=mudebs://localhost:2166/C166
C167_URI=mudebs://localhost:2167/C167
C168_URI=mudebs://localhost:2168/C168
C169_URI=mudebs://localhost:2169/C169
C170_URI=mudebs://localhost:2170/C170
C171_URI=mudebs://localhost:2171/C171
C172_URI=mudebs://localhost:2172/C172
C173_URI=mudebs://localhost:2173/C173
C174_URI=mudebs://localhost:2174/C174
C175_URI=mudebs://localhost:2175/C175
C176_URI=mudebs://localhost:2176/C176
C177_URI=mudebs://localhost:2177/C177
C178_URI=mudebs://localhost:2178/C178
C179_URI=mudebs://localhost:2179/C179
C180_URI=mudebs://localhost:2180/C180
C181_URI=mudebs://localhost:2181/C181
C182_URI=mudebs://localhost:2182/C182
C183_URI=mudebs://localhost:2183/C183
C184_URI=mudebs://localhost:2184/C184
C185_URI=mudebs://localhost:2185/C185
C186_URI=mudebs://localhost:2186/C186
C187_URI=mudebs://localhost:2187/C187
C188_URI=mudebs://localhost:2188/C188
C189_URI=mudebs://localhost:2189/C189
C190_URI=mudebs://localhost:2190/C190
C191_URI=mudebs://localhost:2191/C191
C192_URI=mudebs://localhost:2192/C192
C193_URI=mudebs://localhost:2193/C193
C194_URI=mudebs://localhost:2194/C194
C195_URI=mudebs://localhost:2195/C195
#------------------------------------------------------------------------------


WHITE="\033[0m"
RED="\033[31m"
BLUE="\033[34m"
PINK="\033[35m"
CYAN="\033[36m"
YELLOW="\033[33m"
GREEN="\033[42m"

colorbroker(){
  broker=$1
  echo -e $CYAN""$broker$WHITE
}

colorclient(){
  client=$1
  echo -e $YELLOW""$client$WHITE
}

colordimension(){
  dimension=$1
  echo -e $RED""$dimension$WHITE
}

colorscope(){
  scope=$1
  echo -e $PINK""$scope$WHITE
}

colorfilter(){
  filter=$1
  echo -e $GREEN""$filter$WHITE
}

generateidbroker(){
  for (( i = $1; i <= $2; i++ ))
  do
    echo "B$i=B$i"
  done
}

generateuribroker(){
  for (( i = $1; i <= $2; i++ ))
  do
    echo 'B'$i'_URI=mudebs://'$3':'`expr $STARTING_PORT_BROKER + $i`'/B'$i
  done
}

generateidclient(){
  for (( i = $1; i <= $2; i++ ))
  do
    echo "C$i=C$i"
  done
}

generateuriclient(){
  for (( i = $1; i <= $2; i++ ))
  do
    echo 'C'$i'_URI=mudebs://'$3':'`expr $STARTING_PORT_CLIENT + $i`'/C'$i
  done
}

generatestartbroker(){
for (( i = $1; i <= $2; i++ ))
  do
    broker="B${i}"
    broker_uri="B${i}_URI"
    echo 'echo "Starting broker '${!broker}'"'
    echo '${MUDEBS_HOME_SCRIPTS}/startbroker --uri '${!broker_uri}' --log PERFORMANCE.PERF --perfconfigfile' $CONFIG_FILE
done
}


generatestarconnectingbrokers(){
while read line; do 
   case "$line" in \#*) continue ;; esac
   [[ $line =~ '$ns duplex-link $n('([0-9]+)') $n('([0-9]+) ]]
   first_number=${BASH_REMATCH[1]}
   second_number=${BASH_REMATCH[2]}
   if [ -n "$first_number" ]; then      
      first_broker="B${first_number}"
      second_broker="B${second_number}"
      first_broker_uri="B${first_number}_URI"
      second_broker_uri="B${second_number}_URI"         
      echo 'echo "Connecting broker '$first_broker' to broker '$second_broker'"'
      echo '${MUDEBS_HOME_SCRIPTS}/broker --uri '${!first_broker_uri}' --command connect --neigh '${!second_broker_uri}
      echo 'sleep 1'
   fi
done < $1
}

#/broker's identifier/dimension/subscope/superscope
generatestarjoinscopecalls(){
positiveInteger='[0-9]+$'
while read line; do 
   case "$line" in \#*) continue ;; esac
   broker_id=$(cut -d/ -f2 <<<"${line}")
   dimension=$(cut -d/ -f3 <<<"${line}")
   subscope=$(cut -d/ -f4 <<<"${line}")
   superscope=$(cut -d/ -f5 <<<"${line}")
   if [[ -n "$broker_id" ]]; then 
      broker="B${broker_id}"      
      broker_uri="B${broker_id}_URI"      
      echo -e 'echo "On '${!broker}', join scope ('${subscope}','${superscope}')"'
      echo -e '${MUDEBS_HOME_SCRIPTS}/broker --uri '${!broker_uri}' --command joinscope --dimension '${dimension}' --subscope '${subscope}' --superscope' ${superscope} '--mapupfile' $MAPUP_FILTER '--mapdownfile' $MAPDOWN_FILTER
      echo 'echo sleep 20'
      echo 'sleep 20'
   fi
done < $1
}

generateunsubpuball(){
for (( i = $1; i <= $2; i++ ))
  do
    client="C${i}"      
    client_uri="C${i}_URI" 
    echo -e 'echo "Client '${!client}' unsubscribe all filters"'
    echo '${MUDEBS_HOME_SCRIPTS}/client --uri '${!client_uri}' --command unsubscribe'
    echo -e 'echo "Client '${!client}' unadvertise all filters"'
    echo -e '${MUDEBS_HOME_SCRIPTS}/client --uri '${!client_uri}' --command unadvertise'
    echo 'sleep 1'
done
}

stopclient(){
for (( i = $1; i <= $2; i++ ))
  do
    client="C${i}"
    echo -e "Stoping client ${!client}"
    ${MUDEBS_HOME_SCRIPTS}/stopclient ${!client}
done
}


generatesetlog(){
for (( i = $1; i <= $2; i++ ))
  do
    broker="B${i}"      
    broker_uri="B${i}_URI" 
    echo -e 'Setting logger overlay to debug for '${!broker}
    echo -e '${MUDEBS_HOME_SCRIPTS}/broker --uri '${!broker_uri}' --command setlog --name overlay --level debug' --log PERFORMANCE.PERF --perfconfigfile ./ressources/performancemultiscoping_52nodes.config #--log ROUTING.INFO
    echo 'sleep 5'
    echo sleep 5
done
}


#-------------------------------------------
startperfclientwithlocalbroker(){
for (( i = $1; i <= $2; i++ ))
  do
    client="C${i}"
    client_uri="C${i}_URI"
    broker_uri="B${i}_URI"
    echo -e 'echo "Starting client '${!client}' connected to broker 'B${i}'"'
    echo -e '${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri' ${!client_uri} '--broker' ${!broker_uri} '--log PERFORMANCE.PERF --perfconfigfile' $CONFIG_FILE #--log ROUTING.INFO'
    ${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri ${!client_uri} --broker ${!broker_uri} --log PERFORMANCE.PERF --perfconfigfile $CONFIG_FILE #--log ROUTING.INFO
    echo 'sleep 1'
    sleep 1
done
}

connectbrokers(){
while read line; do 
   case "$line" in \#*) continue ;; esac
   [[ $line =~ '$ns duplex-link $n('([0-9]+)') $n('([0-9]+) ]]
   first_number=${BASH_REMATCH[1]}
   second_number=${BASH_REMATCH[2]}
   if [ -n "$first_number" ]; then      
      first_broker="B${first_number}"
      second_broker="B${second_number}"
      first_broker_uri="B${first_number}_URI"
      second_broker_uri="B${second_number}_URI"         
      echo -e 'echo "Connecting broker '$first_broker' to broker '$second_broker'"'
      echo -e '${MUDEBS_HOME_SCRIPTS}/broker --uri '${!first_broker_uri}' --command connect --neigh '${!second_broker_uri}
      ${MUDEBS_HOME_SCRIPTS}/broker '--uri' ${!first_broker_uri} '--command connect --neigh' ${!second_broker_uri}
      echo 'sleep 1'
      sleep 1
   fi
done < $1
}

starjoinscopecalls(){
positiveInteger='[0-9]+$'
while read line; do 
   case "$line" in \#*) continue ;; esac
   broker_id=$(cut -d/ -f2 <<<"${line}")
   dimension=$(cut -d/ -f3 <<<"${line}")
   subscope=$(cut -d/ -f4 <<<"${line}")
   superscope=$(cut -d/ -f5 <<<"${line}")
   if [[ -n "$broker_id" ]]; then 
      broker="B${broker_id}"      
      broker_uri="B${broker_id}_URI"      
      echo -e 'echo "On '${!broker}', join scope ('${subscope}','${superscope}')"'
      ${MUDEBS_HOME_SCRIPTS}/broker '--uri '${!broker_uri}' --command joinscope --dimension '${dimension}' --subscope '${subscope}' --superscope' ${superscope} '--mapupfile' $MAPUP_FILTER '--mapdownfile' $MAPDOWN_FILTER            echo -e '${MUDEBS_HOME_SCRIPTS}/broker --uri '${!broker_uri}' --command joinscope --dimension '${dimension}' --subscope '${subscope}' --superscope' ${superscope} '--mapupfile' $MAPUP_FILTER '--mapdownfile' $MAPDOWN_FILTER
      echo 'sleep 20'
      sleep 20
   fi
done < $1
}

unsubpuball(){
for (( i = $1; i <= $2; i++ ))
  do
    client="C${i}"      
    client_uri="C${i}_URI" 
    echo -e "Client ${!client} unsubscribe all filters"
    ${MUDEBS_HOME_SCRIPTS}/client --uri ${!client_uri} --command unsubscribe
    echo sleep 30
    sleep 30
    echo -e "Client ${!client} unadvertise all filters"
    ${MUDEBS_HOME_SCRIPTS}/client --uri ${!client_uri} --command unadvertise
    echo sleep 30
    sleep 30
done
}

setlog(){
for (( i = $1; i <= $2; i++ ))
  do
    broker="B${i}"      
    broker_uri="B${i}_URI" 
    echo -e 'Setting logger overlay to debug for '${!broker}
    echo -e ${MUDEBS_HOME_SCRIPTS}/broker --uri ${!broker_uri} --command setlog --name overlay --level debug --log PERFORMANCE.PERF --perfconfigfile $CONFIG_FILE #--log ROUTING.INFO
    ${MUDEBS_HOME_SCRIPTS}/broker --uri ${!broker_uri} --command setlog --name overlay --level debug --log PERFORMANCE.PERF --perfconfigfile $CONFIG_FILE #--log ROUTING.INFO
    echo 'sleep 5'
    echo sleep 5
done
}


stopperfclientwithlocalbroker(){
for (( i = $1; i <= $2; i++ ))
  do
    client="C${i}"
    echo -e "Stoping client ${!client}"
    ${MUDEBS_HOME_SCRIPTS}/stopclient ${!client}
done
}