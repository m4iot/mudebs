#!/bin/bash

. "$(cd $(dirname "$0") && pwd)"/util.sh


echo
echo "Hit return when you want clients to unsubscribe to all filters"
read x
unsubpuball $STARTING_BROKER $ENDING_BROKER 
echo sleep 20
sleep 20


echo
echo "Hit return when you want to set logger overlay to debug for all brokers"
read x
setlog $STARTING_BROKER $ENDING_BROKER
echo sleep 20
sleep 20

echo
echo "Hit return when you want to send termination detection to broker B0"
read x
echo On B0, termination detection
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3000/B0 --command term_detection
echo sleep 20
sleep 20


echo
echo "Hit return when you want to send terminate to broker B0"
read x
echo On B0, terminate
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3000/B0 --command terminate_all
echo sleep 20
sleep 20


echo
echo "Hit return when you want to stop all clients and brokers"
read x
stopperfclientwithlocalbroker $STARTING_BROKER $ENDING_BROKER
