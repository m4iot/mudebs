#!/bin/bash

. "$(cd $(dirname "$0") && pwd)"/util.sh

echo 'Architecture of 7 brokers'
echo sleep 1
sleep 1

stopclient 0 6

stopclient(){
for (( i = $1; i <= $2; i++ ))
  do
    client="C${i}"
    echo -e "Stoping client ${!client}"
    ${MUDEBS_HOME_SCRIPTS}/stopclient ${!client}
done
}