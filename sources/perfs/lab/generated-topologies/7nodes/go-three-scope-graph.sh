#!/bin/bash

. "$(cd $(dirname "$0") && pwd)"/util.sh

echo 'Architecture of 7 brokers'
echo sleep 1
sleep 1


echo
echo "Starting client C0 connected to broker B0"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2000/C0 --broker mudebs://localhost:3000/B0 --log PERFORMANCE.PERF --perfconfigfile ./ressources/multiscoping_7nodes.config #--log ROUTING.INFO 
echo sleep 1
sleep 1
echo "Starting client C1 connected to broker B1"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2001/C1 --broker mudebs://localhost:3001/B1 --log PERFORMANCE.PERF --perfconfigfile ./ressources/multiscoping_7nodes.config #--log ROUTING.INFO
echo sleep 1
sleep 1
echo "Starting client C2 connected to broker B2"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2002/C2 --broker mudebs://localhost:3002/B2 --log PERFORMANCE.PERF --perfconfigfile ./ressources/multiscoping_7nodes.config #--log ROUTING.INFO 
echo sleep 1
sleep 1
echo "Starting client C3 connected to broker B3"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2003/C3 --broker mudebs://localhost:3003/B3 --log PERFORMANCE.PERF --perfconfigfile ./ressources/multiscoping_7nodes.config #--log ROUTING.INFO 
echo sleep 1
sleep 1
echo "Starting client C4 connected to broker B4"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2004/C4 --broker mudebs://localhost:3004/B4 --log PERFORMANCE.PERF --perfconfigfile ./ressources/multiscoping_7nodes.config #--log ROUTING.INFO 
echo sleep 1
sleep 1
echo "Starting client C5 connected to broker B5"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2005/C5 --broker mudebs://localhost:3005/B5 --log PERFORMANCE.PERF --perfconfigfile ./ressources/multiscoping_7nodes.config #--log ROUTING.INFO 
echo sleep 1
sleep 1
echo "Starting client C6 connected to broker B6"
${MUDEBS_HOME_SCRIPTS}/startperfclientwithlocalbroker --uri mudebs://localhost:2006/C6 --broker mudebs://localhost:3006/B6 --log PERFORMANCE.PERF --perfconfigfile ./ressources/multiscoping_7nodes.config #--log ROUTING.INFO 
echo sleep 1


#B0: b,d,f
echo
echo "On B0, join scope (b,top)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3000/B0 --command joinscope --dimension d1 --subscope b --superscope top --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 2
sleep 2
echo "On B0, join scope (d,top)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3000/B0 --command joinscope --dimension d2 --subscope d --superscope top --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 2
sleep 2
echo "On B0, join scope (f,top)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3000/B0 --command joinscope --dimension d3 --subscope f --superscope top --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 2
sleep 2


#B1 <-> B0
echo
echo "Connecting broker B1 to broker B0"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3001/B1 --command connect --neigh mudebs://localhost:3000/B0
echo sleep 1
sleep 1
#B1: b,f
echo "On B1, join scope (b,top)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3001/B1 --command joinscope --dimension d1 --subscope b --superscope top --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 2
sleep 2
echo "On B1, join scope (f,top)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3001/B1 --command joinscope --dimension d3 --subscope f --superscope top --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 2
sleep 2


#B3 <-> B0
echo
echo "Connecting broker B3 to broker B0"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3003/B3 --command connect --neigh mudebs://localhost:3000/B0
echo sleep 1
sleep 1
#B3: d,f
echo "On B3, join scope (d,top)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3003/B3 --command joinscope --dimension d2 --subscope d --superscope top --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 5
sleep 5
echo "On B3, join scope (f,top)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3003/B3 --command joinscope --dimension d3 --subscope f --superscope top --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 5
sleep 5


#B2 <-> B0
echo
echo "Connecting broker B2 to broker B0"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3002/B2 --command connect --neigh mudebs://localhost:3000/B0
echo sleep 1
sleep 1
#B2: b,d
echo "On B2, join scope (b,top)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3002/B2 --command joinscope --dimension d1 --subscope b --superscope top --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 2
sleep 2
echo "On B2, join scope (d,top)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3002/B2 --command joinscope --dimension d2 --subscope d --superscope top --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 2
sleep 2


#B4 <-> B1 
echo
echo "Connecting broker B4 to broker B1"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3004/B4 --command connect --neigh mudebs://localhost:3001/B1
echo sleep 1
sleep 1
#B4 <-> B2 
echo "Connecting broker B4 to broker B2"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3004/B4 --command connect --neigh mudebs://localhost:3002/B2
echo sleep 1
sleep 1
#B4: (a,b)
echo "On B4, join scope (a,b)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3004/B4 --command joinscope --dimension d1 --subscope a --superscope b --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 5
sleep 5


#B5 <-> B2 
echo
echo "Connecting broker B5 to broker B2"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3005/B5 --command connect --neigh mudebs://localhost:3002/B2
echo sleep 1
sleep 1
#B5 <-> B3 
echo "Connecting broker B5 to broker B3"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3005/B5 --command connect --neigh mudebs://localhost:3003/B3
echo sleep 1
sleep 1
#B4: (a,b)
echo "On B5, join scope (c,d)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3005/B5 --command joinscope --dimension d2 --subscope c --superscope d --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 5
sleep 5


#B6 <-> B1 
echo
echo "Connecting broker B6 to broker B1"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3006/B6 --command connect --neigh mudebs://localhost:3001/B1
echo sleep 1
sleep 1
#B6 <-> B3 
echo "Connecting broker B6 to broker B3"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3006/B6 --command connect --neigh mudebs://localhost:3003/B3
echo sleep 1
sleep 1
#B4: (a,b)
echo "On B6, join scope (e,f)"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3006/B6 --command joinscope --dimension d3 --subscope e --superscope f --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
echo sleep 2
sleep 2


echo
echo "Please wait..."
echo "C0 advertising filter 'startperfsubscribing'"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2000/C0 --command advertise --id 'startperfsubscribing' --file ./ressources/advertisement.mudebs
echo sleep 3
sleep 3
echo "C0 publishing subscribing command with advertising filter 'startperfsubscribing'"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2000/C0 --command publish --id 'startperfsubscribing' --content '<command><startsubscribing>w16w-C0-</startsubscribing></command>'


echo
echo "Hit return when you want clients to unsubscribe to all filters"
read x
echo "Client C0 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2000/C0 --command unsubscribe
echo sleep 2
sleep 2
echo "Client C0 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2000/C0 --command unadvertise
echo sleep 2
sleep 2
echo "Client C1 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2001/C1 --command unsubscribe
echo sleep 2
sleep 2
echo "Client C1 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2001/C1 --command unadvertise
echo sleep 2
sleep 2
echo "Client C2 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2002/C2 --command unsubscribe
echo sleep 2
sleep 2
echo "Client C2 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2002/C2 --command unadvertise
echo sleep 2
sleep 2
echo "Client C3 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2003/C3 --command unsubscribe
echo sleep 2
sleep 2
echo "Client C3 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2003/C3 --command unadvertise
echo sleep 2
sleep 2
echo "Client C4 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2004/C4 --command unsubscribe
echo sleep 2
sleep 2
echo "Client C4 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2004/C4 --command unadvertise
echo sleep 2
sleep 2
echo "Client C5 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2005/C5 --command unsubscribe
echo sleep 2
sleep 2
echo "Client C5 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2005/C5 --command unadvertise
echo sleep 2
sleep 2
echo "Client C6 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2006/C6 --command unsubscribe
echo sleep 2
sleep 2
echo "Client C6 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2006/C6 --command unadvertise
echo sleep 2
sleep 2


echo
echo "Hit return when you want to set logger overlay to debug for all brokers"
read x
echo
echo "Setting logger overlay to debug for B0"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3000/B0 --command setlog --name overlay --level debug
echo sleep 2
sleep 2
echo "Setting logger overlay to debug for B1"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3001/B1 --command setlog --name overlay --level debug
echo sleep 2
sleep 2
echo "Setting logger overlay to debug for B2"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3002/B2 --command setlog --name overlay --level debug
echo sleep 2
sleep 2
echo "Setting logger overlay to debug for B3"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3003/B3 --command setlog --name overlay --level debug
echo sleep 2
sleep 2
echo "Setting logger overlay to debug for B4"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3004/B4 --command setlog --name overlay --level debug
echo sleep 2
sleep 2
echo "Setting logger overlay to debug for B5"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3005/B5 --command setlog --name overlay --level debug
echo sleep 2
sleep 2
echo "Setting logger overlay to debug for B6"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3006/B6 --command setlog --name overlay --level debug
echo sleep 2
sleep 2


echo
echo "Hit return when you want to send termination detection to broker B0"
read x
echo On B0, termination detection
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3000/B0 --command term_detection
echo sleep 2
sleep 2


echo
echo "Hit return when you want to send terminate to broker B0"
read x
echo On B0, terminate
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3000/B0 --command terminate_all
