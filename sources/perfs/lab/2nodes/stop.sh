#!/bin/bash

MUDEBS_HOME_SCRIPTS="$(cd $(dirname "$0")/../../../muDEBS/scripts/ && pwd)"
${MUDEBS_HOME_SCRIPTS}/utils.sh

echo 'Architecture of 2 brokers'
echo sleep 1
sleep 1

echo "Stopping all the clients"
${MUDEBS_HOME_SCRIPTS}/stopclient C0
${MUDEBS_HOME_SCRIPTS}/stopclient C1
echo "Stopping all the brokers"
${MUDEBS_HOME_SCRIPTS}/stopbroker B0
${MUDEBS_HOME_SCRIPTS}/stopbroker B1