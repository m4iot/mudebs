#!/bin/bash

MUDEBS_HOME_SCRIPTS="$(cd $(dirname "$0")/../../../muDEBS/scripts/ && pwd)"
${MUDEBS_HOME_SCRIPTS}/utils.sh

C0_URI=mudebs://localhost:2000/C0
C1_URI=mudebs://localhost:2001/C1
B0_URI=mudebs://localhost:3000/B0
B1_URI=mudebs://localhost:3001/B1

echo 'Architecture of 2 brokers'
echo sleep 0.5
sleep 0.5

echo
echo "Starting broker B0"
${MUDEBS_HOME_SCRIPTS}/startbroker --uri $B0_URI --log PERFORMANCE.PERF --perfconfigfile ./ressources/2nodes.config #--log ROUTING.INFO 
echo sleep 0.5
sleep 0.5
echo "Starting broker B1"
${MUDEBS_HOME_SCRIPTS}/startbroker --uri $B1_URI --log PERFORMANCE.PERF --perfconfigfile ./ressources/2nodes.config #--log ROUTING.INFO 
echo sleep 0.5
sleep 0.5
echo
echo "Connecting broker B0 to broker B1"
${MUDEBS_HOME_SCRIPTS}/broker --uri $B0_URI --command connect --neigh $B1_URI 
echo sleep 0.5
sleep 0.5


echo 
echo "Starting client C0 connected to broker B0"
${MUDEBS_HOME_SCRIPTS}/startclient --uri $C0_URI --broker $B0_URI
echo sleep 0.5
sleep 0.5
echo "Starting client C1 connected to broker B1"
${MUDEBS_HOME_SCRIPTS}/startclient --uri $C1_URI --broker $B1_URI
echo sleep 0.5
sleep 0.5


echo
echo "Please wait..."
echo C1 subscribing to filter \'sub\' in \'./commands/subscription.mudebs\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri $C1_URI --command subscribe --id 'sub' --file './ressources/subscription.mudebs'
echo sleep 2
sleep 2
echo C0 advertising to filter \'adv\' in \'./commands/advertisement.mudebs\'
${MUDEBS_HOME_SCRIPTS}/client --uri $C0_URI --command advertise --id 'adv' --file './ressources/advertisement.mudebs'
echo sleep 2
sleep 2
echo C0 publishing a notification with advertising filter \'adv\'
${MUDEBS_HOME_SCRIPTS}/client --uri $C0_URI --command publish --id 'adv' --content '<foo>HelloIAmC0</foo>'
echo sleep 2
sleep 2
echo C0 publishing a second notification with advertising filter \'adv\'
${MUDEBS_HOME_SCRIPTS}/client --uri $C0_URI --command publish --id 'adv' --content '<foo>MySecondNotificationIAmC0</foo>'


echo
echo "Hit return when you want clients to unsubscribe to all filters"
read x
echo "Client C0 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri $C0_URI --command unsubscribe
echo sleep 1
sleep 1
echo "Client C0 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri $C0_URI --command unadvertise
echo sleep 1
sleep 1
echo "Client C1 unsubscribe all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri $C1_URI --command unsubscribe
echo sleep 1
sleep 1
echo "Client C1 unadvertise all filters"
${MUDEBS_HOME_SCRIPTS}/client --uri $C1_URI --command unadvertise
echo sleep 1
sleep 1


echo
echo "Hit return when you want to set logger overlay to debug for all brokers"
read x
echo
echo "Setting logger overlay to debug for B0"
${MUDEBS_HOME_SCRIPTS}/broker --uri $B0_URI --command setlog --name overlay --level debug
echo sleep 1
sleep 1
echo "Setting logger overlay to debug for B1"
${MUDEBS_HOME_SCRIPTS}/broker --uri $B1_URI --command setlog --name overlay --level debug
echo sleep 1
sleep 1


echo
echo "Hit return when you want to send termination detection to broker B0"
read x
echo On B0, termination detection
${MUDEBS_HOME_SCRIPTS}/broker --uri $B0_URI --command term_detection
echo sleep 1
sleep 1


echo
echo "Hit return when you want to send terminate to broker B0"
read x
echo On B0, terminate
${MUDEBS_HOME_SCRIPTS}/broker --uri $B0_URI --command terminate_all
sleep 5

echo
echo "Force termination of clients and brokers"
echo "Stopping all the clients"
${MUDEBS_HOME_SCRIPTS}/stopclient C0
${MUDEBS_HOME_SCRIPTS}/stopclient C1
echo "Stopping all the brokers"
${MUDEBS_HOME_SCRIPTS}/stopbroker B0
${MUDEBS_HOME_SCRIPTS}/stopbroker B1
