#!/bin/bash

MUDEBS_HOME_SCRIPTS="$(cd ../../muDEBS/scripts && pwd)"
HOSTNAME=127.0.0.1
NB=50
NCPB=1
NPUB=1

for (( i = 0; i < $NB; i++ ))
  do 
    port=`expr 3000 + $i`
    echo "Starting broker mudebs://$HOSTNAME:$port/B$i"
    ${MUDEBS_HOME_SCRIPTS}/startbroker --uri mudebs://$HOSTNAME:$port/B$i #--log ROUTING.INFO
    sleep 0.5
done

for (( i = 1; i < $NB; i++ ))
do 
    port=`expr 3000 + $i`
    previous=`expr $i - 1`
    previousport=`expr 3000 + $i - 1`
    echo "Connecting broker mudebs://$HOSTNAME:$port/B$i to broker mudebs://$HOSTNAME:$previousport/B$previous"
    ${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://$HOSTNAME:$port/B$i --command connect --neigh mudebs://$HOSTNAME:$previousport/B$previous
    sleep 0.5
done

for (( i = 0; i < $NB; i++ ))
do 
    step=`expr $NCPB \* $i`
    for (( j = 0; j < $NCPB; j++ ))
    do
	portbroker=`expr 3000 + $i`
   	next=`expr $step + $j`
	port=`expr 2000 + $next`
	id=`expr $next`
	echo "Starting client mudebs://$HOSTNAME:$port/C$id connected to broker mudebs://$HOSTNAME:$portbroker/B$i"
	${MUDEBS_HOME_SCRIPTS}/startclient --uri mudebs://$HOSTNAME:$port/C$id --broker mudebs://$HOSTNAME:$portbroker/B$i
    	sleep 0.5
    done   
done

for (( i = 0; i < $NB; i++ ))
do 
    step=`expr $NCPB \* $i`
    for (( j = 0; j < $NCPB; j++ ))
    do
	portbroker=`expr 3000 + $i`
   	next=`expr $step + $j`
	port=`expr 2000 + $next`
	id=`expr $next`
	echo "Client mudebs://$HOSTNAME:$port/C$id advertising"
	${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://$HOSTNAME:$port/C$id --command advertise --id 'first' --file '../../muDEBS/commands/advertisement.mudebs'
	echo "Client mudebs://$HOSTNAME:$port/C$id subscribing"
	${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://$HOSTNAME:$port/C$id --command subscribe --id 'first' --file '../../muDEBS/commands/subscription.mudebs'
    	sleep 1
    done   
done

for (( i = 0; i < $NB; i++ ))
do 
    step=`expr $NCPB \* $i`
    for (( j = 0; j < $NCPB; j++ ))
    do
	for (( k = 0; k < $NPUB; k++ ))
	do
	    portbroker=`expr 3000 + $i`
   	    next=`expr $step + $j`
	    port=`expr 2000 + $next`
	    id=`expr $next`
	    echo "Client mudebs://$HOSTNAME:$port/C$id publishing"
	    ${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://$HOSTNAME:$port/C$id --command publish --id 'first' --content '<foo>apublication</foo>'
    	    sleep 1
	done
    done   
done

echo
echo "Hit return when you want to stop all the clients and brokers"
read x
echo "Stopping all the client"
NBNCPB=`expr $NB \* $NCPB`
for (( i = 0; i < $NBNCPB; i++ ))
  do 
    port=`expr 2000 + $i`
    echo "Stop client mudebs://$HOSTNAME:$port/C$i"
    ${MUDEBS_HOME_SCRIPTS}/stopclient C$i
done
echo "Stopping all the brokers"
for (( i = 0; i < $NB; i++ ))
  do 
    port=`expr 3000 + $i`
    echo "Stop broker mudebs://$HOSTNAME:$port/B$i"
    ${MUDEBS_HOME_SCRIPTS}/stopbroker B$i
done
