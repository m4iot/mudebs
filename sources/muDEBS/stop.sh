#!/bin/bash

MUDEBS_HOME_SCRIPTS="$(cd $(dirname "$0")/scripts && pwd)"


echo
echo "Hit return when you to stop all clients and all brokers"
read x
echo "Stopping all the clients"
${MUDEBS_HOME_SCRIPTS}/stopclient ClientA
${MUDEBS_HOME_SCRIPTS}/stopclient ClientB
${MUDEBS_HOME_SCRIPTS}/stopclient ClientC
${MUDEBS_HOME_SCRIPTS}/stopclient ClientD
${MUDEBS_HOME_SCRIPTS}/stopclient ClientE
${MUDEBS_HOME_SCRIPTS}/stopclient ClientF
${MUDEBS_HOME_SCRIPTS}/stopclient ClientG
${MUDEBS_HOME_SCRIPTS}/stopclient ClientH

echo "Stopping all the brokers"
${MUDEBS_HOME_SCRIPTS}/stopbroker BrokerA
${MUDEBS_HOME_SCRIPTS}/stopbroker BrokerB
${MUDEBS_HOME_SCRIPTS}/stopbroker BrokerC
${MUDEBS_HOME_SCRIPTS}/stopbroker BrokerD
${MUDEBS_HOME_SCRIPTS}/stopbroker BrokerE
${MUDEBS_HOME_SCRIPTS}/stopbroker BrokerF
