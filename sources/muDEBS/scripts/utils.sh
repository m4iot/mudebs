#!/bin/bash

MAVEN_REPOS=${HOME}/.m2/repository
MUDEBS_VERSION="0.17.3"

MUDEBS_HOME_SCRIPTS="$(cd $(dirname "$0") && pwd)"

PATHSEP=':'

case "`uname`" in
  (CYGWIN*) 
  	MUDEBS_HOME_CONFIG="$(cygpath -m "$MUDEBS_HOME_CONFIG")"
  	PATHSEP='\\;'
  	ADDR=localhost
  	;;
esac


COMMON_JAR=${MAVEN_REPOS}/muDEBS/muDEBS-common/${MUDEBS_VERSION}/muDEBS-common-${MUDEBS_VERSION}.jar
COMMUNICATION_JAR=${MAVEN_REPOS}/muDEBS/muDEBS-communication-javanio/${MUDEBS_VERSION}/muDEBS-communication-javanio-${MUDEBS_VERSION}.jar
BROKER_JAR=${MAVEN_REPOS}/muDEBS/muDEBS-broker/${MUDEBS_VERSION}/muDEBS-broker-${MUDEBS_VERSION}.jar
CLIENT_JAR=${MAVEN_REPOS}/muDEBS/muDEBS-client/${MUDEBS_VERSION}/muDEBS-client-${MUDEBS_VERSION}.jar
PERF_CLIENT_JAR=${MAVEN_REPOS}/muDEBS/muDEBS-perf-client/${MUDEBS_VERSION}/muDEBS-perf-client-${MUDEBS_VERSION}.jar
SCRIPTING_JAR=${MAVEN_REPOS}/muDEBS/muDEBS-scripting/${MUDEBS_VERSION}/muDEBS-scripting-${MUDEBS_VERSION}.jar


LOG4J_JAR=${MAVEN_REPOS}/log4j/log4j/1.2.17/log4j-1.2.17.jar
GSON_JAR=${MAVEN_REPOS}/com/google/code/gson/gson/2.3.1/gson-2.3.1.jar
XACML_JAR=${MAVEN_REPOS}/org/wso2/balana/org.wso2.balana/1.0.0-wso2v7/org.wso2.balana-1.0.0-wso2v7.jar
HAMCREST_JAR=${MAVEN_REPOS}/org/hamcrest/hamcrest-core/1.3/hamcrest-core-1.3.jar
COMMONS_LOGGING_JAR=${MAVEN_REPOS}/commons-logging/commons-logging/1.1.1/commons-logging-1.1.1.jar
