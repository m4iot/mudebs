/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2017 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform.  If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Christian Bac and Denis Conan
Contributor(s):
 */
package mudebs.communication.javanio;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

import mudebs.common.Log;
import mudebs.common.algorithms.routing.ScopePathWithStatus;
import mudebs.common.algorithms.routing.ScopePathWithStatusAdaptor;

import org.apache.log4j.Level;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * This class defines a message as a set of byte buffers.
 * 
 * One message is :
 * <ul>
 * <li>the message type as an <tt>int</tt>, and</li>
 * <li>the data as an object serialised using JSON.</li>
 * </ul>
 * 
 * @author Christian Bac
 * @author Denis Conan
 */
public class FullDuplexMsgWorker {
	/**
	 * number of messages sent and received before garbage collection;
	 */
	public final static int NB_MSGS_BEFORE_GARBAGE_COLLECTION = 1200;
	/**
	 * counter of received and sent messages used to force garbage collection.
	 */
	private static int nbMsgs = 0;
	/**
	 * this arrays can contain message headers in the first buffer (fixed size)
	 * and message body which size is described in the header We need two byte
	 * buffers due to asynchronism in input and output. put and get operations
	 * are not done at once.
	 */
	private ByteBuffer[] inBuffers, outBuffers;
	/**
	 * read message status, to describe completeness of data reception.
	 */
	private ReadMessageStatus readState;
	/**
	 * read/write channel.
	 */
	private SocketChannel rwChan = null;
	/**
	 * The type of the last message that was manipulated by this message worker.
	 */
	private int messType;
	/**
	 * The size of the last message that was manipulated by this message worker.
	 */
	private int messSize;
	/**
	 * Gson object for serialisation.
	 */
	private Gson gson;

	/**
	 * public constructor for an open channel, i.e. after an accept.
	 * 
	 * @param channel
	 *            the socketChannel that has been accepted on broker.
	 */
	public FullDuplexMsgWorker(SocketChannel channel) {
		inBuffers = new ByteBuffer[2];
		outBuffers = new ByteBuffer[2];
		inBuffers[0] = ByteBuffer.allocate(Integer.SIZE * 2 / Byte.SIZE);
		outBuffers[0] = ByteBuffer.allocate(Integer.SIZE * 2 / Byte.SIZE);
		inBuffers[1] = null;
		outBuffers[1] = null;
		readState = ReadMessageStatus.ReadUnstarted;
		rwChan = channel;
		gson = new GsonBuilder().registerTypeAdapter(ScopePathWithStatus.class,
				new ScopePathWithStatusAdaptor()).create();
	}

	/**
	 * configures the channel in non-blocking mode.
	 * 
	 * @throws IOException
	 *             the exception thrown in case of communication problem.
	 */
	public void configureNonBlocking() throws IOException {
		rwChan.configureBlocking(false);
	}

	/**
	 * gets the current channel of this worker.
	 * 
	 * @return my channel
	 */
	public SocketChannel getChannel() {
		return rwChan;
	}

	/**
	 * sends a message using the channel of this worker.
	 * 
	 * @param type
	 *            the message type.
	 * @param content
	 *            the message content is an object.
	 * @return the number of bytes sent.
	 * @throws IOException
	 *             the exception thrown in case of communication problem.
	 */
	public long sendMsg(int type, Object content) throws IOException {
		ByteArrayOutputStream bo = new ByteArrayOutputStream();
		ObjectOutputStream oo = new ObjectOutputStream(bo);
		oo.writeObject(gson.toJson(content));
		oo.close();
		messSize = bo.size();
		messType = type;
		setHeader(messType, messSize);
		if (outBuffers[1] == null || messSize >= outBuffers[1].capacity()) {
			outBuffers[1] = ByteBuffer.allocate(messSize);
		} else {
			outBuffers[1].clear();
		}
		outBuffers[1].put(bo.toByteArray());
		bo.close();
		outBuffers[1].flip();
		sendBuffers();
		nbMsgs++;
		if (nbMsgs >= NB_MSGS_BEFORE_GARBAGE_COLLECTION) {
			gson = new GsonBuilder()
					.registerTypeAdapter(ScopePathWithStatus.class,
							new ScopePathWithStatusAdaptor())
					.create();
			System.gc();
		}
		return messSize;
	}

	/**
	 * initialises the header with the size and the type of the content.
	 * 
	 * @param type
	 *            the message type.
	 * @param size
	 *            the message size.
	 */
	public void setHeader(int type, int size) {
		messType = type;
		messSize = size;
		outBuffers[0].clear();
		outBuffers[0].putInt(messType);
		outBuffers[0].putInt(messSize);
		outBuffers[0].flip();
	}

	/**
	 * sends the buffers through the channel.
	 * 
	 * @throws IOException
	 *             the exception thrown in case of communication problem.
	 */
	public void sendBuffers() throws IOException {
		rwChan.write(outBuffers);
	}

	/**
	 * sets the output buffer to the byte buffer of the data to send.
	 * 
	 * @param outdata
	 *            the byte buffer to send.
	 */
	public void setDataByteBuffer(ByteBuffer outdata) {
		outBuffers[1] = outdata;
	}

	/**
	 * closes the channel.
	 * 
	 * @throws IOException
	 *             the exception thrown in case of communication problem.
	 */
	public void close() throws IOException {
		rwChan.close();
	}

	/**
	 * reads a message.
	 * 
	 * @return a ReadMessageStatus to specify read progress
	 */
	public ReadMessageStatus readMessage() {
		int recvSize;
		if (readState == ReadMessageStatus.ReadUnstarted) {
			inBuffers[0].clear();
			readState = ReadMessageStatus.ReadHeaderStarted;
		}
		if (readState == ReadMessageStatus.ReadDataCompleted) {
			inBuffers[0].clear();
			inBuffers[1] = null;
			readState = ReadMessageStatus.ReadHeaderStarted;
		}
		if (readState == ReadMessageStatus.ReadHeaderStarted) {
			if (inBuffers[0].position() < inBuffers[0].capacity() - 1) {
				try {
					recvSize = rwChan.read(inBuffers[0]);
					if (Log.ON && Log.COMM.isEnabledFor(Level.DEBUG)) {
						Log.COMM.debug("readMessage receives : " + recvSize
								+ " bytes for the headers");
					}
					if (recvSize == 0) {
						return readState;
					}
					if (recvSize < 0) {
						readState = ReadMessageStatus.ChannelClosed;
						close();
						return readState;
					}
					if (inBuffers[0].position() < inBuffers[0].capacity() - 1) {
						return readState;
					}
				} catch (IOException ie) {
					readState = ReadMessageStatus.ChannelClosed;
					try {
						close();
					} catch (IOException e) {
						if (Log.ON && Log.COMM.isEnabledFor(Level.WARN)) {
							Log.COMM.warn(
									Log.printStackTrace(e.getStackTrace()));
						}
					}
					return readState;
				}
			}
			inBuffers[0].flip();
			if (Log.ON && Log.COMM.isEnabledFor(Level.TRACE)) {
				Log.COMM.trace("position and limit of inBuffers: "
						+ inBuffers[0].position() + " " + inBuffers[0].limit());
			}
			messType = inBuffers[0].getInt();
			messSize = inBuffers[0].getInt();
			if (Log.ON && Log.COMM.isEnabledFor(Level.DEBUG)) {
				Log.COMM.trace(
						"message type and size : " + messType + " " + messSize);
			}
			inBuffers[0].rewind();
			readState = ReadMessageStatus.ReadHeaderCompleted;
		}
		if (readState == ReadMessageStatus.ReadHeaderCompleted) {
			if (inBuffers[1] == null || messSize > inBuffers[1].capacity()) {
				inBuffers[1] = ByteBuffer.allocate(messSize);
			} else {
				inBuffers[1].limit(messSize);
			}
			readState = ReadMessageStatus.ReadDataStarted;
		}
		if (readState == ReadMessageStatus.ReadDataStarted) {
			if (inBuffers[1].position() < messSize - 1) {
				try {
					recvSize = rwChan.read(inBuffers[1]);
					if (Log.ON && Log.COMM.isEnabledFor(Level.DEBUG)) {
						Log.COMM.debug("readMessage receives : " + recvSize
								+ " bytes for the data");
					}
					if (recvSize == 0) {
						return readState;
					}
					if (recvSize < 0) {
						close();
						readState = ReadMessageStatus.ChannelClosed;
						return readState;
					}
				} catch (IOException ie) {
					readState = ReadMessageStatus.ChannelClosed;
					try {
						close();
					} catch (IOException e) {
						if (Log.ON && Log.COMM.isEnabledFor(Level.WARN)) {
							Log.COMM.warn(
									Log.printStackTrace(e.getStackTrace()));
						}
					}
					return readState;
				}
			}
			if (Log.ON && Log.COMM.isEnabledFor(Level.TRACE)) {
				Log.COMM.trace("position and capacity of inBuffers: "
						+ inBuffers[1].position() + " "
						+ inBuffers[1].capacity());
			}
			if (inBuffers[1].position() >= inBuffers[1].capacity() - 1) {
				readState = ReadMessageStatus.ReadDataCompleted;
			}
		}
		return readState;
	}

	/**
	 * returns the content data build out of the data part of the received
	 * message when the readStat is ReadDataCompleted. This operation should be
	 * stateless for the ByteBuffers, meaning that we can getData and after
	 * write the ByteBuffer if necessary.
	 * 
	 * @return content data as a serialised string.
	 * @throws IOException
	 *             the exception thrown in case of communication problem.
	 */
	public String getData() throws IOException {
		String res = null;
		if (readState == ReadMessageStatus.ReadDataCompleted) {
			try {
				inBuffers[1].flip();
				ByteArrayInputStream bi = new ByteArrayInputStream(
						inBuffers[1].array());
				ObjectInputStream oi = new ObjectInputStream(bi);
				res = (String) oi.readObject();
				oi.close();
				bi.close();
			} catch (ClassNotFoundException e) {
				if (Log.ON && Log.COMM.isEnabledFor(Level.WARN)) {
					Log.COMM.warn(Log.printStackTrace(e.getStackTrace()));
				}
			}
			inBuffers[1].rewind();
		}
		nbMsgs++;
		if (nbMsgs >= NB_MSGS_BEFORE_GARBAGE_COLLECTION) {
			gson = new GsonBuilder()
					.registerTypeAdapter(ScopePathWithStatus.class,
							new ScopePathWithStatusAdaptor())
					.create();
			System.gc();
		}
		return res;
	}

	/**
	 * gets the message type.
	 * 
	 * @return the message type.
	 */
	public int getMessType() {
		return messType;
	}

	/**
	 * gets the message size.
	 * 
	 * @return the message size.
	 */
	public int getMessSize() {
		return messSize;
	}

	/**
	 * gets the Gson serializer.
	 * 
	 * @return the Gson serialiszer.
	 */
	public Gson getGson() {
		return gson;
	}
}
