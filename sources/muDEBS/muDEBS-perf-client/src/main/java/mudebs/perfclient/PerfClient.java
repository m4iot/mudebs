/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2017 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Léon Lim
 */
package mudebs.perfclient;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import mudebs.client.Client;
import mudebs.client.ClientDelegate;
import mudebs.client.api.Consumer;
import mudebs.client.api.Worker;
import mudebs.common.MuDEBSException;
import mudebs.common.algorithms.ACK;
import mudebs.common.algorithms.OperationalMode;
import mudebs.common.algorithms.routing.Subscription;

/**
 * This class is the client of the muDEBS broker dedicated for performance
 * evaluation. It configures the client, launches the message dispatcher thread,
 * and acts as a client advertising, subscribing, publishing, etc., and reacting
 * to notifications.
 * 
 * @author Léon Lim
 * @author Denis Conan
 * 
 */
public class PerfClient extends Client {
	/**
	 * the termination of the main thread is asked for.
	 */
	boolean terminationRequired = false;

	/**
	 * states whether the client starts subscribing
	 */
	boolean startsubscribing = false;

	/**
	 * states whether the client starts publishing
	 */
	boolean startpublishing = false;

	/**
	 * the subscription filter identifier for receiving command start
	 * subscribing.
	 */
	private String subCommandId = "startperfsubCommand";

	/**
	 * the subscription filter identifier for receiving command start
	 * publishing.
	 */
	private String pubCommandId = "startperfpubCommand";

	/**
	 * the subscription filter for receiving command start subscribing.
	 */
	private String subPubFilterCommand = "function evaluate(doc) {"
			+ "importPackage(javax.xml.xpath);"
			+ "xpath = XPathFactory.newInstance().newXPath();"
			+ "var n = xpath.evaluate('/command', doc, XPathConstants.NODE);"
			+ "if (n == null) return false;" + "return true;}";

	public PerfClient(String[] argv, Consumer consumer, Worker worker)
			throws URISyntaxException, IOException, MuDEBSException {
		super(argv, consumer, worker);
	}

	@Override
	public void run() {
		delegate.startMessageDispatcher();
		try {
			delegate.waitConnectionToAccessBroker();
		} catch (MuDEBSException e1) {
			e1.printStackTrace();
		}
		List<Subscription> subs = new ArrayList<Subscription>();
		subs.add(new Subscription(subCommandId, subPubFilterCommand, null,
				null));
		subs.add(new Subscription(pubCommandId, subPubFilterCommand, null,
				null));
		try {
			delegate.subscribe(OperationalMode.GLOBAL, ACK.NO, subs);
		} catch (MuDEBSException e1) {
			e1.printStackTrace();
		}
		while (!terminationRequired) {
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			worker.work();
		}
	}

	/**
	 * gets the client delegate.
	 * 
	 * @return delegate
	 */
	public ClientDelegate delegate() {
		return super.delegate;
	}

	/**
	 * gets the filter command.
	 * 
	 * @return the filter command.
	 */
	public String getSubPubFilterCommand() {
		return subPubFilterCommand;
	}

	/**
	 * gets the subscription filter identifier for receiving command start
	 * publishing.
	 * 
	 * @return the identifier of the subscription filter.
	 */
	public String getPubFilterCommandId() {
		return pubCommandId;
	}

	/**
	 * gets the subscription filter identifier for receiving command start
	 * subscribing.
	 * 
	 * @return the subscription filter identifier.
	 */
	public String getSubCommandId() {
		return subCommandId;
	}
}
