/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2017 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Léon Lim
 */
package mudebs.perfclient;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import mudebs.client.api.Consumer;
import mudebs.client.api.Worker;
import mudebs.common.Constants;
import mudebs.common.MuDEBSException;
import mudebs.common.algorithms.ACK;
import mudebs.common.algorithms.OperationalMode;
import mudebs.common.algorithms.routing.MultiScopingSpecification;
import mudebs.common.algorithms.routing.Publication;
import mudebs.common.algorithms.routing.PublicationMsgContent;
import mudebs.common.algorithms.routing.Scope;
import mudebs.common.algorithms.routing.Subscription;

/**
 * This class is a dummy client, which is used for instance by the scripting
 * client. It references a client object.
 * 
 * @author Denis Conan
 * @author Léon Lim
 */
public class PerfWorker implements Worker {
	/**
	 * the boolean that states the client publishes.
	 */
	private boolean publish = true;
	/**
	 * the list of scopes known by access broker.
	 */
	private List<Scope> scopesList = null;
	/**
	 * the set of subsets of scopes;
	 */
	private Set<Set<Scope>> scopeSetsSet = null;
	/**
	 * the boolean that states information is displayed
	 */
	private boolean INFO = false;
	/**
	 * the URI of this worker.
	 */
	String myURI = null;
	/**
	 * the number of this worker.
	 */
	int myNumber = -1;
	/**
	 * total number of clients.
	 */
	private int nbClients = -1;
	/**
	 * the boolean that states the client has started subscribing.
	 */
	private boolean pubStarted = false;
	/**
	 * the boolean that states the client has started subscribing.
	 */
	private boolean subStarted = false;
	/**
	 * the client.
	 */
	private PerfClient client;
	/**
	 * delay factor between two advertisements.
	 */
	private static long TIME_BETWEEN_TWO_ADV = 200;
	/**
	 * delay factor between advertisement and publication.
	 */
	private static long TIME_BETWEEN_ADV_PUB = 200;
	/**
	 * delay factor between two publications.
	 */
	private static long TIME_BETWEEN_TWO_PUB = 200;
	/**
	 * delay factor between two subscriptions.
	 */
	private static long TIME_BETWEEN_TWO_SUB = 200;
	/**
	 * delay factor before publications after advertisements.
	 */
	private static long DELAY = 200;
	private List<String> perfAdvIds = new ArrayList<String>();
	private String pubSubPerfFilter = "function evaluate(doc) {"
			+ "importPackage(javax.xml.xpath);"
			+ "var xpath = XPathFactory.newInstance().newXPath();"
			+ "var n1 = xpath.evaluate('/performance', doc, XPathConstants.NODE);"
			+ "return (n1 != null);}";
	private boolean getScopeToDo = false;
	private boolean getScopeEnded = false;
	private String getScopeToDoContent = null;
	private boolean subEnded = false;
	/**
	 * the consumer of publication messages.
	 */
	protected Consumer consumer = new Consumer() {
		@Override
		public void consume(PublicationMsgContent msg) {
			String content = msg.getContent();
			if (INFO) {
				System.out.println(myURI + " receives msg : " + content);
				System.out.flush();
			}
			if (content.contains("startsubscribing")) {
				if (!subStarted && !subEnded) {
					getScopeToDo = true;
					getScopeToDoContent = content;
					if (getStringIdOfClientFromMsg(
							getScopeToDoContent) == myNumber) {
						subStarted = true;
					}
				}
			}
			if (!pubStarted) {
				if (content.contains("startpublishing")) {
					nbClients = getNbOfClientsFromMsg(content);
					// the identifier of the first client must be C0
					int publishingClientNumber = getStringIdOfClientFromMsg(
							content);
					if (publishingClientNumber == myNumber) {
						pubStarted = true;
						System.out.println(
								"C" + myNumber + " receives msg " + content);
						System.out.flush();
						int intId = 0;
						String stringId = "advertisement";
						if (scopeSetsSet != null && !scopeSetsSet.isEmpty()) {
							for (Set<Scope> scopesSet : scopeSetsSet) {
								MultiScopingSpecification phi = null;
								if (!content.contains("noscoping")) {
									phi = MultiScopingSpecification
											.create(scopesSet);
								}
								try {
									String advId = stringId + ++intId;
									perfAdvIds.add(advId);
									client.delegate().advertise(advId,
											OperationalMode.LOCAL, ACK.NO,
											pubSubPerfFilter, phi, null);
								} catch (MuDEBSException e) {
									e.printStackTrace();
								}
							}
							System.out.println(myURI + " : adv : "
									+ scopeSetsSet.size() + " advertisements");
							System.out.println(
									myURI + " : waits : " + TIME_BETWEEN_TWO_ADV
											* scopeSetsSet.size() / 5);
							System.out.flush();
							try {
								Thread.sleep(TIME_BETWEEN_TWO_ADV
										* scopeSetsSet.size() / 5);
							} catch (InterruptedException e1) {
								e1.printStackTrace();
							}
						}
						List<Publication> pubs = new ArrayList<Publication>();
						int i = 0;
						for (String advId : perfAdvIds) {
							String pubMsg = "<performance>C" + myNumber
									+ ", msg " + (++i) + "</performance>";
							pubs.add(new Publication(advId, 0, pubMsg));
						}
						System.out.print(myURI + " : pub");
						if (INFO) {
							System.out.print(myURI + " : pub");
							for (Publication pub : pubs) {
								System.out.print(" : " + pub.getIdAdv());
							}
							System.out.println();
						} else {
							System.out.println(
									" : " + pubs.size() + " publications"
											+ " (in a collective publication)");
						}
						System.out.flush();
						try {
							client.delegate().publish(pubs, ACK.NO);
						} catch (MuDEBSException e) {
							e.printStackTrace();
						}
						System.out.println(myURI
								+ " : waits (between two pub) : "
								+ TIME_BETWEEN_TWO_PUB * scopeSetsSet.size());
						System.out.flush();
						try {
							Thread.sleep(
									TIME_BETWEEN_TWO_PUB * scopeSetsSet.size());
						} catch (InterruptedException e1) {
							e1.printStackTrace();
						}
						System.out.println(myURI + " : unsub : "
								+ client.getPubFilterCommandId());
						System.out.flush();
						try {
							client.delegate().unsubscribe(
									client.getPubFilterCommandId(), ACK.NO);
						} catch (MuDEBSException e2) {
							e2.printStackTrace();
						}
						if (publishingClientNumber < nbClients - 1) {
							String pubId = client.getPubFilterCommandId()
									+ myNumber;
							System.out.println(myURI + " : adv : " + pubId);
							System.out.flush();
							try {
								client.delegate().advertise(pubId,
										OperationalMode.GLOBAL, ACK.NO,
										client.getSubPubFilterCommand(), null,
										null);
							} catch (MuDEBSException e) {
								e.printStackTrace();
							}
							System.out.println(myURI + " : waits : "
									+ (TIME_BETWEEN_TWO_PUB
											* scopeSetsSet.size()));
							System.out.flush();
							try {
								Thread.sleep(TIME_BETWEEN_TWO_PUB
										* scopeSetsSet.size());
							} catch (InterruptedException e1) {
								e1.printStackTrace();
							}
							int nexPublishingClientNumber = publishingClientNumber
									+ 1;
							String pubCommand = "<command><startpublishing>w"
									+ nbClients + "w-C"
									+ nexPublishingClientNumber
									+ "-</startpublishing></command>";
							if (content.contains("noscoping")) {
								pubCommand = "<command><startpublishing><noscoping>w"
										+ nbClients + "w-C"
										+ nexPublishingClientNumber
										+ "-</noscoping></startpublishing></command>";
							}
							try {
								System.out.println(myURI + " : pub : " + pubId);
								client.delegate().publish(pubId, ACK.NO,
										pubCommand);
								if (INFO) {
									System.out.println("C" + myNumber
											+ " sends " + pubCommand + " to "
											+ "C" + nexPublishingClientNumber);
								}
								System.out.flush();
							} catch (MuDEBSException e) {
								e.printStackTrace();
							}

						}

					}
				}
			}
		}
	};

	/**
	 * Constructor.
	 * 
	 * @param args
	 *            the command line arguments.
	 */
	public PerfWorker(String[] args) {
		for (int i = 0; i < args.length; i = i + 2) {
			if (args[i].equalsIgnoreCase(Constants.OPTION_URI)) {
				myURI = args[i + 1];
			}
		}
		if ((myURI != null) && !myURI.equalsIgnoreCase("")) {
			String myStringId = "";
			Pattern myIdPattern = Pattern.compile("[0-9]+$");
			Matcher myIdMatcher = myIdPattern.matcher(myURI);
			if (myIdMatcher.find()) {
				myStringId = myIdMatcher.group();
			}
			myNumber = Integer.parseInt(myStringId);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mudebs.client.api.Worker#initialise(java.lang.String[])
	 */
	@Override
	public void initialise(String[] args) throws Exception {
		client = new PerfClient(args, consumer, this);
		client.start();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mudebs.client.api.Worker#work()
	 */
	@Override
	public void work() {
		// the identifier of the first client must be C0
		if (getScopeToDo && !getScopeEnded) {
			getScopeEnded = true;
			try {
				scopesList = client.delegate().getScopesOfBroker();
				scopeSetsSet = getAllPossibleSubSetsOfScopes(scopesList);
				if (INFO) {
					System.out.println(
							myURI + " get set of all subsets of scopes of size "
									+ scopeSetsSet.size());
					System.out.flush();
				}
			} catch (MuDEBSException e2) {
				e2.printStackTrace();
			}
		}
		if (subStarted && !subEnded) {
			subEnded = true;
			String stringId = "subscription";
			int intId = 0;
			if (scopeSetsSet != null && !scopeSetsSet.isEmpty()) {
				List<Subscription> subs = new ArrayList<Subscription>();
				for (Set<Scope> scopesSet : scopeSetsSet) {
					MultiScopingSpecification phi = null;
					if (!getScopeToDoContent.contains("noscoping")) {
						phi = MultiScopingSpecification.create(scopesSet);
					}
					String subId = stringId + ++intId;
					subs.add(new Subscription(subId, pubSubPerfFilter, phi,
							null));
					System.out.println(" : " + phi);
				}
				try {
					System.out.print(myURI + " : sub");
					if (INFO) {
						for (Subscription sub : subs) {
							System.out.print(" : " + sub.getIdentifier() + " "
									+ sub.getPhi());
						}
						System.out.println();
					} else {
						System.out.println(" : " + subs.size() + " subcriptions"
								+ " (in a collective subcription)");
					}
					System.out.flush();
					client.delegate().subscribe(OperationalMode.GLOBAL, ACK.NO,
							subs);
				} catch (MuDEBSException e) {
					e.printStackTrace();
				}
			}
			System.out
					.println(myURI + " : unsub : " + client.getSubCommandId());
			System.out.flush();
			try {
				client.delegate().unsubscribe(client.getSubCommandId(), ACK.NO);
			} catch (MuDEBSException e) {
				e.printStackTrace();
			}
			nbClients = getNbOfClientsFromMsg(getScopeToDoContent);
			int publishingClientNumber = getStringIdOfClientFromMsg(
					getScopeToDoContent);
			if (publishingClientNumber < nbClients - 1) {
				long delayBeforeStartingSubForNextClient = TIME_BETWEEN_TWO_SUB
						* scopeSetsSet.size();
				System.out.println(myURI + " : waits : "
						+ delayBeforeStartingSubForNextClient + " ms");
				System.out.flush();
				try {
					Thread.sleep(delayBeforeStartingSubForNextClient);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
				String pubId = client.getSubCommandId() + myNumber;
				System.out.println(myURI + " : adv : " + pubId);
				System.out.flush();
				try {
					client.delegate().advertise(pubId, OperationalMode.GLOBAL,
							ACK.NO, client.getSubPubFilterCommand(), null,
							null);
				} catch (MuDEBSException e) {
					e.printStackTrace();
				}
				System.out.println(
						myURI + " : waits : " + TIME_BETWEEN_ADV_PUB + " ms");
				System.out.flush();
				try {
					Thread.sleep(TIME_BETWEEN_ADV_PUB);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
				int nexPublishingClientNumber = publishingClientNumber + 1;
				String subCommand = "<command><startsubscribing>w" + nbClients
						+ "w-C" + nexPublishingClientNumber
						+ "-</startsubscribing></command>";
				if (getScopeToDoContent.contains("noscoping")) {
					subCommand = "<command><startsubscribing><noscoping>w"
							+ nbClients + "w-C" + nexPublishingClientNumber
							+ "-</noscoping></startsubscribing></command>";
				}
				try {
					System.out.println(myURI + " : pub : " + pubId);
					System.out.flush();
					client.delegate().publish(pubId, ACK.NO, subCommand);
				} catch (MuDEBSException e) {
					e.printStackTrace();
				}

			}
			if ((publishingClientNumber == nbClients - 1) && publish) {
				long delayBeforeStartingPubForNextClient = DELAY * nbClients
						* 10;
				System.out.println(myURI + " : waits : "
						+ delayBeforeStartingPubForNextClient + " ms");
				System.out.flush();
				try {
					Thread.sleep(delayBeforeStartingPubForNextClient);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
				String pubId = client.getPubFilterCommandId() + myNumber;
				System.out.println(myURI + " : advertise : " + pubId);
				System.out.flush();
				try {
					client.delegate().advertise(pubId, OperationalMode.GLOBAL,
							ACK.NO, client.getSubPubFilterCommand(), null,
							null);
				} catch (MuDEBSException e) {
					e.printStackTrace();
				}
				System.out.println(
						myURI + " : waits : " + TIME_BETWEEN_ADV_PUB + " ms");
				System.out.flush();
				try {
					Thread.sleep(TIME_BETWEEN_ADV_PUB);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
				String pubCommand = "<command><startpublishing>w" + nbClients
						+ "w-C" + 0 + "-</startpublishing></command>";
				if (getScopeToDoContent.contains("noscoping")) {
					pubCommand = "<command><startpublishing><noscoping>w"
							+ nbClients + "w-C" + 0
							+ "-</noscoping></startpublishing></command>";
				}
				System.out.println(myURI + " : publish : " + pubId);
				System.out.flush();
				try {
					client.delegate().publish(pubId, ACK.NO, pubCommand);
					System.out.println("C" + myNumber + " sends " + pubCommand
							+ " to " + "C" + 0);
					System.out.flush();
				} catch (MuDEBSException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mudebs.client.api.Worker#terminate()
	 */
	@Override
	public void terminate() {
		if (client != null) {
			client.terminate();
		}
	}

	/**
	 * gets the number of clients from message content.
	 * 
	 * @param content
	 *            the content of the message.
	 * @return the total number of clients.
	 */
	private static int getNbOfClientsFromMsg(String content) {
		String nbString = "";
		Pattern patternClient = Pattern.compile("w(.*?)w");
		Matcher matcherClient = patternClient.matcher(content);
		if (matcherClient.find()) {
			nbString = matcherClient.group(1);
			Pattern intOnly = Pattern.compile("\\d+");
			Matcher makeMatch = intOnly.matcher(nbString);
			if (makeMatch.find()) {
				nbString = makeMatch.group();
			}
		}
		return Integer.parseInt(nbString);
	}

	/**
	 * gets the string identifier of the client from the message.
	 * 
	 * @param content
	 *            the content of the message.
	 * @return the identifier of the next client to subscribe to advertise.
	 */
	private static int getStringIdOfClientFromMsg(String content) {
		if (content == null) {
			return -1;
		}
		String publishingClient = "";
		Pattern patternClient = Pattern.compile("-(.*?)-");
		Matcher matcherClient = patternClient.matcher(content);
		if (matcherClient.find()) {
			publishingClient = matcherClient.group(1);
			Pattern intOnly = Pattern.compile("\\d+");
			Matcher makeMatch = intOnly.matcher(publishingClient);
			if (makeMatch.find()) {
				publishingClient = makeMatch.group();
			}
		}
		return Integer.parseInt(publishingClient);
	}

	/**
	 * gets the list of scopes in a list.
	 * 
	 * @return list of scopes known by access broker.
	 */
	public List<Scope> scopeList() {
		return scopesList;
	}

	/**
	 * gets the set of scopes sets.
	 * 
	 * @return set of sets of scopes.
	 */
	public Set<Set<Scope>> scopeSetsSet() {
		return scopeSetsSet;
	}

	/**
	 * states whether there exists already a scope in the set with the same
	 * diemension as the one of <tt>scopeToTest</tt>
	 * 
	 * @param scopesSet
	 *            the set of scopes.
	 * @param scopeToTest
	 *            the scope to test (this is the dimension that is searched
	 *            for).
	 * @return <tt>true</tt> if the dimension of the scope to test exists in the
	 *         set of scopes.
	 */
	public static boolean dimensionExistsInSet(Set<Scope> scopesSet,
			Scope scopeToTest) {
		if (scopesSet == null) {
			return false;
		} else {
			for (Scope scope : scopesSet) {
				if (scope.dimension().equals(scopeToTest.dimension())) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * returns of the possible sub sets of scopes such that two scopes in the
	 * same sub set are associated with two different dimensions.
	 * 
	 * @param scopesList
	 *            the list of scopes.
	 * @return non-empty sub sets of scopes.
	 */
	public static Set<Set<Scope>> getAllPossibleSubSetsOfScopes(
			List<Scope> scopesList) {
		Set<Set<Scope>> subSets = new HashSet<Set<Scope>>();
		subSets.add(new HashSet<Scope>());
		for (Scope scope : scopesList) {
			Set<Set<Scope>> tempClone = new HashSet<Set<Scope>>(subSets);
			for (Set<Scope> subset : tempClone) {
				Set<Scope> extended = new HashSet<Scope>(subset);
				if (!dimensionExistsInSet(extended, scope)) {
					extended.add(scope);
					subSets.add(extended);
				}
			}
		}
		subSets.remove(new HashSet<Set<Scope>>());
		return subSets;
	}
}
