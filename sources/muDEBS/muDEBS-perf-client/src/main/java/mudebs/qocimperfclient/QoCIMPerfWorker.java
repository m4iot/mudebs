/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2017 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan, Léon Lim and Pierrick Marie
Contributor(s): 
 */
package mudebs.qocimperfclient;

import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import mudebs.client.api.Consumer;
import mudebs.client.api.Worker;
import mudebs.common.MuDEBSException;
import mudebs.common.algorithms.ACK;
import mudebs.common.algorithms.OperationalMode;
import mudebs.common.algorithms.routing.ABACAttribute;
import mudebs.common.algorithms.routing.ABACInformation;
import mudebs.common.algorithms.routing.Publication;
import mudebs.common.algorithms.routing.PublicationMsgContent;
import mudebs.common.algorithms.routing.Subscription;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * This class is a dummy client, which is used for instance by the scripting
 * client. It references a client object.
 * 
 * @author Denis Conan
 * @author Léon Lim
 */
public class QoCIMPerfWorker implements Worker {
	/**
	 * the client.
	 */
	private QoCIMPerfClient client;
	/**
	 * the boolean that states information is displayed
	 */
	private boolean INFO = true;
	/**
	 * the URI of this worker.
	 */
	private String myURI = null;
	/**
	 * the number of this worker.
	 */
	private int myNumber = -1;
	/**
	 * the boolean that states the client has started subscribing.
	 */
	private boolean pubStarted = false;
	/**
	 * the boolean that states the client has started subscribing.
	 */
	private boolean subStarted = false;
	/**
	 * the boolean that states the client has ended subscribing.
	 */
	private boolean subEnded = false;
	/**
	 * delay factor between two advertisements.
	 */
	private final long TIME_BETWEEN_TWO_ADV = 200;
	/**
	 * delay factor between advertisement and publication.
	 */
	private final long TIME_BETWEEN_ADV_PUB = 200;
	/**
	 * default delay factor between two publications.
	 */
	private long time_between_two_pub = 200;
	/**
	 * delay factor between two subscriptions.
	 */
	private final long TIME_BETWEEN_TWO_SUB = 200;
	/**
	 * delay factor between subscriptions and publications.
	 */
	private final long TIME_BETWEEN_SUB_PUB = 5000;
	/**
	 * list advertisements identifiers.
	 */
	private List<String> perfAdvIds = new ArrayList<String>();
	/**
	 * the total number of advertisements (i.e publications).
	 */
	private int nbAdv = 1;
	/**
	 * the total number of subscriptions.
	 */
	private int nbSub = 1;
	/**
	 * the number of constraints.
	 */
	private int nbConstraints = -1;
	/**
	 * the number of indicators.
	 */
	private int nbIndicators = -1;
	/**
	 * information on the configuration.
	 */
	private boolean info_config_provided = false;
	/**
	 * the evaluation mode.
	 */
	private QoCIMEvaluationMode perfMode = null;
	/**
	 * the ABAC information.
	 */
	private ABACInformation abac = null;
	/**
	 * the policy that may contain 4 or more attributes ("purposes").
	 */
	private String policyWithSeveralAttribute = null;
	/**
	 * the number of attributes.
	 */
	private int nbAttributes = -1;
	/**
	 * the number of policies.
	 */
	private int nbPolicies = -1;
	/**
	 * the identifier of the advertisement with privacy with which context
	 * reports are published.
	 */
	private int idOfTheAvertisement = -1;
	/**
	 * the context report.
	 */
	private String contextReport = "";

	/**
	 * this filter must be changed.
	 */
	private String koPubSubPerfFilter = "function evaluate(doc) {"
			+ "importPackage(javax.xml.xpath);"
			+ "var xpath = XPathFactory.newInstance().newXPath();"
			+ "var n1 = xpath.evaluate('/performance', doc, XPathConstants.NODE);"
			+ "return (n1 == null);}";
	/**
	 * this filter must be changed.
	 */
	private String okPubSubPerfFilter = "function evaluate(doc) {"
			+ "importPackage(javax.xml.xpath);"
			+ "var xpath = XPathFactory.newInstance().newXPath();"
			+ "var n1 = xpath.evaluate('/performance', doc, XPathConstants.NODE);"
			+ "return (n1 != null);}";

	/**
	 * the consumer of publication messages.
	 */
	protected Consumer consumer = new Consumer() {
		@Override
		public void consume(PublicationMsgContent msg) {
			String content = msg.getContent();
			if (INFO) {
				if (content.length() > 84) {
					System.out.println(myURI + " : receives msg "
							+ content.substring(0, 42) + "..."
							+ content.substring(content.length() - 42));
				} else {
					System.out.println(myURI + " : receives msg " + content);
				}
				if (!info_config_provided) {
					info_config_provided = true;
					System.out.println(myURI + " : perfMode=" + perfMode);
					System.out.println(myURI + " : nbConstraints="
							+ nbConstraints);
					System.out.println(myURI + " : nbIndicators="
							+ nbIndicators);
					System.out.println(myURI + " : nbPolicies=" + nbPolicies);
					System.out.println(myURI
							+ " : nbAttributes="
							+ ((myNumber == 1) ? nbAttributes
									: nbAttributes + 3));
					System.out.flush();
				}
			}
			if (content.contains("startsubscribing")) {
				if (!subStarted && !subEnded) {
					if (Util.getStringIdOfClientFromMsg(content) == myNumber) {
						subStarted = true;
					}
				}
			}
			if (!pubStarted) {
				if (content.contains("startpublishing")) {
					// the identifiers of the two clients are C0 and C1, which
					// are the producer and the consumer, respectively.
					int publishingClientNumber = Util
							.getStringIdOfClientFromMsg(content);
					if (publishingClientNumber == myNumber) {
						pubStarted = true;
						System.out
								.println(myURI + " : receives msg " + content);
						System.out.flush();
						String stringId = "advertisement";
						int intId = 0;
						List<Publication> pubs = new ArrayList<Publication>();
						if (perfMode.equals(QoCIMEvaluationMode.XACML_F)) {
							// 1000 "false" policies
							if (nbPolicies > 0) {
								for (int k = -1; k >= (-nbPolicies); k--) {
									String xacmlpolicy = Constants.XACML_POLICY;
									xacmlpolicy = xacmlpolicy.replace("$ID$",
											String.valueOf(k));
									xacmlpolicy = xacmlpolicy.replace(
											"$INCOME$",
											"INCOME" + String.valueOf(k));
									String okAdvId = stringId + (k);
									try {
										client.delegate().advertise(okAdvId,
												OperationalMode.LOCAL, ACK.NO,
												okPubSubPerfFilter, null,
												xacmlpolicy);
									} catch (MuDEBSException e) {
										e.printStackTrace();
									}
									Util.displayTimeToWaitInMS(myURI, "adv",
											okAdvId, TIME_BETWEEN_TWO_ADV * 3);
								}
							}
							// the same policy advertised 1000 times
							for (int j = 0; j < nbAdv; j++) {
								String xacmlpolicy = Constants.XACML_POLICY;
								xacmlpolicy = xacmlpolicy.replace("$ID$",
										String.valueOf(idOfTheAvertisement));
								xacmlpolicy = xacmlpolicy
										.replace(
												"$INCOME$",
												"INCOME"
														+ String.valueOf(idOfTheAvertisement));
								String okAdvId = stringId + (j + 1);
								perfAdvIds.add(okAdvId);
								try {
									client.delegate().advertise(okAdvId,
											OperationalMode.LOCAL, ACK.NO,
											okPubSubPerfFilter, null,
											xacmlpolicy);
								} catch (MuDEBSException e) {
									e.printStackTrace();
								}
								Util.displayTimeToWaitInMS(myURI, "adv",
										okAdvId, TIME_BETWEEN_TWO_ADV * 3);
							}
							Util.displayTimeToWaitInMS(myURI, "wait",
									"before pub", TIME_BETWEEN_ADV_PUB * 10);
							for (int i = 0; i < perfAdvIds.size(); i++) {
								Publication pub = new Publication(
										perfAdvIds.get(i), 0, contextReport);
								try {
									client.delegate().publish(pub.getIdAdv(),
											ACK.NO, pub.getContent());
								} catch (MuDEBSException e) {
									e.printStackTrace();
								}
								Util.displayTimeToWaitInMS(myURI, "pub",
										perfAdvIds.get(i), time_between_two_pub);
							}
						} else {
							if (nbAdv > 0) {
								for (int j = 0; j < nbAdv; j++) {
									String aStringPolicy = null;
									if (perfMode
											.equals(QoCIMEvaluationMode.XACML_A)) {
										aStringPolicy = policyWithSeveralAttribute;
									}
									String okAdvId = stringId + ++intId;
									try {
										perfAdvIds.add(okAdvId);
										client.delegate().advertise(okAdvId,
												OperationalMode.LOCAL, ACK.NO,
												okPubSubPerfFilter, null,
												aStringPolicy);
									} catch (MuDEBSException e) {
										e.printStackTrace();
									}
									Util.displayTimeToWaitInMS(myURI, "adv",
											okAdvId, TIME_BETWEEN_TWO_ADV);
								}
							}
							for (String advId : perfAdvIds) {
								pubs.add(new Publication(advId, 0,
										contextReport));
							}
							System.out.println(myURI + " : start publishing "
									+ pubs.size() + " publications");
							for (Publication pub : pubs) {
								try {
									client.delegate().publish(pub.getIdAdv(),
											ACK.NO, pub.getContent());
								} catch (MuDEBSException e) {
									e.printStackTrace();
								}
								Util.displayTimeToWaitInMS(myURI, "pub",
										pub.getIdAdv(), time_between_two_pub);

							}
						}
					}
				}
			}
		}
	};

	/*
	 * (non-Javadoc)
	 * 
	 * @see mudebs.client.api.Worker#work()
	 */
	@Override
	public void work() {
		// only C1 executes the following code
		if (subStarted && !subEnded) {
			subEnded = true;
			String stringId = "subscription";
			List<Subscription> subs = new ArrayList<Subscription>();
			int ithSub = 0;
			// add nbSub - 1 false filters.
			if (nbSub > 1) {
				for (int j = 0; j < nbSub - 1; j++) {
					String koSubId = stringId + ++ithSub;
					Subscription sub = new Subscription(koSubId,
							koPubSubPerfFilter, null, abac);
					subs.add(sub);
				}
			}
			// add 1 true filter.
			System.out.println("abac=" + abac);
			String okSubId = stringId + ++ithSub;
			Subscription sub = new Subscription(okSubId, okPubSubPerfFilter,
					null, abac);
			subs.add(sub);
			System.out.println(myURI + " : start subscribing " + subs.size()
					+ " subscriptions");
			for (Subscription suscription : subs) {
				try {
					client.delegate().subscribe(suscription.getIdentifier(),
							OperationalMode.GLOBAL, ACK.NO,
							suscription.getRoutingFilter(),
							suscription.getPhi(),
							suscription.getAbacInformation());
				} catch (MuDEBSException e) {
					e.printStackTrace();
				}
				Util.displayTimeToWaitInMS(myURI, "sub",
						suscription.getIdentifier(), TIME_BETWEEN_TWO_SUB);
			}
			// C1 does not receive any command anymore
			try {
				client.delegate().unsubscribe(client.getSubCommandId(), ACK.NO);
			} catch (MuDEBSException e3) {
				e3.printStackTrace();
			}
			Util.displayTimeToWaitInMS(myURI, "unsub",
					client.getSubCommandId(), TIME_BETWEEN_TWO_SUB);
			try {
				client.delegate().unsubscribe(client.getPubCommandId(), ACK.NO);
			} catch (MuDEBSException e3) {
				e3.printStackTrace();
			}
			Util.displayTimeToWaitInMS(myURI, "unsub",
					client.getPubCommandId(), TIME_BETWEEN_SUB_PUB
							* (nbSub / 100));
			// C1 publishes command "startpublishing" (to C0)
			String pubId = client.getPubCommandId();
			try {
				client.delegate().advertise(pubId, OperationalMode.GLOBAL,
						ACK.NO, client.getSubPubFilterCommand(), null, null);
			} catch (MuDEBSException e) {
				e.printStackTrace();
			}
			Util.displayTimeToWaitInMS(myURI, "publish", pubId,
					TIME_BETWEEN_ADV_PUB);
			String pubCommand = "<command><startpublishing><noscoping>-" + "C"
					+ 0 + "-</noscoping></startpublishing></command>";
			try {
				client.delegate().publish(pubId, ACK.NO, pubCommand);
				System.out.println(myURI + " : sends " + pubCommand + " to "
						+ "C" + 0);
				System.out.flush();
			} catch (MuDEBSException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Constructor.
	 * 
	 * @param args
	 *            the command line arguments.
	 * @throws IOException
	 *             the exception thrown in case of problem.
	 */
	public QoCIMPerfWorker(String[] args) throws IOException {
		for (int i = 0; i < args.length; i = i + 2) {
			if (args[i].equalsIgnoreCase(mudebs.common.Constants.OPTION_URI)) {
				myURI = args[i + 1];
			}
			if (args[i].equalsIgnoreCase(Constants.OPTION_NB_CONSTRAINTS)) {
				nbConstraints = Integer.parseInt(args[i + 1]);
			}
			if (args[i].equalsIgnoreCase(Constants.OPTION_NB_INDICATORS)) {
				nbIndicators = Integer.parseInt(args[i + 1]);
			}
			if (args[i].equalsIgnoreCase(Constants.OPTION_TIME_BETWEEN_TWO_PUB)) {
				time_between_two_pub = Integer.parseInt(args[i + 1]);
			}
			if (args[i].equalsIgnoreCase(Constants.OPTION_NB_POLICIES)) {
				nbPolicies = Integer.parseInt(args[i + 1]);
			}
			if (args[i].equalsIgnoreCase(Constants.OPTION_NB_ATTRIBUTES)) {
				nbAttributes = Integer.parseInt(args[i + 1]);
			}
			if (args[i].equalsIgnoreCase(Constants.OPTION_ID_ADV_FOR_PUB)) {
				idOfTheAvertisement = Integer.parseInt(args[i + 1]);
			}
			if (args[i]
					.equalsIgnoreCase(Constants.OPTION_EVAL_PERF_FILTERS_MODE)) {
				if (args[i + 1]
						.equalsIgnoreCase(Constants.CONTEXT_BASED_CONSTRAINT)) {
					perfMode = QoCIMEvaluationMode.CBC;
				}
				if (args[i + 1]
						.equalsIgnoreCase(Constants.QOC_BASED_MULTI_CONSTRAINTS_CRITERION_CONSTRAINTS)) {
					perfMode = QoCIMEvaluationMode.QOCMCCC;
				}
				if (args[i + 1]
						.equalsIgnoreCase(Constants.QOC_BASED_MULTI_CONSTRAINTS_VALUE_CONSTRAINTS)) {
					perfMode = QoCIMEvaluationMode.QOCMCVC;
				}
				if (args[i + 1].equalsIgnoreCase(Constants.XACML_POLICY_F)) {
					perfMode = QoCIMEvaluationMode.XACML_F;
				}
				if (args[i + 1].equalsIgnoreCase(Constants.XACML_POLICY_A)) {
					perfMode = QoCIMEvaluationMode.XACML_A;
				}
				if (args[i + 1]
						.equalsIgnoreCase(Constants.QOC_BASED_MULTI_METADATA_CRITERION_CONSTRAINTS)) {
					perfMode = QoCIMEvaluationMode.QOCMMCC;
				}
				if (args[i + 1]
						.equalsIgnoreCase(Constants.QOC_BASED_MULTI_METADATA_VALUE_CONSTRAINTS)) {
					perfMode = QoCIMEvaluationMode.QOCMMVC;
				}
			}
			if (args[i].equalsIgnoreCase(Constants.OPTION_EVAL_PERF_NB_PUB)) {
				nbAdv = Integer.parseInt(args[i + 1]);
			}
			if (args[i].equalsIgnoreCase(Constants.OPTION_EVAL_PERF_NB_SUB)) {
				nbSub = Integer.parseInt(args[i + 1]);
			}
		}
		if ((myURI != null) && !myURI.equalsIgnoreCase("")) {
			String myStringId = "";
			Pattern myIdPattern = Pattern.compile("[0-9]+$");
			Matcher myIdMatcher = myIdPattern.matcher(myURI);
			if (myIdMatcher.find()) {
				myStringId = myIdMatcher.group();
			}
			myNumber = Integer.parseInt(myStringId);
		}
		if (perfMode != null) {
			switch (perfMode) {
			case CBC: {
				contextReport = Constants.CBC_BEGIN_REPORT;
				if (nbIndicators != -1) {
					contextReport += Constants.CBC_PRECISION_INDICATOR;
				}
				contextReport += Constants.CBC_END_REPORT;
				koPubSubPerfFilter = Constants.CBC_NO_MATCH_FILTER;
				okPubSubPerfFilter = Constants.CBC_FILTER;
			}
				break;
			case QOCMCCC: {
				contextReport = Constants.QOCMCCC_REPORT;
				okPubSubPerfFilter = Constants.QOCMCCC_BEGIN_FILTER;
				for (int i = nbConstraints; i > 0; i--) {
					String constraint = "";
					constraint = Constants.QOCMCCC_CONSTRAINT.replace(
							"$VALUE$", String.valueOf(i));
					okPubSubPerfFilter += constraint;
				}
				okPubSubPerfFilter += Constants.QOCMCCC_END_FILTER;
				koPubSubPerfFilter = okPubSubPerfFilter
						.replace("true", "false");
			}
				break;
			case QOCMCVC: {
				contextReport = Constants.QOCMCVC_REPORT;
				okPubSubPerfFilter = Constants.QOCMCVC_BEGIN_FILTER;
				for (int i = nbConstraints; i > 0; i--) {
					String constraint = "";
					constraint = Constants.QOCMCVC_CONSTRAINT.replace(
							"$VALUE$", String.valueOf(i));
					okPubSubPerfFilter += constraint;
				}
				okPubSubPerfFilter += Constants.QOCMCVC_END_FILTER;
				koPubSubPerfFilter = okPubSubPerfFilter
						.replace("true", "false");
			}
				break;
			case QOCMMCC: {
				Document doc = null;
				DocumentBuilderFactory factory = null;
				DocumentBuilder builder = null;
				InputSource is = null;
				StringBuffer buffer = null;
				String indicator = null;
				buffer = new StringBuffer();
				buffer.append(Constants.QOCMMCC_BEGIN_REPORT);
				for (int i = 0; i < nbIndicators; i++) {
					indicator = Constants.QOCMMCC_INDICATOR.replace("$VALUE$",
							String.valueOf(i));
					buffer.append(indicator);
				}
				buffer.append(Constants.QOCMMCC_PRECISION_INDICATOR);
				buffer.append(Constants.QOCMMCC_END_REPORT);
				factory = DocumentBuilderFactory.newInstance();
				try {
					builder = factory.newDocumentBuilder();
				} catch (ParserConfigurationException e) {
					e.printStackTrace();
				}
				is = new InputSource();
				is.setCharacterStream(new StringReader(buffer.toString()));
				try {
					doc = builder.parse(is);
				} catch (SAXException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				contextReport = Util.getStringFromDoc(doc);
				okPubSubPerfFilter = Constants.QOCMMCC_FILTER;
				koPubSubPerfFilter = okPubSubPerfFilter
						.replace("true", "false");
			}
				break;
			case QOCMMVC: {
				Document doc = null;
				DocumentBuilderFactory factory = null;
				DocumentBuilder builder = null;
				InputSource is = null;
				StringBuffer buffer = null;
				String indicator = null;
				buffer = new StringBuffer();
				buffer.append(Constants.QOCMMVC_BEGIN_REPORT);
				for (int i = 0; i < nbIndicators; i++) {
					indicator = Constants.QOCMMVC_INDICATOR.replace("$VALUE$",
							String.valueOf(i));
					buffer.append(indicator);
				}
				buffer.append(Constants.QOCMMVC_PRECISION_INDICATOR);
				buffer.append(Constants.QOCMMVC_END_REPORT);
				factory = DocumentBuilderFactory.newInstance();
				try {
					builder = factory.newDocumentBuilder();
				} catch (ParserConfigurationException e) {
					e.printStackTrace();
				}
				is = new InputSource();
				is.setCharacterStream(new StringReader(buffer.toString()));
				try {
					doc = builder.parse(is);
				} catch (SAXException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				contextReport = Util.getStringFromDoc(doc);
				okPubSubPerfFilter = Constants.QOCMMVC_FILTER;
				koPubSubPerfFilter = okPubSubPerfFilter
						.replace("true", "false");
			}
				break;
			case XACML_F: {
				contextReport = Constants.CBC_BEGIN_REPORT;
				contextReport += Constants.CBC_END_REPORT;
				okPubSubPerfFilter = Constants.CBC_FILTER;
				koPubSubPerfFilter = Constants.CBC_NO_MATCH_FILTER;
				ABACAttribute abacAtt1 = new ABACAttribute("categoryperf-1", "idperf-1",
						"INCOME" + idOfTheAvertisement,
						"http://www.w3.org/2001/XMLSchema#string");
				ABACAttribute abacAtt2 = new ABACAttribute("categoryperf-2", "idperf-2",
						"PersonalUse",
						"http://www.w3.org/2001/XMLSchema#string");
				ABACAttribute abacAtt3 = new ABACAttribute("categoryperf-3", "idperf-3",
						"abcd",
						"http://www.w3.org/2001/XMLSchema#string");
				ABACAttribute abacAtt4 = new ABACAttribute("categoryperf-4", "idperf-4",
						"access",
						"http://www.w3.org/2001/XMLSchema#string");
				ArrayList<ABACAttribute> abacAttlist = new ArrayList<ABACAttribute>();
				abacAttlist.add(abacAtt1);
				abacAttlist.add(abacAtt2);
				abacAttlist.add(abacAtt3);
				abacAttlist.add(abacAtt4);
				abac = new ABACInformation(abacAttlist);
			}
				break;
			case XACML_A: {
				contextReport = Constants.CBC_BEGIN_REPORT;
				contextReport += Constants.CBC_END_REPORT;
				okPubSubPerfFilter = Constants.CBC_FILTER;
				koPubSubPerfFilter = Constants.CBC_NO_MATCH_FILTER;
				String policyBegin = Constants.XACML_A_BEGIN;
				String policyAttribute = Constants.XACML_A_MID;
				String policyEnd = Constants.XACML_A_END;
				policyWithSeveralAttribute = policyBegin;
				for (int i = 1; i <= nbAttributes; i++) {
					policyWithSeveralAttribute += policyAttribute.replace(
							"$id$", String.valueOf(i));
				}
				policyWithSeveralAttribute += policyEnd;
				ArrayList<ABACAttribute> abacAttlist = new ArrayList<ABACAttribute>();
				for (int i = 1; i <= nbAttributes; i++) {
					ABACAttribute abacAtt = new ABACAttribute("category" + i,
							"id" + i, "INCOME" + i,
							"http://www.w3.org/2001/XMLSchema#string");
					abacAttlist.add(abacAtt);
				}
				abac = new ABACInformation(abacAttlist);
			}
				break;
			default:
				break;
			}
		}
	}

	public static String readFile(String path, Charset encoding)
			throws IOException {
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, encoding);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mudebs.client.api.Worker#initialise(java.lang.String[])
	 */
	@Override
	public void initialise(String[] args) throws Exception {
		client = new QoCIMPerfClient(args, consumer, this);
		client.start();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mudebs.client.api.Worker#terminate()
	 */
	@Override
	public void terminate() {
		if (client != null) {
			client.terminate();
		}
	}
}