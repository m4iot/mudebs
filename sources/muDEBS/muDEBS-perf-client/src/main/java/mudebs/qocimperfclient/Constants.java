/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Léon Lim
Contributor(s): 
 */
package mudebs.qocimperfclient;

/**
 * This class contains the attributes that are constants of the QoCIMPerfClient.
 * 
 * @author Léon Lim
 */
public class Constants {
	/**
	 * string option for specifying the identifier of the advertisement with
	 * privacy with which context reports are published.
	 */
	public final static String OPTION_ID_ADV_FOR_PUB = "--id_adv_privacy";
	/**
	 * string option for specifying the number of policies used the performance
	 * evaluation of XACML-based filters. Each filter contains one attribute.
	 */
	public final static String OPTION_NB_POLICIES = "--nb_policies";
	/**
	 * string option for specifying the number of policies used the performance
	 * evaluation of XACML-based filter that contains several attributes..
	 */
	public final static String OPTION_NB_ATTRIBUTES = "--nb_attributes";
	/**
	 * string option for specifying the number of constraints used in the
	 * performance evaluation of QoC-based filters with multi-constraints.
	 */
	public final static String OPTION_NB_CONSTRAINTS = "--nb_constraints";
	/**
	 * string option for specifying the number of indicators used in the
	 * performance evaluation of QoC-based routing filters with multi-metadata.
	 */
	public final static String OPTION_NB_INDICATORS = "--nb_indicators";
	/**
	 * attribute name for analysing the context-based filters with constraints.
	 */
	public final static String CONTEXT_BASED_CONSTRAINT = "CBC";
	/**
	 * attribute name for analysing the QoC-based multi-constraints with
	 * crierion constraints.
	 */
	public final static String QOC_BASED_MULTI_CONSTRAINTS_CRITERION_CONSTRAINTS = "QOCMCCC";
	/**
	 * attribute name for analysing the QoC-based multi-constraints with value
	 * constraints.
	 */
	public final static String QOC_BASED_MULTI_CONSTRAINTS_VALUE_CONSTRAINTS = "QOCMCVC";
	/**
	 * attribute name for analysing the QoC-based multi-metadata with criterion
	 * constraints.
	 */
	public final static String QOC_BASED_MULTI_METADATA_CRITERION_CONSTRAINTS = "QOCMMCC";
	/**
	 * attribute name for analysing the QoC-based multi-metadata with value
	 * constraints.
	 */
	public final static String QOC_BASED_MULTI_METADATA_VALUE_CONSTRAINTS = "QOCMMVC";
	/**
	 * string option for specifying the mode of performance evaluation of
	 * filters each of which contains one attribute.
	 */
	public final static String XACML_POLICY_F = "XACML_F";
	/**
	 * string option for specifying the mode of performance evaluation of filter
	 * containing several attributes.
	 */
	public final static String XACML_POLICY_A = "XACML_A";
	/**
	 * string option for specifying the mode of performance evaluation of
	 * filters.
	 */
	public final static String OPTION_EVAL_PERF_FILTERS_MODE = "--eval_perf_filters_mode";
	/**
	 * string option for specifying the number of publication in the performance
	 * evaluation of filters.
	 */
	public final static String OPTION_EVAL_PERF_NB_PUB = "--eval_perf_nb_pub";
	/**
	 * string option for specifying the number of subscriptions in the
	 * performance evaluation of filters.
	 */
	public final static String OPTION_EVAL_PERF_NB_SUB = "--eval_perf_nb_sub";
	/**
	 * strng option for specifying the delay between tow publications.
	 */
	public final static String OPTION_TIME_BETWEEN_TWO_PUB = "--time_between_two_pub";
	/**
	 * context-based filter with constraints.
	 */
	public static final String CBC_FILTER = "function evaluate(doc) {"
			+ "importPackage(javax.xml.xpath);"
			+ "var xpath = XPathFactory.newInstance().newXPath(); "
			+ "if( xpath.evaluate(\"//observable[uri='mucontext://localhost:3000/Something' "
			+ "and entity[uri='mucontext://localhost:3000/user/Somebody']]\", "
			+ "doc, XPathConstants.NODESET).length == 0) {return false;} "
			+ "return true;}";
	/**
	 * context-based filter with constraints that does not match any context
	 * report.
	 */
	public static final String CBC_NO_MATCH_FILTER = "function evaluate(doc) {"
			+ "importPackage(javax.xml.xpath);"
			+ "var xpath = XPathFactory.newInstance().newXPath(); "
			+ "if( xpath.evaluate(\"//observable[uri='mucontext://localhost:3000/Something' "
			+ "and entity[uri='mucontext://localhost:3000/user/NO_MATCH']]\", "
			+ "doc, XPathConstants.NODESET).length == 0) {return false;} "
			+ "return true;}";
	/**
	 * the begining of context report that match the context-based filter with
	 * constraints
	 */
	public static final String CBC_BEGIN_REPORT = "<?xml version=\"1.0\" encoding=\"UTF-8\" "
			+ "standalone=\"yes\"?><contextReport>"
			+ "<id>report</id><observations><observation>"
			+ "<id>Observation</id><value xsi:type=\"xs:int\" "
			+ "xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" "
			+ "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">2500</value><observable>"
			+ "<name>Something</name><uri>mucontext://localhost:3000/Something</uri><entity>"
			+ "<name>Somebody</name><uri>mucontext://localhost:3000/user/Somebody</uri>"
			+ "<relations/></entity><observations/></observable>"
			+ "<date>2014-07-16T12:28:13.443+02:00</date>";
	/**
	 * the end of context report that match the context-based filter with
	 * constraints
	 */
	public static final String CBC_END_REPORT = "</observation></observations>"
			+ "<multiscalabilityreports/><reports/></contextReport>";
	/**
	 * precision indicator that can be added to the context report
	 */
	public static final String CBC_PRECISION_INDICATOR = "<qocindicator id=\"10\" "
			+ "name=\"PrecisionIndicator\"><qoccriterion id=\"[10.1][10.2]\" "
			+ "name=\"PrecisionCriterion\"><qocmetricdefinition compositeDefinition=\"\" "
			+ "direction=\"UPPER\" id=\"10.1\" isInvariant=\"false\" maxValue=\"100\" "
			+ "values=\"0\" minValue=\"0\" primitiveDefinition=\"\" "
			+ "providerUri=\"//sensor\" unit=\"%\" name=\"PercentPrecisionDefinition\">"
			+ "<description informalDescription=\"This is an informal description of the "
			+ "precision criterion.\" name=\"PrecisionDescription\">"
			+ "<keywords>sensor</keywords><keywords>efficiency</keywords>"
			+ "</description></qocmetricdefinition></qoccriterion>"
			+ "<qocmetricvalue creationDate=\"2014-07-16T12:28:13.440+02:00\" "
			+ "definition=\"10.1\" id=\"0\" modificationDate=\"2014-07-16T12:28:13.440+02:00\" "
			+ "value=\"50\" name=\"PercentPrecisionMetricValue\"/></qocindicator>";
	/**
	 * the context report that matches QoC-based multi-constraints (QOCMCCC)
	 * filters with criterion constraints.
	 */
	public static final String QOCMCCC_REPORT = "<?xml version=\"1.0\" encoding=\"UTF-8\" "
			+ "standalone=\"yes\"?><contextReport><id>report</id><observations><observation>"
			+ "<id>Observation</id><value xsi:type=\"xs:int\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" "
			+ "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">2500</value>"
			+ "<observable><name>Something</name><uri>mucontext://localhost:3000/Something</uri>"
			+ "<entity><name>Somebody</name><uri>mucontext://localhost:3000/user/Somebody</uri>"
			+ "<relations/></entity><observations/></observable><date>2014-07-16T12:28:13.443+02:00</date>"
			+ "<qocindicator id=\"1\" name=\"QoCIndicator\"><qoccriterion id=\"1\" "
			+ "name=\"QoCCriterion\"><qocmetricdefinition compositeDefinition=\"\" direction=\"UPPER\" "
			+ "id=\"1\" isInvariant=\"false\" maxValue=\"100\" values=\"0\" minValue=\"0\" "
			+ "primitiveDefinition=\"\" providerUri=\"//sensor\" unit=\"%\" name=\"QoCMetricDefinition\">"
			+ "<description informalDescription=\"This is an informal description of the criterion.\" "
			+ "name=\"QoCDescription\"><keywords>sensor</keywords><keywords>efficiency</keywords></description>"
			+ "</qocmetricdefinition></qoccriterion><qocmetricvalue creationDate=\"2014-07-16T12:28:13.440+02:00\" "
			+ "definition=\"1\" id=\"0\" modificationDate=\"2014-07-16T12:28:13.440+02:00\" value=\"50\" "
			+ "name=\"QoCMetricValue\"/></qocindicator></observation></observations><multiscalabilityreports/>"
			+ "<reports/></contextReport>";

	public static final String QOCMCCC_BEGIN_FILTER = "function evaluate(doc) {importPackage(javax.xml.xpath);"
			+ "var xpath = XPathFactory.newInstance().newXPath();";

	public static final String QOCMCCC_CONSTRAINT = "if( xpath.evaluate(\"//qocindicator[@id<='$VALUE$' "
			+ "and qoccriterion[@id<='$VALUE$']/qocmetricdefinition[@id<='$VALUE$']]\", "
			+ "doc, XPathConstants.NODESET).length == 0) {return false;}";

	public static final String QOCMCCC_END_FILTER = "return true;}";

	/**
	 * the context report that matches QoC-based multi-constraints (QOCMCVC)
	 * filters with value constraints.
	 */
	public static final String QOCMCVC_REPORT = "<?xml version=\"1.0\" encoding=\"UTF-8\" "
			+ "standalone=\"yes\"?><contextReport><id>report</id><observations><observation>"
			+ "<id>Observation</id><value xsi:type=\"xs:int\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" "
			+ "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">2500</value><observable>"
			+ "<name>Something</name>"
			+ "<uri>mucontext://localhost:3000/Something</uri><entity><name>Somebody</name>"
			+ "<uri>mucontext://localhost:3000/user/Somebody</uri><relations/></entity><observations/>"
			+ "</observable><date>2014-07-16T12:28:13.443+02:00</date>"
			+ "<qocindicator id=\"1\" name=\"QoCIndicator\"><qoccriterion id=\"1\" "
			+ "name=\"QoCCriterion\"><qocmetricdefinition compositeDefinition=\"\" "
			+ "direction=\"UPPER\" id=\"1\" isInvariant=\"false\" maxValue=\"100\" "
			+ "values=\"0\" minValue=\"0\" primitiveDefinition=\"\" providerUri=\"//sensor\" "
			+ "unit=\"%\" name=\"QoCMetricDefinition\"><description informalDescription=\"This is an "
			+ "informal description of the criterion.\" name=\"QoCDescription\"><keywords>sensor"
			+ "</keywords><keywords>efficiency</keywords></description></qocmetricdefinition>"
			+ "</qoccriterion><qocmetricvalue creationDate=\"2014-07-16T12:28:13.440+02:00\" "
			+ "definition=\"1\" id=\"0\" modificationDate=\"2014-07-16T12:28:13.440+02:00\" "
			+ "value=\"1\" name=\"QoCMetricValue\"/></qocindicator></observation></observations>"
			+ "<multiscalabilityreports/><reports/></contextReport>";

	public static final String QOCMCVC_BEGIN_FILTER = "function evaluate(doc) {"
			+ "importPackage(javax.xml.xpath);	"
			+ "var xpath = XPathFactory.newInstance().newXPath();";

	public static final String QOCMCVC_CONSTRAINT = "if( xpath.evaluate(\"//qocindicator[@id<='$VALUE$' "
			+ "and qoccriterion[@id<='$VALUE$']/qocmetricdefinition[@id<='$VALUE$'] and "
			+ "qocmetricvalue[@value<='$VALUE$']]\", doc, XPathConstants.NODESET).length == 0)"
			+ "{ return false; }";

	public static final String QOCMCVC_END_FILTER = "return true;}";

	/**
	 * the context report that matches QoC-based multi-metadata (QOCMMCC)
	 * filters with criterion constraints.
	 */
	public static final String QOCMMCC_BEGIN_REPORT = "<?xml version=\"1.0\" encoding=\"UTF-8\" "
			+ "standalone=\"yes\"?><contextReport><id>report</id><observations>"
			+ "<observation><id>Observation</id><value xsi:type=\"xs:int\" "
			+ "xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" "
			+ "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">2500</value>"
			+ "<observable><name>Something</name><uri>mucontext://localhost:3000/Something</uri>"
			+ "<entity><name>Somebody</name><uri>mucontext://localhost:3000/user/Somebody</uri>"
			+ "<relations/></entity><observations/>"
			+ "</observable><date>2014-07-16T12:28:13.443+02:00</date>";
	public static final String QOCMMCC_END_REPORT = "</observation></observations>"
			+ "<multiscalabilityreports/><reports/></contextReport>";
	public static final String QOCMMCC_INDICATOR = "<qocindicator id=\"$VALUE$\" "
			+ "name=\"QoCIndicator\"><qoccriterion id=\"[$VALUE$]\" "
			+ "name=\"QoCCriterion\"><qocmetricdefinition "
			+ "compositeDefinition=\"\" direction=\"LOWER\" id=\"$VALUE$\" "
			+ "isInvariant=\"false\" maxValue=\"60\" "
			+ "values=\"0\" minValue=\"0\" primitiveDefinition=\"\" "
			+ "providerUri=\"//sensor\" unit=\"second\" "
			+ "name=\"QoCMetricDefinition\"><description "
			+ "informalDescription=\"This is an informal description of the "
			+ "criterion.\" name=\"QoCDescription\"><keywords>time</keywords>"
			+ "<keywords>measurement</keywords><keywords>interval</keywords>"
			+ "</description></qocmetricdefinition></qoccriterion><qocmetricvalue "
			+ "creationDate=\"2014-07-16T14:00:54.133+02:00\" definition=\"15.1\" id=\"0\" "
			+ "modificationDate=\"2014-07-16T14:00:54.133+02:00\" value=\"1\" "
			+ "name=\"QoCMetricValue\"/></qocindicator>";
	public static final String QOCMMCC_PRECISION_INDICATOR = "<qocindicator "
			+ "id=\"10\" name=\"PrecisionIndicator\"><qoccriterion id=\"[10.1][10.2]\" "
			+ "name=\"PrecisionCriterion\"><qocmetricdefinition compositeDefinition=\"\" "
			+ "direction=\"UPPER\" id=\"10.1\" isInvariant=\"false\" maxValue=\"100\" "
			+ "values=\"0\" minValue=\"0\" primitiveDefinition=\"\" providerUri=\"//sensor\" "
			+ "unit=\"%\" name=\"PercentPrecisionDefinition\"><description "
			+ "informalDescription=\"This is an informal description of the precision "
			+ "criterion.\" name=\"PrecisionDescription\"><keywords>sensor</keywords>"
			+ "<keywords>efficiency</keywords></description></qocmetricdefinition>"
			+ "</qoccriterion><qocmetricvalue creationDate=\"2014-07-16T12:28:13.440+02:00\" "
			+ "definition=\"10.1\" id=\"0\" modificationDate=\"2014-07-16T12:28:13.440+02:00\" "
			+ "value=\"50\" name=\"PercentPrecisionMetricValue\"/></qocindicator>";
	public static final String QOCMMCC_FILTER = "function evaluate(doc) "
			+ "{importPackage(javax.xml.xpath); "
			+ "var xpath = XPathFactory.newInstance().newXPath(); "
			+ "if( xpath.evaluate(\"//qocindicator[@id='10' and "
			+ "qoccriterion[@id='[10.1][10.2]']/qocmetricdefinition[@id='10.1']]\", doc, "
			+ "XPathConstants.NODESET).length == 0) { return false; } return true;}";

	/**
	 * the context report that matches QoC-based multi-metadata (QOCMMVC)
	 * filters with value constraints.
	 */
	public static final String QOCMMVC_BEGIN_REPORT = "<?xml version=\"1.0\" "
			+ "encoding=\"UTF-8\" standalone=\"yes\"?><contextReport><id>report"
			+ "</id><observations><observation><id>Observation</id>"
			+ "<value xsi:type=\"xs:int\" "
			+ "xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" "
			+ "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">2500</value>"
			+ "<observable><name>Something</name>"
			+ "<uri>mucontext://localhost:3000/Something</uri><entity>"
			+ "<name>Somebody</name><uri>mucontext://localhost:3000/user/Somebody"
			+ "</uri><relations/></entity><observations/></observable>"
			+ "<date>2014-07-16T12:28:13.443+02:00</date>";
	public static final String QOCMMVC_END_REPORT = "</observation>"
			+ "</observations><multiscalabilityreports/><reports/></contextReport>";
	public static final String QOCMMVC_INDICATOR = "<qocindicator id=\"$VALUE$\" "
			+ "name=\"QoCIndicator\"><qoccriterion id=\"[$VALUE$]\" "
			+ "name=\"QoCCriterion\"><qocmetricdefinition "
			+ "compositeDefinition=\"\" direction=\"LOWER\" "
			+ "id=\"$VALUE$\" isInvariant=\"false\" "
			+ "maxValue=\"60\" values=\"0\" minValue=\"0\" "
			+ "primitiveDefinition=\"\" providerUri=\"//sensor\" "
			+ "unit=\"second\" name=\"QoCMetricDefinition\">"
			+ "<description informalDescription=\"This is an informal "
			+ "description of the criterion.\" name=\"QoCDescription\">"
			+ "<keywords>time</keywords><keywords>measurement</keywords>"
			+ "<keywords>interval</keywords></description></qocmetricdefinition>"
			+ "</qoccriterion><qocmetricvalue "
			+ "creationDate=\"2014-07-16T14:00:54.133+02:00\" "
			+ "definition=\"15.1\" id=\"0\" "
			+ "modificationDate=\"2014-07-16T14:00:54.133+02:00\" "
			+ "value=\"1\" name=\"QoCMetricValue\"/></qocindicator>";
	public static final String QOCMMVC_PRECISION_INDICATOR = "<qocindicator "
			+ "id=\"10\" name=\"PrecisionIndicator\"><qoccriterion "
			+ "id=\"[10.1][10.2]\" name=\"PrecisionCriterion\">"
			+ "<qocmetricdefinition compositeDefinition=\"\" "
			+ "direction=\"UPPER\" id=\"10.1\" isInvariant=\"false\" "
			+ "maxValue=\"100\" values=\"0\" minValue=\"0\" "
			+ "primitiveDefinition=\"\" providerUri=\"//sensor\" "
			+ "unit=\"%\" name=\"PercentPrecisionDefinition\">"
			+ "<description informalDescription=\"This is an informal "
			+ "description of the precision criterion.\" "
			+ "name=\"PrecisionDescription\"><keywords>sensor</keywords>"
			+ "<keywords>efficiency</keywords></description>"
			+ "</qocmetricdefinition></qoccriterion><qocmetricvalue "
			+ "creationDate=\"2014-07-16T12:28:13.440+02:00\" "
			+ "definition=\"10.1\" id=\"0\" "
			+ "modificationDate=\"2014-07-16T12:28:13.440+02:00\" "
			+ "value=\"50\" name=\"PercentPrecisionMetricValue\"/>"
			+ "</qocindicator>";
	public static final String QOCMMVC_FILTER = "function evaluate(doc) "
			+ "{importPackage(javax.xml.xpath);	"
			+ "var xpath = XPathFactory.newInstance().newXPath(); "
			+ "if( xpath.evaluate(\"//qocindicator[@id='10' and "
			+ "qoccriterion[@id='[10.1][10.2]']/qocmetricdefinition[@id='10.1'] "
			+ "and qocmetricvalue[@value='50']]\", doc, "
			+ "XPathConstants.NODESET).length == 0) "
			+ "{ return false; } return true;}";

	/**
	 * a simple policy.
	 */
	public static final String XACML_POLICY = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
			+ "<Policy xmlns=\"urn:oasis:names:tc:xacml:3.0:core:schema:wd-17\" "
			+ "xmlns:xacml =\"urn:oasis:names:tc:xacml:3.0:core:schema:wd-17\" "
			+ "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" "
			+ "xsi:schemaLocation=\"urn:oasis:names:tc:xacml:3.0:core:schema:wd-17 "
			+ "http://docs.oasis-open.org/xacml/3.0/xacml-core-v3-schema-wd-17.xsd\" "
			+ "xmlns:md=\"http:www.med.example.com/schemas/record.xsd\" "
			+ "PolicyId=\"urn:oasis:names:tc:xacml:3.0:example:simple:policyid:$ID$\" "
			+ "Version=\"1.0\" "
			+ "RuleCombiningAlgId=\"urn:oasis:names:tc:xacml:3.0:rule-combining-algorithm:permit-overrides\">"
			+ "<Description>Test policy</Description><Target/>"
			+ "<Rule RuleId=\"urn:oasis:names:tc:xacml:3.0:example:simple:ruleid:1\"  "
			+ "Effect=\"Permit\"><Description>A simple policy defined by one attribute</Description>"
			+ "<Target>"
			// first attribute ("categoryperf-1","INCOMEX","idperf-1")
			+ "<AnyOf><AllOf><Match MatchId=\"urn:oasis:names:tc:xacml:1.0:function:string-equal\">"
			+ "<AttributeValue DataType=\"http://www.w3.org/2001/XMLSchema#string\">$INCOME$"
			+ "</AttributeValue><AttributeDesignator MustBePresent=\"false\" "
			+ "Category=\"categoryperf-1\" AttributeId=\"mudebs://localhost:port/xacml/attribute-id/idperf-1\" "
			+ "DataType=\"http://www.w3.org/2001/XMLSchema#string\"/></Match></AllOf>"
			+ "</AnyOf>"
			// first attribute ("categoryperf-2","PersonalUse","idperf-2")
			+ "<AnyOf><AllOf><Match MatchId=\"urn:oasis:names:tc:xacml:1.0:function:string-equal\">"
			+ "<AttributeValue DataType=\"http://www.w3.org/2001/XMLSchema#string\">PersonalUse"
			+ "</AttributeValue><AttributeDesignator MustBePresent=\"false\" "
			+ "Category=\"categoryperf-2\" AttributeId=\"mudebs://localhost:port/xacml/attribute-id/idperf-2\" "
			+ "DataType=\"http://www.w3.org/2001/XMLSchema#string\"/></Match></AllOf>"
			+ "</AnyOf>"
			// first attribute ("categoryperf-3","abcd","idperf-2")
			+ "<AnyOf><AllOf><Match MatchId=\"urn:oasis:names:tc:xacml:1.0:function:string-equal\">"
			+ "<AttributeValue DataType=\"http://www.w3.org/2001/XMLSchema#string\">abcd"
			+ "</AttributeValue><AttributeDesignator MustBePresent=\"false\" "
			+ "Category=\"categoryperf-3\" AttributeId=\"mudebs://localhost:port/xacml/attribute-id/idperf-3\" "
			+ "DataType=\"http://www.w3.org/2001/XMLSchema#string\"/></Match></AllOf>"
			+ "</AnyOf>"
			// first attribute ("categoryperf-4","access","idperf-3")
			+ "<AnyOf><AllOf><Match MatchId=\"urn:oasis:names:tc:xacml:1.0:function:string-equal\">"
			+ "<AttributeValue DataType=\"http://www.w3.org/2001/XMLSchema#string\">access"
			+ "</AttributeValue><AttributeDesignator MustBePresent=\"false\" "
			+ "Category=\"categoryperf-4\" AttributeId=\"mudebs://localhost:port/xacml/attribute-id/idperf-4\" "
			+ "DataType=\"http://www.w3.org/2001/XMLSchema#string\"/></Match></AllOf>"
			+ "</AnyOf>"
			+ "</Target>"
			+ "</Rule><Rule RuleId=\"urn:oasis:names:tc:xacml:3.0:example:simple:ruleid:2:default\" "
			+ "Effect=\"Deny\"><Target/></Rule></Policy>";

	public static final String XACML_A_BEGIN = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
			+ "<Policy xmlns=\"urn:oasis:names:tc:xacml:3.0:core:schema:wd-17\" "
			+ "xmlns:xacml =\"urn:oasis:names:tc:xacml:3.0:core:schema:wd-17\" "
			+ "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" "
			+ "xsi:schemaLocation=\"urn:oasis:names:tc:xacml:3.0:core:schema:wd-17 "
			+ "http://docs.oasis-open.org/xacml/3.0/xacml-core-v3-schema-wd-17.xsd\" "
			+ "xmlns:md=\"http:www.med.example.com/schemas/record.xsd\" "
			+ "PolicyId=\"urn:oasis:names:tc:xacml:3.0:example:simple:policyid:111\" "
			+ "Version=\"1.0\" "
			+ "RuleCombiningAlgId=\"urn:oasis:names:tc:xacml:3.0:rule-combining-algorithm:permit-overrides\">"
			+ "<Description>Test policy</Description><Target/>"
			+ "<Rule RuleId=\"urn:oasis:names:tc:xacml:3.0:example:simple:ruleid:1\"  "
			+ "Effect=\"Permit\"><Description>A simple policy defined by one attribute</Description>"
			+ "<Target>";

	/**
	 * a simple policy divided into three parts.
	 */
	public static final String XACML_A_MID =
	// first attribute ("categoryX","INCOMEX","idX")
	"<AnyOf><AllOf><Match MatchId=\"urn:oasis:names:tc:xacml:1.0:function:string-equal\">"
			+ "<AttributeValue DataType=\"http://www.w3.org/2001/XMLSchema#string\">INCOME$id$"
			+ "</AttributeValue><AttributeDesignator MustBePresent=\"false\" "
			+ "Category=\"category$id$\" AttributeId=\"mudebs://localhost:port/xacml/attribute-id/id$id$\" "
			+ "DataType=\"http://www.w3.org/2001/XMLSchema#string\"/></Match></AllOf>"
			+ "</AnyOf>";

	public static final String XACML_A_END = "</Target>"
			+ "</Rule><Rule RuleId=\"urn:oasis:names:tc:xacml:3.0:example:simple:ruleid:2:default\" "
			+ "Effect=\"Deny\"><Target/></Rule></Policy>";
}