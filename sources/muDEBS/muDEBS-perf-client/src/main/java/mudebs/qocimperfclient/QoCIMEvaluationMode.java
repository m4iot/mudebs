/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Léon Lim
Contributor(s):
 */
package mudebs.qocimperfclient;

/**
 * This enumeration defines the operational mode of a call: global or local.
 * 
 * @author Léon Lim
 * 
 */
public enum QoCIMEvaluationMode {
	/**
	 * the context-based filters with constraints (CBC) is evaluated.
	 */
	CBC,
	/**
	 * the QoC-based multi-constraints filters with criterion constraints
	 * (QOCMCCC) is evaluated.
	 */
	QOCMCCC,
	/**
	 * the QoC-based multi-constraints filters with value constraints (QOCMCVC)
	 * is evaluated
	 */
	QOCMCVC,
	/**
	 * the QoC-based multi-metadata filters with criterion constraints (QOCMMCC)
	 * is evaluated.
	 */
	QOCMMCC,
	/**
	 * the QoC-based multi-metadata filters with value constraints (QOCMMVC) is
	 * evaluateD
	 */
	QOCMMVC,
	/**
	 * the context-based filters with XACML policies containing one attribute is
	 * evaluated.
	 */
	XACML_F,
	/**
	 * the context-based filter with XACML policy containing several attributes.
	 */
	XACML_A
}