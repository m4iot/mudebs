/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2017 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Léon Lim
Contributor(s): Denis Conan
 */
package mudebs.qocimperfclient;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.w3c.dom.Document;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;

/**
 * @author Léon Lim
 * @author Denis Conan
 *
 */
public class Util {
	/**
	 * gets the number of clients from the message content.
	 * 
	 * @param content
	 *            the content of the message.
	 * @return the total number of clients.
	 */
	public static int getNbOfClientsFromMsg(String content) {
		String nbString = "";
		Pattern patternClient = Pattern.compile("w(.*?)w");
		Matcher matcherClient = patternClient.matcher(content);
		if (matcherClient.find()) {
			nbString = matcherClient.group(1);
			Pattern intOnly = Pattern.compile("\\d+");
			Matcher makeMatch = intOnly.matcher(nbString);
			if (makeMatch.find()) {
				nbString = makeMatch.group();
			}
		}
		return Integer.parseInt(nbString);
	}

	/**
	 * gets the string identifier of the the client from the message content.
	 * 
	 * @param content
	 *            the content of the message.
	 * @return the identifier of the next client to subscribe to advertise.
	 */
	public static int getStringIdOfClientFromMsg(String content) {
		if (content == null) {
			return -1;
		}
		String publishingClient = "";
		Pattern patternClient = Pattern.compile("-(.*?)-");
		Matcher matcherClient = patternClient.matcher(content);
		if (matcherClient.find()) {
			publishingClient = matcherClient.group(1);
			Pattern intOnly = Pattern.compile("\\d+");
			Matcher makeMatch = intOnly.matcher(publishingClient);
			if (makeMatch.find()) {
				publishingClient = makeMatch.group();
			}
		}
		return Integer.parseInt(publishingClient);
	}

	/**
	 * loads string content from a document.
	 * 
	 * @param doc
	 *            the document.
	 * @return the string content.
	 */
	public static String loadStringFromDoc(Document doc) {
		DOMImplementationLS domImplementation = (DOMImplementationLS) doc
				.getImplementation();
		LSSerializer lsSerializer = domImplementation.createLSSerializer();
		return lsSerializer.writeToString(doc);
	}

	/**
	 * display time to wait and wait.
	 * 
	 * @param who
	 *            entity that wants to wait.
	 * @param type
	 *            type of delay.
	 * @param id
	 *            identifier of the subscription/advertisement.
	 * @param timeToWait
	 *            time to wait in milliseconds.
	 */
	public static void displayTimeToWaitInMS(final String who,
			final String type, final String id, final long timeToWait) {
		System.out.println(who + " : " + type + " " + id);
		System.out
				.println(who + " : " + "wait" + " " + timeToWait + " " + "ms");
		try {
			Thread.sleep(timeToWait);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
	}

	/**
	 * get the string from a DOM.
	 * 
	 * @param doc
	 *            the document.
	 * @return the string.
	 */
	public static String getStringFromDoc(Document doc) {
		DOMImplementationLS domImplementation = (DOMImplementationLS) doc
				.getImplementation();
		LSSerializer lsSerializer = domImplementation.createLSSerializer();
		return lsSerializer.writeToString(doc);
	}
}
