/**
This file is part of the muDEBS middleware.

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Léon Lim
Contributor(s):
 */
package mudebs.perfclient.perfclient;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Ignore;

import mudebs.common.MuDEBSException;
import mudebs.common.algorithms.routing.Scope;
import mudebs.perfclient.PerfWorker;

/**
 * Tests for the class <tt>PerfClient</tt>.
 * 
 * NB: by a slight abuse of usage, the method toString is used for comparisons.
 * 
 * @author Léon Lim
 *
 */
public class TestPerfWorker {

	@Ignore
	public void testSubSets1() throws MuDEBSException, URISyntaxException {
		URI d1 = new URI("mudebs:localhost:portnumber/d1");
		URI d2 = new URI("mudebs:localhost:portnumber/d2");
		URI d3 = new URI("mudebs:localhost:portnumber/d3");
		Scope s1 = new Scope(d1, "s1");
		Scope u1 = new Scope(d2, "u1");
		Scope t1 = new Scope(d3, "t1");
		List<Scope> scopes = new ArrayList<Scope>();
		scopes.add(s1);
		scopes.add(u1);
		scopes.add(t1);
		Assert.assertEquals(
				"[[Scope [id=[[s1]], [Scope [id=u1]], [Scope [id=t1]],"
						+ " [Scope [id=s1], Scope [id=u1]], [Scope [id=s1],"
						+ " Scope [id=t1]], [Scope [id=u1], Scope [id=t1]],"
						+ " [Scope [id=s1], Scope [id=u1], Scope [id=t]]1]]]",
				PerfWorker.getAllPossibleSubSetsOfScopes(scopes).toString());
	}

	@Ignore
	public void testSubSets2() throws MuDEBSException, URISyntaxException {
		URI d1 = new URI("mudebs:localhost:portnumber/d1");
		URI d2 = new URI("mudebs:localhost:portnumber/d2");
		URI d3 = new URI("mudebs:localhost:portnumber/d3");
		Scope s1 = new Scope(d1, "s1");
		Scope u1 = new Scope(d2, "u1");
		Scope t1 = new Scope(d3, "t1");
		Scope t2 = new Scope(d3, "t2");
		Scope t3 = new Scope(d3, "t3");
		List<Scope> scopes = new ArrayList<Scope>();
		scopes.add(s1);
		scopes.add(u1);
		scopes.add(t1);
		scopes.add(t2);
		scopes.add(t3);
		Assert.assertEquals(
				"[[Scope [id=[[s1], Scope [id=t3]], [Scope [id=s1],"
						+ " Scope [id=t2]], [Scope [id=s1], Scope [id=u1]],"
						+ " [Scope [id=s1], Scope [id=t1]], [Scope [id=t3],"
						+ " Scope [id=u1]], [Scope [id=t2], Scope [id=u1]],"
						+ " [Scope [id=u1], Scope [id=t1]], [Scope [id=s1]],"
						+ " [Scope [id=t3]], [Scope [id=t2]], [Scope [id=u1]],"
						+ " [Scope [id=t1]], [Scope [id=s1], Scope [id=t3],"
						+ " Scope [id=u1]], [Scope [id=s1], Scope [id=t2],"
						+ " Scope [id=u1]], [Scope [id=s1], Scope [id=u1],"
						+ " Scope [id=t]]1]]]",
				PerfWorker.getAllPossibleSubSetsOfScopes(scopes).toString());
	}
}
