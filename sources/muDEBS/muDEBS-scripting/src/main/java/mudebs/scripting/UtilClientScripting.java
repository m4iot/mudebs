/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */
package mudebs.scripting;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import mudebs.common.Constants;
import mudebs.common.Log;
import mudebs.common.algorithms.ACK;
import mudebs.common.algorithms.OperationalMode;
import mudebs.common.algorithms.routing.ABACAttribute;
import mudebs.common.algorithms.routing.ABACInformation;
import mudebs.common.algorithms.routing.AdvertisementMsgContent;
import mudebs.common.algorithms.routing.CollectivePublicationMsgContent;
import mudebs.common.algorithms.routing.CollectiveSubscriptionMsgContent;
import mudebs.common.algorithms.routing.GetScopesMsgContent;
import mudebs.common.algorithms.routing.MultiScopingSpecification;
import mudebs.common.algorithms.routing.Publication;
import mudebs.common.algorithms.routing.PublicationMsgContent;
import mudebs.common.algorithms.routing.RequestMsgContent;
import mudebs.common.algorithms.routing.Scope;
import mudebs.common.algorithms.routing.ScopeBottom;
import mudebs.common.algorithms.routing.ScopeTop;
import mudebs.common.algorithms.routing.Subscription;
import mudebs.common.algorithms.routing.SubscriptionMsgContent;
import mudebs.common.algorithms.routing.UnadvertisementMsgContent;
import mudebs.common.algorithms.scripting.client.AlgorithmScriptingClient;
import mudebs.common.algorithms.scripting.client.DisconnectReconnectMsgContent;
import mudebs.common.algorithms.scripting.client.TerminationMsgContent;
import mudebs.communication.javanio.FullDuplexMsgWorker;

import org.apache.log4j.Level;

/**
 * This class is the main of a program to script clients, that is to say to send
 * commands to clients.
 * 
 * @author Denis Conan
 * 
 */
public class UtilClientScripting {

	/**
	 * usage message.
	 */
	public static final String USAGE = "Usage: java "
			+ "mudebs.scripting.ClientScripting "
			+ Constants.OPTION_URI
			+ " <URI of the client> "
			+ "\n\t    "
			+ Constants.OPTION_COMMAND
			+ " "
			+ Constants.COMMAND_ADVERTISE
			+ " ["
			+ Constants.OPTION_ACK
			+ " <no | local | global>] "
			+ Constants.OPTION_ID
			+ " <identifier> ["
			+ Constants.OPTION_COMMAND_CONTENT
			+ " <content> | "
			+ Constants.OPTION_COMMAND_FILE
			+ " <filepath>] "
			+ " ["
			+ Constants.OPTION_COMMAND_POLICY
			+ " <filepath>] "
			+ " ["
			+ Constants.OPTION_COMMAND_DIMENSION
			+ " <URI of the dimension> "
			+ Constants.OPTION_COMMAND_SCOPE
			+ " <name of the scope> |"
			+ Constants.OPTION_COMMAND_SCOPEBOTTOM
			+ " ]* "
			+ "\n\t  | "
			+ Constants.OPTION_COMMAND
			+ " "
			+ Constants.COMMAND_ADVERTISE_GLOBAL
			+ " ["
			+ Constants.OPTION_ACK
			+ " <no | local | global>] "
			+ Constants.OPTION_ID
			+ " <identifier> ["
			+ Constants.OPTION_COMMAND_CONTENT
			+ " <content> | "
			+ Constants.OPTION_COMMAND_FILE
			+ " <filepath>] "
			+ " ["
			+ Constants.OPTION_COMMAND_POLICY
			+ " <filepath>] "
			+ " ["
			+ Constants.OPTION_COMMAND_DIMENSION
			+ " <URI of the dimension> "
			+ Constants.OPTION_COMMAND_SCOPE
			+ " <name of the scope> |"
			+ Constants.OPTION_COMMAND_SCOPEBOTTOM
			+ " ]* "
			+ "\n\t  | "
			+ Constants.OPTION_COMMAND
			+ " "
			+ Constants.COMMAND_PUBLISH
			+ " ["
			+ Constants.OPTION_ACK
			+ " <no | local | global>] "
			+ " ["
			+ Constants.OPTION_ID
			+ " <identifier> "
			+ Constants.OPTION_COMMAND_CONTENT
			+ " <content>]* "
			+ "\n\t  | "
			+ Constants.OPTION_COMMAND
			+ " "
			+ Constants.COMMAND_REQUEST
			+ " "
			+ Constants.OPTION_COMMAND_FILE
			+ " <filepath> "
			+ " ["
			+ Constants.OPTION_COMMAND_ABAC
			+ " group=<group>:purpose=<purpose>:"
			+ "resource=<resource>:action=<action>] "
			+ " ["
			+ Constants.OPTION_COMMAND_DIMENSION
			+ " <URI of the dimension> "
			+ Constants.OPTION_COMMAND_SCOPE
			+ " <name of the scope>]* "
			+ "\n\t  | "
			+ Constants.OPTION_COMMAND
			+ " "
			+ Constants.COMMAND_REQUEST_LOCAL
			+ " "
			+ Constants.OPTION_COMMAND_FILE
			+ " <filepath> "
			+ " ["
			+ Constants.OPTION_COMMAND_ABAC
			+ " group=<group>:purpose=<purpose>:"
			+ "resource=<resource>:action=<action>] "
			+ " ["
			+ Constants.OPTION_COMMAND_DIMENSION
			+ " <URI of the dimension> "
			+ Constants.OPTION_COMMAND_SCOPE
			+ " <name of the scope>]* "
			+ "\n\t  | "
			+ Constants.OPTION_COMMAND
			+ " "
			+ Constants.COMMAND_SUBSCRIBE
			+ " ["
			+ Constants.OPTION_ACK
			+ " <no | local | global>] "
			+ " ["
			+ Constants.OPTION_ID
			+ " <identifier> ["
			+ Constants.OPTION_COMMAND_CONTENT
			+ " <content> | "
			+ Constants.OPTION_COMMAND_FILE
			+ " <filepath>]]* "
			+ " ["
			+ Constants.OPTION_COMMAND_ABAC
			+ " group=<group>:purpose=<purpose>:"
			+ "resource=<resource>:action=<action>] "
			+ " ["
			+ Constants.OPTION_COMMAND_DIMENSION
			+ " <URI of the dimension> "
			+ Constants.OPTION_COMMAND_SCOPE
			+ " <name of the scope>]* "
			+ "\n\t  | "
			+ Constants.OPTION_COMMAND
			+ " "
			+ Constants.COMMAND_SUBSCRIBE_LOCAL
			+ " ["
			+ Constants.OPTION_ACK
			+ " <no | local | global>] "
			+ " ["
			+ Constants.OPTION_ID
			+ " <identifier> ["
			+ Constants.OPTION_COMMAND_CONTENT
			+ " <content> | "
			+ Constants.OPTION_COMMAND_FILE
			+ " <filepath>]]* "
			+ " ["
			+ Constants.OPTION_COMMAND_ABAC
			+ " group=<group>:purpose=<purpose>:"
			+ "resource=<resource>:action=<action>] "
			+ " ["
			+ Constants.OPTION_COMMAND_DIMENSION
			+ " <URI of the dimension> "
			+ Constants.OPTION_COMMAND_SCOPE
			+ " <name of the scope>]* "
			+ "\n\t  | "
			+ Constants.OPTION_COMMAND
			+ " "
			+ Constants.COMMAND_TERMINATE
			+ "\n\t  | "
			+ Constants.OPTION_COMMAND
			+ " "
			+ Constants.COMMAND_UNADVERTISE
			+ " ["
			+ Constants.OPTION_ACK
			+ " <no | local | global>] "
			+ " ["
			+ Constants.OPTION_ID
			+ " <identifier>]"
			+ "\n\t  | "
			+ Constants.OPTION_COMMAND
			+ " "
			+ Constants.COMMAND_UNSUBSCRIBE
			+ " ["
			+ Constants.OPTION_ACK
			+ " <no | local | global>] "
			+ " ["
			+ Constants.OPTION_ID
			+ " <identifier>]"
			+ "\n\t  | "
			+ Constants.OPTION_COMMAND
			+ " "
			+ Constants.COMMAND_GET_SCOPES
			+ "\n\t  | "
			+ Constants.OPTION_COMMAND
			+ " "
			+ Constants.COMMAND_DISCONNECT_RECONNECT
			+ " "
			+ Constants.OPTION_BROKER + " <URI of the new broker>";
	/**
	 * URI of the client, which is provided as a command line argument.
	 */
	private static String uri = null;
	/**
	 * name of the command to send to the client and that will be executed by
	 * the client.
	 */
	private static String command = null;
	/**
	 * blocking command value: "no", or "local", or "global". By default, the
	 * value is "no".
	 */
	private static ACK ack = ACK.NO;
	/**
	 * contents of the command to send to the client and that will be executed
	 * by the client.
	 */
	private static List<String> contents = new ArrayList<String>();
	/**
	 * files containing the content of the command to send to the client and
	 * that will be executed by the client.
	 */
	private static List<String> files = new ArrayList<String>();
	/**
	 * file containing the content of the XACML policy.
	 */
	private static String policyFile = null;
	/**
	 * ABAC information.
	 */
	private static ABACInformation abacInfo = null;
	/**
	 * identifiers of the advertisement (when command is <tt>advertise</tt>) of
	 * the subscription (when command is <tt>subscribe</tt>).
	 */
	private static List<String> identifiers = new ArrayList<String>();
	/**
	 * set of sets of scope paths associated with the subscription our
	 * advertisement filter
	 */
	private static MultiScopingSpecification phi;
	/**
	 * set of dimensions.
	 */
	private static List<String> dimensions = new ArrayList<String>();
	/**
	 * set of scope identifiers.
	 */
	private static List<String> scopeIds = new ArrayList<String>();
	/**
	 * the scope bottom is provided in advertise command.
	 */
	private static boolean scopeBottom = false;
	/**
	 * the scope top is provided in subscribe command.
	 */
	private static boolean scopeTop = false;
	/**
	 * identifier of the new broker (when command is <tt>dis-/re-connect</tt>).
	 */
	private static String broker = null;

	public static void reset() {
		uri = null;
		command = null;
		ack = ACK.NO;
		contents = new ArrayList<String>();
		files = new ArrayList<String>();
		policyFile = null;
		abacInfo = null;
		identifiers = new ArrayList<String>();
		scopeBottom = false;
		scopeTop = false;
		broker = null;
		dimensions = new ArrayList<String>();
		scopeIds = new ArrayList<String>();
	}

	/**
	 * reads a command line.
	 * 
	 * @param argv
	 *            the words of the command line.
	 */
	public static void readCommandLine(final String[] argv) {
		boolean scopeToCome = false;
		for (int i = 0; i < argv.length; i = i + 2) {
			if (argv[i].equalsIgnoreCase(Constants.OPTION_URI)) {
				uri = argv[i + 1];
			} else if (argv[i].equalsIgnoreCase(Constants.OPTION_COMMAND)) {
				command = argv[i + 1];
			} else if (argv[i].equalsIgnoreCase(Constants.OPTION_ACK)) {
				String bstring = argv[i + 1];
				if (bstring.equalsIgnoreCase("no")) {
					ack = ACK.NO;
				} else if (bstring.equalsIgnoreCase("local")) {
					ack = ACK.LOCAL;
				} else if (bstring.equalsIgnoreCase("global")) {
					ack = ACK.GLOBAL;
				}
			} else if (argv[i]
					.equalsIgnoreCase(Constants.OPTION_COMMAND_CONTENT)) {
				contents.add(argv[i + 1]);
				files.add("");
			} else if (argv[i].equalsIgnoreCase(Constants.OPTION_COMMAND_FILE)) {
				contents.add("");
				files.add(argv[i + 1]);
			} else if (argv[i]
					.equalsIgnoreCase(Constants.OPTION_COMMAND_POLICY)) {
				policyFile = argv[i + 1];
			} else if (argv[i].equals(Constants.OPTION_COMMAND_ABAC)) {
				String[] infos = argv[i + 1].split(":");
				ArrayList<ABACAttribute> attList = new ArrayList<ABACAttribute>();
				for (int k = 0; k < infos.length; k++) {
					String category = null;
					String identifier = null;
					String value = null;
					ABACAttribute att = null;
					String[] attInfo = infos[k].split(",");
					if (attInfo.length != 3) {
						break;
					}
					category = attInfo[0];
					identifier = attInfo[1];
					value = attInfo[2];
					att = new ABACAttribute(category, identifier, value,
							"http://www.w3.org/2001/XMLSchema#string");
					attList.add(att);
				}
				if (!attList.isEmpty()) {
					abacInfo = new ABACInformation(attList);
				}
			} else if (argv[i].equalsIgnoreCase(Constants.OPTION_ID)) {
				identifiers.add(argv[i + 1]);
			} else if (argv[i].equalsIgnoreCase(Constants.OPTION_BROKER)) {
				broker = argv[i + 1];
			} else if (argv[i]
					.equalsIgnoreCase(Constants.OPTION_COMMAND_DIMENSION)) {
				dimensions.add(argv[i + 1]);
				scopeToCome = true;
			} else if (argv[i]
					.equalsIgnoreCase(Constants.OPTION_COMMAND_SCOPEBOTTOM)) {
				scopeBottom = true;
			} else if (argv[i]
					.equalsIgnoreCase(Constants.OPTION_COMMAND_SCOPETOP)) {
				scopeTop = true;
			} else if (argv[i].equalsIgnoreCase(Constants.OPTION_COMMAND_SCOPE)) {
				if (!scopeToCome) {
					Log.CONFIG.error("missing dimension for scope "
							+ argv[i + 1]);
					Log.CONFIG.error(USAGE);
					return;
				}
				scopeToCome = false;
				scopeIds.add(argv[i + 1]);
			}
		}
		if ((uri == null) || (uri.equals("")) || (command == null)
				|| (command.equals(""))) {
			if (Log.ON && Log.CONFIG.isEnabledFor(Level.ERROR)) {
				Log.CONFIG.error("some arguments are missing");
				Log.CONFIG.error(USAGE);
			}
			throw new RuntimeException("argument URI is missing");
		}
		if (identifiers.size() != 0
				&& (contents.size() != 0 || files.size() != 0)) {
			if (identifiers.size() != contents.size()
					|| identifiers.size() != files.size()) {
				if (Log.ON && Log.CONFIG.isEnabledFor(Level.ERROR)) {
					Log.CONFIG.error("not the same number of identifiers,"
							+ " and contents or files");
					Log.CONFIG.error(USAGE);
				}
				throw new RuntimeException("argument URI is missing");
			}
		}
		if (!dimensions.isEmpty() || scopeBottom || scopeBottom) {
			if (dimensions.size() != scopeIds.size()) {
				if (Log.ON && Log.CONFIG.isEnabledFor(Level.ERROR)) {
					Log.CONFIG
							.error("number of dimensions != number of scopes");
					Log.CONFIG.error(USAGE);
				}
				return;
			}
			Set<Scope> scopes = new TreeSet<Scope>();
			if (scopeBottom) {
				scopes.add(new ScopeBottom());
			}
			if (scopeTop) {
				scopes.add(new ScopeTop());
			}
			for (int i = 0; i < dimensions.size(); i++) {
				Scope scope = null;
				if (scopeIds.get(i).equalsIgnoreCase(ScopeBottom.IDENTIFIER)) {
					scope = new ScopeBottom();
				} else if (scopeIds.get(i)
						.equalsIgnoreCase(ScopeTop.IDENTIFIER)) {
					scope = new ScopeTop();
				} else {
					try {
						scope = new Scope(new URI(dimensions.get(i)),
								scopeIds.get(i));
					} catch (URISyntaxException e) {
						if (Log.ON && Log.CONFIG.isEnabledFor(Level.ERROR)) {
							Log.CONFIG.error("malformed URI for dimensions = "
									+ dimensions.get(i));
							Log.CONFIG.error(USAGE);
						}
						return;
					}
				}
				scopes.add(scope);
			}
			phi = MultiScopingSpecification.create(scopes);
		}
	}

	/**
	 * performs the actions.
	 */
	public static void work() {
		SocketChannel rwChan;
		Socket rwCon;
		InetSocketAddress rcvAddress;
		URI clientURI = null;
		String policyContent = null;
		try {
			clientURI = new URI(uri);
		} catch (URISyntaxException e) {
			if (Log.ON && Log.CONFIG.isEnabledFor(Level.ERROR)) {
				Log.CONFIG.error(uri + ", "
						+ Log.printStackTrace(e.getStackTrace()));
			}
			return;
		}
		InetAddress destAddr;
		try {
			destAddr = InetAddress.getByName(clientURI.getHost());
		} catch (UnknownHostException e) {
			if (Log.ON && Log.COMM.isEnabledFor(Level.ERROR)) {
				Log.COMM.error(uri + ", "
						+ Log.printStackTrace(e.getStackTrace()));
			}
			return;
		}
		try {
			rwChan = SocketChannel.open();
		} catch (IOException e) {
			if (Log.ON && Log.COMM.isEnabledFor(Level.ERROR)) {
				Log.COMM.error(uri + ", "
						+ Log.printStackTrace(e.getStackTrace()));
			}
			return;
		}
		rwCon = rwChan.socket();
		rcvAddress = new InetSocketAddress(destAddr, clientURI.getPort());
		try {
			rwCon.connect(rcvAddress);
		} catch (IOException e) {
			if (Log.ON && Log.COMM.isEnabledFor(Level.ERROR)) {
				Log.COMM.error(uri + ", "
						+ Log.printStackTrace(e.getStackTrace()));
			}
			return;
		}
		FullDuplexMsgWorker msgWorker = new FullDuplexMsgWorker(rwChan);
		try {
			if (command.equalsIgnoreCase(Constants.COMMAND_ADVERTISE)
					|| command
							.equalsIgnoreCase(Constants.COMMAND_ADVERTISE_GLOBAL)) {
				if (identifiers.size() == 0 || identifiers.get(0) == null
						|| identifiers.get(0).equals("")) {
					Log.CONFIG.error("missing identifier");
					Log.CONFIG.error(USAGE);
					return;
				}
				OperationalMode opMode = command
						.equalsIgnoreCase(Constants.COMMAND_ADVERTISE) ? OperationalMode.LOCAL
						: OperationalMode.GLOBAL;
				if (contents.size() != 0 && contents.get(0) != null
						&& !contents.get(0).equals("")) {
					if (policyFile != null) {
						try {
							policyContent = readFile(policyFile,
									StandardCharsets.UTF_8);
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
					msgWorker.sendMsg(AlgorithmScriptingClient.ADVERTISEMENT
							.getActionIndex(), new AdvertisementMsgContent(
							clientURI, identifiers.get(0), 0, opMode, ack,
							contents.get(0), phi, policyContent));
				} else if (files.size() != 0 && files.get(0) != null
						&& !files.get(0).equals("")) {
					String content = "";
					try {
						FileInputStream fstream = new FileInputStream(
								files.get(0));
						DataInputStream in = new DataInputStream(fstream);
						InputStreamReader isr = new InputStreamReader(in,
								"UTF-8");
						BufferedReader br = new BufferedReader(isr);
						String strLine;
						while ((strLine = br.readLine()) != null) {
							content += strLine;
						}
						isr.close();
						in.close();
						fstream.close();
					} catch (FileNotFoundException e) {
						if (Log.ON && Log.CONFIG.isEnabledFor(Level.ERROR)) {
							Log.CONFIG.error(uri + ", "
									+ Log.printStackTrace(e.getStackTrace()));
						}
						return;
					}
					if (policyFile != null) {
						try {
							policyContent = readFile(policyFile,
									StandardCharsets.UTF_8);
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
					msgWorker.sendMsg(AlgorithmScriptingClient.ADVERTISEMENT
							.getActionIndex(), new AdvertisementMsgContent(
							clientURI, identifiers.get(0), 0, opMode, ack,
							content, phi, policyContent));
				} else {
					if (Log.ON && Log.CONFIG.isEnabledFor(Level.ERROR)) {
						Log.CONFIG.error("missing content or file name");
						Log.CONFIG.error(USAGE);
					}
					return;
				}
			} else if (command.equalsIgnoreCase(Constants.COMMAND_PUBLISH)) {
				if (identifiers.size() == 0 || identifiers.get(0) == null
						|| identifiers.get(0).equals("")) {
					Log.CONFIG.error("missing identifier");
					Log.CONFIG.error(USAGE);
					return;
				}
				List<URI> path = new ArrayList<URI>();
				path.add(clientURI);
				if (identifiers.size() == 1) {
					if (contents.size() == 0 || contents.get(0) == null
							|| contents.get(0).equals("")) {
						Log.CONFIG.error("missing content");
						Log.CONFIG.error(USAGE);
						return;
					}
					// the two sequence numbers are irrelevant here and will be
					// assigned by the client delegate
					msgWorker.sendMsg(AlgorithmScriptingClient.PUBLICATION
							.getActionIndex(), new PublicationMsgContent(path,
							ack, 0, identifiers.get(0), 0, contents.get(0)));
				} else {
					List<Publication> pubs = new ArrayList<Publication>();
					for (int i = 0; i < contents.size(); i++) {
						if (contents.get(i) != null
								&& !contents.get(i).equals("")) {
							// the sequence number is irrelevant here and will
							// be
							// assigned by the client delegate
							pubs.add(new Publication(identifiers.get(i), 0,
									contents.get(i)));
						}
					}
					// the sequence number is irrelevant here and will be
					// assigned by the client delegate
					msgWorker.sendMsg(
							AlgorithmScriptingClient.COLLECTIVE_PUBLICATION
									.getActionIndex(),
							new CollectivePublicationMsgContent(path, ack, 0,
									pubs));
				}
			} else if (command.equalsIgnoreCase(Constants.COMMAND_REQUEST)
					|| command
							.equalsIgnoreCase(Constants.COMMAND_REQUEST_LOCAL)) {
				if (files.size() == 0 || files.get(0) == null
						|| files.get(0).equals("")) {
					Log.CONFIG.error("missing file path");
					Log.CONFIG.error(USAGE);
					return;
				}
				OperationalMode local = command
						.equalsIgnoreCase(Constants.COMMAND_REQUEST_LOCAL) ? OperationalMode.LOCAL
						: OperationalMode.GLOBAL;
				String content = "";
				try {
					FileInputStream fstream = new FileInputStream(files.get(0));
					DataInputStream in = new DataInputStream(fstream);
					InputStreamReader isr = new InputStreamReader(in, "UTF-8");
					BufferedReader br = new BufferedReader(isr);
					String strLine;
					content = "";
					while ((strLine = br.readLine()) != null) {
						content += strLine;
					}
					isr.close();
					in.close();
					fstream.close();
				} catch (FileNotFoundException e) {
					if (Log.ON && Log.CONFIG.isEnabledFor(Level.ERROR)) {
						Log.CONFIG.error(uri + ", "
								+ Log.printStackTrace(e.getStackTrace()));
					}
					return;
				}
				// the sequence number is irrelevant here and will be
				// assigned by the client delegate
				msgWorker.sendMsg(AlgorithmScriptingClient.REQUEST
						.getActionIndex(), new RequestMsgContent(clientURI,
						local, 0, content, phi, abacInfo));
			} else if (command.equalsIgnoreCase(Constants.COMMAND_SUBSCRIBE)
					|| command
							.equalsIgnoreCase(Constants.COMMAND_SUBSCRIBE_LOCAL)) {
				if (identifiers.size() == 0 || identifiers.get(0) == null
						|| identifiers.get(0).equals("")) {
					Log.CONFIG.error("missing identifier");
					Log.CONFIG.error(USAGE);
					return;
				}
				OperationalMode opMode = command
						.equalsIgnoreCase(Constants.COMMAND_SUBSCRIBE) ? OperationalMode.GLOBAL
						: OperationalMode.LOCAL;
				if (identifiers.size() == 1) {
					if (contents.size() != 0 && contents.get(0) != null
							&& !contents.get(0).equals("")) {
						msgWorker.sendMsg(AlgorithmScriptingClient.SUBSCRIPTION
								.getActionIndex(), new SubscriptionMsgContent(
								clientURI, identifiers.get(0), 0, opMode, ack,
								contents.get(0), phi, abacInfo));
					} else if (files.size() != 0 && files.get(0) != null
							&& !files.get(0).equals("")) {
						String content = "";
						try {
							FileInputStream fstream = new FileInputStream(
									files.get(0));
							DataInputStream in = new DataInputStream(fstream);
							InputStreamReader isr = new InputStreamReader(in,
									"UTF-8");
							BufferedReader br = new BufferedReader(isr);
							String strLine;
							content = "";
							while ((strLine = br.readLine()) != null) {
								content += strLine;
							}
							isr.close();
							in.close();
							fstream.close();
						} catch (FileNotFoundException e) {
							if (Log.ON && Log.CONFIG.isEnabledFor(Level.ERROR)) {
								Log.CONFIG
										.error(uri
												+ ", "
												+ Log.printStackTrace(e
														.getStackTrace()));
							}
							return;
						}
						msgWorker.sendMsg(AlgorithmScriptingClient.SUBSCRIPTION
								.getActionIndex(), new SubscriptionMsgContent(
								clientURI, identifiers.get(0), 0, opMode, ack,
								content, phi, abacInfo));
					} else {
						if (Log.ON && Log.CONFIG.isEnabledFor(Level.ERROR)) {
							Log.CONFIG.error("missing content or file name");
							Log.CONFIG.error(USAGE);
						}
						return;
					}
				} else {
					List<Subscription> subs = new ArrayList<Subscription>();
					for (int i = 0; i < contents.size(); i++) {
						if (contents.get(i) != null
								&& !contents.get(i).equals("")) {
							subs.add(new Subscription(identifiers.get(i),
									contents.get(i), phi, abacInfo));
						} else if (files.get(i) != null
								&& !files.get(i).equals("")) {
							String content = "";
							try {
								FileInputStream fstream = new FileInputStream(
										files.get(i));
								DataInputStream in = new DataInputStream(
										fstream);
								InputStreamReader isr = new InputStreamReader(
										in, "UTF-8");
								BufferedReader br = new BufferedReader(isr);
								String strLine;
								content = "";
								while ((strLine = br.readLine()) != null) {
									content += strLine;
								}
								isr.close();
								in.close();
								fstream.close();
							} catch (FileNotFoundException e) {
								if (Log.ON
										&& Log.CONFIG.isEnabledFor(Level.ERROR)) {
									Log.CONFIG.error(uri
											+ ", "
											+ Log.printStackTrace(e
													.getStackTrace()));
								}
								return;
							}
							subs.add(new Subscription(identifiers.get(i),
									content, phi, abacInfo));
						} else {
							if (Log.ON && Log.CONFIG.isEnabledFor(Level.ERROR)) {
								Log.CONFIG
										.error("missing content or file name");
								Log.CONFIG.error(USAGE);
							}
							return;
						}
					}
					msgWorker.sendMsg(
							AlgorithmScriptingClient.COLLECTIVE_SUBSCRIPTION
									.getActionIndex(),
							new CollectiveSubscriptionMsgContent(clientURI, 0,
									opMode, ack, subs));
				}
			} else if (command.equalsIgnoreCase(Constants.COMMAND_TERMINATE)) {
				msgWorker.sendMsg(
						AlgorithmScriptingClient.TERMINATION.getActionIndex(),
						new TerminationMsgContent(clientURI));
			} else if (command.equalsIgnoreCase(Constants.COMMAND_GET_SCOPES)) {
				msgWorker.sendMsg(
						AlgorithmScriptingClient.GET_SCOPES.getActionIndex(),
						new GetScopesMsgContent(clientURI, 0));
			} else if (command.equalsIgnoreCase(Constants.COMMAND_UNADVERTISE)) {
				// no identifier = unadvertiseAll
				String id = (identifiers.size() == 0) ? null : identifiers
						.get(0);
				msgWorker.sendMsg(AlgorithmScriptingClient.UNADVERTISEMENT
						.getActionIndex(), new UnadvertisementMsgContent(
						clientURI, id, 0, ack));
			} else if (command.equalsIgnoreCase(Constants.COMMAND_UNSUBSCRIBE)) {
				// no identifier = unsubscribe all
				String id = (identifiers.size() == 0) ? null : identifiers
						.get(0);
				msgWorker.sendMsg(AlgorithmScriptingClient.UNSUBSCRIPTION
						.getActionIndex(), new UnadvertisementMsgContent(
						clientURI, id, 0, ack));
			} else if (command
					.equalsIgnoreCase(Constants.COMMAND_DISCONNECT_RECONNECT)) {
				if (broker == null || broker.equals("")) {
					if (Log.ON && Log.COMM.isEnabledFor(Level.ERROR)) {
						Log.CONFIG.error("missing URI of the new broker");
						Log.CONFIG.error(USAGE);
						return;
					}
				}
				URI brokerURI = null;
				try {
					brokerURI = new URI(broker);
				} catch (URISyntaxException e) {
					if (Log.ON && Log.CONFIG.isEnabledFor(Level.ERROR)) {
						Log.CONFIG.error(uri + ", "
								+ Log.printStackTrace(e.getStackTrace()));
					}
					return;
				}
				msgWorker.sendMsg(AlgorithmScriptingClient.DISCONNECTRECONNECT
						.getActionIndex(), new DisconnectReconnectMsgContent(
						clientURI, brokerURI));
			}
		} catch (IOException e) {
			if (Log.ON && Log.COMM.isEnabledFor(Level.ERROR)) {
				Log.COMM.error(uri + ", "
						+ Log.printStackTrace(e.getStackTrace()));
			}
		}
		try {
			msgWorker.close();
		} catch (IOException e) {
			if (Log.ON && Log.COMM.isEnabledFor(Level.WARN)) {
				Log.COMM.warn(uri + ", "
						+ Log.printStackTrace(e.getStackTrace()));
			}
		}
	}

	/**
	 * opens a binary file, reads the contents of the file into a byte array,
	 * and then closes the file.
	 * 
	 * @param path
	 *            the file path.
	 * @param encoding
	 *            the specified encoding.
	 * @return the string corresponding to the content of the file.
	 * @throws IOException
	 *             thrown when problem occurs while reading the file.
	 */
	private static String readFile(String path, Charset encoding)
			throws IOException {
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, encoding);
	}
}
