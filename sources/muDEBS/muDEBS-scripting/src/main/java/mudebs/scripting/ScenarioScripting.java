/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis
The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */
package mudebs.scripting;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import mudebs.broker.BrokerMain;
import mudebs.client.ClientMain;
import mudebs.perfclient.PerfClientMain;

/**
 * This class is the main of a program to script brokers, that is to say to send
 * commands to brokers.
 * 
 * @author Denis Conan
 * 
 */
public class ScenarioScripting {

	/**
	 * the main method.
	 * 
	 * @param argv
	 *            the command line arguments.
	 * @throws IOException
	 *             the exception thrown when manipulating the file of the
	 *             scenario to execute.
	 */
	public static void main(final String[] argv) throws IOException {
		List<Process> processes = new ArrayList<Process>();
		BufferedReader console = new BufferedReader(new InputStreamReader(
				System.in));
		FileInputStream fstream = new FileInputStream(argv[0]);
		DataInputStream in = new DataInputStream(fstream);
		InputStreamReader isr = new InputStreamReader(in, "UTF-8");
		BufferedReader br = new BufferedReader(isr);
		String strLine;
		while ((strLine = br.readLine()) != null) {
			String line = (strLine.indexOf('#') == -1) ? strLine : strLine
					.substring(0, strLine.indexOf('#'));
			String[] words = line.split(" ");
			for (int i = 0; i < words.length; i++) {
				if (words[i].equals("''") || words[i].equals("\"\"")
						|| words[i].equals("\\'\\'")
						|| words[i].equals("\\\"\\\"")) {
					words[i] = "";
				}
				if (words[i].startsWith("'") || words[i].startsWith("\"")) {
					words[i] = words[i].substring(1);
				} else if (words[i].startsWith("\\'")
						|| words[i].startsWith("\\\"")) {
					words[i] = words[i].substring(2);
				}
				if (words[i].endsWith("\\'") || words[i].endsWith("\\\"")) {
					words[i] = words[i].substring(0, words[i].length() - 2);
				} else if (words[i].endsWith("'") || words[i].endsWith("\"")) {
					words[i] = words[i].substring(0, words[i].length() - 1);
				}
			}
			if (words[0].startsWith("#")) {
				// nop because this is a comment line
			} else if (words[0].equals("echo")) {
				for (String s : Arrays.copyOfRange(words, 1, words.length)) {
					System.out.print(s + " ");
				}
				System.out.println();
			} else if (words[0].equals("sleep")) {
				try {
					Thread.sleep(new Integer(words[1]) * 1000);
				} catch (NumberFormatException e) {
					e.printStackTrace();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			} else if (words[0].equals("read")) {
				console.readLine();
			} else if (words[0].endsWith("startbroker")) {
				List<String> command = new ArrayList<String>();
				command.add("java");
				command.add("-Xms128m");
				command.add("-Xmx512m");
				command.addAll(classpath());
				command.add(BrokerMain.class.getName());
				String[] wcommand = Arrays.copyOfRange(words, 1, words.length);
				for (String s : wcommand) {
					command.add(s);
				}
				for (String s : command) {
					System.out.print(s + " ");
				}
				System.out.println();
				ProcessBuilder pb = new ProcessBuilder(command);
				pb.inheritIO();
				Process p = pb.start();
				processes.add(p);
			} else if (words[0].endsWith("stopbroker")
					|| words[0].endsWith("stopclient")) {
				for (Process p : processes) {
					p.destroy();
				}
			} else if (words[0].endsWith("broker")) {
				System.out.print("broker ");
				for (String s : Arrays.copyOfRange(words, 1, words.length)) {
					System.out.print(s + " ");
				}
				System.out.println();
				try {
					UtilBrokerScripting.readCommandLine(Arrays.copyOfRange(
							words, 1, words.length));
					UtilBrokerScripting.work();
					UtilBrokerScripting.reset();
				} catch (IllegalArgumentException e) {
					System.out.println(e.getMessage());
				}
			} else if (words[0].endsWith("startclient")) {
				List<String> command = new ArrayList<String>();
				command.add("java");
				command.add("-Xms128m");
				command.add("-Xmx512m");
				command.addAll(classpath());
				command.add(ClientMain.class.getName());
				String[] wcommand = Arrays.copyOfRange(words, 1, words.length);
				for (String s : wcommand) {
					command.add(s);
				}
				for (String s : command) {
					System.out.print(s + " ");
				}
				System.out.println();
				ProcessBuilder pb = new ProcessBuilder(command);
				pb.inheritIO();
				Process p = pb.start();
				processes.add(p);
			} else if (words[0].endsWith("startperfclient")) {
				List<String> command = new ArrayList<String>();
				command.add("java");
				command.add("-Xms128m");
				command.add("-Xmx512m");
				command.addAll(classpath());
				command.add(PerfClientMain.class.getName());
				String[] wcommand = Arrays.copyOfRange(words, 1, words.length);
				for (String s : wcommand) {
					command.add(s);
				}
				for (String s : command) {
					System.out.print(s + " ");
				}
				System.out.println();
				ProcessBuilder pb = new ProcessBuilder(command);
				pb.inheritIO();
				Process p = pb.start();
				processes.add(p);
			} else if (words[0].endsWith("client")) {
				System.out.print("client ");
				for (String s : Arrays.copyOfRange(words, 1, words.length)) {
					System.out.print(s + " ");
				}
				System.out.println();
				try {
					UtilClientScripting.readCommandLine(Arrays.copyOfRange(
							words, 1, words.length));
					UtilClientScripting.work();
					UtilClientScripting.reset();
				} catch (IllegalArgumentException e) {
					System.out.println(e.getMessage());
				}
			}
		}
		isr.close();
		in.close();
		fstream.close();
	}

	private static List<String> classpath() {
		List<String> result = new ArrayList<String>();
		String sep = System.getProperty("file.separator");
		String version = "0.17.3";
		String repo = System.getProperty("user.home") + sep + ".m2" + sep
				+ "repository" + sep;
		String mudebs = "muDEBS" + sep;
		String psep = System.getProperty("path.separator");
		String common = repo + mudebs + "muDEBS-common" + sep + version + sep
				+ "muDEBS-common-" + version + ".jar";
		String comm = repo + mudebs + "muDEBS-communication-javanio" + sep
				+ version + sep + "muDEBS-communication-javanio-" + version
				+ ".jar";
		String broker = repo + mudebs + "muDEBS-broker" + sep + version + sep
				+ "muDEBS-broker-" + version + ".jar";
		String client = repo + mudebs + "muDEBS-client" + sep + version + sep
				+ "muDEBS-client-" + version + ".jar";
		String perfclient = repo + mudebs + "muDEBS-perf-client" + sep
				+ version + sep + "muDEBS-perf-client-" + version + ".jar";
		String scripting = repo + mudebs + "muDEBS-scripting" + sep + version
				+ sep + "muDEBS-scripting-" + version + ".jar";
		String log4j = repo + "log4j" + sep + "log4j" + sep + "1.2.17" + sep
				+ "log4j-1.2.17.jar";
		String gson = repo + "com" + sep + "google" + sep + "code" + sep
				+ "gson" + sep + "gson" + sep + "2.3.1" + sep
				+ "gson-2.3.1.jar";
		String logging = repo + "commons-logging" + sep + "commons-logging"
				+ sep + "1.1.1" + sep + "commons-logging-1.1.1.jar";
		String balana = repo + "org" + sep + "wso2" + sep + "balana" + sep
				+ "org.wso2.balana" + sep + "1.0.0-wso2v7" + sep
				+ "org.wso2.balana-1.0.0-wso2v7.jar";
		result.add("-cp");
		result.add(common + psep + comm + psep + broker + psep + client + psep
				+ perfclient + psep + scripting + psep + log4j + psep + gson
				+ psep + logging + psep + balana);
		return result;
	}
}
