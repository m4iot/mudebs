/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Léon Lim
 */
package mudebs.scripting;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.nio.channels.SocketChannel;
import java.util.HashSet;
import java.util.Set;

import mudebs.common.Constants;
import mudebs.common.Log;
import mudebs.common.algorithms.EntityType;
import mudebs.common.algorithms.overlaymanagement.AlgorithmOverlayManagement;
import mudebs.common.algorithms.overlaymanagement.ColorTerminationDetection;
import mudebs.common.algorithms.overlaymanagement.IdentityMsgContent;
import mudebs.common.algorithms.overlaymanagement.TerminationDetectionMsgContent;
import mudebs.common.algorithms.overlaymanagement.TerminationMsgContent;
import mudebs.common.algorithms.routing.JoinScopeMsgContent;
import mudebs.common.algorithms.routing.LeaveScopeMsgContent;
import mudebs.common.algorithms.routing.Scope;
import mudebs.common.algorithms.routing.ScopeBottom;
import mudebs.common.algorithms.routing.ScopeTop;
import mudebs.common.algorithms.scripting.broker.AlgorithmScriptingBroker;
import mudebs.common.algorithms.scripting.broker.ConnectToNeighbourMsgContent;
import mudebs.common.algorithms.scripting.broker.InformationRequestMsgContent;
import mudebs.common.algorithms.scripting.broker.SetLogRequestMsgContent;
import mudebs.common.filters.JavaScriptVisibilityFilter;
import mudebs.communication.javanio.FullDuplexMsgWorker;

import org.apache.log4j.Level;

/**
 * This class is the main of a program to script brokers, that is to say to send
 * commands to brokers.
 * 
 * @author Denis Conan
 * @author Léon Lim
 * 
 */
public class UtilBrokerScripting {

	/**
	 * usage message.
	 */
	private static final String USAGE = "Usage: java "
			+ "mudebs.scripting.BrokerScripting "
			+ Constants.OPTION_URI
			+ " <URI of the broker> "
			+ "\n\t    "
			+ Constants.OPTION_COMMAND
			+ " "
			+ Constants.COMMAND_CONNECT
			+ " "
			+ Constants.OPTION_NEIGHBOUR
			+ " "
			+ "<URI of the new neighbouring broker>"
			+ "\n\t    "
			+ Constants.OPTION_COMMAND
			+ " "
			+ Constants.COMMAND_INFORMATION_REQUEST
			+ " "
			+ Constants.OPTION_COMMAND_WHICH_INFO
			+ " <"
			+ Constants.OPTION_INFO_SCOPE_GRAPHS
			+ "|"
			+ Constants.OPTION_INFO_SLTS
			+ "|"
			+ Constants.OPTION_INFO_RTS
			+ "|"
			+ Constants.OPTION_INFO_ALL
			+ ">"
			+ "\n\t    "
			+ Constants.OPTION_COMMAND
			+ " "
			+ Constants.COMMAND_SETLOG
			+ " "
			+ Constants.OPTION_COMMAND_LOGGER_NAME
			+ " "
			+ " <"
			+ Log.LOGGER_NAME_COMM
			+ "|"
			+ Log.LOGGER_NAME_CONFIG
			+ "|"
			+ Log.LOGGER_NAME_DISPATCH
			+ "|"
			+ Log.LOGGER_NAME_OVERLAY
			+ "|"
			+ Log.LOGGER_NAME_ROUTING
			+ "|"
			+ Log.LOGGER_NAME_SCRIPTING
			+ "> "
			+ Constants.OPTION_COMMAND_LOGGER_LEVEL
			+ " "
			+ "<"
			+ Log.LEVEL_FATAL
			+ "|"
			+ Log.LEVEL_ERROR
			+ "|"
			+ Log.LEVEL_WARN
			+ "|"
			+ Log.LEVEL_INFO
			+ "|"
			+ Log.LEVEL_DEBUG
			+ "|"
			+ Log.LEVEL_TRACE
			+ ">\n\t    "
			+ Constants.OPTION_COMMAND
			+ " "
			+ Constants.COMMAND_JOIN_SCOPE
			+ " "
			+ Constants.OPTION_COMMAND_DIMENSION
			+ " <uri_dimension>"
			+ " "
			+ Constants.OPTION_COMMAND_SUB_SCOPE
			+ " <name_of_subscope>"
			+ " "
			+ Constants.OPTION_COMMAND_SUPER_SCOPE
			+ " <name_of_superscope>"
			+ " ["
			+ Constants.OPTION_COMMAND_MAP_UP_FILTER_FILE
			+ " <map_up_filter_file>"
			+ " "
			+ Constants.OPTION_COMMAND_MAP_DOWN_FILTER_FILE
			+ " <map down filter file>]"
			+ " ["
			+ Constants.OPTION_COMMAND_EXPLICIT_BROKER_SET
			+ "<number_of_brokers set_of_broker_uri_seperated_by_spaces>]"
			+ "\n\t    "
			+ Constants.OPTION_COMMAND
			+ " "
			+ Constants.COMMAND_LEAVE_SCOPE
			+ " "
			+ Constants.OPTION_COMMAND_DIMENSION
			+ " <uri_dimension>"
			+ " "
			+ Constants.OPTION_COMMAND_SUB_SCOPE
			+ " <name_of_subscope>"
			+ " "
			+ Constants.OPTION_COMMAND_SUPER_SCOPE
			+ " <name_of_superscope>"
			+ "\n\t    "
			+ Constants.OPTION_COMMAND
			+ " "
			+ Constants.COMMAND_TERMINATION_DETECTION
			+ "\n\t    "
			+ Constants.OPTION_COMMAND
			+ " "
			+ Constants.COMMAND_TERMINATE_ALL
			+ "\n\t    "
			+ Constants.OPTION_COMMAND
			+ " "
			+ Constants.COMMAND_TERMINATE_NOT_CLIENTS;
	/**
	 * URI of the broker, which is provided as a command line argument.
	 */
	private static String uri = null;
	/**
	 * name of the command to send to the broker and that will be executed by
	 * the broker.
	 */
	private static String command = null;
	/**
	 * URI of the neighbouring broker to connect to.
	 */
	private static String uriNeighbour = null;
	/**
	 * name of the information to ask for.
	 */
	private static String whichinfo = null;
	/**
	 * the dimension for the command join or leave scope.
	 */
	private static String dimension = null;
	/**
	 * the sub-scope for the command join or leave scope.
	 */
	private static String subscope = null;
	/**
	 * the super-scope for the command join or leave scope.
	 */
	private static String superscope = null;
	/**
	 * the file containing the visibility filter from sub-scope to super-scope.
	 */
	private static String mapupFilter = null;
	/**
	 * the content of the file containing the visibility filter from sub-scope
	 * to super-scope.
	 */
	private static String contentMapupFilter = null;
	/**
	 * the file containing the visibility filter from super-scope to sub-scope.
	 */
	private static String mapdownFilter = null;
	/**
	 * the content of the file containing the visibility filter from super-scope
	 * to sub-scope.
	 */
	private static String contentMapdownFilter = null;
	/**
	 * the content of set of URI of brokers that maintain the scope.
	 */
	private static Set<String> contentExplicitBrokerSet = null;
	/**
	 * the logger name.
	 */
	private static String loggerName = null;
	/**
	 * the logger level.
	 */
	private static String logLevel = null;

	public static void reset() {
		uri = null;
		command = null;
		uriNeighbour = null;
		whichinfo = null;
		dimension = null;
		subscope = null;
		superscope = null;
		mapupFilter = null;
		contentMapupFilter = null;
		mapdownFilter = null;
		contentMapdownFilter = null;
		loggerName = null;
		logLevel = null;
		contentExplicitBrokerSet = null;
	}

	public static void readCommandLine(final String[] argv) {
		// with the option "--brokerset" the number of arguments is not
		// necessary even so that the lines are commented
		// if ((argv.length % 2) == 1) {
		// if (Log.ON && Log.CONFIG.isEnabledFor(Level.ERROR)) {
		// Log.CONFIG.error("verify that the number of arguments"
		// + " to the command line is even" + USAGE);
		// }
		// throw new IllegalArgumentException("invalid number of arguments");
		// }
		for (int i = 0; i < argv.length; i = i + 2) {
			if (argv[i].equalsIgnoreCase(Constants.OPTION_URI)) {
				uri = argv[i + 1];
			} else if (argv[i].equalsIgnoreCase(Constants.OPTION_COMMAND)) {
				command = argv[i + 1];
			} else if (argv[i]
					.equalsIgnoreCase(Constants.OPTION_COMMAND_WHICH_INFO)) {
				whichinfo = argv[i + 1];
			} else if (argv[i].equalsIgnoreCase(Constants.OPTION_NEIGHBOUR)) {
				uriNeighbour = argv[i + 1];
			} else if (argv[i]
					.equalsIgnoreCase(Constants.OPTION_COMMAND_DIMENSION)) {
				dimension = argv[i + 1];
			} else if (argv[i]
					.equalsIgnoreCase(Constants.OPTION_COMMAND_SUB_SCOPE)) {
				subscope = argv[i + 1];
			} else if (argv[i]
					.equalsIgnoreCase(Constants.OPTION_COMMAND_SUPER_SCOPE)) {
				superscope = argv[i + 1];
			} else if (argv[i]
					.equalsIgnoreCase(Constants.OPTION_COMMAND_MAP_UP_FILTER_FILE)) {
				mapupFilter = argv[i + 1];
			} else if (argv[i]
					.equalsIgnoreCase(Constants.OPTION_COMMAND_MAP_DOWN_FILTER_FILE)) {
				mapdownFilter = argv[i + 1];
			} else if (argv[i].equalsIgnoreCase(Constants.COMMAND_SETLOG)) {
				command = argv[i + 1];
			} else if (argv[i]
					.equalsIgnoreCase(Constants.OPTION_COMMAND_LOGGER_NAME)) {
				loggerName = argv[i + 1];
			} else if (argv[i]
					.equalsIgnoreCase(Constants.OPTION_COMMAND_LOGGER_LEVEL)) {
				logLevel = argv[i + 1];
			} else if (argv[i]
					.equalsIgnoreCase(Constants.OPTION_COMMAND_EXPLICIT_BROKER_SET)) {
				int nbBroker = Integer.parseInt(argv[i + 1]);
				contentExplicitBrokerSet = new HashSet<String>();
				for (int j = i + 2; j < (i + 2 + nbBroker); j++) {
					contentExplicitBrokerSet.add(argv[j]);
				}
				i = i + 2 + nbBroker;
			}
		}
		if ((uri == null) || (uri.equals("")) || (command == null)
				|| (command.equals(""))) {
			if (Log.ON && Log.CONFIG.isEnabledFor(Level.ERROR)) {
				Log.CONFIG.error("some arguments are missing");
				Log.CONFIG.error(USAGE);
			}
			throw new IllegalArgumentException("argument URI is missing");
		}
	}

	/**
	 * performs the action.
	 */
	public static void work() {
		SocketChannel rwChan;
		Socket rwCon;
		InetSocketAddress rcvAddress;
		URI brokerURI = null;
		Set<URI> explicitBrokerURISet = null;
		try {
			brokerURI = new URI(uri);
		} catch (URISyntaxException e) {
			if (Log.ON && Log.CONFIG.isEnabledFor(Level.ERROR)) {
				Log.CONFIG.error(uri + ", "
						+ Log.printStackTrace(e.getStackTrace()));
			}
			return;
		}
		InetAddress destAddr;
		try {
			destAddr = InetAddress.getByName(brokerURI.getHost());
		} catch (UnknownHostException e) {
			if (Log.ON && Log.COMM.isEnabledFor(Level.ERROR)) {
				Log.COMM.error(uri + ", "
						+ Log.printStackTrace(e.getStackTrace()));
			}
			return;
		}
		FullDuplexMsgWorker msgWorker = null;
		for (int i = 1; i <= Constants.NB_CONNECTION_RETRIES; i++) {
			try {
				rwChan = SocketChannel.open();
			} catch (IOException e) {
				if (Log.ON && Log.COMM.isEnabledFor(Level.ERROR)) {
					Log.COMM.error(uri + ", "
							+ Log.printStackTrace(e.getStackTrace()));
				}
				return;
			}
			rwCon = rwChan.socket();
			rcvAddress = new InetSocketAddress(destAddr, brokerURI.getPort());
			try {
				rwCon.connect(rcvAddress);
			} catch (IOException e) {
				if (Log.ON && Log.COMM.isEnabledFor(Level.DEBUG)) {
					Log.COMM.debug("connection retry to " + brokerURI + " = "
							+ i);
				}
				if (i == Constants.NB_CONNECTION_RETRIES) {
					if (Log.ON && Log.COMM.isEnabledFor(Level.ERROR)) {
						Log.COMM.error(uri + ", max retries reached => give up");
					}
					return;
				}
				try {
					Thread.sleep(Constants.TIME_WAIT_CONNECTION_RETRIES);
				} catch (InterruptedException e1) {
					if (Log.ON && Log.COMM.isEnabledFor(Level.ERROR)) {
						Log.COMM.error(uri + ", "
								+ Log.printStackTrace(e.getStackTrace()));
					}
				}
				continue;
			}
			msgWorker = new FullDuplexMsgWorker(rwChan);
			break;
		}
		try {
			long nb = msgWorker.sendMsg(AlgorithmOverlayManagement.IDENTITY
					.getActionIndex(), new IdentityMsgContent(new URI(
					"mudebs://localhost:0/scripting"), EntityType.SCRIPTING));
			if (Log.ON && Log.OVERLAY.isEnabledFor(Level.DEBUG)) {
				Log.OVERLAY.debug("Identity message sent: " + nb + " bytes");
			}
			if (command.equalsIgnoreCase(Constants.COMMAND_CONNECT)) {
				if (uriNeighbour == null || uriNeighbour.equals("")) {
					if (Log.ON && Log.CONFIG.isEnabledFor(Level.ERROR)) {
						Log.CONFIG.error("uri of neighbour to connect to"
								+ " is missing");
						Log.CONFIG.error(USAGE);
					}
					return;
				}
				URI neighbour = null;
				try {
					neighbour = new URI(uriNeighbour);
				} catch (URISyntaxException e) {
					if (Log.ON && Log.CONFIG.isEnabledFor(Level.ERROR)) {
						Log.CONFIG.error(uri + ", "
								+ Log.printStackTrace(e.getStackTrace()));
					}
					return;
				}
				msgWorker.sendMsg(AlgorithmScriptingBroker.CONNECT_NEIGHBOUR
						.getActionIndex(), new ConnectToNeighbourMsgContent(
						neighbour));
			} else if (command
					.equalsIgnoreCase(Constants.COMMAND_INFORMATION_REQUEST)) {
				if (whichinfo == null || whichinfo.equals("")) {
					if (Log.ON && Log.CONFIG.isEnabledFor(Level.ERROR)) {
						Log.CONFIG.error("whichinfo to display is missing");
						Log.CONFIG.error(USAGE);
					}
					return;
				}
				msgWorker.sendMsg(AlgorithmScriptingBroker.INFORMATION_REQUEST
						.getActionIndex(), new InformationRequestMsgContent(
						whichinfo));
			} else if (command.equalsIgnoreCase(Constants.COMMAND_JOIN_SCOPE)) {
				if (dimension == null || dimension.equals("")) {
					if (Log.ON && Log.CONFIG.isEnabledFor(Level.ERROR)) {
						Log.CONFIG.error("the dimension is missing");
						Log.CONFIG.error(USAGE);
					}
					return;
				}
				URI dimensionURI = null;
				try {
					dimensionURI = new URI(dimension);
				} catch (URISyntaxException e) {
					if (Log.ON && Log.CONFIG.isEnabledFor(Level.ERROR)) {
						Log.CONFIG.error(dimension + ", "
								+ Log.printStackTrace(e.getStackTrace()));
					}
					return;
				}
				if (contentExplicitBrokerSet != null
						&& !contentExplicitBrokerSet.isEmpty()) {
					URI maintainerBrokerURI = null;
					explicitBrokerURISet = new HashSet<URI>();
					for (String maintainerBrokerString : contentExplicitBrokerSet) {
						try {
							maintainerBrokerURI = new URI(
									maintainerBrokerString);
							explicitBrokerURISet.add(maintainerBrokerURI);
						} catch (URISyntaxException e) {
							if (Log.ON && Log.CONFIG.isEnabledFor(Level.ERROR)) {
								Log.CONFIG
										.error(maintainerBrokerString
												+ ", "
												+ Log.printStackTrace(e
														.getStackTrace()));
							}
							return;
						}
					}
				}
				if (subscope == null || subscope.equals("")) {
					if (Log.ON && Log.CONFIG.isEnabledFor(Level.ERROR)) {
						Log.CONFIG.error("the sub-scope is missing");
						Log.CONFIG.error(USAGE);
					}
					return;
				} else if (subscope.equals(ScopeTop.IDENTIFIER)) {
					if (Log.ON && Log.CONFIG.isEnabledFor(Level.ERROR)) {
						Log.CONFIG.error("top cannot be the sub-scope");
					}
					return;
				}
				if (superscope == null || superscope.equals("")) {
					if (Log.ON && Log.CONFIG.isEnabledFor(Level.ERROR)) {
						Log.CONFIG.error("the super-scope is missing");
						Log.CONFIG.error(USAGE);
					}
					return;
				} else if (superscope.equals(ScopeBottom.IDENTIFIER)) {
					if (Log.ON && Log.CONFIG.isEnabledFor(Level.ERROR)) {
						Log.CONFIG.error("bottom cannot be the super-scope");
					}
					return;
				}
				if (subscope.equalsIgnoreCase(ScopeBottom.IDENTIFIER)) {
					contentMapupFilter = JavaScriptVisibilityFilter.ALWAYS_TRUE;
					contentMapdownFilter = JavaScriptVisibilityFilter.ALWAYS_FALSE;
				} else if (superscope.equalsIgnoreCase(ScopeTop.IDENTIFIER)) {
					contentMapupFilter = JavaScriptVisibilityFilter.ALWAYS_TRUE;
					contentMapdownFilter = JavaScriptVisibilityFilter.ALWAYS_FALSE;
				} else {
					if (mapupFilter == null || mapupFilter.equals("")) {
						if (Log.ON && Log.CONFIG.isEnabledFor(Level.ERROR)) {
							Log.CONFIG
									.error("the map up filter file is missing");
							Log.CONFIG.error(USAGE);
						}
						return;
					}
					if (mapdownFilter == null || mapdownFilter.equals("")) {
						if (Log.ON && Log.CONFIG.isEnabledFor(Level.ERROR)) {
							Log.CONFIG
									.error("the map down filter file is missing");
							Log.CONFIG.error(USAGE);
						}
						return;
					}
					try {
						FileInputStream fstream = new FileInputStream(
								mapupFilter);
						DataInputStream in = new DataInputStream(fstream);
						InputStreamReader isr = new InputStreamReader(in,
								"UTF-8");
						BufferedReader br = new BufferedReader(isr);
						String strLine;
						contentMapupFilter = "";
						while ((strLine = br.readLine()) != null) {
							contentMapupFilter += strLine;
						}
						isr.close();
						in.close();
						fstream.close();
					} catch (FileNotFoundException e) {
						if (Log.ON && Log.CONFIG.isEnabledFor(Level.ERROR)) {
							Log.CONFIG.error(uri + ", "
									+ Log.printStackTrace(e.getStackTrace()));
						}
						return;
					}
					if (mapdownFilter == null || mapdownFilter.equals("")) {
						if (Log.ON && Log.CONFIG.isEnabledFor(Level.ERROR)) {
							Log.CONFIG
									.error("the map down filter file is missing");
							Log.CONFIG.error(USAGE);
						}
						return;
					}
					try {
						FileInputStream fstream = new FileInputStream(
								mapdownFilter);
						DataInputStream in = new DataInputStream(fstream);
						InputStreamReader isr = new InputStreamReader(in,
								"UTF-8");
						BufferedReader br = new BufferedReader(isr);
						String strLine;
						contentMapdownFilter = "";
						while ((strLine = br.readLine()) != null) {
							contentMapdownFilter += strLine;
						}
						isr.close();
						in.close();
						fstream.close();
					} catch (FileNotFoundException e) {
						if (Log.ON && Log.CONFIG.isEnabledFor(Level.ERROR)) {
							Log.CONFIG.error(uri + ", "
									+ Log.printStackTrace(e.getStackTrace()));
						}
						return;
					}
				}
				msgWorker.sendMsg(AlgorithmScriptingBroker.JOIN_SCOPE
						.getActionIndex(), new JoinScopeMsgContent(brokerURI,
						0, dimensionURI, subscope, superscope,
						contentMapupFilter, contentMapdownFilter,
						explicitBrokerURISet));
			} else if (command.equalsIgnoreCase(Constants.COMMAND_LEAVE_SCOPE)) {
				if (dimension == null || dimension.equals("")) {
					if (Log.ON && Log.CONFIG.isEnabledFor(Level.ERROR)) {
						Log.CONFIG.error("the dimension is missing");
						Log.CONFIG.error(USAGE);
					}
					return;
				}
				URI dimensionURI = null;
				try {
					dimensionURI = new URI(dimension);
				} catch (URISyntaxException e) {
					if (Log.ON && Log.CONFIG.isEnabledFor(Level.ERROR)) {
						Log.CONFIG.error(dimension + ", "
								+ Log.printStackTrace(e.getStackTrace()));
					}
					return;
				}
				if (subscope == null || subscope.equals("")) {
					if (Log.ON && Log.CONFIG.isEnabledFor(Level.ERROR)) {
						Log.CONFIG.error("the sub-scope is missing");
						Log.CONFIG.error(USAGE);
					}
					return;
				}
				if (superscope == null || superscope.equals("")) {
					if (Log.ON && Log.CONFIG.isEnabledFor(Level.ERROR)) {
						Log.CONFIG.error("the super-scope is missing");
						Log.CONFIG.error(USAGE);
					}
					return;
				}
				msgWorker.sendMsg(AlgorithmScriptingBroker.LEAVE_SCOPE
						.getActionIndex(), new LeaveScopeMsgContent(brokerURI,
						0, new Scope(dimensionURI, subscope), new Scope(
								dimensionURI, superscope)));
			} else if (command.equalsIgnoreCase(Constants.COMMAND_SETLOG)) {
				if (loggerName == null || loggerName.equals("")) {
					if (Log.ON && Log.CONFIG.isEnabledFor(Level.ERROR)) {
						Log.CONFIG.error("the name of the logger is missing");
						Log.CONFIG.error(USAGE);
					}
					return;
				}
				if (logLevel == null || logLevel.equals("")) {
					if (Log.ON && Log.CONFIG.isEnabledFor(Level.ERROR)) {
						Log.CONFIG.error("the level of the logger is missing");
						Log.CONFIG.error(USAGE);
					}
					return;
				}
				msgWorker.sendMsg(AlgorithmScriptingBroker.SETLOG_REQUEST
						.getActionIndex(), new SetLogRequestMsgContent(
						loggerName, logLevel));
			} else if (command
					.equalsIgnoreCase(Constants.COMMAND_TERMINATION_DETECTION)) {
				msgWorker.sendMsg(
						AlgorithmScriptingBroker.TERMINATION_DETECTION
								.getActionIndex(),
						new TerminationDetectionMsgContent(null, new URI(
								"mudebs://localhost:0/scripting"), 0, 0,
								ColorTerminationDetection.White, null));
			} else if (command
					.equalsIgnoreCase(Constants.COMMAND_TERMINATE_ALL)) {
				msgWorker.sendMsg(AlgorithmScriptingBroker.TERMINATION
						.getActionIndex(), new TerminationMsgContent(new URI(
						"mudebs://localhost:0/scripting"), true));
			} else if (command
					.equalsIgnoreCase(Constants.COMMAND_TERMINATE_NOT_CLIENTS)) {
				msgWorker.sendMsg(AlgorithmScriptingBroker.TERMINATION
						.getActionIndex(), new TerminationMsgContent(new URI(
						"mudebs://localhost:0/scripting"), false));
			}
		} catch (IOException e) {
			if (Log.ON && Log.COMM.isEnabledFor(Level.ERROR)) {
				Log.COMM.error(uri + ", "
						+ Log.printStackTrace(e.getStackTrace()));
			}
		} catch (URISyntaxException e) {
			if (Log.ON && Log.COMM.isEnabledFor(Level.ERROR)) {
				Log.COMM.error(uri + ", "
						+ Log.printStackTrace(e.getStackTrace()));
			}
		}
		try {
			msgWorker.close();
		} catch (IOException e) {
			if (Log.ON && Log.COMM.isEnabledFor(Level.WARN)) {
				Log.COMM.warn(uri + ", "
						+ Log.printStackTrace(e.getStackTrace()));
			}
		}
	}
}
