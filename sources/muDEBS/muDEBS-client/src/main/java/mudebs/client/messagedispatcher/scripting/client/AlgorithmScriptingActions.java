/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */
package mudebs.client.messagedispatcher.scripting.client;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import mudebs.client.messagedispatcher.MessageDispatcherState;
import mudebs.common.Log;
import mudebs.common.MuDEBSException;
import mudebs.common.algorithms.ACK;
import mudebs.common.algorithms.AbstractMessageContent;
import mudebs.common.algorithms.CommandResult;
import mudebs.common.algorithms.Reply;
import mudebs.common.algorithms.State;
import mudebs.common.algorithms.routing.AdminPublication;
import mudebs.common.algorithms.routing.AdvertisementMsgContent;
import mudebs.common.algorithms.routing.CollectivePublicationMsgContent;
import mudebs.common.algorithms.routing.CollectiveSubscriptionMsgContent;
import mudebs.common.algorithms.routing.Publication;
import mudebs.common.algorithms.routing.PublicationMsgContent;
import mudebs.common.algorithms.routing.RequestMsgContent;
import mudebs.common.algorithms.routing.Scope;
import mudebs.common.algorithms.routing.SubscriptionMsgContent;
import mudebs.common.algorithms.routing.UnadvertisementMsgContent;
import mudebs.common.algorithms.routing.UnsubscriptionMsgContent;

import org.apache.log4j.Level;

/**
 * This class defines the methods implementing the reaction concerning the
 * reception of scripting messages.
 * 
 * @author Denis Conan
 * 
 */
class AlgorithmScriptingActions {
	public static void receiveAdCommand(final State state,
			final AbstractMessageContent content) throws Exception {
		MessageDispatcherState cstate = (MessageDispatcherState) state;
		AdvertisementMsgContent mymsg = (AdvertisementMsgContent) content;
		try {
			cstate.semaphore.acquire();
		} catch (InterruptedException e) {
			if (Log.ON && Log.SCRIPTING.isEnabledFor(Level.DEBUG)) {
				Log.SCRIPTING.debug(cstate.identity.getPath() + ", "
						+ e.getMessage() + "\n"
						+ Log.printStackTrace(e.getStackTrace()));
			}
			return;
		}
		try {
			if (Log.ON && Log.SCRIPTING.isEnabledFor(Level.DEBUG)) {
				Log.SCRIPTING.debug(cstate.identity.getPath()
						+ ", scripted advertisement = " + mymsg);
			}
			CommandResult result = cstate.clientDelegate.advertise(
					mymsg.getIdentifier(), mymsg.getOperationalMode(),
					mymsg.getAck(), mymsg.getRoutingFilter(),
					mymsg.getPhiNotif(), mymsg.getPolicyContent());
			if (mymsg.getAck() != ACK.NO) {
				if (Log.ON && Log.SCRIPTING.isEnabledFor(Level.INFO)) {
					Log.SCRIPTING.info(cstate.identity.getPath()
							+ ", command result of advertise ("
							+ mymsg.getIdentifier() + ") is " + result);
				}
			}
		} catch (MuDEBSException e) {
			if (Log.ON && Log.SCRIPTING.isEnabledFor(Level.DEBUG)) {
				Log.SCRIPTING.debug(cstate.identity.getPath() + ", "
						+ e.getMessage() + "\n" + e.getMessage() + "\n"
						+ Log.printStackTrace(e.getStackTrace()));
			}
		}
		cstate.semaphore.release();
	}

	public static void receivePubCommand(final State state,
			final AbstractMessageContent content) {
		MessageDispatcherState cstate = (MessageDispatcherState) state;
		PublicationMsgContent mymsg = (PublicationMsgContent) content;
		try {
			cstate.semaphore.acquire();
		} catch (InterruptedException e) {
			if (Log.ON && Log.SCRIPTING.isEnabledFor(Level.DEBUG)) {
				Log.SCRIPTING.debug(cstate.identity.getPath() + ", "
						+ e.getMessage() + "\n"
						+ Log.printStackTrace(e.getStackTrace()));
			}
			return;
		}
		try {
			if (Log.ON && Log.SCRIPTING.isEnabledFor(Level.DEBUG)) {
				Log.SCRIPTING.debug(cstate.identity.getPath()
						+ ", scripted publication = " + mymsg);
			}
			List<URI> path = new ArrayList<URI>();
			path.add(cstate.identity);
			CommandResult result = cstate.clientDelegate.publish(
					mymsg.getIdAdv(), mymsg.getAck(), mymsg.getContent());
			if (mymsg.getAck() != ACK.NO) {
				if (Log.ON && Log.SCRIPTING.isEnabledFor(Level.INFO)) {
					Log.SCRIPTING.info(cstate.identity.getPath()
							+ ", command result of publication is " + result);
				}
			}
		} catch (MuDEBSException e) {
			if (Log.ON && Log.SCRIPTING.isEnabledFor(Level.DEBUG)) {
				Log.SCRIPTING.debug(cstate.identity.getPath() + ", "
						+ e.getMessage() + "\n"
						+ Log.printStackTrace(e.getStackTrace()));
			}
		}
		cstate.semaphore.release();
	}

	public static void receiveCollectivePubCommand(final State state,
			final AbstractMessageContent content) {
		MessageDispatcherState cstate = (MessageDispatcherState) state;
		CollectivePublicationMsgContent mymsg = (CollectivePublicationMsgContent) content;
		try {
			cstate.semaphore.acquire();
		} catch (InterruptedException e) {
			if (Log.ON && Log.SCRIPTING.isEnabledFor(Level.DEBUG)) {
				Log.SCRIPTING.debug(cstate.identity.getPath() + ", "
						+ e.getMessage() + "\n"
						+ Log.printStackTrace(e.getStackTrace()));
			}
			return;
		}
		try {
			if (Log.ON && Log.SCRIPTING.isEnabledFor(Level.DEBUG)) {
				Log.SCRIPTING.debug(cstate.identity.getPath()
						+ ", scripted collective publication = " + mymsg);
			}
			List<URI> path = new ArrayList<URI>();
			path.add(cstate.identity);
			List<Publication> toPublish = new ArrayList<Publication>();
			for (AdminPublication pub : mymsg.getPublications()) {
				if (pub != null) {
					toPublish.add(new Publication(pub.getIdAdv(), pub
							.getSequenceNumber(), pub.getContent()));
				}
			}
			CommandResult result = cstate.clientDelegate.publish(toPublish,
					mymsg.getAck());
			if (mymsg.getAck() != ACK.NO) {
				if (Log.ON && Log.SCRIPTING.isEnabledFor(Level.INFO)) {
					Log.SCRIPTING.info(cstate.identity.getPath()
							+ ", command result of collective publication is "
							+ result);
				}
			}
		} catch (MuDEBSException e) {
			if (Log.ON && Log.SCRIPTING.isEnabledFor(Level.DEBUG)) {
				Log.SCRIPTING.debug(cstate.identity.getPath() + ", "
						+ e.getMessage() + "\n"
						+ Log.printStackTrace(e.getStackTrace()));
			}
		}
		cstate.semaphore.release();
	}

	public static void receiveReqCommand(final State state,
			final AbstractMessageContent content) {
		MessageDispatcherState cstate = (MessageDispatcherState) state;
		RequestMsgContent mymsg = (RequestMsgContent) content;
		try {
			cstate.semaphore.acquire();
		} catch (InterruptedException e) {
			if (Log.ON && Log.SCRIPTING.isEnabledFor(Level.DEBUG)) {
				Log.SCRIPTING.debug(cstate.identity.getPath() + ", "
						+ e.getMessage() + "\n"
						+ Log.printStackTrace(e.getStackTrace()));
			}
			return;
		}
		try {
			if (Log.ON && Log.SCRIPTING.isEnabledFor(Level.DEBUG)) {
				Log.SCRIPTING.debug(cstate.identity.getPath()
						+ ", scripted request = " + mymsg);
			}
			List<Reply> result = cstate.clientDelegate.request(
					mymsg.getLocal(), mymsg.getFilter(), mymsg.getPhi(),
					mymsg.getABACInformation());
			if (Log.ON && Log.SCRIPTING.isEnabledFor(Level.INFO)) {
				Log.SCRIPTING.info(cstate.identity.getPath()
						+ ", command result of request is " + result);
			}
		} catch (MuDEBSException e) {
			if (Log.ON && Log.SCRIPTING.isEnabledFor(Level.DEBUG)) {
				Log.SCRIPTING.debug(cstate.identity.getPath() + ", "
						+ e.getMessage() + "\n"
						+ Log.printStackTrace(e.getStackTrace()));
			}
		}
		cstate.semaphore.release();
	}

	public static void receiveSubCommand(final State state,
			final AbstractMessageContent content) {
		MessageDispatcherState cstate = (MessageDispatcherState) state;
		SubscriptionMsgContent mymsg = (SubscriptionMsgContent) content;
		try {
			cstate.semaphore.acquire();
		} catch (InterruptedException e) {
			if (Log.ON && Log.SCRIPTING.isEnabledFor(Level.DEBUG)) {
				Log.SCRIPTING.debug(cstate.identity.getPath() + ", "
						+ e.getMessage() + "\n"
						+ Log.printStackTrace(e.getStackTrace()));
			}
			return;
		}
		try {
			if (Log.ON && Log.SCRIPTING.isEnabledFor(Level.DEBUG)) {
				Log.SCRIPTING.debug(cstate.identity.getPath()
						+ ", scripted subscription = " + mymsg);
			}
			CommandResult result = cstate.clientDelegate.subscribe(
					mymsg.getFilterId(), mymsg.getOperationalMode(),
					mymsg.getAck(), mymsg.getRoutingFilter(),
					mymsg.getPhiSub(), mymsg.getABACInformation());
			if (mymsg.getAck() != ACK.NO) {
				if (Log.ON && Log.SCRIPTING.isEnabledFor(Level.INFO)) {
					Log.SCRIPTING.info(cstate.identity.getPath()
							+ ", command result of subscribe ("
							+ mymsg.getFilterId() + ") is " + result);
				}
			}
		} catch (MuDEBSException e) {
			if (Log.ON && Log.SCRIPTING.isEnabledFor(Level.DEBUG)) {
				Log.SCRIPTING.debug(cstate.identity.getPath() + ", "
						+ e.getMessage() + "\n"
						+ Log.printStackTrace(e.getStackTrace()));
			}
		}
		cstate.semaphore.release();
	}

	public static void receiveCollectiveSubCommand(final State state,
			final AbstractMessageContent content) {
		MessageDispatcherState cstate = (MessageDispatcherState) state;
		CollectiveSubscriptionMsgContent mymsg = (CollectiveSubscriptionMsgContent) content;
		try {
			cstate.semaphore.acquire();
		} catch (InterruptedException e) {
			if (Log.ON && Log.SCRIPTING.isEnabledFor(Level.DEBUG)) {
				Log.SCRIPTING.debug(cstate.identity.getPath() + ", "
						+ e.getMessage() + "\n"
						+ Log.printStackTrace(e.getStackTrace()));
			}
			return;
		}
		try {
			if (Log.ON && Log.SCRIPTING.isEnabledFor(Level.DEBUG)) {
				Log.SCRIPTING.debug(cstate.identity.getPath()
						+ ", scripted subscription = " + mymsg);
			}
			@SuppressWarnings("unused")
			CommandResult result = cstate.clientDelegate.subscribe(
					mymsg.getOperationalMode(), mymsg.getACK(),
					mymsg.getSubscriptions());
		} catch (MuDEBSException e) {
			if (Log.ON && Log.SCRIPTING.isEnabledFor(Level.DEBUG)) {
				Log.SCRIPTING.debug(cstate.identity.getPath() + ", "
						+ e.getMessage() + "\n"
						+ Log.printStackTrace(e.getStackTrace()));
			}
		}
		cstate.semaphore.release();
	}

	public static void receiveEndCommand(final State state,
			final AbstractMessageContent content) {
		if (Log.ON && Log.ROUTING.isEnabledFor(Level.DEBUG)) {
			Log.ROUTING.debug("scripting request for the termination");
		}
		MessageDispatcherState cstate = (MessageDispatcherState) state;
		try {
			cstate.semaphore.acquire();
		} catch (InterruptedException e) {
			if (Log.ON && Log.SCRIPTING.isEnabledFor(Level.DEBUG)) {
				Log.SCRIPTING.debug(cstate.identity.getPath() + ", "
						+ e.getMessage() + "\n"
						+ Log.printStackTrace(e.getStackTrace()));
			}
			return;
		}
		// forces the termination, no means to know which threads to stop
		System.exit(0);
	}

	public static void receiveUnadCommand(final State state,
			final AbstractMessageContent content) {
		MessageDispatcherState cstate = (MessageDispatcherState) state;
		UnadvertisementMsgContent mymsg = (UnadvertisementMsgContent) content;
		try {
			cstate.semaphore.acquire();
		} catch (InterruptedException e) {
			if (Log.ON && Log.SCRIPTING.isEnabledFor(Level.DEBUG)) {
				Log.SCRIPTING.debug(cstate.identity.getPath() + ", "
						+ e.getMessage() + "\n"
						+ Log.printStackTrace(e.getStackTrace()));
			}
			return;
		}
		try {
			if (Log.ON && Log.SCRIPTING.isEnabledFor(Level.DEBUG)) {
				if (Log.ON && Log.SCRIPTING.isEnabledFor(Level.INFO)) {
					Log.SCRIPTING.info(cstate.identity.getPath()
							+ ", command result of unadvertise is " + mymsg);
				}
				Log.SCRIPTING.debug(cstate.identity.getPath()
						+ ", scripted unadvertisement = " + mymsg);
			}
			CommandResult result = null;
			if (mymsg.getIdentifier() == null) {
				result = cstate.clientDelegate.unadvertiseAll(mymsg.getAck());
			} else {
				result = cstate.clientDelegate.unadvertise(
						mymsg.getIdentifier(), mymsg.getAck());
			}
			if (mymsg.getAck() != ACK.NO) {
				System.out.println(cstate.identity.getPath()
						+ ", command result of unadvertise ("
						+ mymsg.getIdentifier() + ") is " + result);
			}
		} catch (MuDEBSException e) {
			if (Log.ON && Log.SCRIPTING.isEnabledFor(Level.DEBUG)) {
				Log.SCRIPTING.debug(cstate.identity.getPath() + ", "
						+ e.getMessage() + "\n"
						+ Log.printStackTrace(e.getStackTrace()));
			}
		}
		cstate.semaphore.release();
	}

	public static void receiveDisconnectReconnectCommand(final State state,
			final AbstractMessageContent content) {
		if (Log.ON && Log.ROUTING.isEnabledFor(Level.DEBUG)) {
			Log.ROUTING.debug("scripting request for disconnection "
					+ "and reconnection");
		}
		System.out.println("The disconnection and reconnection functionality "
				+ "is temporarily disable (since March 2014)");
		// MessageDispatcherState cstate = (MessageDispatcherState) state;
		// DisconnectReconnectMsgContent mymsg = (DisconnectReconnectMsgContent)
		// content;
		// try {
		// cstate.semaphore.acquire();
		// } catch (InterruptedException e) {
		// if (Log.ON && Log.SCRIPTING.isEnabledFor(Level.DEBUG)) {
		// Log.SCRIPTING.debug(cstate.identity.getPath() + ", " + e.getMessage()
		// + "\n"
		// + Log.printStackTrace(e.getStackTrace()));
		// }
		// return;
		// }
		// try {
		// HashMap<String, String> advertisements = new HashMap<String,
		// String>();
		// advertisements.putAll(cstate.localAdvertisements);
		// if (Log.ON && Log.COMM.isEnabledFor(Level.TRACE)) {
		// Log.COMM.trace(cstate.identity.getPath()
		// + ", cstate.advertisements before disconnection ="
		// + cstate.localAdvertisements);
		// }
		// cstate.clientDelegate.unadvertiseAll(ACK.NO);
		// HashMap<String, String> subscriptions = new HashMap<String,
		// String>();
		// subscriptions.putAll(cstate.globalSubscriptions);
		// if (Log.ON && Log.COMM.isEnabledFor(Level.TRACE)) {
		// Log.COMM.trace(cstate.identity.getPath()
		// + ", cstate.subscription before disconnection ="
		// + cstate.globalSubscriptions);
		// }
		// cstate.clientDelegate.unsubscribeAll(ACK.NO);
		// cstate.messageDispatcher.disconnectFromReconnectToBroker(mymsg
		// .getNewBroker());
		// for (String identifier : advertisements.keySet()) {
		// String elems[] = identifier.split("/");
		// cstate.clientDelegate.advertise(elems[elems.length - 1], true,
		// ACK.NO, advertisements.get(identifier));
		// }
		// if (Log.ON && Log.COMM.isEnabledFor(Level.TRACE)) {
		// Log.COMM.trace(cstate.identity.getPath()
		// + ", cstate.advertisements after reconnection ="
		// + cstate.localAdvertisements);
		// }
		// for (String identifier : subscriptions.keySet()) {
		// String elems[] = identifier.split("/");
		// cstate.clientDelegate.subscribe(elems[elems.length - 1], false,
		// ACK.NO, subscriptions.get(identifier));
		// }
		// if (Log.ON && Log.COMM.isEnabledFor(Level.TRACE)) {
		// Log.COMM.trace(cstate.identity.getPath()
		// + ", cstate.subscriptions after reconnetion ="
		// + cstate.globalSubscriptions);
		// }
		// } catch (IOException e) {
		// if (Log.ON && Log.DISPATCH.isEnabledFor(Level.DEBUG)) {
		// Log.DISPATCH.debug(cstate.identity.getPath() + ", "
		// + Log.printStackTrace(e.getStackTrace()));
		// }
		// cstate.semaphore.release();
		// } catch (MuDEBSException e) {
		// if (Log.ON && Log.DISPATCH.isEnabledFor(Level.DEBUG)) {
		// Log.DISPATCH.debug(cstate.identity.getPath() + ", "+ e.getMessage() +
		// "\n"
		// + Log.printStackTrace(e.getStackTrace()));
		// }
		// }
		// cstate.semaphore.release();
	}

	public static void receiveUnsubCommand(final State state,
			final AbstractMessageContent content) {
		MessageDispatcherState cstate = (MessageDispatcherState) state;
		UnsubscriptionMsgContent mymsg = (UnsubscriptionMsgContent) content;
		try {
			cstate.semaphore.acquire();
		} catch (InterruptedException e) {
			if (Log.ON && Log.SCRIPTING.isEnabledFor(Level.DEBUG)) {
				Log.SCRIPTING.debug(cstate.identity.getPath() + ", "
						+ e.getMessage() + "\n"
						+ Log.printStackTrace(e.getStackTrace()));
			}
			return;
		}
		try {
			if (Log.ON && Log.SCRIPTING.isEnabledFor(Level.DEBUG)) {
				Log.SCRIPTING.debug(cstate.identity.getPath()
						+ ", scripted unsubscription = " + mymsg);
			}
			CommandResult result = null;
			if (mymsg.getIdentifier() == null) {
				result = cstate.clientDelegate.unsubscribeAll(mymsg.getAck());
			} else {
				result = cstate.clientDelegate.unsubscribe(
						mymsg.getIdentifier(), mymsg.getAck());
			}
			if (mymsg.getAck() != ACK.NO) {
				if (Log.ON && Log.SCRIPTING.isEnabledFor(Level.INFO)) {
					Log.SCRIPTING.info(cstate.identity.getPath()
							+ ", command result of unsubscribe ("
							+ mymsg.getIdentifier() + ") is " + result);
				}
			}
		} catch (MuDEBSException e) {
			if (Log.ON && Log.SCRIPTING.isEnabledFor(Level.DEBUG)) {
				Log.SCRIPTING.debug(cstate.identity.getPath() + ", "
						+ e.getMessage() + "\n"
						+ Log.printStackTrace(e.getStackTrace()));
			}
		}
		cstate.semaphore.release();
	}

	public static void receiveGetscopesCommand(final State state,
			final AbstractMessageContent content) {
		MessageDispatcherState cstate = (MessageDispatcherState) state;
		try {
			cstate.semaphore.acquire();
		} catch (InterruptedException e) {
			if (Log.ON && Log.SCRIPTING.isEnabledFor(Level.DEBUG)) {
				Log.SCRIPTING.debug(cstate.identity.getPath() + ", "
						+ e.getMessage() + "\n"
						+ Log.printStackTrace(e.getStackTrace()));
			}
			return;
		}
		try {
			if (Log.ON && Log.SCRIPTING.isEnabledFor(Level.DEBUG)) {
				Log.SCRIPTING.debug(cstate.identity.getPath()
						+ ", scripted get scopes");
			}
			List<Scope> result = null;
			result = cstate.clientDelegate.getScopesOfBroker();
			if (Log.ON && Log.SCRIPTING.isEnabledFor(Level.INFO)) {
				Log.SCRIPTING.info(cstate.identity.getPath()
						+ ", command result of get scopes=" + result);
			}
		} catch (MuDEBSException e) {
			if (Log.ON && Log.SCRIPTING.isEnabledFor(Level.INFO)) {
				Log.SCRIPTING.info(cstate.identity.getPath() + ", "
						+ e.getMessage() + "\n"
						+ Log.printStackTrace(e.getStackTrace()));
			}
		}
		cstate.semaphore.release();
	}
}
