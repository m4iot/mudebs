/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2017 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Christian Bac and Denis Conan
Contributor(s):
 */
package mudebs.client.messagedispatcher;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import mudebs.client.api.Consumer;
import mudebs.client.messagedispatcher.overlaymanagement.AlgorithmOverlayManagement;
import mudebs.common.Constants;
import mudebs.common.Log;
import mudebs.common.algorithms.EntityType;
import mudebs.common.algorithms.overlaymanagement.IdentityMsgContent;
import mudebs.communication.javanio.FullDuplexMsgWorker;
import mudebs.communication.javanio.ReadMessageStatus;

import org.apache.log4j.Level;

/**
 * This class is the thread of the message dispatcher of the client. It connects
 * to a broker, and enters an infinite loop for receiving messages and executing
 * the corresponding actions of the client.
 * 
 * @author Christian Bac
 * @author Denis Conan
 * 
 */
public class MessageDispatcherThread extends Thread {
	/**
	 * selector object for the main loop waiting for connections and messages.
	 */
	private Selector selector;
	/**
	 * selection keys for accepting connections from scripting clients.
	 */
	private SelectionKey acceptSelectionKey;
	/**
	 * client socket channel for accepting scripting client connections.
	 */
	private ServerSocketChannel listenChannel;
	/**
	 * selection keys of the scripting client message workers.
	 */
	private HashMap<SelectionKey, FullDuplexMsgWorker> allScriptingClientWorkers;
	/**
	 * client's state for the distributed algorithms implemented in the client.
	 */
	private MessageDispatcherState state;
	/**
	 * message worker of this client for exchanging messages with the broker.
	 */
	private FullDuplexMsgWorker brokerMsgWorker = null;
	/**
	 * selection key of the connection to the broker.
	 */
	private SelectionKey brokerKey;
	/**
	 * the termination of the message dispatcher thread is asked for.
	 */
	private boolean termination = false;

	/**
	 * Constructor of the client message dispatcher.
	 * 
	 * @param s
	 *            the state of the message dispatcher.
	 * @param identity
	 *            the URI of this client.
	 * @param broker
	 *            the URI of the broker of the client.
	 * @param consumer
	 *            the consumer defining the method <tt>consume</tt> for the
	 *            logic of the client when receiving a publication.
	 * @throws URISyntaxException
	 *             exception thrown when the URI is not correct.
	 * @throws IOException
	 *             exception thrown when problem in creating the socket to
	 *             accept connections.
	 */
	public MessageDispatcherThread(final MessageDispatcherState s,
			final String identity, final String broker, final Consumer consumer)
			throws URISyntaxException, IOException {
		state = s;
		state.messageDispatcher = this;
		state.consumer = consumer;
		state.localAdvertisements = new HashMap<String, String>();
		state.globalSubscriptions = new HashMap<String, String>();
		state.lastPublicationsReceived = new HashMap<URI, HashMap<String, Integer>>();
		try {
			state.identity = new URI(identity);
		} catch (URISyntaxException e) {
			throw new URISyntaxException(identity,
					"The URI of the client is malformed");
		}
		try {
			state.brokerIdentity = new URI(broker);
		} catch (URISyntaxException e) {
			throw new URISyntaxException(identity,
					"The URI of the broker is malformed");
		}
		if (!state.identity.getScheme().equalsIgnoreCase("muDEBS")) {
			throw new URISyntaxException(state.identity.getScheme(),
					"The protocol of the URI "
							+ "of this client is not 'muDEBS'.");
		}
		if (state.identity.getPath() == null) {
			throw new URISyntaxException(state.identity.getPath(),
					"There is no path specified in the URI."
							+ " Your must specify the name of this client.");
		}
		if (Log.ON && Log.CONFIG.isEnabledFor(Level.DEBUG)) {
			Log.CONFIG.debug(state.identity.getPath() + ", identity = "
					+ state.identity);
		}
		allScriptingClientWorkers = new HashMap<SelectionKey, FullDuplexMsgWorker>();
		InetSocketAddress rcvAddress;
		ServerSocket listenSocket;
		// creates the selector and the listener
		selector = Selector.open();
		listenChannel = ServerSocketChannel.open();
		listenSocket = listenChannel.socket();
		rcvAddress = new InetSocketAddress(state.identity.getPort());
		listenSocket.setReuseAddress(true);
		listenSocket.bind(rcvAddress);
		listenChannel.configureBlocking(false);
		// register selection keys
		acceptSelectionKey = listenChannel.register(selector,
				SelectionKey.OP_ACCEPT);
	}

	/**
	 * gets the reference to the state of the message dispatcher.
	 * 
	 * Be careful! Access to state variables must be made in mutual exclusion by
	 * acquiring the attribute semaphore.
	 * 
	 * @return the reference to the state.
	 */
	public MessageDispatcherState getMsgDispatcherState() {
		return state;
	}

	/**
	 * accept connection (socket level), create MsgWorker, and register
	 * selection key of a client or a broker. This method is called when
	 * accepting a connection from a client or a broker.
	 * 
	 * NB: begin with adding the selection key to the set of selection keys of
	 * clients. Afterwards, if the identity message stipulates that it is a
	 * broker, then move the selection key to the set of keys of brokers.
	 * 
	 * @param socketChannel
	 *            socket channel.
	 * @throws IOException
	 */
	private void acceptNewConnection(final ServerSocketChannel socketChannel)
			throws IOException {
		SocketChannel rwChan;
		SelectionKey newKey;
		rwChan = socketChannel.accept();
		if (rwChan != null) {
			try {
				FullDuplexMsgWorker worker = new FullDuplexMsgWorker(rwChan);
				worker.configureNonBlocking();
				newKey = rwChan.register(selector, SelectionKey.OP_READ);
				allScriptingClientWorkers.put(newKey, worker);
				if (Log.ON && Log.COMM.isEnabledFor(Level.DEBUG)) {
					Log.COMM.debug(state.identity.getPath()
							+ ", accepting a new connection");
				}
				if (Log.ON && Log.COMM.isEnabledFor(Level.TRACE)) {
					Log.COMM.trace(state.identity.getPath()
							+ ", allScriptingClientWorkers.size() = "
							+ allScriptingClientWorkers.size());
				}
			} catch (ClosedChannelException e) {
				if (Log.ON && Log.COMM.isEnabledFor(Level.ERROR)) {
					Log.COMM.error(state.identity.getPath() + ", "
							+ e.getMessage() + "\n"
							+ Log.printStackTrace(e.getStackTrace()));
				}
			}
		}
	}

	/**
	 * connects the client to its broker.
	 * 
	 * @param uri
	 *            the URI of the broker, which the client is connected to.
	 * @throws IOException
	 *             the exception thrown when there exists a problem in
	 *             connection making.
	 */
	private void connectToBroker(final URI uri) throws IOException {
		if (brokerKey != null) {
			if (Log.ON && Log.COMM.isEnabledFor(Level.WARN)) {
				Log.COMM.warn(state.identity.getPath()
						+ ", the client is already connected to a broker");
			}
			return;
		}
		state.brokerIdentity = uri;
		SocketChannel rwChan;
		Socket rwCon;
		InetSocketAddress rcvAddress;
		InetAddress destAddr = InetAddress
				.getByName(state.brokerIdentity.getHost());
		for (int i = 1; i <= Constants.NB_CONNECTION_RETRIES; i++) {
			rwChan = SocketChannel.open();
			rwCon = rwChan.socket();
			rcvAddress = new InetSocketAddress(destAddr,
					state.brokerIdentity.getPort());
			try {
				rwCon.connect(rcvAddress);
			} catch (IOException e) {
				if (Log.ON && Log.COMM.isEnabledFor(Level.DEBUG)) {
					Log.COMM.debug("connection retry to " + uri + " = " + i);
				}
				if (i == Constants.NB_CONNECTION_RETRIES) {
					if (Log.ON && Log.COMM.isEnabledFor(Level.ERROR)) {
						Log.COMM.error(state.identity
								+ ", max retries reached => give up");
					}
					throw e;
				}
				try {
					Thread.sleep(Constants.TIME_WAIT_CONNECTION_RETRIES);
				} catch (InterruptedException e1) {
					if (Log.ON && Log.COMM.isEnabledFor(Level.FATAL)) {
						Log.COMM.fatal(state.identity + ", "
								+ Log.printStackTrace(e.getStackTrace()));
					}
				}
				continue;
			}
			brokerMsgWorker = new FullDuplexMsgWorker(rwChan);
			state.brokerMsgWorker = brokerMsgWorker;
			brokerMsgWorker.configureNonBlocking();
			brokerKey = rwChan.register(selector, SelectionKey.OP_READ);
			break;
		}
		long nb = brokerMsgWorker.sendMsg(
				AlgorithmOverlayManagement.IDENTITY.getActionIndex(),
				new IdentityMsgContent(state.identity, EntityType.CLIENT));
		if (Log.ON && Log.OVERLAY.isEnabledFor(Level.DEBUG)) {
			Log.OVERLAY.debug(state.identity.getPath()
					+ ", identity message sent: " + nb + " bytes");
		}
		state.waitConnectBrokerSemaphore.release();
	}

	/**
	 * disconnects the client to its current broker and reconnect to a new one.
	 * 
	 * @param newBroker
	 *            the URI of the new broker, which the client is connected to.
	 * @throws IOException
	 *             the exception thrown when there exists a problem in
	 *             connection making.
	 */
	public void disconnectFromReconnectToBroker(final URI newBroker)
			throws IOException {
		if (brokerKey != null) {
			brokerKey.channel().close();
			brokerKey.cancel();
			brokerMsgWorker.close();
		}
		brokerKey = null;
		brokerMsgWorker = null;
		connectToBroker(newBroker);
	}

	/**
	 * send a message to the broker, which the client is connected to
	 * 
	 * @param type
	 *            the message's type.
	 * @param content
	 *            the message as an object to be serialised with Gson.
	 * @throws IOException
	 *             the exception thrown in case of problem.
	 */
	public void sendToBroker(final int type, final Object content)
			throws IOException {
		brokerMsgWorker.sendMsg(type, content);
		if (Log.ON && Log.COMM.isEnabledFor(Level.DEBUG)) {
			Log.COMM.debug(
					state.identity.getPath() + ", message sent to broker");
		}
	}

	/**
	 * run method that connects the client to the broker and enters the loop for
	 * receiving messages.
	 */
	@Override
	public void run() {
		try {
			connectToBroker(state.brokerIdentity);
		} catch (SocketException e) {
			if (Log.ON && Log.COMM.isEnabledFor(Level.FATAL)) {
				Log.COMM.fatal(state.identity.getPath() + ", " + e.getMessage()
						+ "\n" + Log.printStackTrace(e.getStackTrace()));
			}
			return;
		} catch (UnknownHostException e) {
			if (Log.ON && Log.COMM.isEnabledFor(Level.FATAL)) {
				Log.COMM.fatal(state.identity.getPath() + ", " + e.getMessage()
						+ "\n" + Log.printStackTrace(e.getStackTrace()));
			}
			return;
		} catch (IOException e) {
			if (Log.ON && Log.COMM.isEnabledFor(Level.FATAL)) {
				Log.COMM.fatal(state.identity.getPath() + ", " + e.getMessage()
						+ "\n" + Log.printStackTrace(e.getStackTrace()));
			}
			return;
		}
		loop();
	}

	/**
	 * is the infinite loop organised around the call to select.
	 */
	private void loop() {
		if (Log.ON && Log.COMM.isEnabledFor(Level.DEBUG)) {
			Log.COMM.debug(
					state.identity.getPath() + ", entering the infinite loop");
		}
		if (Log.ON && Log.COMM.isEnabledFor(Level.TRACE)) {
			Log.COMM.trace(
					state.identity.getPath() + ", listenChannel ok on port "
							+ listenChannel.socket().getLocalPort());
		}
		while (!termination) {
			try {
				selector.select();
			} catch (IOException e) {
				if (Log.ON && Log.COMM.isEnabledFor(Level.FATAL)) {
					Log.COMM.fatal(state.identity.getPath() + ", "
							+ e.getMessage() + "\n"
							+ Log.printStackTrace(e.getStackTrace()));
				}
				return;
			}
			Set<SelectionKey> readyKeys = selector.selectedKeys();
			Iterator<SelectionKey> readyIter = readyKeys.iterator();
			while (readyIter.hasNext()) {
				SelectionKey key = readyIter.next();
				readyIter.remove();
				state.currKey = key;
				if (key.isAcceptable()) {
					treatAccept();
				}
				if (key.isReadable()) {
					if (key.equals(brokerKey)) {
						treatMessageFromBroker();
					}
					FullDuplexMsgWorker readWorker = allScriptingClientWorkers
							.get(state.currKey);
					if (readWorker != null) {
						treatMessageFromScriptingClient();
					}
				}
			}
		}
		shutdown();
	}

	/**
	 * is the treatment of the creation of a new connection with a scripting
	 * client.
	 */
	private void treatAccept() {
		if (state.currKey.equals(acceptSelectionKey)) {
			try {
				acceptNewConnection(listenChannel);
				if (Log.ON && Log.COMM.isEnabledFor(Level.TRACE)) {
					Log.COMM.trace(state.identity.getPath()
							+ ", allScriptingClientWorkers.size() = "
							+ allScriptingClientWorkers.size());
				}
			} catch (IOException e) {
				if (Log.ON && Log.COMM.isEnabledFor(Level.ERROR)) {
					Log.COMM.error(state.identity.getPath() + ", "
							+ e.getMessage() + "\n"
							+ Log.printStackTrace(e.getStackTrace()));
				}
			}
		} else {
			if (Log.ON && Log.COMM.isEnabledFor(Level.WARN)) {
				Log.COMM.warn(state.identity.getPath() + ", unknown accept");
			}
			return;
		}
	}

	/**
	 * is the treatment of the message received from the access broker.
	 */
	private void treatMessageFromBroker() {
		try {
			ReadMessageStatus status;
			status = brokerMsgWorker.readMessage();
			if (status == ReadMessageStatus.ChannelClosed) {
				// remote end point has been closed
				brokerMsgWorker.close();
				allScriptingClientWorkers.remove(state.currKey);
				if (Log.ON && Log.COMM.isEnabledFor(Level.DEBUG)) {
					Log.COMM.debug(
							state.identity.getPath() + ", closing a channel");
				}
			}
			if (status == ReadMessageStatus.ReadDataCompleted) {
				// fully received a message
				int messType = brokerMsgWorker.getMessType();
				// place where we get the data from the
				// message
				String msg = brokerMsgWorker.getData();
				if (Log.ON && Log.COMM.isEnabledFor(Level.DEBUG)) {
					Log.COMM.debug(state.identity.getPath()
							+ ", message received " + msg);
				}
				// message for client's event dispatcher
				if (Log.ON && Log.COMM.isEnabledFor(Level.TRACE)) {
					Log.COMM.trace(state.identity.getPath()
							+ ", message received from " + "the broker");
					Log.COMM.trace(
							state.identity.getPath() + ", going to execute"
									+ " action for message type #" + messType);
				}
				mudebs.client.messagedispatcher.ListOfAlgorithms.execute(state,
						messType, msg);
			}
		} catch (IOException e) {
			if (Log.ON && Log.COMM.isEnabledFor(Level.ERROR)) {
				Log.COMM.error(state.identity.getPath() + ", " + e.getMessage()
						+ "\n" + Log.printStackTrace(e.getStackTrace()));
			}
		}
	}

	/**
	 * is the treatment of the message received from a scripting client. Such a
	 * message may lead to a call to advertise, subscribe, publish, etc. Since
	 * this call may generate a message sent to the access broker, which may
	 * then acknowledge the treatment of the message, the treatment of the
	 * message (method <tt>execute</tt>) must not be performed by the message
	 * dispatcher thread. As a consequence, the reception of a message from a
	 * scripting client leads to the creation of a new thread.
	 */
	private void treatMessageFromScriptingClient() {
		try {
			FullDuplexMsgWorker readWorker = allScriptingClientWorkers
					.get(state.currKey);
			ReadMessageStatus status;
			status = readWorker.readMessage();
			if (status == ReadMessageStatus.ChannelClosed) {
				// remote end point has been closed
				readWorker.close();
				allScriptingClientWorkers.remove(state.currKey);
				if (Log.ON && Log.COMM.isEnabledFor(Level.DEBUG)) {
					Log.COMM.debug(
							state.identity.getPath() + ", closing a channel");
				}
				if (Log.ON && Log.COMM.isEnabledFor(Level.TRACE)) {
					Log.COMM.trace(state.identity.getPath()
							+ ", allScriptingClientWorkers.size() = "
							+ allScriptingClientWorkers.size());
				}
			}
			if (status == ReadMessageStatus.ReadDataCompleted) {
				// fully received a message
				int messType = readWorker.getMessType();
				// place where we get the data from the
				// message
				String msg = readWorker.getData();
				if (Log.ON && Log.COMM.isEnabledFor(Level.DEBUG)) {
					Log.COMM.debug(state.identity.getPath()
							+ ", message received " + msg);
				}
				state.currentScriptingMsg = msg;
				state.currentScriptingMsgType = messType;
				new Thread() {
					public void run() {
						String msg = state.currentScriptingMsg;
						int msgType = state.currentScriptingMsgType;
						state.scriptingSemaphore.release();
						if (Log.ON && Log.COMM.isEnabledFor(Level.TRACE)) {
							Log.COMM.trace(state.identity.getPath()
									+ ", message received from "
									+ "a scripting client");
							Log.COMM.trace(state.identity.getPath()
									+ ", going to execute"
									+ " action for message type #" + msgType);
						}
						mudebs.client.messagedispatcher.ListOfAlgorithms
								.execute(state, msgType, msg);
					}
				}.start();
				state.scriptingSemaphore.acquire();
			}
		} catch (IOException e) {
			if (Log.ON && Log.COMM.isEnabledFor(Level.ERROR)) {
				Log.COMM.error(state.identity.getPath() + ", " + e.getMessage()
						+ "\n" + Log.printStackTrace(e.getStackTrace()));
			}
		} catch (InterruptedException e) {
			if (Log.ON && Log.COMM.isEnabledFor(Level.ERROR)) {
				Log.COMM.error(state.identity.getPath() + ", " + e.getMessage()
						+ "\n" + Log.printStackTrace(e.getStackTrace()));
			}
		}
	}

	/**
	 * asks for the termination of the message dispatcher thread.
	 * 
	 */
	public void terminate() {
		if (Log.ON && Log.CONFIG.isEnabledFor(Level.DEBUG)) {
			Log.CONFIG.debug("request for the termination"
					+ " of the message dispatcher thread");
		}
		termination = true;
	}

	/**
	 * shuts down the message dispatcher: closes all the connections and clears
	 * all the state maps.
	 */
	private void shutdown() {
		try {
			acceptSelectionKey.channel().close();
			acceptSelectionKey.cancel();
			brokerKey.channel().close();
			brokerKey.cancel();
			brokerMsgWorker.close();
		} catch (IOException e) {
			if (Log.ON && Log.COMM.isEnabledFor(Level.ERROR)) {
				Log.COMM.error(state.identity.getPath() + ", " + e.getMessage()
						+ "\n" + Log.printStackTrace(e.getStackTrace()));
			}
		}
		try {
			for (SelectionKey key : allScriptingClientWorkers.keySet()) {
				key.channel().close();
				key.cancel();
			}
			allScriptingClientWorkers.clear();
		} catch (IOException e) {
			if (Log.ON && Log.COMM.isEnabledFor(Level.ERROR)) {
				Log.COMM.error(state.identity.getPath() + ", " + e.getMessage()
						+ "\n" + Log.printStackTrace(e.getStackTrace()));
			}
		}
	}
}
