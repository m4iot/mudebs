/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2017 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */
package mudebs.client.api;

/**
 * This interface defines the method to work (publish, etc.).
 * 
 * @author Denis Conan
 * 
 */
public interface Worker {
	/**
	 * initialises the worker.
	 * 
	 * @param args
	 *            the arguments of the client.
	 * @exception Exception
	 *                the exception thrown in case of problem.
	 */
	void initialise(String[] args) throws Exception;

	/**
	 * starts working.
	 */
	void work();

	/**
	 * stops working.
	 */
	void terminate();
}
