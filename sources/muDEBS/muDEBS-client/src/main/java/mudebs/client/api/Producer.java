/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */
package mudebs.client.api;

import java.util.List;

import mudebs.common.MuDEBSException;
import mudebs.common.algorithms.ACK;
import mudebs.common.algorithms.CommandResult;
import mudebs.common.algorithms.OperationalMode;
import mudebs.common.algorithms.routing.MultiScopingSpecification;
import mudebs.common.algorithms.routing.Publication;
import mudebs.common.algorithms.routing.Scope;

/**
 * This interface defines the method to advertise a filter, and to publish a
 * publication. The content of the publication must match its corresponding
 * advertisement.
 * 
 * @author Denis Conan
 * @author Léon Lim
 * 
 */
public interface Producer {
	/**
	 * advertises the given filter. The advertisement is uniquely identified
	 * using the URI of the client and the <tt>identifier</tt> of the
	 * advertisement. The advertisement is either local or global. It is either
	 * acknowledged or not. When no acknowledgement is required, the command
	 * result is returned immediately with a status
	 * <tt>CommandStatus.SUCCESS</tt>.
	 * 
	 * @param identifier
	 *            the identifier of the advertisement.
	 * @param opMode
	 *            the operational mode of the advertisement (global or local).
	 * @param ack
	 *            the acknowledgement requirement.
	 * @param filter
	 *            the filter of the advertisement.
	 * @param policyContent
	 *            the XACML policy content.
	 * @param phiNotif
	 *            the set of sets of scope paths associated with the filter.
	 * @return the command result.
	 * @throws MuDEBSException
	 *             the exception thrown in case of problem.
	 */
	CommandResult advertise(String identifier, OperationalMode opMode, ACK ack,
			String filter, MultiScopingSpecification phiNotif,
			String policyContent) throws MuDEBSException;

	/**
	 * publishes a content. The content must match the filter of the
	 * advertisement of the given <tt>identifier</tt>. The publication is
	 * uniquely identifier using the URI of the publisher and the
	 * <tt>identifier</tt> of the corresponding advertisement plus a sequence
	 * number computed by muDEBS. The publication is either acknowledged or not.
	 * When no acknowledgement is required, the command result is returned
	 * immediately with a status <tt>CommandStatus.SUCCESS</tt>.
	 * 
	 * @param identifier
	 *            the identifier of the corresponding advertisement.
	 * @param ack
	 *            the acknowledgement requirement.
	 * @param content
	 *            the content of the publication.
	 * @return the command result.
	 * @throws MuDEBSException
	 *             the exception thrown in case of problem.
	 */
	CommandResult publish(String identifier, ACK ack, String content)
			throws MuDEBSException;

	/**
	 * publishes several contents ---i.e., this is a collective publication.
	 * Each content must match the filter of the advertisement of the given
	 * <tt>identifier</tt>. Each publication is uniquely identifier using the URI
	 * of the publisher and the <tt>identifier</tt> of the corresponding
	 * advertisement plus a sequence number computed by muDEBS. Each publication
	 * is either acknowledged or not. When no acknowledgement is required, the
	 * command result is returned immediately with a status
	 * <tt>CommandStatus.SUCCESS</tt>.
	 * 
	 * @param pubs
	 *            the publications.
	 * @param ack
	 *            the acknowledgement requirement.
	 * @return the command result.
	 * @throws MuDEBSException
	 *             the exception thrown in case of problem.
	 */
	CommandResult publish(List<Publication> pubs, final ACK ack)
			throws MuDEBSException;

	/**
	 * unadvertises the given advertisement (of the given identifier). The
	 * unadvertisement is either local or global. It is either acknowledged or
	 * not. When no acknowledgement is required, the command result is returned
	 * immediately with a status <tt>CommandStatus.SUCCESS</tt>.
	 * 
	 * @param identifier
	 *            the identifier of the advertisement to unregister.
	 * @param ack
	 *            the acknowledgement requirement.
	 * @return the command result.
	 * @throws MuDEBSException
	 *             the exception thrown in case of problem.
	 */
	CommandResult unadvertise(String identifier, ACK ack)
			throws MuDEBSException;

	/**
	 * unadvertises all the advertisements. The unadvertisements are
	 * acknowledged or not. When no acknowledgement is required, the command
	 * result is returned immediately with a status
	 * <tt>CommandStatus.SUCCESS</tt>.
	 * 
	 * @param ack
	 *            the acknowledgement requirement.
	 * @return the command result.
	 * @throws MuDEBSException
	 *             the exception thrown in case of problem.
	 */
	CommandResult unadvertiseAll(ACK ack) throws MuDEBSException;

	/**
	 * gets the set of scopes of the access broker.
	 * 
	 * @return the list of the scopes that the access broker is aware of.
	 * @throws MuDEBSException
	 *             the exception thrown in case of problem.
	 */
	List<Scope> getScopesOfBroker() throws MuDEBSException;
}
