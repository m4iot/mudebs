/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Christian Bac and Denis Conan
Contributor(s):
 */
package mudebs.client;

import mudebs.client.api.Consumer;
import mudebs.client.api.Worker;
import mudebs.common.algorithms.routing.PublicationMsgContent;

/**
 * This class is a dummy client, which is used for instance by the scripting
 * client. It references a client object.
 * 
 * @author Denis Conan
 * 
 */
public class DummyWorker implements Worker {
	/**
	 * the consumer of publication messages.
	 */
	private Consumer consumer = new Consumer() {
		@Override
		public void consume(PublicationMsgContent msg) {
			// write here the logic of your client that receives messages
			System.out.println("msg=" + msg);
		}
	};

	/**
	 * the client.
	 */
	private Client client;

	/**
	 * Constructor.
	 * 
	 * @param args
	 *            the command line arguments.
	 */
	public DummyWorker(String[] args) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mudebs.client.api.Worker#initialise(java.lang.String[])
	 */
	@Override
	public void initialise(String[] args) throws Exception {
		client = new Client(args, consumer, this);
		client.start();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mudebs.client.api.Worker#work()
	 */
	@Override
	public void work() {
		// write here the logic of your client that sends messages
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mudebs.client.api.Worker#terminate()
	 */
	@Override
	public void terminate() {
		if (client != null) {
			client.terminate();
		}
	}
}
