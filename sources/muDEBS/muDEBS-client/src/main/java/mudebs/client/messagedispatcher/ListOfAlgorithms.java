/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2017 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */
package mudebs.client.messagedispatcher;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import mudebs.client.messagedispatcher.overlaymanagement.AlgorithmOverlayManagement;
import mudebs.client.messagedispatcher.routing.AlgorithmRouting;
import mudebs.client.messagedispatcher.scripting.broker.AlgorithmScriptingBroker;
import mudebs.client.messagedispatcher.scripting.client.AlgorithmScriptingClient;
import mudebs.common.Log;
import mudebs.common.algorithms.AbstractMessageContent;
import mudebs.common.algorithms.AlgorithmActionInterface;
import mudebs.common.algorithms.AlgorithmActionInterfaceToBeDeprecated;
import mudebs.common.algorithms.AlgorithmActionInvocationException;
import mudebs.common.algorithms.routing.ScopePathWithStatus;
import mudebs.common.algorithms.routing.ScopePathWithStatusAdaptor;

import org.apache.log4j.Level;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * This Enumeration type declares the algorithms of the client.
 * 
 * @author Denis Conan
 * 
 */
public enum ListOfAlgorithms {
	/**
	 * message types exchanged between the client and the broker for the routing
	 * of events.
	 */
	ALGORITHM_ROUTING(AlgorithmRouting.mapOfActions),
	/**
	 * message types exchanged for the management of the overlay.
	 */
	ALGORITHM_OVERLAY_MANAGEMENT(AlgorithmOverlayManagement.mapOfActions),
	/**
	 * message types exchanged for the scripting client.
	 */
	ALGORITHM_SCRIPTING_CLIENT(AlgorithmScriptingClient.mapOfActions),
	/**
	 * message types exchanged for the scripting broker.
	 */
	ALGORITHM_SCRIPTING_BROKER(AlgorithmScriptingBroker.mapOfActions);

	/**
	 * collection of the algorithms of the client. This collection is kept
	 * private to avoid modifications.
	 */
	private final static List<Map<Integer, ? extends AlgorithmActionInterface>> privateMapOfAlgorithms;
	/**
	 * unmodifiable collection corresponding to <tt>privateMapOfAlgorithms</tt>.
	 */
	public final static List<Map<Integer, ? extends AlgorithmActionInterface>> mapOfAlgorithms;
	/**
	 * collection of the actions of this algorithm object of the client.
	 */
	private final Map<Integer, ? extends AlgorithmActionInterface> mapOfActions;
	/**
	 * Gson object to de-serialise.
	 */
	private final static Gson gson = new GsonBuilder().registerTypeAdapter(
			ScopePathWithStatus.class, new ScopePathWithStatusAdaptor())
			.create();

	/**
	 * static block to build collections.
	 */
	static {
		privateMapOfAlgorithms = new ArrayList<Map<Integer, ? extends AlgorithmActionInterface>>();
		for (ListOfAlgorithms am : ListOfAlgorithms.values()) {
			privateMapOfAlgorithms.add(am.mapOfActions);
		}
		mapOfAlgorithms = Collections.unmodifiableList(privateMapOfAlgorithms);
		checkListOfAlgorithms();
	}

	/**
	 * constructor of this algorithm object.
	 * 
	 * @param map
	 *            collection of actions of this algorithm.
	 */
	private ListOfAlgorithms(
			Map<Integer, ? extends AlgorithmActionInterface> map) {
		mapOfActions = map;
	}

	/**
	 * check whether this list of algorithms conforms to the specification.
	 * 
	 * @return
	 */
	private static boolean checkListOfAlgorithms() {
		if (ListOfAlgorithms.mapOfAlgorithms.size() != mudebs.common.algorithms.ListOfAlgorithms.mapOfAlgorithms
				.size()) {
			throw new RuntimeException(
					"Bad number of algorithms: "
							+ " is "
							+ ListOfAlgorithms.mapOfAlgorithms.size()
							+ ", but should be "
							+ mudebs.common.algorithms.ListOfAlgorithms.mapOfAlgorithms
									.size());
		}
		for (int i = 0; i < ListOfAlgorithms.mapOfAlgorithms.size(); i++) {
			if (ListOfAlgorithms.mapOfAlgorithms.get(i).size() != mudebs.common.algorithms.ListOfAlgorithms.mapOfAlgorithms
					.get(i).size()) {
				throw new RuntimeException(
						"Bad number of messages in algorithm "
								+ i
								+ ": is "
								+ ListOfAlgorithms.mapOfAlgorithms.get(i)
										.size()
								+ ", but should be "
								+ mudebs.common.algorithms.ListOfAlgorithms.mapOfAlgorithms
										.get(i).size());
			}
			for (Integer msgIndex : ListOfAlgorithms.mapOfAlgorithms.get(i)
					.keySet()) {
				AlgorithmActionInterface a = ListOfAlgorithms.mapOfAlgorithms
						.get(i).get(msgIndex);
				AlgorithmActionInterfaceToBeDeprecated b = mudebs.common.algorithms.ListOfAlgorithms.mapOfAlgorithms
						.get(i).get(msgIndex);
				if (a.getActionIndex() != b.getActionIndex()) {
					throw new RuntimeException("Bad action index: is "
							+ a.getActionIndex() + ", but should be "
							+ b.getActionIndex());
				}
				if (!a.getMsgContentClass().equals(b.getMsgContentClass())) {
					throw new RuntimeException("Bad message content class:"
							+ " is " + a.getMsgContentClass()
							+ ", but should be " + b.getMsgContentClass());
				}
			}
		}
		return true;
	}

	/**
	 * search for the action to execute in the collection of actions of the
	 * algorithm of the client.
	 * 
	 * @param state
	 *            state of the client.
	 * @param actionIndex
	 *            index of the action to execute.
	 * @param content
	 *            content of the message just received.
	 */
	public static void execute(final MessageDispatcherState state,
			final int actionIndex, final Object content) {
		boolean executed = false;
		for (Iterator<Map<Integer, ? extends AlgorithmActionInterface>> protocols = mapOfAlgorithms
				.iterator(); protocols.hasNext();) {
			Map<Integer, ? extends AlgorithmActionInterface> protocol = protocols
					.next();
			for (Iterator<? extends AlgorithmActionInterface> actions = protocol
					.values().iterator(); actions.hasNext();) {
				AlgorithmActionInterface action = actions.next();
				if (action.getActionIndex() == actionIndex) {
					executed = true;
					AbstractMessageContent c;
					if (content instanceof String) {
						c = gson.fromJson((String) content,
								action.getMsgContentClass());
						if (Log.ON && Log.DISPATCH.isEnabledFor(Level.TRACE)) {
							Log.DISPATCH.trace(state.identity.getPath()
									+ ", going to execute action for message: "
									+ c);
						}
						action.execute(state, c);
					} else {
						if (Log.ON && Log.DISPATCH.isEnabledFor(Level.WARN)) {
							Log.DISPATCH.warn(state.identity.getPath()
									+ ", the content is not "
									+ "a String serialised with Gson: "
									+ content.getClass().getName());
						}
					}
				}
			}
		}
		if (!executed) {
			if (Log.ON && Log.DISPATCH.isEnabledFor(Level.WARN)) {
				Log.DISPATCH.warn(state.identity.getPath()
						+ ", unknown action: " + actionIndex);
			}
		}
	}
}
