/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Christian Bac and Denis Conan
Contributor(s):
 */
package mudebs.client;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import mudebs.client.api.Consumer;
import mudebs.client.api.Worker;
import mudebs.common.Constants;
import mudebs.common.Log;
import mudebs.common.MuDEBSException;

import org.apache.log4j.Level;

/**
 * This class is the generic client of the muDEBS broker. It configures the
 * client, launches the message dispatcher thread, and acts as a client
 * advertising, subscribing, publishing, etc., and reacting to notifications.
 * 
 * @author Denis Conan
 * 
 */
public class Client extends Thread {
	/**
	 * the usage string.
	 */
	static final String USAGE = "Usage: java mudebs.client.ClientMain "
			+ Constants.OPTION_URI + " <URI of this client> "
			+ Constants.OPTION_BROKER + " <URI of the broker> " + "["
			+ Constants.OPTION_LOGGER + " <logger.level>]...";
	/**
	 * the client delegate.
	 */
	protected ClientDelegate delegate;
	/**
	 * the worker.
	 */
	protected Worker worker;
	/**
	 * URI of the client, which is provided as a command line argument.
	 */
	protected String uri = null;
	/**
	 * URI of the broker to which the client is connected. The URI is provided
	 * as a command line argument.
	 */
	protected String uriBroker = null;
	/**
	 * the termination of the main thread is asked for.
	 */
	boolean terminationRequired = false;

	/**
	 * Constructor of a client.
	 * 
	 * @param argv
	 *            the command line arguments.
	 * @param consumer
	 *            the consumer to address when pushing a publication message.
	 * @param worker
	 *            the worker to use for advertising, subscribing, etc.
	 * @throws URISyntaxException
	 *             exception thrown when the URI is not correct.
	 * @throws IOException
	 *             exception thrown when problem in creating the socket to
	 *             accept connections.
	 * @throws MuDEBSException
	 *             the exception thrown in case of connection problem to the
	 *             neighbours.
	 */
	public Client(final String[] argv, final Consumer consumer,
			final Worker worker) throws URISyntaxException, IOException,
			MuDEBSException {
		if ((argv.length % 2) == 1) {
			if (Log.ON && Log.CONFIG.isEnabledFor(Level.FATAL)) {
				Log.CONFIG.fatal(USAGE);
			}
			throw new IllegalArgumentException("not the right arity "
					+ "of arguments ((argv.length % 2) == 1)");
		}
		List<String> loggers = new ArrayList<String>();
		for (int i = 0; i < argv.length; i = i + 2) {
			if (argv[i].equalsIgnoreCase(Constants.OPTION_URI)) {
				uri = argv[i + 1];
			} else if (argv[i].equalsIgnoreCase(Constants.OPTION_BROKER)) {
				uriBroker = argv[i + 1];
			} else if (argv[i].equalsIgnoreCase(Constants.OPTION_LOGGER)) {
				loggers.add(argv[i + 1]);
			}
		}
		if ((uri == null) || uri.equalsIgnoreCase("") || (uriBroker == null)
				|| uriBroker.equalsIgnoreCase("")) {
			if (Log.ON && Log.CONFIG.isEnabledFor(Level.FATAL)) {
				Log.CONFIG.fatal("The URIs of the client or the broker "
						+ "are not provided");
				Log.CONFIG.fatal(USAGE);
			}
			throw new IllegalArgumentException(
					"the URIs of the client or the broker "
							+ "are not provided");
		}
		for (String logger : loggers) {
			String[] conf = logger.split("\\.");
			if (conf.length == 2) {
				Log.configureALogger(conf[0], conf[1]);
			}
		}
		delegate = new ClientDelegate(uri, uriBroker, consumer);
		this.worker = worker;
	}

	/**
	 * asks for the termination of the client thread.
	 */
	public void terminate() {
		if (Log.ON && Log.CONFIG.isEnabledFor(Level.DEBUG)) {
			Log.CONFIG.debug(uri
					+ ", request for the termination of the client");
		}
		terminationRequired = true;
		delegate.terminate();
	}

	@Override
	public void run() {
		delegate.startMessageDispatcher();
		while (!terminationRequired) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			worker.work();
		}
	}
}
