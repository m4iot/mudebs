/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2017 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */
package mudebs.client.messagedispatcher;

import java.util.concurrent.Semaphore;

import mudebs.common.algorithms.CommandResult;

/**
 * This class defines the blocking command semaphore and result.
 * 
 * @author Denis Conan
 * 
 */
public class CommandSemaphoreAndResult {
	/**
	 * the semaphore of the blocking command.
	 */
	private Semaphore semaphore;
	/**
	 * the result of the blocking command.
	 */
	private CommandResult commandResult;

	/**
	 * the constructor.
	 * 
	 * @param s
	 *            the semaphore.
	 * @param r
	 *            the command result.
	 */
	public CommandSemaphoreAndResult(final Semaphore s, final CommandResult r) {
		semaphore = s;
		commandResult = r;
	}

	/**
	 * @return the semaphore
	 */
	public Semaphore getSemaphore() {
		return semaphore;
	}

	/**
	 * @return the commandResult
	 */
	public CommandResult getCommandResult() {
		return commandResult;
	}

	/**
	 * sets the command result.
	 * 
	 * @param r
	 *            the commandResult.
	 */
	public void setCommandResult(final CommandResult r) {
		commandResult = r;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CommandSemaphoreAndResult [semaphore=" + semaphore
				+ ", commandResult=" + commandResult + "]";
	}
}
