/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2017 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Christian Bac and Denis Conan
Contributor(s):
 */
package mudebs.client;

import java.util.ArrayList;
import java.util.List;

import mudebs.broker.Broker;
import mudebs.common.Constants;

/**
 * This class is the main of a generic dummy worker and with a broker in the
 * same virtual machine.
 * 
 * @author Denis Conan
 * 
 */
public class ClientWithLocalBrokerMain {

	/**
	 * the main method that creates an instance of the client, which is a dummy
	 * worker.
	 * 
	 * @param args
	 *            the command line arguments.
	 * @throws Exception
	 *             the exception in case of problem.
	 */
	public static void main(final String[] args) throws Exception {
		List<String> brokerArgs = new ArrayList<String>();
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals(Constants.OPTION_URI)) {
				i++;
			} else if (args[i].equals(Constants.OPTION_BROKER)) {
				brokerArgs.add(Constants.OPTION_URI);
				i++;
				brokerArgs.add(args[i]);
			} else {
				brokerArgs.add(args[i]);
			}
		}
		Broker broker = new Broker(
				(String[]) brokerArgs.toArray(new String[brokerArgs.size()]));
		broker.start();
		new DummyWorker(args).initialise(args);
	}
}
