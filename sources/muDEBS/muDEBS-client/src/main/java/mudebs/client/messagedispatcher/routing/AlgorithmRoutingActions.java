/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */
package mudebs.client.messagedispatcher.routing;

import java.util.List;

import mudebs.client.messagedispatcher.CommandSemaphoreAndResult;
import mudebs.client.messagedispatcher.MessageDispatcherState;
import mudebs.client.messagedispatcher.RequestSemaphoreAndResults;
import mudebs.common.Log;
import mudebs.common.algorithms.AbstractMessageContent;
import mudebs.common.algorithms.CommandResult;
import mudebs.common.algorithms.CommandStatus;
import mudebs.common.algorithms.Reply;
import mudebs.common.algorithms.State;
import mudebs.common.algorithms.routing.AcknowledgementMsgContent;
import mudebs.common.algorithms.routing.AdminPublication;
import mudebs.common.algorithms.routing.CollectivePublicationMsgContent;
import mudebs.common.algorithms.routing.PublicationMsgContent;
import mudebs.common.algorithms.routing.ReplyMsgContent;

import org.apache.log4j.Level;

/**
 * This class defines the methods implementing the reaction concerning the
 * reception of routing messages.
 * 
 * @author Denis Conan
 * 
 */
class AlgorithmRoutingActions {
	public static void receivePublication(final State state,
			final AbstractMessageContent content) {
		MessageDispatcherState cstate = (MessageDispatcherState) state;
		PublicationMsgContent mymsg = (PublicationMsgContent) content;
		if (MessageDispatcherState.getSequenceNumber(
				cstate.lastPublicationsReceived, mymsg.getPath().get(0),
				mymsg.getIdAdv(), mymsg.getSequenceNumber()) < mymsg
				.getSequenceNumber()) {
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.DEBUG)) {
				Log.ROUTING.debug(cstate.identity.getPath()
						+ ", publication = " + mymsg);
			}
			try {
				cstate.semaphore.acquire();
			} catch (InterruptedException e) {
				if (Log.ON && Log.ROUTING.isEnabledFor(Level.DEBUG)) {
					Log.ROUTING.debug(cstate.identity.getPath() + ", "
							+ e.getMessage() + "\n"
							+ Log.printStackTrace(e.getStackTrace()));
				}
				return;
			}
			cstate.nbPublicationsReceived++;
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
				Log.ROUTING.trace(cstate.identity.getPath()
						+ ", nbPublicationsReceived = "
						+ cstate.nbPublicationsReceived);
			}
			MessageDispatcherState.setSequenceNumber(
					cstate.lastPublicationsReceived, mymsg.getPath().get(0),
					mymsg.getIdAdv(), mymsg.getSequenceNumber());
			if (cstate.consumer != null) {
				cstate.lastPublication = mymsg;
				cstate.consumer.consume(mymsg);
			} else {
				if (Log.ON && Log.ROUTING.isEnabledFor(Level.DEBUG)) {
					Log.ROUTING.debug(cstate.identity.getPath()
							+ ", no consumer already registered"
							+ " => publication message is ignored");
				}
			}
			cstate.semaphore.release();
		}
	}

	public static void receiveCollectivePublication(final State state,
			final AbstractMessageContent content) {
		MessageDispatcherState cstate = (MessageDispatcherState) state;
		CollectivePublicationMsgContent mymsg = (CollectivePublicationMsgContent) content;
		if (mymsg.getPublications() == null
				|| mymsg.getPublications().size() == 0
				|| mymsg.getPublications().get(0) == null) {
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.WARN)) {
				Log.ROUTING.warn(cstate.identity.getPath()
						+ ", erroneous publication received= " + mymsg);
			}
			return;
		}
		for (AdminPublication pub : mymsg.getPublications()) {
			if (MessageDispatcherState.getSequenceNumber(
					cstate.lastPublicationsReceived, mymsg.getPath().get(0),
					pub.getIdAdv(), pub.getSequenceNumber()) < pub
					.getSequenceNumber()) {
				if (Log.ON && Log.ROUTING.isEnabledFor(Level.DEBUG)) {
					Log.ROUTING.debug(cstate.identity.getPath()
							+ ", publication = " + pub);
				}
				try {
					cstate.semaphore.acquire();
				} catch (InterruptedException e) {
					if (Log.ON && Log.ROUTING.isEnabledFor(Level.DEBUG)) {
						Log.ROUTING.debug(cstate.identity.getPath() + ", "
								+ e.getMessage() + "\n"
								+ Log.printStackTrace(e.getStackTrace()));
					}
					return;
				}
				cstate.nbPublicationsReceived++;
				if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
					Log.ROUTING.trace(cstate.identity.getPath()
							+ ", nbPublicationsReceived = "
							+ cstate.nbPublicationsReceived);
				}
				MessageDispatcherState.setSequenceNumber(
						cstate.lastPublicationsReceived,
						mymsg.getPath().get(0), pub.getIdAdv(),
						pub.getSequenceNumber());
				if (cstate.consumer != null) {
					PublicationMsgContent toNotify = new PublicationMsgContent(
							mymsg.getPath(), mymsg.getAck(),
							mymsg.getSeqNumberForAck(), pub.getIdAdv(),
							pub.getSequenceNumber(), pub.getContent());
					cstate.lastPublication = toNotify;
					cstate.consumer.consume(toNotify);
				} else {
					if (Log.ON && Log.ROUTING.isEnabledFor(Level.DEBUG)) {
						Log.ROUTING.debug(cstate.identity.getPath()
								+ ", no consumer already registered"
								+ " => publication message is ignored");
					}
				}
				cstate.semaphore.release();
			}
		}
	}

	public static void receiveAcknowledgement(final State state,
			final AbstractMessageContent content) {
		MessageDispatcherState cstate = (MessageDispatcherState) state;
		AcknowledgementMsgContent mymsg = (AcknowledgementMsgContent) content;
		try {
			cstate.semaphore.acquire();
		} catch (InterruptedException e) {
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.DEBUG)) {
				Log.ROUTING.debug(cstate.identity.getPath() + ", "
						+ e.getMessage() + "\n"
						+ Log.printStackTrace(e.getStackTrace()));
			}
			return;
		}
		CommandSemaphoreAndResult bc = cstate.blockingCommands.get(String
				.valueOf(mymsg.getSeqNumber()));
		if (bc == null) {
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.DEBUG)) {
				Log.ROUTING.debug(cstate.identity.getPath()
						+ ", acknowledgement for unknown command");
			}
		} else {
			CommandResult result = mymsg.getResult();
			if (result.getStatus() == CommandStatus.ERROR) {
				if (Log.ON && Log.ROUTING.isEnabledFor(Level.WARN)) {
					Log.ROUTING.warn(cstate.identity.getPath() + ", "
							+ result.getResult());
				}
			}
			bc.setCommandResult(result);
			bc.getSemaphore().release();
		}
		cstate.semaphore.release();
	}

	public static void receiveResponse(final State state,
			final AbstractMessageContent content) {
		MessageDispatcherState cstate = (MessageDispatcherState) state;
		ReplyMsgContent mymsg = (ReplyMsgContent) content;
		try {
			cstate.semaphore.acquire();
		} catch (InterruptedException e) {
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.DEBUG)) {
				Log.ROUTING.debug(cstate.identity.getPath() + ", "
						+ e.getMessage() + "\n"
						+ Log.printStackTrace(e.getStackTrace()));
			}
			return;
		}
		RequestSemaphoreAndResults bc = cstate.blockingRequests.get(String
				.valueOf(mymsg.getSequenceNumber()));
		if (bc == null) {
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.DEBUG)) {
				Log.ROUTING.debug(cstate.identity.getPath()
						+ ", replies for unknown request");
			}
		} else {
			List<Reply> results = mymsg.getResults();
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
				Log.ROUTING.trace(cstate.identity.getPath() + ", " + results);
			}
			bc.setReplies(results);
			bc.getSemaphore().release();
		}
		cstate.semaphore.release();
	}
}
