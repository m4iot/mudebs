/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */
package mudebs.client.messagedispatcher.overlaymanagement;

import mudebs.client.messagedispatcher.MessageDispatcherState;
import mudebs.common.Log;
import mudebs.common.algorithms.AbstractMessageContent;
import mudebs.common.algorithms.State;
import mudebs.common.algorithms.overlaymanagement.IdentityMsgContent;

import org.apache.log4j.Level;

/**
 * This class defines the methods implementing the reaction concerning the
 * reception of overlay management messages.
 * 
 * @author Denis Conan
 * 
 */
public class AlgorithmOverlayManagementActions {

	public static void receiveIdentity(final State state,
			final AbstractMessageContent content) {
		MessageDispatcherState cstate = (MessageDispatcherState) state;
		IdentityMsgContent mymsg = (IdentityMsgContent) content;
		if (Log.ON && Log.OVERLAY.isEnabledFor(Level.DEBUG)) {
			Log.OVERLAY.debug(cstate.identity.getPath()
					+ ", identity message received from "
					+ mymsg.getIdentity().getPath());
		}
		cstate.nbIdentitiesReceived++;
		if (Log.ON && Log.OVERLAY.isEnabledFor(Level.TRACE)) {
			Log.OVERLAY
					.trace(cstate.identity.getPath()
							+ ", nbIdentitiesReceived = "
							+ cstate.nbIdentitiesReceived);
		}
	}

	public static void receiveTermination(final State state,
			AbstractMessageContent content) {
		MessageDispatcherState cstate = (MessageDispatcherState) state;
		if (Log.ON && Log.OVERLAY.isEnabledFor(Level.DEBUG)) {
			Log.OVERLAY.debug(cstate.identity.getPath()
					+ ", termination message received");
		}
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// nop
		}
		System.exit(0);
	}
}
