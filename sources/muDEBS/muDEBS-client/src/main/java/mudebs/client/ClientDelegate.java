/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2017 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Christian Bac and Denis Conan
Contributor(s): Léon Lim
 */
package mudebs.client;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Semaphore;

import mudebs.client.api.Consumer;
import mudebs.client.api.Producer;
import mudebs.client.api.Subscriber;
import mudebs.client.messagedispatcher.CommandSemaphoreAndResult;
import mudebs.client.messagedispatcher.MessageDispatcherState;
import mudebs.client.messagedispatcher.MessageDispatcherThread;
import mudebs.client.messagedispatcher.RequestSemaphoreAndResults;
import mudebs.client.messagedispatcher.routing.AlgorithmRouting;
import mudebs.common.Log;
import mudebs.common.MuDEBSException;
import mudebs.common.algorithms.ACK;
import mudebs.common.algorithms.CommandResult;
import mudebs.common.algorithms.CommandStatus;
import mudebs.common.algorithms.OperationalMode;
import mudebs.common.algorithms.Reply;
import mudebs.common.algorithms.routing.ABACInformation;
import mudebs.common.algorithms.routing.AdvertisementMsgContent;
import mudebs.common.algorithms.routing.CollectivePublicationMsgContent;
import mudebs.common.algorithms.routing.CollectiveSubscriptionMsgContent;
import mudebs.common.algorithms.routing.GetScopesMsgContent;
import mudebs.common.algorithms.routing.MultiScopingSpecification;
import mudebs.common.algorithms.routing.Publication;
import mudebs.common.algorithms.routing.PublicationMsgContent;
import mudebs.common.algorithms.routing.RequestMsgContent;
import mudebs.common.algorithms.routing.Scope;
import mudebs.common.algorithms.routing.Subscription;
import mudebs.common.algorithms.routing.SubscriptionMsgContent;
import mudebs.common.algorithms.routing.UnadvertisementMsgContent;
import mudebs.common.algorithms.routing.UnsubscriptionMsgContent;

import org.apache.log4j.Level;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

/**
 * This class contains the generic methods to interact with brokers: connection,
 * identification, advertisement, publication, subscription, etc.
 * 
 * @author Denis Conan
 * 
 */
public class ClientDelegate implements Producer, Subscriber {

	/**
	 * reference to the message dispatcher thread.
	 */
	private MessageDispatcherThread msgDispatcher;
	/**
	 * reference to the state of the client's message dispatcher.
	 */
	private MessageDispatcherState state;
	/**
	 * the URI of the client.
	 */
	private String uri;
	/**
	 * Gson object for de-serialisation.
	 */
	private Gson gson = new GsonBuilder().create();

	/**
	 * Constructor.
	 * 
	 * @param uri
	 *            the URI / identity of the client.
	 * @param broker
	 *            the URI / identity of the broker.
	 * @param consumer
	 *            the object that contains the methods to call when receiving a
	 *            publication.
	 * @throws MuDEBSException
	 *             the exception thrown by muDEBS in case of problem when
	 *             initialising the client delegate.
	 */
	public ClientDelegate(final String uri, final String broker,
			final Consumer consumer) throws MuDEBSException {
		if (uri == null) {
			throw new IllegalArgumentException("Cannot create"
					+ " a client delegate with a null identity");
		}
		if (broker == null) {
			throw new IllegalArgumentException("Cannot create"
					+ " a client delegate with a null broker URI");
		}
		state = new MessageDispatcherState();
		try {
			msgDispatcher = new MessageDispatcherThread(state, uri, broker,
					consumer);
		} catch (URISyntaxException e) {
			throw new MuDEBSException(
					uri + ", problem in creating the client message dispatcher: "
							+ e.getMessage());
		} catch (IOException e) {
			throw new MuDEBSException(
					uri + ", problem in creating the client message dispatcher: "
							+ e.getMessage());
		}
		this.uri = uri;
		state.clientDelegate = this;
	}

	/**
	 * starts the message dispatcher.
	 */
	public final void startMessageDispatcher() {
		if (Log.ON && Log.CONFIG.isEnabledFor(Level.DEBUG)) {
			Log.CONFIG.debug("The termination is required for broker " + uri);
		}
		msgDispatcher.start();
	}

	/**
	 * waits for the connection to the access broker.
	 * 
	 * @throws MuDEBSException
	 *             the exception thrown in case of problem.
	 */
	public final void waitConnectionToAccessBroker() throws MuDEBSException {
		if (Log.ON && Log.CONFIG.isEnabledFor(Level.DEBUG)) {
			Log.CONFIG.debug(
					"Wait for the connection to the access broker " + uri);
		}
		try {
			state.waitConnectBrokerSemaphore.acquire();
		} catch (InterruptedException e) {
			throw new MuDEBSException("waitConnectBrokerSemaphore.acquire"
					+ " interrupted " + e.getLocalizedMessage());
		}
	}

	/**
	 * asks for the termination of the main thread.
	 */
	public void terminate() {
		msgDispatcher.terminate();
	}

	/**
	 * forces the termination of the client by a <tt>System.exit(0)</tt>.
	 */
	public void forceTermination() {
		if (Log.ON && Log.CONFIG.isEnabledFor(Level.DEBUG)) {
			Log.CONFIG.debug("The termination is forced for client " + uri);
		}
		throw new RuntimeException("termination forced");
	}

	/**
	 * sends an advertisement to the access broker.
	 * 
	 * NB: be careful to acquire the semaphore when initiating a subscription
	 * outside from a reaction (out of a consume method called by the client
	 * delegate).
	 * 
	 * @param identifier
	 *            the identifier of the advertisement.
	 * @param local
	 *            the boolean stating whether the advertisement is local.
	 * @param ack
	 *            the acknowledgement requirement.
	 * @param filter
	 *            the advertisement message.
	 * @param phi
	 *            the set of sets of scope paths associated with the filter.
	 * @param policyContent
	 *            the XACML policy.
	 * @return the command result.
	 * @throws MuDEBSException
	 *             the exception thrown in case of problem: communication
	 *             problem with the access broker; advertisement already
	 *             registered; internal problem with a semaphore.
	 */
	@Override
	public final CommandResult advertise(final String identifier,
			final OperationalMode local, final ACK ack, final String filter,
			final MultiScopingSpecification phi, final String policyContent)
			throws MuDEBSException {
		CommandResult result = null;
		if (identifier == null || identifier.equals("") || filter == null
				|| filter.equals("")) {
			if (Log.ON && Log.COMM.isEnabledFor(Level.WARN)) {
				Log.COMM.debug(uri + ", missing argument = '" + identifier
						+ "' '" + filter + "'");
			}
			throw new MuDEBSException(uri = ", problem when advertising:"
					+ " missing argument = '" + identifier + "' '" + filter
					+ "'");
		}
		int seqNumber = ++state.seqNumberBlockingCommands;
		if (state.localAdvertisements
				.containsKey(state.identity.toString() + "/" + identifier)) {
			if (Log.ON && Log.COMM.isEnabledFor(Level.WARN)) {
				Log.COMM.warn(state.identity
						+ ", advertisement already registered = " + identifier);
			}
			throw new MuDEBSException(
					"Advertisement already registered = " + identifier);
		} else {
			state.localAdvertisements
					.put(state.identity.toString() + "/" + identifier, filter);
			try {
				state.brokerMsgWorker.sendMsg(
						AlgorithmRouting.ADVERTISEMENT.getActionIndex(),
						new AdvertisementMsgContent(state.identity,
								state.identity.toString() + "/" + identifier,
								seqNumber, local, ack, filter, phi,
								(policyContent == null) ? null
										: policyContent));
			} catch (IOException e) {
				if (Log.ON && Log.COMM.isEnabledFor(Level.WARN)) {
					Log.COMM.warn(state.identity
							+ ", communication problem with the access broker: "
							+ e.getMessage());
				}
				throw new MuDEBSException(
						"Communication problem with the access broker: "
								+ e.getMessage());
			}
			state.nbAdvertisementsSent++;
			if (Log.ON && Log.COMM.isEnabledFor(Level.DEBUG)) {
				Log.COMM.debug(state.identity + ", advertisement sent = "
						+ identifier + "/" + filter);
			}
		}
		if (ack != ACK.NO) {
			result = waitForAcknowledgement(seqNumber);
		} else {
			result = new CommandResult(CommandStatus.SUCCESS, null);
		}
		if (result.getStatus().equals(CommandStatus.ERROR)) {
			state.localAdvertisements
					.remove(state.identity.toString() + "/" + identifier);
		}
		if (Log.ON && Log.COMM.isEnabledFor(Level.DEBUG)) {
			Log.COMM.debug(uri + ", advertisement done = " + identifier + "/"
					+ filter);
		}
		return result;
	}

	/**
	 * sends a subscription to the access broker. The subscription may be
	 * controlled by ABAC information.
	 * 
	 * NB: be careful to acquire the semaphore when initiating a subscription
	 * outside from a reaction (out of a consume method called by the client
	 * delegate).
	 * 
	 * @param identifier
	 *            the identifier of the subscription.
	 * @param local
	 *            the boolean stating whether the subscription is local.
	 * @param ack
	 *            the acknowledgement requirement.
	 * @param filter
	 *            the filter.
	 * @param phi
	 *            the set of sets of scope paths associated with filter.
	 * @param abacInformation
	 *            the ABAC information.
	 * @return the command result.
	 * @throws MuDEBSException
	 *             the exception thrown in case of problem: communication
	 *             problem with the access broker; subscription already
	 *             registered; internal problem with a semaphore.
	 */
	@Override
	public final CommandResult subscribe(final String identifier,
			final OperationalMode local, final ACK ack, final String filter,
			final MultiScopingSpecification phi,
			final ABACInformation abacInformation) throws MuDEBSException {
		CommandResult result = null;
		if (identifier == null || identifier.equals("") || filter == null
				|| filter.equals("")) {
			if (Log.ON && Log.COMM.isEnabledFor(Level.WARN)) {
				Log.COMM.debug(uri + ", missing argument = '" + identifier
						+ "' '" + filter + "' '" + abacInformation + "'");
			}
			throw new MuDEBSException(uri = ", problem when subscribing:"
					+ " missing argument = '" + identifier + "' '" + filter
					+ "' '" + abacInformation + "'");
		}
		int seqNumber = ++state.seqNumberBlockingCommands;
		if (state.globalSubscriptions
				.containsKey(state.identity.toString() + "/" + identifier)) {
			if (Log.ON && Log.COMM.isEnabledFor(Level.WARN)) {
				Log.COMM.warn(state.identity
						+ ", subscription already registered = " + identifier);
			}
			throw new MuDEBSException(
					"Subscription already registered = " + identifier);
		} else {
			state.globalSubscriptions
					.put(state.identity.toString() + "/" + identifier, filter);
			try {
				state.brokerMsgWorker
						.sendMsg(AlgorithmRouting.SUBSCRIPTION.getActionIndex(),
								new SubscriptionMsgContent(state.identity,
										state.identity.toString() + "/"
												+ identifier,
										seqNumber, local, ack, filter, phi,
										abacInformation));
			} catch (IOException e) {
				if (Log.ON && Log.COMM.isEnabledFor(Level.WARN)) {
					Log.COMM.warn(state.identity
							+ ", communication problem with the access broker: "
							+ e.getMessage());
				}
				throw new MuDEBSException(
						"Communication problem with the access broker: "
								+ e.getMessage());
			}
			state.nbSubscriptionsSent++;
			if (Log.ON && Log.COMM.isEnabledFor(Level.DEBUG)) {
				Log.COMM.debug(
						state.identity + ", subscription sent = " + filter);
			}
		}
		if (ack != ACK.NO) {
			result = waitForAcknowledgement(seqNumber);
		} else {
			result = new CommandResult(CommandStatus.SUCCESS, null);
		}
		if (result.getStatus().equals(CommandStatus.ERROR)) {
			state.globalSubscriptions
					.remove(state.identity.toString() + "/" + identifier);
		}
		if (Log.ON && Log.COMM.isEnabledFor(Level.DEBUG)) {
			Log.COMM.debug(uri + ", subscription done = " + identifier + "/"
					+ filter + " with ABAC = " + abacInformation);
		}
		return result;
	}

	/**
	 * sends a collective subscription to the access broker.
	 * 
	 * NB: be careful to acquire the semaphore when initiating a subscription
	 * outside from a reaction (out of a consume method called by the client
	 * delegate).
	 * 
	 * @param opMode
	 *            the boolean stating whether the subscription is local or
	 *            global.
	 * @param ack
	 *            the acknowledgement requirement.
	 * @param subscriptions
	 *            the subscription filters to subscribe to.
	 * @return the command result.
	 * @throws MuDEBSException
	 *             the exception thrown in case of problem: communication
	 *             problem with the access broker; subscription already
	 *             registered; internal problem with a semaphore.
	 */
	@Override
	public final CommandResult subscribe(final OperationalMode opMode,
			final ACK ack, final List<Subscription> subscriptions)
			throws MuDEBSException {
		CommandResult result = null;
		if (subscriptions == null || subscriptions.size() == 0) {
			if (Log.ON && Log.COMM.isEnabledFor(Level.WARN)) {
				Log.COMM.debug(
						uri + ", missing argument = '" + subscriptions + "'");
			}
			throw new MuDEBSException(uri = ", problem when subscribing:"
					+ " missing argument = '" + subscriptions + "'");
		}
		int seqNumber = ++state.seqNumberBlockingCommands;
		for (Subscription subscription : subscriptions) {
			if (state.globalSubscriptions.containsKey(state.identity.toString()
					+ "/" + subscription.getIdentifier())) {
				if (Log.ON && Log.COMM.isEnabledFor(Level.WARN)) {
					Log.COMM.warn(state.identity
							+ ", subscription already registered = "
							+ subscription.getIdentifier());
				}
				throw new MuDEBSException("Subscription already registered = "
						+ subscription.getIdentifier());
			}
		}
		List<Subscription> toSubmit = new ArrayList<Subscription>();
		for (Subscription subscription : subscriptions) {
			state.globalSubscriptions.put(
					state.identity.toString() + "/"
							+ subscription.getIdentifier(),
					subscription.getRoutingFilter());
			toSubmit.add(new Subscription(
					state.identity.toString() + "/"
							+ subscription.getIdentifier(),
					subscription.getRoutingFilter(), subscription.getPhi(),
					subscription.getAbacInformation()));
		}
		try {
			state.brokerMsgWorker.sendMsg(
					AlgorithmRouting.COLLECTIVE_SUBSCRIPTION.getActionIndex(),
					new CollectiveSubscriptionMsgContent(state.identity,
							seqNumber, opMode, ack, toSubmit));
		} catch (IOException e) {
			if (Log.ON && Log.COMM.isEnabledFor(Level.WARN)) {
				Log.COMM.warn(state.identity
						+ ", communication problem with the access broker: "
						+ e.getMessage());
			}
			throw new MuDEBSException(
					"Communication problem with the access broker: "
							+ e.getMessage());
		}
		state.nbSubscriptionsSent++;
		if (Log.ON && Log.COMM.isEnabledFor(Level.DEBUG)) {
			Log.COMM.debug(
					state.identity + ", subscription sent = " + toSubmit);
		}
		if (ack != ACK.NO) {
			result = waitForAcknowledgement(seqNumber);
		} else {
			result = new CommandResult(CommandStatus.SUCCESS, null);
		}
		if (result.getStatus().equals(CommandStatus.ERROR)) {
			for (Subscription subscription : subscriptions) {
				state.globalSubscriptions.remove(state.identity.toString() + "/"
						+ subscription.getIdentifier());
			}
		}
		if (Log.ON && Log.COMM.isEnabledFor(Level.DEBUG)) {
			Log.COMM.debug(uri + ", subscriptions done = " + toSubmit);
		}
		return result;
	}

	/**
	 * waits for the acknowledgement from the access broker.
	 * 
	 * @param seqNumber
	 *            the sequence number of the call.
	 * @return the result of the call that is included in the acknowledgement.
	 * @throws MuDEBSException
	 */
	private CommandResult waitForAcknowledgement(final int seqNumber)
			throws MuDEBSException {
		Semaphore semaphore = new Semaphore(0);
		CommandSemaphoreAndResult csar = new CommandSemaphoreAndResult(
				semaphore, null);
		state.blockingCommands.put(String.valueOf(seqNumber), csar);
		state.semaphore.release();
		try {
			semaphore.acquire();
		} catch (InterruptedException e) {
			semaphore.release();
			try {
				state.semaphore.acquire();
			} catch (InterruptedException ex) {
				throw new MuDEBSException("Semaphore problem when waiting"
						+ " for the acknowledgement (" + seqNumber + "): "
						+ ex.getMessage());
			}
			throw new MuDEBSException("Semaphore problem when waiting"
					+ " for the acknowledgement (" + seqNumber + "): "
					+ e.getMessage());
		}
		try {
			state.semaphore.acquire();
		} catch (InterruptedException e) {
			throw new MuDEBSException("Semaphore problem when waiting"
					+ " for the acknowledgement (" + seqNumber + "): "
					+ e.getMessage());
		}
		state.blockingCommands.remove(String.valueOf(seqNumber));
		if (Log.ON && Log.ROUTING.isEnabledFor(Level.DEBUG)) {
			Log.ROUTING.debug(
					uri + ", acknowledgement received for call " + seqNumber);
		}
		if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
			Log.ROUTING.debug(uri + " command result = " + csar);
			Log.ROUTING.trace(
					uri + ", blockingCommands = " + state.blockingCommands);
		}
		return csar.getCommandResult();
	}

	/**
	 * sends a publication to the access broker.
	 * 
	 * NB: be careful to acquire the semaphore when initiating a publication
	 * outside from a reaction (out of a consume method called by the client
	 * delegate).
	 * 
	 * @param identifier
	 *            the identifier of the advertisement corresponding to the
	 *            publication.
	 * @param ack
	 *            the acknowledgement requirement.
	 * @param content
	 *            the publication message.
	 * @return the command result.
	 * @throws MuDEBSException
	 *             the exception thrown in case of problem: communication
	 *             problem with the access broker; internal problem with a
	 *             semaphore.
	 */
	@Override
	public final CommandResult publish(final String identifier, final ACK ack,
			final String content) throws MuDEBSException {
		CommandResult result = null;
		if (identifier == null || identifier.equals("") || content == null
				|| content.equals("")) {
			if (Log.ON && Log.COMM.isEnabledFor(Level.WARN)) {
				Log.COMM.debug(uri + ", missing argument = '" + identifier
						+ "' '" + content + "'");
			}
			throw new MuDEBSException(
					uri = ", problem when publishing:" + " missing argument = '"
							+ identifier + "' '" + content + "'");
		}
		int seqNumber = ++state.seqNumberBlockingCommands;
		List<URI> path = new ArrayList<URI>();
		path.add(state.identity);
		state.nbPublicationsSent++;
		try {
			state.brokerMsgWorker.sendMsg(
					AlgorithmRouting.PUBLICATION.getActionIndex(),
					new PublicationMsgContent(path, ack, seqNumber,
							state.identity.toString() + "/" + identifier,
							state.nbPublicationsSent, content));
		} catch (IOException e) {
			if (Log.ON && Log.COMM.isEnabledFor(Level.WARN)) {
				Log.COMM.warn(state.identity
						+ ", communication problem with the access broker: "
						+ e.getMessage());
			}
			throw new MuDEBSException(
					"Communication problem with the access broker: "
							+ e.getMessage());
		}
		if (Log.ON && Log.COMM.isEnabledFor(Level.DEBUG)) {
			Log.COMM.debug(state.identity + ", publication sent = " + content);
		}
		if (ack != ACK.NO) {
			result = waitForAcknowledgement(seqNumber);
		} else {
			result = new CommandResult(CommandStatus.SUCCESS, null);
		}
		if (Log.ON && Log.COMM.isEnabledFor(Level.DEBUG)) {
			Log.COMM.debug(
					uri + ", publication done = " + identifier + "/" + content);
		}
		return result;
	}

	/**
	 * publishes several contents ---i.e., this is a collective publication.
	 * Each content must match the filter of the advertisement of the given
	 * <tt>identifier</tt>. Each publication is uniquely identifier using the
	 * URI of the publisher and the <tt>identifier</tt> of the corresponding
	 * advertisement plus a sequence number computed by muDEBS. Each publication
	 * is either acknowledged or not. When no acknowledgement is required, the
	 * command result is returned immediately with a status
	 * <tt>CommandStatus.SUCCESS</tt>.
	 * 
	 * @param pubs
	 *            the publications.
	 * @param ack
	 *            the acknowledgement requirement.
	 * @return the command result.
	 * @throws MuDEBSException
	 *             the exception thrown in case of problem.
	 */
	@Override
	public final CommandResult publish(List<Publication> pubs, final ACK ack)
			throws MuDEBSException {
		CommandResult result = null;
		if (pubs == null || pubs.size() == 0) {
			if (Log.ON && Log.COMM.isEnabledFor(Level.WARN)) {
				Log.COMM.debug(uri + ", missing publications = '" + pubs + "'");
			}
			throw new MuDEBSException(uri = ", problem when publishing:"
					+ " missing publication = '" + pubs + "'");
		}
		int seqNumber = ++state.seqNumberBlockingCommands;
		List<URI> path = new ArrayList<URI>();
		path.add(state.identity);
		List<Publication> toSend = new ArrayList<Publication>();
		for (Publication pub : pubs) {
			if (pub != null) {
				state.nbPublicationsSent++;
				toSend.add(new Publication(
						state.identity.toString() + "/" + pub.getIdAdv(),
						state.nbPublicationsSent, pub.getContent()));
			}
		}
		try {
			state.brokerMsgWorker.sendMsg(
					AlgorithmRouting.COLLECTIVE_PUBLICATION.getActionIndex(),
					new CollectivePublicationMsgContent(path, ack, seqNumber,
							toSend));
		} catch (IOException e) {
			if (Log.ON && Log.COMM.isEnabledFor(Level.WARN)) {
				Log.COMM.warn(state.identity
						+ ", communication problem with the access broker: "
						+ e.getMessage());
			}
			throw new MuDEBSException(
					"Communication problem with the access broker: "
							+ e.getMessage());
		}
		if (Log.ON && Log.COMM.isEnabledFor(Level.DEBUG)) {
			Log.COMM.debug(state.identity + ", publication sent = " + toSend);
		}
		if (ack != ACK.NO) {
			result = waitForAcknowledgement(seqNumber);
		} else {
			result = new CommandResult(CommandStatus.SUCCESS, null);
		}
		if (Log.ON && Log.COMM.isEnabledFor(Level.DEBUG)) {
			Log.COMM.debug(uri + ", publication done = " + toSend);
		}
		return result;
	}

	/**
	 * requests some content. The request may lead to several replies. The first
	 * reply is provided as the command result of this call. The additional
	 * replies are received by the requestee object. Thus, this is is the
	 * responsibility of the client to manage the waiting of several replies.
	 * The request may be local or global, that is the collect of publications
	 * is local to the access broker of the requester or spans all the brokers.
	 * 
	 * NB: be careful to acquire the semaphore when initiating a subscription
	 * outside from a reaction (out of a consume method called by the client
	 * delegate).
	 * 
	 * @param local
	 *            the boolean stating whether the request is local or global
	 * @param filter
	 *            the filter of the request.
	 * @param phi
	 *            the set of scopes associated with the filter of the request.
	 * @param abacInfo
	 *            the ABAC information to have access to publications with
	 *            privacy requirements.
	 * @return the command result.
	 * @throws MuDEBSException
	 *             the exception thrown in case of problem: communication
	 *             problem with the access broker; internal problem with a
	 *             semaphore.
	 */
	@Override
	public final List<Reply> request(final OperationalMode local,
			final String filter, final MultiScopingSpecification phi,
			final ABACInformation abacInfo) throws MuDEBSException {
		List<Reply> result = null;
		if (filter == null || filter.equals("")) {
			if (Log.ON && Log.COMM.isEnabledFor(Level.WARN)) {
				Log.COMM.debug(uri + ", missing argument = '" + filter + "'");
			}
			throw new MuDEBSException(uri = ", problem when subscribing:"
					+ " missing argument = '" + filter + "'");
		}
		int seqNumber = ++state.seqNumberBlockingCommands;
		try {
			state.brokerMsgWorker.sendMsg(
					AlgorithmRouting.REQUEST.getActionIndex(),
					new RequestMsgContent(state.identity, local, seqNumber,
							filter, phi, abacInfo));
		} catch (IOException e) {
			if (Log.ON && Log.COMM.isEnabledFor(Level.WARN)) {
				Log.COMM.warn(state.identity
						+ ", communication problem with the access broker: "
						+ e.getMessage());
			}
			throw new MuDEBSException(
					"Communication problem with the access broker: "
							+ e.getMessage());
		}
		state.nbSubscriptionsSent++;
		if (Log.ON && Log.COMM.isEnabledFor(Level.DEBUG)) {
			Log.COMM.debug(state.identity + ", subscription sent = " + filter);
		}
		result = waitForReplies(seqNumber);
		if (Log.ON && Log.COMM.isEnabledFor(Level.DEBUG)) {
			Log.COMM.debug(
					uri + ", request done = " + seqNumber + "/" + filter);
		}
		return result;
	}

	/**
	 * waits for the acknowledgement from the access broker.
	 * 
	 * @param seqNumber
	 *            the sequence number of the call.
	 * @return the result of the call that is included in the acknowledgement.
	 * @throws MuDEBSException
	 */
	private List<Reply> waitForReplies(final int seqNumber)
			throws MuDEBSException {
		Semaphore semaphore = new Semaphore(0);
		RequestSemaphoreAndResults csar = new RequestSemaphoreAndResults(
				semaphore, null);
		state.blockingRequests.put(String.valueOf(seqNumber), csar);
		state.semaphore.release();
		try {
			semaphore.acquire();
		} catch (InterruptedException e) {
			semaphore.release();
			try {
				state.semaphore.acquire();
			} catch (InterruptedException ex) {
				throw new MuDEBSException(
						"Semaphore problem when waiting" + " for the replies ("
								+ seqNumber + "): " + ex.getMessage());
			}
			throw new MuDEBSException(
					"Semaphore problem when waiting" + " for the replies ("
							+ seqNumber + "): " + e.getMessage());
		}
		try {
			state.semaphore.acquire();
		} catch (InterruptedException e) {
			throw new MuDEBSException(
					"Semaphore problem when waiting" + " for the replies ("
							+ seqNumber + "): " + e.getMessage());
		}
		state.blockingRequests.remove(String.valueOf(seqNumber));
		if (Log.ON && Log.ROUTING.isEnabledFor(Level.DEBUG)) {
			Log.ROUTING.debug(
					uri + ", acknowledgement received for call " + seqNumber);
		}
		if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
			Log.ROUTING.debug(uri + " command result = " + csar);
			Log.ROUTING.trace(
					uri + ", blockingRequests = " + state.blockingRequests);
		}
		return csar.getRequestResults();
	}

	/**
	 * sends an unadvertisement to the access broker.
	 * 
	 * NB: be careful to acquire the semaphore when initiating an
	 * unadvertisement outside from a reaction (out of a consume method called
	 * by the client delegate).
	 * 
	 * @param identifier
	 *            the identifier of the unadvertisement to unregister to.
	 * @param ack
	 *            the acknowledgement requirement.
	 * @return the command result.
	 * @throws MuDEBSException
	 *             the exception thrown in case of problem: communication
	 *             problem with the access broker; internal problem with a
	 *             semaphore.
	 */
	@Override
	public final CommandResult unadvertise(final String identifier,
			final ACK ack) throws MuDEBSException {
		CommandResult result = null;
		if (identifier == null || identifier.equals("")) {
			if (Log.ON && Log.COMM.isEnabledFor(Level.WARN)) {
				Log.COMM.debug(
						uri + ", missing argument = '" + identifier + "'");
			}
			throw new MuDEBSException(uri = ", problem when unadvertising:"
					+ " missing argument = '" + identifier + "'");
		}
		int seqNumber = ++state.seqNumberBlockingCommands;
		if (!state.localAdvertisements
				.containsKey(state.identity.toString() + "/" + identifier)) {
			if (Log.ON && Log.COMM.isEnabledFor(Level.WARN)) {
				Log.COMM.warn(state.identity + ", unknown advertisement = "
						+ identifier);
			}
		} else {
			state.localAdvertisements
					.remove(state.identity.toString() + "/" + identifier);
			try {
				state.brokerMsgWorker.sendMsg(
						AlgorithmRouting.UNADVERTISEMENT.getActionIndex(),
						new UnadvertisementMsgContent(state.identity,
								state.identity.toString() + "/" + identifier,
								seqNumber, ack));
			} catch (IOException e) {
				if (Log.ON && Log.COMM.isEnabledFor(Level.WARN)) {
					Log.COMM.warn(state.identity
							+ ", communication problem with the access broker: "
							+ e.getMessage());
				}
				throw new MuDEBSException(
						"Communication problem with the access broker: "
								+ e.getMessage());
			}
			state.nbUnadvertisementsSent++;
			if (Log.ON && Log.COMM.isEnabledFor(Level.DEBUG)) {
				Log.COMM.debug(state.identity + ", unadvertisement sent = "
						+ identifier);
			}
		}
		if (ack != ACK.NO) {
			result = waitForAcknowledgement(seqNumber);
		} else {
			result = new CommandResult(CommandStatus.SUCCESS, null);
		}
		if (Log.ON && Log.COMM.isEnabledFor(Level.DEBUG)) {
			Log.COMM.debug(uri + ", unadvertisement done = " + identifier);
		}
		return result;
	}

	/**
	 * sends all the unadvertisements to the access broker.
	 * 
	 * NB: be careful to acquire the semaphore when initiating an unadvertise
	 * all outside from a reaction (out of a consume method called by the client
	 * delegate).
	 * 
	 * @param ack
	 *            the acknowledgement requirement.
	 * @return the command result.
	 * @throws MuDEBSException
	 *             the exception thrown in case of problem: communication
	 *             problem with the access broker; internal problem with a
	 *             semaphore: internal problem with a semaphore.
	 */
	@Override
	public CommandResult unadvertiseAll(final ACK ack) throws MuDEBSException {
		CommandResult result = null;
		int seqNumber = ++state.seqNumberBlockingCommands;
		Iterator<Map.Entry<String, String>> iterator = state.localAdvertisements
				.entrySet().iterator();
		while (iterator.hasNext()) {
			Map.Entry<String, String> entry = iterator.next();
			iterator.remove();
			try {
				state.brokerMsgWorker.sendMsg(
						AlgorithmRouting.UNADVERTISEMENT.getActionIndex(),
						new UnadvertisementMsgContent(state.identity,
								entry.getKey(), seqNumber, ack));
			} catch (IOException e) {
				if (Log.ON && Log.COMM.isEnabledFor(Level.WARN)) {
					Log.COMM.warn(state.identity
							+ ", communication problem with the access broker: "
							+ e.getMessage());
				}
				throw new MuDEBSException(
						"Communication problem with the access broker: "
								+ e.getMessage());
			}
			state.nbUnadvertisementsSent++;
			if (Log.ON && Log.COMM.isEnabledFor(Level.DEBUG)) {
				Log.COMM.debug(state.identity + ", unadvertisement sent = "
						+ entry.getKey());
			}
		}

		if (ack != ACK.NO) {
			result = waitForAcknowledgement(seqNumber);
		} else {
			result = new CommandResult(CommandStatus.SUCCESS, null);
		}
		return result;
	}

	/**
	 * sends an unsubscription to the access broker.
	 * 
	 * NB: be careful to acquire the semaphore when initiating an unsubscription
	 * outside from a reaction (out of a consume method called by the client
	 * delegate).
	 * 
	 * @param identifier
	 *            the identifier of the unsubscription to unregister to.
	 * @param ack
	 *            the acknowledgement requirement.
	 * @return the command result.
	 * @throws MuDEBSException
	 *             the exception thrown in case of problem: communication
	 *             problem with the access broker; internal problem with a
	 *             semaphore.
	 */
	@Override
	public final CommandResult unsubscribe(final String identifier,
			final ACK ack) throws MuDEBSException {
		CommandResult result = null;
		if (identifier == null || identifier.equals("")) {
			if (Log.ON && Log.COMM.isEnabledFor(Level.WARN)) {
				Log.COMM.debug(
						uri + ", missing argument = '" + identifier + "'");
			}
			throw new MuDEBSException(uri = ", problem when unsubscribing:"
					+ " missing argument = '" + identifier + "'");
		}
		int seqNumber = ++state.seqNumberBlockingCommands;
		if (!state.globalSubscriptions
				.containsKey(state.identity.toString() + "/" + identifier)) {
			if (Log.ON && Log.COMM.isEnabledFor(Level.WARN)) {
				Log.COMM.warn(state.identity + ", unknown subscription = "
						+ identifier);
			}
		} else {
			state.globalSubscriptions
					.remove(state.identity.toString() + "/" + identifier);
			try {
				state.brokerMsgWorker.sendMsg(
						AlgorithmRouting.UNSUBSCRIPTION.getActionIndex(),
						new UnsubscriptionMsgContent(state.identity,
								state.identity.toString() + "/" + identifier,
								seqNumber, ack));
			} catch (IOException e) {
				if (Log.ON && Log.COMM.isEnabledFor(Level.WARN)) {
					Log.COMM.warn(state.identity
							+ ", communication problem with the access broker: "
							+ e.getMessage());
				}
				throw new MuDEBSException(
						"Communication problem with the access broker: "
								+ e.getMessage());
			}
			state.nbUnsubscriptionsSent++;
			if (Log.ON && Log.COMM.isEnabledFor(Level.DEBUG)) {
				Log.COMM.debug(state.identity + ", unsubscription sent = "
						+ identifier);
			}
		}
		if (ack != ACK.NO) {
			result = waitForAcknowledgement(seqNumber);
		} else {
			result = new CommandResult(CommandStatus.SUCCESS, null);
		}
		if (Log.ON && Log.COMM.isEnabledFor(Level.DEBUG)) {
			Log.COMM.debug(uri + ", unsubscription done = " + identifier);
		}
		return result;
	}

	/**
	 * sends all the unsubscriptions to the access broker.
	 * 
	 * NB: be careful to acquire the semaphore when initiating an unsubscribe
	 * all outside from a reaction (out of a consume method called by the client
	 * delegate).
	 * 
	 * @param ack
	 *            the acknowledgement requirement.
	 * @return the command result.
	 * @throws MuDEBSException
	 *             the exception thrown in case of problem: communication
	 *             problem with the access broker; internal problem with a
	 *             semaphore.
	 */
	@Override
	public CommandResult unsubscribeAll(final ACK ack) throws MuDEBSException {
		CommandResult result = null;
		int seqNumber = ++state.seqNumberBlockingCommands;

		Iterator<Map.Entry<String, String>> iterator = state.globalSubscriptions
				.entrySet().iterator();
		while (iterator.hasNext()) {
			Map.Entry<String, String> entry = iterator.next();
			iterator.remove();
			try {
				state.brokerMsgWorker.sendMsg(
						AlgorithmRouting.UNSUBSCRIPTION.getActionIndex(),
						new UnsubscriptionMsgContent(state.identity,
								entry.getKey(), seqNumber, ack));
			} catch (IOException e) {
				if (Log.ON && Log.COMM.isEnabledFor(Level.WARN)) {
					Log.COMM.warn(state.identity
							+ ", communication problem with the access broker: "
							+ e.getMessage());
				}
				throw new MuDEBSException(
						"Communication problem with the access broker: "
								+ e.getMessage());
			}
			state.nbUnsubscriptionsSent++;
			if (Log.ON && Log.COMM.isEnabledFor(Level.DEBUG)) {
				Log.COMM.debug(state.identity + ", unsubscription sent = "
						+ entry.getKey());
			}
		}
		if (ack != ACK.NO) {
			result = waitForAcknowledgement(seqNumber);
		} else {
			result = new CommandResult(CommandStatus.SUCCESS, null);
		}
		return result;
	}

	/**
	 * gets the set of scopes of the access broker.
	 * 
	 * @return the list of the scopes that the access broker is aware of.
	 * @throws MuDEBSException
	 *             the exception thrown in case of problem: communication
	 *             problem with the access broker; internal problem with a
	 *             semaphore.
	 */
	@Override
	public List<Scope> getScopesOfBroker() throws MuDEBSException {
		int seqNumber = ++state.seqNumberBlockingCommands;
		try {
			state.brokerMsgWorker.sendMsg(
					AlgorithmRouting.GET_SCOPES.getActionIndex(),
					new GetScopesMsgContent(state.identity, seqNumber));
		} catch (IOException e) {
			if (Log.ON && Log.COMM.isEnabledFor(Level.WARN)) {
				Log.COMM.warn(state.identity
						+ ", communication problem with the access broker: "
						+ e.getMessage());
			}
			throw new MuDEBSException(
					"Communication problem with the access broker: "
							+ e.getMessage());
		}
		CommandResult result = waitForAcknowledgement(seqNumber);
		Type listType = (new TypeToken<ArrayList<String>>() {
		}).getType();
		List<String> l = gson.fromJson(result.getResult(), listType);
		List<Scope> scopes = new ArrayList<Scope>();
		for (int i = 0; i < l.size(); i += 2) {
			try {
				scopes.add(new Scope(new URI(l.get(i)), l.get(i + 1)));
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
		}
		return scopes;
	}

	@Override
	public PublicationMsgContent getLastPublication() {
		return msgDispatcher.getMsgDispatcherState().lastPublication;
	}
}
