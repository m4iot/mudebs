/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2017 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */
package mudebs.client.messagedispatcher;

import java.net.URI;
import java.nio.channels.SelectionKey;
import java.util.HashMap;
import java.util.concurrent.Semaphore;

import mudebs.client.ClientDelegate;
import mudebs.client.api.Consumer;
import mudebs.common.algorithms.State;
import mudebs.common.algorithms.routing.PublicationMsgContent;
import mudebs.communication.javanio.FullDuplexMsgWorker;

/**
 * This class defines the attributes of the state of the message dispatcher of
 * the client. All the attributes are public and special care must be taken to
 * access these attributes when multi-threading: The semaphore must be used in
 * this case.
 * 
 * @author Denis Conan
 * 
 */
public class MessageDispatcherState implements State {
	/**
	 * backward reference to the message dispatcher object in order to use its
	 * methods to send messages. This is necessary to implement the client
	 * scripting functionality to disconnect from/reconnect to the access
	 * broker.
	 */
	public MessageDispatcherThread messageDispatcher;
	/**
	 * backward reference to the client delegate in order to use its API. This
	 * is necessary to implement client scripting functionalities.
	 */
	public ClientDelegate clientDelegate;
	/**
	 * message worker of this client for exchanging messages with the broker.
	 * This is necessary to implement client API in the client delegate.
	 */
	public FullDuplexMsgWorker brokerMsgWorker;
	/**
	 * identity of this client.
	 */
	public URI identity;
	/**
	 * identity of the access broker.
	 */
	public URI brokerIdentity;
	/**
	 * reference to the consumer object, which defines the logic of the client
	 * when receiving a publication.
	 */
	public Consumer consumer;
	/**
	 * selection key of the connection from which the last message was received.
	 */
	public SelectionKey currKey;
	/**
	 * current scripting message in treatment.
	 */
	public String currentScriptingMsg;
	/**
	 * last publication received.
	 */
	public PublicationMsgContent lastPublication;
	/**
	 * semaphore to wait for the connection to the access brokes.
	 */
	public Semaphore waitConnectBrokerSemaphore = new Semaphore(0);
	/**
	 * semaphore to transmit message from dispatcher thread to the thread
	 * treating the scripting message.
	 */
	public Semaphore scriptingSemaphore = new Semaphore(0);
	/**
	 * message type of the current scripting message.
	 */
	public int currentScriptingMsgType;
	/**
	 * number of identity messages received.
	 */
	public int nbIdentitiesReceived = 0;
	/**
	 * number of identity messages sent.
	 */
	public int nbIdentitiesSent = 0;
	/**
	 * number of advertisements sent.
	 */
	public int nbAdvertisementsSent = 0;
	/**
	 * set of local advertisements. The key is the identifier (in fact, URI) and
	 * the object is the filter.
	 */
	public HashMap<String, String> localAdvertisements;
	/**
	 * number of publications received.
	 */
	public int nbPublicationsReceived = 0;
	/**
	 * sequence number of the last publication received from a given producer
	 * for a given filter. The key of the outer hash table is the identifier of
	 * the producer (in fact, the URI). The key of the inner hash table is the
	 * identifier of the advertisement.
	 */
	public HashMap<URI, HashMap<String, Integer>> lastPublicationsReceived;
	/**
	 * number of publications sent.
	 */
	public int nbPublicationsSent = 0;
	/**
	 * number of subscriptions sent.
	 */
	public int nbSubscriptionsSent = 0;
	/**
	 * set of global subscriptions. The key is the identifier (in fact, the URI)
	 * and the object is the filter script method.
	 */
	public HashMap<String, String> globalSubscriptions;
	/**
	 * number of unadvertisements sent.
	 */
	public int nbUnadvertisementsSent = 0;
	/**
	 * number of unsubscriptions sent.
	 */
	public int nbUnsubscriptionsSent = 0;
	/**
	 * semaphore to protect concurrent access of client's state
	 */
	public Semaphore semaphore = new Semaphore(1);
	/**
	 * the sequence number of blocking commands.
	 */
	public int seqNumberBlockingCommands;
	/**
	 * the set of semaphore for blocking commands' semaphores and results. The
	 * key is the sequence number of the call.
	 */
	public HashMap<String, CommandSemaphoreAndResult> blockingCommands = new HashMap<String, CommandSemaphoreAndResult>();
	/**
	 * the set of semaphore for blocking requests' semaphores and results. The
	 * key is the sequence number of the request.
	 */
	public HashMap<String, RequestSemaphoreAndResults> blockingRequests = new HashMap<String, RequestSemaphoreAndResults>();

	/**
	 * public constructor of the singleton pattern.
	 */
	public MessageDispatcherState() {
	}

	/**
	 * gets the sequence number of an entry of a collection. The method tests
	 * whether the sender is known using its URI. If not, a new entry is put in
	 * the outer hash map, with a new entry in the outer hash map. Then, the
	 * method tests whether the advertisement is known using its identifier.
	 * 
	 * @param collection
	 *            the collection to use.
	 * @param uri
	 *            the URI of the entry to use.
	 * @param advId
	 *            the identifier of the advertisement.
	 * @param value
	 *            the new value of the sequence number.
	 * @return the sequence number.
	 */
	public static int getSequenceNumber(
			final HashMap<URI, HashMap<String, Integer>> collection,
			final URI uri, final String advId, final int value) {
		if (!collection.containsKey(uri)) {
			HashMap<String, Integer> inner = new HashMap<String, Integer>();
			inner.put(advId, 0);
			collection.put(uri, inner);
		}
		if (!collection.get(uri).containsKey(advId)) {
			collection.get(uri).put(advId, 0);
		}
		return collection.get(uri).get(advId);
	}

	/**
	 * sets the sequence number of an entry of a collection. The method tests
	 * whether the sender is known using its URI. If not, a new entry is put in
	 * the outer hash map, with a new entry in the outer hash map. Then, the
	 * method tests whether the advertisement is known using its identifier.
	 * 
	 * @param collection
	 *            the collection to use.
	 * @param uri
	 *            the URI of the entry to use.
	 * @param advId
	 *            the identifier of the advertisement.
	 * @param value
	 *            the new value of the sequence number.
	 */
	public static void setSequenceNumber(
			final HashMap<URI, HashMap<String, Integer>> collection,
			final URI uri, final String advId, final int value) {
		if (!collection.containsKey(uri)) {
			HashMap<String, Integer> inner = new HashMap<String, Integer>();
			inner.put(advId, 0);
			collection.put(uri, inner);
		}
		collection.get(uri).remove(advId);
		collection.get(uri).put(advId, value);
	}
}
