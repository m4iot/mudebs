/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2017 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */
package mudebs.client.messagedispatcher;

import java.util.List;
import java.util.concurrent.Semaphore;

import mudebs.common.algorithms.Reply;

/**
 * This class defines the blocking request semaphore and result.
 * 
 * @author Denis Conan
 * 
 */
public class RequestSemaphoreAndResults {
	/**
	 * the semaphore of the blocking request.
	 */
	private Semaphore semaphore;
	/**
	 * the results of the blocking request.
	 */
	private List<Reply> replies;

	/**
	 * the constructor.
	 * 
	 * @param s
	 *            the semaphore.
	 * @param r
	 *            the result as a list of replies.
	 */
	public RequestSemaphoreAndResults(final Semaphore s, final List<Reply> r) {
		semaphore = s;
		replies = r;
	}

	/**
	 * @return the semaphore
	 */
	public Semaphore getSemaphore() {
		return semaphore;
	}

	/**
	 * @return the commandResult
	 */
	public List<Reply> getRequestResults() {
		return replies;
	}

	/**
	 * sets the content of the replies (a list of replies).
	 * 
	 * @param r
	 *            the commandResult.
	 */
	public void setReplies(final List<Reply> r) {
		replies = r;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RequestSemaphoreAndResult [semaphore=" + semaphore
				+ ", requestResults=" + replies + "]";
	}
}
