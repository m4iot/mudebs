/**
 This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

 The muDEBS software is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 The muDEBS software platform is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

 Initial developer(s): Denis Conan
 Contributor(s):
 */

package mudebs.client.api;

import java.util.List;

import mudebs.common.MuDEBSException;
import mudebs.common.algorithms.ACK;
import mudebs.common.algorithms.CommandResult;
import mudebs.common.algorithms.OperationalMode;
import mudebs.common.algorithms.Reply;
import mudebs.common.algorithms.routing.ABACInformation;
import mudebs.common.algorithms.routing.MultiScopingSpecification;
import mudebs.common.algorithms.routing.PublicationMsgContent;
import mudebs.common.algorithms.routing.Scope;
import mudebs.common.algorithms.routing.Subscription;

/**
 * This interface defines the method to subscribe to a filter.
 * 
 * @author Denis Conan
 * @author Léon Lim
 * 
 */
public interface Subscriber {
	/**
	 * subscribes the given filter with ABAC (Attribute-Based Access Control)
	 * information. The subscription is uniquely identified using the URI of the
	 * client and the <tt>identifier</tt> of the subscription. The subscription
	 * is either local or global. It is either acknowledged or not. When no
	 * acknowledgement is required, the command result is returned immediately
	 * with a status <tt>CommandStatus.SUCCES</tt>.
	 * 
	 * @param identifier
	 *            the identifier of the subscription.
	 * @param opMode
	 *            the operational mode of the subscription is (global or local).
	 * @param ack
	 *            the acknowledgement requirement.
	 * @param filter
	 *            the filter of the subscription.
	 * @param phi
	 *            the set of sets of scope paths associated with the filter.
	 * @param abacInformation
	 *            the ABAC information.
	 * @return the command result.
	 * @throws MuDEBSException
	 *             the exception thrown in case of problem.
	 */
	CommandResult subscribe(String identifier, OperationalMode opMode, ACK ack,
			String filter, MultiScopingSpecification phi,
			ABACInformation abacInformation) throws MuDEBSException;

	/**
	 * subscribes the given filters with ABAC (Attribute-Based Access Control)
	 * information. The subscriptions are uniquely identified using the URI of
	 * the client and the <tt>identifier</tt> of the subscription. The
	 * subscription is either local or global. It is either acknowledged or not.
	 * When no acknowledgement is required, the command result is returned
	 * immediately with a status <tt>CommandStatus.SUCCES</tt>.
	 * 
	 * @param opMode
	 *            the operational mode of the subscription is (global or local).
	 * @param ack
	 *            the acknowledgement requirement.
	 * @param subscriptions
	 *            the list of subscriptions.
	 * @return the command result.
	 * @throws MuDEBSException
	 *             the exception thrown in case of problem.
	 */
	CommandResult subscribe(OperationalMode opMode, ACK ack,
			List<Subscription> subscriptions) throws MuDEBSException;

	/**
	 * requests some content. The request is global. The request may lead to
	 * several replies. The first reply is provided as the command result of
	 * this call. The additional replies are received by the requestee object.
	 * Thus, this is is the responsibility of the client to manage the waiting
	 * of several replies. The request may be local or global, that is the
	 * collect of publications is local to the access broker of the requester or
	 * spans all the brokers.
	 * 
	 * @param local
	 *            the boolean stating whether the request is local or global
	 * @param filter
	 *            the filter of the request.
	 * @param abacInfo
	 *            the ABAC information to have access to publications with
	 *            privacy requirements.
	 * @param phi
	 *            the set of set of scope paths associated with the filter
	 * @return the replies of the request.
	 * @throws MuDEBSException
	 *             the exception thrown in case of problem.
	 */
	List<Reply> request(OperationalMode local, String filter,
			MultiScopingSpecification phi, ABACInformation abacInfo)
			throws MuDEBSException;

	/**
	 * unsubscribes the given subscription (of the given identifier). The
	 * unsubscription is acknowledged or not. When no acknowledgement is
	 * required, the command result is returned immediately with a status
	 * <tt>CommandStatus.SUCCES</tt>.
	 * 
	 * @param identifier
	 *            the identifier of the subscription to unregister.
	 * @param ack
	 *            the acknowledgement requirement.
	 * @return the command result.
	 * @throws MuDEBSException
	 *             the exception thrown in case of problem.
	 */
	CommandResult unsubscribe(String identifier, ACK ack)
			throws MuDEBSException;

	/**
	 * unsubscribes all the subscriptions. The unsubscription is acknowledged or
	 * not. When no acknowledgement is required, the command result is returned
	 * immediately with a status <tt>CommandStatus.SUCCES</tt>.
	 * 
	 * @param ack
	 *            the acknowledgement requirement.
	 * @return the command result.
	 * @throws MuDEBSException
	 *             the exception thrown in case of problem.
	 */
	CommandResult unsubscribeAll(ACK ack) throws MuDEBSException;

	/**
	 * gets the set of scopes of the access broker.
	 * 
	 * @return the list of the scopes that the access broker is aware of.
	 * @throws MuDEBSException
	 *             the exception thrown in case of problem.
	 */
	List<Scope> getScopesOfBroker() throws MuDEBSException;

	/**
	 * gets the last publication received.
	 * 
	 * @return the last publication.
	 */
	PublicationMsgContent getLastPublication();
}
