/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2017 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Léon Lim and Denis Conan
Contributor(s): 
 */
package mudebs.broker.algorithms.routing;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;

import javax.xml.parsers.ParserConfigurationException;

import mudebs.broker.BrokerState;
import mudebs.common.MuDEBSException;
import mudebs.common.algorithms.EntityType;
import mudebs.common.algorithms.OperationalMode;
import mudebs.common.algorithms.routing.Scope;
import mudebs.common.algorithms.routing.ScopeGraph;
import mudebs.common.algorithms.routing.ScopePathWithStatusAdaptor;
import mudebs.common.algorithms.routing.SetOfScopePathsWithStatus;
import mudebs.common.algorithms.routing.MultiScopingSpecification;
import mudebs.common.algorithms.routing.ScopePathStatus;
import mudebs.common.algorithms.routing.ScopePathWithStatus;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Tests for the method computeScopePathsForSub.
 * 
 * NB: by a slight abuse of usage, the method toString is used for comparisons.
 * 
 * @author Léon Lim
 * @author Denis Conan
 *
 */
public class TestComputeScopePathsForSub {
	private GsonBuilder gsonBuilder = new GsonBuilder();

	@Before
	public void setup() {
		gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(ScopePathWithStatus.class,
				new ScopePathWithStatusAdaptor());

	}

	/**
	 * <pre>
	 * sub(Y,f'), with phi_f' = {((is),up)}, received by B5
	 * </pre>
	 * 
	 */
	@Test
	public void testComputeScopePathsForSub1() throws MuDEBSException,
			URISyntaxException, IOException, ParserConfigurationException,
			SAXException {
		BrokerState state = BrokerState.getInstance();
		URI d = new URI("membership");
		URI b5 = new URI("b5");
		// identity of the broker
		state.identity = b5;
		state.reinitialise();
		// scope graph
		ScopeGraph g1 = new ScopeGraph(d);
		Scope es = new Scope(d, "es");
		Scope fs = new Scope(d, "fs");
		Scope is = new Scope(d, "is");
		Scope ir = new Scope(d, "ir");
		g1.addEdge(es, g1.top());
		g1.addEdge(is, fs);
		g1.addEdge(ir, g1.top());
		g1.addEdge(is, ir);
		state.scopeGraphs.put(g1.dimension(), g1);
		Assert.assertEquals(
				"[top/es/bot, top/is/bot, top/fs/is/bot, top/fs/bot, top/ir/bot, top/ir/is/bot]",
				state.scopeGraphs.get(g1.dimension()).toStringSubScopes());
		Assert.assertEquals(
				"[top/es/bot, top/is/bot, top/fs/is/bot, top/fs/bot, "
						+ "top/ir/bot, top/ir/is/bot]",
				state.scopeGraphs.get(g1.dimension()).toStringSubScopes());
		// routing table
		// routing entry (fs,d(is),B5)
		MultiScopingSpecification phi_fs = new MultiScopingSpecification();
		SetOfScopePathsWithStatus ssp_fs = new SetOfScopePathsWithStatus();
		ScopePathWithStatus sp_fs = new ScopePathWithStatus(d,
				ScopePathStatus.DOWN);
		sp_fs.addScope(fs);
		ssp_fs.addScopePathWithStatus(Arrays.asList(sp_fs));
		phi_fs.addSetOfScopePathsWithStatus(ssp_fs);
		VisibilityFilter down_fs_is = new VisibilityFilter(
				"function evaluate() {return true;}", is);
		RoutingTableEntry re1 = new RoutingTableEntryVisibility(
				OperationalMode.LOCAL, "re1", down_fs_is,
				"function evaluate() {return true;}", phi_fs,
				EntityType.BROKER, b5, is);
		// routing entry (is,u(fs),B5)
		MultiScopingSpecification phi_is = new MultiScopingSpecification();
		SetOfScopePathsWithStatus ssp_is = new SetOfScopePathsWithStatus();
		ScopePathWithStatus sp_is = new ScopePathWithStatus(d,
				ScopePathStatus.UP);
		sp_is.addScope(is);
		ssp_is.addScopePathWithStatus(Arrays.asList(sp_is));
		phi_is.addSetOfScopePathsWithStatus(ssp_is);
		VisibilityFilter up_is_fs = new VisibilityFilter(
				"function evaluate() {return true;}", fs);
		RoutingTableEntry re2 = new RoutingTableEntryVisibility(
				OperationalMode.LOCAL, "re2", up_is_fs,
				"function evaluate() {return true;}", phi_is,
				EntityType.BROKER, b5, fs);
		// routing entry (ir,u(Top),B5)
		MultiScopingSpecification phi_ir = new MultiScopingSpecification();
		SetOfScopePathsWithStatus ssp_ir = new SetOfScopePathsWithStatus();
		ScopePathWithStatus sp_ir = new ScopePathWithStatus(d,
				ScopePathStatus.UP);
		sp_ir.addScope(ir);
		ssp_ir.addScopePathWithStatus(Arrays.asList(sp_ir));
		phi_ir.addSetOfScopePathsWithStatus(ssp_ir);
		VisibilityFilter up_ir_top = new VisibilityFilter(
				"function evaluate() {return true;}", g1.top());
		RoutingTableEntry re3 = new RoutingTableEntryVisibility(
				OperationalMode.LOCAL, "re3", up_ir_top,
				"function evaluate() {return true;}", phi_ir,
				EntityType.BROKER, b5, g1.top());
		// routing entry (ir,u(Top),B5)
		MultiScopingSpecification phi_ir_2 = new MultiScopingSpecification();
		SetOfScopePathsWithStatus ssp_ir_2 = new SetOfScopePathsWithStatus();
		ScopePathWithStatus sp_ir_2 = new ScopePathWithStatus(d,
				ScopePathStatus.DOWN);
		sp_ir_2.addScope(ir);
		ssp_ir_2.addScopePathWithStatus(Arrays.asList(sp_ir_2));
		phi_ir_2.addSetOfScopePathsWithStatus(ssp_ir_2);
		VisibilityFilter down_ir_is = new VisibilityFilter(
				"function evaluate() {return true;}", is);
		RoutingTableEntry re4 = new RoutingTableEntryVisibility(
				OperationalMode.LOCAL, "re4", down_ir_is,
				"function evaluate() {return true;}", phi_ir_2,
				EntityType.BROKER, b5, is);
		// routing entry (ir,u(Top),B5)
		MultiScopingSpecification phi_is_2 = new MultiScopingSpecification();
		SetOfScopePathsWithStatus ssp_is_2 = new SetOfScopePathsWithStatus();
		ScopePathWithStatus sp_is_2 = new ScopePathWithStatus(d,
				ScopePathStatus.UP);
		sp_is_2.addScope(is);
		ssp_is_2.addScopePathWithStatus(Arrays.asList(sp_is_2));
		phi_is_2.addSetOfScopePathsWithStatus(ssp_is_2);
		VisibilityFilter up_is_ir = new VisibilityFilter(
				"function evaluate() {return true;}", ir);
		RoutingTableEntry re5 = new RoutingTableEntryVisibility(
				OperationalMode.LOCAL, "re5", up_is_ir,
				"function evaluate() {return true;}", phi_is_2,
				EntityType.BROKER, b5, ir);
		state.brokerRoutingTables.put(b5,
				Arrays.asList(re1, re2, re3, re4, re5));
		// phi sub
		MultiScopingSpecification phiSUB = new MultiScopingSpecification();
		SetOfScopePathsWithStatus sspSUB = new SetOfScopePathsWithStatus();
		ScopePathWithStatus spSUB = new ScopePathWithStatus(d,
				ScopePathStatus.UP);
		spSUB.addScope(is);
		sspSUB.addScopePathWithStatus(Arrays.asList(spSUB));
		phiSUB.addSetOfScopePathsWithStatus(sspSUB);
		// the test
		Assert.assertEquals("[{((is), UP), ((is, fs), UP), ((is, ir), UP)}]",
				Util.computeScopePathsForSub(phiSUB).toString());
	}

	/**
	 * <pre>
	 * sub(phi_f',f',B5), with phi_f' = {{((is), UP), ((is, fs), UP), 
	 * ((is, ir), UP), ((is, ir, top), UP)}}, received by B1
	 * </pre>
	 * 
	 * TODO: method for deleting a scope from the graph of scopes. TODO: make
	 * the methods addEdge(s,t) and removeEdge(s,t) idempotent.
	 */
	@Test
	public void testComputeScopePathsForSub2() throws MuDEBSException,
			URISyntaxException, IOException, ParserConfigurationException,
			SAXException {
		// scope graph at known by B5
		URI d = new URI("mudebs:localhost:portnumber/membership");
		ScopeGraph g1 = new ScopeGraph(d);
		Scope es = new Scope(d, "es");
		Scope fs = new Scope(d, "fs");
		Scope is = new Scope(d, "is");
		Scope ir = new Scope(d, "ir");
		g1.addEdge(es, g1.top());
		g1.addEdge(is, fs);
		g1.addEdge(ir, g1.top());
		g1.addEdge(is, ir);
		// phi sub sent by B5
		Gson gson = gsonBuilder.create();
		MultiScopingSpecification phiSUB = new MultiScopingSpecification();
		SetOfScopePathsWithStatus sspSUB = new SetOfScopePathsWithStatus();
		ScopePathWithStatus spSUB_1 = new ScopePathWithStatus(d,
				ScopePathStatus.UP);
		spSUB_1.addScope(is);
		ScopePathWithStatus gsonspSUB_1 = gson.fromJson(gson.toJson(spSUB_1),
				ScopePathWithStatus.class);
		ScopePathWithStatus spSUB_2 = new ScopePathWithStatus(d,
				ScopePathStatus.UP);
		spSUB_2.addScope(Arrays.asList(is, ir));
		ScopePathWithStatus gsonspSUB_2 = gson.fromJson(gson.toJson(spSUB_2),
				ScopePathWithStatus.class);
		ScopePathWithStatus spSUB_3 = new ScopePathWithStatus(d,
				ScopePathStatus.UP);
		spSUB_3.addScope(Arrays.asList(is, fs));
		ScopePathWithStatus gsonspSUB_3 = gson.fromJson(gson.toJson(spSUB_3),
				ScopePathWithStatus.class);
		ScopePathWithStatus spSUB_4 = new ScopePathWithStatus(d,
				ScopePathStatus.UP);
		spSUB_4.addScope(Arrays.asList(is, ir, g1.top()));
		ScopePathWithStatus gsonspSUB_4 = gson.fromJson(gson.toJson(spSUB_4),
				ScopePathWithStatus.class);
		sspSUB.addScopePathWithStatus(Arrays.asList(gsonspSUB_1, gsonspSUB_2,
				gsonspSUB_3, gsonspSUB_4));
		Assert.assertEquals(
				"ScopeGraph [dimension=mudebs:localhost:portnumber/membership, "
						+ "toStringSubScopes()=[top/es/bot, top/is/bot, top/fs/is/bot, "
						+ "top/fs/bot, top/ir/bot, top/ir/is/bot]]",
				g1.toString());
		g1.removeEdge(is, ir);
		g1.removeEdge(ir, g1.top());
		g1.removeEdge(is, fs);
		g1.removeEdge(es, g1.top());
		Assert.assertEquals(
				"ScopeGraph [dimension=mudebs:localhost:portnumber/membership, "
						+ "toStringSubScopes()=[top/es/bot, top/is/bot, top/fs/bot, top/ir/bot]]",
				g1.toString());
		phiSUB.addSetOfScopePathsWithStatus(sspSUB);
		Assert.assertEquals(
				"[{((is), UP), ((is, ir), UP), ((is, fs), UP), ((is, ir, top), UP)}]",
				phiSUB.toString());
		// in the sequel, g1 can be reinitilised
		BrokerState state = BrokerState.getInstance();
		URI b1 = new URI("b1");
		// identity of the broker
		state.identity = b1;
		state.reinitialise();
		// scope graph
		g1.addEdge(fs, es);
		g1.addEdge(is, fs);
		state.scopeGraphs.put(g1.dimension(), g1);
		Assert.assertEquals(
				"[top/es/bot, top/es/fs/bot, top/es/fs/is/bot, top/is/bot, "
						+ "top/fs/bot, top/fs/is/bot, top/ir/bot]",
				state.scopeGraphs.get(g1.dimension()).toStringSubScopes());
		MultiScopingSpecification phi_es = new MultiScopingSpecification();
		SetOfScopePathsWithStatus ssp_es = new SetOfScopePathsWithStatus();
		ScopePathWithStatus sp_es = new ScopePathWithStatus(d,
				ScopePathStatus.UP);
		sp_es.addScope(es);
		ssp_es.addScopePathWithStatus(Arrays.asList(sp_es));
		phi_es.addSetOfScopePathsWithStatus(ssp_es);
		VisibilityFilter up_es_top = new VisibilityFilter(
				"function evaluate() {return true;}", g1.top());
		RoutingTableEntry re1 = new RoutingTableEntryVisibility(
				OperationalMode.LOCAL, "re1", up_es_top,
				"function evaluate() {return true;}", phi_es,
				EntityType.BROKER, b1, g1.top());
		// routing entry (es,d(fs),B1)
		MultiScopingSpecification phi_es_2 = new MultiScopingSpecification();
		SetOfScopePathsWithStatus ssp_es_2 = new SetOfScopePathsWithStatus();
		ScopePathWithStatus sp_es_2 = new ScopePathWithStatus(d,
				ScopePathStatus.DOWN);
		sp_es_2.addScope(es);
		ssp_es_2.addScopePathWithStatus(Arrays.asList(sp_es_2));
		phi_es_2.addSetOfScopePathsWithStatus(ssp_es_2);
		VisibilityFilter down_es_fs = new VisibilityFilter(
				"function evaluate() {return true;}", fs);
		RoutingTableEntry re2 = new RoutingTableEntryVisibility(
				OperationalMode.LOCAL, "re2", down_es_fs,
				"function evaluate() {return true;}", phi_es_2,
				EntityType.BROKER, b1, fs);
		// routing entry (ir,u(Top),B5)
		MultiScopingSpecification phi_fs = new MultiScopingSpecification();
		SetOfScopePathsWithStatus ssp_fs = new SetOfScopePathsWithStatus();
		ScopePathWithStatus sp_fs = new ScopePathWithStatus(d,
				ScopePathStatus.UP);
		sp_fs.addScope(fs);
		ssp_fs.addScopePathWithStatus(Arrays.asList(sp_fs));
		phi_fs.addSetOfScopePathsWithStatus(ssp_fs);
		VisibilityFilter up_fs_es = new VisibilityFilter(
				"function evaluate() {return true;}", es);
		RoutingTableEntry re3 = new RoutingTableEntryVisibility(
				OperationalMode.LOCAL, "re3", up_fs_es,
				"function evaluate() {return true;}", phi_fs,
				EntityType.BROKER, b1, es);
		// add routing entries
		state.brokerRoutingTables.put(b1, Arrays.asList(re1, re2, re3));
		// the test
		Assert.assertEquals("[{((is), UP), ((is, ir), UP), ((is, fs), UP), "
				+ "((is, fs, es), UP), ((is, ir, top), UP)}]", Util
				.computeScopePathsForSub(phiSUB).toString());
	}
}
