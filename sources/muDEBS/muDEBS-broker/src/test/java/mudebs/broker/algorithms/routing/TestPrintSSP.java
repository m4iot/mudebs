/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Léon Lim
Contributor(s): 
 */
package mudebs.broker.algorithms.routing;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import mudebs.common.MuDEBSException;
import mudebs.common.algorithms.routing.Scope;
import mudebs.common.algorithms.routing.ScopeGraph;
import mudebs.common.algorithms.routing.SetOfScopePathsWithStatus;
import mudebs.common.algorithms.routing.ScopePathStatus;
import mudebs.common.algorithms.routing.ScopePathWithStatus;
import mudebs.common.algorithms.routing.MultiScopingSpecification;

import org.junit.Assert;
import org.junit.Test;

/**
 * Tests for the method <tt>prinSSP</tt>.
 * 
 * @author Léon Lim
 *
 */
public class TestPrintSSP {

	@Test(expected = IllegalArgumentException.class)
	public void testprintSSPa() throws MuDEBSException, URISyntaxException {
		URI d1 = new URI("mudebs:localhost:portnumber/membership");
		Scope ls = new Scope(d1, "ls");
		Scope us = new Scope(d1, "us");
		Scope es = new Scope(d1, "es");
		Scope fs = new Scope(d1, "fs");
		Scope is = new Scope(d1, "is");
		Scope ir = new Scope(d1, "ir");
		ls.addSuperScope(us);
		es.addSubScope(us);
		fs.addSuperScope(es);
		is.addSuperScope(fs);
		ir.addSubScope(is);
		MultiScopingSpecification phi = new MultiScopingSpecification();
		SetOfScopePathsWithStatus ssp = new SetOfScopePathsWithStatus();
		phi.addSetOfScopePathsWithStatus(ssp);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testprintSSPb() throws MuDEBSException, URISyntaxException {
		URI d1 = new URI("mudebs:localhost:portnumber/membership");
		Scope ls = new Scope(d1, "ls");
		Scope us = new Scope(d1, "us");
		Scope es = new Scope(d1, "es");
		Scope fs = new Scope(d1, "fs");
		Scope is = new Scope(d1, "is");
		Scope ir = new Scope(d1, "ir");
		ls.addSuperScope(us);
		es.addSubScope(us);
		fs.addSuperScope(es);
		is.addSuperScope(fs);
		ir.addSubScope(is);
		MultiScopingSpecification phi = new MultiScopingSpecification();
		SetOfScopePathsWithStatus ssp = null;
		phi.addSetOfScopePathsWithStatus(ssp);
	}

	@Test
	public void testprintSSP2() throws MuDEBSException, URISyntaxException {
		URI d1 = new URI("mudebs:localhost:portnumber/membership");
		Scope ls = new Scope(d1, "ls");
		Scope us = new Scope(d1, "us");
		Scope es = new Scope(d1, "es");
		Scope fs = new Scope(d1, "fs");
		Scope is = new Scope(d1, "is");
		Scope ir = new Scope(d1, "ir");
		ls.addSuperScope(us);
		es.addSubScope(us);
		fs.addSuperScope(es);
		is.addSuperScope(fs);
		ir.addSubScope(is);
		ScopePathWithStatus sp1 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		MultiScopingSpecification phi = new MultiScopingSpecification();
		SetOfScopePathsWithStatus ssp = new SetOfScopePathsWithStatus();
		ssp.addScopePathWithStatus(sp1);
		phi.addSetOfScopePathsWithStatus(ssp);
		Assert.assertEquals("[{()}]", phi.toString());
	}

	@Test
	public void testprintSSP3() throws MuDEBSException, URISyntaxException {
		URI d1 = new URI("mudebs:localhost:portnumber/membership");
		Scope ls = new Scope(d1, "ls");
		Scope us = new Scope(d1, "us");
		Scope es = new Scope(d1, "es");
		Scope fs = new Scope(d1, "fs");
		Scope is = new Scope(d1, "is");
		Scope ir = new Scope(d1, "ir");
		ls.addSuperScope(us);
		es.addSubScope(us);
		fs.addSuperScope(es);
		is.addSuperScope(fs);
		ir.addSubScope(is);
		ScopePathWithStatus sp1 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp1.addScope(ls);
		sp1.addScope(us);
		sp1.addScope(es);
		sp1.addScope(fs);
		sp1.addScope(is);
		MultiScopingSpecification phi = new MultiScopingSpecification();
		SetOfScopePathsWithStatus ssp = new SetOfScopePathsWithStatus();
		ssp.addScopePathWithStatus(sp1);
		phi.addSetOfScopePathsWithStatus(ssp);
		Assert.assertEquals("[{((ls, us, es, fs, is), DOWN)}]", phi.toString());
	}

	@Test
	public void testexistCommonSuffixPrefixWithStatusVerification2()
			throws MuDEBSException, URISyntaxException {
		URI d1 = new URI("mudebs:localhost:portnumber/membership");
		Scope ls = new Scope(d1, "ls");
		Scope us = new Scope(d1, "us");
		Scope es = new Scope(d1, "es");
		Scope fs = new Scope(d1, "fs");
		Scope is = new Scope(d1, "is");
		Scope ir = new Scope(d1, "ir");
		ScopeGraph g1 = new ScopeGraph(d1);
		g1.addEdge(ls, us);
		g1.addEdge(us, es);
		g1.addEdge(fs, es);
		g1.addEdge(is, fs);
		g1.addEdge(is, ir);
		ScopePathWithStatus sp1 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp1.addScope(is);
		sp1.addScope(fs);
		sp1.addScope(es);
		MultiScopingSpecification phi1 = new MultiScopingSpecification();
		SetOfScopePathsWithStatus ssp1 = new SetOfScopePathsWithStatus();
		ssp1.addScopePathWithStatus(sp1);
		phi1.addSetOfScopePathsWithStatus(ssp1);
		Assert.assertEquals("[{((is, fs, es), UP)}]", phi1.toString());
	}

	@Test
	public void testprintSSP4() throws MuDEBSException, URISyntaxException {
		URI d1 = new URI("mudebs:localhost:portnumber/membership");
		Scope ls = new Scope(d1, "ls");
		Scope us = new Scope(d1, "us");
		Scope es = new Scope(d1, "es");
		Scope fs = new Scope(d1, "fs");
		Scope is = new Scope(d1, "is");
		Scope ir = new Scope(d1, "ir");
		ls.addSuperScope(us);
		es.addSubScope(us);
		fs.addSuperScope(es);
		is.addSuperScope(fs);
		ir.addSubScope(is);
		ScopePathWithStatus sp1 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp1.addScope(ls);
		Assert.assertEquals(ScopePathStatus.UP, sp1.status());
		Assert.assertEquals(ls,
				sp1.scopeSequence().get(sp1.scopeSequence().size() - 1));
		sp1.addScope(us);
		Assert.assertEquals(ScopePathStatus.UP, sp1.status());
		Assert.assertEquals(us,
				sp1.scopeSequence().get(sp1.scopeSequence().size() - 1));
		sp1.addScope(es);
		Assert.assertEquals(ScopePathStatus.UP, sp1.status());
		Assert.assertEquals(es,
				sp1.scopeSequence().get(sp1.scopeSequence().size() - 1));
		sp1.addScope(fs);
		Assert.assertEquals(ScopePathStatus.DOWN, sp1.status());
		Assert.assertEquals(fs,
				sp1.scopeSequence().get(sp1.scopeSequence().size() - 1));
		sp1.addScope(is);
		Assert.assertEquals(ScopePathStatus.DOWN, sp1.status());
		Assert.assertEquals(is,
				sp1.scopeSequence().get(sp1.scopeSequence().size() - 1));
		Assert.assertEquals("((ls, us, es, fs, is), DOWN)", sp1.toStringPath());
		ScopePathWithStatus sp2 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp2.addScope(is);
		Assert.assertEquals("((ls, us, es, fs, is), DOWN)", sp1.toStringPath());
		Assert.assertEquals("((is), UP)", sp2.toStringPath());
		Assert.assertEquals("(is)", Util.computeCommonSuffixPrefix(sp1, sp2)
				.toString());
		MultiScopingSpecification phi = new MultiScopingSpecification();
		SetOfScopePathsWithStatus ssp = new SetOfScopePathsWithStatus();
		sp2.addScope(fs);
		sp2.addScope(es);
		ssp.addScopePathWithStatus(sp1);
		ssp.addScopePathWithStatus(sp2);
		phi.addSetOfScopePathsWithStatus(ssp);
		Assert.assertEquals(
				"[{((ls, us, es, fs, is), DOWN), ((is, fs, es), UP)}]",
				phi.toString());
	}

	@Test
	public void testprintSSP5() throws MuDEBSException, URISyntaxException {
		URI d1 = new URI("mudebs:localhost:portnumber/membership");
		Scope ls = new Scope(d1, "ls");
		Scope us = new Scope(d1, "us");
		Scope es = new Scope(d1, "es");
		Scope fs = new Scope(d1, "fs");
		Scope is = new Scope(d1, "is");
		Scope ir = new Scope(d1, "ir");
		ls.addSuperScope(us);
		es.addSubScope(us);
		fs.addSuperScope(es);
		is.addSuperScope(fs);
		ir.addSubScope(is);
		ScopePathWithStatus sp1 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp1.addScope(ls);
		Assert.assertEquals(ScopePathStatus.UP, sp1.status());
		Assert.assertEquals(ls,
				sp1.scopeSequence().get(sp1.scopeSequence().size() - 1));
		sp1.addScope(us);
		Assert.assertEquals(ScopePathStatus.UP, sp1.status());
		Assert.assertEquals(us,
				sp1.scopeSequence().get(sp1.scopeSequence().size() - 1));
		sp1.addScope(es);
		Assert.assertEquals(ScopePathStatus.UP, sp1.status());
		Assert.assertEquals(es,
				sp1.scopeSequence().get(sp1.scopeSequence().size() - 1));
		sp1.addScope(fs);
		Assert.assertEquals(ScopePathStatus.DOWN, sp1.status());
		Assert.assertEquals(fs,
				sp1.scopeSequence().get(sp1.scopeSequence().size() - 1));
		sp1.addScope(is);
		Assert.assertEquals(ScopePathStatus.DOWN, sp1.status());
		Assert.assertEquals(is,
				sp1.scopeSequence().get(sp1.scopeSequence().size() - 1));
		Assert.assertEquals("((ls, us, es, fs, is), DOWN)", sp1.toStringPath());
		ScopePathWithStatus sp2 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp2.addScope(is);
		Assert.assertEquals("((ls, us, es, fs, is), DOWN)", sp1.toStringPath());
		Assert.assertEquals("((is), UP)", sp2.toStringPath());
		Assert.assertEquals("(is)", Util.computeCommonSuffixPrefix(sp1, sp2)
				.toString());
		HashMap<URI, List<ScopePathWithStatus>> phi = new HashMap<URI, List<ScopePathWithStatus>>();
		List<ScopePathWithStatus> ssp = new Vector<ScopePathWithStatus>();
		sp2.addScope(fs);
		sp2.addScope(es);
		ssp.add(sp1);
		ssp.add(sp2);
		phi.put(d1, ssp);
		URI d2 = new URI("mudebs:localhost:portnumber/administrativeare");
		Scope lo = new Scope(d2, "lo");
		Scope uk = new Scope(d2, "uk");
		Scope eu = new Scope(d2, "eu");
		ScopePathWithStatus sp3 = new ScopePathWithStatus(d2,
				ScopePathStatus.UP);
		lo.addSuperScope(uk);
		uk.addSuperScope(eu);
		sp3.addScope(lo);
		sp3.addScope(uk);
		sp3.addScope(eu);
		List<ScopePathWithStatus> list2 = new Vector<ScopePathWithStatus>();
		list2.add(sp3);
		phi.put(d2, list2);
	}
}
