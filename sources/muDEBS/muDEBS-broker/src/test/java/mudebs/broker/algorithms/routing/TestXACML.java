/**
 This file is part of the muDEBS middleware.

 Copyright (C) 2013-2015 Télécom SudParis

 The muDEBS software is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 The muDEBS software platform is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

 Initial developer(s): Léon Lim
 Contributor(s): 
 */
package mudebs.broker.algorithms.routing;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import mudebs.common.algorithms.routing.ABACAttribute;
import mudebs.common.algorithms.routing.ABACInformation;

import org.junit.Assert;
import org.junit.Test;

/**
 * Tests on the usages of XACML.
 * 
 * @author Léon Lim
 * 
 */
public class TestXACML {
	/**
	 * directory name that states the test type
	 */
	private final static String ROOT_DIRECTORY = "xacml";
	/**
	 * directory name that states XACML version
	 */
	private final static String VERSION_DIRECTORY = "basic";

	@Test
	public void testGoodReqGoodPolicy() throws Exception {
		XACMLPEP pep = new XACMLPEP();
		String goodOrBadRequest = "good";
		String goodOrBadPolicy = "good";
		String policiesLocation = Thread.currentThread()
				.getContextClassLoader().getResource("xacml/basic/policies")
				.getPath();
		String policyPath = policiesLocation + "/" + goodOrBadPolicy
				+ "Policy.xml";
		String policyContent = Util
				.readFile(policyPath, StandardCharsets.UTF_8);
		pep.addPolicy(policyContent);
		String request = UtilStringBasedPolicyFinderModule.createRequest(
				ROOT_DIRECTORY, VERSION_DIRECTORY, goodOrBadRequest
						+ "Request.xml");
		String response = pep.evaluate(request);
		Assert.assertEquals(true, response.contains("Permit"));
	}

	@Test
	public void test1Attribute() throws Exception {
		XACMLPEP pep = new XACMLPEP();
		String policiesLocation = Thread.currentThread()
				.getContextClassLoader().getResource("xacml/basic/policies")
				.getPath();
		String policyPath = policiesLocation + "/" + "1attribute.xml";
		String policyContent = Util
				.readFile(policyPath, StandardCharsets.UTF_8);
		pep.addPolicy(policyContent);
		ABACAttribute abacAtt = new ABACAttribute("category1", "attribute1",
				"value1", "http://www.w3.org/2001/XMLSchema#string");
		ArrayList<ABACAttribute> abacAttlist = new ArrayList<ABACAttribute>();
		abacAttlist.add(abacAtt);
		ABACInformation abac = new ABACInformation(abacAttlist);
		XACMLPrivacyFilter filter = new XACMLPrivacyFilter(pep, abac, null);
		Assert.assertEquals(filter.evaluate(null), true);
	}

	@Test
	public void test1AttributeKO1() throws Exception {
		XACMLPEP pep = new XACMLPEP();
		String policiesLocation = Thread.currentThread()
				.getContextClassLoader().getResource("xacml/basic/policies")
				.getPath();
		String policyPath = policiesLocation + "/" + "1attribute.xml";
		String policyContent = Util
				.readFile(policyPath, StandardCharsets.UTF_8);
		pep.addPolicy(policyContent);
		ABACAttribute abacAtt = new ABACAttribute("categoryX", "attribute1",
				"value1", "http://www.w3.org/2001/XMLSchema#string");
		ArrayList<ABACAttribute> abacAttlist = new ArrayList<ABACAttribute>();
		abacAttlist.add(abacAtt);
		ABACInformation abac = new ABACInformation(abacAttlist);
		XACMLPrivacyFilter filter = new XACMLPrivacyFilter(pep, abac, null);
		Assert.assertEquals(filter.evaluate(null), false);
	}

	@Test
	public void test1AttributeKO2() throws Exception {
		XACMLPEP pep = new XACMLPEP();
		String policiesLocation = Thread.currentThread()
				.getContextClassLoader().getResource("xacml/basic/policies")
				.getPath();
		String policyPath = policiesLocation + "/" + "1attribute.xml";
		String policyContent = Util
				.readFile(policyPath, StandardCharsets.UTF_8);
		pep.addPolicy(policyContent);
		ABACAttribute abacAtt = new ABACAttribute("category1", "attributeX",
				"value1", "http://www.w3.org/2001/XMLSchema#string");
		ArrayList<ABACAttribute> abacAttlist = new ArrayList<ABACAttribute>();
		abacAttlist.add(abacAtt);
		ABACInformation abac = new ABACInformation(abacAttlist);
		XACMLPrivacyFilter filter = new XACMLPrivacyFilter(pep, abac, null);
		Assert.assertEquals(filter.evaluate(null), false);
	}

	@Test
	public void test1AttributeKO3() throws Exception {
		XACMLPEP pep = new XACMLPEP();
		String policiesLocation = Thread.currentThread()
				.getContextClassLoader().getResource("xacml/basic/policies")
				.getPath();
		String policyPath = policiesLocation + "/" + "1attribute.xml";
		String policyContent = Util
				.readFile(policyPath, StandardCharsets.UTF_8);
		pep.addPolicy(policyContent);
		ABACAttribute abacAtt = new ABACAttribute("category1", "attribute1",
				"valueX", "http://www.w3.org/2001/XMLSchema#string");
		ArrayList<ABACAttribute> abacAttlist = new ArrayList<ABACAttribute>();
		abacAttlist.add(abacAtt);
		ABACInformation abac = new ABACInformation(abacAttlist);
		XACMLPrivacyFilter filter = new XACMLPrivacyFilter(pep, abac, null);
		Assert.assertEquals(filter.evaluate(null), false);
	}

	@Test
	public void test1AttributeKO4() throws Exception {
		XACMLPEP pep = new XACMLPEP();
		String policiesLocation = Thread.currentThread()
				.getContextClassLoader().getResource("xacml/basic/policies")
				.getPath();
		String policyPath = policiesLocation + "/" + "1attribute.xml";
		String policyContent = Util
				.readFile(policyPath, StandardCharsets.UTF_8);
		pep.addPolicy(policyContent);
		ABACAttribute abacAtt = new ABACAttribute("category1", "attribute1",
				"valueX", "http://www.w3.org/2001/XMLSchema#strinX");
		ArrayList<ABACAttribute> abacAttlist = new ArrayList<ABACAttribute>();
		abacAttlist.add(abacAtt);
		ABACInformation abac = new ABACInformation(abacAttlist);
		XACMLPrivacyFilter filter = new XACMLPrivacyFilter(pep, abac, null);
		Assert.assertEquals(filter.evaluate(null), false);
	}

	@Test
	public void test2Attributes() throws Exception {
		XACMLPEP pep = new XACMLPEP();
		String policiesLocation = Thread.currentThread()
				.getContextClassLoader().getResource("xacml/basic/policies")
				.getPath();
		String policyPath = policiesLocation + "/" + "2attributes.xml";
		String policyContent = Util
				.readFile(policyPath, StandardCharsets.UTF_8);
		pep.addPolicy(policyContent);
		ABACAttribute abacAtt1 = new ABACAttribute("category1", "attribute1",
				"value1", "http://www.w3.org/2001/XMLSchema#string");
		ABACAttribute abacAtt2 = new ABACAttribute("category2", "attribute2",
				"value2", "http://www.w3.org/2001/XMLSchema#string");
		ArrayList<ABACAttribute> abacAttlist = new ArrayList<ABACAttribute>();
		abacAttlist.add(abacAtt1);
		abacAttlist.add(abacAtt2);
		ABACInformation abac = new ABACInformation(abacAttlist);
		XACMLPrivacyFilter filter = new XACMLPrivacyFilter(pep, abac, null);
		Assert.assertEquals(filter.evaluate(null), true);
	}

	@Test
	public void test3Attributes() throws Exception {
		XACMLPEP pep = new XACMLPEP();
		String policiesLocation = Thread.currentThread()
				.getContextClassLoader().getResource("xacml/basic/policies")
				.getPath();
		String policyPath = policiesLocation + "/" + "3attributes.xml";
		String policyContent = Util
				.readFile(policyPath, StandardCharsets.UTF_8);
		pep.addPolicy(policyContent);
		ABACAttribute abacAtt1 = new ABACAttribute("category1", "attribute1",
				"value1", "http://www.w3.org/2001/XMLSchema#string");
		ABACAttribute abacAtt2 = new ABACAttribute("category2", "attribute2",
				"value2", "http://www.w3.org/2001/XMLSchema#string");
		ABACAttribute abacAtt3 = new ABACAttribute("category3", "attribute3",
				"value3", "http://www.w3.org/2001/XMLSchema#string");
		ArrayList<ABACAttribute> abacAttlist = new ArrayList<ABACAttribute>();
		abacAttlist.add(abacAtt1);
		abacAttlist.add(abacAtt2);
		abacAttlist.add(abacAtt3);
		ABACInformation abac = new ABACInformation(abacAttlist);
		XACMLPrivacyFilter filter = new XACMLPrivacyFilter(pep, abac, null);
		Assert.assertEquals(filter.evaluate(null), true);
	}

	@Test
	public void test11Attributes() throws Exception {
		XACMLPEP pep = new XACMLPEP();
		String policiesLocation = Thread.currentThread()
				.getContextClassLoader().getResource("xacml/basic/policies")
				.getPath();
		String policyPath = policiesLocation + "/" + "11attributes.xml";
		String policyContent = Util
				.readFile(policyPath, StandardCharsets.UTF_8);
		pep.addPolicy(policyContent);
		ArrayList<ABACAttribute> abacAttlist = new ArrayList<ABACAttribute>();
		for (int i = 1; i <= 11; i++) {
			ABACAttribute abacAtt = new ABACAttribute("category" + i,
					"attribute" + i, "value" + i,
					"http://www.w3.org/2001/XMLSchema#string");
			abacAttlist.add(abacAtt);
		}
		ABACInformation abac = new ABACInformation(abacAttlist);
		XACMLPrivacyFilter filter = new XACMLPrivacyFilter(pep, abac, null);
		Assert.assertEquals(filter.evaluate(null), true);
	}

	@Test
	public void testBadReqGoodPolicy() throws Exception {
		XACMLPEP pep = new XACMLPEP();
		String goodOrBadRequest = "bad";
		String goodOrBadPolicy = "good";
		String policiesLocation = Thread.currentThread()
				.getContextClassLoader().getResource("xacml/basic/policies")
				.getPath();
		String policyPath = policiesLocation + "/" + goodOrBadPolicy
				+ "Policy.xml";
		String policyContent = Util
				.readFile(policyPath, StandardCharsets.UTF_8);
		pep.addPolicy(policyContent);
		String request = UtilStringBasedPolicyFinderModule.createRequest(
				ROOT_DIRECTORY, VERSION_DIRECTORY, goodOrBadRequest
						+ "Request.xml");
		String response = pep.evaluate(request);
		Assert.assertEquals(false, response.contains("Permit"));
	}

	@Test
	public void testGoodReqBadPolicy() throws Exception {
		XACMLPEP pep = new XACMLPEP();
		String goodOrBadRequest = "good";
		String goodOrBadPolicy = "bad";
		String policiesLocation = Thread.currentThread()
				.getContextClassLoader().getResource("xacml/basic/policies")
				.getPath();
		String policyPath = policiesLocation + "/" + goodOrBadPolicy
				+ "Policy.xml";
		String policyContent = Util
				.readFile(policyPath, StandardCharsets.UTF_8);
		pep.addPolicy(policyContent);
		String request = UtilStringBasedPolicyFinderModule.createRequest(
				ROOT_DIRECTORY, VERSION_DIRECTORY, goodOrBadRequest
						+ "Request.xml");
		String response = pep.evaluate(request);
		Assert.assertEquals(false, response.contains("Permit"));
	}

	@Test
	public void testBadReqBadPolicy() throws Exception {
		XACMLPEP pep = new XACMLPEP();
		String goodOrBadRequest = "bad";
		String goodOrBadPolicy = "bad";
		String policiesLocation = Thread.currentThread()
				.getContextClassLoader().getResource("xacml/basic/policies")
				.getPath();
		String policyPath = policiesLocation + "/" + goodOrBadPolicy
				+ "Policy.xml";
		String policyContent = Util
				.readFile(policyPath, StandardCharsets.UTF_8);
		pep.addPolicy(policyContent);
		String request = UtilStringBasedPolicyFinderModule.createRequest(
				ROOT_DIRECTORY, VERSION_DIRECTORY, goodOrBadRequest
						+ "Request.xml");
		String response = pep.evaluate(request);
		Assert.assertEquals(true, response.contains("Permit"));
	}

	@Test
	public void testGoodReq3Policies() throws Exception {
		XACMLPEP pep = new XACMLPEP();
		String goodOrBadRequest = "good";
		String policiesLocation = Thread.currentThread()
				.getContextClassLoader().getResource("xacml/basic/policies")
				.getPath();
		String goodPolicyPath = policiesLocation + "/" + "good" + "Policy.xml";
		String goodPolicyContent = Util.readFile(goodPolicyPath,
				StandardCharsets.UTF_8);
		String badPolicyPath = policiesLocation + "/" + "bad" + "Policy.xml";
		String badPolicyContent = Util.readFile(badPolicyPath,
				StandardCharsets.UTF_8);
		String request = UtilStringBasedPolicyFinderModule.createRequest(
				ROOT_DIRECTORY, VERSION_DIRECTORY, goodOrBadRequest
						+ "Request.xml");
		pep.addPolicy(badPolicyContent);
		pep.addPolicy(goodPolicyContent);
		String response = pep.evaluate(request);
		Assert.assertEquals(true, response.contains("Permit"));
	}

	@Test
	public void testGoodReq3Policies2() throws Exception {
		XACMLPEP pep = new XACMLPEP();
		String goodOrBadRequest = "good";
		String policiesLocation = Thread.currentThread()
				.getContextClassLoader().getResource("xacml/basic/policies")
				.getPath();
		String goodPolicyPath = policiesLocation + "/" + "good" + "Policy.xml";
		String goodPolicyContent = Util.readFile(goodPolicyPath,
				StandardCharsets.UTF_8);
		String badPolicyPath = policiesLocation + "/" + "bad" + "Policy.xml";
		String badPolicyContent = Util.readFile(badPolicyPath,
				StandardCharsets.UTF_8);
		String request = UtilStringBasedPolicyFinderModule.createRequest(
				ROOT_DIRECTORY, VERSION_DIRECTORY, goodOrBadRequest
						+ "Request.xml");
		pep.addPolicy(goodPolicyContent);
		pep.addPolicy(badPolicyContent);
		String response = pep.evaluate(request);
		Assert.assertEquals(true, response.contains("Permit"));
	}
}