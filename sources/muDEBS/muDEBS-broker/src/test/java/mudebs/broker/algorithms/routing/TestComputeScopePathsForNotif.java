/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2017 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Léon Lim and Denis Conan
Contributor(s): 
 */
package mudebs.broker.algorithms.routing;

import java.io.IOException;
import java.io.StringReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import mudebs.broker.BrokerState;
import mudebs.common.MuDEBSException;
import mudebs.common.algorithms.EntityType;
import mudebs.common.algorithms.OperationalMode;
import mudebs.common.algorithms.routing.Scope;
import mudebs.common.algorithms.routing.ScopeGraph;
import mudebs.common.algorithms.routing.SetOfScopePathsWithStatus;
import mudebs.common.algorithms.routing.MultiScopingSpecification;
import mudebs.common.algorithms.routing.ScopePathStatus;
import mudebs.common.algorithms.routing.ScopePathWithStatus;

import org.junit.Assert;
import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * Tests for the method computeScopePathsForNotif.
 * 
 * NB: by a slight abuse of usage, the method toString is used for comparisons.
 * 
 * @author Léon Lim
 * @author Denis Conan
 *
 */
public class TestComputeScopePathsForNotif {

	/**
	 * <pre>
	 * pub(X,f) with f<SUB>&#934;</SUB> = {ls} (the set of scope paths associated 
	 * with the filter f') received by B3
	 * </pre>
	 */
	@Test
	public void testComputeScopePathsForNotif1() throws MuDEBSException,
			URISyntaxException, IOException, ParserConfigurationException,
			SAXException {
		BrokerState state = BrokerState.getInstance();
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		Document doc = null;
		builder = factory.newDocumentBuilder();
		doc = builder.parse(new InputSource(new StringReader("<foo></foo>")));
		URI d = new URI("membership");
		URI b2 = new URI("b2");
		URI b3 = new URI("b3");
		// identity of the broker
		state.identity = b3;
		state.reinitialise();
		// neighbours
		state.brokerIdentities.put(b2, null);
		// scope graph
		ScopeGraph g1 = new ScopeGraph(d);
		Scope es = new Scope(d, "es");
		Scope ls = new Scope(d, "ls");
		Scope ir = new Scope(d, "ir");
		Scope us = new Scope(d, "us");
		g1.addEdge(es, g1.top());
		g1.addEdge(ls, us);
		g1.addEdge(ir, g1.top());
		state.scopeGraphs.put(g1.dimension(), g1);
		Assert.assertEquals("[top/es/bot, top/ls/bot, top/us/ls/bot, "
				+ "top/us/bot, top/ir/bot]",
				state.scopeGraphs.get(g1.dimension()).toStringSubScopes());
		// SLT
		state.scopeLookupTables.put(g1.top(), Arrays.asList(b2));
		state.scopeLookupTables.put(es, Arrays.asList(b2));
		state.scopeLookupTables.put(us, Arrays.asList(b3));
		state.scopeLookupTables.put(ls, Arrays.asList(b3));
		state.scopeLookupTables.put(us, Arrays.asList(b2));
		state.scopeLookupTables.put(ls, Arrays.asList(b2));
		state.scopeLookupTables.put(ir, Arrays.asList(b2));
		// routing table
		MultiScopingSpecification phiSub = new MultiScopingSpecification();
		SetOfScopePathsWithStatus sspSub = new SetOfScopePathsWithStatus();
		ScopePathWithStatus spSub = new ScopePathWithStatus(d,
				ScopePathStatus.UP);
		spSub.addScope(ls);
		sspSub.addScopePathWithStatus(Arrays.asList(spSub));
		phiSub.addSetOfScopePathsWithStatus(sspSub);
		VisibilityFilter up = new VisibilityFilter(
				"function evaluate() {return true;}", us);
		MultiScopingSpecification phiSub2 = new MultiScopingSpecification();
		SetOfScopePathsWithStatus sspSub2 = new SetOfScopePathsWithStatus();
		ScopePathWithStatus spSub2 = new ScopePathWithStatus(d,
				ScopePathStatus.DOWN);
		spSub2.addScope(us);
		sspSub2.addScopePathWithStatus(Arrays.asList(spSub2));
		phiSub2.addSetOfScopePathsWithStatus(sspSub2);
		VisibilityFilter down = new VisibilityFilter(
				"function evaluate() {return true;}", ls);
		RoutingTableEntry re1 = new RoutingTableEntryVisibility(
				OperationalMode.LOCAL, "re1", up,
				"function evaluate() {return true;}", phiSub,
				EntityType.BROKER, b3, us);
		RoutingTableEntry re2 = new RoutingTableEntryVisibility(
				OperationalMode.LOCAL, "re2", down,
				"function evaluate() {return true;}", phiSub2,
				EntityType.BROKER, b3, ls);
		state.brokerRoutingTables.put(b3, Arrays.asList(re1, re2));
		// phi pub
		MultiScopingSpecification phiNotif = new MultiScopingSpecification();
		SetOfScopePathsWithStatus sspNotif = new SetOfScopePathsWithStatus();
		ScopePathWithStatus spNotif = new ScopePathWithStatus(d,
				ScopePathStatus.UP);
		spNotif.addScope(ls);
		sspNotif.addScopePathWithStatus(Arrays.asList(spNotif));
		phiNotif.addSetOfScopePathsWithStatus(sspNotif);
		// intermediate tests
		Assert.assertEquals(
				"((ls, [b2]), (top, [b2]), (ir, [b2]), (es, [b2]), (us, [b2]))",
				Util.toStringSLT());
		// compute phiPUB
		Assert.assertEquals("[{((ls), UP), ((ls, us), UP)}]", Util
				.computeScopePathsForNotif(phiNotif, doc).toString());
	}

	/**
	 * <pre>
	 * [{((ls), UP), ((ls, us), UP)}] received by B2.
	 * </pre>
	 * 
	 * TODO:
	 */
	@Test
	public void testComputeScopePaths2() throws MuDEBSException,
			URISyntaxException, IOException {

	}
}
