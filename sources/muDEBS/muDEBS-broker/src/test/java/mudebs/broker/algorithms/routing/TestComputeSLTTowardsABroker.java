/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2017 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Léon Lim
Contributor(s): 
 */
package mudebs.broker.algorithms.routing;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;

import javax.xml.parsers.ParserConfigurationException;

import mudebs.broker.BrokerState;
import mudebs.common.MuDEBSException;
import mudebs.common.algorithms.routing.Scope;
import mudebs.common.algorithms.routing.ScopeGraph;

import org.junit.Assert;
import org.junit.Test;
import org.xml.sax.SAXException;

/**
 * Test for the method <tt>computeSLTTowardsBroker</tt>.
 * 
 * NB: by a slight abuse of usage, the method toString is used for comparisons.
 * 
 * @author Léon Lim
 * @author denis Denis Conan
 *
 */
public class TestComputeSLTTowardsABroker {

	@Test
	public void testComputeScopePathsForSub1() throws MuDEBSException,
			URISyntaxException, IOException, ParserConfigurationException,
			SAXException {
		BrokerState state = BrokerState.getInstance();
		state.reinitialise();
		// dimension
		URI d = new URI("membership");
		// the scope graph
		ScopeGraph g = new ScopeGraph(d);
		Scope es = new Scope(d, "es");
		Scope fs = new Scope(d, "fs");
		g.addEdge(fs, es);
		state.scopeGraphs.put(g.dimension(), g);
		// all the broker in the overlay
		URI b1 = new URI("b1");
		URI b2 = new URI("b2");
		URI b3 = new URI("b3");
		URI b4 = new URI("b4");
		URI b5 = new URI("b5");
		// the handling broker
		state.identity = b1;
		// neighbours
		state.brokerIdentities.put(b2, null);
		state.brokerIdentities.put(b4, null);
		state.brokerIdentities.put(b5, null);
		// scope look up tables
		state.scopeLookupTables.put(g.top(), Arrays.asList(b1, b5, b4, b2));
		state.scopeLookupTables.put(es, Arrays.asList(b1, b5, b4, b2));
		state.scopeLookupTables.put(fs, Arrays.asList(b1));
		Assert.assertEquals("[Scope [id=top], Scope [id=es], Scope [id=fs]]",
				Util.computeSLTTowardsABroker(b1).toString());
		Assert.assertEquals("[Scope [id=top], Scope [id=es]]", Util
				.computeSLTTowardsABroker(b2).toString());
		Assert.assertEquals("[]", Util.computeSLTTowardsABroker(b3).toString());
		Assert.assertEquals("[Scope [id=top], Scope [id=es]]", Util
				.computeSLTTowardsABroker(b4).toString());
		Assert.assertEquals("[Scope [id=top], Scope [id=es]]", Util
				.computeSLTTowardsABroker(b5).toString());
	}
}
