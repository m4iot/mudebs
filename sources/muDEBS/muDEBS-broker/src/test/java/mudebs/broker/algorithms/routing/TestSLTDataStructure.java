/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Léon Lim
Contributor(s): Denis Conan
 */
package mudebs.broker.algorithms.routing;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;

import mudebs.broker.BrokerState;
import mudebs.common.MuDEBSException;
import mudebs.common.algorithms.EntityType;
import mudebs.common.algorithms.OperationalMode;
import mudebs.common.algorithms.routing.MultiScopingSpecification;
import mudebs.common.algorithms.routing.Scope;
import mudebs.common.algorithms.routing.ScopeGraph;
import mudebs.common.algorithms.routing.ScopePathStatus;
import mudebs.common.algorithms.routing.ScopePathWithStatus;
import mudebs.common.algorithms.routing.SetOfScopePathsWithStatus;

import org.junit.Assert;
import org.junit.Test;

/**
 * By a slight abuse of usage, the method <tt>toString</tt> of the class
 * <tt>Scope</tt> is used for the comparisons.
 * 
 * @author Léon Lim
 * @author Denis Conan
 */
public class TestSLTDataStructure {

	/**
	 * <pre>
	 * joinScope(X2,T) at B1 from X2
	 * </pre>
	 */
	@Test
	public void testGetNeighboursToFowardJoinB1_1() throws MuDEBSException,
			URISyntaxException, IOException {
		URI d = new URI("mudebs://localhost:portnumber/dimension/d");
		URI b2 = new URI("mudebs://localhost:portnumber/broker/b2");
		URI b5 = new URI("mudebs://localhost:portnumber/broker/b5");
		BrokerState state = BrokerState.getInstance();
		state.reinitialise();
		state.brokerIdentities.put(b2, null);
		state.brokerIdentities.put(b5, null);
		ScopeGraph graph = new ScopeGraph(d);
		state.scopeGraphs.put(graph.dimension(), graph);
		Scope t = new Scope(d, "t");
		Assert.assertEquals(
				"[mudebs://localhost:portnumber/broker/b2, mudebs://localhost:portnumber/broker/b5]",
				Util.computeSetOfNeighboursToFowardJoinScope(
						new URI("mudebs://localhost:portnumber/client/x2"),
						null, t).toString());
	}

	/**
	 * <pre>
	 * joinScope(X6,T) at B2 from B4
	 * </pre>
	 * 
	 * after
	 * 
	 * <pre>
	 * joinScope(X2,T) at B1 from X2
	 * </pre>
	 */
	@Test
	public void testGetNeighboursToFowardJoinB2_1() throws MuDEBSException,
			URISyntaxException, IOException {
		URI d = new URI("mudebs://localhost:portnumber/dimension/d");
		URI b1 = new URI("mudebs://localhost:portnumber/broker/b1");
		URI b3 = new URI("mudebs://localhost:portnumber/broker/b3");
		URI b4 = new URI("mudebs://localhost:portnumber/broker/b4");
		BrokerState state = BrokerState.getInstance();
		state.reinitialise();
		state.brokerIdentities.put(b1, null);
		state.brokerIdentities.put(b3, null);
		state.brokerIdentities.put(b4, null);
		// scope graph maintained at B2 : top <--- t <--- bottom
		ScopeGraph graph = new ScopeGraph(d);
		Scope t = new Scope(d, "t");
		graph.top().addSubScope(t);
		state.scopeGraphs.put(graph.dimension(), graph);
		// scope look up tables of B2 : {(T,B1)}
		state.scopeLookupTables.put(t, Arrays.asList(b1));
		Assert.assertEquals("[mudebs://localhost:portnumber/broker/b1]", Util
				.computeSetOfNeighboursToFowardJoinScope(b4, null, t)
				.toString());
	}

	/**
	 * <pre>
	 * joinScope(S,T) at B2
	 * </pre>
	 * 
	 * after
	 * 
	 * <pre>
	 * joinScope(X2,T) at B1 from X2
	 * joinScope(X6,T) at B2 from B4
	 * </pre>
	 */
	@Test
	public void testGetNeighboursToFowardJoinB2_2() throws MuDEBSException,
			URISyntaxException, IOException {
		URI d = new URI("mudebs://localhost:portnumber/dimension/d");
		URI b1 = new URI("mudebs://localhost:portnumber/broker/b1");
		URI b3 = new URI("mudebs://localhost:portnumber/broker/b3");
		URI b4 = new URI("mudebs://localhost:portnumber/broker/b4");
		BrokerState state = BrokerState.getInstance();
		state.reinitialise();
		state.brokerIdentities.put(b1, null);
		state.brokerIdentities.put(b3, null);
		state.brokerIdentities.put(b4, null);
		// a scope graph maintained at B2 : top <--- t <--- bottom
		ScopeGraph graph = new ScopeGraph(d);
		Scope t = new Scope(d, "t");
		Scope s = new Scope(d, "s");
		graph.addEdge(s, t);
		state.scopeGraphs.put(graph.dimension(), graph);
		// scope look up tables of B2 : {(T,B1),(T,B4)}
		state.scopeLookupTables.put(t, Arrays.asList(b1, b4));
		Assert.assertEquals(
				"[]",
				Util.computeSetOfNeighboursToFowardJoinScope(
						new URI("mudebs://localhost:portnumber/client/x2"), s,
						t).toString());
	}

	/**
	 * <pre>
	 * joinScope(X3,S) at B2 from B3
	 * </pre>
	 * 
	 * after
	 * 
	 * <pre>
	 * joinScope(X2,T) at B1 from X2
	 * joinScope(X6,T) at B2 from B4
	 * joinScope(S,T) at B2
	 * </pre>
	 */
	@Test
	public void testGetNeighboursToFowardJoinB2_3() throws MuDEBSException,
			URISyntaxException, IOException {
		URI d = new URI("mudebs://localhost:portnumber/dimension/d");
		URI b1 = new URI("mudebs://localhost:portnumber/broker/b1");
		URI b2 = new URI("mudebs://localhost:portnumber/broker/b1");
		URI b3 = new URI("mudebs://localhost:portnumber/broker/b3");
		URI b4 = new URI("mudebs://localhost:portnumber/broker/b4");
		BrokerState state = BrokerState.getInstance();
		state.reinitialise();
		state.brokerIdentities.put(b1, null);
		state.brokerIdentities.put(b3, null);
		state.brokerIdentities.put(b4, null);
		// scope graph maintained at B2 : top <--- t <--- bottom
		ScopeGraph graph = new ScopeGraph(d);
		Scope t = new Scope(d, "t");
		Scope s = new Scope(d, "s");
		graph.addEdge(s, t);
		state.scopeGraphs.put(graph.dimension(), graph);
		// scope look up tables of B2 : {(T,B1),(T,B4),(S,B2}
		state.scopeLookupTables.put(t, Arrays.asList(b1, b4, b2));
		state.scopeLookupTables.put(s, Arrays.asList(b2));
		Assert.assertEquals("[]",
				Util.computeSetOfNeighboursToFowardJoinScope(b3, null, t)
						.toString());
	}

	/**
	 * <pre>
	 * joinScope(S,T) at B1 from X1
	 * </pre>
	 * 
	 * after
	 * 
	 * <pre>
	 * joinScope(X2,T) at B1 from X2
	 * joinScope(X6,T) at B2 from B4
	 * joinScope(S,T) at B2
	 * joinScope(X3,S) at B2 from B3
	 * </pre>
	 */
	@Test
	public void testGetNeighboursToFowardJoinB1_2() throws MuDEBSException,
			URISyntaxException, IOException {
		URI d = new URI("mudebs://localhost:portnumber/dimension/d");
		URI b1 = new URI("mudebs://localhost:portnumber/broker/b1");
		URI b2 = new URI("mudebs://localhost:portnumber/broker/b2");
		URI b5 = new URI("mudebs://localhost:portnumber/broker/b5");
		BrokerState state = BrokerState.getInstance();
		state.reinitialise();
		state.brokerIdentities.put(b2, null);
		state.brokerIdentities.put(b5, null);
		// scope graph maintained at B2 = {(S,T)}
		ScopeGraph graph = new ScopeGraph(d);
		Scope t = new Scope(d, "t");
		Scope s = new Scope(d, "s");
		graph.addEdge(s, t);
		state.scopeGraphs.put(graph.dimension(), graph);
		// scope look up tables of B2 : {(T,B1),(T,B2)}
		state.scopeLookupTables.put(t, Arrays.asList(b1, b2));
		Assert.assertEquals(
				"[]",
				Util.computeSetOfNeighboursToFowardJoinScope(
						new URI("mudebs://localhost:portnumber/client/x1"), s,
						t).toString());
	}

	/**
	 * <pre>
	 * joinScope(S,U) at B1 from B1
	 * </pre>
	 * 
	 * after
	 * 
	 * <pre>
	 * joinScope(X2,T) at B1 from X2
	 * joinScope(X6,T) at B2 from B4
	 * joinScope(S,T) at B2
	 * joinScope(X3,S) at B2 from B3
	 * joinScope(S,T) at B1 from X1
	 * </pre>
	 */
	@Test
	public void testGetNeighboursToFowardJoinB1_3() throws MuDEBSException,
			URISyntaxException, IOException {
		URI d = new URI("mudebs://localhost:portnumber/dimension/d");
		URI b1 = new URI("mudebs://localhost:portnumber/broker/b1");
		URI b2 = new URI("mudebs://localhost:portnumber/broker/b2");
		URI b5 = new URI("mudebs://localhost:portnumber/broker/b5");
		BrokerState state = BrokerState.getInstance();
		state.reinitialise();
		state.brokerIdentities.put(b2, null);
		state.brokerIdentities.put(b5, null);
		// scope graph maintained at B2 = {(S,T)}
		ScopeGraph graph = new ScopeGraph(d);
		Scope u = new Scope(d, "u");
		Scope t = new Scope(d, "t");
		Scope s = new Scope(d, "s");
		graph.addEdge(s, t);
		graph.addEdge(t, u);
		state.scopeGraphs.put(graph.dimension(), graph);
		// scope look up tables of B2 : {(T,B1),(T,B2)}
		state.scopeLookupTables.put(t, Arrays.asList(b1, b2));
		state.scopeLookupTables.put(s, Arrays.asList(b1));
		Assert.assertEquals(
				"[mudebs://localhost:portnumber/broker/b2, mudebs://localhost:portnumber/broker/b5]",
				Util.computeSetOfNeighboursToFowardJoinScope(
						new URI("mudebs://localhost:portnumber/client/x2"), s,
						u).toString());
	}

	/**
	 * <pre>
	 * joinScope(X5,U) at B1 from B5
	 * </pre>
	 * 
	 * after
	 * 
	 * <pre>
	 * joinScope(X2,T) at B1 from X2
	 * joinScope(X6,T) at B2 from B4
	 * joinScope(S,T) at B2
	 * joinScope(X3,S) at B2 from B3
	 * joinScope(S,T) at B1 from X1
	 * joinScope(S,U) at B1 from B1
	 * </pre>
	 */
	@Test
	public void testGetNeighboursToFowardJoinB1_4() throws MuDEBSException,
			URISyntaxException, IOException {
		URI d = new URI("mudebs://localhost:portnumber/dimension/d");
		URI b1 = new URI("mudebs://localhost:portnumber/broker/b1");
		URI b2 = new URI("mudebs://localhost:portnumber/broker/b2");
		URI b5 = new URI("mudebs://localhost:portnumber/broker/b5");
		BrokerState state = BrokerState.getInstance();
		state.reinitialise();
		state.brokerIdentities.put(b2, null);
		state.brokerIdentities.put(b5, null);
		// scope graph maintained at B2 = {(S,T)}
		ScopeGraph graph = new ScopeGraph(d);
		Scope u = new Scope(d, "u");
		Scope t = new Scope(d, "t");
		Scope s = new Scope(d, "s");
		graph.addEdge(s, t);
		graph.addEdge(t, u);
		state.scopeGraphs.put(graph.dimension(), graph);
		// scope look up tables of B2 : {(T,B1),(T,B2)}
		state.scopeLookupTables.put(t, Arrays.asList(b1, b2));
		state.scopeLookupTables.put(s, Arrays.asList(b1));
		state.scopeLookupTables.put(u, Arrays.asList(b1));
		Assert.assertEquals(
				"[]",
				Util.computeSetOfNeighboursToFowardJoinScope(
						new URI("mudebs://localhost:portnumber/client/x5"),
						null, u).toString());
	}

	/**
	 * <pre>
	 * joinScope(X5,U) at B1 from B5
	 * </pre>
	 * 
	 * after
	 * 
	 * <pre>
	 * joinScope(X2,T) at B1 from X2
	 * joinScope(X6,T) at B2 from B4
	 * joinScope(S,T) at B2
	 * joinScope(X3,S) at B2 from B3
	 * joinScope(S,T) at B1 from X1
	 * joinScope(S,U) at B1 from B1
	 * joinScope(X5,U) at B1 from B5
	 * </pre>
	 */
	@Test
	public void testGetNeighboursToFowardJoinB1_4_withList()
			throws MuDEBSException, URISyntaxException, IOException {
		URI d = new URI("mudebs://localhost:portnumber/dimension/d");
		URI b1 = new URI("mudebs://localhost:portnumber/broker/b1");
		URI b2 = new URI("mudebs://localhost:portnumber/broker/b2");
		URI b5 = new URI("mudebs://localhost:portnumber/broker/b5");
		BrokerState state = BrokerState.getInstance();
		state.reinitialise();
		state.brokerIdentities.put(b2, null);
		state.brokerIdentities.put(b5, null);
		// scope graph maintained at B2 = {(S,T)}
		ScopeGraph graph = new ScopeGraph(d);
		Scope u = new Scope(d, "u");
		Scope t = new Scope(d, "t");
		Scope s = new Scope(d, "s");
		graph.top().addSubScopes(Arrays.asList(u, t));
		state.scopeGraphs.put(graph.dimension(), graph);
		// scope look up tables of B2 : {(T,B1),(T,B2)}
		state.scopeLookupTables.put(t, Arrays.asList(b1, b2));
		state.scopeLookupTables.put(s, Arrays.asList(b1));
		state.scopeLookupTables.put(u, Arrays.asList(b1));
		Assert.assertEquals(
				"[]",
				Util.computeSetOfNeighboursToFowardJoinScope(
						new URI("mudebs://localhost:portnumber/client/x5"),
						null, u).toString());
	}

	/**
	 * <pre>
	 * joinscope(us,es) on B2
	 * </pre>
	 */
	@Test
	public void joinScopeExampleUsEsB2() throws MuDEBSException,
			URISyntaxException, IOException {
		URI d = new URI("mudebs://localhost:membership");
		URI b1 = new URI("mudebs://localhost:2003/B1");
		URI b2 = new URI("mudebs://localhost:2003/B2");
		URI b3 = new URI("mudebs://localhost:2003/B3");
		URI b4 = new URI("mudebs://localhost:2003/B4");
		BrokerState state = BrokerState.getInstance();
		// the broker
		state.identity = b2;
		// neighbours
		state.reinitialise();
		state.brokerIdentities.put(b1, null);
		state.brokerIdentities.put(b3, null);
		state.brokerIdentities.put(b4, null);
		// scope graph : (es, top), (us, es)
		ScopeGraph graph = new ScopeGraph(d);
		Scope us = new Scope(d, "us");
		Scope es = new Scope(d, "es");
		graph.addEdge(es, graph.top());
		graph.addEdge(us, es);
		state.scopeGraphs.put(graph.dimension(), graph);
		// scope look up table : (top, B1), (es, B1), (top, B4), (es, B4), (top,
		// B3), (es, B3), (es, B2), (us, B2)
		state.scopeLookupTables.put(graph.top(), Arrays.asList(b1, b4, b3));
		state.scopeLookupTables.put(es, Arrays.asList(b1, b4, b3, b2));
		state.scopeLookupTables.put(us, Arrays.asList(b2));
		// routing table : (es, d(us), B2), (us, u(es), B2)
		MultiScopingSpecification phiES = new MultiScopingSpecification();
		SetOfScopePathsWithStatus sspES = new SetOfScopePathsWithStatus();
		ScopePathWithStatus spES = new ScopePathWithStatus(d,
				ScopePathStatus.DOWN);
		spES.addScope(es);
		sspES.addScopePathWithStatus(spES);
		phiES.addSetOfScopePathsWithStatus(sspES);
		VisibilityFilter downToUS = new VisibilityFilter(
				"function evaluate() {return true;}", us);
		RoutingTableEntry re1 = new RoutingTableEntry(OperationalMode.LOCAL,
				"function evaluate() {return true;}", downToUS, "id1", phiES,
				EntityType.BROKER, b2);
		MultiScopingSpecification phiUS = new MultiScopingSpecification();
		SetOfScopePathsWithStatus sspUS = new SetOfScopePathsWithStatus();
		ScopePathWithStatus spUS = new ScopePathWithStatus(d,
				ScopePathStatus.UP);
		spUS.addScope(us);
		sspUS.addScopePathWithStatus(spUS);
		phiES.addSetOfScopePathsWithStatus(sspUS);
		VisibilityFilter upToES = new VisibilityFilter(
				"function evaluate() {return true;}", es);
		RoutingTableEntry re2 = new RoutingTableEntry(OperationalMode.LOCAL,
				"function evaluate() {return true;}", upToES, "id2", phiUS,
				EntityType.BROKER, b2);
		state.brokerRoutingTables.put(b3, Arrays.asList(re1, re2));
		// the test
		Assert.assertEquals("[]",
				Util.computeSetOfNeighboursToFowardJoinScope(b2, es, es)
						.toString());
	}
}
