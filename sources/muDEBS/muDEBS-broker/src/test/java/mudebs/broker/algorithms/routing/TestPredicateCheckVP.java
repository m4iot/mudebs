/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2017 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Léon Lim
Contributor(s): 
 */
package mudebs.broker.algorithms.routing;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;

import mudebs.broker.BrokerState;
import mudebs.common.MuDEBSException;
import mudebs.common.algorithms.routing.MultiScopingSpecification;
import mudebs.common.algorithms.routing.Scope;
import mudebs.common.algorithms.routing.ScopeBottom;
import mudebs.common.algorithms.routing.ScopeGraph;
import mudebs.common.algorithms.routing.ScopePathStatus;
import mudebs.common.algorithms.routing.ScopePathWithStatus;
import mudebs.common.algorithms.routing.ScopeTop;
import mudebs.common.algorithms.routing.SetOfScopePathsWithStatus;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

/**
 * Tests for the method (``predicate'') <tt>checkvp</tt>, and related methods.
 * 
 * NB: by a slight abuse of usage, the method toStringPath is used for
 * comparisons.
 * 
 * @author Léon Lim
 * @author denis Denis Conan
 *
 */
public class TestPredicateCheckVP {

	@Test
	public void testcomputeCommonSuffix1() throws MuDEBSException,
			URISyntaxException {
		URI d1 = new URI("mudebs:localhost:portnumber/membership");
		Scope ls = new Scope(d1, "ls");
		Scope us = new Scope(d1, "us");
		Scope es = new Scope(d1, "es");
		Scope fs = new Scope(d1, "fs");
		Scope is = new Scope(d1, "is");
		Scope ir = new Scope(d1, "ir");
		ls.addSuperScope(us);
		es.addSubScope(us);
		fs.addSuperScope(es);
		is.addSuperScope(fs);
		ir.addSubScope(is);
		ScopePathWithStatus sp1 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp1.addScope(ls);
		Assert.assertEquals(ScopePathStatus.UP, sp1.status());
		Assert.assertEquals(ls,
				sp1.scopeSequence().get(sp1.scopeSequence().size() - 1));
		sp1.addScope(us);
		Assert.assertEquals(ScopePathStatus.UP, sp1.status());
		Assert.assertEquals(us,
				sp1.scopeSequence().get(sp1.scopeSequence().size() - 1));
		sp1.addScope(es);
		Assert.assertEquals(ScopePathStatus.UP, sp1.status());
		Assert.assertEquals(es,
				sp1.scopeSequence().get(sp1.scopeSequence().size() - 1));
		sp1.addScope(fs);
		Assert.assertEquals(ScopePathStatus.DOWN, sp1.status());
		Assert.assertEquals(fs,
				sp1.scopeSequence().get(sp1.scopeSequence().size() - 1));
		sp1.addScope(is);
		Assert.assertEquals(ScopePathStatus.DOWN, sp1.status());
		Assert.assertEquals(is,
				sp1.scopeSequence().get(sp1.scopeSequence().size() - 1));
		Assert.assertEquals("((ls, us, es, fs, is), DOWN)", sp1.toStringPath());
		ScopePathWithStatus sp2 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp2.addScope(is);
		Assert.assertEquals("((ls, us, es, fs, is), DOWN)", sp1.toStringPath());
		Assert.assertEquals("((is), UP)", sp2.toStringPath());
		Assert.assertEquals("(is)", Util.computeCommonSuffixPrefix(sp1, sp2)
				.toString());
	}

	@Test
	public void testcomputeCommonSuffix2() throws MuDEBSException,
			URISyntaxException {
		URI d1 = new URI("mudebs:localhost:portnumber/membership");
		Scope ls = new Scope(d1, "ls");
		Scope us = new Scope(d1, "us");
		Scope es = new Scope(d1, "es");
		Scope fs = new Scope(d1, "fs");
		Scope is = new Scope(d1, "is");
		Scope ir = new Scope(d1, "ir");
		ls.addSuperScope(us);
		es.addSubScope(us);
		fs.addSuperScope(es);
		is.addSuperScope(fs);
		ir.addSubScope(is);
		ScopePathWithStatus sp1 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp1.addScope(ls);
		Assert.assertEquals(ScopePathStatus.UP, sp1.status());
		Assert.assertEquals(ls,
				sp1.scopeSequence().get(sp1.scopeSequence().size() - 1));
		sp1.addScope(us);
		Assert.assertEquals(ScopePathStatus.UP, sp1.status());
		Assert.assertEquals(us,
				sp1.scopeSequence().get(sp1.scopeSequence().size() - 1));
		sp1.addScope(es);
		Assert.assertEquals(ScopePathStatus.UP, sp1.status());
		Assert.assertEquals(es,
				sp1.scopeSequence().get(sp1.scopeSequence().size() - 1));
		sp1.addScope(fs);
		Assert.assertEquals(ScopePathStatus.DOWN, sp1.status());
		Assert.assertEquals(fs,
				sp1.scopeSequence().get(sp1.scopeSequence().size() - 1));
		sp1.addScope(is);
		Assert.assertEquals(ScopePathStatus.DOWN, sp1.status());
		Assert.assertEquals(is,
				sp1.scopeSequence().get(sp1.scopeSequence().size() - 1));
		Assert.assertEquals("((ls, us, es, fs, is), DOWN)", sp1.toStringPath());
		ScopePathWithStatus sp2 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp2.addScope(is);
		sp2.addScope(fs);
		sp2.addScope(es);
		Assert.assertEquals("((ls, us, es, fs, is), DOWN)", sp1.toStringPath());
		Assert.assertEquals("((is, fs, es), UP)", sp2.toStringPath());
		Assert.assertEquals("(es, fs, is)",
				Util.computeCommonSuffixPrefix(sp1, sp2).toString());
	}

	@Test
	public void testCheckvp1() throws MuDEBSException, URISyntaxException {
		URI d1 = new URI("mudebs:localhost:portnumber/membership");
		Scope ls = new Scope(d1, "ls");
		Scope us = new Scope(d1, "us");
		Scope es = new Scope(d1, "es");
		Scope fs = new Scope(d1, "fs");
		Scope is = new Scope(d1, "is");
		Scope ir = new Scope(d1, "ir");
		ls.addSuperScope(us);
		es.addSubScope(us);
		fs.addSuperScope(es);
		is.addSuperScope(fs);
		ir.addSubScope(is);
		ScopePathWithStatus sp1 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp1.addScope(Arrays.asList(is, fs, es));
		MultiScopingSpecification phiSUB = new MultiScopingSpecification();
		SetOfScopePathsWithStatus ssp1 = new SetOfScopePathsWithStatus();
		ssp1.addScopePathWithStatus(sp1);
		phiSUB.addSetOfScopePathsWithStatus(ssp1);
		MultiScopingSpecification phiPUB = MultiScopingSpecification.create(
				new ScopeBottom(), ScopePathStatus.UP);
		Assert.assertEquals("[{((is, fs, es), UP)}]", phiSUB.toString());
		Assert.assertEquals("[{((bot), UP)}]", phiPUB.toString());
		Assert.assertEquals(true, Util.checkvp(phiPUB, phiSUB, null));
	}

	@Test
	public void testCheckvp1bis() throws MuDEBSException, URISyntaxException {
		URI d1 = new URI("mudebs:localhost:portnumber/membership");
		Scope ls = new Scope(d1, "ls");
		Scope us = new Scope(d1, "us");
		Scope es = new Scope(d1, "es");
		Scope fs = new Scope(d1, "fs");
		Scope is = new Scope(d1, "is");
		Scope ir = new Scope(d1, "ir");
		ls.addSuperScope(us);
		es.addSubScope(us);
		fs.addSuperScope(es);
		is.addSuperScope(fs);
		ir.addSubScope(is);
		ScopePathWithStatus sp1 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp1.addScope(Arrays.asList(is, fs, es));
		MultiScopingSpecification phiSUB = new MultiScopingSpecification();
		SetOfScopePathsWithStatus ssp1 = new SetOfScopePathsWithStatus();
		ssp1.addScopePathWithStatus(sp1);
		phiSUB.addSetOfScopePathsWithStatus(ssp1);
		MultiScopingSpecification phiPUB = MultiScopingSpecification.create(
				new ScopeBottom(), ScopePathStatus.UP);
		Assert.assertEquals("[{((is, fs, es), UP)}]", phiSUB.toString());
		Assert.assertEquals("[{((bot), UP)}]", phiPUB.toString());
		Assert.assertEquals(true, Util.checkvp(phiPUB, phiSUB, null));
	}

	@Test
	public void testCheckvp2() throws MuDEBSException, URISyntaxException {
		URI d1 = new URI("mudebs:localhost:portnumber/membership");
		Scope ls = new Scope(d1, "ls");
		Scope us = new Scope(d1, "us");
		Scope es = new Scope(d1, "es");
		Scope fs = new Scope(d1, "fs");
		Scope is = new Scope(d1, "is");
		Scope ir = new Scope(d1, "ir");
		ls.addSuperScope(us);
		es.addSubScope(us);
		fs.addSuperScope(es);
		is.addSuperScope(fs);
		ir.addSubScope(is);
		ScopePathWithStatus sp1 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp1.addScope(Arrays.asList(is, fs, es));
		MultiScopingSpecification phiSUB = new MultiScopingSpecification();
		SetOfScopePathsWithStatus ssp1 = new SetOfScopePathsWithStatus();
		ssp1.addScopePathWithStatus(sp1);
		phiSUB.addSetOfScopePathsWithStatus(ssp1);
		ScopeBottom bot1 = new ScopeBottom(d1);
		ScopePathWithStatus sp2 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp2.addScope(bot1);
		SetOfScopePathsWithStatus ssp2 = new SetOfScopePathsWithStatus();
		ssp2.addScopePathWithStatus(sp2);
		MultiScopingSpecification phiPUB = new MultiScopingSpecification();
		phiPUB.addSetOfScopePathsWithStatus(ssp2);
		Assert.assertEquals("[{((is, fs, es), UP)}]", phiSUB.toString());
		Assert.assertEquals("[{((bot), UP)}]", phiPUB.toString());
		Assert.assertEquals(false, Util.checkvp(phiPUB, phiSUB, null));
	}

	@Test
	public void testCheckvp3() throws MuDEBSException, URISyntaxException {
		URI d1 = new URI("mudebs:localhost:portnumber/membership");
		Scope ls = new Scope(d1, "ls");
		Scope us = new Scope(d1, "us");
		Scope es = new Scope(d1, "es");
		Scope fs = new Scope(d1, "fs");
		Scope is = new Scope(d1, "is");
		Scope ir = new Scope(d1, "ir");
		ls.addSuperScope(us);
		es.addSubScope(us);
		fs.addSuperScope(es);
		is.addSuperScope(fs);
		ir.addSubScope(is);
		ScopePathWithStatus sp1 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp1.addScope(Arrays.asList(is, fs, es));
		MultiScopingSpecification phiSUB = new MultiScopingSpecification();
		SetOfScopePathsWithStatus ssp1 = new SetOfScopePathsWithStatus();
		ssp1.addScopePathWithStatus(sp1);
		phiSUB.addSetOfScopePathsWithStatus(ssp1);
		ScopeTop bot1 = new ScopeTop(d1);
		ScopePathWithStatus sp2 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp2.addScope(bot1);
		SetOfScopePathsWithStatus ssp2 = new SetOfScopePathsWithStatus();
		ssp2.addScopePathWithStatus(sp2);
		MultiScopingSpecification phiPUB = new MultiScopingSpecification();
		phiPUB.addSetOfScopePathsWithStatus(ssp2);
		Assert.assertEquals("[{((is, fs, es), UP)}]", phiSUB.toString());
		Assert.assertEquals("[{((top), UP)}]", phiPUB.toString());
		Assert.assertEquals(false, Util.checkvp(phiSUB, phiPUB, null));
	}

	@Test
	public void testCheckvp4() throws MuDEBSException, URISyntaxException {
		URI d1 = new URI("mudebs:localhost:portnumber/membership");
		Scope ls = new Scope(d1, "ls");
		Scope us = new Scope(d1, "us");
		Scope es = new Scope(d1, "es");
		Scope fs = new Scope(d1, "fs");
		Scope is = new Scope(d1, "is");
		Scope ir = new Scope(d1, "ir");
		ls.addSuperScope(us);
		es.addSubScope(us);
		fs.addSuperScope(es);
		is.addSuperScope(fs);
		ir.addSubScope(is);
		ScopePathWithStatus sp1 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp1.addScope(Arrays.asList(is, fs, es));
		MultiScopingSpecification phiSUB = new MultiScopingSpecification();
		SetOfScopePathsWithStatus ssp1 = new SetOfScopePathsWithStatus();
		ssp1.addScopePathWithStatus(sp1);
		phiSUB.addSetOfScopePathsWithStatus(ssp1);
		MultiScopingSpecification phiPUB = MultiScopingSpecification.create(
				new ScopeBottom(), ScopePathStatus.UP);
		Assert.assertEquals("[{((is, fs, es), UP)}]", phiSUB.toString());
		Assert.assertEquals("[{((bot), UP)}]", phiPUB.toString());
		URI d2 = new URI("mudebs:localhost:portnumber/administrationarea");
		Scope lo = new Scope(d2, "lo");
		ScopePathWithStatus sp3 = new ScopePathWithStatus(d2,
				ScopePathStatus.UP);
		sp3.addScope(lo);
		SetOfScopePathsWithStatus ssp3 = new SetOfScopePathsWithStatus();
		ssp3.addScopePathWithStatus(sp3);
		phiSUB.addSetOfScopePathsWithStatus(ssp3);
		Assert.assertEquals("[{((lo), UP)}{((is, fs, es), UP)}]",
				phiSUB.toString());
		Assert.assertEquals("[{((bot), UP)}]", phiPUB.toString());
		Assert.assertEquals(true, Util.checkvp(phiPUB, phiSUB, null));
	}

	@Test
	public void testCheckvp5() throws MuDEBSException, URISyntaxException {
		URI d1 = new URI("mudebs:localhost:portnumber/membership");
		Scope ls = new Scope(d1, "ls");
		Scope us = new Scope(d1, "us");
		Scope es = new Scope(d1, "es");
		Scope fs = new Scope(d1, "fs");
		Scope is = new Scope(d1, "is");
		Scope ir = new Scope(d1, "ir");
		ls.addSuperScope(us);
		es.addSubScope(us);
		fs.addSuperScope(es);
		is.addSuperScope(fs);
		ir.addSubScope(is);
		ScopePathWithStatus sp1 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp1.addScope(Arrays.asList(is, fs, es));
		MultiScopingSpecification phiSUB = new MultiScopingSpecification();
		SetOfScopePathsWithStatus ssp1 = new SetOfScopePathsWithStatus();
		ssp1.addScopePathWithStatus(sp1);
		phiSUB.addSetOfScopePathsWithStatus(ssp1);
		ScopeBottom bot1 = new ScopeBottom(d1);
		ScopePathWithStatus sp2 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp2.addScope(bot1);
		SetOfScopePathsWithStatus ssp2 = new SetOfScopePathsWithStatus();
		ssp2.addScopePathWithStatus(sp2);
		MultiScopingSpecification phiPUB = new MultiScopingSpecification();
		phiPUB.addSetOfScopePathsWithStatus(ssp2);
		Assert.assertEquals("[{((is, fs, es), UP)}]", phiSUB.toString());
		Assert.assertEquals("[{((bot), UP)}]", phiPUB.toString());
		URI d2 = new URI("mudebs:localhost:portnumber/administrationarea");
		Scope lo = new Scope(d2, "lo");
		ScopePathWithStatus sp3 = new ScopePathWithStatus(d2,
				ScopePathStatus.UP);
		sp3.addScope(lo);
		SetOfScopePathsWithStatus ssp3 = new SetOfScopePathsWithStatus();
		ssp3.addScopePathWithStatus(sp3);
		phiSUB.addSetOfScopePathsWithStatus(ssp3);
		Assert.assertEquals("[{((bot), UP)}]", phiPUB.toString());
		Assert.assertEquals("[{((lo), UP)}{((is, fs, es), UP)}]",
				phiSUB.toString());
		Assert.assertEquals(false, Util.checkvp(phiSUB, phiPUB, null));
	}

	@Test
	public void testCheckvp6() throws MuDEBSException, URISyntaxException {
		// dimension membership
		URI d1 = new URI("mudebs:localhost:portnumber/membership");
		// scopes
		Scope ls = new Scope(d1, "ls");
		Scope us = new Scope(d1, "us");
		Scope es = new Scope(d1, "es");
		Scope fs = new Scope(d1, "fs");
		Scope is = new Scope(d1, "is");
		Scope ir = new Scope(d1, "ir");
		// scope graph
		ScopeGraph g1 = new ScopeGraph(d1);
		g1.addEdge(us, es);
		g1.addEdge(fs, es);
		g1.addEdge(ls, us);
		g1.addEdge(is, fs);
		g1.addEdge(is, ir);
		// phi pub
		MultiScopingSpecification phiPUB = new MultiScopingSpecification();
		SetOfScopePathsWithStatus ssp1 = new SetOfScopePathsWithStatus();
		ScopePathWithStatus sp1 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp1.addScope(ls);
		ScopePathWithStatus sp2 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp2.addScope(ls);
		sp2.addScope(us);
		ScopePathWithStatus sp3 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp3.addScope(ls);
		sp3.addScope(us);
		sp3.addScope(es);
		ssp1.addScopePathWithStatus(sp1);
		ssp1.addScopePathWithStatus(sp2);
		ssp1.addScopePathWithStatus(sp3);
		phiPUB.addSetOfScopePathsWithStatus(ssp1);
		Assert.assertEquals(
				"[{((ls), UP), ((ls, us), UP), ((ls, us, es), UP)}]",
				phiPUB.toString());
		// phi sub
		MultiScopingSpecification phiSUB = MultiScopingSpecification.create(
				new ScopeTop(), ScopePathStatus.UP);
		Assert.assertEquals("[{((top), UP)}]", phiSUB.toString());
		BrokerState state = BrokerState.getInstance();
		URI b1 = new URI("mudebs://localhost:portnumber/broker/b1");
		state.brokerIdentities.put(b1, null);
		Assert.assertEquals(false, Util.checkvp(phiSUB, phiPUB, b1));
		Assert.assertEquals(true, Util.checkvp(phiPUB, phiSUB, b1));
	}

	@Test
	public void testCheckvp7() throws MuDEBSException, URISyntaxException {
		// phi pub
		MultiScopingSpecification phiPUB = MultiScopingSpecification.create(
				new ScopeBottom(), ScopePathStatus.UP);
		Assert.assertEquals("[{((bot), UP)}]", phiPUB.toString());
		// phi sub
		MultiScopingSpecification phiSUB = MultiScopingSpecification.create(
				new ScopeTop(), ScopePathStatus.UP);
		Assert.assertEquals("[{((top), UP)}]", phiSUB.toString());

		BrokerState state = BrokerState.getInstance();
		URI b1 = new URI("mudebs://localhost:portnumber/broker/b1");
		state.brokerIdentities.put(b1, null);
		Assert.assertEquals(false, Util.checkvp(phiSUB, phiPUB, b1));
		Assert.assertEquals(true, Util.checkvp(phiPUB, phiSUB, b1));
	}

	@Test
	public void testCheckvp6and7() throws MuDEBSException, URISyntaxException {
		// dimension membership
		URI d1 = new URI("mudebs:localhost:portnumber/membership");
		// scopes
		Scope ls = new Scope(d1, "ls");
		Scope us = new Scope(d1, "us");
		Scope es = new Scope(d1, "es");
		Scope fs = new Scope(d1, "fs");
		Scope is = new Scope(d1, "is");
		Scope ir = new Scope(d1, "ir");
		// scope graph
		ScopeGraph g1 = new ScopeGraph(d1);
		g1.addEdge(us, es);
		g1.addEdge(fs, es);
		g1.addEdge(ls, us);
		g1.addEdge(is, fs);
		g1.addEdge(is, ir);
		// phi pub
		MultiScopingSpecification phiPUB = MultiScopingSpecification.create(
				new ScopeBottom(), ScopePathStatus.UP);
		// phi sub
		MultiScopingSpecification phiSUB = new MultiScopingSpecification();
		SetOfScopePathsWithStatus ssp2 = new SetOfScopePathsWithStatus();
		ScopePathWithStatus sp4 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp4.addScope(is);
		ScopePathWithStatus sp5 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp5.addScope(is);
		sp5.addScope(ir);
		ScopePathWithStatus sp6 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp6.addScope(is);
		sp6.addScope(fs);
		ScopePathWithStatus sp7 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp7.addScope(is);
		sp7.addScope(fs);
		sp7.addScope(es);
		ScopePathWithStatus sp8 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp8.addScope(is);
		sp8.addScope(fs);
		sp8.addScope(es);
		sp8.addScope(us);
		ssp2.addScopePathWithStatus(sp4);
		ssp2.addScopePathWithStatus(sp5);
		ssp2.addScopePathWithStatus(sp6);
		ssp2.addScopePathWithStatus(sp7);
		ssp2.addScopePathWithStatus(sp8);
		phiSUB.addSetOfScopePathsWithStatus(ssp2);
		BrokerState state = BrokerState.getInstance();
		state.clientIdentities.put(new URI("mudebs://localhost:2103/Y"), null);
		Assert.assertEquals("[{((bot), UP)}]", phiPUB.toString());
		Assert.assertEquals(
				"[{((is), UP), ((is, ir), UP), ((is, fs), UP), ((is, fs, es), UP), ((is, fs, es, us), DOWN)}]",
				phiSUB.toString());
		Assert.assertEquals(false, Util.checkvp(phiSUB, phiPUB, new URI(
				"mudebs://localhost:2103/Y")));
		Assert.assertEquals(true, Util.checkvp(phiPUB, phiSUB, new URI(
				"mudebs://localhost:2103/Y")));
	}

	@Test
	public void testCheckvp8() throws MuDEBSException, URISyntaxException {
		// dimension membership
		URI d1 = new URI("mudebs:localhost:portnumber/membership");
		// scopes
		Scope ls = new Scope(d1, "ls");
		Scope us = new Scope(d1, "us");
		Scope es = new Scope(d1, "es");
		Scope fs = new Scope(d1, "fs");
		Scope is = new Scope(d1, "is");
		Scope ir = new Scope(d1, "ir");
		// scope graph
		ScopeGraph g1 = new ScopeGraph(d1);
		g1.addEdge(us, es);
		g1.addEdge(fs, es);
		g1.addEdge(ls, us);
		g1.addEdge(is, fs);
		g1.addEdge(is, ir);
		// phi pub
		MultiScopingSpecification phiPUB = new MultiScopingSpecification();
		SetOfScopePathsWithStatus ssp1 = new SetOfScopePathsWithStatus();
		ScopePathWithStatus sp1 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp1.addScope(ls);
		ScopePathWithStatus sp2 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp2.addScope(ls);
		sp2.addScope(us);
		ScopePathWithStatus sp3 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp3.addScope(ls);
		sp3.addScope(us);
		sp3.addScope(es);
		ssp1.addScopePathWithStatus(sp1);
		ssp1.addScopePathWithStatus(sp2);
		ssp1.addScopePathWithStatus(sp3);
		phiPUB.addSetOfScopePathsWithStatus(ssp1);
		// phi sub
		MultiScopingSpecification phiSUB = new MultiScopingSpecification();
		SetOfScopePathsWithStatus ssp2 = new SetOfScopePathsWithStatus();
		ScopePathWithStatus sp4 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp4.addScope(is);
		ScopePathWithStatus sp5 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp5.addScope(is);
		sp5.addScope(ir);
		ScopePathWithStatus sp6 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp6.addScope(is);
		sp6.addScope(fs);
		ScopePathWithStatus sp7 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp7.addScope(is);
		sp7.addScope(fs);
		sp7.addScope(es);
		ScopePathWithStatus sp8 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp8.addScope(is);
		sp8.addScope(fs);
		sp8.addScope(es);
		sp8.addScope(us);
		ssp2.addScopePathWithStatus(sp4);
		ssp2.addScopePathWithStatus(sp5);
		ssp2.addScopePathWithStatus(sp6);
		ssp2.addScopePathWithStatus(sp7);
		ssp2.addScopePathWithStatus(sp8);
		phiSUB.addSetOfScopePathsWithStatus(ssp2);
		Assert.assertEquals(
				"[{((ls), UP), ((ls, us), UP), ((ls, us, es), UP)}]",
				phiPUB.toString());
		Assert.assertEquals(
				"[{((is), UP), ((is, ir), UP), ((is, fs), UP), ((is, fs, es), UP), ((is, fs, es, us), DOWN)}]",
				phiSUB.toString());
		Assert.assertEquals("(us, es)", Util
				.computeCommonSuffixPrefix(sp3, sp8).toString());
		Assert.assertEquals("(es, us)", Util
				.computeCommonSuffixPrefix(sp8, sp3).toString());
		BrokerState state = BrokerState.getInstance();
		URI b1 = new URI("mudebs://localhost:portnumber/broker/b1");
		state.brokerIdentities.put(b1, null);
		Assert.assertEquals(true, Util.checkvp(phiPUB, phiSUB, b1));
		Assert.assertEquals(true, Util.checkvp(phiSUB, phiPUB, b1));
	}

	@Test
	public void testCheckvpTwoDimensions() throws MuDEBSException,
			URISyntaxException {
		// dimension membership (m)
		URI d1 = new URI("mudebs:localhost:portnumber/membership");
		// scopes
		Scope ls = new Scope(d1, "ls");
		Scope us = new Scope(d1, "us");
		Scope es = new Scope(d1, "es");
		Scope fs = new Scope(d1, "fs");
		Scope is = new Scope(d1, "is");
		Scope ir = new Scope(d1, "ir");
		// scope graph (m)
		ScopeGraph g1 = new ScopeGraph(d1);
		g1.addEdge(us, es);
		g1.addEdge(fs, es);
		g1.addEdge(ls, us);
		g1.addEdge(is, fs);
		g1.addEdge(is, ir);
		// phi pub (m)
		MultiScopingSpecification phiPUB = new MultiScopingSpecification();
		SetOfScopePathsWithStatus ssp1 = new SetOfScopePathsWithStatus();
		ScopePathWithStatus sp1 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp1.addScope(ls);
		ScopePathWithStatus sp2 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp2.addScope(ls);
		sp2.addScope(us);
		ScopePathWithStatus sp3 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp3.addScope(ls);
		sp3.addScope(us);
		sp3.addScope(es);
		ssp1.addScopePathWithStatus(sp1);
		ssp1.addScopePathWithStatus(sp2);
		ssp1.addScopePathWithStatus(sp3);
		phiPUB.addSetOfScopePathsWithStatus(ssp1);
		Assert.assertEquals(
				"[{((ls), UP), ((ls, us), UP), ((ls, us, es), UP)}]",
				phiPUB.toString());
		// dimension administration area (aa)
		URI d2 = new URI("mudebs:localhost:portnumber/administrationarea");
		Scope ch = new Scope(d2, "ch");
		Scope bo = new Scope(d2, "bo");
		Scope fr = new Scope(d2, "fr");
		Scope eu = new Scope(d2, "eu");
		Scope uk = new Scope(d2, "uk");
		Scope lo = new Scope(d2, "lo");
		// scope graph (aa)
		ScopeGraph g2 = new ScopeGraph(d2);
		g2.addEdge(lo, uk);
		g2.addEdge(uk, eu);
		g2.addEdge(fr, eu);
		g2.addEdge(bo, fr);
		g2.addEdge(ch, bo);
		// update phi pub
		SetOfScopePathsWithStatus ssp1aa = new SetOfScopePathsWithStatus();
		ScopePathWithStatus sp1aa = new ScopePathWithStatus(d2,
				ScopePathStatus.UP);
		sp1aa.addScope(Arrays.asList(ch));
		ScopePathWithStatus sp2aa = new ScopePathWithStatus(d2,
				ScopePathStatus.UP);
		sp2aa.addScope(Arrays.asList(ch, bo));
		ScopePathWithStatus sp3aa = new ScopePathWithStatus(d2,
				ScopePathStatus.UP);
		sp3aa.addScope(Arrays.asList(ch, bo, fr));
		ScopePathWithStatus sp4aa = new ScopePathWithStatus(d2,
				ScopePathStatus.UP);
		sp4aa.addScope(Arrays.asList(ch, bo, fr, eu));
		ssp1aa.addScopePathWithStatus(sp1aa);
		ssp1aa.addScopePathWithStatus(sp2aa);
		ssp1aa.addScopePathWithStatus(sp3aa);
		ssp1aa.addScopePathWithStatus(sp4aa);
		phiPUB.addSetOfScopePathsWithStatus(ssp1aa);
		Assert.assertEquals(
				"[{((ch), UP), ((ch, bo), UP), ((ch, bo, fr), UP), "
						+ "((ch, bo, fr, eu), UP)}{((ls), UP), ((ls, us), UP), "
						+ "((ls, us, es), UP)}]", phiPUB.toString());
		// phi sub
		MultiScopingSpecification phiSUB = new MultiScopingSpecification();
		SetOfScopePathsWithStatus ssp2 = new SetOfScopePathsWithStatus();
		ScopePathWithStatus sp4 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp4.addScope(is);
		ScopePathWithStatus sp5 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp5.addScope(is);
		sp5.addScope(ir);
		ScopePathWithStatus sp6 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp6.addScope(is);
		sp6.addScope(fs);
		ScopePathWithStatus sp7 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp7.addScope(is);
		sp7.addScope(fs);
		sp7.addScope(es);
		ScopePathWithStatus sp8 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp8.addScope(is);
		sp8.addScope(fs);
		sp8.addScope(es);
		sp8.addScope(us);
		ssp2.addScopePathWithStatus(sp4);
		ssp2.addScopePathWithStatus(sp5);
		ssp2.addScopePathWithStatus(sp6);
		ssp2.addScopePathWithStatus(sp7);
		ssp2.addScopePathWithStatus(sp8);
		phiSUB.addSetOfScopePathsWithStatus(ssp2);
		// update phi sub
		SetOfScopePathsWithStatus ssp2aa = new SetOfScopePathsWithStatus();
		ScopePathWithStatus sp9 = new ScopePathWithStatus(d2,
				ScopePathStatus.UP);
		sp9.addScope(Arrays.asList(lo));
		ScopePathWithStatus sp10 = new ScopePathWithStatus(d2,
				ScopePathStatus.UP);
		sp10.addScope(Arrays.asList(lo, uk));
		ScopePathWithStatus sp11 = new ScopePathWithStatus(d2,
				ScopePathStatus.UP);
		sp11.addScope(Arrays.asList(lo, uk, eu));
		ScopePathWithStatus sp12 = new ScopePathWithStatus(d2,
				ScopePathStatus.UP);
		sp12.addScope(Arrays.asList(lo, uk, eu, fr));
		ssp2aa.addScopePathWithStatus(sp9);
		ssp2aa.addScopePathWithStatus(sp10);
		ssp2aa.addScopePathWithStatus(sp11);
		ssp2aa.addScopePathWithStatus(sp12);
		phiSUB.addSetOfScopePathsWithStatus(ssp2aa);
		Assert.assertEquals(
				"[{((lo), UP), ((lo, uk), UP), ((lo, uk, eu), UP), "
						+ "((lo, uk, eu, fr), DOWN)}{((is), UP), ((is, ir), UP), "
						+ "((is, fs), UP), ((is, fs, es), UP), ((is, fs, es, us), DOWN)}]",
				phiSUB.toString());
		Assert.assertEquals("(us, es)", Util
				.computeCommonSuffixPrefix(sp3, sp8).toString());
		BrokerState state = BrokerState.getInstance();
		URI b1 = new URI("mudebs://localhost:portnumber/broker/b1");
		state.brokerIdentities.put(b1, null);
		Assert.assertEquals(true, Util.checkvp(phiSUB, phiPUB, b1));
		Assert.assertEquals(true, Util.checkvp(phiPUB, phiSUB, b1));
	}

	@Ignore
	public void testCheckvpTwoDimensionsBottom() throws MuDEBSException,
			URISyntaxException {
		// dimension membership (m)
		URI d1 = new URI("mudebs:localhost:portnumber/membership");
		// scopes
		Scope ls = new Scope(d1, "ls");
		Scope us = new Scope(d1, "us");
		Scope es = new Scope(d1, "es");
		Scope fs = new Scope(d1, "fs");
		Scope is = new Scope(d1, "is");
		Scope ir = new Scope(d1, "ir");
		// scope graph (m)
		ScopeGraph g1 = new ScopeGraph(d1);
		g1.addEdge(us, es);
		g1.addEdge(fs, es);
		g1.addEdge(ls, us);
		g1.addEdge(is, fs);
		g1.addEdge(is, ir);
		// phi pub (m)
		MultiScopingSpecification phiPUB = new MultiScopingSpecification();
		SetOfScopePathsWithStatus ssp1 = new SetOfScopePathsWithStatus();
		ScopePathWithStatus sp1 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp1.addScope(ls);
		ScopePathWithStatus sp2 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp2.addScope(ls);
		sp2.addScope(us);
		ScopePathWithStatus sp3 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp3.addScope(ls);
		sp3.addScope(us);
		sp3.addScope(es);
		ssp1.addScopePathWithStatus(sp1);
		ssp1.addScopePathWithStatus(sp2);
		ssp1.addScopePathWithStatus(sp3);
		phiPUB.addSetOfScopePathsWithStatus(ssp1);
		Assert.assertEquals(
				"[{((ls), UP), ((ls, us), UP), ((ls, us, es), UP)}]",
				phiPUB.toString());
		// dimension administration area (aa)
		URI d2 = new URI("mudebs:localhost:portnumber/administrationarea");
		Scope ch = new Scope(d2, "ch");
		Scope bo = new Scope(d2, "bo");
		Scope fr = new Scope(d2, "fr");
		Scope eu = new Scope(d2, "eu");
		Scope uk = new Scope(d2, "uk");
		Scope lo = new Scope(d2, "lo");
		// scope graph (aa)
		ScopeGraph g2 = new ScopeGraph(d2);
		g2.addEdge(lo, uk);
		g2.addEdge(uk, eu);
		g2.addEdge(fr, eu);
		g2.addEdge(bo, fr);
		g2.addEdge(ch, bo);
		// update phi pub
		SetOfScopePathsWithStatus ssp1aa = new SetOfScopePathsWithStatus();
		ScopePathWithStatus sp1aa = new ScopePathWithStatus(d2,
				ScopePathStatus.UP);
		sp1aa.addScope(g2.bot());
		ssp1aa.addScopePathWithStatus(sp1aa);
		phiPUB.addSetOfScopePathsWithStatus(ssp1aa);
		Assert.assertEquals(
				"[{((bot), UP)}{((ls), UP), ((ls, us), UP), ((ls, us, es), UP)}]",
				phiPUB.toString());
		// phi sub
		MultiScopingSpecification phiSUB = new MultiScopingSpecification();
		SetOfScopePathsWithStatus ssp2 = new SetOfScopePathsWithStatus();
		ScopePathWithStatus sp4 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp4.addScope(is);
		ScopePathWithStatus sp5 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp5.addScope(is);
		sp5.addScope(ir);
		ScopePathWithStatus sp6 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp6.addScope(is);
		sp6.addScope(fs);
		ScopePathWithStatus sp7 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp7.addScope(is);
		sp7.addScope(fs);
		sp7.addScope(es);
		ScopePathWithStatus sp8 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp8.addScope(is);
		sp8.addScope(fs);
		sp8.addScope(es);
		sp8.addScope(us);
		ssp2.addScopePathWithStatus(sp4);
		ssp2.addScopePathWithStatus(sp5);
		ssp2.addScopePathWithStatus(sp6);
		ssp2.addScopePathWithStatus(sp7);
		ssp2.addScopePathWithStatus(sp8);
		phiSUB.addSetOfScopePathsWithStatus(ssp2);
		// update phi sub
		SetOfScopePathsWithStatus ssp2aa = new SetOfScopePathsWithStatus();
		ScopePathWithStatus sp9 = new ScopePathWithStatus(d2,
				ScopePathStatus.UP);
		sp9.addScope(Arrays.asList(lo));
		ScopePathWithStatus sp10 = new ScopePathWithStatus(d2,
				ScopePathStatus.UP);
		sp10.addScope(Arrays.asList(lo, uk));
		ScopePathWithStatus sp11 = new ScopePathWithStatus(d2,
				ScopePathStatus.UP);
		sp11.addScope(Arrays.asList(lo, uk, eu));
		ScopePathWithStatus sp12 = new ScopePathWithStatus(d2,
				ScopePathStatus.UP);
		sp12.addScope(Arrays.asList(lo, uk, eu, fr));
		ssp2aa.addScopePathWithStatus(sp9);
		ssp2aa.addScopePathWithStatus(sp10);
		ssp2aa.addScopePathWithStatus(sp11);
		ssp2aa.addScopePathWithStatus(sp12);
		phiSUB.addSetOfScopePathsWithStatus(ssp2aa);
		Assert.assertEquals(
				"[{((lo), UP), ((lo, uk), UP), ((lo, uk, eu), UP), "
						+ "((lo, uk, eu, fr), DOWN)}{((is), UP), ((is, ir), UP), "
						+ "((is, fs), UP), ((is, fs, es), UP), ((is, fs, es, us), DOWN)}]",
				phiSUB.toString());
		Assert.assertEquals("(us, es)", Util
				.computeCommonSuffixPrefix(sp3, sp8).toString());
		Assert.assertEquals(true, Util.checkvp(phiSUB, phiPUB, null));
		Assert.assertEquals(true, Util.checkvp(phiPUB, phiSUB, null));
	}

	@Ignore
	public void testCheckvpTwoDimensionsTop() throws MuDEBSException,
			URISyntaxException {
		// dimension membership (m)
		URI d1 = new URI("mudebs:localhost:portnumber/membership");
		// scopes
		Scope ls = new Scope(d1, "ls");
		Scope us = new Scope(d1, "us");
		Scope es = new Scope(d1, "es");
		Scope fs = new Scope(d1, "fs");
		Scope is = new Scope(d1, "is");
		Scope ir = new Scope(d1, "ir");
		// scope graph (m)
		ScopeGraph g1 = new ScopeGraph(d1);
		g1.addEdge(us, es);
		g1.addEdge(fs, es);
		g1.addEdge(ls, us);
		g1.addEdge(is, fs);
		g1.addEdge(is, ir);
		// phi pub (m)
		MultiScopingSpecification phiPUB = new MultiScopingSpecification();
		SetOfScopePathsWithStatus ssp1 = new SetOfScopePathsWithStatus();
		ScopePathWithStatus sp1 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp1.addScope(ls);
		ScopePathWithStatus sp2 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp2.addScope(ls);
		sp2.addScope(us);
		ScopePathWithStatus sp3 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp3.addScope(ls);
		sp3.addScope(us);
		sp3.addScope(es);
		ssp1.addScopePathWithStatus(sp1);
		ssp1.addScopePathWithStatus(sp2);
		ssp1.addScopePathWithStatus(sp3);
		phiPUB.addSetOfScopePathsWithStatus(ssp1);
		Assert.assertEquals(
				"[{((ls), UP), ((ls, us), UP), ((ls, us, es), UP)}]",
				phiPUB.toString());
		// dimension administration area (aa)
		URI d2 = new URI("mudebs:localhost:portnumber/administrationarea");
		Scope ch = new Scope(d2, "ch");
		Scope bo = new Scope(d2, "bo");
		Scope fr = new Scope(d2, "fr");
		Scope eu = new Scope(d2, "eu");
		Scope uk = new Scope(d2, "uk");
		Scope lo = new Scope(d2, "lo");
		// scope graph (aa)
		ScopeGraph g2 = new ScopeGraph(d2);
		g2.addEdge(lo, uk);
		g2.addEdge(uk, eu);
		g2.addEdge(fr, eu);
		g2.addEdge(bo, fr);
		g2.addEdge(ch, bo);
		// update phi pub
		SetOfScopePathsWithStatus ssp1aa = new SetOfScopePathsWithStatus();
		ScopePathWithStatus sp1aa = new ScopePathWithStatus(d2,
				ScopePathStatus.UP);
		sp1aa.addScope(Arrays.asList(ch));
		ScopePathWithStatus sp2aa = new ScopePathWithStatus(d2,
				ScopePathStatus.UP);
		sp2aa.addScope(Arrays.asList(ch, bo));
		ScopePathWithStatus sp3aa = new ScopePathWithStatus(d2,
				ScopePathStatus.UP);
		sp3aa.addScope(Arrays.asList(ch, bo, fr));
		ScopePathWithStatus sp4aa = new ScopePathWithStatus(d2,
				ScopePathStatus.UP);
		sp4aa.addScope(Arrays.asList(ch, bo, fr, eu));
		ssp1aa.addScopePathWithStatus(sp1aa);
		ssp1aa.addScopePathWithStatus(sp2aa);
		ssp1aa.addScopePathWithStatus(sp3aa);
		ssp1aa.addScopePathWithStatus(sp4aa);
		phiPUB.addSetOfScopePathsWithStatus(ssp1aa);
		Assert.assertEquals(
				"[{((ch), UP), ((ch, bo), UP), ((ch, bo, fr), UP), "
						+ "((ch, bo, fr, eu), UP)}{((ls), UP), ((ls, us), UP), "
						+ "((ls, us, es), UP)}]", phiPUB.toString());
		// phi sub
		MultiScopingSpecification phiSUB = new MultiScopingSpecification();
		SetOfScopePathsWithStatus ssp2 = new SetOfScopePathsWithStatus();
		ScopePathWithStatus sp4 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp4.addScope(is);
		ScopePathWithStatus sp5 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp5.addScope(is);
		sp5.addScope(ir);
		ScopePathWithStatus sp6 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp6.addScope(is);
		sp6.addScope(fs);
		ScopePathWithStatus sp7 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp7.addScope(is);
		sp7.addScope(fs);
		sp7.addScope(es);
		ScopePathWithStatus sp8 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp8.addScope(is);
		sp8.addScope(fs);
		sp8.addScope(es);
		sp8.addScope(us);
		ssp2.addScopePathWithStatus(sp4);
		ssp2.addScopePathWithStatus(sp5);
		ssp2.addScopePathWithStatus(sp6);
		ssp2.addScopePathWithStatus(sp7);
		ssp2.addScopePathWithStatus(sp8);
		phiSUB.addSetOfScopePathsWithStatus(ssp2);
		// update phi sub
		SetOfScopePathsWithStatus ssp2aa = new SetOfScopePathsWithStatus();
		ScopePathWithStatus sp9 = new ScopePathWithStatus(d2,
				ScopePathStatus.UP);
		sp9.addScope(g2.top());
		ssp2aa.addScopePathWithStatus(sp9);
		phiSUB.addSetOfScopePathsWithStatus(ssp2aa);
		Assert.assertEquals(
				"[{((top), UP)}{((is), UP), ((is, ir), UP), "
						+ "((is, fs), UP), ((is, fs, es), UP), ((is, fs, es, us), DOWN)}]",
				phiSUB.toString());
		Assert.assertEquals("(us, es)", Util
				.computeCommonSuffixPrefix(sp3, sp8).toString());
		Assert.assertEquals(true, Util.checkvp(phiSUB, phiPUB, null));
		Assert.assertEquals(true, Util.checkvp(phiPUB, phiSUB, null));
	}

	@Test
	public void testCheckvpTwoDimensionsNoPath() throws MuDEBSException,
			URISyntaxException {
		// dimension membership (m)
		URI d1 = new URI("mudebs:localhost:portnumber/membership");
		// scopes
		Scope ls = new Scope(d1, "ls");
		Scope us = new Scope(d1, "us");
		Scope es = new Scope(d1, "es");
		Scope fs = new Scope(d1, "fs");
		Scope is = new Scope(d1, "is");
		Scope ir = new Scope(d1, "ir");
		// scope graph (m)
		ScopeGraph g1 = new ScopeGraph(d1);
		g1.addEdge(us, es);
		g1.addEdge(fs, es);
		g1.addEdge(ls, us);
		g1.addEdge(is, fs);
		g1.addEdge(is, ir);
		// phi pub (m)
		MultiScopingSpecification phiPUB = new MultiScopingSpecification();
		SetOfScopePathsWithStatus ssp1 = new SetOfScopePathsWithStatus();
		ScopePathWithStatus sp1 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp1.addScope(ls);
		ScopePathWithStatus sp2 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp2.addScope(ls);
		sp2.addScope(us);
		ScopePathWithStatus sp3 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp3.addScope(Arrays.asList(ls, us, es));
		ssp1.addScopePathWithStatus(Arrays.asList(sp1, sp2, sp3));
		phiPUB.addSetOfScopePathsWithStatus(ssp1);
		Assert.assertEquals(
				"[{((ls), UP), ((ls, us), UP), ((ls, us, es), UP)}]",
				phiPUB.toString());
		// dimension administration area (aa)
		URI d2 = new URI("mudebs:localhost:portnumber/administrationarea");
		Scope ch = new Scope(d2, "ch");
		Scope bo = new Scope(d2, "bo");
		Scope fr = new Scope(d2, "fr");
		Scope eu = new Scope(d2, "eu");
		Scope uk = new Scope(d2, "uk");
		Scope lo = new Scope(d2, "lo");
		// scope graph (aa)
		ScopeGraph g2 = new ScopeGraph(d2);
		g2.addEdge(lo, uk);
		g2.addEdge(uk, eu);
		g2.addEdge(fr, eu);
		g2.addEdge(bo, fr);
		g2.addEdge(ch, bo);
		// update phi pub
		SetOfScopePathsWithStatus ssp1aa = new SetOfScopePathsWithStatus();
		ScopePathWithStatus sp1aa = new ScopePathWithStatus(d2,
				ScopePathStatus.UP);
		sp1aa.addScope(Arrays.asList(ch));
		ScopePathWithStatus sp2aa = new ScopePathWithStatus(d2,
				ScopePathStatus.UP);
		sp2aa.addScope(Arrays.asList(ch, bo));
		ssp1aa.addScopePathWithStatus(Arrays.asList(sp1aa, sp2aa));
		phiPUB.addSetOfScopePathsWithStatus(ssp1aa);
		// phi sub
		MultiScopingSpecification phiSUB = new MultiScopingSpecification();
		SetOfScopePathsWithStatus ssp2 = new SetOfScopePathsWithStatus();
		ScopePathWithStatus sp4 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp4.addScope(is);
		ScopePathWithStatus sp5 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp5.addScope(Arrays.asList(is, ir));
		ScopePathWithStatus sp6 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp6.addScope(Arrays.asList(is, fs));
		ScopePathWithStatus sp7 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp7.addScope(Arrays.asList(is, fs, es));
		ScopePathWithStatus sp8 = new ScopePathWithStatus(d1,
				ScopePathStatus.UP);
		sp8.addScope(Arrays.asList(is, fs, es, us));
		ssp2.addScopePathWithStatus(Arrays.asList(sp4, sp5, sp6, sp7, sp8));
		phiSUB.addSetOfScopePathsWithStatus(ssp2);
		// update phi sub
		SetOfScopePathsWithStatus ssp2aa = new SetOfScopePathsWithStatus();
		ScopePathWithStatus sp9 = new ScopePathWithStatus(d2,
				ScopePathStatus.UP);
		sp9.addScope(Arrays.asList(lo));
		ScopePathWithStatus sp10 = new ScopePathWithStatus(d2,
				ScopePathStatus.UP);
		sp10.addScope(Arrays.asList(lo, uk));
		ScopePathWithStatus sp11 = new ScopePathWithStatus(d2,
				ScopePathStatus.UP);
		sp11.addScope(Arrays.asList(lo, uk, eu));
		ScopePathWithStatus sp12 = new ScopePathWithStatus(d2,
				ScopePathStatus.UP);
		sp12.addScope(Arrays.asList(lo, uk, eu, fr));
		ssp2aa.addScopePathWithStatus(Arrays.asList(sp9, sp10, sp11, sp12));
		phiSUB.addSetOfScopePathsWithStatus(ssp2aa);
		Assert.assertEquals(
				"[{((ch), UP), ((ch, bo), UP)}{((ls), UP), ((ls, us), UP), "
						+ "((ls, us, es), UP)}]", phiPUB.toString());
		Assert.assertEquals(
				"[{((lo), UP), ((lo, uk), UP), ((lo, uk, eu), UP), "
						+ "((lo, uk, eu, fr), DOWN)}{((is), UP), ((is, ir), UP), "
						+ "((is, fs), UP), ((is, fs, es), UP), ((is, fs, es, us), DOWN)}]",
				phiSUB.toString());
		Assert.assertEquals("(us, es)", Util
				.computeCommonSuffixPrefix(sp3, sp8).toString());
		Assert.assertEquals(false, Util.checkvp(phiSUB, phiPUB, null));
		Assert.assertEquals(false, Util.checkvp(phiPUB, phiSUB, null));
	}
}
