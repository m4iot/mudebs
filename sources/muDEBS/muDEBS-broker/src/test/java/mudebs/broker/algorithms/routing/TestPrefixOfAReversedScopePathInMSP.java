/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2017 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Léon Lim
Contributor(s): 
 */
package mudebs.broker.algorithms.routing;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;

import mudebs.common.MuDEBSException;
import mudebs.common.algorithms.routing.MultiScopingSpecification;
import mudebs.common.algorithms.routing.Scope;
import mudebs.common.algorithms.routing.ScopeGraph;
import mudebs.common.algorithms.routing.ScopePathStatus;
import mudebs.common.algorithms.routing.ScopePathWithStatus;
import mudebs.common.algorithms.routing.SetOfScopePathsWithStatus;

import org.junit.Assert;
import org.junit.Test;

/**
 * Tests for the method <tt>isPrefixOfAReversedScopePathInMSP</tt>.
 * 
 * @author Léon Lim
 * @author denis Denis Conan
 *
 */
public class TestPrefixOfAReversedScopePathInMSP {

	@Test
	public void testEmptyPath() throws URISyntaxException, MuDEBSException {
		URI d = new URI("membership");
		Scope is = new Scope(d, "is");
		MultiScopingSpecification phi = new MultiScopingSpecification();
		SetOfScopePathsWithStatus ssp = new SetOfScopePathsWithStatus();
		ScopePathWithStatus sp = new ScopePathWithStatus(d, ScopePathStatus.UP);
		sp.addScope(is);
		ssp.addScopePathWithStatus(Arrays.asList(sp));
		phi.addSetOfScopePathsWithStatus(ssp);
		// the prefix
		ScopePathWithStatus scopePath = new ScopePathWithStatus(d,
				ScopePathStatus.UP);
		Assert.assertEquals(false,
				Util.isPrefixOfAReversedScopePathInMSP(scopePath, phi));
	}

	@Test
	public void testNotSameDimension() throws URISyntaxException,
			MuDEBSException {
		URI d = new URI("membership");
		URI d2 = new URI("administrationarea");
		Scope is = new Scope(d, "is");
		MultiScopingSpecification phi = new MultiScopingSpecification();
		SetOfScopePathsWithStatus ssp = new SetOfScopePathsWithStatus();
		ScopePathWithStatus sp = new ScopePathWithStatus(d, ScopePathStatus.UP);
		sp.addScope(is);
		ssp.addScopePathWithStatus(Arrays.asList(sp));
		phi.addSetOfScopePathsWithStatus(ssp);
		// the prefix
		ScopePathWithStatus scopePath = new ScopePathWithStatus(d2,
				ScopePathStatus.UP);
		Assert.assertEquals(false,
				Util.isPrefixOfAReversedScopePathInMSP(scopePath, phi));
	}

	@Test
	public void testPathWithOneScope() throws URISyntaxException,
			MuDEBSException {
		URI d = new URI("membership");
		Scope is = new Scope(d, "is");
		MultiScopingSpecification phi = new MultiScopingSpecification();
		SetOfScopePathsWithStatus ssp = new SetOfScopePathsWithStatus();
		ScopePathWithStatus sp = new ScopePathWithStatus(d, ScopePathStatus.UP);
		sp.addScope(is);
		ssp.addScopePathWithStatus(Arrays.asList(sp));
		phi.addSetOfScopePathsWithStatus(ssp);
		// the prefix
		ScopePathWithStatus scopePath = new ScopePathWithStatus(d,
				ScopePathStatus.UP);
		scopePath.addScope(is);
		Assert.assertEquals(true,
				Util.isPrefixOfAReversedScopePathInMSP(scopePath, phi));
	}

	@Test
	public void testPathWithSeveralScopes() throws URISyntaxException,
			MuDEBSException {
		URI d = new URI("membership");
		ScopeGraph g = new ScopeGraph(d);
		Scope es = new Scope(d, "es");
		Scope ls = new Scope(d, "ls");
		Scope ir = new Scope(d, "ir");
		Scope fs = new Scope(d, "fs");
		Scope us = new Scope(d, "us");
		Scope is = new Scope(d, "is");
		g.addEdge(ls, us);
		g.addEdge(us, es);
		g.addEdge(fs, es);
		g.addEdge(is, fs);
		g.addEdge(is, ir);
		MultiScopingSpecification phi = new MultiScopingSpecification();
		SetOfScopePathsWithStatus ssp = new SetOfScopePathsWithStatus();
		ScopePathWithStatus sp = new ScopePathWithStatus(d, ScopePathStatus.UP);
		sp.addScope(Arrays.asList(ls, us, es, fs, is));
		ssp.addScopePathWithStatus(Arrays.asList(sp));
		phi.addSetOfScopePathsWithStatus(ssp);
		// the prefix
		ScopePathWithStatus scopePath = new ScopePathWithStatus(d,
				ScopePathStatus.UP);
		scopePath.addScope(Arrays.asList(is, fs));
		Assert.assertEquals(true,
				Util.isPrefixOfAReversedScopePathInMSP(scopePath, phi));
	}

	@Test
	public void testEqualsPaths() throws URISyntaxException, MuDEBSException {
		URI d = new URI("membership");
		ScopeGraph g = new ScopeGraph(d);
		Scope es = new Scope(d, "es");
		Scope ls = new Scope(d, "ls");
		Scope ir = new Scope(d, "ir");
		Scope fs = new Scope(d, "fs");
		Scope us = new Scope(d, "us");
		Scope is = new Scope(d, "is");
		g.addEdge(ls, us);
		g.addEdge(us, es);
		g.addEdge(fs, es);
		g.addEdge(is, fs);
		g.addEdge(is, ir);
		MultiScopingSpecification phi = new MultiScopingSpecification();
		SetOfScopePathsWithStatus ssp = new SetOfScopePathsWithStatus();
		ScopePathWithStatus sp = new ScopePathWithStatus(d, ScopePathStatus.UP);
		sp.addScope(Arrays.asList(ls, us, es, fs, is));
		ssp.addScopePathWithStatus(Arrays.asList(sp));
		phi.addSetOfScopePathsWithStatus(ssp);
		// the prefix
		ScopePathWithStatus scopePath = new ScopePathWithStatus(d,
				ScopePathStatus.UP);
		scopePath.addScope(Arrays.asList(is, fs, es));
		Assert.assertEquals(true,
				Util.isPrefixOfAReversedScopePathInMSP(scopePath, phi));
	}

	@Test
	public void testPhiWithSeveralDimensions() throws URISyntaxException,
			MuDEBSException {
		URI d = new URI("membership");
		URI d2 = new URI("administrationarea");
		ScopeGraph g = new ScopeGraph(d);
		Scope es = new Scope(d, "es");
		Scope ls = new Scope(d, "ls");
		Scope ir = new Scope(d, "ir");
		Scope fs = new Scope(d, "fs");
		Scope us = new Scope(d, "us");
		Scope is = new Scope(d, "is");
		g.addEdge(ls, us);
		g.addEdge(us, es);
		g.addEdge(fs, es);
		g.addEdge(is, fs);
		g.addEdge(is, ir);
		MultiScopingSpecification phi = new MultiScopingSpecification();
		SetOfScopePathsWithStatus ssp = new SetOfScopePathsWithStatus();
		ScopePathWithStatus sp = new ScopePathWithStatus(d, ScopePathStatus.UP);
		sp.addScope(Arrays.asList(ls, us, es, fs, is));
		ssp.addScopePathWithStatus(Arrays.asList(sp));
		ScopeGraph g2 = new ScopeGraph(d2);
		Scope lo = new Scope(d2, "lo");
		Scope uk = new Scope(d2, "uk");
		g2.addEdge(lo, uk);
		SetOfScopePathsWithStatus ssp2 = new SetOfScopePathsWithStatus();
		ScopePathWithStatus sp2 = new ScopePathWithStatus(d2,
				ScopePathStatus.UP);
		sp2.addScope(Arrays.asList(lo, uk));
		ssp2.addScopePathWithStatus(sp2);
		phi.addSetOfScopePathsWithStatus(ssp);
		phi.addSetOfScopePathsWithStatus(ssp2);
		// the prefix
		ScopePathWithStatus scopePath = new ScopePathWithStatus(d,
				ScopePathStatus.UP);
		scopePath.addScope(Arrays.asList(is, fs, es, us));
		Assert.assertEquals(true,
				Util.isPrefixOfAReversedScopePathInMSP(scopePath, phi));
	}
}
