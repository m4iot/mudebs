/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2017 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Léon Lim
 */
package mudebs.broker.algorithms.overlaymanagement;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import mudebs.common.algorithms.overlaymanagement.ColorTerminationDetection;

/**
 * This class contains the attributes of the state of the broker for a
 * termination detection.
 * 
 * @author Denis Conan
 * 
 */
public class TerminationDetectionState {
	/**
	 * the father in this termination detection.
	 */
	public URI father;
	/**
	 * the number of messages in transit.
	 */
	public int nbMsgsInTransit = 0;
	/**
	 * the set of the broker in this wave.
	 */
	public ColorTerminationDetection color;
	/**
	 * the set of statistic values. The object is the URI of the broker.
	 */
	public HashMap<URI, HashMap<String, Number>> statistics;
	/**
	 * the set of brokers from which the token has already been received.
	 */
	public List<URI> alreadyReceived;
	/**
	 * the token has already been sent to the father in the wave.
	 */
	public boolean waveAlreadyterminated = false;

	/**
	 * constructor.
	 * 
	 * @param father
	 *            the URI of the father of the termination detection algorithm.
	 */
	public TerminationDetectionState(final URI father) {
		this.father = father;
		color = ColorTerminationDetection.White;
		statistics = new HashMap<URI, HashMap<String, Number>>();
		alreadyReceived = new ArrayList<URI>();
	}
}
