/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2017 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Léon Lim
 */
package mudebs.broker.algorithms.routing;

import javax.xml.parsers.ParserConfigurationException;

import mudebs.broker.BrokerState;
import mudebs.common.Log;
import mudebs.common.algorithms.OperationalMode;
import mudebs.common.algorithms.routing.ABACInformation;
import mudebs.common.filters.PrivacyFilter;

import org.apache.log4j.Level;
import org.w3c.dom.Document;

/**
 * This interface defines a muDEBS XACML privacy filter, that is to say a
 * privacy filter containing the ABAC information for a request to the PEP.
 * 
 * @author Denis Conan
 * @author Léon Lim
 */
public class XACMLPrivacyFilter implements PrivacyFilter {
	/**
	 * the reference to the local PEP of the broker.
	 */
	private XACMLPEP xacmlPEP;
	/**
	 * the ABAC information to give to the PEP.
	 */
	private ABACInformation abacInfo;
	/**
	 * the boolean stating whether the corresponding subscription is local or
	 * global.
	 */
	private OperationalMode opMode;

	/**
	 * constructs an XACML privacy filter.
	 * 
	 * @param pep
	 *            the XACML PEP.
	 * @param info
	 *            the ABAC information.
	 * @param mode
	 *            the operational mode (global or local).
	 */
	public XACMLPrivacyFilter(final XACMLPEP pep, final ABACInformation info,
			final OperationalMode mode) {
		xacmlPEP = pep;
		abacInfo = info;
		opMode = mode;
	}

	/**
	 * gets the ABAC information.
	 * 
	 * @return the ABAC information.
	 */
	public ABACInformation getABACInformation() {
		return abacInfo;
	}

	/**
	 * evaluates whether the publication associated with a subscription matches
	 * the privacy filter. The matching is done with an XACML request to the
	 * local PEP using the ABAC information of the subscription. The matching is
	 * done once for a publication:
	 * <ul>
	 * <li>If the advertisement is local and the subscription is global then it
	 * is done at the broker of the producer (the attribute <tt>path</tt> of the
	 * publication message is of size 1).</li>
	 * <li>If the advertisement is global and the subscription is local then it
	 * is done at the broker of the consumer (the attribute
	 * <tt>localSubscription</tt> is equal to <tt>true</tt>).</li>
	 * </ul>
	 * 
	 * @param publication
	 *            the publication to evaluate. This argument is not used in this
	 *            filter.
	 * @return the boolean stating the matching.
	 */
	@Override
	public boolean evaluate(Document publication) {
		try {
			XACMLBasicRequest request = new XACMLBasicRequest(abacInfo);
			// the pep sends the request to the PDP
			String result = null;
			String requestString = request.getDocInString();
			try {
				result = xacmlPEP.evaluate(requestString);
			} catch (Exception e) {
				if (Log.ON && Log.ROUTING.isEnabledFor(Level.ERROR)) {
					Log.ROUTING
							.error(BrokerState.getInstance().identity.getPath()
									+ ", " + e.getMessage() + "\n"
									+ Log.printStackTrace(e.getStackTrace()));
				}
				return false;
			}
			// the PEP prints the XACML decision
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.DEBUG)) {
				Log.ROUTING.debug("PDP result is: " + result);
			}
			// There are 4 possible decisions
			// - permit
			// - deny
			// - NotApplicable = no matching authZ rules (This case is
			// not possible in this example because there is a default
			// policy that matches always
			// - Indeterminate = system error or there is a required
			// attribute in the policy that is missing in the context
			// request
			if (result.contains("Permit")) {
				if (Log.ON && Log.ROUTING.isEnabledFor(Level.DEBUG)) {
					Log.ROUTING.debug("PDP, permission to access context data");
				}
			} else if (result.contains("Deny")) {
				if (Log.ON && Log.ROUTING.isEnabledFor(Level.DEBUG)) {
					Log.ROUTING.debug(
							"PDP, not authorised to access" + " context data");
				}
				return false;
			} else if (result.contains("NotApplicable")) {
				if (Log.ON && Log.ROUTING.isEnabledFor(Level.DEBUG)) {
					Log.ROUTING.debug("PDP, no rule in the policy,"
							+ " no decision can be taken");
				}
				return false;
			} else {
				if (Log.ON && Log.ROUTING.isEnabledFor(Level.DEBUG)) {
					Log.ROUTING.debug("PDP , ???");
				}
				return false;
			}
		} catch (ParserConfigurationException e) {
			if (Log.ON && Log.DISPATCH.isEnabledFor(Level.FATAL)) {
				Log.DISPATCH.fatal("PDP, no PEP decision" + e.getMessage()
						+ "\n" + Log.printStackTrace(e.getStackTrace()));
			}
			return false;
		}
		return true;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "XACMLPrivacyFilter [xacmlPEP=" + xacmlPEP + ", abacInfo="
				+ abacInfo + ", opMode=" + opMode + "]";
	}
}
