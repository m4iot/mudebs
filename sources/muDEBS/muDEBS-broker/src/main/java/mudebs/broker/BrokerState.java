/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2017 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Léon Lim
 */
package mudebs.broker;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.nio.channels.SelectionKey;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Semaphore;

import javax.script.ScriptEngineManager;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import mudebs.broker.algorithms.overlaymanagement.TerminationDetectionState;
import mudebs.broker.algorithms.routing.AdvertisementTableEntry;
import mudebs.broker.algorithms.routing.RoutingTableEntry;
import mudebs.broker.algorithms.routing.XACMLPEP;
import mudebs.common.Log;
import mudebs.common.algorithms.EntityType;
import mudebs.common.algorithms.Reply;
import mudebs.common.algorithms.routing.Scope;
import mudebs.common.algorithms.routing.ScopeGraph;
import mudebs.common.filters.Filter;

import org.apache.log4j.Level;

/**
 * This class contains the attributes of the state of the broker.
 * 
 * @author Denis Conan
 * 
 */
public class BrokerState {
	/**
	 * the singleton instance of this class.
	 */
	private static BrokerState instance = new BrokerState();
	/**
	 * the factory for the document builder.
	 */
	public DocumentBuilderFactory factory = null;
	/**
	 * the document builder.
	 */
	public DocumentBuilder builder = null;
	/**
	 * boolean stating whether the termination is required.
	 */
	public boolean terminationRequired = false;
	/**
	 * the set of active termination detection waves. The main key is the URI of
	 * the initiator and the second key is the number of the wave.
	 */
	public HashMap<URI, HashMap<Integer, TerminationDetectionState>> termWaves;
	/**
	 * the number of the last wave initiated by this broker for termination
	 * detection.
	 */
	public int lastWaveNumber = 0;
	/**
	 * backward reference to the broker selector object in order to use its
	 * methods to send messages.
	 */
	public Broker broker;
	/**
	 * the number of messages sent minus the number of messages received as
	 * considered in termination detection.
	 */
	public int nbMsgsInTransit = 0;
	/**
	 * identity of this broker.
	 */
	public URI identity;
	/**
	 * script engine for JavaScript methods of filters.
	 */
	public ScriptEngineManager scriptEngineManager;
	/**
	 * selection key of the connection from which the last message was received.
	 */
	public SelectionKey currKey;
	/**
	 * entity type of the entity connected through <tt>currKey</tt>.
	 */
	public EntityType currIdentityType;
	/**
	 * number of identity messages received.
	 */
	public int nbIdentitiesReceived = 0;
	/**
	 * number of identity messages sent.
	 */
	public int nbIdentitiesSent = 0;
	/**
	 * number of advertisement messages received.
	 */
	public int nbAdvertisementsReceived = 0;
	/**
	 * number of advertisement messages sent.
	 */
	public int nbAdvertisementsSent = 0;
	/**
	 * number of publication messages received.
	 */
	public int nbPublicationsReceived = 0;
	/**
	 * number of publication messages sent.
	 */
	public int nbPublicationsSent = 0;
	/**
	 * number of request messages received.
	 */
	public int nbRequestsReceived = 0;
	/**
	 * number of replies messages received.
	 */
	public int nbRepliesReceived = 0;
	/**
	 * number of subscription messages received from clients.
	 */
	public int nbSubscriptionsReceived = 0;
	/**
	 * number of subscription messages received from brokers.
	 */
	public int nbAdminSubscriptionsReceived = 0;
	/**
	 * number of subscription messages sent.
	 */
	public int nbAdminSubscriptionsSent = 0;
	/**
	 * number of publication messages sent.
	 */
	public int nbAdminPublicationsSent = 0;
	/**
	 * number of publication messages received from brokers.
	 */
	public int nbAdminPublicationsReceived = 0;
	/**
	 * number of unadvertisement messages received.
	 */
	public int nbUnadvertisementsReceived = 0;
	/**
	 * number of unsubscription messages received from brokers.
	 */
	public int nbAdminUnsubscriptionsReceived = 0;
	/**
	 * number of unadvertisement messages sent.
	 */
	public int nbUnadvertisementsSent = 0;
	/**
	 * number of unsubscription messages received.
	 */
	public int nbUnsubscriptionsReceived = 0;
	/**
	 * number of unsubscription messages sent.
	 */
	public int nbUnsubscriptionsSent = 0;
	/**
	 * number of join scope messages received from scripting brokers.
	 */
	public int nbJoinScopesReceived = 0;
	/**
	 * number of delivered notifications. The same client may be taken into
	 * account several times since it may receive notifications several times.
	 */
	public int nbPublicationsDelivered = 0;
	/**
	 * size of routing table for brokers.
	 */
	public int brokersRoutingTableSize = 0;
	/**
	 * size of routing table for clients.
	 */
	public int clientsRoutingTableSize = 0;
	/**
	 * number of brokers involved in routing notifications.
	 */
	public double nbBrokersInvolvedInRouting = 0;
	/**
	 * average number of involved brokers in the diffusion of a notification
	 */
	public double averageNbInvolvedBrokers = 0.;
	/**
	 * average execution time.
	 */
	public double averageExecutiontime = 0;
	/**
	 * number of matched publications
	 */
	public int nbMatchedPublications = 0;
	/**
	 * list of execution time.
	 */
	public List<Double> listOfExecutionTime = new ArrayList<Double>();
	/**
	 * identities of neighbouring brokers.
	 */
	public HashMap<URI, SelectionKey> brokerIdentities;
	/**
	 * identities of local clients.
	 */
	public HashMap<URI, SelectionKey> clientIdentities;
	/**
	 * set of blocking calls that are awaiting acknowledgements from
	 * neighbouring brokers. The key of the enclosing map is the URI of the
	 * client, which originally performs the call. The key of the included map
	 * is the sequence number of the acknowledgement. The included list is the
	 * set of neighbouring brokers, from which this broker is waiting for an
	 * acknowledgement.
	 */
	public HashMap<URI, HashMap<Integer, List<URI>>> acksToReceive;
	/**
	 * set of brokers waiting for an acknowledgement from this broker. The key
	 * of the enclosing map is the URI of the (local or remote) client, which
	 * originally performs the call. The key of the included map is the sequence
	 * number of the acknowledgement. The URI contained in the included map is
	 * the URI of the neighbouring broker waiting for acknowledgements from this
	 * broker.
	 */
	public HashMap<URI, HashMap<Integer, List<URI>>> acksToSendToBrokers;
	/**
	 * set of brokers waiting for an acknowledgement from this broker. The key
	 * of the enclosing map is the URI of the local client, which performs the
	 * call. The URI is the URI of the client waiting for acknowledgements from
	 * this broker.
	 */
	public HashMap<URI, List<Integer>> acksToSendToClients;
	/**
	 * set of request replies. The key of the enclosing map is the URI of the
	 * (local or remote) client, which originally performs the request call. The
	 * key of the included map is the sequence number of the request (like the
	 * sequence number of an acknowledgement). The list is the command results,
	 * that is the replies.
	 */
	public HashMap<URI, HashMap<Integer, List<Reply>>> requestReplies;
	/**
	 * advertisement table for local clients. The key is the URI of the local
	 * client. The string is the identifier of the acknowledgement.
	 */
	public HashMap<URI, HashMap<String, AdvertisementTableEntry>> localAdvertisements;
	/**
	 * advertisement table for remote clients. The URI key of the enclosing map
	 * is the URI of the broker that has forwarded the admin advertisement
	 * message. The URI key of the included map is the URI of the client
	 * advertising. The string is the identifier of the acknowledgement.
	 */
	public HashMap<URI, HashMap<URI, HashMap<String, AdvertisementTableEntry>>> remoteAdvertisements;
	/**
	 * routing table for local clients.
	 */
	public HashMap<URI, List<RoutingTableEntry>> clientRoutingTables;
	/**
	 * routing table for brokers.
	 */
	public HashMap<URI, List<RoutingTableEntry>> brokerRoutingTables;
	/**
	 * scope lookup table, which is modified by calls to <tt>joinScope</tt> and
	 * <tt>leaveScope</tt>. It indicates in which directions (in terms of
	 * neighbouring brokers) clients belonging to the scope can be found. The
	 * key of the map is the scope, and the content of the map corresponds to
	 * the list of neighbouring brokers that know the scope.
	 */
	public HashMap<Scope, List<URI>> scopeLookupTables;
	/**
	 * set of scope graphs, one scope graph per dimension.
	 */
	public HashMap<URI, ScopeGraph> scopeGraphs;
	/**
	 * XACML PEP of the broker.
	 * 
	 * NB: this not a pure and elegant XACML architecture. When PEP and PDP are
	 * on different devices, it is not possible to initialise the PDP via the
	 * PEP.
	 */
	public XACMLPEP xacmlPEP;
	/**
	 * semaphore to protect concurrent access of broker's state
	 */
	public Semaphore semaphore = new Semaphore(1);
	/**
	 * set of JavaScript filters for advertisement filters used during
	 * performance evaluation.
	 */
	public Map<String, Filter> advertisementFiltersForPerf;
	/**
	 * set of JavaScript filters for subscription filters used during
	 * performance evaluation.
	 */
	public Map<String, Filter> subscriptionFiltersForPerf;
	/**
	 * set of JavaScript filters for visibility filters used during performance
	 * evaluation.
	 */
	public Map<String, Filter> visibilityFiltersForPerf;
	/**
	 * the JavaScript filter that returns <tt>true</tt>.
	 */
	public String returnTrueFilter = null;
	/**
	 * the JavaScript filter that returns <tt>false</tt>.
	 */
	public String returnFalseFilter = null;

	/**
	 * private constructor of the singleton pattern.
	 */
	private BrokerState() {
		factory = DocumentBuilderFactory.newInstance();
		try {
			builder = factory.newDocumentBuilder();
		} catch (Exception e) {
			if (Log.ON && Log.CONFIG.isEnabledFor(Level.ERROR)) {
				Log.CONFIG.error("error when handling publication:" + ", "
						+ e.getMessage() + "\n"
						+ Log.printStackTrace(e.getStackTrace()));
			}
		}
		scriptEngineManager = new ScriptEngineManager();
		termWaves = new HashMap<URI, HashMap<Integer, TerminationDetectionState>>();
		brokerIdentities = new HashMap<URI, SelectionKey>();
		clientIdentities = new HashMap<URI, SelectionKey>();
		acksToReceive = new HashMap<URI, HashMap<Integer, List<URI>>>();
		acksToSendToBrokers = new HashMap<URI, HashMap<Integer, List<URI>>>();
		acksToSendToClients = new HashMap<URI, List<Integer>>();
		requestReplies = new HashMap<URI, HashMap<Integer, List<Reply>>>();
		localAdvertisements = new HashMap<URI, HashMap<String, AdvertisementTableEntry>>();
		remoteAdvertisements = new HashMap<URI, HashMap<URI, HashMap<String, AdvertisementTableEntry>>>();
		clientRoutingTables = new HashMap<URI, List<RoutingTableEntry>>();
		brokerRoutingTables = new HashMap<URI, List<RoutingTableEntry>>();
		scopeLookupTables = new HashMap<Scope, List<URI>>();
		xacmlPEP = new XACMLPEP();
		scopeGraphs = new HashMap<URI, ScopeGraph>();
		advertisementFiltersForPerf = new HashMap<String, Filter>();
		subscriptionFiltersForPerf = new HashMap<String, Filter>();
		visibilityFiltersForPerf = new HashMap<String, Filter>();
		String line = null;
		returnTrueFilter = "";
		returnFalseFilter = "";
		try {
			InputStream stream = Thread.currentThread().getContextClassLoader()
					.getResourceAsStream(
							"muDEBS/config/filter-return-false.mudebs");
			if (stream != null) {
				BufferedReader br = new BufferedReader(
						new InputStreamReader(stream));
				while ((line = br.readLine()) != null) {
					returnFalseFilter += line;
				}
				stream.close();
			}
		} catch (FileNotFoundException ex) {
			if (Log.ON && Log.CONFIG.isEnabledFor(Level.WARN)) {
				Log.CONFIG.warn(identity.getPath()
						+ ", unable to open file 'filter-return-false.mudebs'");
			}
		} catch (IOException ex) {
			if (Log.ON && Log.CONFIG.isEnabledFor(Level.WARN)) {
				Log.CONFIG.warn(identity.getPath()
						+ ", error reading file 'filter-return-false.mudebs'");
			}
		}
		try {
			InputStream stream = Thread.currentThread().getContextClassLoader()
					.getResourceAsStream(
							"muDEBS/config/filter-return-true.mudebs");
			if (stream != null) {
				BufferedReader br = new BufferedReader(
						new InputStreamReader(stream));
				while ((line = br.readLine()) != null) {
					returnTrueFilter += line;
				}
				stream.close();
			}
		} catch (FileNotFoundException ex) {
			if (Log.ON && Log.CONFIG.isEnabledFor(Level.WARN)) {
				Log.CONFIG.warn(identity.getPath()
						+ ", unable to open file 'filter-return-true.mudebs'");
			}
		} catch (IOException ex) {
			if (Log.ON && Log.CONFIG.isEnabledFor(Level.WARN)) {
				Log.CONFIG.warn(identity.getPath()
						+ ", error reading file 'filter-return-true.mudebs'");
			}
		}
	}

	/**
	 * gets the singleton instance.
	 * 
	 * @return the singleton instance of the state.
	 */
	public static BrokerState getInstance() {
		return instance;
	}

	/**
	 * creates an entry in the structure <tt>acksToReceive</tt>.
	 * 
	 * @param client
	 *            the URI of the client.
	 * @param seqNumber
	 *            the sequence number of the acknowledgement.
	 */
	public void createEntryAcksToReceive(final URI client,
			final int seqNumber) {
		HashMap<Integer, List<URI>> acksToR = acksToReceive.get(client);
		if (acksToR == null) {
			acksToR = new HashMap<Integer, List<URI>>();
			acksToReceive.put(client, acksToR);
		}
		if (!acksToR.containsKey(seqNumber)) {
			acksToR.put(seqNumber, new ArrayList<URI>());
		}
	}

	/**
	 * creates an entry in the structure <tt>acksToSendToClients</tt>
	 * 
	 * @param client
	 *            the URI of the client.
	 * @param seqNumber
	 *            the sequence number of the acknowledgement.
	 */
	public void createEntryAcksToSendToClients(final URI client,
			final int seqNumber) {
		List<Integer> acksToS = acksToSendToClients.get(client);
		if (acksToS == null) {
			acksToS = new ArrayList<Integer>();
			acksToSendToClients.put(client, acksToS);
		}
		acksToS.add(seqNumber);
	}

	/**
	 * creates an entry in the structure <tt>acksToSendToBrokers</tt>.
	 * 
	 * @param client
	 *            the URI of the client.
	 * @param seqNumber
	 *            the sequence number of the acknowledgement.
	 * @param broker
	 *            the URI of the broker.
	 */
	public void createEntryAcksToSendToBrokers(final URI client,
			final int seqNumber, final URI broker) {
		HashMap<Integer, List<URI>> acksToS = acksToSendToBrokers.get(client);
		if (acksToS == null) {
			acksToS = new HashMap<Integer, List<URI>>();
			acksToSendToBrokers.put(client, acksToS);
			acksToS.put(seqNumber, new ArrayList<URI>());
		}
		if (!acksToS.containsKey(seqNumber)) {
			acksToS.put(seqNumber, new ArrayList<URI>());
		}
		acksToS.get(seqNumber).add(broker);
	}

	/**
	 * creates an entry in the structure <tt>requestReplies</tt>.
	 * 
	 * @param client
	 *            the URI of the client.
	 * @param seqNumber
	 *            the sequence number of the reply.
	 */
	public void createEntryRequestReplies(final URI client,
			final int seqNumber) {
		HashMap<Integer, List<Reply>> replies = requestReplies.get(client);
		if (replies == null) {
			replies = new HashMap<Integer, List<Reply>>();
			requestReplies.put(client, replies);
			replies.put(seqNumber, new ArrayList<Reply>());
		}
		if (!replies.containsKey(seqNumber)) {
			replies.put(seqNumber, new ArrayList<Reply>());
		}
	}

	public void reinitialise() {
		factory = DocumentBuilderFactory.newInstance();
		try {
			builder = factory.newDocumentBuilder();
		} catch (Exception e) {
			if (Log.ON && Log.CONFIG.isEnabledFor(Level.ERROR)) {
				Log.CONFIG.error("error when handling publication:" + ", "
						+ e.getMessage() + "\n"
						+ Log.printStackTrace(e.getStackTrace()));
			}
		}
		scriptEngineManager = new ScriptEngineManager();
		termWaves = new HashMap<URI, HashMap<Integer, TerminationDetectionState>>();
		brokerIdentities = new HashMap<URI, SelectionKey>();
		clientIdentities = new HashMap<URI, SelectionKey>();
		acksToReceive = new HashMap<URI, HashMap<Integer, List<URI>>>();
		acksToSendToBrokers = new HashMap<URI, HashMap<Integer, List<URI>>>();
		acksToSendToClients = new HashMap<URI, List<Integer>>();
		requestReplies = new HashMap<URI, HashMap<Integer, List<Reply>>>();
		localAdvertisements = new HashMap<URI, HashMap<String, AdvertisementTableEntry>>();
		remoteAdvertisements = new HashMap<URI, HashMap<URI, HashMap<String, AdvertisementTableEntry>>>();
		clientRoutingTables = new HashMap<URI, List<RoutingTableEntry>>();
		brokerRoutingTables = new HashMap<URI, List<RoutingTableEntry>>();
		scopeLookupTables = new HashMap<Scope, List<URI>>();
		xacmlPEP = new XACMLPEP();
		scopeGraphs = new HashMap<URI, ScopeGraph>();
		advertisementFiltersForPerf = new HashMap<String, Filter>();
		subscriptionFiltersForPerf = new HashMap<String, Filter>();
		visibilityFiltersForPerf = new HashMap<String, Filter>();
	}

	public String getSizeOfSets() {
		StringBuffer result = new StringBuffer();
		result.append("termWaves=");
		result.append(termWaves.size());
		for (HashMap<Integer, TerminationDetectionState> elem : termWaves
				.values()) {
			result.append(" ");
			result.append(elem.size());
		}
		result.append(", ");
		result.append("brokerIdentities=");
		result.append(brokerIdentities.size());
		result.append(", ");
		result.append("clientIdentities=");
		result.append(clientIdentities.size());
		result.append(", ");
		result.append("acksToReceive=");
		result.append(acksToReceive.size());
		result.append(", ");
		result.append("acksToSendToBrokers=");
		result.append(acksToSendToBrokers.size());
		for (HashMap<Integer, List<URI>> elem : acksToSendToBrokers.values()) {
			result.append(" ");
			result.append(elem.size());
			for (List<URI> elem2 : elem.values()) {
				result.append(" ");
				result.append(elem2.size());
			}
		}
		result.append(", ");
		result.append("acksToSendToClients=");
		result.append(acksToSendToClients.size());
		for (List<Integer> elem : acksToSendToClients.values()) {
			result.append(" ");
			result.append(elem.size());
		}
		result.append(", ");
		result.append("requestReplies=");
		result.append(requestReplies.size());
		for (HashMap<Integer, List<Reply>> elem : requestReplies.values()) {
			result.append(" ");
			result.append(elem.size());
			for (List<Reply> elem2 : elem.values()) {
				result.append(" ");
				result.append(elem2.size());
			}
		}
		result.append(", ");
		result.append("localAdvertisements=");
		result.append(localAdvertisements.size());
		for (HashMap<String, AdvertisementTableEntry> elem : localAdvertisements
				.values()) {
			result.append(" ");
			result.append(elem.size());
		}
		result.append(", ");
		result.append("remoteAdvertisements=");
		result.append(remoteAdvertisements.size());
		for (HashMap<URI, HashMap<String, AdvertisementTableEntry>> elem : remoteAdvertisements
				.values()) {
			result.append(" ");
			result.append(elem.size());
			for (HashMap<String, AdvertisementTableEntry> elem2 : elem
					.values()) {
				result.append(" ");
				result.append(elem2.size());
			}
		}
		result.append(", ");
		result.append("clientRoutingTables=");
		result.append(clientRoutingTables.size());
		for (List<RoutingTableEntry> elem : clientRoutingTables.values()) {
			result.append(" ");
			result.append(elem.size());
		}
		result.append(", ");
		result.append("brokerRoutingTables=");
		result.append(brokerRoutingTables.size());
		for (List<RoutingTableEntry> elem : brokerRoutingTables.values()) {
			result.append(" ");
			result.append(elem.size());
		}
		result.append(", ");
		result.append("scopeLookupTables=");
		result.append(scopeLookupTables.size());
		for (List<URI> elem : scopeLookupTables.values()) {
			result.append(" ");
			result.append(elem.size());
		}
		result.append(", ");
		result.append("advertisementFilters=");
		result.append(advertisementFiltersForPerf.size());
		result.append(", ");
		result.append("subscriptionFilters=");
		result.append(subscriptionFiltersForPerf.size());
		result.append(", ");
		result.append("visibilityFilters=");
		result.append(visibilityFiltersForPerf.size());
		return result.toString();
	}
}
