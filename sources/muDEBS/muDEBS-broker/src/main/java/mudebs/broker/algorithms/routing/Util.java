/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2017 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Léon Lim
 */
package mudebs.broker.algorithms.routing;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.nio.channels.SelectionKey;
import java.nio.charset.Charset;
import java.nio.file.FileSystemException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Properties;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import mudebs.broker.BrokerState;
import mudebs.common.Log;
import mudebs.common.MuDEBSException;
import mudebs.common.PerfAttributes;
import mudebs.common.PerfLevel;
import mudebs.common.algorithms.ACK;
import mudebs.common.algorithms.CommandResult;
import mudebs.common.algorithms.CommandStatus;
import mudebs.common.algorithms.routing.AcknowledgementMsgContent;
import mudebs.common.algorithms.routing.AdminAcknowledgementMsgContent;
import mudebs.common.algorithms.routing.AdminReplyMsgContent;
import mudebs.common.algorithms.routing.MultiScopingSpecification;
import mudebs.common.algorithms.routing.ReplyMsgContent;
import mudebs.common.algorithms.routing.Scope;
import mudebs.common.algorithms.routing.ScopeBottom;
import mudebs.common.algorithms.routing.ScopePath;
import mudebs.common.algorithms.routing.ScopePathStatus;
import mudebs.common.algorithms.routing.ScopePathWithStatus;
import mudebs.common.algorithms.routing.ScopeTop;
import mudebs.common.algorithms.routing.SetOfScopePathsWithStatus;
import mudebs.common.filters.Filter;

import org.apache.log4j.Level;
import org.w3c.dom.Document;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.SAXException;

/**
 * This class defines the utilitary methods for the routing algorithm.
 * 
 * @author Denis Conan
 * @author Léon Lim
 */
public class Util {
	/**
	 * creates a JavaScript engine executing the filter.
	 * 
	 * @param routingFilter
	 *            the JavaScript code.
	 * @return the filter.
	 * @throws ScriptException
	 *             the exception exception thrown when initiating the JavaScript
	 *             engine.
	 */
	static Filter createJavaScriptFilter(final String routingFilter,
			final Class<? extends Filter> clazz) throws ScriptException {
		BrokerState state = BrokerState.getInstance();
		ScriptEngine engine = state.scriptEngineManager
				.getEngineByName("JavaScript");
		engine.eval(routingFilter);
		Invocable inv = (Invocable) engine;
		return inv.getInterface(clazz);
	}

	/**
	 * creates a document by loading XML from an XML file.
	 * 
	 * @param xmlFilePath
	 *            the XML file path.
	 * @return the XML document.
	 * @throws ParserConfigurationException
	 *             the exception thrown in case of problem when creating the
	 *             document.
	 * @throws IOException
	 *             the exception thrown in case of problem when reading the
	 *             content of the file.
	 * @throws SAXException
	 *             the exception thrown in case of problem when parsing the
	 *             content of the file.
	 */
	public static Document loadXMLFromFile(String xmlFilePath)
			throws ParserConfigurationException, SAXException, IOException {
		File file = new File(xmlFilePath);
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document document = db.parse(file);
		return document;
	}

	/**
	 * creates a document from a string.
	 * 
	 * @param xmlString
	 *            the XML String.
	 * @return the XML document.
	 * @throws ParserConfigurationException
	 *             the exception thrown in case of problem when creating the
	 *             document.
	 * @throws IOException
	 *             the exception thrown in case of problem when reading the
	 *             string.
	 * @throws SAXException
	 *             the exception thrown in case of problem when parsing the
	 *             string.
	 */
	public static Document loadXMLFromString(String xmlString)
			throws ParserConfigurationException, SAXException, IOException {
		Document doc = null;
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		DocumentBuilder builder = null;
		builder = factory.newDocumentBuilder();
		doc = builder.parse(new ByteArrayInputStream(xmlString.getBytes()));
		return doc;
	}

	/**
	 * loads string content from a document.
	 * 
	 * @param doc
	 *            the document.
	 * @return the string content.
	 */
	public static String loadStringFromDoc(Document doc) {
		DOMImplementationLS domImplementation = (DOMImplementationLS) doc
				.getImplementation();
		LSSerializer lsSerializer = domImplementation.createLSSerializer();
		return lsSerializer.writeToString(doc);
	}

	/**
	 * loads file content into a string.
	 * 
	 * @param path
	 *            the file path.
	 * @param encoding
	 *            the encoding mode.
	 * @return the string content of the file.
	 * @throws IOException
	 *             exception thrown when problem occurs while loading the
	 *             content of the file.
	 */
	public static String readFile(String path, Charset encoding) throws IOException {
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, encoding);
	}

	static void sendNAK(final ACK ack, final URI forwarder, final URI client,
			final int seqNumber, final String errMsg, final Level logLevel) {
		if (Log.ON && Log.ROUTING.isEnabledFor(logLevel)) {
			Log.ROUTING.log(logLevel, errMsg);
		}
		if (ack != ACK.NO) {
			if (forwarder == null) {
				Util.sendAcknowledgementToClient(client, seqNumber,
						new CommandResult(CommandStatus.ERROR, errMsg));
			} else {
				BrokerState.getInstance().nbMsgsInTransit++;
				Util.sendAcknowledgementToBroker(forwarder, client, seqNumber,
						new CommandResult(CommandStatus.ERROR, errMsg));
			}
		}
	}

	/**
	 * sends an acknowledgement to a client.
	 * 
	 * @param client
	 *            the URI of the client
	 * @param seqNumber
	 *            the sequence number of the acknowledgement.
	 * @param result
	 *            the result of the call to acknowledge.
	 */
	static void sendAcknowledgementToClient(final URI client,
			final int seqNumber, final CommandResult result) {
		BrokerState state = BrokerState.getInstance();
		try {
			AcknowledgementMsgContent ackMsg = new AcknowledgementMsgContent(
					client, seqNumber, result);
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
				Log.ROUTING.trace(state.identity.getPath()
						+ ", sending acknowledgment: " + client
						+ ", with sequence number: " + seqNumber);
			}
			state.broker.sendToAClient(state.clientIdentities.get(client),
					AlgorithmRouting.ACKNOWLEDGEMENT.getActionIndex(), ackMsg);
		} catch (IOException e) {
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.ERROR)) {
				Log.ROUTING.error(state.identity.getPath() + ", "
						+ e.getMessage() + "\n"
						+ Log.printStackTrace(e.getStackTrace()));
			}
		}
	}

	/**
	 * sends an acknowledgement to a neighbouring broker.
	 * 
	 * @param broker
	 *            the URI of the broker, which forwarded the call.
	 * @param client
	 *            the URI of the client, which made the call.
	 * @param seqNumber
	 *            the sequence number of the acknowledgement.
	 * @param result
	 *            the result of the call to acknowledge.
	 */
	static void sendAcknowledgementToBroker(final URI broker, final URI client,
			final int seqNumber, final CommandResult result) {
		BrokerState state = BrokerState.getInstance();
		try {
			AdminAcknowledgementMsgContent ackMsg = new AdminAcknowledgementMsgContent(
					state.identity, client, seqNumber, result);
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
				Log.ROUTING.trace(state.identity.getPath()
						+ ", sending acknowledgment to broker: " + broker
						+ ", for client: " + client
						+ ", with sequence number: " + seqNumber);
			}
			state.broker.sendToABroker(state.brokerIdentities.get(broker),
					AlgorithmRouting.ADMIN_ACKNOWLEDGEMENT.getActionIndex(),
					ackMsg);
		} catch (IOException e) {
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.ERROR)) {
				Log.ROUTING.error(state.identity.getPath() + ", "
						+ e.getMessage() + "\n"
						+ Log.printStackTrace(e.getStackTrace()));
			}
		}
	}

	/**
	 * tests whether no more acknowledgements from neighbouring brokers are
	 * expected or whether the command status of the acknowledgement just
	 * received is <tt>ERROR</tt>. If so, sends the acknowledgement either to
	 * the neighbouring broker or to the client that are waiting for the
	 * acknowledgement of this broker.
	 * 
	 * @param client
	 *            the URI of the client, which made the call.
	 * @param seqNumber
	 *            the sequence number of the acknowledgement.
	 * @param result
	 *            the result of the acknowledgement just received.
	 */
	static void testAndSendAcknowledgement(final URI client,
			final int seqNumber, final CommandResult result) {
		BrokerState bstate = BrokerState.getInstance();
		HashMap<Integer, List<URI>> acksToR = bstate.acksToReceive.get(client);
		if (acksToR == null) {
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.DEBUG)) {
				Log.ROUTING.debug(bstate.identity
						+ ", unknown requester of this acknowledgement = "
						+ client + " " + seqNumber);
			}
			return;
		}
		List<URI> acksFromBrokers = acksToR.get(seqNumber);
		if (acksFromBrokers == null) {
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.DEBUG)) {
				Log.ROUTING.debug(bstate.identity
						+ ", unknown requester of this acknowledgement = "
						+ client + " " + seqNumber);
			}
			return;
		}
		if (acksToR.get(seqNumber).isEmpty()
				|| result.getStatus() == CommandStatus.ERROR) {
			acksToR.remove(seqNumber);
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
				Log.ROUTING.trace(bstate.identity.getPath()
						+ ", acks to receive = " + bstate.acksToReceive);
			}
			HashMap<Integer, List<URI>> acksForBroker = bstate.acksToSendToBrokers
					.get(client);
			if (acksForBroker != null) {
				List<URI> brokers = acksForBroker.get(seqNumber);
				if (brokers != null) {
					for (URI uri : brokers) {
						Util.sendAcknowledgementToBroker(uri, client,
								seqNumber, result);
					}
					acksForBroker.remove(seqNumber);
					if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
						Log.ROUTING.trace(bstate.identity
								+ ", acknowledgement to send to brokers = "
								+ bstate.acksToSendToBrokers);
					}
					return;
				}
			}
			List<Integer> acksForClient = bstate.acksToSendToClients
					.get(client);
			if (acksForClient != null) {
				Util.sendAcknowledgementToClient(client, seqNumber, result);
				bstate.acksToSendToClients.remove(client);
				return;
			}
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.ERROR)) {
				Log.ROUTING.error(bstate.identity
						+ ", unknown requester of this acknowledgement = "
						+ client + " " + seqNumber);
			}
		}
	}

	/**
	 * tests whether no more replies from neighbouring brokers are expected. If
	 * so, sends the replies either to the neighbouring broker or to the client
	 * that are waiting for the replies of this broker.
	 * 
	 * @param client
	 *            the URI of the client, which made the call.
	 * @param seqNumber
	 *            the sequence number of the acknowledgement.
	 */
	static void testAndSendReplies(final URI client, final int seqNumber) {
		BrokerState state = BrokerState.getInstance();
		HashMap<Integer, List<URI>> acksToR = state.acksToReceive.get(client);
		if (acksToR == null) {
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.DEBUG)) {
				Log.ROUTING.debug(state.identity
						+ ", unknown requester of this reply = " + client + " "
						+ seqNumber);
			}
			return;
		}
		List<URI> acksFromBrokers = acksToR.get(seqNumber);
		if (acksFromBrokers == null) {
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.DEBUG)) {
				Log.ROUTING.debug(state.identity
						+ ", unknown requester of this reply = " + client + " "
						+ seqNumber);
			}
			return;
		}
		if (acksToR.get(seqNumber).isEmpty()) {
			acksToR.remove(seqNumber);
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
				Log.ROUTING.trace(state.identity.getPath()
						+ ", replies to receive = " + state.acksToReceive);
			}
			HashMap<Integer, List<URI>> acksForBroker = state.acksToSendToBrokers
					.get(client);
			if (acksForBroker != null) {
				List<URI> brokers = acksForBroker.get(seqNumber);
				if (brokers != null) {
					for (URI uri : brokers) {
						sendRepliesToBroker(uri, client, seqNumber);
					}
					acksForBroker.remove(seqNumber);
					if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
						Log.ROUTING.trace(state.identity
								+ ", acknowledgement to send to brokers = "
								+ state.acksToSendToBrokers);
					}
					state.requestReplies.get(client).remove(seqNumber);
					if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
						Log.ROUTING.trace(state.identity
								+ ", requestReplies = " + state.requestReplies);
					}
					return;
				}
			}
			List<Integer> acksForClient = state.acksToSendToClients.get(client);
			if (acksForClient != null) {
				sendRepliesToClient(client, seqNumber);
				state.acksToSendToClients.remove(client);
				if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
					Log.ROUTING.trace(state.identity
							+ ", acknowledgement to send to clients = "
							+ state.acksToSendToClients);
				}
				state.requestReplies.get(client).remove(seqNumber);
				if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
					Log.ROUTING.trace(state.identity + ", requestReplies = "
							+ state.requestReplies);
				}
				return;
			}
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.ERROR)) {
				Log.ROUTING.error(state.identity
						+ ", unknown requester of this reply = " + client + " "
						+ seqNumber);
			}
		}
	}

	/**
	 * sends replies to a neighbouring broker.
	 * 
	 * @param broker
	 *            the URI of the broker, which forwarded the call.
	 * @param client
	 *            the URI of the client, which made the request.
	 * @param seqNumber
	 *            the sequence number of the acknowledgement.
	 */
	private static void sendRepliesToBroker(final URI broker, final URI client,
			final int seqNumber) {
		BrokerState state = BrokerState.getInstance();
		try {
			AdminReplyMsgContent replyMsg = new AdminReplyMsgContent(
					state.identity, client, seqNumber, state.requestReplies
							.get(client).get(seqNumber));
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
				Log.ROUTING.trace(state.identity.getPath()
						+ ", sending replies to broker: " + broker
						+ ", for client: " + client
						+ ", with sequence number: " + seqNumber);
			}
			state.broker.sendToABroker(state.brokerIdentities.get(broker),
					AlgorithmRouting.ADMIN_REPLY.getActionIndex(), replyMsg);
		} catch (IOException e) {
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.ERROR)) {
				Log.ROUTING.error(state.identity.getPath() + ", "
						+ e.getMessage() + "\n"
						+ Log.printStackTrace(e.getStackTrace()));
			}
		}
	}

	/**
	 * sends replies to a client.
	 * 
	 * @param client
	 *            the URI of the client
	 * @param seqNumber
	 *            the sequence number of the acknowledgement.
	 */
	static void sendRepliesToClient(final URI client, final int seqNumber) {
		BrokerState state = BrokerState.getInstance();
		try {
			ReplyMsgContent replyMsg = new ReplyMsgContent(client, seqNumber,
					state.requestReplies.get(client).get(seqNumber));
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
				Log.ROUTING.trace(state.identity.getPath()
						+ ", sending replies: " + client
						+ ", with sequence number: " + seqNumber);
			}
			state.broker.sendToAClient(state.clientIdentities.get(client),
					AlgorithmRouting.REPLY.getActionIndex(), replyMsg);
		} catch (IOException e) {
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.ERROR)) {
				Log.ROUTING.error(state.identity.getPath() + ", "
						+ e.getMessage() + "\n"
						+ Log.printStackTrace(e.getStackTrace()));
			}
		}
	}

	/**
	 * computes SLT<sup>|D</sup>, which is the set of scopes of SLT regarding
	 * the destination broker <tt>D</tt>.
	 * 
	 * @param broker
	 *            the identity of the broker.
	 * @return the set of scopes regarding the destination <tt>t</tt>.
	 */
	static List<Scope> computeSLTTowardsABroker(final URI broker) {
		List<Scope> result = new ArrayList<Scope>();
		if (broker == null) {
			return result;
		}
		BrokerState state = BrokerState.getInstance();
		for (Scope scope : state.scopeLookupTables.keySet()) {
			if (state.scopeLookupTables.get(scope).contains(broker)) {
				result.add(scope);
			}
		}
		return result;
	}

	/**
	 * computes SLT<sup>\D</sup>, which is the set of scopes of SLT regarding
	 * but the destination broker <tt>D</tt>.
	 * 
	 * @param brokerD
	 *            the identity of the broker.
	 * @return the set of scopes.
	 */
	private static List<Scope> computeSLTExceptTowardsABroker(final URI brokerD) {
		if (brokerD == null) {
			throw new IllegalArgumentException("uri of the broker is null");
		}
		List<Scope> result = new ArrayList<Scope>();
		BrokerState state = BrokerState.getInstance();
		boolean add;
		for (Scope scope : state.scopeLookupTables.keySet()) {
			add = false;
			for (URI brokerK : state.scopeLookupTables.get(scope)) {
				if (!brokerK.equals(brokerD)) {
					add = true;
				}
			}
			if (add) {
				result.add(scope);
			}
		}
		return result;
	}

	/**
	 * computes JSLT<sup>s</sup>, which is the set of neighbours E for which
	 * there is no SLT entry (t,D) in SLT, where the scope t is equal to
	 * <tt>s</tt> or is a super-scope of <tt>s</tt> and D is distinct from E.
	 * 
	 * @param s
	 *            the scope.
	 * @return a set of neighbouring brokers.
	 * 
	 */
	private static List<URI> computeJSLT(final Scope s) {
		BrokerState state = BrokerState.getInstance();
		List<URI> result = new ArrayList<URI>();
		boolean existSuperScope;
		if (s == null) {
			return result;
		}
		if (state.scopeGraphs.get(s.dimension()) == null) {
			new MuDEBSException("the scope graph of " + s.dimension()
					+ "does not exist.");
		}
		for (URI brokerD : state.brokerIdentities.keySet()) {
			existSuperScope = false;
			List<Scope> sltnodestination = computeSLTExceptTowardsABroker(brokerD);
			for (Scope t : state.scopeGraphs.get(s.dimension()).top()
					.subScopes()) {
				if (sltnodestination.contains(t)) {
					if (t.isEqualOrSuperScopeTransitively(s)) {
						existSuperScope = true;
					}
				}
			}
			if (!existSuperScope) {
				result.add(brokerD);
			}
		}
		return result;
	}

	/**
	 * computes the set of the neighbours that are in JSLT<sup>s</sup> and in
	 * JSLT<sup>t</sup>, except the last sender <tt>lastSender</tt>. They are
	 * the neighbours to forward the <tt>JoinScopeMsgContent</tt> to. When the
	 * edge includes a client and its super-scope, <tt>s</tt> is <tt>null</tt>.
	 * 
	 * @param lastSender
	 *            the last broker of the path.
	 * @param s
	 *            the starting scope of the edge or <tt>null</tt> in case of
	 *            client.
	 * @param t
	 *            the ending scope of the edge.
	 * @return the set of all neighbours that are in DSLT(<tt>t</tt>) or in
	 *         DSLT(<tt>s</tt>), except the last sender <tt>lastSender</tt>.
	 */
	static List<URI> computeSetOfNeighboursToFowardJoinScope(
			final URI lastSender, final Scope s, final Scope t) {
		List<URI> result = new ArrayList<URI>();
		List<URI> dslts = computeJSLT(s);
		List<URI> dsltt = computeJSLT(t);
		if (dslts != null) {
			result = dslts;
			if (dsltt != null) {
				for (URI uri : dsltt) {
					if (!result.contains(uri)) {
						result.add(uri);
					}
				}
			}
		} else {
			if (dsltt != null) {
				result = dsltt;
			}
		}
		result.remove(lastSender);
		return result;
	}

	/**
	 * checks the path can be a visibility.
	 * 
	 * @param phi1
	 *            the set of sets of scope paths (associated with publication).
	 * @param phi2
	 *            the set of sets of scope paths (associated with subscription).
	 * @return
	 * @throws MuDEBSException
	 */
	static boolean checkvp(final MultiScopingSpecification phi1,
			final MultiScopingSpecification phi2, final URI destination) {
		return (checkvpFromNotif(phi1, phi2, destination) && checkvpFromSub(
				phi2, phi1, destination));
	}

	/**
	 * checks the path can be a visibility from the point of view of
	 * subscription.
	 * 
	 * @param phi1
	 *            the set of sets of scope paths (associated with subscription).
	 * @param phi2
	 *            the set of sets of scope paths (associated with notification).
	 * @param destination
	 * @return
	 */
	static boolean checkvpFromSub(final MultiScopingSpecification phi1,
			final MultiScopingSpecification phi2, final URI destination) {
		BrokerState state = BrokerState.getInstance();
		HashMap<URI, SelectionKey> localClients = state.clientIdentities;
		HashMap<URI, SelectionKey> neighBours = state.brokerIdentities;
		if (phi1 == null || phi2 == null || phi1.getTheSets().isEmpty()
				|| phi2.getTheSets().isEmpty()) {
			return false;
		}
		boolean predicate1, predicate2, predicate3, predicate4 = false;
		int result = 0;
		for (URI dimPhi1 : phi1.getTheSets().keySet()) {
			for (ScopePathWithStatus sp1 : phi1.getTheSets().get(dimPhi1)
					.getTheSet()) {
				predicate1 = sp1.scopeSequence().get(0).identifier()
						.equals(ScopeTop.IDENTIFIER);
				predicate2 = existsBottomPath(phi2)
						&& !dimensionInPhi(phi2, sp1.dimension());
				predicate3 = isPrefixOfAReversedScopePathInMSP(sp1, phi2)
						&& localClients.containsKey(destination);
				predicate4 = existCommonSuffixPrefixWithStatusVerification(sp1,
						phi2) && neighBours.containsKey(destination);
				if (predicate1 || predicate2 || predicate3 || predicate4) {
					result++;
					break;
				}
			}
		}
		return (result == phi1.getTheSets().keySet().size());
	}

	/**
	 * checks the path can be a visibility from the point of view of
	 * notification.
	 * 
	 * @param phi1
	 *            the multiscoping specification (set of sets of scope paths)
	 *            associated with publication.
	 * @param phi2
	 *            the multiscoping specification (set of sets of scope paths)
	 *            associated with subscription.
	 * @param destination
	 * @return
	 */
	static boolean checkvpFromNotif(final MultiScopingSpecification phi1,
			final MultiScopingSpecification phi2, final URI destination) {
		BrokerState state = BrokerState.getInstance();
		HashMap<URI, SelectionKey> localClients = state.clientIdentities;
		HashMap<URI, SelectionKey> neighBours = state.brokerIdentities;
		if (phi1 == null || phi2 == null || phi1.getTheSets().isEmpty()
				|| phi2.getTheSets().isEmpty()) {
			return false;
		}
		boolean predicate1, predicate2, predicate3, predicate4 = false;
		int result = 0;
		for (URI dimPhi1 : phi1.getTheSets().keySet()) {
			for (ScopePathWithStatus sp1 : phi1.getTheSets().get(dimPhi1)
					.getTheSet()) {
				predicate1 = sp1.scopeSequence().get(0).identifier()
						.equals(ScopeBottom.IDENTIFIER);
				predicate2 = existsTopPath(phi2)
						&& !dimensionInPhi(phi2, sp1.dimension());
				predicate3 = existsPrefixOfAReversedScopePathInMSP(phi2, sp1)
						&& localClients.containsKey(destination);
				predicate4 = existCommonSuffixPrefixWithStatusVerification(sp1,
						phi2) && neighBours.containsKey(destination);
				if (predicate1 || predicate2 || predicate3 || predicate4) {
					result++;
					break;
				}
			}
		}
		return (result == phi1.getTheSets().keySet().size());
	}

	/**
	 * test whether there exists a scope in the multiscoping specification (set
	 * of sets of scope paths) <tt>phi</tt> that is prefix of the reversed path
	 * of <tt>scopePath</tt>.
	 * 
	 * @param phi
	 *            the set of sets of scope paths.
	 * @param scopePath
	 *            the scope path.
	 * @return
	 */
	private static boolean existsPrefixOfAReversedScopePathInMSP(
			final MultiScopingSpecification phi,
			final ScopePathWithStatus scopePath) {
		List<Scope> reversedScopePath = reverse(scopePath.scopeSequence());
		if (phi.getTheSets().containsKey(scopePath.dimension())) {
			for (ScopePathWithStatus sp : phi.getTheSets()
					.get(scopePath.dimension()).getTheSet()) {
				if (reversedScopePath.size() >= sp.scopeSequence().size()) {
					int j = 0;
					for (int i = 0; i < sp.scopeSequence().size(); i++) {
						if (sp.scopeSequence().get(i)
								.equals(reversedScopePath.get(i))) {
							j++;
						}
					}
					if (j == sp.scopeSequence().size()) {
						return true;
					}
				}
			}
		} else {
			return false;
		}
		return false;
	}

	/**
	 * tests whether there there exist a scope in the set of scope paths
	 * <tt>phi</tt> of which the scope path <tt>scopePath</tt> is prefix.
	 * 
	 * @param scopePath
	 *            the scope path to test.
	 * @param phi
	 *            the set of scope paths to test.
	 * @return a boolean whether the condition is true.
	 */
	public static boolean isPrefixOfAReversedScopePathInMSP(
			final ScopePathWithStatus scopePath,
			final MultiScopingSpecification phi) {
		if (scopePath.scopeSequence().isEmpty()) {
			return false;
		}
		if (phi.getTheSets().containsKey(scopePath.dimension())) {
			for (ScopePathWithStatus sp : phi.getTheSets()
					.get(scopePath.dimension()).getTheSet()) {
				if (sp.scopeSequence().size() >= scopePath.scopeSequence()
						.size()) {
					List<Scope> reversedSP = reverse(sp.scopeSequence());
					int j = 0;
					for (int i = 0; i < scopePath.scopeSequence().size(); i++) {
						if (scopePath.scopeSequence().get(i)
								.equals(reversedSP.get(i))) {
							j++;
						}
					}
					if (j == scopePath.scopeSequence().size()) {
						return true;
					}
				}
			}
		} else {
			return false;
		}
		return false;
	}

	/**
	 * tests whether there exists a common suffix-prefix between a scope path
	 * <tt>scopePath</tt> and a scope path in the set of sets of scope paths.
	 * empty path.
	 * 
	 * @param scopePath
	 *            the scope path to test.
	 * @param phi
	 *            the set of sets of scope paths.
	 * @return
	 */
	private static boolean existCommonSuffixPrefixWithStatusVerification(
			final ScopePathWithStatus scopePath,
			final MultiScopingSpecification phi) {
		if (phi.getTheSets().containsKey(scopePath.dimension())) {
			for (ScopePathWithStatus sp : phi.getTheSets()
					.get(scopePath.dimension()).getTheSet()) {
				try {
					if (computeCommonSuffixPrefix(scopePath, sp)
							.scopeSequence().size() >= 1
							&& !(scopePath.status() == ScopePathStatus.DOWN && sp
									.status() == ScopePathStatus.DOWN)) {
						return true;
					}
				} catch (MuDEBSException e) {
					if (Log.ON && Log.ROUTING.isEnabledFor(Level.WARN)) {
						Log.ROUTING.warn(BrokerState.getInstance().identity
								.getPath()
								+ ", error in computeCommonSuffixPrefix: "
								+ e.getMessage()
								+ "\n"
								+ Log.printStackTrace(e.getStackTrace()));
					}
					return false;
				}
			}
		}
		return false;
	}

	/**
	 * tests whether there exists a scope path ((&perp;),*) in the set of sets
	 * of scope paths.
	 * 
	 * @param phi
	 *            the set of sets of scope paths.
	 * @return
	 */
	private static boolean existsBottomPath(final MultiScopingSpecification phi) {
		if (phi == null) {
			return false;
		}
		if (phi.getTheSets() == null) {
			return false;
		}
		if (phi.getTheSets().containsKey(Scope.getDummyDimension())) {
			List<ScopePathWithStatus> ssp = phi.getTheSets()
					.get(Scope.getDummyDimension()).getTheSet();
			if (ssp == null) {
				return false;
			}
			for (ScopePathWithStatus sp : ssp) {
				if (sp.scopeSequence().isEmpty()) {
					break;
				}
				if (sp.scopeSequence().get(0).identifier()
						.equals(ScopeBottom.IDENTIFIER)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * tests whether there exists a scope path ((&perp;),*) in the set of sets
	 * of scope paths.
	 * 
	 * @param phi
	 *            the set of sets of scope paths.
	 * @return
	 */
	private static boolean existsTopPath(final MultiScopingSpecification phi) {
		if (phi == null) {
			return false;
		}
		if (phi.getTheSets() == null) {
			return false;
		}
		if (phi.getTheSets().containsKey(Scope.getDummyDimension())) {
			List<ScopePathWithStatus> ssp = phi.getTheSets()
					.get(Scope.getDummyDimension()).getTheSet();
			if (ssp == null) {
				return false;
			}
			for (ScopePathWithStatus sp : ssp) {
				if (sp.scopeSequence().isEmpty()) {
					break;
				}
				if (sp.scopeSequence().get(0).identifier()
						.equals(ScopeTop.IDENTIFIER)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * checks whether the dimension exists in the multiscoping specification.
	 * 
	 * @param phi
	 *            the multiscoping specification.
	 * @param d
	 *            the dimension to search for.
	 * @return the boolean stating whether the dimension is present.
	 */
	public static boolean dimensionInPhi(final MultiScopingSpecification phi,
			final URI d) {
		return phi.getTheSets().containsKey(d);
	}

	/**
	 * compute the suffix of the first scope path that is a prefix of the second
	 * scope path.
	 * 
	 * @param ssp1
	 *            the first scope paths to compute.
	 * @param ssp2
	 *            the second scope path to compute.
	 * @return the computed scope path.
	 * @throws MuDEBSException
	 *             the exception thrown when the two scope paths are associated
	 *             with different dimensions.
	 */
	static ScopePath computeCommonSuffixPrefix(final ScopePathWithStatus ssp1,
			final ScopePathWithStatus ssp2) throws MuDEBSException {
		if (!ssp1.dimension().equals(ssp2.dimension())) {
			throw new MuDEBSException(
					"the two scope paths do not have the same dimension");
		}
		ScopePath commonSuffixPrefix = new ScopePath(ssp1.dimension());
		if (ssp1 == null || ssp2 == null) {
			return commonSuffixPrefix;
		}
		List<Scope> revssp2 = reverse(ssp2.scopeSequence());
		int j = 0;
		for (int i = 0; i < ssp1.scopeSequence().size(); i++) {
			if (ssp1.scopeSequence().get(i).equals(revssp2.get(j))) {
				commonSuffixPrefix.addScope(revssp2.get(j));
				j++;
			}
		}
		return commonSuffixPrefix;
	}

	/**
	 * reverse a list of scopes.
	 * 
	 * @param list
	 *            the list to reverse.
	 * @return
	 */
	private static List<Scope> reverse(final List<Scope> list) {
		List<Scope> result = new ArrayList<Scope>();
		for (int i = list.size() - 1; i >= 0; i--) {
			result.add(list.get(i));
		}
		return result;
	}

	/**
	 * computes the new set of sets of scope paths that is associated with a
	 * notification.
	 * 
	 * @param phi
	 *            the set of sets of scopes path that is associated with a
	 *            notification.
	 * @param notif
	 *            content of the notification to parse.
	 * @return the set of sets of scope paths.
	 * @throws MuDEBSException
	 * 
	 */
	static MultiScopingSpecification computeScopePathsForNotif(
			final MultiScopingSpecification phi, final Document notif)
			throws MuDEBSException {
		MultiScopingSpecification result = phi;
		BrokerState state = BrokerState.getInstance();
		List<RoutingTableEntry> routingTableB = state.brokerRoutingTables
				.get(state.identity);
		if (routingTableB != null) {
			for (URI dimension : phi.getTheSets().keySet()) {
				SetOfScopePathsWithStatus set = phi.getTheSets().get(dimension);
				SetOfScopePathsWithStatus setR = new SetOfScopePathsWithStatus();
				boolean newToAdd = true;
				do {
					newToAdd = false;
					ListIterator<ScopePathWithStatus> iterator = set
							.getTheSet().listIterator();
					// optimisation: use the following condition
					// (iterator.hasNext() &&
					// !(setR.set().contains(iterator.next()))), but
					// NoSuchElementException encountered
					while (iterator.hasNext()) {
						ScopePathWithStatus sp = iterator.next();
						if (!setR.getTheSet().contains(sp)) {
							setR.addScopePathWithStatus(sp);
						}
						if (!(sp.scopeSequence().get(0).identifier()
								.equals(ScopeBottom.IDENTIFIER))
								&& !(sp.scopeSequence().get(0).identifier()
										.equals(ScopeTop.IDENTIFIER))) {
							Scope last = sp.scopeSequence().get(
									sp.scopeSequence().size() - 1);
							ScopePathWithStatus newPath = new ScopePathWithStatus(
									dimension, ScopePathStatus.UP);
							for (Scope s : sp.scopeSequence()) {
								// just copy the content of sp to newPath
								// (without verifying the rules of visibility
								// path construction)
								newPath.addScopeForGsonDeserialisation(s);
							}
							if (sp.status().equals(ScopePathStatus.UP)) {
								List<Scope> mapUpScopeList = findScopesForWhichNotifMatchAVisiFilter(
										ScopePathStatus.UP, last, notif);
								if (mapUpScopeList != null) {
									for (Scope t : mapUpScopeList) {
										if (!newPath.scopeSequence()
												.contains(t)) {
											newPath.addScopeForGsonDeserialisation(t);
											newPath.setStatus(ScopePathStatus.UP);
											if (!set.getTheSet().contains(
													newPath)) {
												iterator.add(newPath);
												if (!setR.getTheSet().contains(
														newPath)) {
													setR.addScopePathWithStatus(newPath);
												}
												newToAdd = true;
											}
											newPath = new ScopePathWithStatus(
													dimension,
													ScopePathStatus.UP);
											for (Scope s : sp.scopeSequence()) {
												// just copy the content of sp
												// to newPath
												// (without verifying the rules
												// of visibility path
												// construction)
												newPath.addScopeForGsonDeserialisation(s);
											}
										}
									}
								}
								List<Scope> mapDownScopeList = findScopesForWhichNotifMatchAVisiFilter(
										ScopePathStatus.DOWN, last, notif);
								if (mapDownScopeList != null) {
									for (Scope r : mapDownScopeList) {
										if (!newPath.scopeSequence()
												.contains(r)) {
											newPath.addScopeForGsonDeserialisation(r);
											newPath.setStatus(ScopePathStatus.DOWN);
											if (!set.getTheSet().contains(
													newPath)) {
												iterator.add(newPath);
												if (!setR.getTheSet().contains(
														newPath)) {
													setR.addScopePathWithStatus(newPath);
												}
												newToAdd = true;
											}
											newPath = new ScopePathWithStatus(
													dimension,
													ScopePathStatus.UP);
											for (Scope s : sp.scopeSequence()) {
												// just copy the content of sp
												// to newPath
												// (without verifying the rules
												// of visibility path
												// construction)
												newPath.addScopeForGsonDeserialisation(s);
											}
										}
									}
								}
							} else if (sp.status().equals(ScopePathStatus.DOWN)) {
								List<Scope> mapDownScopeList = findScopesForWhichNotifMatchAVisiFilter(
										ScopePathStatus.DOWN, last, notif);
								for (Scope r : mapDownScopeList) {
									if (!newPath.scopeSequence().contains(r)) {
										newPath.addScopeForGsonDeserialisation(r);
										newPath.setStatus(ScopePathStatus.DOWN);
										if (!set.getTheSet().contains(newPath)) {
											iterator.add(newPath);
											if (!setR.getTheSet().contains(
													newPath)) {
												setR.addScopePathWithStatus(newPath);
											}
											newToAdd = true;
										}
										newPath = new ScopePathWithStatus(
												dimension, ScopePathStatus.DOWN);
										for (Scope s : sp.scopeSequence()) {
											// just copy the content of sp to
											// newPath
											// (without verifying the rules of
											// visibility path construction)
											newPath.addScopeForGsonDeserialisation(s);
										}
									}
								}
							}
						}
					}
				} while (newToAdd);
				result.addSetOfScopePathsWithStatus(setR);
			}
		}
		return result;
	}

	/**
	 * computes the new set of sets of scope paths that is associated with
	 * subscription.
	 * 
	 * @param phi
	 *            the set of sets of scopes path that is associated with a
	 *            subscription.
	 * @return the set of sets of scope paths.
	 * @throws MuDEBSException
	 *             the exception thrown in case of a problem: when adding an
	 *             edge to a scope graph or adding a scope to a scope path with
	 *             status. This should never happen and must be treated as a
	 *             fatal error by the caller: it points to a potential bug.
	 * 
	 */
	static MultiScopingSpecification computeScopePathsForSub(
			final MultiScopingSpecification phi) throws MuDEBSException {
		MultiScopingSpecification result = phi;
		BrokerState state = BrokerState.getInstance();
		List<RoutingTableEntry> routingTableB = state.brokerRoutingTables
				.get(state.identity);
		if (routingTableB != null) {
			for (URI dimension : phi.getTheSets().keySet()) {
				SetOfScopePathsWithStatus set = phi.getTheSets().get(dimension);
				SetOfScopePathsWithStatus setR = new SetOfScopePathsWithStatus();
				boolean newToAdd = true;
				do {
					newToAdd = false;
					ListIterator<ScopePathWithStatus> iterator = set
							.getTheSet().listIterator();
					// optimisation: use the following condition
					// (iterator.hasNext() &&
					// !(setR.set().contains(iterator.next()))), but
					// NoSuchElementException encountered
					while (iterator.hasNext()) {
						ScopePathWithStatus sp = iterator.next();
						if (!setR.getTheSet().contains(sp)) {
							setR.addScopePathWithStatus(sp);
						}
						if (!(sp.scopeSequence().get(0).identifier()
								.equals(ScopeBottom.IDENTIFIER))
								&& !(sp.scopeSequence().get(0).identifier()
										.equals(ScopeTop.IDENTIFIER))) {
							Scope last = sp.scopeSequence().get(
									sp.scopeSequence().size() - 1);
							ScopePathWithStatus newPath = new ScopePathWithStatus(
									dimension, ScopePathStatus.UP);
							for (Scope s : sp.scopeSequence()) {
								// just copy the content of sp to newPath
								// (without verifying the rules of visibility
								// path construction)
								newPath.addScopeForGsonDeserialisation(s);
							}
							if (sp.status().equals(ScopePathStatus.UP)) {
								List<Scope> mapUpScopeList = findDirectSubScopesOrSuperScopes(
										ScopePathStatus.UP, last);
								if (mapUpScopeList != null) {
									for (Scope t : mapUpScopeList) {
										if (!newPath.scopeSequence()
												.contains(t)) {
											newPath.addScopeForGsonDeserialisation(t);
											newPath.setStatus(ScopePathStatus.UP);
											if (!set.getTheSet().contains(
													newPath)) {
												iterator.add(newPath);
												if (!setR.getTheSet().contains(
														newPath)) {
													setR.addScopePathWithStatus(newPath);
												}
												newToAdd = true;
											}
											newPath = new ScopePathWithStatus(
													dimension,
													ScopePathStatus.UP);
											for (Scope s : sp.scopeSequence()) {
												// just copy the content of sp
												// to newPath
												// (without verifying the rules
												// of visibility path
												// construction)
												newPath.addScopeForGsonDeserialisation(s);
											}
										}
									}
								}
								List<Scope> mapDownScopeList = findDirectSubScopesOrSuperScopes(
										ScopePathStatus.DOWN, last);
								if (mapDownScopeList != null) {
									for (Scope r : mapDownScopeList) {
										if (!newPath.scopeSequence()
												.contains(r)) {
											newPath.addScopeForGsonDeserialisation(r);
											newPath.setStatus(ScopePathStatus.DOWN);
											if (!set.getTheSet().contains(
													newPath)) {
												iterator.add(newPath);
												if (!setR.getTheSet().contains(
														newPath)) {
													setR.addScopePathWithStatus(newPath);
												}
												newToAdd = true;
											}
											newPath = new ScopePathWithStatus(
													dimension,
													ScopePathStatus.UP);
											for (Scope s : sp.scopeSequence()) {
												// just copy the content of sp
												// to newPath
												// (without verifying the rules
												// of visibility path
												// construction)
												newPath.addScopeForGsonDeserialisation(s);
											}
										}
									}
								}
							} else if (sp.status().equals(ScopePathStatus.DOWN)) {
								List<Scope> mapDownScopeList = findDirectSubScopesOrSuperScopes(
										ScopePathStatus.DOWN, last);
								for (Scope r : mapDownScopeList) {
									if (!newPath.scopeSequence().contains(r)) {
										newPath.addScopeForGsonDeserialisation(r);
										newPath.setStatus(ScopePathStatus.DOWN);
										if (!set.getTheSet().contains(newPath)) {
											iterator.add(newPath);
											if (!setR.getTheSet().contains(
													newPath)) {
												setR.addScopePathWithStatus(newPath);
											}
											newToAdd = true;
										}
										newPath = new ScopePathWithStatus(
												dimension, ScopePathStatus.DOWN);
										for (Scope s : sp.scopeSequence()) {
											// just copy the content of sp to
											// newPath
											// (without verifying the rules of
											// visibility path construction)
											newPath.addScopeForGsonDeserialisation(s);
										}
									}
								}
							}
						}
					}
				} while (newToAdd);
				result.addSetOfScopePathsWithStatus(setR);
			}
		}
		return result;
	}

	/**
	 * gets the list of scopes that are sub-scopes [super-scopes] of <tt>s</tt>,
	 * where (*,downward [upward] visibility from s to r,B) is in T<SUB>B</SUB>,
	 * and the notification <tt>notif</tt> matches the visibility filter.
	 * 
	 * @param status
	 *            the status of the scope path of the notification.
	 * @param s
	 *            the departure scope.
	 * @param notif
	 *            the notification to parse.
	 * @return
	 */
	private static List<Scope> findScopesForWhichNotifMatchAVisiFilter(
			final ScopePathStatus status, final Scope s, final Document notif) {
		List<Scope> result = new ArrayList<Scope>();
		BrokerState state = BrokerState.getInstance();
		List<RoutingTableEntry> routingTableB = state.brokerRoutingTables
				.get(state.identity);
		if (routingTableB != null) {
			for (RoutingTableEntry re : routingTableB) {
				if (re instanceof RoutingTableEntryVisibility) {
					SetOfScopePathsWithStatus ssp = re.getPhi().getTheSets()
							.get(s.dimension());
					if (ssp != null) {
						Scope superORsubScope = ssp.getTheSet().get(0)
								.scopeSequence().get(0);
						ScopePathStatus pathStatus = ssp.getTheSet().get(0)
								.status();
						if (!((RoutingTableEntryVisibility) re)
								.getEndingScope().identifier()
								.equals(ScopeTop.IDENTIFIER)
								&& pathStatus.equals(status)
								&& superORsubScope.equals(s)
								&& re.getFilter().evaluate(notif)) {
							result.add(((RoutingTableEntryVisibility) re)
									.getEndingScope());
						}
					}
				}
			}
		}
		return result;
	}

	/**
	 * gets the list of scopes that are direct sub-scopes [super-scopes] of
	 * <tt>s</tt>, where (*,downward [upward] visibility from s to r,B) is in
	 * T<SUB>B</SUB>.
	 * 
	 * @param status
	 *            the status of the scope path of the notification.
	 * @param s
	 *            the departure scope.
	 * @param notif
	 *            the notification to parse.
	 * @return
	 */
	private static List<Scope> findDirectSubScopesOrSuperScopes(
			final ScopePathStatus status, final Scope s) {
		List<Scope> result = new ArrayList<Scope>();
		BrokerState state = BrokerState.getInstance();
		List<RoutingTableEntry> routingTableB = state.brokerRoutingTables
				.get(state.identity);
		if (routingTableB != null) {
			for (RoutingTableEntry re : routingTableB) {
				if (re instanceof RoutingTableEntryVisibility) {
					SetOfScopePathsWithStatus ssp = re.getPhi().getTheSets()
							.get(s.dimension());
					if (ssp != null) {
						Scope superORsubScope = ssp.getTheSet().get(0)
								.scopeSequence().get(0);
						ScopePathStatus pathStatus = ssp.getTheSet().get(0)
								.status();
						if (!((RoutingTableEntryVisibility) re)
								.getEndingScope().identifier()
								.equals(ScopeTop.IDENTIFIER)
								&& pathStatus.equals(status)
								&& superORsubScope.equals(s)) {
							result.add(((RoutingTableEntryVisibility) re)
									.getEndingScope());
						}
					}
				}
			}
		}
		return result;
	}

	/**
	 * gets the entries of scope lookup table of the broker as a string.
	 * 
	 * @param slt
	 *            the scope lookup table.
	 * @return
	 */
	static String toStringSLT() {
		BrokerState state = BrokerState.getInstance();
		HashMap<Scope, List<URI>> slt = state.scopeLookupTables;
		if (slt.keySet().isEmpty()) {
			return "()";
		}
		String result = "(";
		List<Scope> scopeList = new ArrayList<Scope>();
		for (Scope scope : slt.keySet()) {
			scopeList.add(scope);
		}
		for (int i = 0; i < scopeList.size() - 1; i++) {
			result = result + "(" + scopeList.get(i).identifier() + ", "
					+ slt.get(scopeList.get(i)) + "), ";
		}
		return result + "(" + scopeList.get(scopeList.size() - 1).identifier()
				+ ", " + slt.get(scopeList.get(scopeList.size() - 1)) + "))";
	}

	/**
	 * checks whether the neighbouring broker is interested in the subscription
	 * currently in process.
	 * 
	 * @param broker
	 *            the neighbouring broker.
	 * @param phi
	 *            the multi-scoping specification.
	 * @return the boolean stating
	 */
	static boolean neighbouringBrokerInterestedInSubscription(final URI broker,
			final MultiScopingSpecification phi) {
		BrokerState state = BrokerState.getInstance();
		for (URI dimension : phi.getTheSets().keySet()) {
			for (ScopePathWithStatus sp : phi.getTheSets().get(dimension)
					.getTheSet()) {
				if (sp.scopeSequence().get(sp.scopeSequence().size() - 1) instanceof ScopeTop) {
					return true;
				}
				if (!(state.scopeLookupTables.get(sp.scopeSequence().get(
						sp.scopeSequence().size() - 1)) == null)) {
					if (state.scopeLookupTables.get(
							sp.scopeSequence().get(
									sp.scopeSequence().size() - 1)).contains(
							broker)) {
						return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * updates the value of the attribute <tt>attribute</tt> into the file
	 * <tt>PerfAttributes.RESULT_DIR/PerfAttributes.PREF_CONFIG_FILE</tt>.
	 * 
	 * @param attribute
	 *            the performance evaluation attribute (defined in class
	 *            {@link PerfLevel}.
	 * @param value
	 *            the new value associated with the attribute.
	 * @throws IOException
	 *             exception thrown in case of error.
	 */
	public static void updateResult(final String attribute, final Number value)
			throws IOException {
		BrokerState state = BrokerState.getInstance();
		OutputStream out = null;
		try {
			String resultDir = PerfAttributes.RESULT_DIR;
			File dir = new File(resultDir);
			if (!dir.exists()) {
				if (!dir.mkdir()) {
					throw new FileSystemException(
							"failed to create directory '"
									+ PerfAttributes.RESULT_DIR + "'");
				}
			}
			String perfConfigFileName = PerfAttributes.PREF_CONFIG_FILE;
			int posSlash = PerfAttributes.PREF_CONFIG_FILE.lastIndexOf("/");
			int posPoint = PerfAttributes.PREF_CONFIG_FILE.lastIndexOf(".");
			if (posSlash > 0 && posPoint > 0 && posSlash + 1 < posPoint) {
				perfConfigFileName = perfConfigFileName.substring(posSlash + 1,
						posPoint);
			}
			PerfAttributes.PERF_RESULT_FILE = resultDir + "/"
					+ perfConfigFileName;
			Date date = new Date();
			DateFormat df = DateFormat.getDateTimeInstance(DateFormat.SHORT,
					DateFormat.SHORT);
			PerfAttributes.PERF_RESULT_FILE += "_"
					+ df.format(date).replaceAll("/| |:", "-")
					+ "_"
					+ state.identity.toString().replaceAll("mudebs://", "")
							.replaceAll(":|/", "-") + ".result";
			Properties props = new Properties();
			File f = new File(PerfAttributes.PERF_RESULT_FILE);
			if (f.exists()) {
				props.load(new FileReader(f));
				props.setProperty(attribute, value.toString());
			} else {
				props.setProperty(attribute, value.toString());
				f.createNewFile();
			}
			out = new FileOutputStream(f);
			props.store(out, "file created by " + state.identity);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
