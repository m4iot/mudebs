/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */
package mudebs.broker.algorithms.overlaymanagement;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import mudebs.common.Constants;
import mudebs.common.algorithms.AbstractMessageContent;
import mudebs.common.algorithms.AlgorithmActionInterfaceToBeDeprecated;
import mudebs.common.algorithms.overlaymanagement.IdentityMsgContent;
import mudebs.common.algorithms.overlaymanagement.TerminationDetectionMsgContent;
import mudebs.common.algorithms.overlaymanagement.TerminationMsgContent;

/**
 * This Enumeration type declares the algorithm of the management of the overlay
 * network of brokers and clients.
 * 
 * @author Denis Conan
 * 
 */
public enum AlgorithmOverlayManagement implements AlgorithmActionInterfaceToBeDeprecated {
	/**
	 * identity message type for the management of the overlay.
	 */
	IDENTITY(IdentityMsgContent.class) {
		public void execute(final AbstractMessageContent content) {
			AlgorithmOverlayManagementActions.receiveIdentity(content);
		}
	},
	/**
	 * termination detection message for the overlay network of brokers.
	 */
	TERMINATION_DETECTION(TerminationDetectionMsgContent.class) {
		public void execute(final AbstractMessageContent content) {
			AlgorithmOverlayManagementActions
					.receiveTerminationDetection(content);
		}
	},
	/**
	 * termination message for the overlay network of brokers.
	 */
	TERMINATION(TerminationMsgContent.class) {
		public void execute(final AbstractMessageContent content) {
			AlgorithmOverlayManagementActions
					.receiveTermination(content);
		}
	};

	/**
	 * index of the first message type of this algorithm.
	 */
	public final int algorithmOffset = Constants.OVERLAY_MANAGEMENT_START_INDEX;
	/**
	 * collection of the actions of this algorithm. The Key is the index of the
	 * corresponding message type. This collection is kept private to avoid
	 * modifications.
	 */
	private final static Map<Integer, AlgorithmOverlayManagement> privateMapOfActions;
	/**
	 * unmodifiable collection corresponding to <tt>privateMapOfActions</tt>.
	 */
	public final static Map<Integer, AlgorithmOverlayManagement> mapOfActions;
	/**
	 * index of the action of this message type.
	 */
	private final int actionIndex;
	/**
	 * class of the content of the message received.
	 */
	private final Class<? extends AbstractMessageContent> msgContentClass;

	/**
	 * static block to build collections of actions.
	 */
	static {
		privateMapOfActions = new HashMap<Integer, AlgorithmOverlayManagement>();
		for (AlgorithmOverlayManagement aa : AlgorithmOverlayManagement
				.values()) {
			privateMapOfActions.put(aa.actionIndex, aa);
		}
		mapOfActions = Collections.unmodifiableMap(privateMapOfActions);
	}

	/**
	 * constructor of message type object.
	 * 
	 * @param msgClass
	 *            the class of the content of the message received.
	 */
	private AlgorithmOverlayManagement(
			final Class<? extends AbstractMessageContent> msgClass) {
		this.actionIndex = Constants.MESSAGE_TYPE_START_INDEX + algorithmOffset
				+ ordinal();
		this.msgContentClass = msgClass;
	}

	/**
	 * obtains the index of this message type.
	 */
	public int getActionIndex() {
		return actionIndex;
	}

	/**
	 * obtains the class of the message received in this action.
	 */
	public Class<? extends AbstractMessageContent> getMsgContentClass() {
		return msgContentClass;
	}

	@Override
	public String toString() {
		return String.valueOf(actionIndex);
	}
}
