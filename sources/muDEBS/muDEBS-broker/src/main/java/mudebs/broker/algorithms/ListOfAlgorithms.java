/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2017 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */
package mudebs.broker.algorithms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import mudebs.broker.BrokerState;
import mudebs.broker.algorithms.overlaymanagement.AlgorithmOverlayManagement;
import mudebs.broker.algorithms.routing.AlgorithmRouting;
import mudebs.broker.algorithms.scripting.broker.AlgorithmScriptingBroker;
import mudebs.broker.algorithms.scripting.client.AlgorithmScriptingClient;
import mudebs.common.Log;
import mudebs.common.algorithms.AbstractMessageContent;
import mudebs.common.algorithms.AlgorithmActionInterfaceToBeDeprecated;
import mudebs.common.algorithms.AlgorithmActionInvocationException;
import mudebs.common.algorithms.routing.ScopePathWithStatus;
import mudebs.common.algorithms.routing.ScopePathWithStatusAdaptor;

import org.apache.log4j.Level;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * This Enumeration type declares the algorithms of the broker.
 * 
 * @author Denis Conan
 * 
 */
public enum ListOfAlgorithms {
	/**
	 * message types exchanged for the routing algorithm.
	 */
	ALGORITHM_ROUTING(AlgorithmRouting.mapOfActions),
	/**
	 * message types exchanged for the management of the overlay.
	 */
	ALGORITHM_OVERLAY_MANAGEMENT(AlgorithmOverlayManagement.mapOfActions),
	/**
	 * message types exchanged for the scripting client.
	 */
	ALGORITHM_SCRIPTING_CLIENT(AlgorithmScriptingClient.mapOfActions),
	/**
	 * message types exchanged for the scripting broker.
	 */
	ALGORITHM_SCRIPTING_BROKER(AlgorithmScriptingBroker.mapOfActions);

	/**
	 * collection of the algorithms of the broker. This collection is kept
	 * private to avoid modifications.
	 */
	private final static List<Map<Integer, ? extends AlgorithmActionInterfaceToBeDeprecated>> privateMapOfAlgorithms;
	/**
	 * unmodifiable collection corresponding to <tt>privateMapOfAlgorithms</tt>.
	 */
	public final static List<Map<Integer, ? extends AlgorithmActionInterfaceToBeDeprecated>> mapOfAlgorithms;
	/**
	 * collection of the actions of this algorithm object of the broker.
	 */
	private final Map<Integer, ? extends AlgorithmActionInterfaceToBeDeprecated> mapOfActions;
	/**
	 * Gson object to de-serialise.
	 */
	private final static Gson gson = new GsonBuilder().registerTypeAdapter(
			ScopePathWithStatus.class, new ScopePathWithStatusAdaptor())
			.create();

	/**
	 * static block to build collections.
	 */
	static {
		privateMapOfAlgorithms = new ArrayList<Map<Integer, ? extends AlgorithmActionInterfaceToBeDeprecated>>();
		for (ListOfAlgorithms am : ListOfAlgorithms.values()) {
			privateMapOfAlgorithms.add(am.mapOfActions);
		}
		mapOfAlgorithms = Collections.unmodifiableList(privateMapOfAlgorithms);
		checkListOfAlgorithms();
	}

	/**
	 * constructor of this algorithm object.
	 * 
	 * @param map
	 *            collection of actions of this algorithm.
	 */
	private ListOfAlgorithms(
			Map<Integer, ? extends AlgorithmActionInterfaceToBeDeprecated> map) {
		mapOfActions = map;
	}

	/**
	 * check whether this list of algorithms conforms to the specification.
	 * 
	 * @return
	 */
	private static boolean checkListOfAlgorithms() {
		if (ListOfAlgorithms.mapOfAlgorithms.size() != mudebs.common.algorithms.ListOfAlgorithms.mapOfAlgorithms
				.size()) {
			throw new RuntimeException(
					"Bad number of algorithms: "
							+ " is "
							+ ListOfAlgorithms.mapOfAlgorithms.size()
							+ ", but should be "
							+ mudebs.common.algorithms.ListOfAlgorithms.mapOfAlgorithms
									.size());
		}
		for (int i = 0; i < ListOfAlgorithms.mapOfAlgorithms.size(); i++) {
			if (ListOfAlgorithms.mapOfAlgorithms.get(i).size() != mudebs.common.algorithms.ListOfAlgorithms.mapOfAlgorithms
					.get(i).size()) {
				throw new RuntimeException(
						"Bad number of messages in algorithm "
								+ i
								+ ": is "
								+ ListOfAlgorithms.mapOfAlgorithms.get(i)
										.size()
								+ ", but should be "
								+ mudebs.common.algorithms.ListOfAlgorithms.mapOfAlgorithms
										.get(i).size());
			}
			for (Integer msgIndex : ListOfAlgorithms.mapOfAlgorithms.get(i)
					.keySet()) {
				AlgorithmActionInterfaceToBeDeprecated a = ListOfAlgorithms.mapOfAlgorithms
						.get(i).get(msgIndex);
				AlgorithmActionInterfaceToBeDeprecated b = mudebs.common.algorithms.ListOfAlgorithms.mapOfAlgorithms
						.get(i).get(msgIndex);
				if (a.getActionIndex() != b.getActionIndex()) {
					throw new RuntimeException("Bad action index: is "
							+ a.getActionIndex() + ", but should be "
							+ b.getActionIndex());
				}
				if (!a.getMsgContentClass().equals(b.getMsgContentClass())) {
					throw new RuntimeException("Bad message content class:"
							+ " is " + a.getMsgContentClass()
							+ ", but should be " + b.getMsgContentClass());
				}
			}
		}
		return true;
	}

	/**
	 * search for the action to execute in the collection of actions of the
	 * algorithm of the broker.
	 * 
	 * @param actionIndex
	 *            index of the action to execute.
	 * @param content
	 *            content of the message just received.
	 */
	public static void execute(final int actionIndex, final Object content) {
		boolean executed = false;
		for (Iterator<Map<Integer, ? extends AlgorithmActionInterfaceToBeDeprecated>> protocols = mapOfAlgorithms
				.iterator(); protocols.hasNext();) {
			Map<Integer, ? extends AlgorithmActionInterfaceToBeDeprecated> protocol = protocols
					.next();
			for (Iterator<? extends AlgorithmActionInterfaceToBeDeprecated> actions = protocol
					.values().iterator(); actions.hasNext();) {
				AlgorithmActionInterfaceToBeDeprecated action = actions.next();
				if (action.getActionIndex() == actionIndex) {
					executed = true;
					AbstractMessageContent c;
					if (content instanceof String) {
						c = gson.fromJson((String) content,
								action.getMsgContentClass());
						action.execute(c);
					} else {
						if (Log.ON && Log.DISPATCH.isEnabledFor(Level.WARN)) {
							Log.DISPATCH
									.warn(BrokerState.getInstance().identity
											.getPath()
											+ ", the content is not "
											+ "a String serialised with Gson: "
											+ content.getClass().getName());
						}
					}
				}
			}
		}
		if (!executed) {
			if (Log.ON && Log.DISPATCH.isEnabledFor(Level.WARN)) {
				Log.DISPATCH.warn(BrokerState.getInstance().identity.getPath()
						+ ", unknown action: " + actionIndex);
			}
		}
	}
}
