/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2017 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Léon Lim
 */
package mudebs.broker.algorithms.routing;

import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.UndeclaredThrowableException;
import java.net.URI;
import java.nio.channels.SelectionKey;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.script.ScriptException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import mudebs.broker.BrokerState;
import mudebs.common.Log;
import mudebs.common.MuDEBSException;
import mudebs.common.PerfAttributes;
import mudebs.common.algorithms.ACK;
import mudebs.common.algorithms.AbstractMessageContent;
import mudebs.common.algorithms.CommandResult;
import mudebs.common.algorithms.CommandStatus;
import mudebs.common.algorithms.EntityType;
import mudebs.common.algorithms.OperationalMode;
import mudebs.common.algorithms.Reply;
import mudebs.common.algorithms.routing.ABACInformation;
import mudebs.common.algorithms.routing.AdminAcknowledgementMsgContent;
import mudebs.common.algorithms.routing.AdminAdvertisementMsgContent;
import mudebs.common.algorithms.routing.AdminJoinScopeMsgContent;
import mudebs.common.algorithms.routing.AdminPublication;
import mudebs.common.algorithms.routing.AdminPublicationMsgContent;
import mudebs.common.algorithms.routing.AdminReplyMsgContent;
import mudebs.common.algorithms.routing.AdminRequestMsgContent;
import mudebs.common.algorithms.routing.AdminSubscriptionMsgContent;
import mudebs.common.algorithms.routing.AdminUnadvertisementMsgContent;
import mudebs.common.algorithms.routing.AdminUnsubscriptionMsgContent;
import mudebs.common.algorithms.routing.AdvertisementMsgContent;
import mudebs.common.algorithms.routing.CollectiveAdminPublicationMsgContent;
import mudebs.common.algorithms.routing.CollectiveAdminSubscriptionMsgContent;
import mudebs.common.algorithms.routing.CollectivePublicationMsgContent;
import mudebs.common.algorithms.routing.CollectiveSubscriptionMsgContent;
import mudebs.common.algorithms.routing.GetScopesMsgContent;
import mudebs.common.algorithms.routing.JoinScopeMsgContent;
import mudebs.common.algorithms.routing.LeaveScopeMsgContent;
import mudebs.common.algorithms.routing.MultiScopingSpecification;
import mudebs.common.algorithms.routing.Publication;
import mudebs.common.algorithms.routing.PublicationMsgContent;
import mudebs.common.algorithms.routing.RequestMsgContent;
import mudebs.common.algorithms.routing.Scope;
import mudebs.common.algorithms.routing.ScopeBottom;
import mudebs.common.algorithms.routing.ScopeGraph;
import mudebs.common.algorithms.routing.ScopePathStatus;
import mudebs.common.algorithms.routing.ScopePathWithStatus;
import mudebs.common.algorithms.routing.ScopeTop;
import mudebs.common.algorithms.routing.SetOfScopePathsWithStatus;
import mudebs.common.algorithms.routing.Subscription;
import mudebs.common.algorithms.routing.SubscriptionMsgContent;
import mudebs.common.algorithms.routing.UnadvertisementMsgContent;
import mudebs.common.algorithms.routing.UnsubscriptionMsgContent;
import mudebs.common.filters.Filter;
import mudebs.common.filters.JavaScriptRoutingFilter;
import mudebs.common.filters.JavaScriptVisibilityFilter;

import org.apache.log4j.Level;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * This class defines the methods implementing the reaction concerning the
 * reception of routing messages.
 * 
 * @author Denis Conan
 * @author Léon Lim
 */
public class AlgorithmRoutingActions {
	/**
	 * receives an advertisement message from a client. The message is of type
	 * <tt>AdvertisementMsgContent</tt>. If required, the broker sends an
	 * acknowledgement.
	 * 
	 * @param content
	 *            the message to treat.
	 * @throws Exception
	 */
	public static void receiveAdvertisement(
			final AbstractMessageContent content) throws Exception {
		BrokerState state = BrokerState.getInstance();
		AdvertisementMsgContent mymsg = (AdvertisementMsgContent) content;
		if (!state.currIdentityType.equals(EntityType.CLIENT)) {
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.ERROR)) {
				Log.ROUTING.error(state.identity.getPath()
						+ ", the advertisement was not sent by a client ("
						+ mymsg + ")");
			}
			return;
		}
		if (Log.ON && Log.ROUTING.getLevel().equals(Level.INFO)) {
			Log.ROUTING.info(state.identity.getPath() + ", advertisement = "
					+ mymsg.toStringBrief());
		} else if (Log.ON && Log.ROUTING.isEnabledFor(Level.DEBUG)) {
			Log.ROUTING.debug(
					state.identity.getPath() + ", advertisement = " + mymsg);
		}
		MultiScopingSpecification phi = null;
		if (mymsg.getPhiNotif() == null) {
			phi = MultiScopingSpecification.create(new ScopeBottom(),
					ScopePathStatus.UP);
		} else {
			phi = mymsg.getPhiNotif();
		}
		List<URI> path = new ArrayList<URI>();
		path.add(mymsg.getClient());
		List<URI> pathJoinScope = new ArrayList<URI>();
		pathJoinScope.add(state.identity);
		for (URI dimension : phi.getTheSets().keySet()) {
			SetOfScopePathsWithStatus ssp = phi.getTheSets().get(dimension);
			if (ssp.getTheSet().size() == 0) {
				Util.sendNAK(mymsg.getAck(), null, mymsg.getClient(),
						mymsg.getSeqNumber(),
						"no scope path for dimension = " + dimension,
						Level.WARN);
				return;
			} else if (ssp.getTheSet().size() > 1) {
				Util.sendNAK(mymsg.getAck(), null, mymsg.getClient(),
						mymsg.getSeqNumber(),
						"more than one scope path for dimension = " + dimension
								+ "(" + ssp.getTheSet() + ")",
						Level.WARN);
				return;
			} else {
				ScopePathWithStatus sp = ssp.getTheSet().get(0);
				if (sp.scopeSequence().size() == 0) {
					Util.sendNAK(mymsg.getAck(), null, mymsg.getClient(),
							mymsg.getSeqNumber(),
							"no scope in scope path of dimension = "
									+ dimension,
							Level.WARN);
					return;
				} else if (sp.scopeSequence().size() > 1) {
					Util.sendNAK(mymsg.getAck(), null, mymsg.getClient(),
							mymsg.getSeqNumber(),
							"more than one scope in scope path of dimension = "
									+ dimension,
							Level.WARN);
					return;
				} else {
					Scope scope = sp.scopeSequence().get(0);
					if (scope.identifier().equals(ScopeTop.IDENTIFIER)) {
						Util.sendNAK(mymsg.getAck(), null, mymsg.getClient(),
								mymsg.getSeqNumber(),
								"scope top not allowed in advertisement",
								Level.WARN);
						return;
					} else {
						if (state.scopeGraphs.get(dimension) == null) {
							handleJoinScopeMessage(true, pathJoinScope,
									mymsg.getSeqNumber(), dimension,
									scope.identifier(), ScopeTop.IDENTIFIER,
									JavaScriptVisibilityFilter.ALWAYS_TRUE,
									JavaScriptVisibilityFilter.ALWAYS_FALSE);
						} else if (!state.scopeGraphs.get(dimension)
								.existsNode(scope)) {
							handleJoinScopeMessage(true, pathJoinScope,
									mymsg.getSeqNumber(), dimension,
									scope.identifier(), ScopeTop.IDENTIFIER,
									JavaScriptVisibilityFilter.ALWAYS_TRUE,
									JavaScriptVisibilityFilter.ALWAYS_FALSE);
						}
					}
				}
			}
		}
		if (PerfAttributes.performance_evaluation
				&& !mymsg.getIdentifier().contains("startperf")) {
			state.nbAdvertisementsReceived++;
		}
		handleAdvertisementMessage(mymsg.getClient(),
				mymsg.getOperationalMode(), mymsg.getAck(),
				mymsg.getSeqNumber(), null, new ArrayList<URI>(),
				mymsg.getIdentifier(), mymsg.getRoutingFilter(), phi,
				mymsg.getPolicyContent());
	}

	/**
	 * receives an admin advertisement message from a broker. The message is of
	 * type <tt>AdminAdvertisementMsgContent</tt>.
	 * 
	 * @param content
	 *            the message to treat.
	 * @throws Exception
	 *             the exception thrown in case of problem.
	 */
	public static void receiveAdminAdvertisement(
			final AbstractMessageContent content) throws Exception {
		BrokerState state = BrokerState.getInstance();
		state.nbMsgsInTransit--;
		AdminAdvertisementMsgContent mymsg = (AdminAdvertisementMsgContent) content;
		if (!state.currIdentityType.equals(EntityType.BROKER)) {
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.ERROR)) {
				Log.ROUTING.error(
						state.identity.getPath() + ", the admin advertisement"
								+ " was sent by a client (" + mymsg + ")");
			}
			return;
		}
		if (Log.ON && Log.ROUTING.getLevel().equals(Level.INFO)) {
			Log.ROUTING.info(state.identity.getPath()
					+ ", admin advertisement = " + mymsg.toStringBrief());
		} else if (Log.ON && Log.ROUTING.isEnabledFor(Level.DEBUG)) {
			Log.ROUTING.debug(state.identity.getPath()
					+ ", admin advertisement = " + mymsg);
		}
		handleAdvertisementMessage(mymsg.getPath().get(0),
				OperationalMode.GLOBAL, mymsg.getAck(), mymsg.getSeqNumber(),
				mymsg.getPath().get(mymsg.getPath().size() - 1),
				mymsg.getPath(), mymsg.getIdentifier(),
				mymsg.getRoutingFilter(), mymsg.getPhiNotif(),
				mymsg.getPolicyContent());
	}

	/**
	 * handles the advertisement message. The advertisement must be a new
	 * advertisement: no already existing corresponding advertisement table
	 * entry (the identifier is the attribute that uniquely identifies an
	 * advertisement table entry). The filter is a JavaScript, which is
	 * evaluated in the scripting engine. Then, the advertisement is added to
	 * the advertisement table of the forwarder, and if this is a global
	 * advertisement, it is forwarded to all the neighbouring brokers, except
	 * <tt>forwarder</tt>. If required, the broker sends an acknowledgement.
	 * 
	 * @param client
	 *            the URI of the client, which originally made the call.
	 * @param opMode
	 *            boolean stating whether this is a local advertisement.
	 * @param ack
	 *            the parameter indicating whether an acknowledgement is
	 *            expected.
	 * @param seqNumber
	 *            the sequence number of the call to use for acknowledging.
	 * @param forwarder
	 *            the URI of the broker that forwards the advertisement. It
	 *            equals <tt>null</tt> when the advertisement comes from a
	 *            client.
	 * @param path
	 *            the path of the advertisement received.
	 * @param identifier
	 *            the identifier of the advertisement.
	 * @param routingFilter
	 *            the filter of the advertisement.
	 * @param phi
	 *            the set of scopes associated with the filter.
	 * @param policyContent
	 *            the XACML policy content.
	 * @throws Exception
	 *             the exception thrown in case of problem.
	 */
	public static void handleAdvertisementMessage(final URI client,
			final OperationalMode opMode, final ACK ack, final int seqNumber,
			final URI forwarder, final List<URI> path, final String identifier,
			final String routingFilter, final MultiScopingSpecification phi,
			final String policyContent) throws Exception {
		BrokerState state = BrokerState.getInstance();
		if (forwarder == null) {
			HashMap<String, AdvertisementTableEntry> advs = state.localAdvertisements
					.get(client);
			if (advs == null) {
				advs = new HashMap<String, AdvertisementTableEntry>();
				state.localAdvertisements.put(client, advs);
			}
			if (advs.containsKey(identifier)) {
				if (Log.ON && Log.ROUTING.isEnabledFor(Level.WARN)) {
					Log.ROUTING.warn(state.identity.getPath()
							+ ", already registered advertisement = "
							+ identifier);
				}
				return;
			}
		} else {
			HashMap<URI, HashMap<String, AdvertisementTableEntry>> fadvs = state.remoteAdvertisements
					.get(forwarder);
			if (fadvs == null) {
				fadvs = new HashMap<URI, HashMap<String, AdvertisementTableEntry>>();
				state.remoteAdvertisements.put(forwarder, fadvs);
			}
			HashMap<String, AdvertisementTableEntry> advs = fadvs.get(client);
			if (advs == null) {
				advs = new HashMap<String, AdvertisementTableEntry>();
				state.remoteAdvertisements.get(forwarder).put(client, advs);
			}
			if (advs.containsKey(identifier)) {
				Util.sendNAK(ack, forwarder, client, seqNumber,
						"already registered remote advertisement = "
								+ identifier,
						Level.INFO);
				return;
			}
		}
		Filter mf = null;
		if (PerfAttributes.performance_evaluation) {
			mf = state.advertisementFiltersForPerf.get(routingFilter);
		}
		if (mf == null) {
			try {
				if (routingFilter.equals(state.returnTrueFilter)) {
					mf = FilterReturnTrue.getInstance();
				} else if (routingFilter.equals(state.returnFalseFilter)) {
					mf = FilterReturnFalse.getInstance();
				} else {
					mf = Util.createJavaScriptFilter(routingFilter,
							JavaScriptRoutingFilter.class);
					if (PerfAttributes.performance_evaluation) {
						state.advertisementFiltersForPerf.put(routingFilter,
								mf);
					}
				}
			} catch (ScriptException e) {
				Util.sendNAK(ack, forwarder, client, seqNumber,
						"javascript engine problem for advertisement = "
								+ identifier,
						Level.ERROR);
				return;
			}
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
				Log.ROUTING
						.trace(state.identity.getPath() + ", inv.getInterface"
								+ "(MonolithicFilter.class) returned " + mf);
			}
		}
		// adding the new policy
		if (policyContent != null) {
			state.xacmlPEP.addPolicy(policyContent);
		}
		AdvertisementTableEntry advEntry = new AdvertisementTableEntry(mf,
				policyContent, client, opMode, identifier, phi);
		if (forwarder == null) {
			state.localAdvertisements.get(client).put(identifier, advEntry);
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
				Log.ROUTING.trace(state.identity.getPath()
						+ ", set of local advertisements for " + client + " is "
						+ state.localAdvertisements.get(client));
				Log.ROUTING.trace(state.identity.getPath()
						+ ", nbAdvertisementsReceived = "
						+ state.nbAdvertisementsReceived);
			}
		} else {
			state.remoteAdvertisements.get(forwarder).get(client)
					.put(identifier, advEntry);
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
				Log.ROUTING.trace(state.identity.getPath()
						+ ", set of remote advertisements for " + forwarder
						+ " is " + state.remoteAdvertisements.get(forwarder));
				Log.ROUTING.trace(state.identity.getPath()
						+ ", nbAdvertisementsReceived = "
						+ state.nbAdvertisementsReceived);
			}
		}
		if (opMode == OperationalMode.GLOBAL) {
			if (ack == ACK.GLOBAL) {
				state.createEntryAcksToReceive(client, seqNumber);
				if (state.acksToReceive.get(client).get(seqNumber)
						.contains(forwarder)) {
					state.acksToReceive.get(client).get(seqNumber)
							.remove(forwarder);
					if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
						Log.ROUTING.trace(state.identity.getPath()
								+ ", admin advertisement serving "
								+ "as an ack for " + forwarder + " and "
								+ client + " with seq. number " + seqNumber);
					}
				}
				if (forwarder == null) {
					state.createEntryAcksToSendToClients(client, seqNumber);
				} else {
					state.createEntryAcksToSendToBrokers(client, seqNumber,
							forwarder);
				}
			}
			if (forwarder == null) {
				path.add(client);
			}
			path.add(state.identity);
			AdminAdvertisementMsgContent m = new AdminAdvertisementMsgContent(
					path, ack, seqNumber, identifier, routingFilter, phi,
					policyContent);
			for (URI uri : state.brokerIdentities.keySet()) {
				if (!path.contains(uri)) {
					try {
						state.broker.sendToABroker(
								state.brokerIdentities.get(uri),
								AlgorithmRouting.ADMIN_ADVERTISEMENT
										.getActionIndex(),
								m);
						state.nbMsgsInTransit++;
						if (ack == ACK.GLOBAL && !state.acksToReceive
								.get(client).get(seqNumber).contains(uri)) {
							state.acksToReceive.get(client).get(seqNumber)
									.add(uri);
						}
					} catch (IOException e) {
						Util.sendNAK(ack, forwarder, client, seqNumber,
								"problem in forwarding the" + " advertisement",
								Level.ERROR);
					}
				}
			}
		}
		if (forwarder == null && Log.ON
				&& Log.ROUTING.isEnabledFor(Level.INFO)) {
			Log.ROUTING.info(
					state.identity.getPath() + ", command result = success");
		}
		if (ack == ACK.LOCAL) {
			Util.sendAcknowledgementToClient(client, seqNumber,
					new CommandResult(CommandStatus.SUCCESS, null));
		} else if (ack == ACK.GLOBAL) {
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
				Log.ROUTING.trace(state.identity.getPath()
						+ ", acksToReceive = " + state.acksToReceive
						+ "\nacksToSendToClients = " + state.acksToSendToBrokers
						+ "\nacksToSendToBrokers = "
						+ state.acksToSendToClients);
			}
			Util.testAndSendAcknowledgement(client, seqNumber,
					new CommandResult(CommandStatus.SUCCESS, null));
		}
	}

	/**
	 * receives a publication with privacy requirement message from a client.
	 * The message is of type <tt>PublicationMsgContent</tt>.
	 * 
	 * @param content
	 *            the message to treat.
	 */
	public static void receivePublication(
			final AbstractMessageContent content) {
		BrokerState state = BrokerState.getInstance();
		PublicationMsgContent mymsg = (PublicationMsgContent) content;
		if (Log.ON && Log.ROUTING.getLevel().equals(Level.INFO)) {
			Log.ROUTING.info(state.identity.getPath() + ", publication = "
					+ mymsg.toStringBrief());
		} else if (Log.ON && Log.ROUTING.isEnabledFor(Level.DEBUG)) {
			Log.ROUTING.debug(
					state.identity.getPath() + ", publication = " + mymsg);
		}
		if (PerfAttributes.performance_evaluation
				&& !mymsg.getIdAdv().contains("startperf")) {
			state.nbPublicationsReceived++;
		}
		if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
			Log.ROUTING.trace(
					state.identity.getPath() + ", nbPublicationsReceived = "
							+ state.nbPublicationsReceived);
		}
		// the privacy requirement is going to be set afterwards
		handlePublicationMessage(mymsg.getPath(), mymsg.getAck(),
				mymsg.getSeqNumberForAck(), mymsg.getIdAdv(), false,
				new ArrayList<String>(), mymsg.getSequenceNumber(), null,
				mymsg.getContent());
	}

	/**
	 * receives a publication with privacy requirement message from a broker.
	 * The message is of type <tt>AdminPublicationMsgContent</tt>.
	 * 
	 * @param content
	 *            the message to treat.
	 */
	public static void receiveAdminPublication(
			final AbstractMessageContent content) {
		BrokerState state = BrokerState.getInstance();
		state.nbMsgsInTransit--;
		AdminPublicationMsgContent mymsg = (AdminPublicationMsgContent) content;
		if (!state.currIdentityType.equals(EntityType.BROKER)) {
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.ERROR)) {
				Log.ROUTING.error(
						state.identity.getPath() + ", the admin publication"
								+ " was sent by a client (" + mymsg + ")");
			}
			return;
		}
		if (Log.ON && Log.ROUTING.getLevel().equals(Level.INFO)) {
			Log.ROUTING.info(state.identity.getPath() + ", admin publication = "
					+ mymsg.toStringBrief());
		} else if (Log.ON && Log.ROUTING.isEnabledFor(Level.DEBUG)) {
			Log.ROUTING.debug(state.identity.getPath()
					+ ", admin publication = " + mymsg);
		}
		if (PerfAttributes.performance_evaluation
				&& !mymsg.getIdAdv().contains("startperf")) {
			state.nbAdminPublicationsReceived++;
		}
		handlePublicationMessage(mymsg.getPath(), mymsg.getAck(),
				mymsg.getSeqNumberForAck(), mymsg.getIdAdv(),
				mymsg.getPrivacyRequirement(), mymsg.getSubsEnabledAC(),
				mymsg.getSequenceNumber(), mymsg.getPhiNotif(),
				mymsg.getContent());
	}

	/**
	 * handles the publication message. At first, if the publication comes from
	 * a local client, it has to be accepted (i.e., it must match the
	 * corresponding advertisement). If the publication is accepted, then it is
	 * memorised as <tt>lastContent</tt> of the advertisement in order to be
	 * provided in response to requests. Then, if the publication has not
	 * already been transmitted (i.e., the sequence number of the publication is
	 * greater than the last one seen), the publication is forwarded to all the
	 * clients and all the brokers that have a matching filter, except to the
	 * clients or brokers that have already sent or forwarded this publication
	 * (i.e., the URI is not in the path of the publication). If required, the
	 * broker sends an acknowledgement.
	 * 
	 * @param state
	 *            the state of the broker.
	 * @param path
	 *            the path of the publication received.
	 * @param ack
	 *            the parameter indicating whether an acknowledgement is
	 *            expected.
	 * @param seqNumberForAck
	 *            the sequence number to be used for acknowledging the call.
	 * @param idAdv
	 *            the identifier of the corresponding advertisement.
	 * @param privacyReq
	 *            the boolean stating whether the publication is sent with
	 *            privacy requirements.
	 * @param subsACEnabled
	 *            the list of subscription filters with access control enabled
	 *            when analysed at the producer's access broker.
	 * @param seqNum
	 *            the sequence number of the publication received.
	 * @param the
	 *            multi-scoping specification of the notification. It is not
	 *            provided by the client with the notification but is going to
	 *            be assigned to the one provided when advertising.
	 * @param content
	 *            the content of the publication to forward.
	 */
	private static void handlePublicationMessage(final List<URI> path,
			final ACK ack, final int seqNumberForAck, final String idAdv,
			final boolean privacyReq, final List<String> subsACEnabled,
			final int seqNum, final MultiScopingSpecification phiNotif,
			final String content) {
		long startTime = System.nanoTime();
		BrokerState state = BrokerState.getInstance();
		DocumentBuilderFactory factory = null;
		if (factory == null) {
			factory = DocumentBuilderFactory.newInstance();
		}
		DocumentBuilder builder = null;
		Document doc = null;
		try {
			if (builder == null) {
				builder = factory.newDocumentBuilder();
			}
			doc = builder.parse(new InputSource(new StringReader(content)));
		} catch (Exception e) {
			if (Log.ON && Log.DISPATCH.isEnabledFor(Level.ERROR)) {
				Log.DISPATCH.error("error when handling publication:" + ", "
						+ e.getMessage() + "\n"
						+ Log.printStackTrace(e.getStackTrace()));
			}
		}
		URI client = path.get(0);
		LinkedHashSet<URI> forwardToClients = new LinkedHashSet<URI>();
		LinkedHashSet<URI> forwardToBrokers = new LinkedHashSet<URI>();
		boolean pr = privacyReq;
		List<URI> newPath = new ArrayList<URI>();
		newPath.addAll(path);
		newPath.add(state.identity);
		MultiScopingSpecification newPhi = phiNotif;
		if (path.size() == 1) {
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
				Log.ROUTING.trace(state.identity.getPath()
						+ ", searching for local adv. " + idAdv);
			}
			if (state.localAdvertisements.containsKey(client)) {
				if (state.localAdvertisements.get(client).containsKey(idAdv)) {
					try {
						AdvertisementTableEntry adv = state.localAdvertisements
								.get(client).get(idAdv);
						newPhi = adv.getPhiNotif();
						pr = (adv.getPolicyContent() == null) ? false : true;
						if (adv.getFilter().evaluate(doc)) {
							if (Log.ON
									&& Log.ROUTING.isEnabledFor(Level.TRACE)) {
								Log.ROUTING.trace(state.identity.getPath()
										+ ", evaluating local adv." + adv);
							}
							adv.setLastContent(content);
							if (adv.getOperationalMode() == OperationalMode.GLOBAL) {
								for (URI uri : state.brokerIdentities
										.keySet()) {
									if (!newPath.contains(uri)) {
										forwardToBrokers.add(uri);
									}
								}
							}
						} else {
							Util.sendNAK(ack, null, client, seqNumberForAck,
									"publication not matching "
											+ " the filter of the "
											+ " advertisement",
									Level.WARN);
							return;
						}
					} catch (UndeclaredThrowableException e) {
						Util.sendNAK(ack, null, client, seqNumberForAck,
								"error in evaluation of the adv. filter",
								Level.ERROR);
						return;
					}
				} else {
					Util.sendNAK(ack, null, client, seqNumberForAck,
							"no matching local advertisement", Level.WARN);
					return;
				}
				if (ack != ACK.NO) {
					Util.sendAcknowledgementToClient(client, seqNumberForAck,
							new CommandResult(CommandStatus.SUCCESS, null));
				}
			} else {
				Util.sendNAK(ack, null, client, seqNumberForAck,
						"no matching advertisement", Level.WARN);
				return;
			}
		}
		try {
			newPhi = Util.computeScopePathsForNotif(newPhi, doc);
		} catch (MuDEBSException e1) {
			e1.printStackTrace();
			return;
		}
		if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
			Log.ROUTING.trace(state.identity.getPath()
					+ ", parsing the client routing tables");
		}
		for (List<RoutingTableEntry> lrte : state.clientRoutingTables
				.values()) {
			for (RoutingTableEntry rte : lrte) {
				try {
					if (rte.getFilter() instanceof SubscriptionFilterWithPrivacy) {
						SubscriptionFilterWithPrivacy sub = (SubscriptionFilterWithPrivacy) rte
								.getFilter();
						sub.setIsLocalAdvertisement(path.size() == 1);
						sub.setHasGotPrivacyRequirement(pr);
						sub.setIsLocalSubscription(rte.getOperationalMode());
						sub.setPublicationPathSize(path.size());
						sub.setAccessControlEnabled(
								subsACEnabled.contains(rte.getIdentifier()));
					} else {
						if (pr) {
							continue;
						}
					}
					if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
						Log.ROUTING.trace(state.identity.getPath()
								+ ", evaluating sub. filter"
								+ rte.getIdentifier());
					}
					if (Util.checkvp(newPhi, rte.getPhi(),
							rte.getBrokerOrClient())
							&& rte.getFilter().evaluate(doc)) {
						forwardToClients.add(rte.getBrokerOrClient());
					}
				} catch (UndeclaredThrowableException e) {
					if (Log.ON && Log.ROUTING.isEnabledFor(Level.ERROR)) {
						Log.ROUTING.error(state.identity.getPath()
								+ ", error in evaluation of filter: " + rte
								+ "\n with content: " + content
								+ "\n and with XML doc:" + doc + "\n"
								+ e.getMessage() + "\n"
								+ Log.printStackTrace(e.getStackTrace()));
					}
					return;
				}
			}
		}
		if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
			Log.ROUTING.trace(state.identity.getPath() + ", publication message"
					+ " matching filters for clients: " + forwardToClients);
		}
		for (HashMap<URI, HashMap<String, AdvertisementTableEntry>> badvs : state.remoteAdvertisements
				.values()) {
			for (HashMap<String, AdvertisementTableEntry> advs : badvs
					.values()) {
				if (advs.containsKey(idAdv)) {
					advs.get(idAdv).setLastContent(content);
					for (URI uri : state.brokerIdentities.keySet()) {
						if (!newPath.contains(uri)
								&& !forwardToBrokers.contains(uri)) {
							// TODO: checkvp?
							forwardToBrokers.add(uri);
						}
					}
					continue;
				}
			}
		}
		if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
			Log.ROUTING.trace(state.identity.getPath()
					+ ", parsing the broker routing tables");
		}
		for (List<RoutingTableEntry> lrte : state.brokerRoutingTables
				.values()) {
			for (RoutingTableEntry rte : lrte) {
				if (!newPath.contains(rte.getBrokerOrClient())) {
					if (rte.getFilter() instanceof SubscriptionFilterWithPrivacy) {
						SubscriptionFilterWithPrivacy sub = (SubscriptionFilterWithPrivacy) rte
								.getFilter();
						sub.setIsLocalAdvertisement((path.size() == 1));
						sub.setHasGotPrivacyRequirement(pr);
						sub.setIsLocalSubscription(rte.getOperationalMode());
						sub.setPublicationPathSize(path.size());
						sub.setAccessControlEnabled(
								subsACEnabled.contains(rte.getIdentifier()));
					} else {
						if (pr) {
							continue;
						}
					}
					try {
						if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
							Log.ROUTING.trace(state.identity.getPath()
									+ ", evaluating sub. filter"
									+ rte.getIdentifier());
						}
						if (Util.checkvp(newPhi, rte.getPhi(),
								rte.getBrokerOrClient())
								&& rte.getFilter().evaluate(doc)) {
							forwardToBrokers.add(rte.getBrokerOrClient());
							if (pr) {
								if ((rte.getFilter() instanceof SubscriptionFilterWithPrivacy)
										&& !subsACEnabled.contains(
												rte.getIdentifier())) {
									subsACEnabled.add(rte.getIdentifier());
								}
							}
						}
					} catch (UndeclaredThrowableException e) {
						if (Log.ON && Log.ROUTING.isEnabledFor(Level.ERROR)) {
							Log.ROUTING.error(state.identity.getPath()
									+ ", error in evaluation" + " of filter: "
									+ rte + "\n with content: " + doc + "\n"
									+ e.getMessage() + "\n"
									+ Log.printStackTrace(e.getStackTrace()));
						}
						return;
					}
				}
			}
		}
		Long endTime = System.nanoTime();
		double duration = (double) (endTime - startTime);
		if (PerfAttributes.performance_evaluation
				&& !idAdv.contains("startperf")) {
			state.listOfExecutionTime.add(duration);
		}
		PublicationMsgContent m = new PublicationMsgContent(newPath, ack,
				seqNumberForAck, idAdv, seqNum, content);
		AdminPublicationMsgContent mb = new AdminPublicationMsgContent(newPath,
				ack, seqNumberForAck, idAdv, pr, seqNum, newPhi, content,
				subsACEnabled);
		for (URI uri : forwardToClients) {
			try {
				state.broker.sendToAClient(state.clientIdentities.get(uri),
						AlgorithmRouting.PUBLICATION.getActionIndex(), m);
				if (PerfAttributes.performance_evaluation
						&& !idAdv.contains("startperf")) {
					state.nbPublicationsDelivered += 1;
					state.nbBrokersInvolvedInRouting += (newPath.size() - 1);
				}
				if (Log.ON && Log.ROUTING.isEnabledFor(Level.DEBUG)) {
					Log.ROUTING.debug(
							state.identity.getPath() + ", publication message"
									+ " forwarded to client " + uri);
				}
			} catch (IOException e) {
				if (Log.ON && Log.ROUTING.isEnabledFor(Level.ERROR)) {
					Log.ROUTING
							.error(state.identity.getPath() + ", cannot forward"
									+ " publication message to client " + uri);
				}
				if (ack != ACK.NO && path.size() == 1) {
					Util.sendNAK(ack, null, client, seqNumberForAck,
							"problem in forwarding publication", Level.ERROR);
				}
			}
		}
		for (URI uri : forwardToBrokers) {
			try {
				state.broker.sendToABroker(state.brokerIdentities.get(uri),
						AlgorithmRouting.ADMIN_PUBLICATION.getActionIndex(),
						mb);
				state.nbMsgsInTransit++;
				if (Log.ON && Log.ROUTING.isEnabledFor(Level.DEBUG)) {
					Log.ROUTING.debug(
							state.identity.getPath() + ", publication message"
									+ " forwarded to broker " + uri);
				}
			} catch (IOException e) {
				if (ack != ACK.NO && path.size() == 1) {
					Util.sendNAK(ack, null, client, seqNumberForAck,
							"problem in forwarding publication", Level.ERROR);
				}
			}
		}
		if (path.size() == 1 && Log.ON
				&& Log.ROUTING.isEnabledFor(Level.INFO)) {
			Log.ROUTING.info(
					state.identity.getPath() + ", command result = success");
		}
		if (path.size() == 1 && ack != ACK.NO) {
			Util.sendAcknowledgementToClient(client, seqNumberForAck,
					new CommandResult(CommandStatus.SUCCESS, null));
		}
	}

	/**
	 * receives a collective publication with privacy requirement message from a
	 * client. The message is of type <tt>CollectivePublicationMsgContent</tt>.
	 * 
	 * @param content
	 *            the message to treat.
	 */
	public static void receiveCollectivePublication(
			final AbstractMessageContent content) {
		BrokerState state = BrokerState.getInstance();
		CollectivePublicationMsgContent mymsg = (CollectivePublicationMsgContent) content;
		if (Log.ON && Log.ROUTING.getLevel().equals(Level.INFO)) {
			Log.ROUTING.info(state.identity.getPath()
					+ ", collective publication = " + mymsg.toStringBrief());
		} else if (Log.ON && Log.ROUTING.isEnabledFor(Level.DEBUG)) {
			Log.ROUTING.debug(state.identity.getPath()
					+ ", collective publication = " + mymsg);
		}
		if (PerfAttributes.performance_evaluation
				&& mymsg.getPublications() != null
				&& mymsg.getPublications().size() != 0
				&& !mymsg.getPublications().get(0).getIdAdv()
						.contains("startperf")) {
			state.nbPublicationsReceived += mymsg.getPublications().size();
		}
		if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
			Log.ROUTING.trace(
					state.identity.getPath() + ", nbPublicationsReceived = "
							+ state.nbPublicationsReceived);
		}
		// the privacy requirement is going to be set afterwards
		handleCollectivePublicationMessage(mymsg.getPath(), mymsg.getAck(),
				mymsg.getSeqNumberForAck(), mymsg.getPublications());
	}

	/**
	 * receives a collective publication with privacy requirement message from a
	 * broker. The message is of type
	 * <tt>CollectiveAdminPublicationMsgContent</tt>.
	 * 
	 * @param content
	 *            the message to treat.
	 */
	public static void receiveCollectiveAdminPublication(
			final AbstractMessageContent content) {
		BrokerState state = BrokerState.getInstance();
		state.nbMsgsInTransit--;
		CollectiveAdminPublicationMsgContent mymsg = (CollectiveAdminPublicationMsgContent) content;
		if (!state.currIdentityType.equals(EntityType.BROKER)) {
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.ERROR)) {
				Log.ROUTING.error(state.identity.getPath()
						+ ", the collective admin publication"
						+ " was sent by a client (" + mymsg + ")");
			}
			return;
		}
		if (Log.ON && Log.ROUTING.getLevel().equals(Level.INFO)) {
			Log.ROUTING.info(state.identity.getPath()
					+ ", collective admin publication = "
					+ mymsg.toStringBrief());
		} else if (Log.ON && Log.ROUTING.isEnabledFor(Level.DEBUG)) {
			Log.ROUTING.debug(state.identity.getPath()
					+ ", collective admin publication = " + mymsg);
		}
		if (PerfAttributes.performance_evaluation) {
			for (AdminPublication pub : mymsg.getPublications()) {
				if (pub != null && !pub.getIdAdv().contains("startperf")) {
					state.nbAdminPublicationsReceived++;
				}
			}
		}
		handleCollectivePublicationMessage(mymsg.getPath(), mymsg.getAck(),
				mymsg.getSeqNumberForAck(), mymsg.getPublications());
	}

	/**
	 * handles the collective publication message. At first, if a publication
	 * comes from a local client, it has to be accepted (i.e., it must match the
	 * corresponding advertisement). If the publication is accepted, then it is
	 * memorised as <tt>lastContent</tt> of the advertisement in order to be
	 * provided in response to requests. Then, if the publication has not
	 * already been transmitted (i.e., the sequence number of the publication is
	 * greater than the last one seen), the publication is forwarded to all the
	 * clients and all the brokers that have a matching filter, except to the
	 * clients or brokers that have already sent or forwarded this publication
	 * (i.e., the URI is not in the path of the publication). If required, the
	 * broker sends an acknowledgement.
	 * 
	 * @param state
	 *            the state of the broker.
	 * @param path
	 *            the path of the publication received.
	 * @param ack
	 *            the parameter indicating whether an acknowledgement is
	 *            expected.
	 * @param seqNumberForAck
	 *            the sequence number to be used for acknowledging the call.
	 * @param pubs
	 *            the set of publications.
	 */
	private static void handleCollectivePublicationMessage(final List<URI> path,
			final ACK ack, final int seqNumberForAck,
			final List<? extends AdminPublication> pubs) {
		BrokerState state = BrokerState.getInstance();
		List<URI> newPath = new ArrayList<URI>();
		newPath.addAll(path);
		newPath.add(state.identity);
		URI client = path.get(0);
		Map<URI, List<Publication>> forwardToClients = new HashMap<URI, List<Publication>>();
		Map<URI, List<AdminPublication>> forwardToBrokers = new HashMap<URI, List<AdminPublication>>();
		for (AdminPublication pub : pubs) {
			Document doc = null;
			try {
				doc = state.builder.parse(
						new InputSource(new StringReader(pub.getContent())));
			} catch (Exception e) {
				if (Log.ON && Log.DISPATCH.isEnabledFor(Level.ERROR)) {
					Log.DISPATCH.error("error when handling publication:" + ", "
							+ e.getMessage() + "\n"
							+ Log.printStackTrace(e.getStackTrace()));
				}
			}
			boolean hasPR = pub.hasGotPrivacyRequirement();
			MultiScopingSpecification newPhi = pub.getPhiNotif();
			if (path.size() == 1) {
				if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
					Log.ROUTING.trace(state.identity.getPath()
							+ ", searching for local adv. " + pub.getIdAdv());
				}
				if (state.localAdvertisements.containsKey(client)) {
					if (state.localAdvertisements.get(client)
							.containsKey(pub.getIdAdv())) {
						try {
							AdvertisementTableEntry adv = state.localAdvertisements
									.get(client).get(pub.getIdAdv());
							newPhi = adv.getPhiNotif();
							hasPR = (adv.getPolicyContent() == null) ? false
									: true;
							if (adv.getFilter().evaluate(doc)) {
								if (Log.ON && Log.ROUTING
										.isEnabledFor(Level.TRACE)) {
									Log.ROUTING.trace(state.identity.getPath()
											+ ", evaluating local adv." + adv);
								}
								adv.setLastContent(pub.getContent());
								// TODO
								// if (adv.getOperationalMode() ==
								// OperationalMode.GLOBAL) {
								// for (URI uri : state.brokerIdentities
								// .keySet()) {
								// if (!newPath.contains(uri)) {
								// List<AdminPublication> in = forwardToBrokers
								// .get(uri);
								// if (in == null) {
								// in = new ArrayList<AdminPublication>();
								// forwardToBrokers.put(uri, in);
								// }
								// if (!in.contains(pub)) {
								// in.add(pub);
								// }
								// }
								// }
								// }
							} else {
								Util.sendNAK(ack, null, client, seqNumberForAck,
										"publication not matching "
												+ " the filter of the "
												+ " advertisement "
												+ pub.getIdAdv(),
										Level.WARN);
								return;
							}
						} catch (UndeclaredThrowableException e) {
							Util.sendNAK(ack, null, client, seqNumberForAck,
									"error in evaluation of the adv. filter"
											+ pub.getIdAdv(),
									Level.ERROR);
							return;
						}
					} else {
						Util.sendNAK(ack, null, client, seqNumberForAck,
								"no matching local advertisement "
										+ pub.getIdAdv(),
								Level.WARN);
						return;
					}
				} else {
					Util.sendNAK(ack, null, client, seqNumberForAck,
							"no matching advertisement", Level.WARN);
					return;
				}
			}
			try {
				newPhi = Util.computeScopePathsForNotif(newPhi, doc);
			} catch (MuDEBSException e1) {
				e1.printStackTrace();
				return;
			}
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
				Log.ROUTING.trace(state.identity.getPath()
						+ ", parsing the client routing tables");
			}
			for (List<RoutingTableEntry> lrte : state.clientRoutingTables
					.values()) {
				for (RoutingTableEntry rte : lrte) {
					try {
						if (rte.getFilter() instanceof SubscriptionFilterWithPrivacy) {
							SubscriptionFilterWithPrivacy sub = (SubscriptionFilterWithPrivacy) rte
									.getFilter();
							sub.setIsLocalAdvertisement(path.size() == 1);
							sub.setHasGotPrivacyRequirement(hasPR);
							sub.setIsLocalSubscription(
									rte.getOperationalMode());
							sub.setPublicationPathSize(path.size());
							sub.setAccessControlEnabled(pub.getSubsEnabledAC()
									.contains(rte.getIdentifier()));
						} else {
							if (hasPR) {
								continue;
							}
						}
						if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
							Log.ROUTING.trace(state.identity.getPath()
									+ ", evaluating sub. filter"
									+ rte.getIdentifier());
						}
						if (Util.checkvp(newPhi, rte.getPhi(),
								rte.getBrokerOrClient())
								&& rte.getFilter().evaluate(doc)) {
							List<Publication> in = forwardToClients
									.get(rte.getBrokerOrClient());
							if (in == null) {
								in = new ArrayList<Publication>();
								forwardToClients.put(rte.getBrokerOrClient(),
										in);
							}
							Publication newPub = new Publication(pub.getIdAdv(),
									pub.getSequenceNumber(), pub.getContent());
							if (!in.contains(newPub)) {
								in.add(newPub);
							}
						}
					} catch (UndeclaredThrowableException e) {
						if (Log.ON && Log.ROUTING.isEnabledFor(Level.ERROR)) {
							Log.ROUTING.error(state.identity.getPath()
									+ ", error in evaluation of filter: " + rte
									+ "\n with content: " + pub.getContent()
									+ "\n and with XML doc:" + doc + "\n"
									+ e.getMessage() + "\n"
									+ Log.printStackTrace(e.getStackTrace()));
						}
						return;
					}
				}
			}
			// TODO
			/*
			 * for (HashMap<URI, HashMap<String, AdvertisementTableEntry>> badvs
			 * : state.remoteAdvertisements .values()) { for (HashMap<String,
			 * AdvertisementTableEntry> advs : badvs .values()) { if
			 * (advs.containsKey(pub.getIdAdv())) {
			 * advs.get(pub.getIdAdv()).setLastContent( pub.getContent()); for
			 * (URI uri : state.brokerIdentities.keySet()) { if
			 * (!newPath.contains(uri) && forwardToBrokers.get(uri) == null) {
			 * // TODO: checkvp? List<AdminPublication> in = forwardToBrokers
			 * .get(uri); if (in == null) { in = new
			 * ArrayList<AdminPublication>(); forwardToBrokers.put(uri, in); }
			 * if (!in.contains(pub)) { AdminPublication newPub = new
			 * AdminPublication( pub.getIdAdv(), pub.getPrivacyRequirement(),
			 * pub.getSequenceNumber(), newPhi, pub.getContent(),
			 * pub.getSubsEnabledAC()); in.add(newPub); } } } continue; } } }
			 */
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
				Log.ROUTING.trace(state.identity.getPath()
						+ ", parsing the broker routing tables");
			}
			for (List<RoutingTableEntry> lrte : state.brokerRoutingTables
					.values()) {
				for (RoutingTableEntry rte : lrte) {
					if (!newPath.contains(rte.getBrokerOrClient())) {
						if (rte.getFilter() instanceof SubscriptionFilterWithPrivacy) {
							SubscriptionFilterWithPrivacy sub = (SubscriptionFilterWithPrivacy) rte
									.getFilter();
							sub.setIsLocalAdvertisement((path.size() == 1));
							sub.setHasGotPrivacyRequirement(hasPR);
							sub.setIsLocalSubscription(
									rte.getOperationalMode());
							sub.setPublicationPathSize(path.size());
							sub.setAccessControlEnabled(pub.getSubsEnabledAC()
									.contains(rte.getIdentifier()));
						} else {
							if (hasPR) {
								continue;
							}
						}
						try {
							if (Log.ON
									&& Log.ROUTING.isEnabledFor(Level.TRACE)) {
								Log.ROUTING.trace(state.identity.getPath()
										+ ", evaluating sub. filter"
										+ rte.getIdentifier());
							}
							if (Util.checkvp(newPhi, rte.getPhi(),
									rte.getBrokerOrClient())
									&& rte.getFilter().evaluate(doc)) {
								List<AdminPublication> in = forwardToBrokers
										.get(rte.getBrokerOrClient());
								if (in == null) {
									in = new ArrayList<AdminPublication>();
									forwardToBrokers
											.put(rte.getBrokerOrClient(), in);
								}
								AdminPublication newPub = new AdminPublication(
										pub.getIdAdv(),
										pub.hasGotPrivacyRequirement(),
										pub.getSequenceNumber(), newPhi,
										pub.getContent(),
										pub.getSubsEnabledAC());
								if (!in.contains(newPub)) {
									in.add(newPub);
								}
								if (hasPR) {
									if ((rte.getFilter() instanceof SubscriptionFilterWithPrivacy)
											&& !pub.getSubsEnabledAC().contains(
													rte.getIdentifier())) {
										newPub.getSubsEnabledAC()
												.add(rte.getIdentifier());
									}
								}
							}
						} catch (UndeclaredThrowableException e) {
							if (Log.ON
									&& Log.ROUTING.isEnabledFor(Level.ERROR)) {
								Log.ROUTING.error(state.identity.getPath()
										+ ", error in evaluation"
										+ " of filter: " + rte
										+ "\n with content: " + doc + "\n"
										+ e.getMessage() + "\n"
										+ Log.printStackTrace(
												e.getStackTrace()));
							}
							return;
						}
					}
				}
			}
		}
		if (ack != ACK.NO) {
			Util.sendAcknowledgementToClient(client, seqNumberForAck,
					new CommandResult(CommandStatus.SUCCESS, null));
		}
		if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
			Log.ROUTING.trace(state.identity.getPath()
					+ ", collective publication message"
					+ " matching filters for clients: "
					+ forwardToClients.keySet());
		}
		for (URI uri : forwardToClients.keySet()) {
			CollectivePublicationMsgContent m = new CollectivePublicationMsgContent(
					newPath, ack, seqNumberForAck, forwardToClients.get(uri));
			try {
				state.broker.sendToAClient(state.clientIdentities.get(uri),
						AlgorithmRouting.COLLECTIVE_PUBLICATION
								.getActionIndex(),
						m);
				if (PerfAttributes.performance_evaluation
						&& m.getPublications() != null
						&& m.getPublications().size() != 0
						&& !m.getPublications().get(0).getIdAdv()
								.contains("startperf")) {
					state.nbPublicationsDelivered += m.getPublications().size();
					state.nbBrokersInvolvedInRouting += (newPath.size() - 1)
							* m.getPublications().size();
				}
				if (Log.ON && Log.ROUTING.isEnabledFor(Level.DEBUG)) {
					Log.ROUTING.debug(state.identity.getPath()
							+ ", collective publication message"
							+ " forwarded to client " + uri);
				}
			} catch (IOException e) {
				if (Log.ON && Log.ROUTING.isEnabledFor(Level.ERROR)) {
					Log.ROUTING
							.error(state.identity.getPath() + ", cannot forward"
									+ " collective publication message to client "
									+ uri);
				}
				if (ack != ACK.NO && path.size() == 1) {
					Util.sendNAK(ack, null, client, seqNumberForAck,
							"problem in forwarding collective publication",
							Level.ERROR);
				}
			}
		}
		if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
			Log.ROUTING.trace(state.identity.getPath()
					+ ", collective publication message"
					+ " matching filters for brokers: "
					+ forwardToBrokers.keySet());
		}
		for (URI uri : forwardToBrokers.keySet()) {
			CollectiveAdminPublicationMsgContent m = new CollectiveAdminPublicationMsgContent(
					newPath, ack, seqNumberForAck, forwardToBrokers.get(uri));
			try {
				state.broker.sendToABroker(state.brokerIdentities.get(uri),
						AlgorithmRouting.COLLECTIVE_ADMIN_PUBLICATION
								.getActionIndex(),
						m);
				state.nbMsgsInTransit++;
				if (Log.ON && Log.ROUTING.isEnabledFor(Level.DEBUG)) {
					Log.ROUTING.debug(state.identity.getPath()
							+ ", collective publication message"
							+ " forwarded to broker " + uri);
				}
			} catch (IOException e) {
				if (ack != ACK.NO && path.size() == 1) {
					Util.sendNAK(ack, null, client, seqNumberForAck,
							"problem in collective forwarding publication",
							Level.ERROR);
				}
			}
		}
		if (path.size() == 1 && Log.ON
				&& Log.ROUTING.isEnabledFor(Level.INFO)) {
			Log.ROUTING.info(
					state.identity.getPath() + ", command result = success");
		}
		if (path.size() == 1 && ack != ACK.NO) {
			Util.sendAcknowledgementToClient(client, seqNumberForAck,
					new CommandResult(CommandStatus.SUCCESS, null));
		}
	}

	/**
	 * receives a request message from a client. The message is of type
	 * <tt>RequestMsgContent</tt>.
	 * 
	 * @param content
	 *            the message to treat.
	 */
	public static void receiveRequest(final AbstractMessageContent content) {
		BrokerState state = BrokerState.getInstance();
		RequestMsgContent mymsg = (RequestMsgContent) content;
		if (!state.currIdentityType.equals(EntityType.CLIENT)) {
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.ERROR)) {
				Log.ROUTING.error(state.identity.getPath()
						+ ", the request was not sent by a client (" + mymsg
						+ ")");
			}
			return;
		}
		if (Log.ON && Log.ROUTING.getLevel().equals(Level.INFO)) {
			Log.ROUTING.info(state.identity.getPath() + ", request = "
					+ mymsg.toStringBrief());
		} else if (Log.ON && Log.ROUTING.isEnabledFor(Level.DEBUG)) {
			Log.ROUTING
					.debug(state.identity.getPath() + ", request = " + mymsg);
		}
		MultiScopingSpecification phi = null;
		if (mymsg.getPhi() == null) {
			phi = MultiScopingSpecification.create(new ScopeTop(),
					ScopePathStatus.UP);
		} else {
			phi = mymsg.getPhi();
		}
		List<URI> path = new ArrayList<URI>();
		path.add(mymsg.getRequester());
		for (URI dimension : phi.getTheSets().keySet()) {
			SetOfScopePathsWithStatus ssp = phi.getTheSets().get(dimension);
			if (ssp.getTheSet().size() == 0) {
				Util.sendNAK(ACK.GLOBAL, null, mymsg.getRequester(),
						mymsg.getSequenceNumber(),
						"no scope path for dimension = " + dimension,
						Level.WARN);
				return;
			} else if (ssp.getTheSet().size() > 1) {
				Util.sendNAK(ACK.GLOBAL, null, mymsg.getRequester(),
						mymsg.getSequenceNumber(),
						"more than one scope path for dimension = " + dimension
								+ "(" + ssp.getTheSet() + ")",
						Level.WARN);
				return;
			} else {
				ScopePathWithStatus sp = ssp.getTheSet().get(0);
				if (sp.scopeSequence().size() == 0) {
					Util.sendNAK(ACK.GLOBAL, null, mymsg.getRequester(),
							mymsg.getSequenceNumber(),
							"no scope in scope path of dimension = "
									+ dimension,
							Level.WARN);
					return;
				} else if (sp.scopeSequence().size() > 1) {
					Util.sendNAK(ACK.GLOBAL, null, mymsg.getRequester(),
							mymsg.getSequenceNumber(),
							"more than one scope in scope path of dimension = "
									+ dimension,
							Level.WARN);
					return;
				} else {
					Scope scope = sp.scopeSequence().get(0);
					List<URI> pathJoinScope = new ArrayList<URI>();
					pathJoinScope.add(state.identity);
					if (scope.identifier().equals(ScopeBottom.IDENTIFIER)) {
						Util.sendNAK(ACK.GLOBAL, null, mymsg.getRequester(),
								mymsg.getSequenceNumber(),
								"scope bottom not allowed in request",
								Level.WARN);
						return;
					} else {
						if (state.scopeGraphs.get(dimension) == null) {
							handleJoinScopeMessage(true, pathJoinScope,
									mymsg.getSequenceNumber(), dimension,
									scope.identifier(), ScopeTop.IDENTIFIER,
									JavaScriptVisibilityFilter.ALWAYS_TRUE,
									JavaScriptVisibilityFilter.ALWAYS_FALSE);
						} else if (!state.scopeGraphs.get(dimension)
								.existsNode(scope)) {
							handleJoinScopeMessage(true, pathJoinScope,
									mymsg.getSequenceNumber(), dimension,
									scope.identifier(), ScopeTop.IDENTIFIER,
									JavaScriptVisibilityFilter.ALWAYS_TRUE,
									JavaScriptVisibilityFilter.ALWAYS_FALSE);
						}
					}
				}
			}
		}
		handleRequest(mymsg.getRequester(), null, new ArrayList<URI>(),
				mymsg.getLocal(), mymsg.getSequenceNumber(), mymsg.getFilter(),
				phi, mymsg.getABACInformation());
	}

	/**
	 * receives a request message from a broker. The message is of type
	 * <tt>AdminRequestMsgContent</tt>.
	 * 
	 * @param content
	 *            the message to treat.
	 */
	public static void receiveAdminRequest(
			final AbstractMessageContent content) {
		BrokerState state = BrokerState.getInstance();
		state.nbMsgsInTransit--;
		AdminRequestMsgContent mymsg = (AdminRequestMsgContent) content;
		if (!state.currIdentityType.equals(EntityType.BROKER)) {
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.ERROR)) {
				Log.ROUTING
						.error(state.identity.getPath() + ", the admin request"
								+ " was sent by a client (" + mymsg + ")");
			}
			return;
		}
		if (Log.ON && Log.ROUTING.getLevel().equals(Level.INFO)) {
			Log.ROUTING.info(state.identity.getPath() + ", admin request = "
					+ mymsg.toStringBrief());
		} else if (Log.ON && Log.ROUTING.isEnabledFor(Level.DEBUG)) {
			Log.ROUTING.debug(
					state.identity.getPath() + ", admin request = " + mymsg);
		}
		handleRequest(mymsg.getPath().get(0),
				mymsg.getPath().get(mymsg.getPath().size() - 1),
				mymsg.getPath(), mymsg.getLocal(), mymsg.getSequenceNumber(),
				mymsg.getFilter(), mymsg.getPhi(), mymsg.getABACInformation());
	}

	/**
	 * handles the request message. The request is uniquely identified with the
	 * URI of the client and the sequence number of the request. Before
	 * analysing the filter, ABAC information is used as a privacy guarantee if
	 * the advertisement contains privacy requirements. If the access control is
	 * enabled, then the filter is analysed. The filter is a JavaScript, which
	 * is evaluated in the scripting engine. Then, the last content of the local
	 * advertisements are parsed with the filter. If the content matches the
	 * filter, then the reply is memorised. The request is either local or
	 * global. For a local request, the set of remote advertisements, which
	 * correspond to global advertisements, are also analysed. A global request
	 * is forwarded to all the neighbouring brokers, except <tt>forwarder</tt>.
	 * The broker sends the replies as an acknowledgement.
	 * 
	 * @param client
	 *            the URI of the client, which originally made the call.
	 * @param forwarder
	 *            the URI of the broker that forwards the request. It equals
	 *            <tt>null</tt> when the request comes from a client.
	 * @param path
	 *            the path of the advertisement received.
	 * @param local
	 *            the operational mode (local or global).
	 * @param seqNumber
	 *            the sequence number of the call to use for acknowledging.
	 * @param filter
	 *            the filter of the request.
	 * @param phi
	 *            the set of scopes associated with filter of the request.
	 * @param abacInfo
	 *            the ABAC information to have access to publications with
	 *            privacy requirements.
	 */
	private static void handleRequest(final URI client, final URI forwarder,
			final List<URI> path, final OperationalMode local,
			final int seqNumber, final String filter,
			final MultiScopingSpecification phi,
			final ABACInformation abacInfo) {
		BrokerState state = BrokerState.getInstance();
		Filter mf = null;
		if (PerfAttributes.performance_evaluation) {
			mf = state.subscriptionFiltersForPerf.get(filter);
		}
		if (mf == null) {
			try {
				if (filter.equals(state.returnTrueFilter)) {
					mf = FilterReturnTrue.getInstance();
				} else if (filter.equals(state.returnFalseFilter)) {
					mf = FilterReturnFalse.getInstance();
				} else {
					mf = Util.createJavaScriptFilter(filter,
							JavaScriptRoutingFilter.class);
					if (PerfAttributes.performance_evaluation) {
						state.subscriptionFiltersForPerf.put(filter, mf);
					}
				}
			} catch (ScriptException e) {
				Util.sendNAK(ACK.GLOBAL, forwarder, client, seqNumber,
						"javascript engine problem for request = " + filter,
						Level.ERROR);
				return;
			}
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
				Log.ROUTING
						.trace(state.identity.getPath() + ", inv.getInterface"
								+ "(MonolithicFilter.class) returned " + mf);
			}
		}
		state.nbRequestsReceived++;
		state.createEntryRequestReplies(client, seqNumber);
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document doc = null;
		XACMLPrivacyFilter xacmlPF = null;
		if (abacInfo != null) {
			xacmlPF = new XACMLPrivacyFilter(state.xacmlPEP, abacInfo,
					OperationalMode.GLOBAL);
		}
		for (HashMap<String, AdvertisementTableEntry> entry : state.localAdvertisements
				.values()) {
			for (AdvertisementTableEntry adv : entry.values()) {
				if (adv.getLastContent() != null) {
					if (adv.getPolicyContent() != null) {
						if (abacInfo == null) {
							continue;
						} else {
							if (!xacmlPF.evaluate(null)) {
								continue;
							}
						}
					}
					try {
						builder = factory.newDocumentBuilder();
						doc = builder.parse(new InputSource(
								new StringReader(adv.getLastContent())));
					} catch (Exception e) {
						if (Log.ON && Log.DISPATCH.isEnabledFor(Level.ERROR)) {
							Log.DISPATCH.error("error when handling reply:"
									+ ", " + e.getMessage() + "\n"
									+ e.getMessage() + "\n"
									+ Log.printStackTrace(e.getStackTrace()));
						}
						continue;
					}
					try {
						if (mf.evaluate(doc)) {
							state.requestReplies.get(client).get(seqNumber)
									.add(new Reply(CommandStatus.SUCCESS,
											adv.getLastContent(),
											adv.getClient()));
						}
					} catch (UndeclaredThrowableException e) {
						if (Log.ON && Log.ROUTING.isEnabledFor(Level.ERROR)) {
							Log.ROUTING.error(state.identity.getPath()
									+ ", error in evaluation"
									+ " of request filter: " + filter
									+ "\n with content: " + adv.getLastContent()
									+ "\n and with XML doc:" + doc + "\n"
									+ e.getMessage() + "\n"
									+ Log.printStackTrace(e.getStackTrace()));
						}
					}
				}
			}
		}
		if (local.equals(OperationalMode.LOCAL)) {
			for (HashMap<URI, HashMap<String, AdvertisementTableEntry>> entries : state.remoteAdvertisements
					.values()) {
				for (HashMap<String, AdvertisementTableEntry> entry : entries
						.values()) {
					for (AdvertisementTableEntry adv : entry.values()) {
						if (adv.getLastContent() != null) {
							if (adv.getPolicyContent() != null) {
								if (abacInfo == null) {
									continue;
								} else {
									if (!xacmlPF.evaluate(null)) {
										continue;
									}
								}
							}
							try {
								builder = factory.newDocumentBuilder();
								doc = builder
										.parse(new InputSource(new StringReader(
												adv.getLastContent())));
							} catch (Exception e) {
								if (Log.ON && Log.DISPATCH
										.isEnabledFor(Level.ERROR)) {
									Log.DISPATCH.error(
											"error when handling reply:" + ", "
													+ e.getMessage() + "\n"
													+ e.getMessage() + "\n"
													+ Log.printStackTrace(
															e.getStackTrace()));
								}
								continue;
							}
							try {
								if (mf.evaluate(doc)) {
									Reply reply = new Reply(
											CommandStatus.SUCCESS,
											adv.getLastContent(),
											adv.getClient());
									if (!state.requestReplies.get(client)
											.get(seqNumber).contains(reply)) {
										state.requestReplies.get(client)
												.get(seqNumber).add(reply);
									}
								}
							} catch (UndeclaredThrowableException e) {
								if (Log.ON && Log.ROUTING
										.isEnabledFor(Level.ERROR)) {
									Log.ROUTING.error(state.identity.getPath()
											+ ", error in evaluation"
											+ " of request filter: " + filter
											+ "\n with content: "
											+ adv.getLastContent()
											+ "\n and with XML doc:" + doc
											+ "\n" + e.getMessage() + "\n"
											+ Log.printStackTrace(
													e.getStackTrace()));
								}
							}
						}
					}
				}
			}
			Util.sendRepliesToClient(client, seqNumber);
		} else {
			state.createEntryAcksToReceive(client, seqNumber);
			if (state.acksToReceive.get(client).get(seqNumber)
					.contains(forwarder)) {
				state.acksToReceive.get(client).get(seqNumber)
						.remove(forwarder);
				if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
					Log.ROUTING.trace(state.identity.getPath()
							+ ", admin request serving " + "as an ack for "
							+ forwarder + " and " + client
							+ " with seq. number " + seqNumber);
				}
			}
			if (forwarder == null) {
				state.createEntryAcksToSendToClients(client, seqNumber);
			} else {
				state.createEntryAcksToSendToBrokers(client, seqNumber,
						forwarder);
			}
			if (forwarder == null) {
				path.add(client);
			}
			path.add(state.identity);
			AdminRequestMsgContent m = new AdminRequestMsgContent(path, local,
					seqNumber, filter, phi, abacInfo);
			for (URI uri : state.brokerIdentities.keySet()) {
				if (!path.contains(uri)) {
					try {
						state.broker.sendToABroker(
								state.brokerIdentities.get(uri),
								AlgorithmRouting.ADMIN_REQUEST.getActionIndex(),
								m);
						state.nbMsgsInTransit++;
						if (!state.acksToReceive.get(client).get(seqNumber)
								.contains(uri)) {
							state.acksToReceive.get(client).get(seqNumber)
									.add(uri);
						}
					} catch (IOException e) {
						if (Log.ON && Log.ROUTING.isEnabledFor(Level.ERROR)) {
							Log.ROUTING.error(state.identity.getPath() + ", "
									+ e.getMessage() + "\n"
									+ Log.printStackTrace(e.getStackTrace()));
						}
						state.requestReplies.get(client).get(seqNumber)
								.add(new Reply(CommandStatus.ERROR,
										"problem in forwarding the"
												+ " advertisement",
										state.identity));
					}
				}
			}
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
				Log.ROUTING.trace(state.identity.getPath()
						+ ", acksToReceive = " + state.acksToReceive
						+ "\nacksToSendToClients = " + state.acksToSendToBrokers
						+ "\nacksToSendToBrokers = " + state.acksToSendToClients
						+ "\nrequestReplies = " + state.requestReplies);
			}
			Util.testAndSendReplies(client, seqNumber);
		}
	}

	/**
	 * receives a reply message from a broker. The message is of type
	 * <tt>AdminReplyMsgContent</tt>.
	 * 
	 * @param content
	 *            the message to treat.
	 */
	public static void receiveAdminReply(final AbstractMessageContent content) {
		BrokerState bstate = BrokerState.getInstance();
		bstate.nbMsgsInTransit--;
		AdminReplyMsgContent mymsg = (AdminReplyMsgContent) content;
		if (Log.ON && Log.ROUTING.getLevel().equals(Level.INFO)) {
			Log.ROUTING.info(bstate.identity.getPath() + ", admin. reply = "
					+ mymsg.toStringBrief());
		} else if (Log.ON && Log.ROUTING.isEnabledFor(Level.DEBUG)) {
			Log.ROUTING.debug(
					bstate.identity.getPath() + ", admin. reply = " + mymsg);
		}
		HashMap<Integer, List<URI>> acksToR = bstate.acksToReceive
				.get(mymsg.getClient());
		if (acksToR == null) {
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.DEBUG)) {
				Log.ROUTING.debug(bstate.identity.getPath()
						+ ", reply for unknown client = " + mymsg.getClient());
			}
			return;
		}
		List<URI> brokersToR = acksToR.get(mymsg.getSeqNumber());
		if (brokersToR == null) {
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.DEBUG)) {
				Log.ROUTING.debug(bstate.identity.getPath()
						+ ", reply for unknown sequence number = "
						+ mymsg.getClient() + " " + mymsg.getSeqNumber());
			}
			return;
		}
		int i = brokersToR.indexOf(mymsg.getBroker());
		if (i == -1) {
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.DEBUG)) {
				Log.ROUTING.debug(bstate.identity.getPath()
						+ ", not waiting ack. from broker = "
						+ mymsg.getBroker() + ", for client = "
						+ mymsg.getClient() + " " + mymsg.getSeqNumber());
			}
			return;
		}
		brokersToR.remove(i);
		if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
			Log.ROUTING.trace(bstate.identity.getPath()
					+ ", replies still to receive for " + mymsg.getClient()
					+ " and " + mymsg.getSeqNumber() + " is: " + brokersToR);
		}
		for (Reply reply : mymsg.getResults()) {
			if (!bstate.requestReplies.get(mymsg.getClient())
					.get(mymsg.getSeqNumber()).contains(reply)) {
				bstate.requestReplies.get(mymsg.getClient())
						.get(mymsg.getSeqNumber()).add(reply);
			}
		}
		Util.testAndSendReplies(mymsg.getClient(), mymsg.getSeqNumber());
	}

	/**
	 * receives a subscription message from a client. The message is of type
	 * <tt>SubscriptionMsgContent</tt>.
	 * 
	 * @param content
	 *            the message to treat.
	 */
	public static void receiveSubscription(
			final AbstractMessageContent content) {
		BrokerState bstate = BrokerState.getInstance();
		SubscriptionMsgContent mymsg = (SubscriptionMsgContent) content;
		if (!bstate.currIdentityType.equals(EntityType.CLIENT)) {
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.ERROR)) {
				Log.ROUTING.error(bstate.identity.getPath()
						+ ", the subscription was not sent by a client ("
						+ mymsg + ")");
			}
			return;
		}
		if (Log.ON && Log.ROUTING.getLevel().equals(Level.INFO)) {
			Log.ROUTING.info(bstate.identity.getPath() + ", subscription = "
					+ mymsg.toStringBrief());
		} else if (Log.ON && Log.ROUTING.isEnabledFor(Level.DEBUG)) {
			Log.ROUTING.debug(
					bstate.identity.getPath() + ", subscription = " + mymsg);
		}
		MultiScopingSpecification phi = null;
		if (mymsg.getPhiSub() == null) {
			phi = MultiScopingSpecification.create(new ScopeTop(),
					ScopePathStatus.UP);
		} else {
			phi = mymsg.getPhiSub();
		}
		List<URI> path = new ArrayList<URI>();
		path.add(mymsg.getClient());
		for (URI dimension : phi.getTheSets().keySet()) {
			SetOfScopePathsWithStatus ssp = phi.getTheSets().get(dimension);
			if (ssp.getTheSet().size() == 0) {
				Util.sendNAK(mymsg.getAck(), null, mymsg.getClient(),
						mymsg.getSeqNumber(),
						"no scope path for dimension = " + dimension,
						Level.WARN);
				return;
			} else if (ssp.getTheSet().size() > 1) {
				Util.sendNAK(mymsg.getAck(), null, mymsg.getClient(),
						mymsg.getSeqNumber(),
						"more than one scope path for dimension = " + dimension
								+ "(" + ssp.getTheSet() + ")",
						Level.WARN);
				return;
			} else {
				ScopePathWithStatus sp = ssp.getTheSet().get(0);
				if (sp.scopeSequence().size() == 0) {
					Util.sendNAK(mymsg.getAck(), null, mymsg.getClient(),
							mymsg.getSeqNumber(),
							"no scope in scope path of dimension = "
									+ dimension,
							Level.WARN);
					return;
				} else if (sp.scopeSequence().size() > 1) {
					Util.sendNAK(mymsg.getAck(), null, mymsg.getClient(),
							mymsg.getSeqNumber(),
							"more than one scope in scope path of dimension = "
									+ dimension,
							Level.WARN);
					return;
				} else {
					Scope scope = sp.scopeSequence().get(0);
					if (scope.identifier().equals(ScopeBottom.IDENTIFIER)) {
						Util.sendNAK(mymsg.getAck(), null, mymsg.getClient(),
								mymsg.getSeqNumber(),
								"scope bottom not allowed in subscription",
								Level.WARN);
						return;
					} else {
						List<URI> pathJoinScope = new ArrayList<URI>();
						pathJoinScope.add(bstate.identity);
						if (bstate.scopeGraphs.get(dimension) == null) {
							handleJoinScopeMessage(true, pathJoinScope,
									mymsg.getSeqNumber(), dimension,
									scope.identifier(), ScopeTop.IDENTIFIER,
									JavaScriptVisibilityFilter.ALWAYS_TRUE,
									JavaScriptVisibilityFilter.ALWAYS_FALSE);
						} else if (!bstate.scopeGraphs.get(dimension)
								.existsNode(scope)) {
							handleJoinScopeMessage(true, pathJoinScope,
									mymsg.getSeqNumber(), dimension,
									scope.identifier(), ScopeTop.IDENTIFIER,
									JavaScriptVisibilityFilter.ALWAYS_TRUE,
									JavaScriptVisibilityFilter.ALWAYS_FALSE);
						}
					}
				}
			}
		}
		handleSubscriptionMessage(mymsg.getClient(), mymsg.getAck(),
				mymsg.getSeqNumber(), mymsg.getOperationalMode(), null,
				new ArrayList<URI>(), mymsg.getFilterId(),
				mymsg.getRoutingFilter(), phi, mymsg.getABACInformation());
		if (PerfAttributes.performance_evaluation
				&& !mymsg.getFilterId().contains("startperf")) {
			bstate.nbSubscriptionsReceived++;
		}
		if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
			Log.ROUTING.trace(bstate.identity.getPath()
					+ ", set of subscriptions for " + mymsg.getClient() + " is "
					+ bstate.clientRoutingTables.get(mymsg.getClient()));
			Log.ROUTING.trace(
					bstate.identity.getPath() + ", nbSubscriptionsReceived = "
							+ bstate.nbSubscriptionsReceived);
		}
	}

	/**
	 * receives a subscription message from a broker. The message is of type
	 * <tt>AdminSubscriptionMsgContent</tt>.
	 * 
	 * @param content
	 *            the message to treat.
	 */
	public static void receiveAdminSubscription(
			final AbstractMessageContent content) {
		BrokerState bstate = BrokerState.getInstance();
		bstate.nbMsgsInTransit--;
		AdminSubscriptionMsgContent mymsg = (AdminSubscriptionMsgContent) content;
		if (!bstate.currIdentityType.equals(EntityType.BROKER)) {
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.ERROR)) {
				Log.ROUTING.error(bstate.identity.getPath()
						+ ", the subscription was not sent by a broker ("
						+ mymsg + ")");
			}
			return;
		}
		if (Log.ON && Log.ROUTING.getLevel().equals(Level.INFO)) {
			Log.ROUTING.info(bstate.identity.getPath() + ", subscription = "
					+ mymsg.toStringBrief());
		} else if (Log.ON && Log.ROUTING.isEnabledFor(Level.DEBUG)) {
			Log.ROUTING.debug(
					bstate.identity.getPath() + ", subscription = " + mymsg);
		}
		if (PerfAttributes.performance_evaluation
				&& !mymsg.getIdentifier().contains("startperf")) {
			bstate.nbAdminSubscriptionsReceived++;
		}
		if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
			Log.ROUTING.trace(bstate.identity.getPath()
					+ ", nbAdminSubscriptionsReceived = "
					+ bstate.nbAdminSubscriptionsReceived);
		}
		if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
			Log.ROUTING.trace(bstate.identity.getPath()
					+ ", bstate.brokerRoutingTables before treatment = "
					+ bstate.brokerRoutingTables);
		}
		handleSubscriptionMessage(mymsg.getClient(), mymsg.getAck(),
				mymsg.getSeqNumber(), OperationalMode.GLOBAL,
				mymsg.getPath().get(mymsg.getPath().size() - 1),
				mymsg.getPath(), mymsg.getIdentifier(),
				mymsg.getRoutingFilter(), mymsg.getPhiSub(),
				mymsg.getABACInformation());
		if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
			Log.ROUTING.trace(bstate.identity.getPath()
					+ ", bstate.brokerRoutingTables after treatment = "
					+ bstate.brokerRoutingTables);
		}
	}

	/**
	 * handles the subscription message. The subscription must be a new
	 * subscription: no already existing corresponding routing table entry (the
	 * identifier is the attribute that uniquely identifies a routing table
	 * entry). The filter is a JavaScript, which is evaluated in the scripting
	 * engine. Then, the subscription is added to the routing table of the
	 * forwarder, and if this is a global subscription, it is forwarded to all
	 * the neighbouring brokers, except <tt>forwarder</tt> (if
	 * <tt>forwarder</tt> is a broker). If required, the broker sends an
	 * acknowledgement.
	 * 
	 * @param client
	 *            the URI of the client, which originally made the call.
	 * @param ack
	 *            the parameter indicating whether an acknowledgement is
	 * @param seqNumber
	 *            the sequence number of the call to use for acknowledging.
	 * @param forwarder
	 *            the URI of the broker that forwards the subscription. It
	 *            equals <tt>null</tt> when the subscription comes from a
	 *            client.
	 * @param path
	 *            the path of the subscription received.
	 * @param identifier
	 *            the identifier of the subscription received.
	 * @param filter
	 *            the new filter.
	 * @param phi
	 *            the set of sets of scope paths.
	 * @param abacInfo
	 *            the ABAC information for the privacy filter.
	 */
	private static void handleSubscriptionMessage(final URI client,
			final ACK ack, final int seqNumber, final OperationalMode opMode,
			final URI forwarder, final List<URI> path, final String identifier,
			final String filter, final MultiScopingSpecification phi,
			final ABACInformation abacInfo) {
		BrokerState state = BrokerState.getInstance();
		if (forwarder == null) {
			for (RoutingTableEntry entry : state.clientRoutingTables
					.get(client)) {
				if (entry.getIdentifier().equalsIgnoreCase(identifier)) {
					Util.sendNAK(ack, null, client, seqNumber,
							"already existing subscription", Level.WARN);
					return;
				}
			}
		} else {
			for (RoutingTableEntry entry : state.brokerRoutingTables
					.get(forwarder)) {
				if (entry.getIdentifier().equalsIgnoreCase(identifier)) {
					if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
						Log.ROUTING.trace(state.identity.getPath() + ", broker "
								+ forwarder + " subscribes to"
								+ " an already existing identifier "
								+ identifier);
					}
					return;
				}
			}
		}
		Filter mf = null;
		if (PerfAttributes.performance_evaluation) {
			mf = state.subscriptionFiltersForPerf.get(filter);
		}
		if (mf == null) {
			try {
				if (filter.equals(state.returnTrueFilter)) {
					mf = FilterReturnTrue.getInstance();
				} else if (filter.equals(state.returnFalseFilter)) {
					mf = FilterReturnFalse.getInstance();
				} else {
					mf = Util.createJavaScriptFilter(filter,
							JavaScriptRoutingFilter.class);
					if (PerfAttributes.performance_evaluation) {
						state.subscriptionFiltersForPerf.put(filter, mf);
					}
				}
			} catch (ScriptException e) {
				if (Log.ON && Log.ROUTING.isEnabledFor(Level.ERROR)) {
					Log.ROUTING.error(state.identity.getPath() + ", "
							+ e.getMessage() + "\n"
							+ Log.printStackTrace(e.getStackTrace()));
				}
				if (ack != ACK.NO && path.size() == 0) {
					Util.sendNAK(ack, null, client, seqNumber,
							"problem with JavaScript code of the filter",
							Level.ERROR);
				}
				return;
			}
		}
		if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
			Log.ROUTING.trace(state.identity.getPath()
					+ ", inv.getInterface(MonolithicFilter.class) returned "
					+ mf);
		}
		Filter f = null;
		if (abacInfo == null) {
			f = mf;
		} else {
			XACMLPrivacyFilter privacyFilter = new XACMLPrivacyFilter(
					state.xacmlPEP, abacInfo, opMode);
			List<Filter> subF = new ArrayList<Filter>();
			subF.add(privacyFilter);
			subF.add(mf);
			f = new SubscriptionFilterWithPrivacy(subF);
		}
		if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
			Log.ROUTING.trace(
					state.identity.getPath() + ", filter to register " + f);
		}
		MultiScopingSpecification newPhi = null;
		try {
			newPhi = Util.computeScopePathsForSub(phi);
		} catch (MuDEBSException e) {
			e.printStackTrace();
			return;
		}
		if (forwarder == null) {
			state.clientRoutingTables.get(client)
					.add(new RoutingTableEntry(opMode, identifier, f, filter,
							newPhi, EntityType.BROKER, client));
		} else {
			state.brokerRoutingTables.get(forwarder)
					.add(new RoutingTableEntry(OperationalMode.GLOBAL,
							identifier, f, filter, newPhi, EntityType.CLIENT,
							forwarder));
		}
		if (Log.ON && Log.ROUTING.isEnabledFor(Level.DEBUG)) {
			Log.ROUTING
					.debug(state.identity.getPath() + ", new routing entry for "
							+ ((path.size() == 0) ? client : forwarder));
		}
		if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
			Log.ROUTING.trace(state.identity.getPath() + ", "
					+ "filter's function is \n" + filter);
		}
		if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
			Log.ROUTING.trace(state.identity.getPath() + ", "
					+ "ABAC information is \n" + abacInfo);
		}
		if (opMode == OperationalMode.GLOBAL) {
			if (ack == ACK.GLOBAL) {
				state.createEntryAcksToReceive(client, seqNumber);
				if (state.acksToReceive.get(client).get(seqNumber)
						.contains(forwarder)) {
					state.acksToReceive.get(client).get(seqNumber)
							.remove(forwarder);
					if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
						Log.ROUTING.trace(state.identity.getPath()
								+ ", admin subscription serving as an ack for "
								+ forwarder + " and " + client
								+ " with seq. number " + seqNumber);
					}
				}
				if (path.size() == 0) {
					state.createEntryAcksToSendToClients(client, seqNumber);
				} else {
					state.createEntryAcksToSendToBrokers(client, seqNumber,
							forwarder);
				}
			}
			List<URI> newPath = new ArrayList<URI>();
			newPath.addAll(path);
			newPath.add(state.identity);
			AdminSubscriptionMsgContent m = new AdminSubscriptionMsgContent(
					client, ack, seqNumber, newPath, identifier, filter, newPhi,
					abacInfo);
			for (URI uri : state.brokerIdentities.keySet()) {
				if (!newPath.contains(uri)) {
					if (Util.neighbouringBrokerInterestedInSubscription(uri,
							newPhi)) {
						try {
							state.broker.sendToABroker(
									state.brokerIdentities.get(uri),
									AlgorithmRouting.ADMIN_SUBSCRIPTION
											.getActionIndex(),
									m);
							state.nbMsgsInTransit++;
							if (ack == ACK.GLOBAL && !state.acksToReceive
									.get(client).get(seqNumber).contains(uri)) {
								state.acksToReceive.get(client).get(seqNumber)
										.add(uri);
							}
						} catch (IOException e) {
							Util.sendNAK(ack, forwarder, client, seqNumber,
									"problem in forwarding the subscription",
									Level.ERROR);
						}
					}
				}
			}
		}
		if (ack == ACK.LOCAL && path.size() == 0) {
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.INFO)) {
				Log.ROUTING.info(state.identity.getPath() + "seqNumber="
						+ seqNumber + ", command result = success");
			}
			Util.sendAcknowledgementToClient(client, seqNumber,
					new CommandResult(CommandStatus.SUCCESS, null));
		} else if (ack == ACK.GLOBAL) {
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
				Log.ROUTING.trace(state.identity.getPath()
						+ ", acksToReceive = " + state.acksToReceive
						+ "\nacksToSendToBrokers = " + state.acksToSendToBrokers
						+ "\nacksToSendToClients = "
						+ state.acksToSendToClients);
			}
			Util.testAndSendAcknowledgement(client, seqNumber,
					new CommandResult(CommandStatus.SUCCESS, null));
		}
	}

	/**
	 * receives a collective subscription message from a client. The message is
	 * of type <tt>CollectiveSubscriptionMsgContent</tt>.
	 * 
	 * @param content
	 *            the message to treat.
	 */
	public static void receiveCollectiveSubscription(
			final AbstractMessageContent content) {
		BrokerState bstate = BrokerState.getInstance();
		CollectiveSubscriptionMsgContent mymsg = (CollectiveSubscriptionMsgContent) content;
		if (!bstate.currIdentityType.equals(EntityType.CLIENT)) {
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.ERROR)) {
				Log.ROUTING.error(bstate.identity.getPath()
						+ ", the collective subscription"
						+ " was not sent by a client (" + mymsg + ")");
			}
			return;
		}
		if (Log.ON && Log.ROUTING.getLevel().equals(Level.INFO)) {
			Log.ROUTING.info(bstate.identity.getPath()
					+ ", collective subscription = " + mymsg.toStringBrief());
		} else if (Log.ON && Log.ROUTING.isEnabledFor(Level.DEBUG)) {
			Log.ROUTING.debug(bstate.identity.getPath()
					+ ", collective subscription = " + mymsg);
		}
		List<Subscription> subs = new ArrayList<Subscription>();
		for (Subscription subscription : mymsg.getSubscriptions()) {
			MultiScopingSpecification phi = null;
			if (subscription.getPhi() == null) {
				phi = MultiScopingSpecification.create(new ScopeTop(),
						ScopePathStatus.UP);
			} else {
				phi = subscription.getPhi();
			}
			List<URI> path = new ArrayList<URI>();
			path.add(mymsg.getClient());
			for (URI dimension : phi.getTheSets().keySet()) {
				SetOfScopePathsWithStatus ssp = phi.getTheSets().get(dimension);
				if (ssp.getTheSet().size() == 0) {
					Util.sendNAK(mymsg.getACK(), null, mymsg.getClient(),
							mymsg.getSequenceNumber(),
							"no scope path for dimension = " + dimension,
							Level.WARN);
					return;
				} else if (ssp.getTheSet().size() > 1) {
					Util.sendNAK(mymsg.getACK(), null, mymsg.getClient(),
							mymsg.getSequenceNumber(),
							"more than one scope path for dimension = "
									+ dimension + "(" + ssp.getTheSet() + ")",
							Level.WARN);
					return;
				} else {
					ScopePathWithStatus sp = ssp.getTheSet().get(0);
					if (sp.scopeSequence().size() == 0) {
						Util.sendNAK(mymsg.getACK(), null, mymsg.getClient(),
								mymsg.getSequenceNumber(),
								"no scope in scope path of dimension = "
										+ dimension,
								Level.WARN);
						return;
					} else if (sp.scopeSequence().size() > 1) {
						Util.sendNAK(mymsg.getACK(), null, mymsg.getClient(),
								mymsg.getSequenceNumber(),
								"more than one scope in scope path of dimension = "
										+ dimension,
								Level.WARN);
						return;
					} else {
						Scope scope = sp.scopeSequence().get(0);
						if (scope.identifier().equals(ScopeBottom.IDENTIFIER)) {
							Util.sendNAK(mymsg.getACK(), null,
									mymsg.getClient(),
									mymsg.getSequenceNumber(),
									"scope bottom not allowed in subscription",
									Level.WARN);
							return;
						} else {
							List<URI> pathJoinScope = new ArrayList<URI>();
							pathJoinScope.add(bstate.identity);
							if (bstate.scopeGraphs.get(dimension) == null) {
								handleJoinScopeMessage(true, pathJoinScope,
										mymsg.getSequenceNumber(), dimension,
										scope.identifier(), ScopeTop.IDENTIFIER,
										JavaScriptVisibilityFilter.ALWAYS_TRUE,
										JavaScriptVisibilityFilter.ALWAYS_FALSE);
							} else if (!bstate.scopeGraphs.get(dimension)
									.existsNode(scope)) {
								handleJoinScopeMessage(true, pathJoinScope,
										mymsg.getSequenceNumber(), dimension,
										scope.identifier(), ScopeTop.IDENTIFIER,
										JavaScriptVisibilityFilter.ALWAYS_TRUE,
										JavaScriptVisibilityFilter.ALWAYS_FALSE);
							}
						}
					}
				}
			}
			subs.add(new Subscription(subscription.getIdentifier(),
					subscription.getRoutingFilter(), phi,
					subscription.getAbacInformation()));
		}
		if (subs.isEmpty()) {
			Util.sendNAK(mymsg.getACK(), null, mymsg.getClient(),
					mymsg.getSequenceNumber(),
					"empty set of collective subscriptions", Level.WARN);
			return;
		}
		if (PerfAttributes.performance_evaluation
				&& !subs.get(0).getIdentifier().contains("startperf")) {
			bstate.nbSubscriptionsReceived += mymsg.getSubscriptions().size();
		}
		handleCollectiveSubscriptionMessage(mymsg.getClient(), mymsg.getACK(),
				mymsg.getSequenceNumber(), mymsg.getOperationalMode(), null,
				new ArrayList<URI>(), subs);
		if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
			Log.ROUTING.trace(bstate.identity.getPath()
					+ ", set of subscriptions for " + mymsg.getClient() + " is "
					+ bstate.clientRoutingTables.get(mymsg.getClient()));
			Log.ROUTING.trace(
					bstate.identity.getPath() + ", nbSubscriptionsReceived = "
							+ bstate.nbSubscriptionsReceived);
		}
	}

	/**
	 * receives a collective subscription message from a broker. The message is
	 * of type <tt>AdminCollectiveSubscriptionMsgContent</tt>.
	 * 
	 * @param content
	 *            the message to treat.
	 */
	public static void receiveAdminCollectiveSubscription(
			final AbstractMessageContent content) {
		BrokerState bstate = BrokerState.getInstance();
		bstate.nbMsgsInTransit--;
		CollectiveAdminSubscriptionMsgContent mymsg = (CollectiveAdminSubscriptionMsgContent) content;
		if (!bstate.currIdentityType.equals(EntityType.BROKER)) {
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.ERROR)) {
				Log.ROUTING.error(bstate.identity.getPath()
						+ ", the collective subscription"
						+ " was not sent by a broker (" + mymsg + ")");
			}
			return;
		}
		if (Log.ON && Log.ROUTING.getLevel().equals(Level.INFO)) {
			Log.ROUTING.info(bstate.identity.getPath()
					+ ", collective subscription = " + mymsg.toStringBrief());
		} else if (Log.ON && Log.ROUTING.isEnabledFor(Level.DEBUG)) {
			Log.ROUTING.debug(bstate.identity.getPath()
					+ ", collective subscription = " + mymsg);
		}
		if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
			Log.ROUTING.trace(bstate.identity.getPath()
					+ ", nbAdminSubscriptionsReceived = "
					+ bstate.nbAdminSubscriptionsReceived);
		}
		if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
			Log.ROUTING.trace(bstate.identity.getPath()
					+ ", bstate.brokerRoutingTables before treatment = "
					+ bstate.brokerRoutingTables);
		}
		if (PerfAttributes.performance_evaluation && !mymsg.getSubscriptions()
				.get(0).getIdentifier().contains("startperf")) {
			bstate.nbAdminSubscriptionsReceived += mymsg.getSubscriptions()
					.size();
		}
		handleCollectiveSubscriptionMessage(mymsg.getClient(), mymsg.getACK(),
				mymsg.getSequenceNumber(), mymsg.getOperationalMode(),
				mymsg.getPath().get(mymsg.getPath().size() - 1),
				mymsg.getPath(), mymsg.getSubscriptions());
		if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
			Log.ROUTING.trace(bstate.identity.getPath()
					+ ", bstate.brokerRoutingTables after treatment = "
					+ bstate.brokerRoutingTables);
		}
	}

	/**
	 * handles the subscription message. The subscription must be a new
	 * subscription: no already existing corresponding routing table entry (the
	 * identifier is the attribute that uniquely identifies a routing table
	 * entry). The filter is a JavaScript, which is evaluated in the scripting
	 * engine. Then, the subscription is added to the routing table of the
	 * forwarder, and if this is a global subscription, it is forwarded to all
	 * the neighbouring brokers, except <tt>forwarder</tt> (if
	 * <tt>forwarder</tt> is a broker). If required, the broker sends an
	 * acknowledgement.
	 * 
	 * @param client
	 *            the URI of the client, which originally made the call.
	 * @param ack
	 *            the parameter indicating whether an acknowledgement is
	 * @param seqNumber
	 *            the sequence number of the call to use for acknowledging.
	 * @param forwarder
	 *            the URI of the broker that forwards the subscription. It
	 *            equals <tt>null</tt> when the subscription comes from a
	 *            client.
	 * @param path
	 *            the path of the subscription received.
	 * @param subs
	 *            the set of collective subscriptions.
	 */
	private static void handleCollectiveSubscriptionMessage(final URI client,
			final ACK ack, final int seqNumber, final OperationalMode opMode,
			final URI forwarder, final List<URI> path,
			final List<Subscription> subs) {
		BrokerState state = BrokerState.getInstance();
		if (forwarder == null) {
			for (RoutingTableEntry entry : state.clientRoutingTables
					.get(client)) {
				for (Subscription sub : subs) {
					if (entry.getIdentifier()
							.equalsIgnoreCase(sub.getIdentifier())) {
						Util.sendNAK(ack, null, client, seqNumber,
								"already existing subscription", Level.WARN);
						return;
					}
				}
			}
		} else {
			for (RoutingTableEntry entry : state.brokerRoutingTables
					.get(forwarder)) {
				for (Subscription sub : subs) {
					if (entry.getIdentifier()
							.equalsIgnoreCase(sub.getIdentifier())) {
						if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
							Log.ROUTING.trace(state.identity.getPath()
									+ ", broker " + forwarder + " subscribes to"
									+ " an already existing identifier "
									+ sub.getIdentifier());
						}
						return;
					}
				}
			}
		}
		Map<URI, List<Subscription>> interestedBrokers = new HashMap<URI, List<Subscription>>();
		List<URI> newPath = new ArrayList<URI>();
		newPath.addAll(path);
		newPath.add(state.identity);
		for (Subscription sub : subs) {
			String identifier = sub.getIdentifier();
			String filter = sub.getRoutingFilter();
			MultiScopingSpecification phi = sub.getPhi();
			ABACInformation abacInfo = sub.getAbacInformation();
			Filter mf = null;
			if (PerfAttributes.performance_evaluation) {
				mf = state.subscriptionFiltersForPerf.get(filter);
			}
			if (mf == null) {
				try {
					if (filter.equals(state.returnTrueFilter)) {
						mf = FilterReturnTrue.getInstance();
					} else if (filter.equals(state.returnFalseFilter)) {
						mf = FilterReturnFalse.getInstance();
					} else {
						mf = Util.createJavaScriptFilter(filter,
								JavaScriptRoutingFilter.class);
						if (PerfAttributes.performance_evaluation) {
							state.subscriptionFiltersForPerf.put(filter, mf);
						}
					}
				} catch (ScriptException e) {
					if (Log.ON && Log.ROUTING.isEnabledFor(Level.ERROR)) {
						Log.ROUTING.error(state.identity.getPath() + ", "
								+ e.getMessage() + "\n"
								+ Log.printStackTrace(e.getStackTrace()));
					}
					if (ack != ACK.NO && path.size() == 0) {
						Util.sendNAK(ack, null, client, seqNumber,
								"problem with JavaScript code of the filter",
								Level.ERROR);
					}
					return;
				}
			}
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
				Log.ROUTING.trace(state.identity.getPath()
						+ ", inv.getInterface(MonolithicFilter.class) returned "
						+ mf);
			}
			Filter f = null;
			if (abacInfo == null) {
				f = mf;
			} else {
				XACMLPrivacyFilter privacyFilter = new XACMLPrivacyFilter(
						state.xacmlPEP, abacInfo, opMode);
				List<Filter> subF = new ArrayList<Filter>();
				subF.add(privacyFilter);
				subF.add(mf);
				f = new SubscriptionFilterWithPrivacy(subF);
			}
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
				Log.ROUTING.trace(
						state.identity.getPath() + ", filter to register " + f);
			}
			MultiScopingSpecification newPhi = null;
			try {
				newPhi = Util.computeScopePathsForSub(phi);
			} catch (MuDEBSException e) {
				e.printStackTrace();
				return;
			}
			if (forwarder == null) {
				state.clientRoutingTables.get(client)
						.add(new RoutingTableEntry(opMode, identifier, f,
								filter, newPhi, EntityType.BROKER, client));
			} else {
				state.brokerRoutingTables.get(forwarder)
						.add(new RoutingTableEntry(OperationalMode.GLOBAL,
								identifier, f, filter, newPhi,
								EntityType.CLIENT, forwarder));
			}
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.DEBUG)) {
				Log.ROUTING.debug(
						state.identity.getPath() + ", new routing entry for "
								+ ((path.size() == 0) ? client : forwarder));
			}
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
				Log.ROUTING.trace(state.identity.getPath() + ", "
						+ "filter's function is \n" + filter);
			}
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
				Log.ROUTING.trace(state.identity.getPath() + ", "
						+ "ABAC information is \n" + abacInfo);
			}
			if (opMode == OperationalMode.GLOBAL) {
				for (URI uri : state.brokerIdentities.keySet()) {
					if (!newPath.contains(uri)) {
						if (Util.neighbouringBrokerInterestedInSubscription(uri,
								newPhi)) {
							List<Subscription> in = interestedBrokers.get(uri);
							if (in == null) {
								in = new ArrayList<Subscription>();
								interestedBrokers.put(uri, in);
							}
							if (!in.contains(sub)) {
								in.add(sub);
							}
						}
					}
				}
			}
		}
		if (ack == ACK.GLOBAL) {
			state.createEntryAcksToReceive(client, seqNumber);
			if (state.acksToReceive.get(client).get(seqNumber)
					.contains(forwarder)) {
				state.acksToReceive.get(client).get(seqNumber)
						.remove(forwarder);
				if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
					Log.ROUTING.trace(state.identity.getPath()
							+ ", admin subscription serving as an ack for "
							+ forwarder + " and " + client
							+ " with seq. number " + seqNumber);
				}
			}
			if (path.size() == 0) {
				state.createEntryAcksToSendToClients(client, seqNumber);
			} else {
				state.createEntryAcksToSendToBrokers(client, seqNumber,
						forwarder);
			}
		}
		for (URI uri : interestedBrokers.keySet()) {
			CollectiveAdminSubscriptionMsgContent m = new CollectiveAdminSubscriptionMsgContent(
					client, seqNumber, newPath, opMode, ack,
					interestedBrokers.get(uri));
			try {
				state.broker.sendToABroker(state.brokerIdentities.get(uri),
						AlgorithmRouting.ADMIN_COLLECTIVE_SUBSCRIPTION
								.getActionIndex(),
						m);
				state.nbMsgsInTransit++;
				if (ack == ACK.GLOBAL && !state.acksToReceive.get(client)
						.get(seqNumber).contains(uri)) {
					state.acksToReceive.get(client).get(seqNumber).add(uri);
				}
			} catch (IOException e) {
				Util.sendNAK(ack, forwarder, client, seqNumber,
						"problem in forwarding the subscription", Level.ERROR);
			}
		}
		if (ack == ACK.LOCAL && path.size() == 0) {
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.INFO)) {
				Log.ROUTING.info(state.identity.getPath() + "seqNumber="
						+ seqNumber + ", command result=success");
			}
			Util.sendAcknowledgementToClient(client, seqNumber,
					new CommandResult(CommandStatus.SUCCESS, null));
		} else if (ack == ACK.GLOBAL) {
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
				Log.ROUTING.trace(state.identity.getPath()
						+ ", acksToReceive = " + state.acksToReceive
						+ "\nacksToSendToBrokers = " + state.acksToSendToBrokers
						+ "\nacksToSendToClients = "
						+ state.acksToSendToClients);
			}
			Util.testAndSendAcknowledgement(client, seqNumber,
					new CommandResult(CommandStatus.SUCCESS, null));
		}
	}

	/**
	 * receives an unadvertisement message from a client. The message is of type
	 * <tt>UnadvertisementMsgContent</tt>. If required, the broker sends an
	 * acknowledgement.
	 * 
	 * @param content
	 *            the message to treat.
	 */
	public static void receiveUnadvertisement(
			final AbstractMessageContent content) {
		BrokerState bstate = BrokerState.getInstance();
		UnadvertisementMsgContent mymsg = (UnadvertisementMsgContent) content;
		if (!bstate.currIdentityType.equals(EntityType.CLIENT)) {
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.ERROR)) {
				Log.ROUTING.error(bstate.identity.getPath()
						+ ", the unadvertisement was not sent by a client ("
						+ mymsg + ")");
			}
			return;
		}
		if (Log.ON && Log.ROUTING.getLevel().equals(Level.INFO)) {
			Log.ROUTING.info(bstate.identity.getPath() + ", unadvertisement = "
					+ mymsg.toStringBrief());
		} else if (Log.ON && Log.ROUTING.isEnabledFor(Level.DEBUG)) {
			Log.ROUTING.debug(
					bstate.identity.getPath() + ", unadvertisement = " + mymsg);
		}
		handleUnadvertisementMessage(mymsg.getClient(), null,
				mymsg.getSeqNumber(), new ArrayList<URI>(),
				mymsg.getIdentifier(), mymsg.getAck());
	}

	/**
	 * receives an unadvertisement message from a broker. The message is of type
	 * <tt>AdminUnadvertisementMsgContent</tt>.
	 * 
	 * @param content
	 *            the message to treat.
	 */
	public static void receiveAdminUnadvertisement(
			final AbstractMessageContent content) {
		BrokerState bstate = BrokerState.getInstance();
		bstate.nbMsgsInTransit--;
		AdminUnadvertisementMsgContent mymsg = (AdminUnadvertisementMsgContent) content;
		if (!bstate.currIdentityType.equals(EntityType.BROKER)) {
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.ERROR)) {
				Log.ROUTING.error(
						bstate.identity.getPath() + ", the unadvertisement"
								+ " was not sent by a broker (" + mymsg + ")");
			}
			return;
		}
		if (Log.ON && Log.ROUTING.getLevel().equals(Level.INFO)) {
			Log.ROUTING.info(bstate.identity.getPath()
					+ ", admin unadvertisement = " + mymsg.toStringBrief());
		} else if (Log.ON && Log.ROUTING.isEnabledFor(Level.DEBUG)) {
			Log.ROUTING.debug(bstate.identity.getPath()
					+ ", admin unadvertisement = " + mymsg);
		}
		handleUnadvertisementMessage(mymsg.getPath().get(0),
				mymsg.getPath().get(mymsg.getPath().size() - 1),
				mymsg.getSeqNumber(), mymsg.getPath(), mymsg.getIdentifier(),
				mymsg.getAck());
	}

	/**
	 * handles the unadvertisement message. If the message comes from a client
	 * or from a broker, the advertisement is searched for in the advertisement
	 * table for clients or for broker, respectively. The corresponding routing
	 * table entry is then removed. If the adervtisement was a global
	 * subscription, then the unsadvertisement is forwarded to broker
	 * neighbours. If required, the broker sends an acknowledgement.
	 * 
	 * @param client
	 *            the URI of the client, which originally made the call.
	 * @param forwarder
	 *            the URI of the broker that forwards the unadvertisement. It
	 *            equals <tt>null</tt> when the unadvertisement comes from a
	 *            client.
	 * @param path
	 *            the path of the unadvertisement received.
	 * @param identifier
	 *            the identifier of the unadvertisement received.
	 * @param ack
	 *            the parameter indicating whether an acknowledgement is
	 *            expected.
	 */
	private static void handleUnadvertisementMessage(final URI client,
			final URI forwarder, final int seqNumber, final List<URI> path,
			final String identifier, final ACK ack) {
		BrokerState bstate = BrokerState.getInstance();
		AdvertisementTableEntry adve = null;
		if (PerfAttributes.performance_evaluation
				&& !PerfAttributes.unAllSubOrPubStarted
				&& PerfAttributes.clientsRoutingTableSize) {
			PerfAttributes.unAllSubOrPubStarted = true;
			bstate.clientsRoutingTableSize = 0;
			if (bstate.clientRoutingTables != null
					&& !bstate.clientRoutingTables.isEmpty()) {
				bstate.clientsRoutingTableSize = 0;
				for (URI uri : bstate.clientRoutingTables.keySet()) {
					if (!bstate.clientRoutingTables.get(uri).isEmpty()) {
						for (RoutingTableEntry re : bstate.clientRoutingTables
								.get(uri)) {
							if (!re.getIdentifier().contains("startperf")
									|| re.getIdentifier()
											.contains("advertisement")) {
								bstate.clientsRoutingTableSize++;
							}
						}

					}
				}
			}
		}
		if (forwarder == null) {
			HashMap<String, AdvertisementTableEntry> advs = bstate.localAdvertisements
					.get(client);
			if (advs == null) {
				advs = new HashMap<String, AdvertisementTableEntry>();
			}
			if (!advs.containsKey(identifier)) {
				Util.sendNAK(ack, null, client, seqNumber,
						"unknown advertisement = " + identifier, Level.WARN);
				return;
			}
			adve = advs.get(identifier);
		} else {
			HashMap<URI, HashMap<String, AdvertisementTableEntry>> fadvs = bstate.remoteAdvertisements
					.get(forwarder);
			if (fadvs == null) {
				fadvs = new HashMap<URI, HashMap<String, AdvertisementTableEntry>>();
				bstate.remoteAdvertisements.put(forwarder, fadvs);
			}
			HashMap<String, AdvertisementTableEntry> advs = fadvs.get(client);
			if (advs == null) {
				advs = new HashMap<String, AdvertisementTableEntry>();
				bstate.remoteAdvertisements.get(forwarder).put(client, advs);
			}
			if (!advs.containsKey(identifier)) {
				Util.sendNAK(ack, forwarder, client, seqNumber,
						"already unregistered remote advertisement = "
								+ identifier,
						Level.WARN);
				return;
			}
			adve = advs.get(identifier);
		}
		if (adve.getOperationalMode() == OperationalMode.GLOBAL) {
			if (ack == ACK.GLOBAL) {
				bstate.createEntryAcksToReceive(client, seqNumber);
				if (bstate.acksToReceive.get(client).get(seqNumber)
						.contains(forwarder)) {
					bstate.acksToReceive.get(client).get(seqNumber)
							.remove(forwarder);
					if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
						Log.ROUTING.trace(bstate.identity.getPath()
								+ ", admin unadvertisement serving as ack for "
								+ forwarder + " and " + client
								+ " with seq. number " + seqNumber);
					}
				}
				if (forwarder == null) {
					bstate.createEntryAcksToSendToClients(client, seqNumber);
				} else {
					bstate.createEntryAcksToSendToBrokers(client, seqNumber,
							forwarder);
				}
			}
			if (forwarder == null) {
				path.add(client);
			}
			path.add(bstate.identity);
			AdminUnadvertisementMsgContent m = new AdminUnadvertisementMsgContent(
					path, ack, seqNumber, identifier);
			for (URI uri : bstate.brokerIdentities.keySet()) {
				if (!path.contains(uri)) {
					try {
						bstate.broker.sendToABroker(
								bstate.brokerIdentities.get(uri),
								AlgorithmRouting.ADMIN_UNADVERTISEMENT
										.getActionIndex(),
								m);
						bstate.nbMsgsInTransit++;
						if (ack == ACK.GLOBAL && !bstate.acksToReceive
								.get(client).get(seqNumber).contains(uri)) {
							bstate.acksToReceive.get(client).get(seqNumber)
									.add(uri);
						}
					} catch (IOException e) {
						Util.sendNAK(ack, forwarder, client, seqNumber,
								"problem in forwarding the"
										+ " unadvertisement",
								Level.ERROR);
					}
				}
			}
		}
		if (forwarder == null) {
			bstate.localAdvertisements.get(client).remove(identifier);
			if (bstate.localAdvertisements.get(client).isEmpty()) {
				bstate.localAdvertisements.remove(client);
			}
		} else {
			bstate.remoteAdvertisements.get(forwarder).get(client)
					.remove(identifier);
			if (bstate.remoteAdvertisements.get(forwarder).get(client)
					.isEmpty()) {
				bstate.remoteAdvertisements.get(forwarder).remove(client);
			}
		}
		if (PerfAttributes.performance_evaluation
				&& !identifier.contains("startperf")) {
			bstate.nbUnadvertisementsReceived++;
		}
		if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
			Log.ROUTING.trace(bstate.identity.getPath()
					+ ", set of local advertisements for " + client + " is "
					+ bstate.localAdvertisements.get(client));
			if (forwarder != null) {
				Log.ROUTING.trace(bstate.identity.getPath()
						+ ", set of remote advertisements for " + forwarder
						+ " is " + bstate.remoteAdvertisements.get(forwarder));
			}
			Log.ROUTING.trace(bstate.identity.getPath()
					+ ", nbUnadvertisementsReceived = "
					+ bstate.nbUnadvertisementsReceived);
		}
		if (forwarder == null && Log.ON
				&& Log.ROUTING.isEnabledFor(Level.INFO)) {
			Log.ROUTING.info(
					bstate.identity.getPath() + ", command result = success");
		}
		if (ack == ACK.LOCAL) {
			Util.sendAcknowledgementToClient(client, seqNumber,
					new CommandResult(CommandStatus.SUCCESS, null));
		} else if (ack == ACK.GLOBAL) {
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
				Log.ROUTING.trace(bstate.identity.getPath()
						+ ", acksToReceive = " + bstate.acksToReceive
						+ "\nacksToSendToClients = "
						+ bstate.acksToSendToBrokers
						+ "\nacksToSendToBrokers = "
						+ bstate.acksToSendToClients);
			}
			Util.testAndSendAcknowledgement(client, seqNumber,
					new CommandResult(CommandStatus.SUCCESS, null));
		}
	}

	/**
	 * receives an unsubscription message from a client. The message is of type
	 * <tt>UnsubscriptionMsgContent</tt>.
	 * 
	 * @param content
	 *            the message to treat.
	 */
	public static void receiveUnsubscription(
			final AbstractMessageContent content) {
		BrokerState bstate = BrokerState.getInstance();
		UnsubscriptionMsgContent mymsg = (UnsubscriptionMsgContent) content;
		if (!bstate.currIdentityType.equals(EntityType.CLIENT)) {
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.ERROR)) {
				Log.ROUTING.error(bstate.identity.getPath()
						+ ", the unsubscription was not sent by a client ("
						+ mymsg + ")");
			}
			return;
		}
		if (Log.ON && Log.ROUTING.getLevel().equals(Level.INFO)) {
			Log.ROUTING.info(bstate.identity.getPath() + ", unsubscription = "
					+ mymsg.toStringBrief());
		} else if (Log.ON && Log.ROUTING.isEnabledFor(Level.DEBUG)) {
			Log.ROUTING.debug(
					bstate.identity.getPath() + ", unsubscription = " + mymsg);
		}
		if (PerfAttributes.performance_evaluation
				&& !mymsg.getIdentifier().contains("startperf")) {
			bstate.nbUnsubscriptionsReceived++;
		}
		handleUnsubscriptionMessage(mymsg.getClient(), null,
				mymsg.getSeqNumber(), new ArrayList<URI>(),
				mymsg.getIdentifier(), mymsg.getAck());
		if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
			Log.ROUTING.trace(
					bstate.identity.getPath() + ", nbUnsubscriptionsReceived = "
							+ bstate.nbUnsubscriptionsReceived);
		}
	}

	/**
	 * receives an unsubscription message from a broker. The message is of type
	 * <tt>AdminUnsubscriptionMsgContent</tt>.
	 * 
	 * @param content
	 *            the message to treat.
	 */
	public static void receiveAdminUnsubscription(
			final AbstractMessageContent content) {
		BrokerState bstate = BrokerState.getInstance();
		bstate.nbMsgsInTransit--;
		AdminUnsubscriptionMsgContent mymsg = (AdminUnsubscriptionMsgContent) content;
		if (!bstate.currIdentityType.equals(EntityType.BROKER)) {
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.ERROR)) {
				Log.ROUTING.error(bstate.identity.getPath()
						+ ", the unsubscription was not sent by a broker ("
						+ mymsg + ")");
			}
			return;
		}
		if (Log.ON && Log.ROUTING.getLevel().equals(Level.INFO)) {
			Log.ROUTING.info(bstate.identity.getPath() + ", unsubscription = "
					+ mymsg.toStringBrief());
		} else if (Log.ON && Log.ROUTING.isEnabledFor(Level.DEBUG)) {
			Log.ROUTING.debug(
					bstate.identity.getPath() + ", unsubscription = " + mymsg);
		}
		if (PerfAttributes.performance_evaluation
				&& !mymsg.getIdentifier().contains("startperf")) {
			bstate.nbAdminUnsubscriptionsReceived++;
		}
		if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
			Log.ROUTING.trace(bstate.identity.getPath()
					+ ", nbAdminUnsubscriptionsReceived = "
					+ bstate.nbAdminUnsubscriptionsReceived);
		}
		if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
			Log.ROUTING.trace(bstate.identity.getPath()
					+ ", bstate.brokerRoutingTables before treatment = "
					+ bstate.brokerRoutingTables);
		}
		handleUnsubscriptionMessage(mymsg.getClient(),
				mymsg.getPath().get(mymsg.getPath().size() - 1),
				mymsg.getSeqNumber(), (ArrayList<URI>) mymsg.getPath(),
				mymsg.getIdentifier(), mymsg.getAck());
		if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
			Log.ROUTING.trace(bstate.identity.getPath()
					+ ", bstate.brokerRoutingTables after treatment = "
					+ bstate.brokerRoutingTables);
		}
	}

	/**
	 * handles the unsubscription message. If the message comes from a client or
	 * from a broker, the subscription is searched for in the routing table for
	 * clients or for broker, respectively. The corresponding routing table
	 * entry is then removed. If the subscription was a global subscription,
	 * then the unsubscription is forwarded to broker neighbours. If required,
	 * the broker sends an acknowledgement.
	 * 
	 * @param client
	 *            the URI of the client, which originally made the call.
	 * @param forwarder
	 *            the URI of the broker that forwards the unsubscription. It
	 *            equals <tt>null</tt> when the unsubscription comes from a
	 *            client.
	 * @param path
	 *            the path of the unsubscription received.
	 * @param identifier
	 *            the identifier of the unsubscription received.
	 * @param ack
	 *            the parameter indicating whether an acknowledgement is
	 *            expected.
	 */
	private static void handleUnsubscriptionMessage(final URI client,
			final URI forwarder, final int seqNumber, final List<URI> path,
			final String identifier, final ACK ack) {
		BrokerState state = BrokerState.getInstance();
		RoutingTableEntry found = null;
		if (PerfAttributes.performance_evaluation
				&& !PerfAttributes.unAllSubOrPubStarted
				&& PerfAttributes.clientsRoutingTableSize) {
			PerfAttributes.unAllSubOrPubStarted = true;
			if (state.clientRoutingTables != null
					&& !state.clientRoutingTables.isEmpty()) {
				state.clientsRoutingTableSize = 0;
				for (URI uri : state.clientRoutingTables.keySet()) {
					if (!state.clientRoutingTables.get(uri).isEmpty()) {
						for (RoutingTableEntry re : state.clientRoutingTables
								.get(uri)) {
							if (!re.getIdentifier().contains("startperf")
									|| re.getIdentifier()
											.contains("advertisement")) {
								state.clientsRoutingTableSize++;
							}
						}

					}
				}
			}
		}
		if (forwarder == null) {
			for (RoutingTableEntry entry : state.clientRoutingTables
					.get(client)) {
				if (entry.getIdentifier().equalsIgnoreCase(identifier)) {
					found = entry;
				}
			}
			if (found == null) {
				Util.sendNAK(ack, null, client,
						seqNumber, " unsubscrption to"
								+ " an unknown identifier " + identifier,
						Level.WARN);
				return;
			}
		} else {
			for (RoutingTableEntry entry : state.brokerRoutingTables
					.get(forwarder)) {
				if (entry.getIdentifier().equalsIgnoreCase(identifier)) {
					found = entry;
				}
			}
			if (found == null) {
				if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
					Log.ROUTING.trace(
							state.identity.getPath() + ", broker " + forwarder
									+ " unsubscribes to an unknown identifier "
									+ identifier);
				}
				// Be careful! don not send an NACK, this case is not an error.
				return;
			}
		}
		if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
			Log.ROUTING.trace(state.identity.getPath()
					+ ", found entry to remove = " + found);
		}
		if (forwarder == null) {
			state.clientRoutingTables.get(client).remove(found);
		} else {
			state.brokerRoutingTables.get(forwarder).remove(found);
		}
		if (Log.ON && Log.ROUTING.isEnabledFor(Level.DEBUG)) {
			Log.ROUTING.debug(
					state.identity.getPath() + ", routing entry removed for "
							+ ((path.size() == 0) ? client : forwarder));
		}
		if (found.getOperationalMode() == OperationalMode.GLOBAL) {
			if (ack == ACK.GLOBAL) {
				state.createEntryAcksToReceive(client, seqNumber);
				if (state.acksToReceive.get(client).get(seqNumber)
						.contains(forwarder)) {
					state.acksToReceive.get(client).get(seqNumber)
							.remove(forwarder);
					if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
						Log.ROUTING.trace(state.identity.getPath()
								+ ", admin unsubscription serving as ack for "
								+ forwarder + " and " + client
								+ " with seq. number " + seqNumber);
					}
				}
				if (path.size() == 0) {
					state.createEntryAcksToSendToClients(client, seqNumber);
				} else {
					state.createEntryAcksToSendToBrokers(client, seqNumber,
							forwarder);
				}
			}
			path.add(state.identity);
			AdminUnsubscriptionMsgContent m = new AdminUnsubscriptionMsgContent(
					client, ack, seqNumber, path, identifier);
			for (URI uri : state.brokerIdentities.keySet()) {
				if (!path.contains(uri)) {
					try {
						state.broker.sendToABroker(
								state.brokerIdentities.get(uri),
								AlgorithmRouting.ADMIN_UNSUBSCRIPTION
										.getActionIndex(),
								m);
						state.nbMsgsInTransit++;
						if (ack == ACK.GLOBAL && !state.acksToReceive
								.get(client).get(seqNumber).contains(uri)) {
							state.acksToReceive.get(client).get(seqNumber)
									.add(uri);
						}
					} catch (IOException e) {
						Util.sendNAK(ack, forwarder, client, seqNumber,
								"problem in forwarding the unsubscription",
								Level.ERROR);
					}
				}
			}
		}
		if (path.size() == 1 && Log.ON
				&& Log.ROUTING.isEnabledFor(Level.INFO)) {
			Log.ROUTING.info(
					state.identity.getPath() + ", command result = success");
		}
		if (ack == ACK.LOCAL && path.size() == 1) {
			Util.sendAcknowledgementToClient(client, seqNumber,
					new CommandResult(CommandStatus.SUCCESS, null));
		} else if (ack == ACK.GLOBAL) {
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
				Log.ROUTING.trace(state.identity.getPath()
						+ ", acksToReceive = " + state.acksToReceive
						+ "\nacksToSendToBrokers = " + state.acksToSendToBrokers
						+ "\nacksToSendToClients = "
						+ state.acksToSendToClients);
			}
			Util.testAndSendAcknowledgement(client, seqNumber,
					new CommandResult(CommandStatus.SUCCESS, null));
		}
	}

	/**
	 * receives an acknowledgement message from a broker. The message is of type
	 * <tt>AdminAcknowledgementMsgContent</tt>.
	 * 
	 * @param content
	 *            the message to treat.
	 */
	public static void receiveAdminAcknowledgement(
			final AbstractMessageContent content) {
		BrokerState bstate = BrokerState.getInstance();
		bstate.nbMsgsInTransit--;
		AdminAcknowledgementMsgContent mymsg = (AdminAcknowledgementMsgContent) content;
		if (Log.ON && Log.ROUTING.getLevel().equals(Level.INFO)) {
			Log.ROUTING.info(bstate.identity.getPath()
					+ ", admin. acknowledgement = " + mymsg.toStringBrief());
		} else if (Log.ON && Log.ROUTING.isEnabledFor(Level.DEBUG)) {
			Log.ROUTING.debug(bstate.identity.getPath()
					+ ", admin. acknowledgement = " + mymsg);
		}
		HashMap<Integer, List<URI>> acksToR = bstate.acksToReceive
				.get(mymsg.getClient());
		if (acksToR == null) {
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.DEBUG)) {
				Log.ROUTING.debug(bstate.identity.getPath()
						+ ", acknowledgement for unknown client = "
						+ mymsg.getClient());
			}
			return;
		}
		List<URI> brokersToR = acksToR.get(mymsg.getSeqNumber());
		if (brokersToR == null) {
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.DEBUG)) {
				Log.ROUTING.debug(bstate.identity.getPath()
						+ ", acknowledgement for unknown sequence number = "
						+ mymsg.getClient() + " " + mymsg.getSeqNumber());
			}
			return;
		}
		int i = brokersToR.indexOf(mymsg.getBroker());
		if (i == -1) {
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.DEBUG)) {
				Log.ROUTING.debug(bstate.identity.getPath()
						+ ", not waiting ack. from broker = "
						+ mymsg.getBroker() + ", for client = "
						+ mymsg.getClient() + " " + mymsg.getSeqNumber());
			}
			return;
		}
		brokersToR.remove(i);
		if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
			Log.ROUTING.trace(bstate.identity.getPath()
					+ ", acknowledgements still to receive for "
					+ mymsg.getClient() + " and " + mymsg.getSeqNumber()
					+ " is: " + brokersToR);
		}
		Util.testAndSendAcknowledgement(mymsg.getClient(), mymsg.getSeqNumber(),
				mymsg.getResult());
	}

	/**
	 * receives a join scope message from a scripting broker. The message is of
	 * type <tt>JoinScopeMsgContent</tt>.
	 * 
	 * @param content
	 *            the message to treat.
	 */
	public static void receiveJoinScope(final AbstractMessageContent content) {
		BrokerState bstate = BrokerState.getInstance();
		JoinScopeMsgContent mymsg = (JoinScopeMsgContent) content;
		if (Log.ON && Log.SCOPING.getLevel().equals(Level.INFO)) {
			Log.SCOPING.info(bstate.identity.getPath() + ", join scope = "
					+ mymsg.toStringBrief());
		} else if (Log.ON && Log.SCOPING.isEnabledFor(Level.DEBUG)) {
			Log.SCOPING.debug(
					bstate.identity.getPath() + ", join scope = " + mymsg);
		}
		List<URI> path = new ArrayList<URI>();
		path.add(mymsg.getSender());
		if (mymsg.getExplicitBrokerURISet() != null
				&& !mymsg.getExplicitBrokerURISet().isEmpty()) {
			handleJoinScopeMessage(true, path, mymsg.getSequenceNumber(),
					mymsg.getDimension(), mymsg.getSubScope(),
					mymsg.getSuperScope(), mymsg.getMapupFilter(),
					mymsg.getMapdownFilter(), mymsg.getExplicitBrokerURISet());
		} else {
			handleJoinScopeMessage(true, path, mymsg.getSequenceNumber(),
					mymsg.getDimension(), mymsg.getSubScope(),
					mymsg.getSuperScope(), mymsg.getMapupFilter(),
					mymsg.getMapdownFilter());
		}
	}

	/**
	 * receives an admin join scope message from a neighbouring broker. The
	 * message is of type <tt>AdminJoinScopeMsgContent</tt>.
	 * 
	 * @param content
	 *            the message to treat.
	 */
	public static void receiveAdminJoinScope(AbstractMessageContent content) {
		BrokerState bstate = BrokerState.getInstance();
		bstate.nbMsgsInTransit--;
		AdminJoinScopeMsgContent mymsg = (AdminJoinScopeMsgContent) content;
		if (Log.ON && Log.SCOPING.getLevel().equals(Level.INFO)) {
			Log.SCOPING.info(bstate.identity.getPath() + ", join scope = "
					+ mymsg.toStringBrief());
		} else if (Log.ON && Log.SCOPING.isEnabledFor(Level.DEBUG)) {
			Log.SCOPING.debug(
					bstate.identity.getPath() + ", join scope = " + mymsg);
		}
		if (mymsg.getExplicitBrokerURISet() != null
				&& !mymsg.getExplicitBrokerURISet().isEmpty()) {
			handleJoinScopeMessage(false, mymsg.getPath(),
					mymsg.getSequenceNumber(), mymsg.getDimension(),
					mymsg.getSubScope(), mymsg.getSuperScope(),
					mymsg.getMapupFilter(), mymsg.getMapdownFilter(),
					mymsg.getExplicitBrokerURISet());
		} else {
			handleJoinScopeMessage(false, mymsg.getPath(),
					mymsg.getSequenceNumber(), mymsg.getDimension(),
					mymsg.getSubScope(), mymsg.getSuperScope(),
					mymsg.getMapupFilter(), mymsg.getMapdownFilter());
		}
	}

	/**
	 * handles the join scope message with explicit set of brokers.
	 * 
	 * @param adminCall
	 *            the boolean stating whether the call is due to
	 *            <tt>receiveJoinScope/tt>.
	 * @param path
	 *            the path of URI of the message.
	 * @param seqNumber
	 *            the sequence number of the call to use for acknowledging.
	 * @param dimension
	 *            the dimension of the scopes.
	 * @param subScopeId
	 *            the identifier of the sub-scope.
	 * @param superScopeId
	 *            the identifier of super-scope.
	 * @param mapupFilter
	 *            the visibility filter of from sub-scope to super-scope.
	 * @param mapdownFilter
	 *            the visibility filter of from super-scope to sub-scope.
	 * @param explicitBrokerURISet
	 *            the set of specified brokers that maintain the scope.
	 */
	private static void handleJoinScopeMessage(final boolean adminCall,
			final List<URI> path, final int sequenceNumber, final URI dimension,
			final String subScopeId, final String superScopeId,
			final String mapupFilter, final String mapdownFilter,
			final Set<URI> explicitBrokerURISet) {
		BrokerState state = BrokerState.getInstance();
		Scope subScope = null;
		if (subScopeId.equalsIgnoreCase(ScopeBottom.IDENTIFIER)) {
			subScope = new ScopeBottom(dimension);
		} else if (subScopeId.equalsIgnoreCase(ScopeTop.IDENTIFIER)) {
			subScope = new ScopeTop(dimension);
		} else {
			subScope = new Scope(dimension, subScopeId);
		}
		Scope superScope = null;
		if (superScopeId.equalsIgnoreCase(ScopeBottom.IDENTIFIER)) {
			superScope = new ScopeBottom(dimension);
		} else if (superScopeId.equalsIgnoreCase(ScopeTop.IDENTIFIER)) {
			superScope = new ScopeTop(dimension);
		} else {
			superScope = new Scope(dimension, superScopeId);
		}
		ScopeGraph sg = state.scopeGraphs.get(dimension);
		if (sg == null) {
			state.scopeGraphs.put(dimension, new ScopeGraph(dimension));
			sg = state.scopeGraphs.get(dimension);
		}
		Filter mapUp = null;
		if (PerfAttributes.performance_evaluation) {
			mapUp = state.visibilityFiltersForPerf.get(mapupFilter);
		}
		if (mapUp == null) {
			try {
				if (mapupFilter.equals(state.returnTrueFilter)) {
					mapUp = FilterReturnTrue.getInstance();
				} else if (mapupFilter.equals(state.returnFalseFilter)) {
					mapUp = FilterReturnFalse.getInstance();
				} else {
					mapUp = Util.createJavaScriptFilter(mapupFilter,
							JavaScriptVisibilityFilter.class);
					if (PerfAttributes.performance_evaluation) {
						state.visibilityFiltersForPerf.put(mapupFilter, mapUp);
					}
				}
			} catch (ScriptException e) {
				if (Log.ON && Log.SCOPING.isEnabledFor(Level.ERROR)) {
					Log.SCOPING.error(state.identity.getPath()
							+ ", problem with JavaScript code of the filter\n"
							+ e.getMessage() + "\n"
							+ Log.printStackTrace(e.getStackTrace()));
				}
				return;
			}
		}
		Filter mapDown = null;
		if (PerfAttributes.performance_evaluation) {
			mapDown = state.visibilityFiltersForPerf.get(mapdownFilter);
		}
		if (mapDown == null) {
			try {
				if (mapdownFilter.equals(state.returnTrueFilter)) {
					mapDown = FilterReturnTrue.getInstance();
				} else if (mapdownFilter.equals(state.returnFalseFilter)) {
					mapDown = FilterReturnFalse.getInstance();
				} else {
					mapDown = Util.createJavaScriptFilter(mapdownFilter,
							JavaScriptVisibilityFilter.class);
					if (PerfAttributes.performance_evaluation) {
						state.visibilityFiltersForPerf.put(mapdownFilter,
								mapDown);
					}
				}
			} catch (ScriptException e) {
				if (Log.ON && Log.SCOPING.isEnabledFor(Level.ERROR)) {
					Log.SCOPING.error(state.identity.getPath()
							+ ", problem with JavaScript code of the filter\n"
							+ e.getMessage() + "\n"
							+ Log.printStackTrace(e.getStackTrace()));
				}
				return;
			}
		}
		// line 30 of the algorithm
		try {
			sg.addEdge(subScope, superScope);
		} catch (MuDEBSException e) {
			if (Log.ON && Log.SCOPING.isEnabledFor(Level.INFO)) {
				Log.SCOPING.info(state.identity.getPath() + ", the edge ("
						+ subScope.identifier() + "," + superScope.identifier()
						+ ") already exists in the scope graph"
						+ " of dimension " + dimension);
			}
		}
		// line 31 of the algorithm
		List<URI> H = new ArrayList<URI>();
		for (Entry<URI, SelectionKey> neighbour : state.brokerIdentities
				.entrySet()) {
			URI uri = neighbour.getKey();
			if (explicitBrokerURISet.contains(uri)) {
				H.add(uri);
			}
		}
		// line 32 of the algorithm
		List<URI> list = state.scopeLookupTables.get(subScope);
		if (list == null) {
			list = new ArrayList<URI>();
			state.scopeLookupTables.put(subScope, list);
		}
		if (!list.contains(path.get(path.size() - 1))) {
			list.add(path.get(path.size() - 1));
		}
		list = state.scopeLookupTables.get(superScope);
		if (list == null) {
			list = new ArrayList<URI>();
			state.scopeLookupTables.put(superScope, list);
		}
		if (!list.contains(path.get(path.size() - 1))) {
			list.add(path.get(path.size() - 1));
		}
		// line 33 of the algorithm
		if (adminCall) {
			List<RoutingTableEntry> entries = state.brokerRoutingTables
					.get(state.identity);
			if (entries == null) {
				state.brokerRoutingTables.put(state.identity,
						new ArrayList<RoutingTableEntry>());
				entries = state.brokerRoutingTables.get(state.identity);
			}
			RoutingTableEntry entry = null;
			if (!subScope.identifier()
					.equalsIgnoreCase(ScopeBottom.IDENTIFIER)) {
				entry = new RoutingTableEntryVisibility(OperationalMode.LOCAL,
						dimension.toString() + subScope.identifier()
								+ superScope.identifier(),
						mapUp, mapupFilter,
						MultiScopingSpecification.create(subScope,
								ScopePathStatus.UP),
						EntityType.BROKER, state.identity, superScope);
				entries.add(entry);
			}
			if (!superScope.identifier()
					.equalsIgnoreCase(ScopeTop.IDENTIFIER)) {
				entry = new RoutingTableEntryVisibility(OperationalMode.LOCAL,
						dimension.toString() + superScope.identifier()
								+ subScope.identifier(),
						mapDown, mapdownFilter,
						MultiScopingSpecification.create(superScope,
								ScopePathStatus.DOWN),
						EntityType.BROKER, state.identity, subScope);
				entries.add(entry);
			}
		}
		// line 34 of the algorithm
		List<URI> newPath = new ArrayList<URI>();
		newPath.addAll(path);
		if (!path.contains(state.identity)) {
			newPath.add(state.identity);
		}
		AdminJoinScopeMsgContent m = new AdminJoinScopeMsgContent(newPath,
				sequenceNumber, dimension, subScope.identifier(),
				superScope.identifier(), mapupFilter, mapdownFilter,
				explicitBrokerURISet);
		for (URI neighbour : H) {
			if (!newPath.contains(neighbour)) {
				try {
					// line 35 of the algorithm
					if (state.clientIdentities
							.get(subScope.identifier()) == null) {
						list = state.scopeLookupTables.get(subScope);
						if (list == null) {
							state.scopeLookupTables.put(subScope,
									new ArrayList<URI>());
							list = state.scopeLookupTables.get(subScope);
						}
						if (!list.contains(neighbour)) {
							list.add(neighbour);
						}
					}
					list = state.scopeLookupTables.get(superScope);
					if (list == null) {
						state.scopeLookupTables.put(superScope,
								new ArrayList<URI>());
						list = state.scopeLookupTables.get(superScope);
					}
					if (!list.contains(neighbour)) {
						list.add(neighbour);
					}
					// line 36 of the algorithm
					state.broker.sendToABroker(
							state.brokerIdentities.get(neighbour),
							AlgorithmRouting.ADMIN_JOIN_SCOPE.getActionIndex(),
							m);
					state.nbMsgsInTransit++;
				} catch (IOException e) {
					if (Log.ON && Log.ROUTING.isEnabledFor(Level.INFO)) {
						Log.ROUTING.info(state.identity.getPath()
								+ ", problem in forwarding join scope");
					}
				}
			}
		}
		state.nbJoinScopesReceived++;
		if (Log.ON && Log.SCOPING.isEnabledFor(Level.TRACE)) {
			Log.SCOPING.trace(state.identity.getPath()
					+ ", scope graph for dimension " + subScope.dimension()
					+ ": " + state.scopeGraphs.get(dimension));
			Log.SCOPING.trace(state.identity.getPath()
					+ ", scope lookup tables for scope " + subScope.identifier()
					+ ": " + state.scopeLookupTables.get(subScope));
			Log.SCOPING.trace(state.identity.getPath()
					+ ", scope lookup tables for scope "
					+ superScope.identifier() + ": "
					+ state.scopeLookupTables.get(superScope));
			Log.SCOPING.trace(state.identity.getPath()
					+ ", routing table entries for scope transformation: "
					+ state.brokerRoutingTables.get(state.identity));
			Log.SCOPING.trace(state.identity.getPath()
					+ ", nbJoinScopeReceived = " + state.nbJoinScopesReceived);
		}
	}

	/**
	 * handles the join scope message.
	 * 
	 * TODO: write javadoc comments and adapt arguments.
	 * 
	 * @param adminCall
	 *            the boolean stating whether the call is due to
	 *            <tt>receiveJoinScope/tt>.
	 * @param path
	 *            the path of URI of the message.
	 * @param seqNumber
	 *            the sequence number of the call to use for acknowledging.
	 * @param dimension
	 *            the dimension of the scopes.
	 * @param subScopeId
	 *            the identifier of the sub-scope.
	 * @param superScopeId
	 *            the identifier of super-scope.
	 * @param mapupFilter
	 *            the visibility filter of from sub-scope to super-scope.
	 * @param mapdownFilter
	 *            the visibility filter of from super-scope to sub-scope.
	 */
	private static void handleJoinScopeMessage(final boolean adminCall,
			final List<URI> path, final int sequenceNumber, final URI dimension,
			final String subScopeId, final String superScopeId,
			final String mapupFilter, final String mapdownFilter) {
		BrokerState state = BrokerState.getInstance();
		Scope subScope = null;
		if (subScopeId.equalsIgnoreCase(ScopeBottom.IDENTIFIER)) {
			subScope = new ScopeBottom(dimension);
		} else if (subScopeId.equalsIgnoreCase(ScopeTop.IDENTIFIER)) {
			subScope = new ScopeTop(dimension);
		} else {
			subScope = new Scope(dimension, subScopeId);
		}
		Scope superScope = null;
		if (superScopeId.equalsIgnoreCase(ScopeBottom.IDENTIFIER)) {
			superScope = new ScopeBottom(dimension);
		} else if (superScopeId.equalsIgnoreCase(ScopeTop.IDENTIFIER)) {
			superScope = new ScopeTop(dimension);
		} else {
			superScope = new Scope(dimension, superScopeId);
		}
		ScopeGraph sg = state.scopeGraphs.get(dimension);
		if (sg == null) {
			state.scopeGraphs.put(dimension, new ScopeGraph(dimension));
			sg = state.scopeGraphs.get(dimension);
		}
		Filter mapUp = null;
		if (PerfAttributes.performance_evaluation) {
			mapUp = state.visibilityFiltersForPerf.get(mapupFilter);
		}
		if (mapUp == null) {
			try {
				if (mapupFilter.equals(state.returnTrueFilter)) {
					mapUp = FilterReturnTrue.getInstance();
				} else if (mapupFilter.equals(state.returnFalseFilter)) {
					mapUp = FilterReturnFalse.getInstance();
				} else {
					mapUp = Util.createJavaScriptFilter(mapupFilter,
							JavaScriptVisibilityFilter.class);
					if (PerfAttributes.performance_evaluation) {
						state.visibilityFiltersForPerf.put(mapupFilter, mapUp);
					}
				}
			} catch (ScriptException e) {
				if (Log.ON && Log.SCOPING.isEnabledFor(Level.ERROR)) {
					Log.SCOPING.error(state.identity.getPath()
							+ ", problem with JavaScript code of the filter\n"
							+ e.getMessage() + "\n"
							+ Log.printStackTrace(e.getStackTrace()));
				}
				return;
			}
		}
		Filter mapDown = null;
		if (PerfAttributes.performance_evaluation) {
			mapDown = state.visibilityFiltersForPerf.get(mapdownFilter);
		}
		if (mapDown == null) {
			try {
				if (mapdownFilter.equals(state.returnTrueFilter)) {
					mapDown = FilterReturnTrue.getInstance();
				} else if (mapdownFilter.equals(state.returnFalseFilter)) {
					mapDown = FilterReturnFalse.getInstance();
				} else {
					mapDown = Util.createJavaScriptFilter(mapdownFilter,
							JavaScriptVisibilityFilter.class);
					if (PerfAttributes.performance_evaluation) {
						state.visibilityFiltersForPerf.put(mapdownFilter,
								mapDown);
					}
				}
			} catch (ScriptException e) {
				if (Log.ON && Log.SCOPING.isEnabledFor(Level.ERROR)) {
					Log.SCOPING.error(state.identity.getPath()
							+ ", problem with JavaScript code of the filter\n"
							+ e.getMessage() + "\n"
							+ Log.printStackTrace(e.getStackTrace()));
				}
				return;
			}
		}
		// line 30 of the algorithm
		try {
			sg.addEdge(subScope, superScope);
		} catch (MuDEBSException e) {
			if (Log.ON && Log.SCOPING.isEnabledFor(Level.INFO)) {
				Log.SCOPING.info(state.identity.getPath() + ", the edge ("
						+ subScope.identifier() + "," + superScope.identifier()
						+ ") already exists in the scope graph"
						+ " of dimension " + dimension);
			}
		}
		// line 31 of the algorithm
		List<URI> H = Util.computeSetOfNeighboursToFowardJoinScope(
				path.get(path.size() - 1), subScope, superScope);
		// line 32 of the algorithm
		List<URI> list = state.scopeLookupTables.get(subScope);
		if (list == null) {
			list = new ArrayList<URI>();
			state.scopeLookupTables.put(subScope, list);
		}
		if (!list.contains(path.get(path.size() - 1))) {
			list.add(path.get(path.size() - 1));
		}
		list = state.scopeLookupTables.get(superScope);
		if (list == null) {
			list = new ArrayList<URI>();
			state.scopeLookupTables.put(superScope, list);
		}
		if (!list.contains(path.get(path.size() - 1))) {
			list.add(path.get(path.size() - 1));
		}
		// line 33 of the algorithm
		if (adminCall) {
			List<RoutingTableEntry> entries = state.brokerRoutingTables
					.get(state.identity);
			if (entries == null) {
				state.brokerRoutingTables.put(state.identity,
						new ArrayList<RoutingTableEntry>());
				entries = state.brokerRoutingTables.get(state.identity);
			}
			RoutingTableEntry entry = null;
			if (!subScope.identifier()
					.equalsIgnoreCase(ScopeBottom.IDENTIFIER)) {
				entry = new RoutingTableEntryVisibility(OperationalMode.LOCAL,
						dimension.toString() + subScope.identifier()
								+ superScope.identifier(),
						mapUp, mapupFilter,
						MultiScopingSpecification.create(subScope,
								ScopePathStatus.UP),
						EntityType.BROKER, state.identity, superScope);
				entries.add(entry);
			}
			if (!superScope.identifier()
					.equalsIgnoreCase(ScopeTop.IDENTIFIER)) {
				entry = new RoutingTableEntryVisibility(OperationalMode.LOCAL,
						dimension.toString() + superScope.identifier()
								+ subScope.identifier(),
						mapDown, mapdownFilter,
						MultiScopingSpecification.create(superScope,
								ScopePathStatus.DOWN),
						EntityType.BROKER, state.identity, subScope);
				entries.add(entry);
			}
		}
		// line 34 of the algorithm
		List<URI> newPath = new ArrayList<URI>();
		newPath.addAll(path);
		if (!path.contains(state.identity)) {
			newPath.add(state.identity);
		}
		AdminJoinScopeMsgContent m = new AdminJoinScopeMsgContent(newPath,
				sequenceNumber, dimension, subScope.identifier(),
				superScope.identifier(), mapupFilter, mapdownFilter, null);
		for (URI neighbour : H) {
			if (!newPath.contains(neighbour)) {
				try {
					// line 35 of the algorithm
					if (state.clientIdentities
							.get(subScope.identifier()) == null) {
						list = state.scopeLookupTables.get(subScope);
						if (list == null) {
							state.scopeLookupTables.put(subScope,
									new ArrayList<URI>());
							list = state.scopeLookupTables.get(subScope);
						}
						if (!list.contains(neighbour)) {
							list.add(neighbour);
						}
					}
					list = state.scopeLookupTables.get(superScope);
					if (list == null) {
						state.scopeLookupTables.put(superScope,
								new ArrayList<URI>());
						list = state.scopeLookupTables.get(superScope);
					}
					if (!list.contains(neighbour)) {
						list.add(neighbour);
					}
					// line 36 of the algorithm
					state.broker.sendToABroker(
							state.brokerIdentities.get(neighbour),
							AlgorithmRouting.ADMIN_JOIN_SCOPE.getActionIndex(),
							m);
					state.nbMsgsInTransit++;
				} catch (IOException e) {
					if (Log.ON && Log.SCOPING.isEnabledFor(Level.INFO)) {
						Log.SCOPING.info(state.identity.getPath()
								+ ", problem in forwarding join scope");
					}
				}
			}
		}
		state.nbJoinScopesReceived++;
		if (Log.ON && Log.SCOPING.isEnabledFor(Level.TRACE)) {
			Log.SCOPING.trace(state.identity.getPath()
					+ ", scope graph for dimension " + subScope.dimension()
					+ ": " + state.scopeGraphs.get(dimension));
			Log.SCOPING.trace(state.identity.getPath()
					+ ", scope lookup tables for scope " + subScope.identifier()
					+ ": " + state.scopeLookupTables.get(subScope));
			Log.SCOPING.trace(state.identity.getPath()
					+ ", scope lookup tables for scope "
					+ superScope.identifier() + ": "
					+ state.scopeLookupTables.get(superScope));
			Log.SCOPING.trace(state.identity.getPath()
					+ ", routing table entries for scope transformation: "
					+ state.brokerRoutingTables.get(state.identity));
			Log.SCOPING.trace(state.identity.getPath()
					+ ", nbJoinScopeReceived = " + state.nbJoinScopesReceived);
		}
	}

	/**
	 * receives a leave scope message from a scripting broker. The message is of
	 * type <tt>LeaveScopeMsgContent</tt>.
	 * 
	 * @param content
	 *            the message to treat.
	 */
	public static void receiveLeaveScope(final AbstractMessageContent content) {
		BrokerState bstate = BrokerState.getInstance();
		LeaveScopeMsgContent mymsg = (LeaveScopeMsgContent) content;
		if (Log.ON && Log.SCOPING.getLevel().equals(Level.INFO)) {
			Log.SCOPING.info(bstate.identity.getPath() + ", leave scope = "
					+ mymsg.toStringBrief());
		} else if (Log.ON && Log.SCOPING.isEnabledFor(Level.DEBUG)) {
			Log.SCOPING.debug(
					bstate.identity.getPath() + ", leave scope = " + mymsg);
		}
		handleLeaveScopeMessage(mymsg.getRequester(), mymsg.getSequenceNumber(),
				mymsg.getSubScope(), mymsg.getSuperScope());
		if (Log.ON && Log.SCOPING.isEnabledFor(Level.TRACE)) {
			Log.SCOPING.trace(bstate.identity.getPath()
					+ ", scope lookup tables for dimension "
					+ mymsg.getSubScope().dimension() + ": "
					+ bstate.scopeLookupTables
							.get(mymsg.getSubScope().dimension()));
		}
	}

	/**
	 * handles the leave scope message.
	 * 
	 * TODO: write javadoc comments and adapt arguments.
	 * 
	 * @param client
	 *            the URI of the caller, which originally made the call.
	 * @param seqNumber
	 *            the sequence number of the call to use for acknowledging.
	 * @param subScope
	 *            the sub-scope.
	 * @param superScope
	 *            the super-scope.
	 */
	private static void handleLeaveScopeMessage(final URI requester,
			final int sequenceNumber, final Scope subScope,
			final Scope superScope) {
	}

	public static void receiveAdminLeaveScope(AbstractMessageContent content) {
		BrokerState bstate = BrokerState.getInstance();
		bstate.nbMsgsInTransit--;
		// TODO Auto-generated method stub
	}

	public static void receiveGetScopes(AbstractMessageContent content) {
		BrokerState state = BrokerState.getInstance();
		GetScopesMsgContent mymsg = (GetScopesMsgContent) content;
		if (!state.currIdentityType.equals(EntityType.CLIENT)) {
			if (Log.ON && Log.SCOPING.isEnabledFor(Level.ERROR)) {
				Log.SCOPING.error(state.identity.getPath()
						+ ", the get scopes was not sent by a client (" + mymsg
						+ ")");
			}
			return;
		}
		if (Log.ON && Log.SCOPING.getLevel().equals(Level.INFO)) {
			Log.SCOPING.info(state.identity.getPath() + ", get scopes = "
					+ mymsg.toStringBrief());
		} else if (Log.ON && Log.SCOPING.isEnabledFor(Level.DEBUG)) {
			Log.SCOPING.debug(
					state.identity.getPath() + ", get scopes = " + mymsg);
		}
		List<String> strs = new ArrayList<String>();
		for (ScopeGraph sg : state.scopeGraphs.values()) {
			for (Scope scope : sg.getScopes()) {
				strs.add(scope.dimension().toString());
				strs.add(scope.identifier());
			}
		}
		Gson gson = new GsonBuilder().create();
		String str = gson.toJson(strs);
		Util.sendAcknowledgementToClient(mymsg.getSender(),
				mymsg.getSequenceNumber(),
				new CommandResult(CommandStatus.SUCCESS, str));
	}
}
