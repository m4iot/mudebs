/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2017 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Christian Bac and Denis Conan
Contributor(s): Léon Lim
 */
package mudebs.broker;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.file.FileSystemException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

import mudebs.broker.algorithms.overlaymanagement.AlgorithmOverlayManagement;
import mudebs.broker.algorithms.routing.RoutingTableEntry;
import mudebs.common.Constants;
import mudebs.common.Log;
import mudebs.common.MuDEBSException;
import mudebs.common.PerfAttributes;
import mudebs.common.algorithms.EntityType;
import mudebs.common.algorithms.overlaymanagement.IdentityMsgContent;
import mudebs.communication.javanio.FullDuplexMsgWorker;
import mudebs.communication.javanio.ReadMessageStatus;

import org.apache.log4j.Level;

/**
 * This class defines the main of the broker. It configures the broker, connects
 * to existing brokers, waits for connections from other brokers and from
 * clients.
 * 
 * The brokers can be organised into a network topology forming cycles since the
 * method <tt>forward</tt> is only called when the message to forward has not
 * already been received and forwarded.
 * 
 * The broker is a thread for being used in OSGi.
 * 
 * @author Christian Bac
 * @author Denis Conan
 * @author Léon Lim
 */
public class Broker extends Thread {
	/**
	 * the usage string.
	 */
	static final String USAGE = "Usage: java mudebs.broker.BrokerMain "
			+ Constants.OPTION_URI + " <this broker URI> " + "["
			+ Constants.OPTION_NEIGHBOUR + " <a remote broker URIs>]..." + "["
			+ Constants.OPTION_LOGGER + " <logger.level>]...";
	/**
	 * selector object for the main loop waiting for connections and messages.
	 */
	private Selector selector;
	/**
	 * selection keys for accepting connections from clients and brokers.
	 */
	private SelectionKey acceptSelectionKey;
	/**
	 * broker socket channel for accepting client and broker connections.
	 */
	private ServerSocketChannel listenChannel;
	/**
	 * selection keys of client or broker message workers. When receiving the
	 * identity message, this object is moved either the relevant collection,
	 * either for clients or brokers.
	 */
	private HashMap<SelectionKey, FullDuplexMsgWorker> allPendingWorkers;
	/**
	 * selection keys of the broker message workers.
	 */
	private HashMap<SelectionKey, FullDuplexMsgWorker> allBrokerWorkers;
	/**
	 * selection keys of the scripting message workers.
	 */
	private HashMap<SelectionKey, FullDuplexMsgWorker> allScriptingWorkers;
	/**
	 * selection keys of the client message workers.
	 */
	private HashMap<SelectionKey, FullDuplexMsgWorker> allClientWorkers;
	/**
	 * broker's state for the distributed algorithms implemented in the broker.
	 */
	private BrokerState state;

	public FullDuplexMsgWorker getBrokerFullDuplexMsgWorker(
			final SelectionKey key) {
		return allBrokerWorkers.get(key);
	}

	/**
	 * Constructor of a broker.
	 * 
	 * @param argv
	 *            the command line arguments.
	 * @throws URISyntaxException
	 *             exception thrown when the URI is not correct.
	 * @throws IOException
	 *             exception thrown when problem in creating the socket to
	 *             accept connections.
	 * @throws MuDEBSException
	 *             the exception thrown in case of connection problem to the
	 *             neighbours.
	 */
	public Broker(final String[] argv)
			throws URISyntaxException, IOException, MuDEBSException {
		if ((argv.length % 2) == 1) {
			if (Log.ON && Log.CONFIG.isEnabledFor(Level.FATAL)) {
				Log.CONFIG.fatal(USAGE);
			}
			throw new IllegalArgumentException("not the right arity "
					+ "of arguments ((argv.length % 2) == 1)");
		}
		String uri = null;
		ArrayList<String> neighbours = new ArrayList<String>();
		ArrayList<String> loggers = new ArrayList<String>();
		for (int i = 0; i < argv.length; i = i + 2) {
			if (argv[i].equalsIgnoreCase(Constants.OPTION_URI)) {
				uri = argv[i + 1];
			} else if (argv[i].equalsIgnoreCase(Constants.OPTION_NEIGHBOUR)) {
				neighbours.add(argv[i + 1]);
			} else if (argv[i].equalsIgnoreCase(Constants.OPTION_LOGGER)) {
				loggers.add(argv[i + 1]);
			} else if (argv[i].equalsIgnoreCase(
					Constants.OPTION_COMMAND_PERF_CONFIG_FILE)) {
				PerfAttributes.PREF_CONFIG_FILE = argv[i + 1];
				Path path = Paths.get(PerfAttributes.PREF_CONFIG_FILE);
				if (Files.isReadable(path)) {
					PerfAttributes.perfConfigFile = true;
				}

			}
		}
		for (String logger : loggers) {
			String[] conf = logger.split("\\.");
			if (conf.length == 2) {
				Log.configureALogger(conf[0], conf[1]);
			}
		}
		if (uri == null) {
			if (Log.ON && Log.CONFIG.isEnabledFor(Level.FATAL)) {
				Log.CONFIG.fatal("The URI of the broker is not provided");
				Log.CONFIG.fatal(USAGE);
			}
			throw new IllegalArgumentException(
					"the URI of the broker" + " is not provided");
		}
		state = BrokerState.getInstance();
		state.broker = this;
		state.identity = new URI(uri);
		setPerfStatistics(PerfAttributes.PREF_CONFIG_FILE);
		if (!state.identity.getScheme().equalsIgnoreCase("muDEBS")) {
			throw new URISyntaxException(uri, "The protocol of the URI "
					+ "of this broker is not 'muDEBS'.");
		}
		if (state.identity.getPath() == null) {
			throw new URISyntaxException(uri,
					"There is no path specified in the URI."
							+ " Your must specify the name of this broker.");
		}
		if (Log.ON && Log.CONFIG.isEnabledFor(Level.DEBUG)) {
			Log.CONFIG.debug(state.identity.getPath() + ", identity = "
					+ state.identity);
		}
		allPendingWorkers = new HashMap<SelectionKey, FullDuplexMsgWorker>();
		allClientWorkers = new HashMap<SelectionKey, FullDuplexMsgWorker>();
		allBrokerWorkers = new HashMap<SelectionKey, FullDuplexMsgWorker>();
		allScriptingWorkers = new HashMap<SelectionKey, FullDuplexMsgWorker>();
		InetSocketAddress rcvAddress;
		ServerSocket listenSocket;
		// creates the selector and the listener
		selector = Selector.open();
		listenChannel = ServerSocketChannel.open();
		listenSocket = listenChannel.socket();
		rcvAddress = new InetSocketAddress(state.identity.getPort());
		listenSocket.setReuseAddress(true);
		listenSocket.bind(rcvAddress);
		listenChannel.configureBlocking(false);
		// register selection keys
		acceptSelectionKey = listenChannel.register(selector,
				SelectionKey.OP_ACCEPT);
		if (Log.ON && Log.CONFIG.isEnabledFor(Level.INFO)) {
			Log.CONFIG.trace(
					uri + ", number of neighbours = " + neighbours.size());
		}
		for (String n : neighbours) {
			try {
				connectToBroker(new URI(n));
			} catch (IOException e) {
				if (Log.ON && Log.COMM.isEnabledFor(Level.FATAL)) {
					Log.COMM.fatal(uri + ", " + e.getMessage() + "\n"
							+ Log.printStackTrace(e.getStackTrace()));
				}
				e.printStackTrace();
				throw new MuDEBSException(uri + ", " + e.getMessage());
			} catch (URISyntaxException e) {
				if (Log.ON && Log.CONFIG.isEnabledFor(Level.FATAL)) {
					Log.CONFIG.fatal(uri + ", " + e.getMessage() + "\n"
							+ Log.printStackTrace(e.getStackTrace()));
				}
			}
		}
	}

	/**
	 * sets the configuration for storing performance evaluation statistics.
	 * 
	 * @param perfConfigFile
	 *            the name of the configuration file for the performance
	 *            evaluation.
	 * @throws FileSystemException
	 *             the exception thrown if statistics cannot be stored in a
	 *             local file.
	 */
	private void setPerfStatistics(final String perfConfigFile)
			throws FileSystemException {
		if (PerfAttributes.performance_evaluation) {
			if (PerfAttributes.perfConfigFile) {
				Properties prop = new Properties();
				InputStream input = null;
				try {
					input = new FileInputStream(perfConfigFile);
					prop.load(input);
					if (prop.getProperty(Constants.ALL_STATS) != null) {
						if (prop.getProperty(Constants.ALL_STATS)
								.equalsIgnoreCase("true")) {
							PerfAttributes.allStats = true;
						}
					}
					if (prop.getProperty(
							Constants.NB_IDENTITIES_RECEIVED) != null) {
						if (prop.getProperty(Constants.NB_IDENTITIES_RECEIVED)
								.equalsIgnoreCase("true")) {
							PerfAttributes.nbIdentitiesReceived = true;
						}
					}
					if (prop.getProperty(
							Constants.NB_IDENTITIES_SENT) != null) {
						if (prop.getProperty(Constants.NB_IDENTITIES_SENT)
								.equalsIgnoreCase("true")) {
							PerfAttributes.nbIdentitiesSent = true;
						}
					}
					if (prop.getProperty(
							Constants.NB_ADVERTISEMENTS_RECEIVED) != null) {
						if (prop.getProperty(
								Constants.NB_ADVERTISEMENTS_RECEIVED)
								.equalsIgnoreCase("true")) {
							PerfAttributes.nbAdvertisementsReceived = true;
						}
					}
					if (prop.getProperty(
							Constants.NB_ADVERTISEMENTS_SENT) != null) {
						if (prop.getProperty(Constants.NB_ADVERTISEMENTS_SENT)
								.equalsIgnoreCase("true")) {
							PerfAttributes.nbAdvertisementsSent = true;
						}
					}
					if (prop.getProperty(
							Constants.NB_PUBLICATIONS_RECEIVED) != null) {
						if (prop.getProperty(Constants.NB_PUBLICATIONS_RECEIVED)
								.equalsIgnoreCase("true")) {
							PerfAttributes.nbPublicationsReceived = true;
						}
					}
					if (prop.getProperty(
							Constants.NB_PUBLICATIONS_SENT) != null) {
						if (prop.getProperty(Constants.NB_PUBLICATIONS_SENT)
								.equalsIgnoreCase("true")) {
							PerfAttributes.nbPublicationsSent = true;
						}
					}
					if (prop.getProperty(
							Constants.NB_REPLIES_RECEIVED) != null) {
						if (prop.getProperty(Constants.NB_REPLIES_RECEIVED)
								.equalsIgnoreCase("true")) {
							PerfAttributes.nbRepliesReceived = true;
						}
					}
					if (prop.getProperty(
							Constants.NB_ADMIN_SUBSCRIPTIONS_RECEIVED) != null) {
						if (prop.getProperty(
								Constants.NB_ADMIN_SUBSCRIPTIONS_RECEIVED)
								.equalsIgnoreCase("true")) {
							PerfAttributes.nbAdminSubscriptionsReceived = true;
						}
					}
					if (prop.getProperty(
							Constants.NB_ADMIN_SUBSCRIPTIONS_SENT) != null) {
						if (prop.getProperty(
								Constants.NB_ADMIN_SUBSCRIPTIONS_SENT)
								.equalsIgnoreCase("true")) {
							PerfAttributes.nbAdminSubscriptionsSent = true;
						}
					}
					if (prop.getProperty(
							Constants.NB_UNADVERTISEMENTS_RECEIVED) != null) {
						if (prop.getProperty(
								Constants.NB_UNADVERTISEMENTS_RECEIVED)
								.equalsIgnoreCase("true")) {
							PerfAttributes.nbUnadvertisementsReceived = true;
						}
					}
					if (prop.getProperty(
							Constants.NB_ADMIN_UNSCRIPTIONS_RECEIVED) != null) {
						if (prop.getProperty(
								Constants.NB_ADMIN_UNSCRIPTIONS_RECEIVED)
								.equalsIgnoreCase("true")) {
							PerfAttributes.nbAdminUnsubscriptionsReceived = true;
						}
					}
					if (prop.getProperty(
							Constants.NB_UNADVERTISEMENTS_SENT) != null) {
						if (prop.getProperty(Constants.NB_UNADVERTISEMENTS_SENT)
								.equalsIgnoreCase("true")) {
							PerfAttributes.nbUnadvertisementsSent = true;
						}
					}
					if (prop.getProperty(
							Constants.NB_UNSCRIPTIONS_RECEIVED) != null) {
						if (prop.getProperty(Constants.NB_UNSCRIPTIONS_RECEIVED)
								.equalsIgnoreCase("true")) {
							PerfAttributes.nbUnsubscriptionsReceived = true;
						}
					}
					if (prop.getProperty(
							Constants.NB_UNSCRIPTIONS_SENT) != null) {
						if (prop.getProperty(Constants.NB_UNSCRIPTIONS_SENT)
								.equalsIgnoreCase("true")) {
							PerfAttributes.nbUnsubscriptionsSent = true;
						}
					}
					if (prop.getProperty(
							Constants.NB_JOIN_SCOPE_RECEIVED) != null) {
						if (prop.getProperty(Constants.NB_JOIN_SCOPE_RECEIVED)
								.equalsIgnoreCase("true")) {
							PerfAttributes.nbJoinScopeReceived = true;
						}
					}
					if (prop.getProperty(
							Constants.NB_ADMIN_PUBLICATIONS_RECEIVED) != null) {
						if (prop.getProperty(
								Constants.NB_ADMIN_PUBLICATIONS_RECEIVED)
								.equalsIgnoreCase("true")) {
							PerfAttributes.nbAdminPublicationsReceived = true;
						}
					}
					if (prop.getProperty(
							Constants.NB_ADMIN_PUBLICATION_SENT) != null) {
						if (prop.getProperty(
								Constants.NB_ADMIN_PUBLICATION_SENT)
								.equalsIgnoreCase("true")) {
							PerfAttributes.nbAdminPublicationsSent = true;
						}
					}
					if (prop.getProperty(
							Constants.AVERAGE_NB_INVOLVED_BROKERS) != null) {
						if (prop.getProperty(
								Constants.AVERAGE_NB_INVOLVED_BROKERS)
								.equalsIgnoreCase("true")) {
							PerfAttributes.averageNbInvolvedBrokers = true;
							PerfAttributes.networkSize = Integer
									.parseInt(prop.getProperty("networkSize"));
						}
					}
					if (prop.getProperty(
							Constants.NB_BROKERS_ROUTING_TABLE_SIZE) != null) {
						if (prop.getProperty(
								Constants.NB_BROKERS_ROUTING_TABLE_SIZE)
								.equalsIgnoreCase("true")) {
							PerfAttributes.brokersRoutingTableSize = true;
						}
					}
					if (prop.getProperty(
							Constants.NB_CLIENTS_ROUTING_TABLE_SIZE) != null) {
						if (prop.getProperty(
								Constants.NB_CLIENTS_ROUTING_TABLE_SIZE)
								.equalsIgnoreCase("true")) {
							PerfAttributes.clientsRoutingTableSize = true;
						}
					}
					if (prop.getProperty(
							Constants.NB_PUBLICATIONS_DELIVERED) != null) {
						if (prop.getProperty(
								Constants.NB_PUBLICATIONS_DELIVERED)
								.equalsIgnoreCase("true")) {
							PerfAttributes.nbPublicationsDelivered = true;
						}
					}
					if (prop.getProperty(Constants.SCOPING) != null) {
						if (prop.getProperty(Constants.SCOPING)
								.equalsIgnoreCase("true")) {
							PerfAttributes.scoping = true;
						}
					}
					if (prop.getProperty(Constants.EXECUTION_TIME) != null) {
						if (prop.getProperty(Constants.EXECUTION_TIME)
								.equalsIgnoreCase("true")) {
							PerfAttributes.executionTime = true;
						}
					}
				} catch (IOException ex) {
					ex.printStackTrace();
				} finally {
					if (input != null) {
						try {
							input.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
	}

	/**
	 * accepts connection (socket level), create MsgWorker, and register
	 * selection key of a client or a broker. This method is called when
	 * accepting a connection from a client, a broker, or a scripting broker.
	 * 
	 * NB: begin with adding the selection key to the set of selection keys of
	 * clients. Afterwards, if the identity message stipulates that it is a
	 * broker, then move the selection key to the set of keys of brokers.
	 * 
	 * @param socketChannel
	 *            socket channel.
	 * @throws IOException
	 */
	private void acceptNewConnection(final ServerSocketChannel socketChannel)
			throws IOException {
		SocketChannel rwChan;
		SelectionKey newKey;
		rwChan = socketChannel.accept();
		if (rwChan != null) {
			try {
				FullDuplexMsgWorker worker = new FullDuplexMsgWorker(rwChan);
				worker.configureNonBlocking();
				newKey = rwChan.register(selector, SelectionKey.OP_READ);
				allPendingWorkers.put(newKey, worker);
				if (Log.ON && Log.COMM.isEnabledFor(Level.DEBUG)) {
					Log.COMM.debug(state.identity.getPath()
							+ ", accepting a new connection");
				}
				if (Log.ON && Log.COMM.isEnabledFor(Level.TRACE)) {
					Log.COMM.trace(state.identity.getPath()
							+ ", allPendingWorkers.size() = "
							+ allPendingWorkers.size());
				}
			} catch (ClosedChannelException e) {
				if (Log.ON && Log.COMM.isEnabledFor(Level.ERROR)) {
					Log.COMM.error(state.identity.getPath() + ", "
							+ e.getMessage() + "\n"
							+ Log.printStackTrace(e.getStackTrace()));
				}
			}
		}
	}

	/**
	 * adds the selection key and the corresponding message worker to
	 * <tt>allClientWorkers</tt>.
	 * 
	 * @param uri
	 *            the URI of the client.
	 * @param key
	 *            the selection key of the worker to move.
	 */
	public void movePendingWorkertoClientWorkers(final URI uri,
			final SelectionKey key) {
		allClientWorkers.put(key, allPendingWorkers.get(key));
		allPendingWorkers.remove(key);
		if (Log.ON && Log.COMM.isEnabledFor(Level.TRACE)) {
			Log.COMM.trace(state.identity.getPath()
					+ ", allClientWorkers.size() = " + allClientWorkers.size());
			Log.COMM.trace(
					state.identity.getPath() + ", allPendingWorkers.size() = "
							+ allPendingWorkers.size());
		}
		state.clientRoutingTables.put(uri, new ArrayList<RoutingTableEntry>());
		if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
			Log.ROUTING.trace(state.identity.getPath()
					+ ", add routing table for client " + uri);
		}
	}

	/**
	 * adds the selection key and the corresponding message worker to
	 * <tt>allBrokerWorkers</tt>.
	 * 
	 * @param uri
	 *            the URI of the broker.
	 * @param key
	 *            the selection key of the worker to move.
	 */
	public void movePendingWorkertoBrokerWorkers(final URI uri,
			final SelectionKey key) {
		allBrokerWorkers.put(key, allPendingWorkers.get(key));
		allPendingWorkers.remove(key);
		if (Log.ON && Log.COMM.isEnabledFor(Level.TRACE)) {
			Log.COMM.trace(state.identity.getPath()
					+ ", allBrokerWorkers.size() = " + allBrokerWorkers.size());
			Log.COMM.trace(
					state.identity.getPath() + ", allPendingWorkers.size() = "
							+ allPendingWorkers.size());
		}
		state.brokerRoutingTables.put(uri, new ArrayList<RoutingTableEntry>());
		if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
			Log.ROUTING.trace(state.identity.getPath()
					+ ", add routing table for broker " + uri);
		}
	}

	/**
	 * adds the selection key and the corresponding message worker to
	 * <tt>allScriptingWorkers</tt>.
	 * 
	 * @param uri
	 *            the URI of the scripting broker.
	 * @param key
	 *            the selection key of the worker to move.
	 */
	public void movePendingWorkertoScriptingWorkers(final URI uri,
			final SelectionKey key) {
		this.allScriptingWorkers.put(key, allPendingWorkers.get(key));
		allPendingWorkers.remove(key);
		if (Log.ON && Log.COMM.isEnabledFor(Level.TRACE)) {
			Log.COMM.trace(
					state.identity.getPath() + ", allScriptingWorkers.size() = "
							+ allScriptingWorkers.size());
			Log.COMM.trace(
					state.identity.getPath() + ", allPendingWorkers.size() = "
							+ allPendingWorkers.size());
		}
	}

	/**
	 * connect socket, create MsgWorker, and register selection key of the
	 * remote broker. This method is called when connecting to a remote broker.
	 * Connection data are provided as arguments to the main.
	 * 
	 * NB: the selection key is added to the set of selection keys of brokers
	 * because this broker initiate a connection with another broker, i.e. it
	 * cannot initiate a connection with a client.
	 * 
	 * @param uri
	 *            URI of the neighbouring broker.
	 * @throws IOException
	 *             the exception thrown in case of problem
	 */
	public void connectToBroker(final URI uri) throws IOException {
		Socket rwSock;
		SocketChannel rwChan;
		InetSocketAddress rcvAddress;
		if (Log.ON && Log.COMM.isEnabledFor(Level.DEBUG)) {
			Log.COMM.debug(
					state.identity.getPath() + ", connecting to broker " + uri);
		}
		FullDuplexMsgWorker worker = null;
		SelectionKey brokerKey = null;
		for (int i = 1; i <= Constants.NB_CONNECTION_RETRIES; i++) {
			InetAddress destAddr = InetAddress.getByName(uri.getHost());
			rwChan = SocketChannel.open();
			rwSock = rwChan.socket();
			// obtain the IP address of the target host
			rcvAddress = new InetSocketAddress(destAddr, uri.getPort());
			// connect sending socket to remote port
			try {
				rwSock.connect(rcvAddress);
			} catch (IOException e) {
				if (Log.ON && Log.COMM.isEnabledFor(Level.DEBUG)) {
					Log.COMM.debug("connection retry to " + uri + " = " + i);
				}
				if (i == Constants.NB_CONNECTION_RETRIES) {
					if (Log.ON && Log.COMM.isEnabledFor(Level.ERROR)) {
						Log.COMM.error(state.identity
								+ ", max retries reached => give up");
					}
					throw e;
				}
				try {
					Thread.sleep(Constants.TIME_WAIT_CONNECTION_RETRIES);
				} catch (InterruptedException e1) {
					if (Log.ON && Log.COMM.isEnabledFor(Level.FATAL)) {
						Log.COMM.fatal(state.identity + ", "
								+ Log.printStackTrace(e.getStackTrace()));
					}
				}
				continue;
			}
			worker = new FullDuplexMsgWorker(rwChan);
			worker.configureNonBlocking();
			brokerKey = rwChan.register(selector, SelectionKey.OP_READ);
			break;
		}
		allBrokerWorkers.put(brokerKey, worker);
		state.brokerIdentities.put(uri, brokerKey);
		state.brokerRoutingTables.put(uri, new ArrayList<RoutingTableEntry>());
		if (Log.ON && Log.COMM.isEnabledFor(Level.TRACE)) {
			Log.COMM.trace(state.identity.getPath()
					+ ", allBrokerWorkers.size() = " + allBrokerWorkers.size());
		}
		if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
			Log.ROUTING.trace(state.identity.getPath()
					+ ", add routing table for broker " + uri);
		}
		long nb = worker.sendMsg(
				AlgorithmOverlayManagement.IDENTITY.getActionIndex(),
				new IdentityMsgContent(state.identity, EntityType.BROKER));
		if (Log.ON && Log.OVERLAY.isEnabledFor(Level.DEBUG)) {
			Log.OVERLAY.debug(state.identity.getPath()
					+ ", identity message sent: " + nb + " bytes");
		}
	}

	/**
	 * send a message to a particular remote broker / neighbour. This is a
	 * utility method for implementing distributed algorithms in the brokers'
	 * state machine: use this method when this broker needs sending messages to
	 * a neighbour.
	 * 
	 * @param targetKey
	 *            selection key of the neighbour.
	 * @param type
	 *            message's type.
	 * @param content
	 *            message as an object to be serialised with Gson.
	 * @throws IOException
	 *             the exception thrown in case of problem.
	 */
	public void sendToABroker(final SelectionKey targetKey, final int type,
			final Object content) throws IOException {
		FullDuplexMsgWorker sendWorker = allBrokerWorkers.get(targetKey);
		if (sendWorker == null) {
			if (Log.ON && Log.COMM.isEnabledFor(Level.WARN)) {
				Log.COMM.warn(state.identity.getPath()
						+ ", bad receiver for broker key " + targetKey);
			}
		} else {
			sendWorker.sendMsg(type, content);
		}
		if (Log.ON && Log.COMM.isEnabledFor(Level.DEBUG)) {
			Log.COMM.debug(
					state.identity.getPath() + ", message sent to a broker");
		}
	}

	/**
	 * send a message to a particular client. This is a utility method for
	 * implementing distributed algorithms in the brokers' state machine: use
	 * this method when this broker needs sending messages to a client.
	 * 
	 * @param targetKey
	 *            the selection key of the client.
	 * @param type
	 *            the message's type.
	 * @param content
	 *            the message as an object to be serialised with Gson.
	 * @throws IOException
	 *             the exception thrown in case of problem.
	 */
	public void sendToAClient(final SelectionKey targetKey, final int type,
			final Object content) throws IOException {
		FullDuplexMsgWorker sendWorker = allClientWorkers.get(targetKey);
		if (sendWorker == null) {
			if (Log.ON && Log.COMM.isEnabledFor(Level.WARN)) {
				Log.COMM.warn(state.identity.getPath()
						+ ", bad receiver for client key " + targetKey);
			}
		} else {
			sendWorker.sendMsg(type, content);
		}
		if (Log.ON && Log.COMM.isEnabledFor(Level.DEBUG)) {
			Log.COMM.debug(
					state.identity.getPath() + ", message sent to a client");
		}
	}

	/**
	 * is the infinite loop organised around the call to select.
	 */
	private void loop() {
		if (Log.ON && Log.COMM.isEnabledFor(Level.DEBUG)) {
			Log.COMM.debug(
					state.identity.getPath() + ", entering the infinite loop");
		}
		if (Log.ON && Log.COMM.isEnabledFor(Level.TRACE)) {
			Log.COMM.trace(
					state.identity.getPath() + ", listenChannel ok on port "
							+ listenChannel.socket().getLocalPort());
		}
		while (!state.terminationRequired) {
			try {
				selector.select();
			} catch (IOException e) {
				if (Log.ON && Log.COMM.isEnabledFor(Level.FATAL)) {
					Log.COMM.fatal(state.identity.getPath() + ", "
							+ e.getMessage() + "\n"
							+ Log.printStackTrace(e.getStackTrace()));
				}
				return;
			}
			Set<SelectionKey> readyKeys = selector.selectedKeys();
			Iterator<SelectionKey> readyIter = readyKeys.iterator();
			while (readyIter.hasNext()) {
				SelectionKey key = readyIter.next();
				readyIter.remove();
				if (key.isAcceptable()) {
					try {
						if (key.equals(acceptSelectionKey)) {
							acceptNewConnection(listenChannel);
							if (Log.ON && Log.COMM.isEnabledFor(Level.TRACE)) {
								Log.COMM.trace(state.identity.getPath()
										+ ", allPendingWorkers.size() = "
										+ allPendingWorkers.size());
							}
						} else {
							if (Log.ON && Log.COMM.isEnabledFor(Level.FATAL)) {
								Log.COMM.fatal(state.identity.getPath()
										+ ", unknown accept");
							}
							return;
						}
					} catch (IOException e) {
						if (Log.ON && Log.COMM.isEnabledFor(Level.FATAL)) {
							Log.COMM.fatal(state.identity.getPath() + ", "
									+ e.getMessage() + "\n"
									+ Log.printStackTrace(e.getStackTrace()));
						}
					}
				}
				if (key.isReadable()) {
					state.currKey = key;
					FullDuplexMsgWorker clientWorker = allClientWorkers
							.get(key);
					if (clientWorker != null) {
						// message comes from a client
						state.currIdentityType = EntityType.CLIENT;
						try {
							ReadMessageStatus status;
							status = clientWorker.readMessage();
							if (status == ReadMessageStatus.ChannelClosed) {
								// remote end point has been closed
								clientWorker.close();
								closeClientConnection(key);
							}
							if (status == ReadMessageStatus.ReadDataCompleted) {
								// fully received a message
								int messType = clientWorker.getMessType();
								// place where we get the data from the
								// message
								String msg = clientWorker.getData();
								if (Log.ON
										&& Log.COMM.isEnabledFor(Level.DEBUG)) {
									Log.COMM.debug(state.identity.getPath()
											+ ", message received " + msg);
								}
								// message for broker's message dispatcher
								if (Log.ON
										&& Log.COMM.isEnabledFor(Level.TRACE)) {
									Log.COMM.trace(state.identity.getPath()
											+ ", going to execute"
											+ " action for message type #"
											+ messType);
								}
								mudebs.broker.algorithms.ListOfAlgorithms
										.execute(messType, msg);
							}
						} catch (IOException e) {
							if (Log.ON && Log.COMM.isEnabledFor(Level.ERROR)) {
								Log.COMM.error(state.identity.getPath() + ", "
										+ e.getMessage() + "\n"
										+ Log.printStackTrace(
												e.getStackTrace()));
							}
							closeClientConnection(key);
						}
					}
					FullDuplexMsgWorker brokerWorker = allBrokerWorkers
							.get(key);
					if (brokerWorker != null) {
						// message comes from another broker
						state.currIdentityType = EntityType.BROKER;
						try {
							ReadMessageStatus status;
							status = brokerWorker.readMessage();
							if (status == ReadMessageStatus.ChannelClosed) {
								// remote end point has been closed
								brokerWorker.close();
								closeBrokerConnection(key);
							}
							if (status == ReadMessageStatus.ReadDataCompleted) {
								// fully received a message
								int messType = brokerWorker.getMessType();
								// place where we get the data from the
								// message
								String msg = brokerWorker.getData();
								if (Log.ON
										&& Log.COMM.isEnabledFor(Level.DEBUG)) {
									Log.COMM.debug(state.identity.getPath()
											+ ", message received " + msg);
								}
								// message for broker's message dispatcher
								if (Log.ON
										&& Log.COMM.isEnabledFor(Level.TRACE)) {
									Log.COMM.trace(state.identity.getPath()
											+ ", action for message type #"
											+ messType);
								}
								mudebs.broker.algorithms.ListOfAlgorithms
										.execute(messType, msg);
							}
						} catch (IOException e) {
							if (Log.ON && Log.COMM.isEnabledFor(Level.ERROR)) {
								Log.COMM.error(state.identity.getPath() + ", "
										+ e.getMessage() + "\n"
										+ Log.printStackTrace(
												e.getStackTrace()));
							}
							try {
								brokerWorker.close();
							} catch (IOException e1) {
								e1.printStackTrace();
							}
							closeBrokerConnection(key);
						}
					}
					FullDuplexMsgWorker scriptingWorker = allScriptingWorkers
							.get(key);
					if (scriptingWorker != null) {
						// message comes from a scripting broker
						state.currIdentityType = EntityType.SCRIPTING;
						try {
							ReadMessageStatus status;
							status = scriptingWorker.readMessage();
							if (status == ReadMessageStatus.ChannelClosed) {
								// remote end point has been closed
								scriptingWorker.close();
								closeScriptingConnection(key);
							}
							if (status == ReadMessageStatus.ReadDataCompleted) {
								// fully received a message
								int messType = scriptingWorker.getMessType();
								// place where we get the data from the
								// message
								String msg = scriptingWorker.getData();
								if (Log.ON
										&& Log.COMM.isEnabledFor(Level.DEBUG)) {
									Log.COMM.debug(state.identity.getPath()
											+ ", message received " + msg);
								}
								// message for broker's message dispatcher
								if (Log.ON
										&& Log.COMM.isEnabledFor(Level.TRACE)) {
									Log.COMM.trace(state.identity.getPath()
											+ ", going to execute"
											+ " action for message type #"
											+ messType);
								}
								mudebs.broker.algorithms.ListOfAlgorithms
										.execute(messType, msg);
							}
						} catch (IOException e) {
							if (Log.ON && Log.COMM.isEnabledFor(Level.ERROR)) {
								Log.COMM.error(state.identity.getPath() + ", "
										+ e.getMessage() + "\n"
										+ Log.printStackTrace(
												e.getStackTrace()));
							}
							try {
								scriptingWorker.close();
							} catch (IOException e1) {
								e1.printStackTrace();
							}
							closeScriptingConnection(key);
						}
					}
					FullDuplexMsgWorker worker = allPendingWorkers.get(key);
					if (worker != null) {
						// message comes from a client or a broker
						// don't know yet, but execute only identity messages
						state.currIdentityType = EntityType.UNKNOWN;
						try {
							ReadMessageStatus status;
							status = worker.readMessage();
							if (status == ReadMessageStatus.ChannelClosed) {
								// remote end point has been closed
								worker.close();
								allBrokerWorkers.remove(key);
								if (Log.ON
										&& Log.COMM.isEnabledFor(Level.DEBUG)) {
									Log.COMM.debug(state.identity.getPath()
											+ ", closing a channel");
								}
								if (Log.ON
										&& Log.COMM.isEnabledFor(Level.TRACE)) {
									Log.COMM.trace(state.identity.getPath()
											+ ", allPendingsWorkers.size() = "
											+ allPendingWorkers.size());
								}
							}
							if (status == ReadMessageStatus.ReadDataCompleted) {
								// fully received a message
								int messType = worker.getMessType();
								// place where we get the data from the
								// message
								String msg = worker.getData();
								if (Log.ON
										&& Log.COMM.isEnabledFor(Level.DEBUG)) {
									Log.COMM.debug(state.identity.getPath()
											+ ", message received " + msg);
								}
								if (messType == AlgorithmOverlayManagement.IDENTITY
										.getActionIndex()) {
									// message for broker's message dispatcher
									if (Log.ON && Log.COMM
											.isEnabledFor(Level.TRACE)) {
										Log.COMM.trace(state.identity.getPath()
												+ ", going to execute"
												+ " action for message type #"
												+ messType);
									}
									mudebs.broker.algorithms.ListOfAlgorithms
											.execute(messType, msg);
								}
							}
						} catch (IOException e) {
							if (Log.ON && Log.COMM.isEnabledFor(Level.ERROR)) {
								Log.COMM.error(state.identity.getPath() + ", "
										+ e.getMessage() + "\n"
										+ Log.printStackTrace(
												e.getStackTrace()));
							}
							try {
								worker.close();
							} catch (IOException e1) {
								e1.printStackTrace();
							}
							allBrokerWorkers.remove(key);
						}
					}
				}
			}
		}
	}

	/**
	 * performs actions when a client connection is closed.
	 * 
	 * TODO: treat current subscriptions, advertisements, etc.
	 * 
	 * @param key
	 *            the selection key of the connection.
	 */
	private void closeClientConnection(final SelectionKey key) {
		allClientWorkers.remove(key);
		if (Log.ON && Log.COMM.isEnabledFor(Level.DEBUG)) {
			Log.COMM.debug(state.identity.getPath() + ", closing a channel");
		}
		if (Log.ON && Log.COMM.isEnabledFor(Level.TRACE)) {
			Log.COMM.trace(state.identity.getPath()
					+ ", allClientWorkers.size() = " + allClientWorkers.size());
		}
		for (URI uri : state.clientIdentities.keySet()) {
			if (state.clientIdentities.get(uri).equals(key)) {
				state.localAdvertisements.remove(uri);
				if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
					Log.ROUTING.trace(
							state.identity.getPath() + ", advertisement table"
									+ " removed for client " + uri);
				}
				state.clientRoutingTables.remove(uri);
				if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
					Log.ROUTING.trace(state.identity.getPath()
							+ ", routing table" + " removed for client " + uri);
				}
			}
		}
		URI removeElem = null;
		for (URI elem : state.clientIdentities.keySet()) {
			if (state.clientIdentities.get(elem).equals(state.currKey)) {
				removeElem = elem;
			}
		}
		state.clientIdentities.remove(removeElem);
	}

	/**
	 * performs actions when a broker connection is closed.
	 * 
	 * TODO: treat current subscriptions, advertisements, etc.
	 * 
	 * @param key
	 *            the selection key of the connection.
	 */
	private void closeBrokerConnection(final SelectionKey key) {
		allBrokerWorkers.remove(key);
		if (Log.ON && Log.COMM.isEnabledFor(Level.DEBUG)) {
			Log.COMM.debug(state.identity.getPath() + ", closing a channel");
		}
		if (Log.ON && Log.COMM.isEnabledFor(Level.TRACE)) {
			Log.COMM.trace(
					state.identity.getPath() + ", allBrokersWorkers.size() = "
							+ allBrokerWorkers.size());
		}
		for (URI uri : state.brokerIdentities.keySet()) {
			if (state.brokerIdentities.get(uri).equals(key)) {
				state.remoteAdvertisements.remove(uri);
				if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
					Log.ROUTING.trace(
							state.identity.getPath() + ", advertisement table"
									+ " removed for broker " + uri);
				}
				state.brokerRoutingTables.remove(uri);
				if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
					Log.ROUTING.trace(state.identity.getPath()
							+ ", routing table" + " removed for broker " + uri);
				}
			}
		}
		URI removeElem = null;
		for (URI elem : state.brokerIdentities.keySet()) {
			if (state.brokerIdentities.get(elem).equals(state.currKey)) {
				removeElem = elem;
			}
		}
		state.brokerIdentities.remove(removeElem);
	}

	/**
	 * performs actions when a scripting broker connection is closed.
	 * 
	 * @param key
	 *            the selection key of the connection.
	 */
	private void closeScriptingConnection(final SelectionKey key) {
		allScriptingWorkers.remove(key);
		if (Log.ON && Log.COMM.isEnabledFor(Level.DEBUG)) {
			Log.COMM.debug(state.identity.getPath() + ", closing a channel");
		}
		if (Log.ON && Log.COMM.isEnabledFor(Level.TRACE)) {
			Log.COMM.trace(
					state.identity.getPath() + ", allScriptingWorkers.size() = "
							+ allScriptingWorkers.size());
		}
	}

	/**
	 * requires the termination of the broker.
	 */
	public void terminate() {
		if (Log.ON && Log.CONFIG.isEnabledFor(Level.DEBUG)) {
			Log.CONFIG.debug("The termination is required for broker "
					+ state.currIdentityType);
		}
		state.terminationRequired = true;
	}

	/**
	 * forces the termination of the broker.
	 */
	public void forceTermination() {
		if (Log.ON && Log.CONFIG.isEnabledFor(Level.DEBUG)) {
			Log.CONFIG.debug("The termination is forced for broker "
					+ state.currIdentityType);
		}
		System.exit(0);
	}

	/**
	 * shuts down the broker: closes all the connections and clears all the
	 * state maps.
	 */
	private void shutdown() {
		try {
			acceptSelectionKey.channel().close();
			acceptSelectionKey.cancel();
		} catch (IOException e) {
			if (Log.ON && Log.COMM.isEnabledFor(Level.ERROR)) {
				Log.COMM.error(state.identity.getPath() + ", " + e.getMessage()
						+ "\n" + Log.printStackTrace(e.getStackTrace()));
			}
		}
		try {
			for (SelectionKey key : state.brokerIdentities.values()) {
				key.channel().close();
				key.cancel();
				allBrokerWorkers.remove(key);
			}
			state.brokerIdentities.clear();
			state.brokerRoutingTables.clear();
		} catch (IOException e) {
			if (Log.ON && Log.COMM.isEnabledFor(Level.ERROR)) {
				Log.COMM.error(state.identity.getPath() + ", " + e.getMessage()
						+ "\n" + Log.printStackTrace(e.getStackTrace()));
			}
		}
		try {
			for (SelectionKey key : state.clientIdentities.values()) {
				key.channel().close();
				key.cancel();
				allClientWorkers.remove(key);
			}
			state.clientIdentities.clear();
			state.clientRoutingTables.clear();
			state.localAdvertisements.clear();
		} catch (IOException e) {
			if (Log.ON && Log.COMM.isEnabledFor(Level.ERROR)) {
				Log.COMM.error(state.identity.getPath() + ", " + e.getMessage()
						+ "\n" + Log.printStackTrace(e.getStackTrace()));
			}
		}
		try {
			for (SelectionKey key : allPendingWorkers.keySet()) {
				key.channel().close();
				key.cancel();
				allPendingWorkers.remove(key);
			}
		} catch (IOException e) {
			if (Log.ON && Log.COMM.isEnabledFor(Level.ERROR)) {
				Log.COMM.error(state.identity.getPath() + ", " + e.getMessage()
						+ "\n" + Log.printStackTrace(e.getStackTrace()));
			}
		}
	}

	@Override
	public void run() {
		loop();
		shutdown();
	}
}
