/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2017 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */
package mudebs.broker.algorithms.routing;

import java.util.List;

import mudebs.broker.BrokerState;
import mudebs.common.Log;
import mudebs.common.algorithms.OperationalMode;
import mudebs.common.filters.ConjunctionOfFilters;
import mudebs.common.filters.Filter;
import mudebs.common.filters.PrivacyFilter;

import org.apache.log4j.Level;
import org.w3c.dom.Document;

/**
 * This class defines a muDEBS composite filter that is a conjunction of filters
 * for a subscription with privacy.
 * 
 * @author Denis Conan
 * 
 */
public class SubscriptionFilterWithPrivacy extends ConjunctionOfFilters {
	/**
	 * boolean stating whether the current publication possesses privacy
	 * requirement.
	 */
	private boolean privacyRequirement = false;
	/**
	 * operational mode indicating whether the advertisement corresponding to
	 * the publication is local.
	 */
	private OperationalMode isLocalAdvertisement = OperationalMode.GLOBAL;
	/**
	 * the operational mode of the subscription (global or local) corresponding
	 * to the routing table entry.
	 */
	private OperationalMode opMode = OperationalMode.GLOBAL;
	/**
	 * the size of the path of the publication to be analysed.
	 */
	private int publicationPathSize = 0;
	/**
	 * boolean stating whether the subscription corresponding to the routing
	 * table entry has got access control enabled when the analysis took place
	 * at the producer's access broker.
	 */
	private boolean accessControlEnabled = false;

	/**
	 * builds a filter that is a conjunction of filters. Since there is no
	 * method to add/remove a filter to/from the list, a conjunction of filters
	 * cannot include itself.
	 * 
	 * @param fs
	 *            the list of filters
	 */
	public SubscriptionFilterWithPrivacy(List<Filter> fs) {
		super(fs);
	}

	/**
	 * evaluates whether the publication matches the conjunction of filters. The
	 * filters are applied in the order of the list and all the filters that are
	 * evaluated must return <tt>true</tt>. The conditions for the necessity to
	 * analyse the privacy filter are the following ones:
	 * <table border=1 summary="">
	 * <tr>
	 * <td></td>
	 * <td>Local subscription</td>
	 * <td>Global subscription</td>
	 * </tr>
	 * <tr>
	 * <td>Local advertisement</td>
	 * <td>if advertisement with a privacy policy and<br>
	 * publication path of size 1</td>
	 * <td>if advertisement with a privacy policy and<br>
	 * publication path of size 1</td>
	 * </tr>
	 * <tr>
	 * <td>Global advertisement</td>
	 * <td>if advertisement with a privacy policy</td>
	 * <td>if advertisement with a privacy policy<br>
	 * and publication path of size 1</td>
	 * </tr>
	 * </table>
	 * <p>
	 * Let us recall that publication messages contain a boolean stating whether
	 * the corresponding advertisement possesses a privacy requirement, and if
	 * so, the list of global subscriptions with enabled access control when
	 * analysed on the producer's access broker. In addition, when received at
	 * the access broker of the producer, the path of the publication message
	 * contains the URI of the client, thus its size is equal to 1.
	 * </p>
	 * 
	 * Let <tt>ls = local subscription</tt>, <tt>gs = global subscription</tt>,
	 * <tt>p = advertisement with privacy policy</tt>,
	 * <tt>s1 = publication path of size 1</tt> (the size of the path is always
	 * &ge; 1). We have the following:
	 * <ul>
	 * <li>
	 * From the table
	 * <ul>
	 * <li>
	 * <tt>(la and ls and p and s1) or (la and gs and p and s1) or (ga and ls and p) or (ga and gs and p and s1)</tt>
	 * </li>
	 * </ul>
	 * </li>
	 * <li>
	 * Using distributivity of <tt>and</tt> over <tt>or</tt>, commutativity, and
	 * associativity
	 * <ul>
	 * <li>
	 * <tt>p and [s1 and [(la and ls) or (la and gs) or (ga and gs)] or (ga and ls)]</tt>
	 * </li>
	 * </ul>
	 * </li>
	 * <li>
	 * By definition of <tt>la = not ga</tt>, and <tt>ls = not gs</tt>
	 * <ul>
	 * <li>
	 * <tt>p and [s1 and [(la and ls) or (la and not ls) or (not la and not ls)] or (not la and ls)]</tt>
	 * </li>
	 * </ul>
	 * </li>
	 * <li>
	 * Using associativity, and distributivity of <tt>and</tt> over <tt>or</tt>
	 * <ul>
	 * <li>
	 * <tt>p and [s1 and [(la and (ls and not ls)) or (not la and not ls)] or (not la and ls)]</tt>
	 * </li>
	 * </ul>
	 * </li>
	 * <li>
	 * Using excluded middle, and identity of <tt>and</tt>
	 * <ul>
	 * <li>
	 * <tt>p and [s1 and [la or (not la and not ls)] or (not la and ls)]</tt></li>
	 * </ul>
	 * </li>
	 * <li>
	 * Using absorption: <tt>p or (not p and q) = p or q</tt>
	 * <ul>
	 * <li>
	 * <tt>p and [s1 and (la or not ls) or (not la and ls)]</tt></li>
	 * </ul>
	 * </li>
	 * </ul>
	 * 
	 * Finally, when the publication is tagged with
	 * "privacy requirement required", then the conjunction of filters must
	 * contain a privacy filter. When the publication is tagged with
	 * "privacy requirement required", the subscription is global, and the
	 * access control was not enabled at the producer's access broker, then the
	 * composite filter must return <tt>false</tt>.
	 * 
	 * @param publication
	 *            the publication to evaluate.
	 * @return the boolean stating the matching.
	 */
	@Override
	public boolean evaluate(Document publication) {
		boolean p = privacyRequirement;
		boolean privacyFilterFound = (p) ? false : true;
		boolean la = (isLocalAdvertisement.equals(OperationalMode.LOCAL)) ? true
				: false;
		boolean ls = (opMode == OperationalMode.LOCAL) ? true : false;
		boolean s1 = (publicationPathSize == 1) ? true : false;
		boolean ace = accessControlEnabled;
		if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
			Log.ROUTING.trace(BrokerState.getInstance().identity.getPath()
					+ ", " + this + "\np = " + p + ", la = " + la + ", ls = "
					+ ls + ", s1 = " + s1 + ", ac = " + ace);
		}
		for (Filter f : filters) {
			if (f instanceof PrivacyFilter) {
				privacyFilterFound = true;
				if (p && !ls && !s1 && !ace) {
					return false;
				}
				if (!(p && (s1 && (la || !ls)) || (!la && ls))) {
					continue;
				}
			}
			if (!f.evaluate(publication)) {
				if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
					Log.ROUTING.trace("publication does not match filter " + f);
				}
				resetFilterAttributes();
				return false;
			} else {
				if (Log.ON && Log.ROUTING.isEnabledFor(Level.TRACE)) {
					Log.ROUTING.trace("publication matches filter " + f);
				}
			}
		}
		resetFilterAttributes();
		return privacyFilterFound;
	}

	/**
	 * sets the privacy requirement. This method should be called before the
	 * method <tt>evaluate</tt>.
	 * 
	 * @param pr
	 *            the privacy requirement.
	 */
	public void setHasGotPrivacyRequirement(final boolean pr) {
		privacyRequirement = pr;
	}

	/**
	 * sets the boolean stating whether the advertisement corresponding to the
	 * publication is local.
	 * 
	 * @param local
	 *            whether the advertisement is local.
	 */
	public void setIsLocalAdvertisement(final boolean local) {
		isLocalAdvertisement = (local) ? OperationalMode.LOCAL
				: OperationalMode.GLOBAL;
	}

	/**
	 * sets the boolean stating whether the subscription corresponding to the
	 * routing table entry is local.
	 * 
	 * @param mode
	 *            the operational mode of the subscription (global or local).
	 */
	public void setIsLocalSubscription(final OperationalMode mode) {
		opMode = mode;
	}

	/**
	 * sets the size of the path of the publication to be analysed.
	 * 
	 * @param s
	 *            the size of the path.
	 */
	public void setPublicationPathSize(final int s) {
		publicationPathSize = s;
	}

	/**
	 * sets the boolean stating whether the subscription corresponding to the
	 * routing table entry has got access control enabled when the analysis took
	 * place at the producer's access broker.
	 * 
	 * @param a
	 *            whether the access control is already enabled.
	 */
	public void setAccessControlEnabled(final boolean a) {
		accessControlEnabled = a;
	}

	/**
	 * resets the attributes of the filter.
	 */
	private void resetFilterAttributes() {
		privacyRequirement = false;
		isLocalAdvertisement = OperationalMode.GLOBAL;
		opMode = OperationalMode.GLOBAL;
		publicationPathSize = 0;
		accessControlEnabled = false;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SubscriptionFilterWithPrivacy [privacyRequirement="
				+ privacyRequirement + ", isLocalAdvertisement="
				+ isLocalAdvertisement + ", isLocalSubscription="
				+ (opMode == OperationalMode.LOCAL) + ", publicationPathSize="
				+ publicationPathSize + ", accessControlEnabled="
				+ accessControlEnabled + ", filters=" + filters + "]";
	}
}
