/**
Copyright (c) WSO2 Inc. (http://www.wso2.org) All Rights Reserved.

WSO2 Inc. licenses this file to you under the Apache License,
Version 2.0 (the "License"); you may not use this file except
in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.

This file is derived from the file balana/1.0.0/modules/
balana-core/src/main/java/org/wso2/balana/finder/impl/
FileBasedPolicyFinderModule.java.
 
Modifications copyright (C) 2015 Léon Lim:
<Leon.Lim@telecom-sudparis.eu> 
 */

package mudebs.broker.algorithms.routing;

import mudebs.broker.BrokerState;
import mudebs.common.Log;

import org.apache.log4j.Level;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.wso2.balana.*;
import org.wso2.balana.combine.PolicyCombiningAlgorithm;
import org.wso2.balana.ctx.EvaluationCtx;
import org.wso2.balana.ctx.Status;
import org.wso2.balana.finder.PolicyFinder;
import org.wso2.balana.finder.PolicyFinderModule;
import org.wso2.balana.finder.PolicyFinderResult;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.*;

/**
 * This class defines a string-based policy finder module. Policies are defined
 * as strings.
 */
public class XACMLPolicyFinderModule extends PolicyFinderModule {
	/**
	 * the policy finder instance.
	 */
	private PolicyFinder finder = null;
	/**
	 * set of abstract policies. The key of the map is the identifier of the
	 * policy.
	 */
	private Map<URI, AbstractPolicy> policies;
	/**
	 * set of content of the policies.
	 */
	private Set<String> setOfStringPolicies;
	/**
	 * the policy combining algorithm.
	 */
	private PolicyCombiningAlgorithm combiningAlg;

	/**
	 * the default constructor without any policy.
	 * 
	 * @param algo
	 *            the combining algorithm.
	 */
	public XACMLPolicyFinderModule(final PolicyCombiningAlgorithm algo) {
		policies = new HashMap<URI, AbstractPolicy>();
		setOfStringPolicies = new HashSet<String>();
		combiningAlg = algo;
	}

	/**
	 * @param setOfStringPolicies
	 *            the set of string-based policies.
	 * @param algo
	 *            the combining algorithm to be used.
	 */
	public XACMLPolicyFinderModule(final Set<String> setOfStringPolicies,
			final PolicyCombiningAlgorithm algo) {
		policies = new HashMap<URI, AbstractPolicy>();
		this.setOfStringPolicies = setOfStringPolicies;
		combiningAlg = algo;
	}

	@Override
	public void init(final PolicyFinder finder) {
		this.finder = finder;
		loadPolicies();
	}

	@Override
	public PolicyFinderResult findPolicy(final EvaluationCtx context) {
		// BrokerState state = BrokerState.getInstance();
		ArrayList<AbstractPolicy> selectedPolicies = new ArrayList<AbstractPolicy>();
		Set<Map.Entry<URI, AbstractPolicy>> entrySet = policies.entrySet();
		// iterate through all the policies we currently have loaded
		for (Map.Entry<URI, AbstractPolicy> entry : entrySet) {
			AbstractPolicy policy = entry.getValue();
			MatchResult match = policy.match(context);
			int result = match.getResult();
			// if target matching was indeterminate, then return the error
			if (result == MatchResult.INDETERMINATE)
				return new PolicyFinderResult(match.getStatus());
			// see if the target matched
			if (result == MatchResult.MATCH) {
				if ((combiningAlg == null) && (selectedPolicies.size() > 0)) {
					// we found a match before, so this is an error
					ArrayList<String> code = new ArrayList<String>();
					code.add(Status.STATUS_PROCESSING_ERROR);
					Status status = new Status(code,
							"too many applicable " + "top-level policies");
					return new PolicyFinderResult(status);
				}
				// this is the first match we've found, so remember it
				selectedPolicies.add(policy);
			}
		}
		// no errors happened during the search, so now take the right
		// action based on how many policies we found
		switch (selectedPolicies.size()) {
		case 0:
			// if (Log.ON && Log.ROUTING.isEnabledFor(Level.ERROR)) {
			// Log.ROUTING.warn(state.identity.getPath()
			// + ", No matching XACML policy found");
			// }
			return new PolicyFinderResult();
		case 1:
			return new PolicyFinderResult((selectedPolicies.get(0)));
		default:
			return new PolicyFinderResult(
					new PolicySet(null, combiningAlg, null, selectedPolicies));
		}
	}

	@Override
	public PolicyFinderResult findPolicy(final URI idReference, final int type,
			final VersionConstraints constraints,
			final PolicyMetaData parentMetaData) {
		AbstractPolicy policy = policies.get(idReference);
		if (policy != null) {
			if (type == PolicyReference.POLICY_REFERENCE) {
				if (policy instanceof Policy) {
					return new PolicyFinderResult(policy);
				}
			} else {
				if (policy instanceof PolicySet) {
					return new PolicyFinderResult(policy);
				}
			}
		}
		// if there was an error loading the policy, return the error
		ArrayList<String> code = new ArrayList<String>();
		code.add(Status.STATUS_PROCESSING_ERROR);
		Status status = new Status(code, "couldn't load referenced policy");
		return new PolicyFinderResult(status);
	}

	@Override
	public boolean isIdReferenceSupported() {
		return true;
	}

	@Override
	public boolean isRequestSupported() {
		return true;
	}

	/**
	 * resets the policies known to this module to those contained in the given
	 * set of strings.
	 *
	 */
	public void loadPolicies() {
		policies.clear();
		for (String policyContent : setOfStringPolicies) {
			loadPolicy(policyContent, finder);
		}
	}

	/**
	 * private helper that tries to load the given string-based policy, and
	 * returns null if any error occurs.
	 *
	 * @param stringPolicy
	 *            the string-based policy.
	 * @param finder
	 *            the policy finder.
	 * @return <code>AbstractPolicy</code>. the abstract policy.
	 */
	private AbstractPolicy loadPolicy(final String stringPolicy,
			final PolicyFinder finder) {
		BrokerState state = BrokerState.getInstance();
		AbstractPolicy policy = null;
		InputStream stream = null;
		try {
			// create the factory
			DocumentBuilderFactory factory = DocumentBuilderFactory
					.newInstance();
			factory.setIgnoringComments(true);
			factory.setNamespaceAware(true);
			factory.setValidating(false);
			InputStream is = new ByteArrayInputStream(stringPolicy.getBytes());
			// create a builder based on the factory & try to load the policy
			DocumentBuilder db = factory.newDocumentBuilder();
			Document doc = db.parse(is);
			// handle the policy, if it's a known type
			Element root = doc.getDocumentElement();
			String name = root.getLocalName();
			if (name.equals("Policy")) {
				policy = Policy.getInstance(root);
			} else if (name.equals("PolicySet")) {
				policy = PolicySet.getInstance(root, finder);
			}
		} catch (Exception e) {
			// just only logs
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.ERROR)) {
				Log.ROUTING.error(state.identity.getPath()
						+ ", No matching XACML policy found");
			}
		} finally {
			if (stream != null) {
				try {
					stream.close();
				} catch (IOException e) {
					if (Log.ON && Log.ROUTING.isEnabledFor(Level.ERROR)) {
						Log.ROUTING.error(state.identity.getPath()
								+ ", Error while closing input stream");
					}
				}
			}
		}
		if (policy != null) {
			policies.put(policy.getId(), policy);
		}
		return policy;
	}
}