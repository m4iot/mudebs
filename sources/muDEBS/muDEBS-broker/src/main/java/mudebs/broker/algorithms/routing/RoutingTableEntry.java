/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2017 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Léon Lim
 */
package mudebs.broker.algorithms.routing;

import java.net.URI;

import mudebs.common.algorithms.EntityType;
import mudebs.common.algorithms.OperationalMode;
import mudebs.common.algorithms.routing.MultiScopingSpecification;
import mudebs.common.filters.Filter;

/**
 * This class defines an entry of routing table of a muDEBS broker. The unique
 * identity of a routing table entry is its identifier.
 * 
 * @author Denis Conan
 **/
public class RoutingTableEntry {
	/**
	 * the operational mode of the subscription (global or local) or of the
	 * visibility filter (always local).
	 */
	private OperationalMode opMode;
	/**
	 * the identifier of the filter. This is the identifier provided by the
	 * client application.
	 */
	private String identifier;
	/**
	 * the set of sets of pairs (scope path, status).
	 */
	private MultiScopingSpecification phi;
	/**
	 * the filter.
	 */
	private Filter filter;
	/**
	 * the JavaScript content of the filter.
	 */
	private String filterContent;
	/**
	 * Entity type: broker or client, of this routing table.
	 */
	private EntityType type;
	/**
	 * URI of the broker to forward the publication matching the filter.
	 */
	private URI brokerOrClient;

	/**
	 * constructor.
	 * 
	 * @param mode
	 *            the operational mode of the filter (global or local). The
	 *            entries that correspond to subscription filters are local or
	 *            global, and the entries that corresponds to visibility filters
	 *            are local.
	 * @param id
	 *            the identifier of the filter.
	 * @param f
	 *            the filter of the routing table entry.
	 * @param fContent
	 *            the JavaScript content of the filter.
	 * @param phi
	 *            the set of scopes associated with the filter.
	 * @param t
	 *            the entity type.
	 * @param uri
	 *            the URI of the remote broker or client.
	 * @throws IllegalArgumentException
	 *             the exception thrown when null filter
	 */
	public RoutingTableEntry(final OperationalMode mode, final String id,
			final Filter f, final String fContent,
			final MultiScopingSpecification phi, final EntityType t,
			final URI uri) throws IllegalArgumentException {
		opMode = mode;
		if (id == null || id.equals("")) {
			throw new IllegalArgumentException(
					"Cannot create a routing table entry"
							+ " with a wrong identifier '" + id + "'");
		}
		identifier = id;
		if (f == null) {
			throw new IllegalArgumentException(
					"Cannot create a routing table entry with a null filter");
		}
		filter = f;
		filterContent = fContent;
		this.phi = phi;
		type = t;
		if (uri == null) {
			throw new IllegalArgumentException(
					"Cannot create a routing table entry with a null URI");
		}
		brokerOrClient = uri;
	}

	/**
	 * gets the boolean stating whether the filter is local. The entries that
	 * correspond to subscription filters are local or global, and the entries
	 * that corresponds to visibility filters are local.
	 * 
	 * @return the boolean
	 */
	public OperationalMode getOperationalMode() {
		return opMode;
	}

	/**
	 * gets the identifier of the routing table entry.
	 * 
	 * @return the identifier.
	 */
	public String getIdentifier() {
		return identifier;
	}

	/**
	 * gets the JavaScript content of the filter.
	 * 
	 * @return the JavaScript content of the filter.
	 */
	public String getFilterContent() {
		return filterContent;
	}

	/**
	 * gets the filter of the routing table entry.
	 * 
	 * @return the filter
	 */
	public Filter getFilter() {
		return filter;
	}

	/**
	 * get the set of sets of scope paths.
	 * 
	 * @return the multiscoping specification.
	 */
	public MultiScopingSpecification getPhi() {
		return phi;
	}

	/**
	 * gets the type (broker or client) of the routing table entry.
	 * 
	 * @return broker or client
	 */
	public EntityType getEntityType() {
		return type;
	}

	/**
	 * gets the URI of the broker or client of the routing table entry.
	 * 
	 * @return the broker or client
	 */
	public URI getBrokerOrClient() {
		return brokerOrClient;
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((identifier == null) ? 0 : identifier.hashCode());
		return result;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof RoutingTableEntry)) {
			return false;
		}
		RoutingTableEntry other = (RoutingTableEntry) obj;
		if (identifier == null) {
			if (other.identifier != null) {
				return false;
			}
		} else if (!identifier.equals(other.identifier)) {
			return false;
		}
		return true;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RoutingTableEntry [opMode="
				+ opMode
				+ ", identifier="
				+ identifier
				+ ", filter="
				+ filter
				+ ", filterContent="
				+ filterContent.substring(0, (filterContent.length() > 35) ? 35
						: filterContent.length() - 1) + "..., scopes=" + phi
				+ ", type=" + type + ", destination=" + brokerOrClient + "]";
	}
}
