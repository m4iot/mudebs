/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2017 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Léon Lim
 */
package mudebs.broker.algorithms.routing;

import java.net.URI;

import mudebs.common.algorithms.EntityType;
import mudebs.common.algorithms.OperationalMode;
import mudebs.common.algorithms.routing.MultiScopingSpecification;
import mudebs.common.algorithms.routing.Scope;
import mudebs.common.filters.Filter;

/**
 * This class defines an entry of routing table of a muDEBS broker when the
 * filter is a visibility filter.
 * 
 * @author Denis Conan
 **/
public class RoutingTableEntryVisibility extends RoutingTableEntry {
	/**
	 * the ending scope.
	 */
	private Scope endingScope;

	/**
	 * constructor.
	 * 
	 * @param mode
	 *            the operational mode of the filter (global or local). The
	 *            entries that correspond to subscription filters are local or
	 *            global, and the entries that corresponds to visibility filters
	 *            are local.
	 * @param id
	 *            the identifier of the filter.
	 * @param f
	 *            the filter of the routing table entry.
	 * @param fContent
	 *            the JavaScript content of the filter.
	 * @param phi
	 *            the set of scopes associated with the filter.
	 * @param t
	 *            the entity type.
	 * @param uri
	 *            the URI of the remote broker or client.
	 * @param endingScope
	 *            the ending scope, that is the scope after the application of
	 *            the routing entry.
	 * @throws IllegalArgumentException
	 *             the exception thrown when null filter
	 */
	public RoutingTableEntryVisibility(final OperationalMode mode,
			final String id, final Filter f, final String fContent,
			final MultiScopingSpecification phi, final EntityType t,
			final URI uri, final Scope endingScope)
			throws IllegalArgumentException {
		super(mode, id, f, fContent, phi, t, uri);
		if (endingScope == null) {
			throw new IllegalArgumentException(
					"Cannot create a routing table entry "
							+ "for visibility with a null endingScope");
		}
		this.endingScope = endingScope;
	}

	/**
	 * gets the ending scope.
	 * 
	 * @return the ending scope.
	 */
	public Scope getEndingScope() {
		return endingScope;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RoutingTableEntry [local=" + this.getOperationalMode()
				+ ", identifier=" + this.getIdentifier() + ", filter="
				+ this.getFilter() + ", filterContent="
				+ this.getFilterContent().substring(0,
						(this.getFilterContent().length() > 35) ? 35
								: this.getFilterContent().length() - 1)
				+ "..., scopes=" + this.getPhi() + ", endingScope="
				+ endingScope + ", type=" + this.getEntityType()
				+ ", destination=" + this.getBrokerOrClient() + "]";
	}
}
