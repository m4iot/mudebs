/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Léon Lim
 */
package mudebs.broker.algorithms.routing;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptException;

import mudebs.broker.BrokerState;
import mudebs.common.algorithms.routing.Scope;
import mudebs.common.filters.Filter;
import mudebs.common.filters.JavaScriptVisibilityFilter;

import org.w3c.dom.Document;

/**
 * This class defines a muDEBS visibility filter, which is a delegate of a
 * <tt>JavaScriptVisibilityFilter</tt>.
 * 
 * @author Denis Conan
 * @author Léon Lim
 * 
 */
public class VisibilityFilter implements Filter {
	/**
	 * the JavaScript visibility filter.
	 */
	JavaScriptVisibilityFilter delegate;
	Scope endingScope;

	/**
	 * constructs visibility filter.
	 * 
	 * @param f
	 *            the JavaScript filter.
	 * @param scope
	 *            the ending scope.
	 */
	public VisibilityFilter(String f, Scope scope) {
		BrokerState state = BrokerState.getInstance();
		ScriptEngine engine = state.scriptEngineManager
				.getEngineByName("JavaScript");
		try {
			engine.eval(f);
		} catch (ScriptException e) {

		}
		Invocable inv = (Invocable) engine;
		delegate = inv.getInterface(JavaScriptVisibilityFilter.class);
		endingScope = scope;
	}

	public Scope getEndingScope() {
		return endingScope;
	}

	@Override
	public boolean evaluate(Document publication) {
		return delegate.evaluate(publication);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "VisibilityFilter [delegate=" + delegate + ", endingScope="
				+ endingScope + "]";
	}
}
