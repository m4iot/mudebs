/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */
package mudebs.broker.algorithms.scripting.broker;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import mudebs.common.Constants;
import mudebs.common.Log;
import mudebs.common.algorithms.AbstractMessageContent;
import mudebs.common.algorithms.AlgorithmActionInterfaceToBeDeprecated;
import mudebs.common.algorithms.overlaymanagement.TerminationDetectionMsgContent;
import mudebs.common.algorithms.overlaymanagement.TerminationMsgContent;
import mudebs.common.algorithms.routing.AdminJoinScopeMsgContent;
import mudebs.common.algorithms.routing.AdminLeaveScopeMsgContent;
import mudebs.common.algorithms.routing.JoinScopeMsgContent;
import mudebs.common.algorithms.routing.LeaveScopeMsgContent;
import mudebs.common.algorithms.scripting.broker.ConnectToNeighbourMsgContent;
import mudebs.common.algorithms.scripting.broker.InformationRequestMsgContent;
import mudebs.common.algorithms.scripting.broker.SetLogRequestMsgContent;

import org.apache.log4j.Level;

/**
 * This Enumeration type declares the algorithm of the scripting part of the
 * client.
 * 
 * @author Denis Conan
 * 
 */
public enum AlgorithmScriptingBroker implements AlgorithmActionInterfaceToBeDeprecated {
	/**
	 * message to connect to a neighbouring broker.
	 */
	CONNECT_NEIGHBOUR(ConnectToNeighbourMsgContent.class) {
		public void execute(final AbstractMessageContent content) {
			AlgorithmScriptingActions.receiveConnectNeighbour(content);
		}
	},
	/**
	 * information message type for the scripting broker algorithm.
	 */
	INFORMATION_REQUEST(InformationRequestMsgContent.class) {
		public void execute(final AbstractMessageContent content) {
			AlgorithmScriptingActions.receiveInfRequest(content);
		}
	},
	/**
	 * set log request message type for the scripting broker algorithm.
	 */
	SETLOG_REQUEST(SetLogRequestMsgContent.class) {
		public void execute(final AbstractMessageContent content) {
			AlgorithmScriptingActions.receiveSetLogRequest(content);
		}
	},
	/**
	 * join scope message type for the scripting broker algorithm.
	 */
	JOIN_SCOPE(JoinScopeMsgContent.class) {
		public void execute(final AbstractMessageContent content) {
			AlgorithmScriptingActions.receiveJoinScope(content);
		}
	},
	/**
	 * admin join scope message type for the scripting broker algorithm.
	 */
	ADMIN_JOIN_SCOPE(AdminJoinScopeMsgContent.class) {
		public void execute(final AbstractMessageContent content) {
			if (Log.ON && Log.DISPATCH.isEnabledFor(Level.ERROR)) {
				Log.DISPATCH.error("a broker should not "
						+ "receive an admin join scope message"
						+ " from scripting broker");
			}
		}
	},
	/**
	 * leave scope message type for the scripting broker algorithm.
	 */
	LEAVE_SCOPE(LeaveScopeMsgContent.class) {
		public void execute(final AbstractMessageContent content) {
			AlgorithmScriptingActions.receiveLeaveScope(content);
		}
	},
	/**
	 * admin leave scope message type for the scripting broker algorithm.
	 */
	ADMIN_LEAVE_SCOPE(AdminLeaveScopeMsgContent.class) {
		public void execute(final AbstractMessageContent content) {
			if (Log.ON && Log.DISPATCH.isEnabledFor(Level.ERROR)) {
				Log.DISPATCH.error("a broker should not "
						+ "receive an admin leave scope message"
						+ " from scripting broker");
			}
		}
	},
	/**
	 * termination detection message for the overlay network of brokers.
	 */
	TERMINATION_DETECTION(TerminationDetectionMsgContent.class) {
		public void execute(final AbstractMessageContent content) {
			AlgorithmScriptingActions
					.receiveTerminationDetection(content);
		}
	},
	/**
	 * termination message for the overlay network of brokers.
	 */
	TERMINATION(TerminationMsgContent.class) {
		public void execute(final AbstractMessageContent content) {
			AlgorithmScriptingActions
					.receiveTermination(content);
		}
	};

	/**
	 * index of the first message type of this algorithm.
	 */
	public final int algorithmOffset = Constants.SCRIPTING_BROKER_START_INDEX;
	/**
	 * collection of the actions of this algorithm. The Key is the index of the
	 * corresponding message type. This collection is kept private to avoid
	 * modifications.
	 */
	private final static Map<Integer, AlgorithmScriptingBroker> privateMapOfActions;
	/**
	 * unmodifiable collection corresponding to <tt>privateMapOfActions</tt>.
	 */
	public final static Map<Integer, AlgorithmScriptingBroker> mapOfActions;
	/**
	 * index of the action of this message type.
	 */
	private final int actionIndex;
	/**
	 * class of the content of the message received.
	 */
	private final Class<? extends AbstractMessageContent> msgContentClass;

	/**
	 * static block to build collections of actions.
	 */
	static {
		privateMapOfActions = new HashMap<Integer, AlgorithmScriptingBroker>();
		for (AlgorithmScriptingBroker aa : AlgorithmScriptingBroker.values()) {
			privateMapOfActions.put(aa.actionIndex, aa);
		}
		mapOfActions = Collections.unmodifiableMap(privateMapOfActions);
	}

	/**
	 * constructor of message type object.
	 * 
	 * @param msgClass
	 *            the class of the content of the message received.
	 */
	private AlgorithmScriptingBroker(
			final Class<? extends AbstractMessageContent> msgClass) {
		this.actionIndex = Constants.MESSAGE_TYPE_START_INDEX + algorithmOffset
				+ ordinal();
		this.msgContentClass = msgClass;
	}

	/**
	 * obtains the index of this message type.
	 */
	public int getActionIndex() {
		return actionIndex;
	}

	/**
	 * obtains the class of the message received in this action.
	 */
	public Class<? extends AbstractMessageContent> getMsgContentClass() {
		return msgContentClass;
	}

	@Override
	public String toString() {
		return String.valueOf(actionIndex);
	}
}
