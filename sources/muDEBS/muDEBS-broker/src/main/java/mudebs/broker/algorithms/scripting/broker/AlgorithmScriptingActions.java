/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */
package mudebs.broker.algorithms.scripting.broker;

import java.io.IOException;
import java.net.URI;
import java.util.HashMap;

import mudebs.broker.BrokerState;
import mudebs.broker.algorithms.overlaymanagement.AlgorithmOverlayManagement;
import mudebs.broker.algorithms.overlaymanagement.TerminationDetectionState;
import mudebs.broker.algorithms.routing.AlgorithmRoutingActions;
import mudebs.common.Constants;
import mudebs.common.Log;
import mudebs.common.algorithms.AbstractMessageContent;
import mudebs.common.algorithms.EntityType;
import mudebs.common.algorithms.overlaymanagement.TerminationDetectionMsgContent;
import mudebs.common.algorithms.overlaymanagement.TerminationMsgContent;
import mudebs.common.algorithms.routing.Scope;
import mudebs.common.algorithms.routing.ScopeGraph;
import mudebs.common.algorithms.scripting.broker.ConnectToNeighbourMsgContent;
import mudebs.common.algorithms.scripting.broker.InformationRequestMsgContent;
import mudebs.common.algorithms.scripting.broker.SetLogRequestMsgContent;

import org.apache.log4j.Level;

/**
 * This class defines the methods implementing the reaction concerning the
 * reception of scripting messages.
 * 
 * @author Denis Conan
 * 
 */
class AlgorithmScriptingActions {

	public static void receiveConnectNeighbour(AbstractMessageContent content) {
		BrokerState state = BrokerState.getInstance();
		ConnectToNeighbourMsgContent mymsg = (ConnectToNeighbourMsgContent) content;
		if (state.brokerIdentities.containsKey(mymsg.getUIRNeighbour())) {
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.ERROR)) {
				Log.ROUTING.error(state.identity.getPath()
						+ ", the broker is already a neighbour ("
						+ mymsg.getUIRNeighbour() + ")");
			}
			return;
		}
		try {
			state.broker.connectToBroker(mymsg.getUIRNeighbour());
		} catch (IOException e) {
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.ERROR)) {
				Log.ROUTING.error(state.identity.getPath()
						+ ", pb when trying to connect to broker ("
						+ mymsg.getUIRNeighbour() + ") = " + e.getMessage());
			}
		}
	}

	public static void receiveInfRequest(final AbstractMessageContent content) {
		BrokerState state = BrokerState.getInstance();
		InformationRequestMsgContent mymsg = (InformationRequestMsgContent) content;
		if (mymsg.getWhichInfo().equals(Constants.OPTION_INFO_SCOPE_GRAPHS)
				|| mymsg.getWhichInfo().equals(Constants.OPTION_INFO_ALL)) {
			System.out.print(state.identity.getPath()
					+ ", the scope graphs are: ");
			if (state.scopeGraphs.isEmpty()) {
				System.out.println("[empty]");
			} else {
				System.out.println();
				for (ScopeGraph sg : state.scopeGraphs.values()) {
					System.out.println(sg);
				}
			}
		}
		if (mymsg.getWhichInfo().equals(Constants.OPTION_INFO_SLTS)
				|| mymsg.getWhichInfo().equals(Constants.OPTION_INFO_ALL)) {
			System.out.print(state.identity.getPath()
					+ ", the scope lookup tables are: ");
			if (state.scopeLookupTables.isEmpty()) {
				System.out.println("[empty]");
			} else {
				System.out.println();
				for (Scope scope : state.scopeLookupTables.keySet()) {
					System.out.println("scope " + scope.dimension() + "/"
							+ scope.identifier() + ": "
							+ state.scopeLookupTables.get(scope));
				}
			}
		}
		if (mymsg.getWhichInfo().equals(Constants.OPTION_INFO_RTS)
				|| mymsg.getWhichInfo().equals(Constants.OPTION_INFO_ALL)) {
			System.out.print(state.identity.getPath()
					+ ", the broker routing table: ");
			if (state.brokerRoutingTables.isEmpty()) {
				System.out.println("[empty]");
			} else {
				System.out.println();
				for (URI uri : state.brokerRoutingTables.keySet()) {
					System.out.println(uri + ": "
							+ state.brokerRoutingTables.get(uri));
				}
			}
			System.out.print(state.identity.getPath()
					+ ", the client routing table: ");
			if (state.clientRoutingTables.isEmpty()) {
				System.out.println("[empty]");
			} else {
				System.out.println();
				for (URI uri : state.clientRoutingTables.keySet()) {
					System.out.println(uri + ": "
							+ state.clientRoutingTables.get(uri));
				}
			}
		}
	}

	public static void receiveSetLogRequest(AbstractMessageContent content) {
		SetLogRequestMsgContent mymsg = (SetLogRequestMsgContent) content;
		Log.configureALogger(mymsg.getLoggerName(), mymsg.getLogLevel());
	}

	public static void receiveJoinScope(AbstractMessageContent content) {
		AlgorithmRoutingActions.receiveJoinScope(content);
	}

	public static void receiveLeaveScope(AbstractMessageContent content) {
		AlgorithmRoutingActions.receiveLeaveScope(content);
	}

	public static void receiveTerminationDetection(
			AbstractMessageContent content) {
		BrokerState state = BrokerState.getInstance();
		if (state.currIdentityType != EntityType.SCRIPTING) {
			if (Log.ON && Log.OVERLAY.isEnabledFor(Level.WARN)) {
				Log.OVERLAY.warn(state.identity.getPath()
						+ ", termination detection message not"
						+ " received from a scripting broker");
			}
			return;
		}
		HashMap<Integer, TerminationDetectionState> term = state.termWaves
				.get(state.identity);
		if (term == null) {
			term = new HashMap<Integer, TerminationDetectionState>();
			state.termWaves.put(state.identity,
					term);
		}
		TerminationDetectionState termState = new TerminationDetectionState(
				state.identity);
		state.lastWaveNumber++;
		term.put(new Integer(state.lastWaveNumber), termState);
		if (Log.ON && Log.OVERLAY.isEnabledFor(Level.DEBUG)) {
			Log.OVERLAY.debug(state.identity.getPath()
					+ ", start termination detection: wave number="
					+ state.lastWaveNumber);
		}
		TerminationDetectionMsgContent toSend = new TerminationDetectionMsgContent(
				state.identity, state.identity, state.lastWaveNumber, 0,
				termState.color, null);
		for (URI uri : state.brokerIdentities.keySet()) {
			try {
				state.broker.sendToABroker(state.brokerIdentities.get(uri),
						AlgorithmOverlayManagement.TERMINATION_DETECTION
								.getActionIndex(), toSend);
			} catch (IOException e) {
				if (Log.ON && Log.ROUTING.isEnabledFor(Level.ERROR)) {
					Log.ROUTING.error(state.identity.getPath() + ", "
							+ e.getMessage() + "\n"
							+ Log.printStackTrace(e.getStackTrace()));
				}
			}
		}
	}

	public static void receiveTermination(AbstractMessageContent content) {
		BrokerState state = BrokerState.getInstance();
		TerminationMsgContent mymsg = (TerminationMsgContent) content;
		if (state.currIdentityType != EntityType.SCRIPTING) {
			if (Log.ON && Log.OVERLAY.isEnabledFor(Level.WARN)) {
				Log.OVERLAY.warn(state.identity.getPath()
						+ ", termination message not"
						+ " received from a scripting broker");
			}
			return;
		}
		TerminationMsgContent toSend = new TerminationMsgContent(
				state.identity, mymsg.getForwardToClients());
		for (URI uri : state.brokerIdentities.keySet()) {
			try {
				state.broker.sendToABroker(state.brokerIdentities.get(uri),
						AlgorithmOverlayManagement.TERMINATION
								.getActionIndex(), toSend);
			} catch (IOException e) {
				if (Log.ON && Log.ROUTING.isEnabledFor(Level.ERROR)) {
					Log.ROUTING.error(state.identity.getPath() + ", "
							+ e.getMessage() + "\n"
							+ Log.printStackTrace(e.getStackTrace()));
				}
			}
		}
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// nop
		}
		System.exit(0);
	}
}
