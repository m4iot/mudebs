/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2017 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Léon Lim
 */
package mudebs.broker.algorithms.routing;

import java.net.URI;

import mudebs.common.algorithms.OperationalMode;
import mudebs.common.algorithms.routing.MultiScopingSpecification;
import mudebs.common.filters.Filter;

/**
 * This class defines an entry of advertisement table of a muDEBS broker.
 * Advertisement is uniquely identified by the URI of the client plus an
 * identifier provided by the client.
 * 
 * @author Denis Conan
 */
public class AdvertisementTableEntry {

	/**
	 * the identifier of the advertisement.
	 */
	private String identifier;
	/**
	 * the operational mode of advertisement (global or local).
	 */
	private OperationalMode opMode;
	/**
	 * the filter.
	 */
	private Filter filter;
	/**
	 * the set of sets of scope paths.
	 */
	private MultiScopingSpecification phiNotif;
	/**
	 * the XACML policy.
	 */
	private String policyContent;
	/**
	 * the URI of the client that submit the advertisement.
	 */
	private URI client;
	/**
	 * the content of the last publication or the last pull call to the client.
	 * This content may be not null only when this is a local advertisement.
	 */
	private String lastContent;

	/**
	 * constructor.
	 * 
	 * @param f
	 *            the filter of the advertisement.
	 * @param p
	 *            the XACML policy.
	 * @param uri
	 *            the URI of the client.
	 * @param mode
	 *            the operational mode of the advertisement (global or local).
	 * @param id
	 *            the identifier of the advertisement.
	 * @param phiNotif
	 *            the multiscoping specification.
	 * @throws IllegalArgumentException
	 *             the exception thrown when null filter.
	 */
	public AdvertisementTableEntry(final Filter f, final String p,
			final URI uri, final OperationalMode mode, final String id,
			final MultiScopingSpecification phiNotif)
			throws IllegalArgumentException {
		if (f == null) {
			throw new IllegalArgumentException(
					"Cannot create an advertisement table entry"
							+ " with a null filter");
		}
		filter = f;
		this.phiNotif = phiNotif;
		policyContent = p;
		if (uri == null) {
			throw new IllegalArgumentException(
					"Cannot create an advertisement table entry"
							+ " with a null URI");
		}
		client = uri;
		if (id == null) {
			throw new IllegalArgumentException(
					"Cannot create an advertisement table entry"
							+ " with a null identifier");
		}
		opMode = mode;
		identifier = id;
		lastContent = null;
	}

	/**
	 * gets the filter of the advertisement table entry.
	 * 
	 * @return the filter
	 */
	public Filter getFilter() {
		return filter;
	}

	/**
	 * gets the policy content.
	 * 
	 * @return the policy
	 */
	public String getPolicyContent() {
		return policyContent;
	}

	/**
	 * gets the URI of the client.
	 * 
	 * @return the broker or client
	 */
	public URI getClient() {
		return client;
	}

	/**
	 * gets the operational mode of the advertisement (global or local).
	 * 
	 * @return the boolean
	 */
	public OperationalMode getOperationalMode() {
		return opMode;
	}

	/**
	 * gets the identifier of the advertisement.
	 * 
	 * @return the identifier
	 */
	public String getIdentifier() {
		return identifier;
	}

	/**
	 * gets the set of sets of scope paths.
	 * 
	 * @return the set of scopes associated with advertisement.
	 */
	public MultiScopingSpecification getPhiNotif() {
		return phiNotif;
	}

	/**
	 * gets the last content.
	 * 
	 * @return the last content
	 */
	public String getLastContent() {
		return lastContent;
	}

	/**
	 * sets the last content.
	 * 
	 * @param content
	 *            the last content.
	 */
	public void setLastContent(String content) {
		lastContent = content;
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((client == null) ? 0 : client.hashCode());
		result = prime * result
				+ ((identifier == null) ? 0 : identifier.hashCode());
		return result;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof AdvertisementTableEntry)) {
			return false;
		}
		AdvertisementTableEntry other = (AdvertisementTableEntry) obj;
		if (client == null) {
			if (other.client != null) {
				return false;
			}
		} else if (!client.equals(other.client)) {
			return false;
		}
		if (identifier == null) {
			if (other.identifier != null) {
				return false;
			}
		} else if (!identifier.equals(other.identifier)) {
			return false;
		}
		return true;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AdvertisementTableEntry [identifier=" + identifier + ", opMode="
				+ opMode + ", filter=" + filter + ", phiNotif" + phiNotif
				+ ", policyContent=" + policyContent + ", client=" + client
				+ ", lastContent=" + lastContent + "]";
	}
}
