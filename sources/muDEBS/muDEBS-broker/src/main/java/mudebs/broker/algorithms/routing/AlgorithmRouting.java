/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */
package mudebs.broker.algorithms.routing;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import mudebs.common.Constants;
import mudebs.common.Log;
import mudebs.common.algorithms.AbstractMessageContent;
import mudebs.common.algorithms.AlgorithmActionInterfaceToBeDeprecated;
import mudebs.common.algorithms.routing.AcknowledgementMsgContent;
import mudebs.common.algorithms.routing.AdminAcknowledgementMsgContent;
import mudebs.common.algorithms.routing.AdminAdvertisementMsgContent;
import mudebs.common.algorithms.routing.CollectiveAdminSubscriptionMsgContent;
import mudebs.common.algorithms.routing.AdminJoinScopeMsgContent;
import mudebs.common.algorithms.routing.AdminLeaveScopeMsgContent;
import mudebs.common.algorithms.routing.AdminPublicationMsgContent;
import mudebs.common.algorithms.routing.AdminReplyMsgContent;
import mudebs.common.algorithms.routing.AdminRequestMsgContent;
import mudebs.common.algorithms.routing.AdminSubscriptionMsgContent;
import mudebs.common.algorithms.routing.AdminUnadvertisementMsgContent;
import mudebs.common.algorithms.routing.AdminUnsubscriptionMsgContent;
import mudebs.common.algorithms.routing.AdvertisementMsgContent;
import mudebs.common.algorithms.routing.CollectiveAdminPublicationMsgContent;
import mudebs.common.algorithms.routing.CollectivePublicationMsgContent;
import mudebs.common.algorithms.routing.CollectiveSubscriptionMsgContent;
import mudebs.common.algorithms.routing.GetScopesMsgContent;
import mudebs.common.algorithms.routing.JoinScopeMsgContent;
import mudebs.common.algorithms.routing.LeaveScopeMsgContent;
import mudebs.common.algorithms.routing.PublicationMsgContent;
import mudebs.common.algorithms.routing.ReplyMsgContent;
import mudebs.common.algorithms.routing.RequestMsgContent;
import mudebs.common.algorithms.routing.SubscriptionMsgContent;
import mudebs.common.algorithms.routing.UnadvertisementMsgContent;
import mudebs.common.algorithms.routing.UnsubscriptionMsgContent;

import org.apache.log4j.Level;

/**
 * This Enumeration type declares the algorithm of the routing part of the
 * broker.
 * 
 * @author Denis Conan
 * 
 */
public enum AlgorithmRouting implements AlgorithmActionInterfaceToBeDeprecated {
	/**
	 * advertisement message type for the routing algorithm.
	 */
	ADVERTISEMENT(AdvertisementMsgContent.class) {
		public void execute(final AbstractMessageContent content) {
			try {
				AlgorithmRoutingActions.receiveAdvertisement(content);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	},
	/**
	 * publication message type for the routing algorithm.
	 */
	PUBLICATION(PublicationMsgContent.class) {
		public void execute(final AbstractMessageContent content) {
			AlgorithmRoutingActions.receivePublication(content);
		}
	},
	/**
	 * collective publication message type for the routing algorithm.
	 */
	COLLECTIVE_PUBLICATION(CollectivePublicationMsgContent.class) {
		public void execute(final AbstractMessageContent content) {
			AlgorithmRoutingActions.receiveCollectivePublication(content);
		}
	},
	/**
	 * publication message type for the routing algorithm.
	 */
	ADMIN_PUBLICATION(AdminPublicationMsgContent.class) {
		public void execute(final AbstractMessageContent content) {
			AlgorithmRoutingActions.receiveAdminPublication(content);
		}
	},
	/**
	 * collective publication message type for the routing algorithm.
	 */
	COLLECTIVE_ADMIN_PUBLICATION(CollectiveAdminPublicationMsgContent.class) {
		public void execute(final AbstractMessageContent content) {
			AlgorithmRoutingActions.receiveCollectiveAdminPublication(content);
		}
	},
	/**
	 * request message type for the routing algorithm.
	 */
	REQUEST(RequestMsgContent.class) {
		public void execute(final AbstractMessageContent content) {
			try {
				AlgorithmRoutingActions.receiveRequest(content);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	},
	/**
	 * admin request message type for the routing algorithm.
	 */
	ADMIN_REQUEST(AdminRequestMsgContent.class) {
		public void execute(final AbstractMessageContent content) {
			try {
				AlgorithmRoutingActions.receiveAdminRequest(content);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	},
	/**
	 * response message type for the routing algorithm.
	 */
	REPLY(ReplyMsgContent.class) {
		public void execute(final AbstractMessageContent content) {
			if (Log.ON && Log.DISPATCH.isEnabledFor(Level.ERROR)) {
				Log.DISPATCH
						.error("a broker should not " + "receive any reply");
			}
		}
	},
	/**
	 * admin reply message type for the routing algorithm.
	 */
	ADMIN_REPLY(AdminReplyMsgContent.class) {
		public void execute(final AbstractMessageContent content) {
			AlgorithmRoutingActions.receiveAdminReply(content);
		}
	},
	/**
	 * client subscription message type for the routing algorithm.
	 */
	SUBSCRIPTION(SubscriptionMsgContent.class) {
		public void execute(final AbstractMessageContent content) {
			AlgorithmRoutingActions.receiveSubscription(content);
		}
	},
	/**
	 * client collective subscription message type for the routing algorithm.
	 */
	COLLECTIVE_SUBSCRIPTION(CollectiveSubscriptionMsgContent.class) {
		public void execute(final AbstractMessageContent content) {
			AlgorithmRoutingActions.receiveCollectiveSubscription(content);
		}
	},
	/**
	 * broker subscription message type for the routing algorithm.
	 */
	ADMIN_SUBSCRIPTION(AdminSubscriptionMsgContent.class) {
		public void execute(final AbstractMessageContent content) {
			AlgorithmRoutingActions.receiveAdminSubscription(content);
		}
	},
	/**
	 * broker subscription message type for the routing algorithm.
	 */
	ADMIN_COLLECTIVE_SUBSCRIPTION(CollectiveAdminSubscriptionMsgContent.class) {
		public void execute(final AbstractMessageContent content) {
			AlgorithmRoutingActions.receiveAdminCollectiveSubscription(content);
		}
	},
	/**
	 * unadvertisement message type for the routing algorithm.
	 */
	UNADVERTISEMENT(UnadvertisementMsgContent.class) {
		public void execute(final AbstractMessageContent content) {
			AlgorithmRoutingActions.receiveUnadvertisement(content);
		}
	},
	/**
	 * unsubscription message type for the routing algorithm.
	 */
	UNSUBSCRIPTION(UnsubscriptionMsgContent.class) {
		public void execute(final AbstractMessageContent content) {
			AlgorithmRoutingActions.receiveUnsubscription(content);
		}
	},
	/**
	 * broker unsubscription message type for the routing algorithm.
	 */
	ADMIN_UNSUBSCRIPTION(AdminUnsubscriptionMsgContent.class) {
		public void execute(final AbstractMessageContent content) {
			AlgorithmRoutingActions.receiveAdminUnsubscription(content);
		}
	},
	/**
	 * broker advertisement message type for the routing algorithm.
	 */
	ADMIN_ADVERTISEMENT(AdminAdvertisementMsgContent.class) {
		public void execute(final AbstractMessageContent content) {
			try {
				AlgorithmRoutingActions.receiveAdminAdvertisement(content);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	},
	/**
	 * broker unadvertisement message type for the routing algorithm.
	 */
	ADMIN_UNADVERTISEMENT(AdminUnadvertisementMsgContent.class) {
		public void execute(final AbstractMessageContent content) {
			AlgorithmRoutingActions.receiveAdminUnadvertisement(content);
		}
	},
	/**
	 * acknowledgement message type for the routing algorithm.
	 */
	ACKNOWLEDGEMENT(AcknowledgementMsgContent.class) {
		public void execute(final AbstractMessageContent content) {
			if (Log.ON && Log.DISPATCH.isEnabledFor(Level.ERROR)) {
				Log.DISPATCH.error("a broker should not "
						+ "receive any acknowledgement");
			}
		}
	},
	/**
	 * broker acknowledgement message type for the routing algorithm.
	 */
	ADMIN_ACKNOWLEDGEMENT(AdminAcknowledgementMsgContent.class) {
		public void execute(final AbstractMessageContent content) {
			AlgorithmRoutingActions.receiveAdminAcknowledgement(content);
		}
	},
	/**
	 * admin join scope message type for the join scope algorithm.
	 */
	JOIN_SCOPE(JoinScopeMsgContent.class) {
		public void execute(final AbstractMessageContent content) {
			AlgorithmRoutingActions.receiveJoinScope(content);
		}
	},
	/**
	 * join scope message type for the join scope algorithm.
	 */
	ADMIN_JOIN_SCOPE(AdminJoinScopeMsgContent.class) {
		public void execute(final AbstractMessageContent content) {
			AlgorithmRoutingActions.receiveAdminJoinScope(content);
		}
	},
	/**
	 * leave scope message type for the leave scope algorithm.
	 */
	LEAVE_SCOPE(LeaveScopeMsgContent.class) {
		public void execute(final AbstractMessageContent content) {
			AlgorithmRoutingActions.receiveLeaveScope(content);
		}
	},
	/**
	 * admin leave scope message type for the leave scope algorithm.
	 */
	ADMIN_LEAVE_SCOPE(AdminLeaveScopeMsgContent.class) {
		public void execute(final AbstractMessageContent content) {
			AlgorithmRoutingActions.receiveAdminLeaveScope(content);
		}
	},
	/**
	 * get scope scope message type for asking for the set of known scopes.
	 */
	GET_SCOPES(GetScopesMsgContent.class) {
		public void execute(final AbstractMessageContent content) {
			AlgorithmRoutingActions.receiveGetScopes(content);
		}
	};

	/**
	 * index of the first message type of this algorithm.
	 */
	public final int algorithmOffset = Constants.ROUTING_START_INDEX;
	/**
	 * collection of the actions of this algorithm. The Key is the index of the
	 * corresponding message type. This collection is kept private to avoid
	 * modifications.
	 */
	private final static Map<Integer, AlgorithmRouting> privateMapOfActions;
	/**
	 * unmodifiable collection corresponding to <tt>privateMapOfActions</tt>.
	 */
	public final static Map<Integer, AlgorithmRouting> mapOfActions;
	/**
	 * index of the action of this message type.
	 */
	private final int actionIndex;
	/**
	 * class of the content of the message received.
	 */
	private final Class<? extends AbstractMessageContent> msgContentClass;

	/**
	 * static block to build collections of actions.
	 */
	static {
		privateMapOfActions = new HashMap<Integer, AlgorithmRouting>();
		for (AlgorithmRouting aa : AlgorithmRouting.values()) {
			privateMapOfActions.put(aa.actionIndex, aa);
		}
		mapOfActions = Collections.unmodifiableMap(privateMapOfActions);
	}

	/**
	 * constructor of message type object.
	 * 
	 * @param msgClass
	 *            the class of the content of the message received.
	 */
	private AlgorithmRouting(
			final Class<? extends AbstractMessageContent> msgClass) {
		this.actionIndex = Constants.MESSAGE_TYPE_START_INDEX + algorithmOffset
				+ ordinal();
		this.msgContentClass = msgClass;
	}

	/**
	 * obtains the index of this message type.
	 */
	public int getActionIndex() {
		return actionIndex;
	}

	/**
	 * obtains the class of the message received in this action.
	 */
	public Class<? extends AbstractMessageContent> getMsgContentClass() {
		return msgContentClass;
	}

	@Override
	public String toString() {
		return String.valueOf(actionIndex);
	}
}
