/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Christian Bac and Denis Conan
Contributor(s):
 */
package mudebs.broker;


/**
 * This class defines the main of the broker.
 * 
 * @author Denis Conan
 * 
 */
public class BrokerMain {

	/**
	 * the main method that creates an instance of the broker and then connects
	 * the broker to other brokers, whose connection data are given provided as
	 * arguments of the main.
	 * 
	 * @param argv
	 *            java command arguments.
	 * @throws Exception
	 *             exception thrown in case of problem.
	 */
	public static void main(final String[] argv) throws Exception {
		Broker broker = new Broker(argv);
		// do not call start since the broker is not multi-threaded
		broker.run();
		broker.forceTermination();
	}
}
