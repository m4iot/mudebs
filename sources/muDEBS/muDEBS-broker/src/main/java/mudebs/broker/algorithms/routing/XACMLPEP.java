/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Léon Lim
Contributor(s): Denis Conan
 */
package mudebs.broker.algorithms.routing;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;

import mudebs.broker.BrokerState;
import mudebs.common.Log;

import org.apache.log4j.Level;
import org.wso2.balana.Balana;
import org.wso2.balana.PDP;
import org.wso2.balana.PDPConfig;
import org.wso2.balana.combine.xacml3.PermitOverridesPolicyAlg;
import org.wso2.balana.finder.PolicyFinder;
import org.wso2.balana.finder.PolicyFinderModule;

/**
 * This class implements a basic PEP.
 * 
 * @author Léon Lim
 * @author Denis Conan
 *
 */
public class XACMLPEP {
	/**
	 * the pdp.
	 */
	private PDP pdp;
	/**
	 * the set of policies.
	 */
	private Set<String> policies;
	/**
	 * the simple default policy.
	 */
	@SuppressWarnings("unused")
	private String simplePolicy = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
			+ "<Policy xmlns=\"urn:oasis:names:tc:xacml:3.0:core:schema:wd-17\" "
			+ "xmlns:xacml =\"urn:oasis:names:tc:xacml:3.0:core:schema:wd-17\" "
			+ "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" "
			+ "xsi:schemaLocation=\"urn:oasis:names:tc:xacml:3.0:core:schema:wd-17 "
			+ "http://docs.oasis-open.org/xacml/3.0/xacml-core-v3-schema-wd-17.xsd\" "
			+ "xmlns:md=\"http:www.med.example.com/schemas/record.xsd\" "
			+ "PolicyId=\"urn:oasis:names:tc:xacml:3.0:example:simple:policyid:1122334455\" "
			+ "Version=\"1.0\" "
			+ "RuleCombiningAlgId=\"urn:oasis:names:tc:xacml:3.0:rule-combining-algorithm:permit-overrides\">"
			+ "<Description>Test policy</Description><Target/>"
			+ "<Rule RuleId=\"urn:oasis:names:tc:xacml:3.0:example:simple:ruleid:1\"  "
			+ "Effect=\"Permit\"><Description>A simple policy defined by one attribute</Description>"
			+ "<Target>"
			// first attribute ("category1","INCOME","id1")
			+ "<AnyOf><AllOf><Match MatchId=\"urn:oasis:names:tc:xacml:1.0:function:string-equal\">"
			+ "<AttributeValue DataType=\"http://www.w3.org/2001/XMLSchema#string\">INCOME"
			+ "</AttributeValue><AttributeDesignator MustBePresent=\"false\" "
			+ "Category=\"category1\" AttributeId=\"mudebs://localhost:port/xacml/attribute-id/id1\" "
			+ "DataType=\"http://www.w3.org/2001/XMLSchema#string\"/></Match></AllOf>"
			+ "</AnyOf>"
			// second attribute ("category2","PersonalUse","id2")
			+ "<AnyOf><AllOf><Match MatchId=\"urn:oasis:names:tc:xacml:1.0:function:string-equal\">"
			+ "<AttributeValue DataType=\"http://www.w3.org/2001/XMLSchema#string\">PersonalUse"
			+ "</AttributeValue><AttributeDesignator MustBePresent=\"false\" "
			+ "Category=\"category2\" AttributeId=\"mudebs://localhost:port/xacml/attribute-id/id2\" "
			+ "DataType=\"http://www.w3.org/2001/XMLSchema#string\"/></Match></AllOf>"
			+ "</AnyOf>"
			// third attribute ("category3","abcd","id3")
			+ "<AnyOf><AllOf><Match MatchId=\"urn:oasis:names:tc:xacml:1.0:function:string-equal\">"
			+ "<AttributeValue DataType=\"http://www.w3.org/2001/XMLSchema#string\">abcd"
			+ "</AttributeValue><AttributeDesignator MustBePresent=\"false\" "
			+ "Category=\"category3\" AttributeId=\"mudebs://localhost:port/xacml/attribute-id/id3\" "
			+ "DataType=\"http://www.w3.org/2001/XMLSchema#string\"/></Match></AllOf>"
			+ "</AnyOf>"
			// fourth attribute ("category4","access","id4")
			+ "<AnyOf><AllOf><Match MatchId=\"urn:oasis:names:tc:xacml:1.0:function:string-equal\">"
			+ "<AttributeValue DataType=\"http://www.w3.org/2001/XMLSchema#string\">access"
			+ "</AttributeValue><AttributeDesignator MustBePresent=\"false\" "
			+ "Category=\"category4\" AttributeId=\"mudebs://localhost:port/xacml/attribute-id/id4\" "
			+ "DataType=\"http://www.w3.org/2001/XMLSchema#string\"/></Match></AllOf>"
			+ "</AnyOf>"
			+ "</Target>"
			+ "</Rule><Rule RuleId=\"urn:oasis:names:tc:xacml:3.0:example:simple:ruleid:2:default\" "
			+ "Effect=\"Deny\"><Target/></Rule></Policy>";

	/**
	 * construct a XACMLPEP with the default policy. Several policies can be
	 * defined and added to the XACMLPEP.
	 * 
	 */
	public XACMLPEP() {
		BrokerState bstate = BrokerState.getInstance();
		policies = new HashSet<String>();
		String line = null;
		String defautPolicy = "";
		try {
			InputStream stream = Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("xacml/basic/defaultPolicy.xml");
			if (stream != null) {
				BufferedReader br = new BufferedReader(new InputStreamReader(
						stream));
				while ((line = br.readLine()) != null) {
					defautPolicy += line;
				}
				stream.close();
			}
		} catch (FileNotFoundException ex) {
			if (Log.ON && Log.CONFIG.isEnabledFor(Level.WARN)) {
				Log.CONFIG.warn(bstate.identity.getPath()
						+ ", unable to open file 'defaultPolicy.xml'");
			}
		} catch (IOException ex) {
			if (Log.ON && Log.CONFIG.isEnabledFor(Level.WARN)) {
				Log.CONFIG.warn(bstate.identity.getPath()
						+ ", error reading file 'defaultPolicy.xml'");
			}
		}
		policies.add(defautPolicy);
		pdp = getPDPNewInstance(policies);
	}

	/**
	 * adds a policy into the PDP by setting the XACML schema from a string.
	 * 
	 * @param policyToAdd
	 *            the policy to add.
	 */
	public void addPolicy(String policyToAdd) {
		policies.add(policyToAdd);
		pdp = getPDPNewInstance(policies);
	}

	/**
	 * evaluates a request contained in an DOM structure.
	 * 
	 * @param request
	 *            the content of the DOM structure containing the request.
	 * @return the string result.
	 * @throws Exception
	 *             the exception thrown in case of problem.
	 */
	public String evaluate(final String request) throws Exception {
		return pdp.evaluate(request);
	}

	/**
	 * returns a new PDP instance with new XACML policies
	 *
	 * @param policies
	 *            Set of XACML policies.
	 * @return a PDP instance
	 */
	private PDP getPDPNewInstance(Set<String> policies) {
		PolicyFinder finder = new PolicyFinder();
		XACMLPolicyFinderModule xacmPolicyFinderModule = new XACMLPolicyFinderModule(
				policies, new PermitOverridesPolicyAlg());
		Set<PolicyFinderModule> policyModules = new HashSet<PolicyFinderModule>();
		policyModules.add(xacmPolicyFinderModule);
		finder.setModules(policyModules);
		Balana balana = Balana.getInstance();
		PDPConfig pdpConfig = balana.getPdpConfig();
		pdpConfig = new PDPConfig(pdpConfig.getAttributeFinder(), finder,
				pdpConfig.getResourceFinder(), true);
		return new PDP(pdpConfig);
	}

	/**
	 * get the set of policies.
	 * 
	 * @return the policies.
	 */
	public Set<String> getPolicies() {
		return policies;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "XACMLPolicy [policDocuments=" + policies + "]";
	}
}
