/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2017 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */
package mudebs.broker.algorithms.routing;

import mudebs.common.filters.Filter;

import org.w3c.dom.Document;

/**
 * This class defines a visibility filter, which always return <tt>false</tt>.
 * 
 * @author Denis Conan
 * 
 */
public class FilterReturnFalse implements Filter {
	/**
	 * the singleton instance of this class.
	 */
	private static FilterReturnFalse instance = new FilterReturnFalse();

	/**
	 * private constructor of the singleton pattern.
	 */
	private FilterReturnFalse() {
	}

	/**
	 * gets the singleton instance.
	 * 
	 * @return the singleton instance.
	 */
	public static FilterReturnFalse getInstance() {
		return instance;
	}

	@Override
	public boolean evaluate(Document publication) {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "FilterReturnTrue []";
	}
}
