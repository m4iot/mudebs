/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2017 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan, Léon Lim
Contributor(s): 
 */
package mudebs.broker.algorithms.routing;

import java.io.StringWriter;
import java.io.Writer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import mudebs.broker.BrokerState;
import mudebs.common.Log;
import mudebs.common.algorithms.routing.ABACAttribute;
import mudebs.common.algorithms.routing.ABACInformation;

import org.apache.log4j.Level;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class XACMLBasicRequest {
	/**
	 * the document that represent the request.
	 */
	private Document doc;
	/**
	 * the namespace for attribute identifiers.
	 */
	private final String iDnameSpace = "mudebs://localhost:port/xacml/attribute-id/";

	/**
	 * constructs a basic request.
	 * 
	 * @param abac
	 *            the ABAC information.
	 * @throws ParserConfigurationException
	 *             the exception thrown when parsing error occurs.
	 */
	public XACMLBasicRequest(final ABACInformation abac)
			throws ParserConfigurationException {
		DocumentBuilderFactory docFactory = DocumentBuilderFactory
				.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		// root elements
		doc = docBuilder.newDocument();
		Element rootElement = doc.createElement("Request");
		doc.appendChild(rootElement);
		Attr attr = doc.createAttribute("xmlns");
		attr.setValue("urn:oasis:names:tc:xacml:3.0:core:schema:wd-17");
		rootElement.setAttributeNode(attr);
		attr = doc.createAttribute("xmlns:xsi");
		attr.setValue("http://www.w3.org/2001/XMLSchema-instance");
		rootElement.setAttributeNode(attr);
		attr = doc.createAttribute("xsi:schemaLocation");
		attr.setValue("urn:oasis:names:tc:xacml:3.0:core:schema:wd-17 "
				+ "http://docs.oasis-open.org/xacml/3.0/xacml-core-v3-schema-wd-17.xsd");
		rootElement.setAttributeNode(attr);
		attr = doc.createAttribute("CombinedDecision");
		attr.setValue("false");
		rootElement.setAttributeNode(attr);
		attr = doc.createAttribute("ReturnPolicyIdList");
		attr.setValue("false");
		rootElement.setAttributeNode(attr);
		for (ABACAttribute abacAtt : abac.getListOfAttributes()) {
			String category = abacAtt.getCategory();
			String identifier = abacAtt.getIdentifier();
			String value = abacAtt.getValue();
			String type = abacAtt.getType();
			Element genericAttribute = doc.createElement("Attributes");
			attr = doc.createAttribute("Category");
			attr.setValue(category);
			genericAttribute.setAttributeNode(attr);
			Element oneAtt = createAttribute("false", identifier, type, value);
			genericAttribute.appendChild(oneAtt);
			rootElement.appendChild(genericAttribute);
		}
	}

	/**
	 * creates attribute element.
	 * 
	 * @param includeInResult
	 *            the string stating whether the attribute is included in the
	 *            result.
	 * @param attrId
	 *            the attribute identifier.
	 * @param attrValueDT
	 *            the attribute data type.
	 * @param value
	 *            the value.
	 * @return the attribute element.
	 */
	public Element createAttribute(String includeInResult, String attrId,
			String attrValueDT, String value) {
		Element oneAttribute = doc.createElement("Attribute");
		Attr attr = doc.createAttribute("IncludeInResult");
		attr.setValue(includeInResult);
		oneAttribute.setAttributeNode(attr);
		attr = doc.createAttribute("AttributeId");
		attr.setValue(iDnameSpace + attrId);
		oneAttribute.setAttributeNode(attr);
		Element oneAttributeValue = doc.createElement("AttributeValue");
		attr = doc.createAttribute("DataType");
		attr.setValue(attrValueDT);
		oneAttributeValue.setAttributeNode(attr);
		oneAttributeValue.appendChild(doc.createTextNode(value));
		oneAttribute.appendChild(oneAttributeValue);
		return oneAttribute;
	}

	/**
	 * gets the string version of the document of the request.
	 * 
	 * @return the string.
	 */
	public String getDocInString() {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		String result = null;
		dbf.setValidating(false);
		BrokerState state = BrokerState.getInstance();
		Transformer tf = null;
		try {
			tf = TransformerFactory.newInstance().newTransformer();
			tf.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			tf.setOutputProperty(OutputKeys.INDENT, "yes");
			Writer out = new StringWriter();
			tf.transform(new DOMSource(doc), new StreamResult(out));
			result = out.toString();
		} catch (TransformerException e) {
			if (Log.ON && Log.ROUTING.isEnabledFor(Level.ERROR)) {
				Log.ROUTING.error(
						state.identity.getPath() + ", " + e.getMessage() + "\n"
								+ Log.printStackTrace(e.getStackTrace()));
			}
		}
		return result;
	}
}
