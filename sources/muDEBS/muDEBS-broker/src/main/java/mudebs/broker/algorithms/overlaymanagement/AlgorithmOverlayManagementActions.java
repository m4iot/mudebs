/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */
package mudebs.broker.algorithms.overlaymanagement;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;

import mudebs.broker.BrokerState;
import mudebs.broker.algorithms.routing.RoutingTableEntry;
import mudebs.broker.algorithms.routing.Util;
import mudebs.common.Constants;
import mudebs.common.Log;
import mudebs.common.PerfAttributes;
import mudebs.common.algorithms.AbstractMessageContent;
import mudebs.common.algorithms.EntityType;
import mudebs.common.algorithms.overlaymanagement.ColorTerminationDetection;
import mudebs.common.algorithms.overlaymanagement.IdentityMsgContent;
import mudebs.common.algorithms.overlaymanagement.TerminationDetectionMsgContent;
import mudebs.common.algorithms.overlaymanagement.TerminationMsgContent;

import org.apache.log4j.Level;

/**
 * This class defines the methods implementing the reaction concerning the
 * reception of overlay management messages.
 * 
 * @author Denis Conan
 * 
 */
public class AlgorithmOverlayManagementActions {

	public static void receiveIdentity(AbstractMessageContent content) {
		BrokerState bstate = BrokerState.getInstance();
		IdentityMsgContent mymsg = (IdentityMsgContent) content;
		if (!bstate.currIdentityType.equals(EntityType.UNKNOWN)) {
			if (Log.ON && Log.OVERLAY.isEnabledFor(Level.WARN)) {
				Log.OVERLAY.warn(bstate.identity.getPath()
						+ ", identity message received stating that "
						+ mymsg.getIdentity().getPath() + "is a broker,"
						+ " but the entity type was already known");
			}
			return;
		}
		if (mymsg.getEntityType().equals(EntityType.BROKER)) {
			bstate.broker.movePendingWorkertoBrokerWorkers(mymsg.getIdentity(),
					bstate.currKey);
			bstate.brokerIdentities.put(mymsg.getIdentity(), bstate.currKey);
			if (Log.ON && Log.OVERLAY.isEnabledFor(Level.DEBUG)) {
				Log.OVERLAY.debug(bstate.identity.getPath()
						+ ", identity message stating that "
						+ mymsg.getIdentity().getPath() + "is a broker");
			}
			if (Log.ON && Log.OVERLAY.isEnabledFor(Level.TRACE)) {
				Log.OVERLAY.trace(bstate.identity.getPath()
						+ ", brokerIdentities.size() = "
						+ bstate.brokerIdentities.size());
			}
		} else if (mymsg.getEntityType().equals(EntityType.SCRIPTING)) {
			bstate.broker.movePendingWorkertoScriptingWorkers(
					mymsg.getIdentity(), bstate.currKey);
			if (Log.ON && Log.OVERLAY.isEnabledFor(Level.INFO)) {
				Log.OVERLAY.info(bstate.identity.getPath()
						+ ", identity message stating that"
						+ " this is a scripting broker");
			}
		} else if (mymsg.getEntityType().equals(EntityType.CLIENT)) {
			bstate.broker.movePendingWorkertoClientWorkers(mymsg.getIdentity(),
					bstate.currKey);
			bstate.clientIdentities.put(mymsg.getIdentity(), bstate.currKey);
			if (Log.ON && Log.OVERLAY.isEnabledFor(Level.INFO)) {
				Log.OVERLAY.info(bstate.identity.getPath()
						+ ", identity message stating that "
						+ mymsg.getIdentity().getPath() + "is a client");
			}
			if (Log.ON && Log.OVERLAY.isEnabledFor(Level.TRACE)) {
				Log.OVERLAY.trace(bstate.identity.getPath()
						+ ", clientIdentities.size() = "
						+ bstate.clientIdentities.size());
			}
		}
		bstate.nbIdentitiesReceived++;
		if (Log.ON && Log.OVERLAY.isEnabledFor(Level.TRACE)) {
			Log.OVERLAY
					.trace(bstate.identity.getPath()
							+ ", nbIdentitiesReceived = "
							+ bstate.nbIdentitiesReceived);
		}
	}

	public static void receiveTerminationDetection(
			AbstractMessageContent content) {
		BrokerState bstate = BrokerState.getInstance();
		TerminationDetectionMsgContent mymsg = (TerminationDetectionMsgContent) content;
		if (bstate.currIdentityType != EntityType.BROKER) {
			if (Log.ON && Log.OVERLAY.isEnabledFor(Level.WARN)) {
				Log.OVERLAY.warn(bstate.identity.getPath()
						+ ", termination detection message not"
						+ " received from a broker");
			}
			return;
		}
		if (Log.ON && Log.OVERLAY.isEnabledFor(Level.DEBUG)) {
			Log.OVERLAY.debug(bstate.identity.getPath()
					+ ", termination detection message="
					+ mymsg.toStringBrief());
		}
		URI initiator = mymsg.getInitiator();
		URI forwarder = mymsg.getForwarder();
		int waveNumber = mymsg.getWaveNumber();
		HashMap<Integer, TerminationDetectionState> term = bstate.termWaves
				.get(initiator);
		if (term == null) {
			term = new HashMap<Integer, TerminationDetectionState>();
			bstate.termWaves.put(initiator, term);
		}
		TerminationDetectionState termState = term.get(new Integer(waveNumber));
		if (termState == null) {
			if (Log.ON && Log.OVERLAY.isEnabledFor(Level.TRACE)) {
				Log.OVERLAY.trace(bstate.identity.getPath()
						+ ", termination detection (" + initiator + ","
						+ waveNumber + "): father=" + forwarder);
			}
			termState = new TerminationDetectionState(forwarder);
			term.put(new Integer(waveNumber), termState);
			TerminationDetectionMsgContent toSend = new TerminationDetectionMsgContent(
					initiator, bstate.identity, waveNumber, 0,
					ColorTerminationDetection.White, null);
			for (URI uri : bstate.brokerIdentities.keySet()) {
				if (!uri.equals(forwarder)) {
					try {
						bstate.broker
								.sendToABroker(
										bstate.brokerIdentities.get(uri),
										AlgorithmOverlayManagement.TERMINATION_DETECTION
												.getActionIndex(), toSend);
					} catch (IOException e) {
						if (Log.ON && Log.ROUTING.isEnabledFor(Level.ERROR)) {
							Log.ROUTING.error(bstate.identity.getPath() + ", "
									+ e.getMessage() + "\n"
									+ Log.printStackTrace(e.getStackTrace()));
						}
					}
				}
			}
		}
		if (!termState.alreadyReceived.contains(forwarder)) {
			termState.alreadyReceived.add(forwarder);
		}
		termState.nbMsgsInTransit += mymsg.getNbMsgsInTransit();
		if (Log.ON && Log.OVERLAY.isEnabledFor(Level.TRACE)) {
			Log.OVERLAY.trace(bstate.identity.getPath()
					+ ", termination detection(" + initiator + "," + waveNumber
					+ "): rec=" + termState.alreadyReceived.size() + "/"
					+ bstate.brokerIdentities.size() + " broker(s)");
		}
		if (mymsg.getColor() == ColorTerminationDetection.BLACK
				|| !bstate.localAdvertisements.isEmpty()) {
			termState.color = ColorTerminationDetection.BLACK;
			if (Log.ON && Log.OVERLAY.isEnabledFor(Level.TRACE)) {
				Log.OVERLAY.trace(bstate.identity.getPath()
						+ ", termination detection(" + initiator + ","
						+ waveNumber + "): nb local advs="
						+ bstate.localAdvertisements.size());
			}
		}
		if (mymsg.getStatistics() != null) {
			termState.statistics.putAll(mymsg.getStatistics());
		}
		if (termState.alreadyReceived.size() == bstate.brokerIdentities
				.keySet().size() && !termState.waveAlreadyterminated) {
			termState.waveAlreadyterminated = true;
			if (Log.ON && Log.OVERLAY.isEnabledFor(Level.TRACE)) {
				Log.OVERLAY.trace(bstate.identity.getPath()
						+ ", termination detection (" + initiator + ","
						+ waveNumber + "): end of wave, father="
						+ termState.father);
			}
			HashMap<String, Number> myStats = new HashMap<String, Number>();
			getStatisticsIn(myStats);
			termState.statistics.put(bstate.identity, myStats);
			termState.nbMsgsInTransit = termState.nbMsgsInTransit
					+ bstate.nbMsgsInTransit;
			if (initiator.equals(bstate.identity)) {
				if (Log.ON && Log.OVERLAY.isEnabledFor(Level.DEBUG)) {
					Log.OVERLAY
							.debug(bstate.identity.getPath()
									+ ", termination detection : end of wave with color="
									+ ((termState.color == ColorTerminationDetection.White) ? "white"
											: "black, and counter ="
													+ termState.nbMsgsInTransit));
				}
				if (termState.color == ColorTerminationDetection.White) {
					if (PerfAttributes.performance_evaluation) {
						recordStatistics(termState);
					}
					if (termState.nbMsgsInTransit == 0) {
						if (Log.ON && Log.OVERLAY.isEnabledFor(Level.DEBUG)) {
							Log.OVERLAY.debug(bstate.identity.getPath()
									+ ", termination detected");
						}
					}
				}
			} else {
				try {
					TerminationDetectionMsgContent toSend = new TerminationDetectionMsgContent(
							initiator, bstate.identity, waveNumber,
							termState.nbMsgsInTransit, termState.color,
							termState.statistics);
					bstate.broker.sendToABroker(bstate.brokerIdentities
							.get(termState.father),
							AlgorithmOverlayManagement.TERMINATION_DETECTION
									.getActionIndex(), toSend);
				} catch (IOException e) {
					if (Log.ON && Log.ROUTING.isEnabledFor(Level.ERROR)) {
						Log.ROUTING.error(bstate.identity.getPath() + ", "
								+ e.getMessage() + "\n"
								+ Log.printStackTrace(e.getStackTrace()));
					}
				}
			}
		}
	}

	private static void recordStatistics(TerminationDetectionState termState) {
		BrokerState state = BrokerState.getInstance();
		int nbIdentitiesSent = 0;
		int nbIdentitiesReceived = 0;
		int nbAdvertisementsReceived = 0;
		int nbAdvertisementsSent = 0;
		int nbPublicationsReceived = 0;
		int nbPublicationsSent = 0;
		int nbRepliesReceived = 0;
		int nbSubscriptionsReceived = 0;
		int nbAdminSubscriptionsReceived = 0;
		int nbAdminSubscriptionsSent = 0;
		int nbAdminPublicationsSent = 0;
		int nbAdminPublicationsReceived = 0;
		int nbUnadvertisementsReceived = 0;
		int nbAdminUnsubscriptionsReceived = 0;
		int nbUnadvertisementsSent = 0;
		int nbUnsubscriptionsReceived = 0;
		int nbUnsubscriptionsSent = 0;
		int nbJoinScopesReceived = 0;
		int nbPublicationsDelivered = 0;
		int brokersRoutingTableSize = 0;
		int clientsRoutingTableSize = 0;
		double nbBrokersInvolvedInRouting = 0;
		double averageNbInvolvedBrokers = 0.;
		double averageBrokersRoutingTableSize = 0;
		double averageclientsRoutingTableSize = 0;
		for (URI uri : termState.statistics.keySet()) {
			if (Log.ON && Log.PERFORMANCE.isEnabledFor(Level.INFO)) {
				Log.PERFORMANCE.trace(uri);
			}
			for (String perfAttribute : termState.statistics.get(uri).keySet()) {
				Number perfValue = termState.statistics.get(uri)
						.get(perfAttribute).intValue();
				if (Constants.NB_IDENTITIES_SENT
						.equalsIgnoreCase(perfAttribute)) {
					nbIdentitiesSent += perfValue.intValue();
				}
				if (Constants.NB_IDENTITIES_RECEIVED
						.equalsIgnoreCase(perfAttribute)) {
					nbIdentitiesReceived += perfValue.intValue();
				}
				if (Constants.NB_ADVERTISEMENTS_RECEIVED
						.equalsIgnoreCase(perfAttribute)) {
					nbAdvertisementsReceived += perfValue.intValue();
				}
				if (Constants.NB_ADVERTISEMENTS_SENT
						.equalsIgnoreCase(perfAttribute)) {
					nbAdvertisementsSent += perfValue.intValue();
				}
				if (Constants.NB_PUBLICATIONS_RECEIVED
						.equalsIgnoreCase(perfAttribute)) {
					nbPublicationsReceived += perfValue.intValue();
				}
				if (Constants.NB_PUBLICATIONS_SENT
						.equalsIgnoreCase(perfAttribute)) {
					nbPublicationsSent += perfValue.intValue();
				}
				if (Constants.NB_REPLIES_RECEIVED
						.equalsIgnoreCase(perfAttribute)) {
					nbRepliesReceived += perfValue.intValue();
				}
				if (Constants.NB_SUBSCRIPTIONS_RECEIVED
						.equalsIgnoreCase(perfAttribute)) {
					nbSubscriptionsReceived += perfValue.intValue();
				}
				if (Constants.NB_ADMIN_SUBSCRIPTIONS_RECEIVED
						.equalsIgnoreCase(perfAttribute)) {
					nbAdminSubscriptionsReceived += perfValue.intValue();
				}
				if (Constants.NB_ADMIN_SUBSCRIPTIONS_SENT
						.equalsIgnoreCase(perfAttribute)) {
					nbAdminSubscriptionsSent += perfValue.intValue();
				}
				if (Constants.NB_ADMIN_PUBLICATION_SENT
						.equalsIgnoreCase(perfAttribute)) {
					nbAdminPublicationsSent += perfValue.intValue();
				}
				if (Constants.NB_ADMIN_PUBLICATIONS_RECEIVED
						.equalsIgnoreCase(perfAttribute)) {
					nbAdminPublicationsReceived += perfValue.intValue();
				}
				if (Constants.NB_ADVERTISEMENTS_RECEIVED
						.equalsIgnoreCase(perfAttribute)) {
					nbUnadvertisementsReceived += perfValue.intValue();
				}
				if (Constants.NB_ADMIN_UNSCRIPTIONS_RECEIVED
						.equalsIgnoreCase(perfAttribute)) {
					nbAdminUnsubscriptionsReceived += perfValue.intValue();
				}
				if (Constants.NB_UNADVERTISEMENTS_SENT
						.equalsIgnoreCase(perfAttribute)) {
					nbUnadvertisementsSent += state.nbUnadvertisementsSent;
				}
				if (Constants.NB_UNSCRIPTIONS_RECEIVED
						.equalsIgnoreCase(perfAttribute)) {
					nbUnsubscriptionsReceived += perfValue.intValue();
				}
				if (Constants.NB_UNSCRIPTIONS_SENT
						.equalsIgnoreCase(perfAttribute)) {
					nbUnsubscriptionsSent += perfValue.intValue();
				}
				if (Constants.NB_JOIN_SCOPE_RECEIVED
						.equalsIgnoreCase(perfAttribute)) {
					nbJoinScopesReceived += perfValue.intValue();
				}
				if (Constants.NB_PUBLICATIONS_DELIVERED
						.equalsIgnoreCase(perfAttribute)) {
					nbPublicationsDelivered += perfValue.intValue();
				}
				if (Constants.NB_BROKERS_INVOLVED_IN_ROUTING
						.equalsIgnoreCase(perfAttribute)) {
					nbBrokersInvolvedInRouting += perfValue.intValue();
				}
				if (Constants.NB_BROKERS_ROUTING_TABLE_SIZE
						.equalsIgnoreCase(perfAttribute)) {
					brokersRoutingTableSize += perfValue.intValue();
				}
				if (Constants.NB_CLIENTS_ROUTING_TABLE_SIZE
						.equalsIgnoreCase(perfAttribute)) {
					clientsRoutingTableSize += perfValue.intValue();
				}
				if (perfAttribute.contains(Constants.AVERAGE_EXECUTION_TIME)) {
					try {
						Util.updateResult(perfAttribute, perfValue);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				if (perfAttribute.contains(Constants.VARIANCE_EXECUTION_TIME)) {
					try {
						Util.updateResult(perfAttribute, perfValue);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				if (perfAttribute.contains(Constants.STDV_EXECUTION_TIME)) {
					try {
						Util.updateResult(perfAttribute, perfValue);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				if (Log.ON && Log.OVERLAY.isEnabledFor(Level.TRACE)) {
					Log.OVERLAY.trace(perfAttribute + " = "
							+ termState.statistics.get(uri).get(perfAttribute));
				}
			}
			if (Log.ON && Log.OVERLAY.isEnabledFor(Level.TRACE)) {
				Log.OVERLAY.trace("\n");
			}
		}
		if (nbPublicationsDelivered != 0) {
			averageNbInvolvedBrokers = (double) nbBrokersInvolvedInRouting
					/ ((double) nbPublicationsDelivered);
		} else {
			if (Log.ON && Log.PERFORMANCE.isEnabledFor(Level.INFO)) {
				Log.PERFORMANCE.info(state.identity
						+ " has not delivered any publication");
			}
		}
		averageBrokersRoutingTableSize = (double) brokersRoutingTableSize
				/ ((double) PerfAttributes.networkSize);
		averageclientsRoutingTableSize = (double) clientsRoutingTableSize
				/ ((double) PerfAttributes.networkSize);
		if (PerfAttributes.allStats || PerfAttributes.nbIdentitiesSent) {
			try {
				Util.updateResult(Constants.NB_IDENTITIES_SENT,
						nbIdentitiesSent);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (PerfAttributes.allStats || PerfAttributes.nbIdentitiesReceived) {
			try {
				Util.updateResult(Constants.NB_IDENTITIES_RECEIVED,
						nbIdentitiesReceived);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (PerfAttributes.allStats || PerfAttributes.nbAdvertisementsReceived) {
			try {
				Util.updateResult(Constants.NB_ADVERTISEMENTS_RECEIVED,
						nbAdvertisementsReceived);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (PerfAttributes.allStats || PerfAttributes.nbAdvertisementsSent) {
			try {
				Util.updateResult(Constants.NB_ADVERTISEMENTS_SENT,
						nbAdvertisementsSent);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (PerfAttributes.allStats || PerfAttributes.nbPublicationsReceived) {
			try {
				Util.updateResult(Constants.NB_PUBLICATIONS_RECEIVED,
						nbPublicationsReceived);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (PerfAttributes.allStats || PerfAttributes.nbPublicationsDelivered) {
			try {
				Util.updateResult(Constants.NB_PUBLICATIONS_DELIVERED,
						nbPublicationsDelivered);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (PerfAttributes.allStats || PerfAttributes.nbPublicationsSent) {
			try {
				Util.updateResult(Constants.NB_PUBLICATIONS_SENT,
						nbPublicationsSent);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (PerfAttributes.allStats || PerfAttributes.nbRepliesReceived) {
			try {
				Util.updateResult(Constants.NB_REPLIES_RECEIVED,
						nbRepliesReceived);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (PerfAttributes.allStats || PerfAttributes.nbSubscriptionsReceived) {
			try {
				Util.updateResult(Constants.NB_SUBSCRIPTIONS_RECEIVED,
						nbSubscriptionsReceived);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (PerfAttributes.allStats
				|| PerfAttributes.nbAdminSubscriptionsReceived) {
			try {
				Util.updateResult(Constants.NB_ADMIN_SUBSCRIPTIONS_RECEIVED,
						nbAdminSubscriptionsReceived);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (PerfAttributes.allStats || PerfAttributes.nbAdminSubscriptionsSent) {
			try {
				Util.updateResult(Constants.NB_ADMIN_SUBSCRIPTIONS_SENT,
						nbAdminSubscriptionsSent);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (PerfAttributes.allStats || PerfAttributes.nbAdminPublicationsSent) {
			try {
				Util.updateResult(Constants.NB_ADMIN_PUBLICATION_SENT,
						nbAdminPublicationsSent);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (PerfAttributes.allStats
				|| PerfAttributes.nbAdminPublicationsReceived) {
			try {
				Util.updateResult(Constants.NB_ADMIN_PUBLICATIONS_RECEIVED,
						nbAdminPublicationsReceived);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (PerfAttributes.allStats || PerfAttributes.nbAdvertisementsReceived) {
			try {
				Util.updateResult(Constants.NB_ADVERTISEMENTS_RECEIVED,
						nbUnadvertisementsReceived);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (PerfAttributes.allStats
				|| PerfAttributes.nbAdminUnsubscriptionsReceived) {
			try {
				Util.updateResult(Constants.NB_ADMIN_UNSCRIPTIONS_RECEIVED,
						nbAdminUnsubscriptionsReceived);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (PerfAttributes.allStats || PerfAttributes.nbUnadvertisementsSent) {
			try {
				Util.updateResult(Constants.NB_UNADVERTISEMENTS_SENT,
						nbUnadvertisementsSent);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (PerfAttributes.allStats || PerfAttributes.nbUnsubscriptionsReceived) {
			try {
				Util.updateResult(Constants.NB_UNSCRIPTIONS_RECEIVED,
						nbUnsubscriptionsReceived);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (PerfAttributes.allStats || PerfAttributes.nbUnsubscriptionsSent) {
			try {
				Util.updateResult(Constants.NB_UNSCRIPTIONS_SENT,
						nbUnsubscriptionsSent);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (PerfAttributes.allStats || PerfAttributes.nbJoinScopeReceived) {
			try {
				Util.updateResult(Constants.NB_JOIN_SCOPE_RECEIVED,
						nbJoinScopesReceived);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (PerfAttributes.allStats || PerfAttributes.averageNbInvolvedBrokers) {
			try {
				Util.updateResult(Constants.NB_BROKERS_INVOLVED_IN_ROUTING,
						nbBrokersInvolvedInRouting);
				Util.updateResult(Constants.AVERAGE_NB_INVOLVED_BROKERS,
						averageNbInvolvedBrokers);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (PerfAttributes.allStats || PerfAttributes.brokersRoutingTableSize) {
			try {
				Util.updateResult(
						Constants.AVERAGE_NB_BROKERS_ROUTING_TABLE_SIZE,
						averageBrokersRoutingTableSize);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (PerfAttributes.allStats || PerfAttributes.clientsRoutingTableSize) {
			try {
				Util.updateResult(
						Constants.AVERAGE_NB_CLIENTS_ROUTING_TABLE_SIZE,
						averageclientsRoutingTableSize);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private static void getStatisticsIn(final HashMap<String, Number> myStats) {
		BrokerState state = BrokerState.getInstance();
		if (PerfAttributes.performance_evaluation
				&& state.brokerRoutingTables != null
				&& !state.brokerRoutingTables.isEmpty()) {
			state.brokersRoutingTableSize = 0;
			for (URI uri : state.brokerRoutingTables.keySet()) {
				if (state.brokerRoutingTables.get(uri).isEmpty()) {
					state.brokersRoutingTableSize++;
				} else {
					state.brokersRoutingTableSize = state.brokersRoutingTableSize
							+ state.brokerRoutingTables.get(uri).size();
				}
			}
		}
		if (PerfAttributes.performance_evaluation
				&& state.clientRoutingTables != null
				&& !state.clientRoutingTables.isEmpty()) {
			state.clientsRoutingTableSize = 0;
			for (URI uri : state.clientRoutingTables.keySet()) {
				if (state.clientRoutingTables.get(uri).isEmpty()) {
					state.clientsRoutingTableSize++;
				} else {
					for (RoutingTableEntry re : state.clientRoutingTables
							.get(uri)) {
						if (re.getIdentifier().contains("subscription")
								|| re.getIdentifier().contains("advertisement")) {
							state.clientsRoutingTableSize++;
						}
					}
				}
			}
		}
		if (PerfAttributes.performance_evaluation) {
			if (PerfAttributes.nbAdvertisementsReceived) {
				myStats.put(Constants.NB_ADVERTISEMENTS_RECEIVED,
						state.nbAdvertisementsReceived);
			}
			if (PerfAttributes.nbPublicationsReceived) {
				myStats.put(Constants.NB_PUBLICATIONS_RECEIVED,
						state.nbPublicationsReceived);
			}
			if (PerfAttributes.nbAdminPublicationsReceived) {
				myStats.put(Constants.NB_ADMIN_PUBLICATIONS_RECEIVED,
						state.nbAdminPublicationsReceived);
			}
			if (PerfAttributes.nbPublicationsDelivered) {
				myStats.put(Constants.NB_PUBLICATIONS_DELIVERED,
						state.nbPublicationsDelivered);

			}
			if (PerfAttributes.averageNbInvolvedBrokers) {
				myStats.put(Constants.NB_BROKERS_INVOLVED_IN_ROUTING,
						state.nbBrokersInvolvedInRouting);
			}
			if (PerfAttributes.nbSubscriptionsReceived) {
				myStats.put(Constants.NB_SUBSCRIPTIONS_RECEIVED,
						state.nbSubscriptionsReceived);
			}
			if (PerfAttributes.nbAdminSubscriptionsReceived) {
				myStats.put(Constants.NB_ADMIN_SUBSCRIPTIONS_RECEIVED,
						state.nbAdminSubscriptionsReceived);
			}
			if (PerfAttributes.nbUnadvertisementsReceived) {
				myStats.put(Constants.NB_UNADVERTISEMENTS_RECEIVED,
						state.nbUnadvertisementsReceived);
			}
			if (PerfAttributes.nbUnsubscriptionsReceived) {
				myStats.put(Constants.NB_UNSCRIPTIONS_RECEIVED,
						state.nbUnsubscriptionsReceived);
			}
			if (PerfAttributes.nbAdminUnsubscriptionsReceived) {
				myStats.put(Constants.NB_ADMIN_UNSCRIPTIONS_RECEIVED,
						state.nbAdminUnsubscriptionsReceived);
			}
			if (PerfAttributes.nbJoinScopeReceived) {
				myStats.put(Constants.NB_JOIN_SCOPE_RECEIVED,
						state.nbJoinScopesReceived);
			}
			if (PerfAttributes.brokersRoutingTableSize) {
				myStats.put(Constants.NB_BROKERS_ROUTING_TABLE_SIZE,
						state.brokersRoutingTableSize);
			}
			if (PerfAttributes.clientsRoutingTableSize) {
				myStats.put(Constants.NB_CLIENTS_ROUTING_TABLE_SIZE,
						state.clientsRoutingTableSize);
			}
			if (PerfAttributes.nbIdentitiesSent) {
				myStats.put(Constants.NB_IDENTITIES_SENT,
						state.nbIdentitiesSent);
			}
			if (PerfAttributes.nbIdentitiesReceived) {
				myStats.put(Constants.NB_IDENTITIES_RECEIVED,
						state.nbIdentitiesReceived);
			}
			if (PerfAttributes.nbAdvertisementsSent) {
				myStats.put(Constants.NB_ADVERTISEMENTS_SENT,
						state.nbAdvertisementsSent);
			}
			if (PerfAttributes.nbPublicationsSent) {
				myStats.put(Constants.NB_PUBLICATIONS_SENT,
						state.nbPublicationsSent);
			}
			if (PerfAttributes.nbRepliesReceived) {
				myStats.put(Constants.NB_REPLIES_RECEIVED,
						state.nbRepliesReceived);
			}
			if (PerfAttributes.nbAdminSubscriptionsSent) {
				myStats.put(Constants.NB_ADMIN_SUBSCRIPTIONS_SENT,
						state.nbAdminSubscriptionsSent);
			}
			if (PerfAttributes.nbAdminPublicationsSent) {
				myStats.put(Constants.NB_ADMIN_PUBLICATION_SENT,
						state.nbAdminPublicationsSent);
			}
			if (PerfAttributes.nbUnadvertisementsSent) {
				myStats.put(Constants.NB_UNADVERTISEMENTS_SENT,
						state.nbUnadvertisementsSent);
			}
			if (PerfAttributes.nbUnsubscriptionsSent) {
				myStats.put(Constants.NB_UNSCRIPTIONS_SENT,
						state.nbUnsubscriptionsSent);
			}
			if (PerfAttributes.executionTime) {
				myStats.put(
						Constants.AVERAGE_EXECUTION_TIME
								+ "_"
								+ (state.identity.toString())
										.substring(state.identity.toString()
												.lastIndexOf("/") + 1),
						getMeanFromListOfExecutionTime(getTruncatedListOfExecutionTime(
								state.listOfExecutionTime, 100)));
				myStats.put(
						Constants.STDV_EXECUTION_TIME
								+ "_"
								+ (state.identity.toString())
										.substring(state.identity.toString()
												.lastIndexOf("/") + 1),
						getStandardDeviationFromListOfExecutionTime(getTruncatedListOfExecutionTime(
								state.listOfExecutionTime, 100)));
			}
		}
	}

	private static List<Double> getTruncatedListOfExecutionTime(
			final List<Double> listOfExecutionTime, final int n) {
		List<Double> truncatedList = new ArrayList<Double>();
		if (listOfExecutionTime.size() > 100) {
			for (int i = n; i < listOfExecutionTime.size(); i++) {
				truncatedList.add(listOfExecutionTime.get(i));
			}
		} else {
			return listOfExecutionTime;
		}
		return truncatedList;
	}

	private static double getMeanFromListOfExecutionTime(
			final List<Double> listOfExecutionTime) {
		double averageExecutiontime = 0;
		double sumExecutiontime = 0;
		if (!listOfExecutionTime.isEmpty()) {
			for (double execTime : listOfExecutionTime) {
				sumExecutiontime += execTime;
			}
			averageExecutiontime = sumExecutiontime
					/ listOfExecutionTime.size();
		}
		return averageExecutiontime;
	}

	private static double getVarianceFromListOfExecutionTime(
			final List<Double> listOfExecutionTime) {
		double varianceExecutionTime = 0;
		double tmp = 0;
		double averageExecutionTime = getMeanFromListOfExecutionTime(listOfExecutionTime);
		if (!listOfExecutionTime.isEmpty()) {
			for (double execTime : listOfExecutionTime) {
				tmp += (execTime - averageExecutionTime)
						* (execTime - averageExecutionTime);
			}
			varianceExecutionTime = tmp / listOfExecutionTime.size();
		}
		return varianceExecutionTime;
	}

	private static double getStandardDeviationFromListOfExecutionTime(
			final List<Double> listOfExecutionTime) {
		double stdv = 0;
		if (listOfExecutionTime.isEmpty()) {
			System.out.println("There is no execution time recorded");
			return 0;
		} else {
			int n = 0;
			System.out.println("There are " + listOfExecutionTime.size()
					+ " execution time recorded");
			for (double execTime : listOfExecutionTime) {
				System.out.println(++n + "   " + execTime);
			}
		}
		stdv = Math
				.sqrt(getVarianceFromListOfExecutionTime(listOfExecutionTime));
		return stdv;
	}

	public static void receiveTermination(AbstractMessageContent content) {
		BrokerState bstate = BrokerState.getInstance();
		TerminationMsgContent mymsg = (TerminationMsgContent) content;
		if (bstate.currIdentityType != EntityType.BROKER) {
			if (Log.ON && Log.OVERLAY.isEnabledFor(Level.WARN)) {
				Log.OVERLAY.warn(bstate.identity.getPath()
						+ ", termination message not"
						+ " received from a broker");
			}
			return;
		}
		if (Log.ON && Log.OVERLAY.isEnabledFor(Level.DEBUG)) {
			Log.OVERLAY.debug(bstate.identity.getPath()
					+ ", termination message=" + mymsg.toString());
		}
		TerminationMsgContent toSend = new TerminationMsgContent(
				bstate.identity, mymsg.getForwardToClients());
		for (URI uri : bstate.brokerIdentities.keySet()) {
			if (!uri.equals(mymsg.getForwarder())) {
				try {
					bstate.broker.sendToABroker(bstate.brokerIdentities
							.get(uri), AlgorithmOverlayManagement.TERMINATION
							.getActionIndex(), toSend);
				} catch (IOException e) {
					if (Log.ON && Log.ROUTING.isEnabledFor(Level.ERROR)) {
						Log.ROUTING.error(bstate.identity.getPath() + ", "
								+ e.getMessage() + "\n"
								+ Log.printStackTrace(e.getStackTrace()));
					}
				}
			}
		}
		if (mymsg.getForwardToClients()) {
			for (URI uri : bstate.clientIdentities.keySet()) {
				try {
					bstate.broker.sendToAClient(bstate.clientIdentities
							.get(uri), AlgorithmOverlayManagement.TERMINATION
							.getActionIndex(), toSend);
				} catch (IOException e) {
					if (Log.ON && Log.ROUTING.isEnabledFor(Level.ERROR)) {
						Log.ROUTING.error(bstate.identity.getPath() + ", "
								+ e.getMessage() + "\n"
								+ Log.printStackTrace(e.getStackTrace()));
					}
				}
			}
		}
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// nop
		}
		System.exit(0);
	}
}
