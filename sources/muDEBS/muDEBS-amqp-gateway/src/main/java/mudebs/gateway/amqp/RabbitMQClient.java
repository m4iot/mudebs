/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2017 Télécom SudParis

The muDEBS platform is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform.  If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Xuan Li
 */
package mudebs.gateway.amqp;

import java.io.IOException;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;

/**
 * This class defines a RabbitMQ broker, which can be both a consumer and a
 * producer. Consumer can send subscriptions and wait for publications. Producer
 * can send publications.
 * 
 * @author Xuan Li
 * 
 */
public class RabbitMQClient {

	/**
	 * The connection abstracts the socket connection, and takes care of
	 * protocol version negotiation and authentication.
	 */
	private Connection connection;

	/**
	 * This is a convenience "factory" class to facilitate opening a Connection
	 * to an AMQP broker.
	 */
	private ConnectionFactory factory;

	/**
	 * The channel is for use by multiple threads.
	 */
	private Channel channel;

	/**
	 * This is a class for encapsulating an arbitrary message.
	 */
	private QueueingConsumer.Delivery delivery;

	/**
	 * The default host to use for connections
	 */
	private String host;

	/**
	 * The AMQP user name to use when connecting to the broker
	 */
	private String userName;

	/**
	 * The password to use when connecting to the broker
	 */
	private String password;

	/**
	 * The virtual host to use when connecting to the broker
	 */
	private String vhost;

	/**
	 * True if we are declaring a durable exchange (the exchange will survive a
	 * server restart).
	 */
	private boolean durable = true;

	/**
	 * True if the server should delete the exchange when it is no longer in
	 * use.
	 */
	private boolean autoDelete = false;

	/**
	 * True if we are declaring an exclusive queue (restricted to this
	 * connection).
	 */
	private boolean exclusive = false;

	/**
	 * Constructor for a RabbitMQ broker.
	 * 
	 * @param host
	 *            The default host to use for connections.
	 * @param userName
	 *            The user name to use when connecting to the broker.
	 * @param password
	 *            The password to use when connecting to the broker.
	 * @param vhost
	 *            The virtual host to use when connecting to the broker.
	 */
	public RabbitMQClient(final String host, final String userName,
			final String password, final String vhost) {
		this.host = host;
		this.userName = userName;
		this.password = password;
		this.vhost = vhost;
		connection = null;
		factory = null;
		channel = null;
		delivery = null;
	}

	/**
	 * initialises a connection and a channel.
	 * 
	 * @throws IOException
	 *             the exception thrown by RabbitMQ library.
	 */
	public void initAMQPConnection() throws IOException {
		factory = new ConnectionFactory();
		factory.setHost(host);
		factory.setUsername(userName);
		factory.setPassword(password);
		factory.setVirtualHost(vhost);
		connection = factory.newConnection();
		channel = connection.createChannel();
		channel.basicQos(1);
	}

	/**
	 * declares an exchange.
	 * 
	 * @param exchangeName
	 *            The name of an exchange.
	 * @throws IOException
	 *             the exception thrown by RabbitMQ library.
	 */
	public void declareExchange(final String exchangeName) throws IOException {
		channel.exchangeDeclare(exchangeName, "topic", durable, autoDelete,
				null);
	}

	/**
	 * declares a queue.
	 * 
	 * @param queueName
	 *            The name of an queue.
	 * @throws IOException
	 *             the exception thrown by RabbitMQ library.
	 */
	public void declareQueue(final String queueName) throws IOException {
		channel.queueDeclare(queueName, durable, exclusive, autoDelete, null);
	}

	/**
	 * publishes a message.
	 * 
	 * @param exchangeName
	 *            The name of an exchange.
	 * @param routingKey
	 *            The routing key for a message.
	 * @param messageSend
	 *            The message to be sent.
	 * @throws IOException
	 *             the exception thrown by RabbitMQ library.
	 */
	public void publish(final String exchangeName, final String routingKey,
			final String messageSend) throws IOException {
		channel.basicPublish(exchangeName, routingKey, null,
				messageSend.getBytes());
	}

	/**
	 * binds a queue to an exchange.
	 * 
	 * @param queueName
	 *            The name of an queue.
	 * @param exchangeName
	 *            The name of an exchange.
	 * @param bindingKey
	 *            The binding key for binding a queue and an exchange together.
	 * @throws IOException
	 *             the exception thrown by RabbitMQ library.
	 */
	public void bindQueue(final String queueName, final String exchangeName,
			final String[] bindingKey) throws IOException {
		for (int i = 0; i < bindingKey.length; i++) {
			channel.queueBind(queueName, exchangeName, bindingKey[i]);
		}
	}

	/**
	 * starts a consumer who will receive messages.
	 * 
	 * @param queueName
	 *            The name of an queue.
	 * @throws IOException
	 *             the exception thrown by RabbitMQ library.
	 */
	public void subscribe(final String queueName) throws IOException {
		QueueingConsumer consumer = new QueueingConsumer(channel);
		channel.basicConsume(queueName, true, consumer);
		while (true) {
			try {
				// block until another message has been delivered from the
				// server.
				delivery = consumer.nextDelivery();
			} catch (InterruptedException ie) {
				continue;
			}
			System.out.println(queueName + " receives "
					+ new String(delivery.getBody()) + " with severity = "
					+ delivery.getEnvelope().getRoutingKey());
		}
	}

	/**
	 * starts a consumer who will receive messages and then send them.
	 * 
	 * @param queueName
	 *            The name of an queue.
	 * @param exchangeName
	 *            The name of an exchange.
	 * @param routingKey
	 *            The routing key for a message.
	 * @throws IOException
	 *             the exception thrown by RabbitMQ library.
	 */
	public void exchangeMessage(final String queueName,
			final String exchangeName, final String routingKey)
			throws IOException {
		QueueingConsumer consumer = new QueueingConsumer(channel);
		channel.basicConsume(queueName, true, consumer);
		while (true) {
			try {
				// block until another message has been delivered from the
				// server.
				delivery = consumer.nextDelivery();
			} catch (InterruptedException ie) {
				continue;
			}
			System.out.println(queueName + " receives "
					+ new String(delivery.getBody()) + " with severity = "
					+ delivery.getEnvelope().getRoutingKey());
			String messageSend = new String(delivery.getBody());
			publish(exchangeName, routingKey, messageSend);
		}
	}

	/**
	 * closes the channel and the connection.
	 * 
	 * @throws IOException the exception thrown in case of problem.
	 */
	public void close() throws IOException {
		channel.close();
		connection.close();
	}

	/**
	 * gets the channel of the AMQP connection.
	 * 
	 * @return the channel.
	 */
	public Channel getChannel() {
		return channel;
	}

	/**
	 * sets the channel of the AMQP connection.
	 * 
	 * @param channel the channel.
	 */
	public void setChannel(Channel channel) {
		this.channel = channel;
	}
}
