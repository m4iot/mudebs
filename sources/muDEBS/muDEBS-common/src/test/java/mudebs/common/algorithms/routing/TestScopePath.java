/**
This file is part of the muDEBS middleware.

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Léon Lim
Contributor(s): 
 */
package mudebs.common.algorithms.routing;

import java.net.URI;
import java.net.URISyntaxException;

import mudebs.common.MuDEBSException;

import org.junit.Assert;
import org.junit.Test;

/**
 * Tests for the class <tt>ScopePath</tt>
 * 
 * 
 * @author Léon Lim
 *
 */
public class TestScopePath {

	@Test(expected = IllegalArgumentException.class)
	public void testConstructorNoDimension() throws MuDEBSException,
			URISyntaxException {
		new ScopePath(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConstructorEmptyId() throws URISyntaxException {
		new ScopePath(null);
	}

	@Test
	public void testConstructorAndIdentifier() throws URISyntaxException {
		URI d = new URI("mudebs:localhost:portnumber/d");
		ScopePath sp = new ScopePath(d);
		Assert.assertNotNull(sp);
	}

	@Test(expected = MuDEBSException.class)
	public void testAddScopeToPathWithDifferentDimension()
			throws MuDEBSException, URISyntaxException {
		URI d = new URI("mudebs:localhost:portnumber/d");
		URI d2 = new URI("mudebs:localhost:portnumber/d2");
		Scope s = new Scope(d, "s1");
		ScopePath sp = new ScopePath(d2);
		sp.addScope(s);
	}

	@Test
	public void testAddScopeToPathCorrectLast() throws MuDEBSException,
			URISyntaxException {
		URI d = new URI("mudebs:localhost:portnumber/d");
		Scope s = new Scope(d, "s");
		ScopePath sp = new ScopePath(d);
		sp.addScope(s);
		Assert.assertEquals(s,
				sp.scopeSequence().get(sp.scopeSequence().size() - 1));
	}

	@Test(expected = MuDEBSException.class)
	public void testAddScopeToPathNoSubSupRelation() throws MuDEBSException,
			URISyntaxException {
		URI d = new URI("mudebs:localhost:portnumber/d");
		Scope s = new Scope(d, "s");
		Scope s2 = new Scope(d, "s2");
		ScopePath sp = new ScopePath(d);
		sp.addScope(s);
		sp.addScope(s2);
	}

	@Test
	public void testAddScopeToPathWithRelation() throws MuDEBSException,
			URISyntaxException {
		URI d = new URI("mudebs:localhost:portnumber/d");
		Scope s = new Scope(d, "s");
		Scope s2 = new Scope(d, "s2");
		ScopePath sp = new ScopePath(d);
		sp.addScope(s);
		s2.addSuperScope(s);
		sp.addScope(s2);
		Assert.assertEquals(s2,
				sp.scopeSequence().get(sp.scopeSequence().size() - 1));
	}
}
