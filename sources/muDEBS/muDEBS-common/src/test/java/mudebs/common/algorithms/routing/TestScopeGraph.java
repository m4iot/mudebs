/**
This file is part of the muDEBS middleware.

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Léon Lim
Contributor(s): Denis Conan
 */
package mudebs.common.algorithms.routing;

import java.net.URI;
import java.net.URISyntaxException;

import org.junit.Assert;

import mudebs.common.MuDEBSException;

import org.junit.Test;

/**
 * Tests for the class <tt>Scope</tt> with <tt>ScopeBottom</tt> and
 * <tt>ScopeTop</tt>
 * 
 * NB: by a slight abuse of usage, the method toStringSubScopes is used for
 * comparisons.
 * 
 * @author Léon Lim
 * @author Denis Conan
 *
 */
public class TestScopeGraph {

	@Test
	public void testAddTop1() throws MuDEBSException, URISyntaxException {
		URI d = new URI("mudebs:localhost:portnumber/d");
		ScopeGraph graph = new ScopeGraph(d);
		ScopeTop top = new ScopeTop(d);
		Scope s = new Scope(d, "s");
		graph.addEdge(s, top);
		Assert.assertEquals("ScopeGraph [dimension=mudebs:localhost:portnumber/d, "
				+ "toStringSubScopes()=[top/s/bot]]", graph.toString());
	}

	@Test
	public void testAddTop2() throws MuDEBSException, URISyntaxException {
		URI d = new URI("mudebs:localhost:portnumber/d");
		ScopeGraph graph = new ScopeGraph(d);
		ScopeTop top = new ScopeTop(d);
		Scope s = new Scope(d, "s");
		graph.addEdge(s, top);
		Scope t = new Scope(d, "t");
		graph.addEdge(t, top);
		Assert.assertEquals("ScopeGraph [dimension=mudebs:localhost:portnumber/d, "
				+ "toStringSubScopes()=[top/s/bot, top/t/bot]]", graph.toString());
	}
	
	@Test
	public void testToString() throws MuDEBSException, URISyntaxException {
		URI d = new URI("mudebs:localhost:portnumber/d");
		ScopeGraph graph = new ScopeGraph(d);
		Assert.assertEquals("ScopeGraph [dimension=mudebs:localhost:portnumber/d, toStringSubScopes()=[top]]",
				graph.toString());
	}

	@Test
	public void addEdge1() throws MuDEBSException, URISyntaxException {
		URI d = new URI("mudebs:localhost:portnumber/d");
		ScopeGraph graph = new ScopeGraph(d);
		Assert.assertEquals("[top]", graph.toStringSubScopes());
	}

	@Test
	public void addEdge2() throws MuDEBSException, URISyntaxException {
		URI d = new URI("mudebs:localhost:portnumber/d");
		ScopeGraph graph = new ScopeGraph(d);
		Scope s1 = new Scope(d, "s1");
		Scope s2 = new Scope(d, "s2");
		Scope s3 = new Scope(d, "s3");
		Scope s4 = new Scope(d, "s4");
		Scope s5 = new Scope(d, "s5");
		Assert.assertEquals("[top]", graph.toStringSubScopes());
		graph.addEdge(s2, s1);
		Assert.assertEquals("[top/s2/bot, top/s1/s2/bot, top/s1/bot]",
				graph.toStringSubScopes());
		graph.addEdge(s4, s3);
		Assert.assertEquals(
				"[top/s2/bot, top/s1/s2/bot, top/s1/bot, top/s4/bot, "
						+ "top/s3/s4/bot, top/s3/bot]",
				graph.toStringSubScopes());
		graph.addEdge(s4, s5);
		Assert.assertEquals(
				"[top/s2/bot, top/s1/s2/bot, top/s1/bot, top/s4/bot, "
						+ "top/s3/s4/bot, top/s3/bot, top/s5/s4/bot, top/s5/bot]",
				graph.toStringSubScopes());
		graph.addEdge(s3, s2);
		Assert.assertEquals(
				"[top/s2/bot, top/s2/s3/s4/bot, top/s2/s3/bot, top/s1/s2/bot, "
						+ "top/s1/s2/s3/s4/bot, top/s1/s2/s3/bot, top/s1/bot, top/s4/bot, "
						+ "top/s3/s4/bot, top/s3/bot, top/s5/s4/bot, top/s5/bot]",
				graph.toStringSubScopes());
	}

	@Test
	public void addEdge3() throws MuDEBSException, URISyntaxException {
		URI d = new URI("mudebs:localhost:portnumber/d");
		ScopeGraph graph = new ScopeGraph(d);
		Scope s1 = new Scope(d, "s1");
		Scope s2 = new Scope(d, "s2");
		Scope s3 = new Scope(d, "s3");
		Scope s4 = new Scope(d, "s4");
		Scope s5 = new Scope(d, "s5");
		Assert.assertEquals("[top]", graph.toStringSubScopes());
		graph.addEdge(s2, s1);
		Assert.assertEquals("[top/s2/bot, top/s1/s2/bot, top/s1/bot]",
				graph.toStringSubScopes());
		graph.addEdge(s4, s3);
		Assert.assertEquals(
				"[top/s2/bot, top/s1/s2/bot, top/s1/bot, top/s4/bot, "
						+ "top/s3/s4/bot, top/s3/bot]",
				graph.toStringSubScopes());
		graph.addEdge(s4, s5);
		Assert.assertEquals(
				"[top/s2/bot, top/s1/s2/bot, top/s1/bot, top/s4/bot, "
						+ "top/s3/s4/bot, top/s3/bot, top/s5/s4/bot, top/s5/bot]",
				graph.toStringSubScopes());
		graph.addEdge(s3, s2);
		Assert.assertEquals(
				"[top/s2/bot, top/s2/s3/s4/bot, top/s2/s3/bot, top/s1/s2/bot, "
						+ "top/s1/s2/s3/s4/bot, top/s1/s2/s3/bot, top/s1/bot, top/s4/bot, "
						+ "top/s3/s4/bot, top/s3/bot, top/s5/s4/bot, top/s5/bot]",
				graph.toStringSubScopes());
	}

	@Test(expected = MuDEBSException.class)
	public void addEdgeWithCycle1() throws MuDEBSException, URISyntaxException {
		URI d = new URI("mudebs:localhost:portnumber/d");
		ScopeGraph graph = new ScopeGraph(d);
		Scope s1 = new Scope(d, "s1");
		Scope s2 = new Scope(d, "s2");
		Scope s3 = new Scope(d, "s3");
		Scope s4 = new Scope(d, "s4");
		Scope s5 = new Scope(d, "s5");
		Assert.assertEquals("[top]", graph.toStringSubScopes());
		graph.addEdge(s2, s1);
		Assert.assertEquals("[top/s2/bot, top/s1/s2/bot, top/s1/bot]",
				graph.toStringSubScopes());
		graph.addEdge(s4, s3);
		Assert.assertEquals(
				"[top/s2/bot, top/s1/s2/bot, top/s1/bot, top/s4/bot, "
						+ "top/s3/s4/bot, top/s3/bot]",
				graph.toStringSubScopes());
		graph.addEdge(s4, s5);
		Assert.assertEquals(
				"[top/s2/bot, top/s1/s2/bot, top/s1/bot, top/s4/bot, "
						+ "top/s3/s4/bot, top/s3/bot, top/s5/s4/bot, top/s5/bot]",
				graph.toStringSubScopes());
		graph.addEdge(s3, s2);
		Assert.assertEquals(
				"[top/s2/bot, top/s2/s3/s4/bot, top/s2/s3/bot, top/s1/s2/bot, "
						+ "top/s1/s2/s3/s4/bot, top/s1/s2/s3/bot, top/s1/bot, top/s4/bot, "
						+ "top/s3/s4/bot, top/s3/bot, top/s5/s4/bot, top/s5/bot]",
				graph.toStringSubScopes());
		graph.addEdge(s1, s3); // cycle, because: s2->s1, s3->s2 and s1->s3
	}

	@Test(expected = MuDEBSException.class)
	public void addEdgeWithCycle2() throws MuDEBSException, URISyntaxException {
		URI d = new URI("mudebs:localhost:portnumber/d");
		ScopeGraph graph = new ScopeGraph(d);
		Scope s1 = new Scope(d, "s1");
		Scope s2 = new Scope(d, "s2");
		Scope s3 = new Scope(d, "s3");
		Scope s4 = new Scope(d, "s4");
		Scope s5 = new Scope(d, "s5");
		Assert.assertEquals("[top]", graph.toStringSubScopes());
		graph.addEdge(s2, s1);
		Assert.assertEquals("[top/s2/bot, top/s1/s2/bot, top/s1/bot]",
				graph.toStringSubScopes());
		graph.addEdge(s4, s3);
		Assert.assertEquals(
				"[top/s2/bot, top/s1/s2/bot, top/s1/bot, top/s4/bot, "
						+ "top/s3/s4/bot, top/s3/bot]",
				graph.toStringSubScopes());
		graph.addEdge(s4, s5);
		Assert.assertEquals(
				"[top/s2/bot, top/s1/s2/bot, top/s1/bot, top/s4/bot, "
						+ "top/s3/s4/bot, top/s3/bot, top/s5/s4/bot, top/s5/bot]",
				graph.toStringSubScopes());
		graph.addEdge(s3, s2);
		Assert.assertEquals(
				"[top/s2/bot, top/s2/s3/s4/bot, top/s2/s3/bot, top/s1/s2/bot, "
						+ "top/s1/s2/s3/s4/bot, top/s1/s2/s3/bot, top/s1/bot, top/s4/bot, "
						+ "top/s3/s4/bot, top/s3/bot, top/s5/s4/bot, top/s5/bot]",
				graph.toStringSubScopes());
		graph.addEdge(s2, s4); // cycle, because: s3->s2, s4->s3 and s2->s4
	}

	@Test
	public void removeEdge1() throws MuDEBSException, URISyntaxException {
		URI d = new URI("mudebs:localhost:portnumber/d");
		ScopeGraph graph = new ScopeGraph(d);
		Scope s1 = new Scope(d, "s1");
		Scope s2 = new Scope(d, "s2");
		Scope s3 = new Scope(d, "s3");
		Scope s4 = new Scope(d, "s4");
		Scope s5 = new Scope(d, "s5");
		Assert.assertEquals("[top]", graph.toStringSubScopes());
		graph.addEdge(s2, s1);
		Assert.assertEquals("[top/s2/bot, top/s1/s2/bot, top/s1/bot]",
				graph.toStringSubScopes());
		graph.addEdge(s4, s3);
		Assert.assertEquals(
				"[top/s2/bot, top/s1/s2/bot, top/s1/bot, top/s4/bot, "
						+ "top/s3/s4/bot, top/s3/bot]",
				graph.toStringSubScopes());
		graph.addEdge(s4, s5);
		Assert.assertEquals(
				"[top/s2/bot, top/s1/s2/bot, top/s1/bot, top/s4/bot, "
						+ "top/s3/s4/bot, top/s3/bot, top/s5/s4/bot, top/s5/bot]",
				graph.toStringSubScopes());
		graph.addEdge(s3, s2);
		Assert.assertEquals(
				"[top/s2/bot, top/s2/s3/s4/bot, top/s2/s3/bot, top/s1/s2/bot, "
						+ "top/s1/s2/s3/s4/bot, top/s1/s2/s3/bot, top/s1/bot, top/s4/bot, "
						+ "top/s3/s4/bot, top/s3/bot, top/s5/s4/bot, top/s5/bot]",
				graph.toStringSubScopes());
		graph.removeEdge(s2, s1);
		Assert.assertEquals(
				"[top/s2/bot, top/s2/s3/s4/bot, top/s2/s3/bot, top/s1/bot, top/s4/bot, "
						+ "top/s3/s4/bot, top/s3/bot, top/s5/s4/bot, top/s5/bot]",
				graph.toStringSubScopes());
	}

	@Test
	public void removeEdge2() throws MuDEBSException, URISyntaxException {
		URI d = new URI("mudebs:localhost:portnumber/d");
		ScopeGraph graph = new ScopeGraph(d);
		Scope s1 = new Scope(d, "s1");
		Scope s2 = new Scope(d, "s2");
		Scope s3 = new Scope(d, "s3");
		Scope s4 = new Scope(d, "s4");
		Scope s5 = new Scope(d, "s5");
		Assert.assertEquals("[top]", graph.toStringSubScopes());
		graph.addEdge(s2, s1);
		Assert.assertEquals("[top/s2/bot, top/s1/s2/bot, top/s1/bot]",
				graph.toStringSubScopes());
		graph.addEdge(s4, s3);
		Assert.assertEquals(
				"[top/s2/bot, top/s1/s2/bot, top/s1/bot, top/s4/bot, "
						+ "top/s3/s4/bot, top/s3/bot]",
				graph.toStringSubScopes());
		graph.addEdge(s4, s5);
		Assert.assertEquals(
				"[top/s2/bot, top/s1/s2/bot, top/s1/bot, top/s4/bot, "
						+ "top/s3/s4/bot, top/s3/bot, top/s5/s4/bot, top/s5/bot]",
				graph.toStringSubScopes());
		graph.addEdge(s3, s2);
		Assert.assertEquals(
				"[top/s2/bot, top/s2/s3/s4/bot, top/s2/s3/bot, top/s1/s2/bot, "
						+ "top/s1/s2/s3/s4/bot, top/s1/s2/s3/bot, top/s1/bot, top/s4/bot, "
						+ "top/s3/s4/bot, top/s3/bot, top/s5/s4/bot, top/s5/bot]",
				graph.toStringSubScopes());
		graph.removeEdge(s2, s1);
		graph.removeEdge(s4, s3);
		Assert.assertEquals(
				"[top/s2/bot, top/s2/s3/bot, top/s1/bot, top/s4/bot, "
						+ "top/s3/bot, top/s5/s4/bot, top/s5/bot]",
				graph.toStringSubScopes());
	}

	@Test
	public void removeEdge3() throws MuDEBSException, URISyntaxException {
		URI d = new URI("mudebs:localhost:portnumber/d");
		ScopeGraph graph = new ScopeGraph(d);
		Scope s1 = new Scope(d, "s1");
		Scope s2 = new Scope(d, "s2");
		Scope s3 = new Scope(d, "s3");
		Scope s4 = new Scope(d, "s4");
		Scope s5 = new Scope(d, "s5");
		Assert.assertEquals("[top]", graph.toStringSubScopes());
		graph.addEdge(s2, s1);
		Assert.assertEquals("[top/s2/bot, top/s1/s2/bot, top/s1/bot]",
				graph.toStringSubScopes());
		graph.addEdge(s4, s3);
		Assert.assertEquals(
				"[top/s2/bot, top/s1/s2/bot, top/s1/bot, top/s4/bot, "
						+ "top/s3/s4/bot, top/s3/bot]",
				graph.toStringSubScopes());
		graph.addEdge(s4, s5);
		Assert.assertEquals(
				"[top/s2/bot, top/s1/s2/bot, top/s1/bot, top/s4/bot, "
						+ "top/s3/s4/bot, top/s3/bot, top/s5/s4/bot, top/s5/bot]",
				graph.toStringSubScopes());
		graph.addEdge(s3, s2);
		Assert.assertEquals(
				"[top/s2/bot, top/s2/s3/s4/bot, top/s2/s3/bot, top/s1/s2/bot, "
						+ "top/s1/s2/s3/s4/bot, top/s1/s2/s3/bot, top/s1/bot, top/s4/bot, "
						+ "top/s3/s4/bot, top/s3/bot, top/s5/s4/bot, top/s5/bot]",
				graph.toStringSubScopes());
		graph.removeEdge(s2, s1);
		graph.removeEdge(s4, s3);
		graph.removeEdge(s4, s5);
		Assert.assertEquals(
				"[top/s2/bot, top/s2/s3/bot, top/s1/bot, top/s4/bot, "
						+ "top/s3/bot, top/s5/bot]", graph.toStringSubScopes());
	}

	@Test
	public void removeEdge4() throws MuDEBSException, URISyntaxException {
		URI d = new URI("mudebs:localhost:portnumber/d");
		ScopeGraph graph = new ScopeGraph(d);
		Scope s1 = new Scope(d, "s1");
		Scope s2 = new Scope(d, "s2");
		Scope s3 = new Scope(d, "s3");
		Scope s4 = new Scope(d, "s4");
		Scope s5 = new Scope(d, "s5");
		Assert.assertEquals("[top]", graph.toStringSubScopes());
		graph.addEdge(s2, s1);
		Assert.assertEquals("[top/s2/bot, top/s1/s2/bot, top/s1/bot]",
				graph.toStringSubScopes());
		graph.addEdge(s4, s3);
		Assert.assertEquals(
				"[top/s2/bot, top/s1/s2/bot, top/s1/bot, top/s4/bot, "
						+ "top/s3/s4/bot, top/s3/bot]",
				graph.toStringSubScopes());
		graph.addEdge(s4, s5);
		Assert.assertEquals(
				"[top/s2/bot, top/s1/s2/bot, top/s1/bot, top/s4/bot, "
						+ "top/s3/s4/bot, top/s3/bot, top/s5/s4/bot, top/s5/bot]",
				graph.toStringSubScopes());
		graph.addEdge(s3, s2);
		Assert.assertEquals(
				"[top/s2/bot, top/s2/s3/s4/bot, top/s2/s3/bot, top/s1/s2/bot, "
						+ "top/s1/s2/s3/s4/bot, top/s1/s2/s3/bot, top/s1/bot, top/s4/bot, "
						+ "top/s3/s4/bot, top/s3/bot, top/s5/s4/bot, top/s5/bot]",
				graph.toStringSubScopes());
		graph.removeEdge(s2, s1);
		graph.removeEdge(s4, s3);
		graph.removeEdge(s4, s5);
		graph.removeEdge(s3, s2);
		Assert.assertEquals(
				"[top/s2/bot, top/s1/bot, top/s4/bot, top/s3/bot, top/s5/bot]",
				graph.toStringSubScopes());
	}

	@Test
	public void existsEdge1() throws MuDEBSException, URISyntaxException {
		URI d = new URI("mudebs:localhost:portnumber/d");
		ScopeGraph graph = new ScopeGraph(d);
		Scope s1 = new Scope(d, "s1");
		Scope s2 = new Scope(d, "s2");
		Scope s3 = new Scope(d, "s3");
		Scope s4 = new Scope(d, "s4");
		Scope s5 = new Scope(d, "s5");
		Assert.assertEquals("[top]", graph.toStringSubScopes());
		graph.addEdge(s2, s1);
		Assert.assertEquals("[top/s2/bot, top/s1/s2/bot, top/s1/bot]",
				graph.toStringSubScopes());
		graph.addEdge(s4, s3);
		Assert.assertEquals(
				"[top/s2/bot, top/s1/s2/bot, top/s1/bot, top/s4/bot, "
						+ "top/s3/s4/bot, top/s3/bot]",
				graph.toStringSubScopes());
		graph.addEdge(s4, s5);
		Assert.assertEquals(
				"[top/s2/bot, top/s1/s2/bot, top/s1/bot, top/s4/bot, "
						+ "top/s3/s4/bot, top/s3/bot, top/s5/s4/bot, top/s5/bot]",
				graph.toStringSubScopes());
		graph.addEdge(s3, s2);
		Assert.assertEquals(
				"[top/s2/bot, top/s2/s3/s4/bot, top/s2/s3/bot, top/s1/s2/bot, "
						+ "top/s1/s2/s3/s4/bot, top/s1/s2/s3/bot, top/s1/bot, top/s4/bot, "
						+ "top/s3/s4/bot, top/s3/bot, top/s5/s4/bot, top/s5/bot]",
				graph.toStringSubScopes());
		Assert.assertTrue(graph.existsEdge(s2, s1));
		Assert.assertTrue(graph.existsEdge(s3, s2));
		Assert.assertTrue(graph.existsEdge(s4, s3));
		Assert.assertTrue(graph.existsEdge(s4, s5));
		Assert.assertTrue(graph.existsEdge(graph.bot(), s5));
		Assert.assertFalse(graph.existsEdge(graph.top(), s5));
		Assert.assertFalse(graph.existsEdge(s1, graph.top()));
		Assert.assertTrue(graph.existsEdge(graph.bot(), s5));
	}
}
