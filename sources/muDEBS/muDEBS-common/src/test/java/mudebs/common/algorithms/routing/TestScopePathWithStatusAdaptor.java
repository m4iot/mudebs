/**
This file is part of the muDEBS middleware.

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Léon Lim
Contributor(s): 
 */
package mudebs.common.algorithms.routing;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;

import mudebs.common.MuDEBSException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Tests for class <tt>ScopePathWithStatusAdaptor</tt>.
 * 
 * NB: by a slight abuse of usage, the method toString and toStringPath are used
 * for comparisons.
 * 
 * @author Léon Lim
 *
 */
public class TestScopePathWithStatusAdaptor {
	private GsonBuilder gsonBuilder = new GsonBuilder();

	@Before
	public void setup() {
		gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(ScopePathWithStatus.class,
				new ScopePathWithStatusAdaptor());

	}

	@Test
	public void testSerialise() throws URISyntaxException {
		Gson gson = gsonBuilder.create();
		URI d = new URI("mudebs:localhost:portnumber/membership");
		final ScopePathWithStatus sp = new ScopePathWithStatus(d,
				ScopePathStatus.UP);
		Assert.assertEquals(
				"{\"dimension\":\"mudebs:localhost:portnumber/membership\",\"lambda\":\"UP\",\"scopeSequence\":[]}",
				gson.toJson(sp));
	}

	@Test
	public void testSerialise2() throws URISyntaxException, MuDEBSException {
		Gson gson = gsonBuilder.create();
		final URI d = new URI("mudebs:localhost:portnumber/membership");
		ScopePathWithStatus sp = new ScopePathWithStatus(d, ScopePathStatus.UP);
		final Scope is = new Scope(d, "is");
		sp.addScope(Arrays.asList(is));
		Assert.assertEquals(
				"{\"dimension\":\"mudebs:localhost:portnumber/membership\","
						+ "\"lambda\":\"UP\",\"scopeSequence\":[\"is\"]}",
				gson.toJson(sp));
	}

	@Test
	public void testSerialise3() throws URISyntaxException, MuDEBSException {
		final Gson gson = gsonBuilder.create();
		final URI d = new URI("mudebs:localhost:portnumber/membership");
		ScopePathWithStatus sp = new ScopePathWithStatus(d, ScopePathStatus.UP);
		final ScopeGraph g = new ScopeGraph(d);
		sp.addScope(g.top());
		Assert.assertEquals(
				"{\"dimension\":\"mudebs:localhost:portnumber/membership\","
						+ "\"lambda\":\"UP\",\"scopeSequence\":[\"top\"]}",
				gson.toJson(sp));
	}

	@Test
	public void testSerialise4() throws URISyntaxException, MuDEBSException {
		final Gson gson = gsonBuilder.create();
		final URI d = new URI("mudebs:localhost:portnumber/membership");
		ScopePathWithStatus sp = new ScopePathWithStatus(d, ScopePathStatus.UP);
		final ScopeGraph g = new ScopeGraph(d);
		final Scope is = new Scope(d, "is");
		final Scope ir = new Scope(d, "ir");
		g.addEdge(is, ir);
		sp.addScope(g.bot());
		Assert.assertEquals(
				"{\"dimension\":\"mudebs:localhost:portnumber/membership\","
						+ "\"lambda\":\"UP\",\"scopeSequence\":[\"bot\"]}",
				gson.toJson(sp));
	}

	@Test
	public void testSerialise5() throws URISyntaxException, MuDEBSException {
		final Gson gson = gsonBuilder.create();
		final URI d = new URI("mudebs:localhost:portnumber/membership");
		ScopePathWithStatus sp = new ScopePathWithStatus(d, ScopePathStatus.UP);
		final ScopeGraph g = new ScopeGraph(d);
		final Scope us = new Scope(d, "us");
		final Scope es = new Scope(d, "es");
		g.addEdge(us, es);
		sp.addScope(Arrays.asList(us, es));
		Assert.assertEquals(
				"{\"dimension\":\"mudebs:localhost:portnumber/membership\","
						+ "\"lambda\":\"UP\",\"scopeSequence\":[\"us\",\"es\"]}",
				gson.toJson(sp));
	}

	@Test
	public void testDeserialise() throws URISyntaxException {
		Gson gson = gsonBuilder.create();
		URI d = new URI("mudebs:localhost:portnumber/membership");
		final ScopePathWithStatus sp = new ScopePathWithStatus(d,
				ScopePathStatus.UP);
		Assert.assertEquals(
				"{\"dimension\":\"mudebs:localhost:portnumber/membership\","
						+ "\"lambda\":\"UP\",\"scopeSequence\":[]}",
				gson.toJson(sp));
		ScopePathWithStatus spDeserialised = gson.fromJson(gson.toJson(sp),
				ScopePathWithStatus.class);
		Assert.assertEquals("mudebs:localhost:portnumber/membership",
				spDeserialised.dimension.toASCIIString());
		Assert.assertEquals("()", spDeserialised.toStringPath());
	}

	@Test
	public void testDeserialise2() throws URISyntaxException, MuDEBSException {
		Gson gson = gsonBuilder.create();
		final URI d = new URI("mudebs:localhost:portnumber/membership");
		ScopePathWithStatus sp = new ScopePathWithStatus(d, ScopePathStatus.UP);
		final Scope is = new Scope(d, "is");
		sp.addScope(Arrays.asList(is));
		Assert.assertEquals(
				"{\"dimension\":\"mudebs:localhost:portnumber/membership\","
						+ "\"lambda\":\"UP\",\"scopeSequence\":[\"is\"]}",
				gson.toJson(sp));
		ScopePathWithStatus spDeserialised = gson.fromJson(gson.toJson(sp),
				ScopePathWithStatus.class);
		Assert.assertEquals("mudebs:localhost:portnumber/membership",
				spDeserialised.dimension.toASCIIString());
		Assert.assertEquals("((is), UP)", spDeserialised.toStringPath());
	}

	@Test
	public void testDeserialise3() throws URISyntaxException, MuDEBSException {
		final Gson gson = gsonBuilder.create();
		final URI d = new URI("mudebs:localhost:portnumber/membership");
		ScopePathWithStatus sp = new ScopePathWithStatus(d, ScopePathStatus.UP);
		final ScopeGraph g = new ScopeGraph(d);
		sp.addScope(g.top());
		Assert.assertEquals(
				"{\"dimension\":\"mudebs:localhost:portnumber/membership\","
						+ "\"lambda\":\"UP\",\"scopeSequence\":[\"top\"]}",
				gson.toJson(sp));
		ScopePathWithStatus spDeserialised = gson.fromJson(gson.toJson(sp),
				ScopePathWithStatus.class);
		Assert.assertEquals("mudebs:localhost:portnumber/membership",
				spDeserialised.dimension.toASCIIString());
		Assert.assertEquals("((top), UP)", spDeserialised.toStringPath());
	}

	@Test
	public void testDeserialise4() throws URISyntaxException, MuDEBSException {
		final Gson gson = gsonBuilder.create();
		final URI d = new URI("mudebs:localhost:portnumber/membership");
		ScopePathWithStatus sp = new ScopePathWithStatus(d, ScopePathStatus.UP);
		final ScopeGraph g = new ScopeGraph(d);
		final Scope is = new Scope(d, "is");
		final Scope ir = new Scope(d, "ir");
		g.addEdge(is, ir);
		sp.addScope(g.bot());
		Assert.assertEquals(
				"{\"dimension\":\"mudebs:localhost:portnumber/membership\","
						+ "\"lambda\":\"UP\",\"scopeSequence\":[\"bot\"]}",
				gson.toJson(sp));
		ScopePathWithStatus spDeserialised = gson.fromJson(gson.toJson(sp),
				ScopePathWithStatus.class);
		Assert.assertEquals("mudebs:localhost:portnumber/membership",
				spDeserialised.dimension.toASCIIString());
		Assert.assertEquals("((bot), UP)", spDeserialised.toStringPath());
	}

	@Test
	public void testDeserialise5() throws URISyntaxException, MuDEBSException {
		final Gson gson = gsonBuilder.create();
		final URI d = new URI("mudebs:localhost:portnumber/membership");
		ScopePathWithStatus sp = new ScopePathWithStatus(d, ScopePathStatus.UP);
		final ScopeGraph g = new ScopeGraph(d);
		final Scope us = new Scope(d, "us");
		final Scope es = new Scope(d, "es");
		g.addEdge(us, es);
		sp.addScope(Arrays.asList(us, es));
		Assert.assertEquals(
				"{\"dimension\":\"mudebs:localhost:portnumber/membership\","
						+ "\"lambda\":\"UP\",\"scopeSequence\":[\"us\",\"es\"]}",
				gson.toJson(sp));
		ScopePathWithStatus spDeserialised = gson.fromJson(gson.toJson(sp),
				ScopePathWithStatus.class);
		Assert.assertEquals("mudebs:localhost:portnumber/membership",
				spDeserialised.dimension.toASCIIString());
		Assert.assertEquals("((us, es), UP)", spDeserialised.toStringPath());

	}
}
