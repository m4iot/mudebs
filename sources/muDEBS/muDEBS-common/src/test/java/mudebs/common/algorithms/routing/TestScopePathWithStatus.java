/**
This file is part of the muDEBS middleware.

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Léon Lim
Contributor(s): 
 */
package mudebs.common.algorithms.routing;

import java.net.URI;
import java.net.URISyntaxException;

import org.junit.Assert;

import mudebs.common.MuDEBSException;

import org.junit.Test;

/**
 * Tests for the class <tt>ScopePathWithStatus</tt>
 * 
 * NB: by a slight abuse of usage, the method toStringPath is used for
 * comparisons.
 * 
 * @author Léon Lim
 *
 */
public class TestScopePathWithStatus {
	@Test(expected = IllegalArgumentException.class)
	public void testConstructorNoDimension() throws MuDEBSException,
			URISyntaxException {
		new ScopePathWithStatus(null, ScopePathStatus.UP);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConstructorNullDimension() throws URISyntaxException {
		new ScopePathWithStatus(null, ScopePathStatus.UP);
	}

	@Test
	public void testConstructorAndIdentifier() throws URISyntaxException {
		URI d = new URI("mudebs:localhost:portnumber/d");
		ScopePathWithStatus sp = new ScopePathWithStatus(d, ScopePathStatus.UP);
		Assert.assertNotNull(sp);
	}

	@Test(expected = MuDEBSException.class)
	public void testAddScopeToPathWithStatusWithDifferentDimension()
			throws MuDEBSException, URISyntaxException {
		URI d = new URI("mudebs:localhost:portnumber/d");
		URI d2 = new URI("mudebs:localhost:portnumber/d2");
		Scope s = new Scope(d, "s1");
		ScopePathWithStatus sp = new ScopePathWithStatus(d2, ScopePathStatus.UP);
		sp.addScope(s);
	}

	@Test
	public void testAddScopeToPathWithStatus1() throws MuDEBSException,
			URISyntaxException {
		URI d = new URI("mudebs:localhost:portnumber/d");
		Scope s = new Scope(d, "s1");
		ScopePathWithStatus sp = new ScopePathWithStatus(d, ScopePathStatus.UP);
		sp.addScope(s);
	}

	@Test(expected = MuDEBSException.class)
	public void testAddScopeToPathNoSubSupRelation() throws MuDEBSException,
			URISyntaxException {
		URI d = new URI("mudebs:localhost:portnumber/d");
		Scope s = new Scope(d, "s1");
		Scope s2 = new Scope(d, "s2");
		ScopePath sp = new ScopePathWithStatus(d, ScopePathStatus.UP);
		sp.addScope(s);
		sp.addScope(s2);
	}

	@Test
	public void testAddScopeToPathWithStatusWithCorrectRelation()
			throws MuDEBSException, URISyntaxException {
		URI d = new URI("mudebs:localhost:portnumber/d");
		Scope s = new Scope(d, "s1");
		Scope s2 = new Scope(d, "s2");
		ScopePathWithStatus sp = new ScopePathWithStatus(d, ScopePathStatus.UP);
		sp.addScope(s);
		s2.addSuperScope(s);
		sp.addScope(s2);
	}

	@Test
	public void testAddScopeToPathWithNoCorrectRelation()
			throws MuDEBSException, URISyntaxException {
		URI d = new URI("mudebs:localhost:portnumber/d");
		Scope s = new Scope(d, "s");
		Scope s2 = new Scope(d, "s2");
		Scope s3 = new Scope(d, "s3");
		ScopePathWithStatus sp = new ScopePathWithStatus(d, ScopePathStatus.UP);
		sp.addScope(s);
		s2.addSuperScope(s);
		sp.addScope(s2);
		s2.addSuperScope(s3);
		Assert.assertEquals(ScopePathStatus.DOWN, sp.status());
	}

	@Test(expected = MuDEBSException.class)
	public void testAddScopeToPathWithNoCorrectRelation2()
			throws MuDEBSException, URISyntaxException {
		URI d = new URI("mudebs:localhost:portnumber/d");
		Scope s = new Scope(d, "s");
		Scope s2 = new Scope(d, "s2");
		Scope s3 = new Scope(d, "s3");
		ScopePathWithStatus sp = new ScopePathWithStatus(d, ScopePathStatus.UP);
		sp.addScope(s);
		s2.addSuperScope(s);
		sp.addScope(s2);
		s2.addSuperScope(s3);
		sp.addScope(s3);
	}

	@Test
	public void testAddScopeToPathWithCorrectGraph1() throws MuDEBSException,
			URISyntaxException {
		URI d1 = new URI("mudebs:localhost:portnumber/membership");
		Scope ls = new Scope(d1, "ls");
		Scope us = new Scope(d1, "us");
		Scope es = new Scope(d1, "es");
		Scope fs = new Scope(d1, "fs");
		Scope is = new Scope(d1, "is");
		Scope ir = new Scope(d1, "ir");
		ls.addSuperScope(us);
		es.addSubScope(us);
		fs.addSuperScope(es);
		is.addSuperScope(fs);
		ir.addSubScope(is);
		ScopePathWithStatus sp = new ScopePathWithStatus(d1, ScopePathStatus.UP);
		sp.addScope(ls);
		Assert.assertEquals(ScopePathStatus.UP, sp.status());
		Assert.assertEquals(ls,
				sp.scopeSequence().get(sp.scopeSequence().size() - 1));
	}

	@Test
	public void testAddScopeToPathWithCorrectGraph2() throws MuDEBSException,
			URISyntaxException {
		URI d1 = new URI("mudebs:localhost:portnumber/membership");
		Scope ls = new Scope(d1, "ls");
		Scope us = new Scope(d1, "us");
		Scope es = new Scope(d1, "es");
		Scope fs = new Scope(d1, "fs");
		Scope is = new Scope(d1, "is");
		Scope ir = new Scope(d1, "ir");
		ls.addSuperScope(us);
		es.addSubScope(us);
		fs.addSuperScope(es);
		is.addSuperScope(fs);
		ir.addSubScope(is);
		ScopePathWithStatus sp = new ScopePathWithStatus(d1, ScopePathStatus.UP);
		sp.addScope(ls);
		Assert.assertEquals(ScopePathStatus.UP, sp.status());
		Assert.assertEquals(ls,
				sp.scopeSequence().get(sp.scopeSequence().size() - 1));
		sp.addScope(us);
		Assert.assertEquals(ScopePathStatus.UP, sp.status());
		Assert.assertEquals(us,
				sp.scopeSequence().get(sp.scopeSequence().size() - 1));
	}

	@Test
	public void testAddScopeToPathWithCorrectGraph3() throws MuDEBSException,
			URISyntaxException {
		URI d1 = new URI("mudebs:localhost:portnumber/membership");
		Scope ls = new Scope(d1, "ls");
		Scope us = new Scope(d1, "us");
		Scope es = new Scope(d1, "es");
		Scope fs = new Scope(d1, "fs");
		Scope is = new Scope(d1, "is");
		Scope ir = new Scope(d1, "ir");
		ls.addSuperScope(us);
		es.addSubScope(us);
		fs.addSuperScope(es);
		is.addSuperScope(fs);
		ir.addSubScope(is);
		ScopePathWithStatus sp = new ScopePathWithStatus(d1, ScopePathStatus.UP);
		sp.addScope(ls);
		Assert.assertEquals(ScopePathStatus.UP, sp.status());
		Assert.assertEquals(ls,
				sp.scopeSequence().get(sp.scopeSequence().size() - 1));
		sp.addScope(us);
		Assert.assertEquals(ScopePathStatus.UP, sp.status());
		Assert.assertEquals(us,
				sp.scopeSequence().get(sp.scopeSequence().size() - 1));
		sp.addScope(es);
		Assert.assertEquals(ScopePathStatus.UP, sp.status());
		Assert.assertEquals(es,
				sp.scopeSequence().get(sp.scopeSequence().size() - 1));
	}

	@Test
	public void testAddScopeToPathWithCorrectGraph4() throws MuDEBSException,
			URISyntaxException {
		URI d1 = new URI("mudebs:localhost:portnumber/membership");
		Scope ls = new Scope(d1, "ls");
		Scope us = new Scope(d1, "us");
		Scope es = new Scope(d1, "es");
		Scope fs = new Scope(d1, "fs");
		Scope is = new Scope(d1, "is");
		Scope ir = new Scope(d1, "ir");
		ls.addSuperScope(us);
		es.addSubScope(us);
		fs.addSuperScope(es);
		is.addSuperScope(fs);
		ir.addSubScope(is);
		ScopePathWithStatus sp = new ScopePathWithStatus(d1, ScopePathStatus.UP);
		sp.addScope(ls);
		Assert.assertEquals(ScopePathStatus.UP, sp.status());
		Assert.assertEquals(ls,
				sp.scopeSequence().get(sp.scopeSequence().size() - 1));
		sp.addScope(us);
		Assert.assertEquals(ScopePathStatus.UP, sp.status());
		Assert.assertEquals(us,
				sp.scopeSequence().get(sp.scopeSequence().size() - 1));
		sp.addScope(es);
		Assert.assertEquals(ScopePathStatus.UP, sp.status());
		Assert.assertEquals(es,
				sp.scopeSequence().get(sp.scopeSequence().size() - 1));
		sp.addScope(fs);
		Assert.assertEquals(ScopePathStatus.DOWN, sp.status());
		Assert.assertEquals(fs,
				sp.scopeSequence().get(sp.scopeSequence().size() - 1));
	}

	@Test
	public void testAddScopeToPathWithCorrectGraph5() throws MuDEBSException,
			URISyntaxException {
		URI d1 = new URI("mudebs:localhost:portnumber/membership");
		Scope ls = new Scope(d1, "ls");
		Scope us = new Scope(d1, "us");
		Scope es = new Scope(d1, "es");
		Scope fs = new Scope(d1, "fs");
		Scope is = new Scope(d1, "is");
		Scope ir = new Scope(d1, "ir");
		ls.addSuperScope(us);
		es.addSubScope(us);
		fs.addSuperScope(es);
		is.addSuperScope(fs);
		ir.addSubScope(is);
		ScopePathWithStatus sp = new ScopePathWithStatus(d1, ScopePathStatus.UP);
		sp.addScope(ls);
		Assert.assertEquals(ScopePathStatus.UP, sp.status());
		Assert.assertEquals(ls,
				sp.scopeSequence().get(sp.scopeSequence().size() - 1));
		sp.addScope(us);
		Assert.assertEquals(ScopePathStatus.UP, sp.status());
		Assert.assertEquals(us,
				sp.scopeSequence().get(sp.scopeSequence().size() - 1));
		sp.addScope(es);
		Assert.assertEquals(ScopePathStatus.UP, sp.status());
		Assert.assertEquals(es,
				sp.scopeSequence().get(sp.scopeSequence().size() - 1));
		sp.addScope(fs);
		Assert.assertEquals(ScopePathStatus.DOWN, sp.status());
		Assert.assertEquals(fs,
				sp.scopeSequence().get(sp.scopeSequence().size() - 1));
		sp.addScope(is);
		Assert.assertEquals(ScopePathStatus.DOWN, sp.status());
		Assert.assertEquals(is,
				sp.scopeSequence().get(sp.scopeSequence().size() - 1));
	}

	@Test(expected = MuDEBSException.class)
	public void testAddScopeToPathWithCorrectGraph5bis()
			throws MuDEBSException, URISyntaxException {
		URI d1 = new URI("mudebs:localhost:portnumber/membership");
		Scope ls = new Scope(d1, "ls");
		Scope us = new Scope(d1, "us");
		Scope es = new Scope(d1, "es");
		Scope fs = new Scope(d1, "fs");
		Scope is = new Scope(d1, "is");
		Scope ir = new Scope(d1, "ir");
		ls.addSuperScope(us);
		es.addSubScope(us);
		fs.addSuperScope(es);
		is.addSuperScope(fs);
		ir.addSubScope(is);
		ScopePathWithStatus sp = new ScopePathWithStatus(d1, ScopePathStatus.UP);
		sp.addScope(ls);
		Assert.assertEquals(ScopePathStatus.UP, sp.status());
		Assert.assertEquals(ls,
				sp.scopeSequence().get(sp.scopeSequence().size() - 1));
		sp.addScope(us);
		Assert.assertEquals(ScopePathStatus.UP, sp.status());
		Assert.assertEquals(us,
				sp.scopeSequence().get(sp.scopeSequence().size() - 1));
		sp.addScope(es);
		Assert.assertEquals(ScopePathStatus.UP, sp.status());
		Assert.assertEquals(es,
				sp.scopeSequence().get(sp.scopeSequence().size() - 1));
		sp.addScope(fs);
		Assert.assertEquals(ScopePathStatus.DOWN, sp.status());
		Assert.assertEquals(fs,
				sp.scopeSequence().get(sp.scopeSequence().size() - 1));
		sp.addScope(is);
		Assert.assertEquals(ScopePathStatus.DOWN, sp.status());
		Assert.assertEquals(is,
				sp.scopeSequence().get(sp.scopeSequence().size() - 1));
		sp.addScope(ir); // status = DOWN, but ir not sub-scope of last scope
	}

	@Test
	public void testAddScopeToPathWithCorrectGraph6() throws MuDEBSException,
			URISyntaxException {
		URI d1 = new URI("mudebs:localhost:portnumber/membership");
		Scope ls = new Scope(d1, "ls");
		Scope us = new Scope(d1, "us");
		Scope es = new Scope(d1, "es");
		Scope fs = new Scope(d1, "fs");
		Scope is = new Scope(d1, "is");
		Scope ir = new Scope(d1, "ir");
		ls.addSuperScope(us);
		es.addSubScope(us);
		fs.addSuperScope(es);
		is.addSuperScope(fs);
		ir.addSubScope(is);
		ScopePathWithStatus sp = new ScopePathWithStatus(d1, ScopePathStatus.UP);
		sp.addScope(ls);
		Assert.assertEquals(ScopePathStatus.UP, sp.status());
		Assert.assertEquals(ls,
				sp.scopeSequence().get(sp.scopeSequence().size() - 1));
		sp.addScope(us);
		Assert.assertEquals(ScopePathStatus.UP, sp.status());
		Assert.assertEquals(us,
				sp.scopeSequence().get(sp.scopeSequence().size() - 1));
		sp.addScope(es);
		Assert.assertEquals(ScopePathStatus.UP, sp.status());
		Assert.assertEquals(es,
				sp.scopeSequence().get(sp.scopeSequence().size() - 1));
		sp.addScope(fs);
		Assert.assertEquals(ScopePathStatus.DOWN, sp.status());
		Assert.assertEquals(fs,
				sp.scopeSequence().get(sp.scopeSequence().size() - 1));
		sp.addScope(is);
		Assert.assertEquals(ScopePathStatus.DOWN, sp.status());
		Assert.assertEquals(is,
				sp.scopeSequence().get(sp.scopeSequence().size() - 1));
		Assert.assertEquals("((ls, us, es, fs, is), DOWN)", sp.toStringPath());
	}
}
