/**
This file is part of the muDEBS middleware.

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Léon Lim
 */
package mudebs.common.algorithms.routing;

import java.net.URI;
import java.net.URISyntaxException;

import org.junit.Assert;

import mudebs.common.MuDEBSException;

import org.junit.Test;

/**
 * Tests for the class <tt>Scope</tt>
 * 
 * NB: by a slight abuse of usage, the method toStringSubScopes is used for
 * comparisons.
 * 
 * @author Denis Conan
 *
 */
public class TestScope {

	@Test(expected = IllegalArgumentException.class)
	public void testConstructorNoDimension() throws MuDEBSException,
			URISyntaxException {
		new Scope(null, "id");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConstructorNullId() throws URISyntaxException {
		URI d = new URI("mudebs:localhost:portnumber/d");
		new Scope(d, null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConstructorEmptyId() throws URISyntaxException {
		URI d = new URI("mudebs:localhost:portnumber/d");
		new Scope(d, "");
	}

	@Test
	public void testConstructorAndIdentifier() throws URISyntaxException {
		URI d = new URI("mudebs:localhost:portnumber/d");
		Scope s = new Scope(d, "id");
		Assert.assertNotNull(s);
		Assert.assertEquals("id", s.identifier());
	}

	@Test
	public void testAddSubScope() throws MuDEBSException, URISyntaxException {
		URI d = new URI("mudebs:localhost:portnumber/d");
		Scope s = new Scope(d, "id");
		Assert.assertNotNull(s);
		Scope s2 = new Scope(d, "id2");
		Assert.assertNotNull(s2);
		s.addSubScope(s2);
		Assert.assertEquals(s.subScopes().get(0), s2);
		Assert.assertEquals(s2.superScopes().get(0), s);
		Assert.assertEquals(0, s.superScopes().size());
		Assert.assertEquals(0, s2.subScopes().size());
	}

	@Test
	public void testRemoveSubScope() throws MuDEBSException, URISyntaxException {
		URI d = new URI("mudebs:localhost:portnumber/d");
		Scope s = new Scope(d, "id");
		Assert.assertNotNull(s);
		Scope s2 = new Scope(d, "id2");
		Assert.assertNotNull(s2);
		s.addSubScope(s2);
		Assert.assertEquals(s.subScopes().get(0), s2);
		Assert.assertEquals(s2.superScopes().get(0), s);
		Assert.assertEquals(0, s.superScopes().size());
		Assert.assertEquals(0, s2.subScopes().size());
		s.removeSubScope(s2);
		Assert.assertEquals(0, s.subScopes().size());
		Assert.assertEquals(0, s2.superScopes().size());
	}

	@Test(expected = MuDEBSException.class)
	public void testAddSubScopeSame() throws MuDEBSException,
			URISyntaxException {
		URI d = new URI("mudebs:localhost:portnumber/d");
		Scope s = new Scope(d, "id");
		Assert.assertNotNull(s);
		Scope s2 = new Scope(d, "id2");
		Assert.assertNotNull(s2);
		s.addSubScope(s2);
		s.addSubScope(s2);
	}

	@Test
	public void testAddTwoSubScopes() throws MuDEBSException,
			URISyntaxException {
		URI d = new URI("mudebs:localhost:portnumber/d");
		Scope s = new Scope(d, "id");
		Assert.assertNotNull(s);
		Scope s2 = new Scope(d, "id2");
		Assert.assertNotNull(s2);
		Scope s3 = new Scope(d, "id3");
		Assert.assertNotNull(s3);
		s.addSubScope(s2);
		s.addSubScope(s3);
		Assert.assertTrue(s.subScopes().contains(s2));
		Assert.assertTrue(s.subScopes().contains(s3));
		Assert.assertEquals(s2.superScopes().get(0), s);
		Assert.assertEquals(s3.superScopes().get(0), s);
		Assert.assertEquals(0, s.superScopes().size());
		Assert.assertEquals(0, s2.subScopes().size());
		Assert.assertEquals(0, s3.subScopes().size());
	}

	@Test
	public void testAddTwoSubScopesAndRemove() throws MuDEBSException,
			URISyntaxException {
		URI d = new URI("mudebs:localhost:portnumber/d");
		Scope s = new Scope(d, "id");
		Assert.assertNotNull(s);
		Scope s2 = new Scope(d, "id2");
		Assert.assertNotNull(s2);
		Scope s3 = new Scope(d, "id3");
		Assert.assertNotNull(s3);
		s.addSubScope(s2);
		s.addSubScope(s3);
		Assert.assertTrue(s.subScopes().contains(s2));
		Assert.assertTrue(s.subScopes().contains(s3));
		Assert.assertEquals(s2.superScopes().get(0), s);
		Assert.assertEquals(s3.superScopes().get(0), s);
		Assert.assertEquals(0, s.superScopes().size());
		Assert.assertEquals(0, s2.subScopes().size());
		Assert.assertEquals(0, s3.subScopes().size());
		s.removeSubScope(s2);
		Assert.assertEquals(1, s.subScopes().size());
		Assert.assertEquals(0, s2.superScopes().size());
		s.removeSubScope(s2);
		Assert.assertEquals(1, s.subScopes().size());
		Assert.assertEquals(0, s2.superScopes().size());
		s.removeSubScope(s3);
		Assert.assertEquals(0, s.subScopes().size());
		Assert.assertEquals(0, s3.superScopes().size());
	}

	@Test
	public void testAddTwoSubScopeThreeLevels() throws MuDEBSException,
			URISyntaxException {
		URI d = new URI("mudebs:localhost:portnumber/d");
		Scope s = new Scope(d, "id");
		Assert.assertNotNull(s);
		Scope s2 = new Scope(d, "id2");
		Assert.assertNotNull(s2);
		Scope s3 = new Scope(d, "id3");
		Assert.assertNotNull(s3);
		s.addSubScope(s2);
		s2.addSubScope(s3);
		Assert.assertTrue(s.subScopes().contains(s2));
		Assert.assertTrue(s2.subScopes().contains(s3));
		Assert.assertEquals(s2.superScopes().get(0), s);
		Assert.assertEquals(s3.superScopes().get(0), s2);
		Assert.assertEquals(0, s.superScopes().size());
		Assert.assertEquals(1, s2.subScopes().size());
		Assert.assertEquals(0, s3.subScopes().size());
	}

	@Test
	public void testAddTwoSubScopeThreeLevelsAndRemove()
			throws MuDEBSException, URISyntaxException {
		URI d = new URI("mudebs:localhost:portnumber/d");
		Scope s = new Scope(d, "id");
		Assert.assertNotNull(s);
		Scope s2 = new Scope(d, "id2");
		Assert.assertNotNull(s2);
		Scope s3 = new Scope(d, "id3");
		Assert.assertNotNull(s3);
		s.addSubScope(s2);
		s2.addSubScope(s3);
		Assert.assertTrue(s.subScopes().contains(s2));
		Assert.assertTrue(s2.subScopes().contains(s3));
		Assert.assertEquals(s2.superScopes().get(0), s);
		Assert.assertEquals(s3.superScopes().get(0), s2);
		Assert.assertEquals(0, s.superScopes().size());
		Assert.assertEquals(1, s2.subScopes().size());
		Assert.assertEquals(0, s3.subScopes().size());
		s.removeSubScope(s2);
		Assert.assertEquals(0, s.subScopes().size());
		Assert.assertEquals(0, s2.superScopes().size());
		s.removeSubScope(s3);
		Assert.assertEquals(0, s.subScopes().size());
		Assert.assertEquals(1, s3.superScopes().size());
		s2.removeSubScope(s3);
		Assert.assertEquals(0, s2.subScopes().size());
		Assert.assertEquals(0, s3.superScopes().size());
	}

	@Test
	public void testAddSubScopesDirectAndIndirect() throws MuDEBSException,
			URISyntaxException {
		URI d = new URI("mudebs:localhost:portnumber/d");
		Scope s = new Scope(d, "id");
		Assert.assertNotNull(s);
		Scope s2 = new Scope(d, "id2");
		Assert.assertNotNull(s2);
		Scope s3 = new Scope(d, "id3");
		Assert.assertNotNull(s3);
		s.addSubScope(s2);
		s2.addSubScope(s3);
		s.addSubScope(s3);
		Assert.assertTrue(s.subScopes().contains(s2));
		Assert.assertTrue(s2.subScopes().contains(s3));
		Assert.assertTrue(s.subScopes().contains(s3));
		Assert.assertEquals(s2.superScopes().get(0), s);
		Assert.assertTrue(s3.superScopes().contains(s));
		Assert.assertTrue(s3.superScopes().contains(s2));
	}

	@Test
	public void testToStringSubScopesToString() throws MuDEBSException,
			URISyntaxException {
		URI d = new URI("mudebs:localhost:portnumber/d");
		Scope s = new Scope(d, "id");
		Assert.assertNotNull(s);
		Scope s2 = new Scope(d, "id2");
		Assert.assertNotNull(s2);
		Scope s3 = new Scope(d, "id3");
		Assert.assertNotNull(s3);
		Scope s4 = new Scope(d, "id4");
		Assert.assertNotNull(s3);
		Scope s5 = new Scope(d, "id5");
		Assert.assertNotNull(s3);
		s.addSubScope(s2);
		s.addSubScope(s3);
		s.addSubScope(s4);
		s2.addSubScope(s5);
		Assert.assertTrue(s.subScopes().contains(s2));
		Assert.assertTrue(s.subScopes().contains(s3));
		Assert.assertTrue(s.subScopes().contains(s4));
		Assert.assertTrue(s2.subScopes().contains(s5));
		Assert.assertEquals(s2.superScopes().get(0), s);
		Assert.assertEquals(s3.superScopes().get(0), s);
		Assert.assertEquals(s4.superScopes().get(0), s);
		Assert.assertEquals(s5.superScopes().get(0), s2);
		Assert.assertNotSame("[null]", s.toStringSubScopes());
	}

	@Test
	public void testAddSuperScope() throws MuDEBSException, URISyntaxException {
		URI d = new URI("mudebs:localhost:portnumber/d");
		Scope s = new Scope(d, "id");
		Assert.assertNotNull(s);
		Scope s2 = new Scope(d, "id2");
		Assert.assertNotNull(s2);
		s.addSuperScope(s2);
		Assert.assertEquals(s.superScopes().get(0), s2);
		Assert.assertEquals(s2.subScopes().get(0), s);
		Assert.assertEquals(0, s.subScopes().size());
		Assert.assertEquals(0, s2.superScopes().size());
	}

	@Test
	public void testRemoveSuperScope() throws MuDEBSException,
			URISyntaxException {
		URI d = new URI("mudebs:localhost:portnumber/d");
		Scope s = new Scope(d, "id");
		Assert.assertNotNull(s);
		Scope s2 = new Scope(d, "id2");
		Assert.assertNotNull(s2);
		s.addSuperScope(s2);
		Assert.assertEquals(s.superScopes().get(0), s2);
		Assert.assertEquals(s2.subScopes().get(0), s);
		Assert.assertEquals(0, s.subScopes().size());
		Assert.assertEquals(0, s2.superScopes().size());
		s.removeSuperScope(s2);
		Assert.assertEquals(0, s.superScopes().size());
		Assert.assertEquals(0, s2.subScopes().size());
	}

	@Test(expected = MuDEBSException.class)
	public void testAddSuperScopeSame() throws MuDEBSException,
			URISyntaxException {
		URI d = new URI("mudebs:localhost:portnumber/d");
		Scope s = new Scope(d, "id");
		Assert.assertNotNull(s);
		Scope s2 = new Scope(d, "id2");
		Assert.assertNotNull(s2);
		s.addSuperScope(s2);
		s.addSuperScope(s2);
	}

	@Test
	public void testAddTwoSuperScopes() throws MuDEBSException,
			URISyntaxException {
		URI d = new URI("mudebs:localhost:portnumber/d");
		Scope s = new Scope(d, "id");
		Assert.assertNotNull(s);
		Scope s2 = new Scope(d, "id2");
		Assert.assertNotNull(s2);
		Scope s3 = new Scope(d, "id3");
		Assert.assertNotNull(s3);
		s.addSuperScope(s2);
		s.addSuperScope(s3);
		Assert.assertTrue(s.superScopes().contains(s2));
		Assert.assertTrue(s.superScopes().contains(s3));
		Assert.assertEquals(s2.subScopes().get(0), s);
		Assert.assertEquals(s3.subScopes().get(0), s);
		Assert.assertEquals(0, s.subScopes().size());
		Assert.assertEquals(0, s2.superScopes().size());
		Assert.assertEquals(0, s3.superScopes().size());
	}

	@Test
	public void testAddTwoSuperScopesAndRemove() throws MuDEBSException,
			URISyntaxException {
		URI d = new URI("mudebs:localhost:portnumber/d");
		Scope s = new Scope(d, "id");
		Assert.assertNotNull(s);
		Scope s2 = new Scope(d, "id2");
		Assert.assertNotNull(s2);
		Scope s3 = new Scope(d, "id3");
		Assert.assertNotNull(s3);
		s.addSuperScope(s2);
		s.addSuperScope(s3);
		Assert.assertTrue(s.superScopes().contains(s2));
		Assert.assertTrue(s.superScopes().contains(s3));
		Assert.assertEquals(s2.subScopes().get(0), s);
		Assert.assertEquals(s3.subScopes().get(0), s);
		Assert.assertEquals(0, s.subScopes().size());
		Assert.assertEquals(0, s2.superScopes().size());
		Assert.assertEquals(0, s3.superScopes().size());
		s.removeSuperScope(s2);
		Assert.assertEquals(1, s.superScopes().size());
		Assert.assertEquals(0, s2.subScopes().size());
		s.removeSuperScope(s2);
		Assert.assertEquals(1, s.superScopes().size());
		Assert.assertEquals(0, s2.subScopes().size());
		s.removeSuperScope(s3);
		Assert.assertEquals(0, s.superScopes().size());
		Assert.assertEquals(0, s3.subScopes().size());
	}

	@Test
	public void testAddTwoSuperScopeThreeLevels() throws MuDEBSException,
			URISyntaxException {
		URI d = new URI("mudebs:localhost:portnumber/d");
		Scope s = new Scope(d, "id");
		Assert.assertNotNull(s);
		Scope s2 = new Scope(d, "id2");
		Assert.assertNotNull(s2);
		Scope s3 = new Scope(d, "id3");
		Assert.assertNotNull(s3);
		s.addSuperScope(s2);
		s2.addSuperScope(s3);
		Assert.assertTrue(s.superScopes().contains(s2));
		Assert.assertTrue(s2.superScopes().contains(s3));
		Assert.assertEquals(s2.subScopes().get(0), s);
		Assert.assertEquals(s3.subScopes().get(0), s2);
		Assert.assertEquals(0, s.subScopes().size());
		Assert.assertEquals(1, s2.superScopes().size());
		Assert.assertEquals(0, s3.superScopes().size());
	}

	@Test
	public void testAddTwoSuperScopeThreeLevelsAndRemove()
			throws MuDEBSException, URISyntaxException {
		URI d = new URI("mudebs:localhost:portnumber/d");
		Scope s = new Scope(d, "id");
		Assert.assertNotNull(s);
		Scope s2 = new Scope(d, "id2");
		Assert.assertNotNull(s2);
		Scope s3 = new Scope(d, "id3");
		Assert.assertNotNull(s3);
		s.addSuperScope(s2);
		s2.addSuperScope(s3);
		Assert.assertTrue(s.superScopes().contains(s2));
		Assert.assertTrue(s2.superScopes().contains(s3));
		Assert.assertEquals(s2.subScopes().get(0), s);
		Assert.assertEquals(s3.subScopes().get(0), s2);
		Assert.assertEquals(0, s.subScopes().size());
		Assert.assertEquals(1, s2.superScopes().size());
		Assert.assertEquals(0, s3.superScopes().size());
		s.removeSuperScope(s2);
		Assert.assertEquals(0, s.superScopes().size());
		Assert.assertEquals(0, s2.subScopes().size());
		s.removeSuperScope(s3);
		Assert.assertEquals(0, s.superScopes().size());
		Assert.assertEquals(1, s3.subScopes().size());
		s2.removeSuperScope(s3);
		Assert.assertEquals(0, s2.superScopes().size());
		Assert.assertEquals(0, s3.subScopes().size());
	}

	@Test
	public void testAddSuperbScopesDirectAndIndirect() throws MuDEBSException,
			URISyntaxException {
		URI d = new URI("mudebs:localhost:portnumber/d");
		Scope s = new Scope(d, "id");
		Assert.assertNotNull(s);
		Scope s2 = new Scope(d, "id2");
		Assert.assertNotNull(s2);
		Scope s3 = new Scope(d, "id3");
		Assert.assertNotNull(s3);
		s.addSuperScope(s2);
		s2.addSuperScope(s3);
		s.addSuperScope(s3);
		Assert.assertTrue(s.superScopes().contains(s2));
		Assert.assertTrue(s2.superScopes().contains(s3));
		Assert.assertTrue(s.superScopes().contains(s3));
		Assert.assertEquals(s2.subScopes().get(0), s);
		Assert.assertTrue(s3.subScopes().contains(s));
		Assert.assertTrue(s3.subScopes().contains(s2));
	}

	@Test
	public void testToStringSuperScopesToString() throws MuDEBSException,
			URISyntaxException {
		URI d = new URI("mudebs:localhost:portnumber/d");
		Scope s = new Scope(d, "id");
		Assert.assertNotNull(s);
		Scope s2 = new Scope(d, "id2");
		Assert.assertNotNull(s2);
		Scope s3 = new Scope(d, "id3");
		Assert.assertNotNull(s3);
		Scope s4 = new Scope(d, "id4");
		Assert.assertNotNull(s3);
		Scope s5 = new Scope(d, "id5");
		Assert.assertNotNull(s3);
		s.addSuperScope(s2);
		s.addSuperScope(s3);
		s.addSuperScope(s4);
		s2.addSuperScope(s5);
		Assert.assertTrue(s.superScopes().contains(s2));
		Assert.assertTrue(s.superScopes().contains(s3));
		Assert.assertTrue(s.superScopes().contains(s4));
		Assert.assertTrue(s2.superScopes().contains(s5));
		Assert.assertEquals(s2.subScopes().get(0), s);
		Assert.assertEquals(s3.subScopes().get(0), s);
		Assert.assertEquals(s4.subScopes().get(0), s);
		Assert.assertEquals(s5.subScopes().get(0), s2);
		Assert.assertNotSame("[null]", s.toStringSuperScopes());
	}

	@Test(expected = MuDEBSException.class)
	public void testAddSubScopeAlreadySuperScope() throws MuDEBSException,
			URISyntaxException {
		URI d = new URI("mudebs:localhost:portnumber/d");
		Scope s = new Scope(d, "id");
		Assert.assertNotNull(s);
		Scope s2 = new Scope(d, "id2");
		Assert.assertNotNull(s2);
		s.addSuperScope(s2);
		s.addSubScope(s2);
	}

	@Test(expected = MuDEBSException.class)
	public void testAddSubScopeAlreadyInSuperScopes() throws MuDEBSException,
			URISyntaxException {
		URI d = new URI("mudebs:localhost:portnumber/d");
		Scope s = new Scope(d, "id");
		Assert.assertNotNull(s);
		Scope s2 = new Scope(d, "id2");
		Assert.assertNotNull(s2);
		Scope s3 = new Scope(d, "id3");
		Assert.assertNotNull(s3);
		s.addSuperScope(s2);
		s2.addSuperScope(s3);
		s.addSubScope(s3);
	}

	@Test(expected = MuDEBSException.class)
	public void testAddSuperScopeAlreadySubScope() throws MuDEBSException,
			URISyntaxException {
		URI d = new URI("mudebs:localhost:portnumber/d");
		Scope s = new Scope(d, "id");
		Assert.assertNotNull(s);
		Scope s2 = new Scope(d, "id2");
		Assert.assertNotNull(s2);
		s.addSubScope(s2);
		s.addSuperScope(s2);
	}

	@Test(expected = MuDEBSException.class)
	public void testAddSuperScopeAlreadyInSubScopes() throws MuDEBSException,
			URISyntaxException {
		URI d = new URI("mudebs:localhost:portnumber/d");
		Scope s = new Scope(d, "id");
		Assert.assertNotNull(s);
		Scope s2 = new Scope(d, "id2");
		Assert.assertNotNull(s2);
		Scope s3 = new Scope(d, "id3");
		Assert.assertNotNull(s3);
		s.addSubScope(s2);
		s2.addSubScope(s3);
		s.addSuperScope(s3);
	}
}
