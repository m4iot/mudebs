/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */
package mudebs.common.algorithms.routing;

import java.util.List;

import mudebs.common.algorithms.AbstractMessageContent;

/**
 * This class defines the content of a publication.
 * 
 * @author Denis Conan
 * 
 */
public class AdminPublication extends AbstractMessageContent {
	private String idAdv;
	private boolean privacyRequirement;
	private List<String> subsEnabledAC;
	private int sequenceNumber;
	private MultiScopingSpecification phiNotif;
	private String content;

	public AdminPublication(final String idAdv, final boolean privacyReq,
			final int seqNumber, final MultiScopingSpecification phiNotif,
			final String content, final List<String> subsEnabledAC) {
		this.idAdv = idAdv;
		this.privacyRequirement = privacyReq;
		this.sequenceNumber = seqNumber;
		this.content = content;
		this.phiNotif = phiNotif;
		this.subsEnabledAC = subsEnabledAC;
	}

	public String getIdAdv() {
		return idAdv;
	}

	public boolean hasGotPrivacyRequirement() {
		return privacyRequirement;
	}

	public int getSequenceNumber() {
		return sequenceNumber;
	}

	public MultiScopingSpecification getPhiNotif() {
		return phiNotif;
	}

	public String getContent() {
		return content;
	}

	public List<String> getSubsEnabledAC() {
		return subsEnabledAC;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idAdv == null) ? 0 : idAdv.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof AdminPublication)) {
			return false;
		}
		AdminPublication other = (AdminPublication) obj;
		if (idAdv == null) {
			if (other.idAdv != null) {
				return false;
			}
		} else if (!idAdv.equals(other.idAdv)) {
			return false;
		}
		return true;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AdminPublicationMsgContent [idAdv=" + idAdv
				+ ", privacyRequirement=" + privacyRequirement
				+ ", subsEnabledAC=" + subsEnabledAC + ", sequenceNumber="
				+ sequenceNumber + ", phi=" + phiNotif + ", content=" + content
				+ "]";
	}

	/**
	 * gets a short version of the message in a string.
	 * 
	 * @return a short description of the message.
	 */
	public String toStringBrief() {
		return "AdminPublicationMsgContent [idAdv="
				+ idAdv
				+ ", privacyRequirement="
				+ privacyRequirement
				+ ", subsEnabledAC="
				+ subsEnabledAC
				+ ", sequenceNumber="
				+ sequenceNumber
				+ ", phi="
				+ phiNotif
				+ ", content="
				+ ((content.length() < 10) ? content : content.substring(0, 10)
						+ "...") + "]";
	}
}
