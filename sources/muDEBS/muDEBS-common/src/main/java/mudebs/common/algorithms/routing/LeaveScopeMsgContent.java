/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Léon Lim
 */
package mudebs.common.algorithms.routing;

import java.net.URI;

import mudebs.common.algorithms.AbstractMessageContent;

/**
 * This class defines the content of a leave scope request.
 * 
 * @author Denis Conan
 * @author Léon Lim
 * 
 */
public class LeaveScopeMsgContent extends AbstractMessageContent {

	private URI requester;
	private int sequenceNumber;
	private Scope subscope;
	private Scope superscope;

	@SuppressWarnings("unused")
	private LeaveScopeMsgContent() {
	} // for use in OSGi Felix

	public LeaveScopeMsgContent(final URI requester, final int sequenceNumber,
			final Scope subscope, final Scope superscope) {
		super();
		this.requester = requester;
		this.sequenceNumber = sequenceNumber;
		this.subscope = subscope;
		this.superscope = superscope;
	}

	public URI getRequester() {
		return requester;
	}

	public int getSequenceNumber() {
		return sequenceNumber;
	}

	public Scope getSubScope() {
		return subscope;
	}

	public Scope getSuperScope() {
		return superscope;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "LeaveScopeMsgContent [requester=" + requester
				+ ", sequenceNumber=" + sequenceNumber + ", subscope="
				+ subscope + ", superscope=" + superscope + "]";
	}

	/**
	 * gets a short version of the message in a string.
	 * 
	 * @return a short description of the message.
	 */
	public String toStringBrief() {
		return toString();
	}
}
