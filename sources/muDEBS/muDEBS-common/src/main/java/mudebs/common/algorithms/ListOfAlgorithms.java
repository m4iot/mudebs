/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */
package mudebs.common.algorithms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import mudebs.common.algorithms.overlaymanagement.AlgorithmOverlayManagement;
import mudebs.common.algorithms.routing.AlgorithmRouting;
import mudebs.common.algorithms.scripting.broker.AlgorithmScriptingBroker;
import mudebs.common.algorithms.scripting.client.AlgorithmScriptingClient;

/**
 * This Enumeration type declares the algorithms (brokers and clients). It is
 * the specification part. Brokers and clients must be provide the
 * implementation.
 * 
 * @author Denis Conan
 * 
 */
public enum ListOfAlgorithms {
	/**
	 * message types exchanged for the routing algorithm.
	 */
	ALGORITHM_ROUTING(AlgorithmRouting.mapOfActions),
	/**
	 * message types exchanged for the management of the overlay.
	 */
	ALGORITHM_OVERLAY_MANAGEMENT(AlgorithmOverlayManagement.mapOfActions),
	/**
	 * message types exchanged for the scripting client.
	 */
	ALGORITHM_SCRIPTING_CLIENT(AlgorithmScriptingClient.mapOfActions),
	/**
	 * message types exchanged for the scripting broker.
	 */
	ALGORITHM_SCRIPTING_BROKER(AlgorithmScriptingBroker.mapOfActions);

	/**
	 * collection of the algorithms of the broker. This collection is kept
	 * private to avoid modifications.
	 */
	private final static List<Map<Integer, ? extends AlgorithmActionInterfaceToBeDeprecated>> privateMapOfAlgorithms;
	/**
	 * unmodifiable collection corresponding to <tt>privateMapOfAlgorithms</tt>.
	 */
	public final static List<Map<Integer, ? extends AlgorithmActionInterfaceToBeDeprecated>> mapOfAlgorithms;
	/**
	 * collection of the actions of this algorithm object of the broker.
	 */
	private final Map<Integer, ? extends AlgorithmActionInterfaceToBeDeprecated> mapOfActions;

	/**
	 * static block to build collections.
	 */
	static {
		privateMapOfAlgorithms = new ArrayList<Map<Integer, ? extends AlgorithmActionInterfaceToBeDeprecated>>();
		for (ListOfAlgorithms am : ListOfAlgorithms.values()) {
			privateMapOfAlgorithms.add(am.mapOfActions);
		}
		mapOfAlgorithms = Collections.unmodifiableList(privateMapOfAlgorithms);
	}

	/**
	 * constructor of this algorithm object.
	 * 
	 * @param map
	 *            collection of actions of this algorithm.
	 */
	private ListOfAlgorithms(
			Map<Integer, ? extends AlgorithmActionInterfaceToBeDeprecated> map) {
		mapOfActions = map;
	}
}
