/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */
package mudebs.common.algorithms.routing;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import mudebs.common.Constants;
import mudebs.common.Log;
import mudebs.common.algorithms.AbstractMessageContent;
import mudebs.common.algorithms.AlgorithmActionInterfaceToBeDeprecated;

import org.apache.log4j.Level;

/**
 * This Enumeration type declares the algorithm of the routing part of the
 * broker and the client.
 * 
 * @author Denis Conan
 * 
 */
public enum AlgorithmRouting implements AlgorithmActionInterfaceToBeDeprecated {
	/**
	 * advertisement message type for the routing algorithm.
	 */
	ADVERTISEMENT(AdvertisementMsgContent.class),
	/**
	 * publication message type for the routing algorithm.
	 */
	PUBLICATION(PublicationMsgContent.class),
	/**
	 * collective publication message type for the routing algorithm.
	 */
	COLLECTIVE_PUBLICATION(CollectivePublicationMsgContent.class),
	/**
	 * admin publication message type for the routing algorithm.
	 */
	ADMIN_PUBLICATION(AdminPublicationMsgContent.class),
	/**
	 * collective admin publication message type for the routing algorithm.
	 */
	COLLECTIVE_ADMIN_PUBLICATION(CollectiveAdminPublicationMsgContent.class),
	/**
	 * request message type for the routing algorithm.
	 */
	REQUEST(RequestMsgContent.class),
	/**
	 * admin request message type for the routing algorithm.
	 */
	ADMIN_REQUEST(AdminRequestMsgContent.class),
	/**
	 * request message type for the routing algorithm.
	 */
	REPLY(ReplyMsgContent.class),
	/**
	 * admin reply message type for the routing algorithm.
	 */
	ADMIN_REPLY(AdminReplyMsgContent.class),
	/**
	 * client subscription message type for the routing algorithm.
	 */
	SUBSCRIPTION(SubscriptionMsgContent.class),
	/**
	 * client collective subscription message type for the routing algorithm.
	 */
	COLLECTIVE_SUBSCRIPTION(CollectiveSubscriptionMsgContent.class),
	/**
	 * broker subscription message type for the routing algorithm.
	 */
	ADMIN_SUBSCRIPTION(AdminSubscriptionMsgContent.class),
	/**
	 * broker collective subscription message type for the routing algorithm.
	 */
	ADMIN_COLLECTIVE_SUBSCRIPTION(CollectiveAdminSubscriptionMsgContent.class),
	/**
	 * unadvertisement message type for the routing algorithm.
	 */
	UNADVERTISEMENT(UnadvertisementMsgContent.class),
	/**
	 * unsubscription message type for the routing algorithm.
	 */
	UNSUBSCRIPTION(UnsubscriptionMsgContent.class),
	/**
	 * broker unsubscription message type for the routing algorithm.
	 */
	ADMIN_UNSUBSCRIPTION(AdminUnsubscriptionMsgContent.class),
	/**
	 * broker advertisement message type for the routing algorithm.
	 */
	ADMIN_ADVERTISEMENT(AdminAdvertisementMsgContent.class),
	/**
	 * broker unadvertisement message type for the routing algorithm.
	 */
	ADMIN_UNADVERTISEMENT(AdminUnadvertisementMsgContent.class),
	/**
	 * acknowledgement message type for the routing algorithm.
	 */
	ACKNOWLEDGEMENT(AcknowledgementMsgContent.class),
	/**
	 * broker acknowledgement message type for the routing algorithm.
	 */
	ADMIN_ACKNOWLEDGEMENT(AdminAcknowledgementMsgContent.class),
	/**
	 * admin join scope message type for the join scope algorithm.
	 */
	JOIN_SCOPE(JoinScopeMsgContent.class),
	/**
	 * join scope message type for the join scope algorithm.
	 */
	ADMIN_JOIN_SCOPE(AdminJoinScopeMsgContent.class),
	/**
	 * leave scope message type for the leave scope algorithm.
	 */
	LEAVE_SCOPE(LeaveScopeMsgContent.class),
	/**
	 * admin leave scope message type for the leave scope algorithm.
	 */
	ADMIN_LEAVE_SCOPE(AdminLeaveScopeMsgContent.class),
	/**
	 * get scope scope message type for asking for the set of known scopes.
	 */
	GET_SCOPES(GetScopesMsgContent.class);

	/**
	 * index of the first message type of this algorithm.
	 */
	public final int algorithmOffset = Constants.ROUTING_START_INDEX;
	/**
	 * collection of the actions of this algorithm. The Key is the index of the
	 * corresponding message type. This collection is kept private to avoid
	 * modifications.
	 */
	private final static Map<Integer, AlgorithmRouting> privateMapOfActions;
	/**
	 * unmodifiable collection corresponding to <tt>privateMapOfActions</tt>.
	 */
	public final static Map<Integer, AlgorithmRouting> mapOfActions;
	/**
	 * index of the action of this message type.
	 */
	private final int actionIndex;
	/**
	 * class of the content of the message received.
	 */
	private final Class<? extends AbstractMessageContent> msgContentClass;

	/**
	 * static block to build collections of actions.
	 */
	static {
		privateMapOfActions = new HashMap<Integer, AlgorithmRouting>();
		for (AlgorithmRouting aa : AlgorithmRouting.values()) {
			privateMapOfActions.put(aa.actionIndex, aa);
		}
		mapOfActions = Collections.unmodifiableMap(privateMapOfActions);
	}

	/**
	 * constructor of message type object.
	 * 
	 * @param msgClass
	 *            the class of the content of the message received.
	 */
	private AlgorithmRouting(
			final Class<? extends AbstractMessageContent> msgClass) {
		this.actionIndex = Constants.MESSAGE_TYPE_START_INDEX + algorithmOffset
				+ ordinal();
		this.msgContentClass = msgClass;
	}

	/**
	 * obtains the index of this message type.
	 */
	public int getActionIndex() {
		return actionIndex;
	}

	/**
	 * obtains the class of the message received in this action.
	 */
	public Class<? extends AbstractMessageContent> getMsgContentClass() {
		return msgContentClass;
	}

	@Override
	public String toString() {
		return String.valueOf(actionIndex);
	}

	@Override
	public void execute(AbstractMessageContent msg) {
		if (Log.ON && Log.DISPATCH.isEnabledFor(Level.FATAL)) {
			Log.DISPATCH.fatal("this is the specification,"
					+ " not the implementation. Should not be here.");
		}
		throw new RuntimeException("this is the specification,"
				+ " not the implementation. Should not be here.");
	}
}
