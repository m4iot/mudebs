/**
This file is part of the muDEBS platform.

Copyright (C) 2013-2017 Télécom SudParis

The muDEBS platform is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Léon Lim
Contributor(s): Denis Conan
 */

package mudebs.common.algorithms.routing;

import java.net.URI;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import mudebs.common.MuDEBSException;

/**
 * this class defines a set of sets of scope paths, with at most one set per
 * dimension.
 * 
 * @author Léon Lim
 * @author Denis Conan
 *
 */
public class MultiScopingSpecification {

	/**
	 * the set of sets of scope paths.
	 */
	private HashMap<URI, SetOfScopePathsWithStatus> phi;

	public MultiScopingSpecification() {
		phi = new HashMap<URI, SetOfScopePathsWithStatus>();
	}

	/**
	 * creates a multiscoping specification from a scope.
	 * 
	 * @param scope
	 *            the scope.
	 * @param lambda
	 *            the status.
	 * 
	 * @return the multiscoping specification
	 */
	public static MultiScopingSpecification create(final Scope scope,
			final ScopePathStatus lambda) {
		if (scope == null) {
			throw new IllegalArgumentException("the scope is  null");
		}
		MultiScopingSpecification s = new MultiScopingSpecification()
				.addSetOfScopePathsWithStatus(
						SetOfScopePathsWithStatus.create(scope, lambda));
		return s;
	}

	/**
	 * creates a multiscoping specification from a list of scopes, one scope per
	 * dimension. All the scope path status are <tt>ScopePathStatus.UP</tt>.
	 * 
	 * @param scopes
	 *            the list of scopes.
	 * @return the multiscoping specification
	 */
	public static MultiScopingSpecification create(final Set<Scope> scopes) {
		if (scopes == null) {
			throw new IllegalArgumentException("the set of scopes is  null");
		}
		MultiScopingSpecification s = new MultiScopingSpecification();
		List<String> dims = new Vector<String>();
		for (Scope scope : scopes) {
			if (dims.contains(scope.dimension)) {
				throw new IllegalArgumentException(
						"several scopes with dimension = " + scope.dimension);
			}
			s.addSetOfScopePathsWithStatus(SetOfScopePathsWithStatus
					.create(scope, ScopePathStatus.UP));
		}
		return s;
	}

	/**
	 * add a set of scope paths associated with a dimension to the set of sets
	 * of scope paths.
	 * 
	 * @param setOfScopePaths
	 *            the set of scope paths to add.
	 * @return the new multiscoping specification.
	 */
	public MultiScopingSpecification addSetOfScopePathsWithStatus(
			final SetOfScopePathsWithStatus setOfScopePaths) {
		if (setOfScopePaths == null) {
			throw new IllegalArgumentException("set of scope paths is null");
		}
		if (setOfScopePaths.getTheSet().isEmpty()) {
			throw new IllegalArgumentException("set of scope paths is empty");
		}
		phi.put(setOfScopePaths.getTheSet().get(0).dimension, setOfScopePaths);
		return this;
	}

	public HashMap<URI, SetOfScopePathsWithStatus> getTheSets() {
		return phi;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((phi == null) ? 0 : phi.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MultiScopingSpecification other = (MultiScopingSpecification) obj;
		if (phi == null) {
			if (other.phi != null)
				return false;
		} else if (!phi.equals(other.phi))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		if (this.getTheSets().isEmpty()) {
			new IllegalArgumentException("set of scope paths is empty");
		}
		if (this.getTheSets().isEmpty()) {
			return "[]";
		}
		Iterator<Map.Entry<URI, SetOfScopePathsWithStatus>> iterator = this
				.getTheSets().entrySet().iterator();

		String result = "[";
		while (iterator.hasNext()) {
			Map.Entry<URI, SetOfScopePathsWithStatus> entry = iterator.next();
			if (entry.getValue() == null) {
				new MuDEBSException(
						"set of scope paths associated with dimension "
								+ entry.getKey() + "is null");
			}
			if (entry.getValue().getTheSet().isEmpty()) {
				new MuDEBSException(
						"set of scope paths associated with dimension "
								+ entry.getKey() + "is empty");
			}
			if (entry.getValue().getTheSet().size() == 1) {
				result = result + "{"
						+ entry.getValue().getTheSet().get(0).toStringPath()
						+ "}";
			} else if (entry.getValue().getTheSet().size() > 1) {
				result = result + "{";
				for (int i = 0; i < entry.getValue().getTheSet().size()
						- 1; i++) {
					result = result
							+ entry.getValue().getTheSet().get(i).toStringPath()
							+ ", ";
				}
				result = result + entry.getValue().getTheSet()
						.get(entry.getValue().getTheSet().size() - 1)
						.toStringPath() + "}";
			}
			if (!iterator.hasNext()) {
				result = result + "]";
			}
		}
		return result;
	}
}
