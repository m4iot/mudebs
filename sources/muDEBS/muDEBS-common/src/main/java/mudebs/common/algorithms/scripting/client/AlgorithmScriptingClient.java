/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */
package mudebs.common.algorithms.scripting.client;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import mudebs.common.Constants;
import mudebs.common.Log;
import mudebs.common.algorithms.AbstractMessageContent;
import mudebs.common.algorithms.AlgorithmActionInterfaceToBeDeprecated;
import mudebs.common.algorithms.routing.AdvertisementMsgContent;
import mudebs.common.algorithms.routing.CollectivePublicationMsgContent;
import mudebs.common.algorithms.routing.CollectiveSubscriptionMsgContent;
import mudebs.common.algorithms.routing.GetScopesMsgContent;
import mudebs.common.algorithms.routing.PublicationMsgContent;
import mudebs.common.algorithms.routing.RequestMsgContent;
import mudebs.common.algorithms.routing.SubscriptionMsgContent;
import mudebs.common.algorithms.routing.UnadvertisementMsgContent;
import mudebs.common.algorithms.routing.UnsubscriptionMsgContent;

import org.apache.log4j.Level;

/**
 * This Enumeration type declares the algorithm of the scripting part of the
 * client.
 * 
 * @author Denis Conan
 * 
 */
public enum AlgorithmScriptingClient implements AlgorithmActionInterfaceToBeDeprecated {
	/**
	 * advertisement message type for the scripting algorithm.
	 */
	ADVERTISEMENT(AdvertisementMsgContent.class),
	/**
	 * publication message type for the scripting algorithm.
	 */
	PUBLICATION(PublicationMsgContent.class),
	/**
	 * collective publication message type for the scripting algorithm.
	 */
	COLLECTIVE_PUBLICATION(CollectivePublicationMsgContent.class),
	/**
	 * request message type for the scripting algorithm.
	 */
	REQUEST(RequestMsgContent.class),
	/**
	 * subscription message type for the scripting algorithm.
	 */
	SUBSCRIPTION(SubscriptionMsgContent.class),
	/**
	 * collective subscription message type for the scripting algorithm.
	 */
	COLLECTIVE_SUBSCRIPTION(CollectiveSubscriptionMsgContent.class),
	/**
	 * termination message type for the scripting algorithm.
	 */
	TERMINATION(TerminationMsgContent.class),
	/**
	 * unadvertisement message type for the scripting algorithm.
	 */
	UNADVERTISEMENT(UnadvertisementMsgContent.class),
	/**
	 * disconnect/reconnect message type for the scripting algorithm.
	 */
	DISCONNECTRECONNECT(DisconnectReconnectMsgContent.class),
	/**
	 * unsubscription message type for the scripting algorithm.
	 */
	UNSUBSCRIPTION(UnsubscriptionMsgContent.class),
	/**
	 * get scopes message type for the scripting algorithm.
	 */
	GET_SCOPES(GetScopesMsgContent.class);

	/**
	 * index of the first message type of this algorithm.
	 */
	public final int algorithmOffset = Constants.SCRIPTING_CLIENT_START_INDEX;
	/**
	 * collection of the actions of this algorithm. The Key is the index of the
	 * corresponding message type. This collection is kept private to avoid
	 * modifications.
	 */
	private final static Map<Integer, AlgorithmScriptingClient> privateMapOfActions;
	/**
	 * unmodifiable collection corresponding to <tt>privateMapOfActions</tt>.
	 */
	public final static Map<Integer, AlgorithmScriptingClient> mapOfActions;
	/**
	 * index of the action of this message type.
	 */
	private final int actionIndex;
	/**
	 * class of the content of the message received.
	 */
	private final Class<? extends AbstractMessageContent> msgContentClass;

	/**
	 * static block to build collections of actions.
	 */
	static {
		privateMapOfActions = new HashMap<Integer, AlgorithmScriptingClient>();
		for (AlgorithmScriptingClient aa : AlgorithmScriptingClient.values()) {
			privateMapOfActions.put(aa.actionIndex, aa);
		}
		mapOfActions = Collections.unmodifiableMap(privateMapOfActions);
	}

	/**
	 * constructor of message type object.
	 * 
	 * @param msgClass
	 *            the class of the content of the message received.
	 */
	private AlgorithmScriptingClient(
			final Class<? extends AbstractMessageContent> msgClass) {
		this.actionIndex = Constants.MESSAGE_TYPE_START_INDEX + algorithmOffset
				+ ordinal();
		this.msgContentClass = msgClass;
	}

	/**
	 * obtains the index of this message type.
	 */
	public int getActionIndex() {
		return actionIndex;
	}

	/**
	 * obtains the class of the message received in this action.
	 */
	public Class<? extends AbstractMessageContent> getMsgContentClass() {
		return msgContentClass;
	}

	@Override
	public String toString() {
		return String.valueOf(actionIndex);
	}

	@Override
	public void execute(AbstractMessageContent msg) {
		if (Log.ON && Log.DISPATCH.isEnabledFor(Level.FATAL)) {
			Log.DISPATCH.fatal("this is the specification,"
					+ " not the implementation. Should not be here.");
		}
		throw new RuntimeException("this is the specification,"
				+ " not the implementation. Should not be here.");
	}
}
