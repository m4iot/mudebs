/**
This file is part of the muDEBS platform.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS platform is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Léon Lim
Contributor(s): Denis Conan
 */

package mudebs.common.filters;


/**
 * This class represents the status of a map down-up filter, i.e., down or up.
 * 
 * @author Léon Lim
 * 
 */
public enum Status {
	/**
	 * states that the visibility filter is a map up filter.
	 */
	UP,
	/**
	 * states that the visibility filter is a map down filter.
	 */
	DOWN;
}
