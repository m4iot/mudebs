/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2017 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Léon Lim
 */
package mudebs.common.algorithms.routing;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import mudebs.common.MuDEBSException;

/**
 * This class defines a scope. A scope is a node that is part of two directed
 * acyclic graphs (DAG): sub-scope relationship and super-scope relationship.
 * 
 * 
 * @author Denis Conan
 * @author Léon Lim
 * 
 */
public class Scope implements Comparable<Scope> {
	/**
	 * the identifier of the scope.
	 */
	protected final String id;
	/**
	 * the dimension of the scope. This is the identifier provided by the client
	 * application or the broker ---i.e., administrator.
	 */
	protected final URI dimension;
	/**
	 * dummy dimension.
	 */
	private static URI dummy;
	/**
	 * the set of sub-scopes.
	 */
	private List<Scope> subScopes;
	/**
	 * the set of super-scopes.
	 */
	private List<Scope> superScopes;

	static {
		try {
			dummy = new URI("mudebs://localhost:dummyDim");
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
	}

	/**
	 * constructor of the scope.
	 * 
	 * @param d
	 *            the URI of the dimension of the scope.
	 * @param identifier
	 *            the identifier of the scope.
	 */
	public Scope(final URI d, final String identifier) {
		if (d == null) {
			throw new IllegalArgumentException("uri of the dimension is null");
		}
		if (identifier == null || identifier.equals("")) {
			throw new IllegalArgumentException("identifier is null or empty");
		}
		dimension = d;
		id = identifier;
		subScopes = new ArrayList<Scope>();
		superScopes = new ArrayList<Scope>();
	}

	/**
	 * gets the identifier of the scope.
	 * 
	 * @return the identifier.
	 */
	public String identifier() {
		return id;
	}

	/**
	 * gets the dimension of the scope.
	 * 
	 * @return the URI of the dimension.
	 */
	public URI dimension() {
		return dimension;
	}

	/**
	 * gets the URI of the <tt>DUMMY</tt> dimension.
	 * 
	 * @return the URI of the dummy dimension.
	 */
	static public URI getDummyDimension() {
		return dummy;
	}

	/**
	 * adds a sub-scope to this scope. The scope <tt>toAdd</tt> must not be in
	 * the DAG of super-scopes. This scope is added as a super-scope to the
	 * sub-scope.
	 * 
	 * @param toAdd
	 *            the sub-scope to add.
	 * @throws MuDEBSException
	 *             the exception thrown when already a sub-scope or super-scope.
	 */
	public void addSubScope(final Scope toAdd) throws MuDEBSException {
		if (toAdd == null) {
			throw new IllegalArgumentException("scope to add is null");
		}
		if (subScopes.contains(toAdd) && !(toAdd instanceof ScopeBottom)) {
			throw new MuDEBSException("scope '" + toAdd
					+ "' is already a (one-vertex) sub-scope of this scope");
		}
		if (searchInSuperScopes(toAdd) && !(toAdd instanceof ScopeBottom)) {
			throw new MuDEBSException("scope '" + toAdd
					+ "' is already in the super-scope DAG of this scope");
		}
		if (!subScopes.contains(toAdd)) {
			subScopes.add(toAdd);
		}
		if (!toAdd.superScopes().contains(this)) {
			toAdd.addAsSuperScope(this);
		}
	}

	/**
	 * adds a list of sub-scopes to this scope using <tt>addSubScope</tt>.
	 * 
	 * @param list
	 *            the list of scopes to add.
	 * @throws MuDEBSException
	 *             the exception thrown when one of the scopes is already a
	 *             sub-scope or super-scope.
	 */
	public void addSubScopes(final List<Scope> list) throws MuDEBSException {
		for (Scope toAdd : list) {
			addSubScope(toAdd);
		}
	}

	/**
	 * searches for the scope in the DAG of super-scopes.
	 * 
	 * @param toSearch
	 *            the scope to search for.
	 * @return the boolean stating whether the scope is found.
	 */
	private boolean searchInSuperScopes(final Scope toSearch) {
		if (superScopes.contains(toSearch)) {
			return true;
		}
		for (Scope scope : superScopes) {
			if (scope.searchInSuperScopes(toSearch)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * adds a super-scope to this scope. This method is called when adding this
	 * scope as a sub-scope of the scope provided in argument. This is why there
	 * is no condition tested before the <tt>add</tt>.
	 * 
	 * The visibility of this method is private to avoid infinite loop.
	 * 
	 * @param toAdd
	 *            the super-scope to add.
	 */
	private void addAsSuperScope(final Scope toAdd) {
		superScopes.add(toAdd);
	}

	/**
	 * removes a sub-scope to this scope.
	 * 
	 * @param toRemove
	 *            the sub-scope to remove.
	 */
	public void removeSubScope(final Scope toRemove) {
		subScopes.remove(toRemove);
		toRemove.removeAsSuperScope(this);
	}

	/**
	 * removes a super-scope of this scope. This method is called when removing
	 * this scope as a sub-scope of the scope provided in argument.
	 * 
	 * The visibility of this method is private to avoid infinite loop.
	 * 
	 * @param toRemove
	 *            the super-scope to remove.
	 */
	private void removeAsSuperScope(final Scope toRemove) {
		superScopes.remove(toRemove);
	}

	/**
	 * gets the set of sub-scopes.
	 * 
	 * @return the list of subscopes.
	 */
	public List<Scope> subScopes() {
		return subScopes;
	}

	/**
	 * adds a super-scope to this scope. The scope <tt>toAdd</tt> must not be in
	 * the DAG of sub-scopes. This scope is added as a sub-scope to the
	 * super-scope.
	 * 
	 * @param toAdd
	 *            the super-scope to add.
	 * @throws MuDEBSException
	 *             the exception thrown when one of the scopes is already a
	 *             sub-scope or super-scope.
	 */
	public void addSuperScope(final Scope toAdd) throws MuDEBSException {
		if (toAdd == null) {
			throw new IllegalArgumentException("scope to add is null");
		}
		if (superScopes.contains(toAdd) && !(this instanceof ScopeBottom)) {
			throw new MuDEBSException("scope '" + toAdd
					+ "' is already a (one-vertex) super-scope of this scope");
		}
		if (searchInSubScopes(toAdd) != null && !(toAdd instanceof ScopeTop)) {
			throw new MuDEBSException("scope '" + toAdd
					+ "' is already in the sub-scope DAG of this scope");
		}
		if (!(this instanceof ScopeBottom) && !(toAdd instanceof ScopeTop)) {
			if (!superScopes.contains(toAdd)) {
				superScopes.add(toAdd);
			}
			toAdd.addAsSubScope(this);
		}
	}

	/**
	 * adds the scope-top <tt>toAdd</tt> the scope and all its sub-scopes
	 * recursively.
	 * 
	 * @param toAdd
	 *            the scope-top to add.
	 * @throws MuDEBSException
	 *             the exception thrown in case of problem.
	 */
	public void addTopToAll(final ScopeTop toAdd) throws MuDEBSException {
		addTop(toAdd, this);
	}

	/**
	 * adds the scope-top <tt>top</tt> to the scope <tt>scope</tt> and all its
	 * sub-scopes recursively.
	 * 
	 * @param top
	 *            the scope-top to add.
	 * @param scope
	 *            the scope to which the scope-top is added.
	 * @throws MuDEBSException
	 *             the exception thrown in case of problem.
	 */
	private void addTop(final ScopeTop top, final Scope scope)
			throws MuDEBSException {
		boolean existTop = false;
		if (scope.superScopes.isEmpty()) {
			if (!(scope instanceof ScopeBottom)) {
				top.addSubScope(scope);
			}
			if (!scope.superScopes().contains(top)) {
				if (!(scope instanceof ScopeBottom)) {
					scope.addSuperScope(top);
				}
			}
		} else {
			for (Scope superScope : scope.superScopes) {
				if (superScope instanceof ScopeTop) {
					existTop = true;
				}
			}
			if (!existTop) {
				if (!(scope instanceof ScopeBottom)) {
					top.addSubScope(scope);
				}
				if (!scope.superScopes().contains(top)) {
					if (!(scope instanceof ScopeBottom)) {
						scope.addSuperScope(top);
					}
				}
			}
		}
		for (Scope subScope : scope.subScopes) {
			addTop(top, subScope);
		}
	}

	/**
	 * adds the scope-bottom <tt>toAdd</tt> to each scope with no children.
	 * 
	 * @param toAdd
	 *            the scope-bottom to add.
	 * @throws MuDEBSException
	 *             the exception thrown in case of problem.
	 */
	public void addBottomToAll(final ScopeBottom toAdd) throws MuDEBSException {
		addBottom(toAdd, this);
	}

	/**
	 * adds the scope-bottom <tt>bottom</tt> to the scope <tt>scope</tt> and all
	 * its sub-scopes recursively.
	 * 
	 * @param bottom
	 *            the scope-bottom to add.
	 * @param scope
	 *            the scope to which the scope-bottom is added.
	 * @throws MuDEBSException
	 *             the exception thrown in case of problem.
	 */
	private void addBottom(final ScopeBottom bot, final Scope scope)
			throws MuDEBSException {
		if (!(scope instanceof ScopeBottom)) {
			if (!(scope instanceof ScopeTop)) {
				scope.addSubScope(bot);
				bot.addSuperScope(scope);
			}
			for (Scope subScope : scope.subScopes) {
				addBottom(bot, subScope);
			}
		}
	}

	/**
	 * searches for the scope in the DAG of sub-scopes.
	 * 
	 * @param toSearch
	 *            the scope to search for.
	 * @return the scope found.
	 */
	public Scope searchInSubScopes(final Scope toSearch) {
		Scope found = null;
		if (subScopes.contains(toSearch)) {
			found = subScopes.get(subScopes.indexOf(toSearch));
			return found;
		}
		for (Scope scope : subScopes) {
			return scope.searchInSubScopes(toSearch);
		}
		return found;
	}

	/**
	 * tests whether the scope is equal to <tt>subScope</tt> or a super-scope of
	 * <tt>subScope</tt> transitively.
	 * 
	 * @param subScope
	 *            the sub-scope to be tested.
	 * @return the boolean stating whether the scope is equal to sub-scope or is
	 *         a super-scope of it transitively.
	 * 
	 */
	public boolean isEqualOrSuperScopeTransitively(final Scope subScope) {
		if (subScope == null) {
			return false;
		}
		if (this.equals(subScope)) {
			return true;
		}
		return searchInSubScopes(subScope) != null;
	}

	/**
	 * adds a sub-scope of this scope. This method is called when adding this
	 * scope as a super-scope of the scope provided in argument.
	 * 
	 * @param toAdd
	 *            the sub-scope to add.
	 */
	public void addAsSubScope(final Scope toAdd) {
		subScopes.add(toAdd);
	}

	/**
	 * removes the super-scope from the set of super-scopes. This scope is
	 * removed as a sub-scope to the super-scope.
	 * 
	 * @param toRemove the super-scope to remove.
	 */
	public void removeSuperScope(final Scope toRemove) {
		superScopes.remove(toRemove);
		toRemove.removeSubScopeFromSuperScope(this);
	}

	/**
	 * removes the super-scope from the set of super-scopes. This scope is
	 * removed as a sub-scope to the super-scope.
	 * 
	 * @param toRemove the super-scope to remove.
	 */
	void removeSubScopeFromSuperScope(final Scope toRemove) {
		subScopes.remove(toRemove);
	}

	/**
	 * gets the set of super-scopes.
	 * 
	 * @return the list of supers-scopes.
	 */
	public List<Scope> superScopes() {
		return superScopes;
	}

	@Override
	public int compareTo(Scope other) {
		if (other == null) {
			return 1;
		}
		return this.id.compareTo(other.id);
	}

	/**
	 * gets all (transitively) the sub-scopes in a list.
	 * 
	 * @return the list of scopes.
	 */
	public List<Scope> getAllSubScopes() {
		List<Scope> scopes = new ArrayList<Scope>();
		for (Scope sub : subScopes()) {
			if (!(sub instanceof ScopeBottom) && !(sub instanceof ScopeTop)) {
				scopes.add(sub);
			}
			scopes.addAll(sub.getAllSubScopes());
		}
		return scopes;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((dimension == null) ? 0 : dimension.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Scope)) {
			return false;
		}
		Scope other = (Scope) obj;
		if (dimension == null) {
			if (other.dimension != null) {
				return false;
			}
		} else if (!dimension.equals(other.dimension)) {
			return false;
		}
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Scope [id=" + id + "]";
	}

	/**
	 * returns the list of the sub-scopes in a string.
	 * 
	 * @return the string representation of the sub-scopes.
	 */
	public String toStringSubScopes() {
		return toStringSubScopes(null).toString();
	}

	/**
	 * returns a list of strings of the sub-scopes, one path (string with "/")
	 * per leaf node accessible from this scope. This method is called by the
	 * method toStringSubScopes.
	 * 
	 * The visibility of this method is private to avoid infinite loop.
	 * 
	 * This method is used in classes @see
	 * mudebs.common.algorithms.routing.TestScope for the purpose of testing.
	 * 
	 * @return the list of strings of the sub-scopes.
	 */
	private List<String> toStringSubScopes(final String path) {
		List<String> result = new ArrayList<String>();
		String newPath = (path == null) ? id : path + "/" + id;
		if (subScopes.isEmpty()) {
			result.add(newPath);
		} else {
			for (Scope subScope : subScopes) {
				result.addAll(subScope.toStringSubScopes(newPath));
			}
		}
		return result;
	}

	/**
	 * returns the list of the super-scopes in a string.
	 * 
	 * @return the string representation of the super-scopes.
	 */
	public String toStringSuperScopes() {
		return toStringSuperScopes(null).toString();
	}

	/**
	 * returns a list of strings of the super-scopes, one path (string "/") per
	 * root node accessible from this scope. This method is called by the method
	 * toStringSuperScopes.
	 * 
	 * The visibility of this method is private to avoid infinite loop.
	 */
	private List<String> toStringSuperScopes(final String path) {
		List<String> result = new ArrayList<String>();
		String newPath = (path == null) ? id : id + "/" + path;
		if (superScopes.isEmpty()) {
			result.add("/" + newPath);
		} else {
			for (Scope superScope : superScopes) {
				result.addAll(superScope.toStringSuperScopes(newPath));
			}
		}
		return result;
	}
}
