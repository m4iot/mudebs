/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */
package mudebs.common.filters;

import java.util.List;

import org.w3c.dom.Document;

/**
 * This class defines a muDEBS composite filter that is a conjunction of
 * filters.
 * 
 * @author Denis Conan
 * 
 */
public class ConjunctionOfFilters extends CompositeFilter {

	/**
	 * builds a filter that is a conjunction of filters. Since there is no
	 * method to add/remove a filter to/from the list, a conjunction of filters
	 * cannot include itself.
	 * 
	 * @param fs
	 *            the list of filters
	 */
	public ConjunctionOfFilters(List<Filter> fs) {
		filters = fs;
	}

	/**
	 * evaluates whether the publication matches the conjunction of filters. The
	 * filters are applied in the order of the list. It returns <tt>true</tt> if
	 * and only if all the filters return <tt>true</tt>. It returns
	 * <tt>false</tt> if one of the filters returns <tt>false</tt>.
	 * 
	 * @param publication
	 *            the publication to evaluate.
	 * @return the boolean stating the matching.
	 */
	@Override
	public boolean evaluate(Document publication) {
		for (Filter f : filters) {
			if (!f.evaluate(publication)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ConjunctionOfFilters [filters=" + filters + "]";
	}

}
