/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */
package mudebs.common.algorithms.scripting.broker;

import mudebs.common.algorithms.AbstractMessageContent;

/**
 * This class defines the content of an information request message.
 * 
 * @author Denis Conan
 *
 */
public class InformationRequestMsgContent extends AbstractMessageContent {
	private String whichInfo;

	@SuppressWarnings("unused")
	private InformationRequestMsgContent() {
	} // for use in OSGi Felix

	public InformationRequestMsgContent(final String r) {
		whichInfo = r;
	}

	public String getWhichInfo() {
		return whichInfo;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "InformationRequestMsgContent [whichinfo=" + whichInfo + "]";
	}

	/**
	 * gets a short version of the message in a string.
	 * 
	 * @return a short description of the message.
	 */
	public String toStringBrief() {
		return this.toString();
	}
}
