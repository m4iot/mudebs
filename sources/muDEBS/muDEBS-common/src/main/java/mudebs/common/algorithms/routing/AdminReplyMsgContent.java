/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */
package mudebs.common.algorithms.routing;

import java.net.URI;
import java.util.List;

import mudebs.common.algorithms.AbstractMessageContent;
import mudebs.common.algorithms.Reply;

/**
 * This class defines the content of an admin reply.
 * 
 * @author Denis Conan
 * 
 */
public class AdminReplyMsgContent extends AbstractMessageContent {
	private URI broker;
	private URI client;
	private int seqNumber;
	private List<Reply> results;

	@SuppressWarnings("unused")
	private AdminReplyMsgContent() {
	} // for use in OSGi Felix

	public AdminReplyMsgContent(final URI b, final URI s, final int seq,
			final List<Reply> r) {
		broker = b;
		client = s;
		seqNumber = seq;
		results = r;
	}

	public URI getBroker() {
		return broker;
	}

	public URI getClient() {
		return client;
	}

	public int getSeqNumber() {
		return seqNumber;
	}

	public List<Reply> getResults() {
		return results;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AdminAcknowledgementMsgContent [broker=" + broker + ", client="
				+ client + ", seqNumber=" + seqNumber + ", results=" + results
				+ "]";
	}

	/**
	 * gets a short version of the message in a string.
	 * 
	 * @return a short description of the message.
	 */
	public String toStringBrief() {
		return "AdminAcknowledgementMsgContent [broker="
				+ broker
				+ ", client="
				+ client
				+ ", seqNumber="
				+ seqNumber
				+ ", results="
				+ ((results.toString().length() < 10) ? results.toString()
						: results.toString().substring(0, 10) + "...") + "]";
	}
}
