/**
This file is part of the muDEBS platform.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS platform is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Léon Lim
 */

package mudebs.common.algorithms.routing;

/**
 * This class represents the status of a scope path. The status states whether
 * the scope path corresponds to a downward part or to a upward part of a
 * visibility path.
 * 
 * @author Léon Lim
 * 
 */
public enum ScopePathStatus {
	/**
	 * states that the scope path is in the upward part.
	 */
	UP,
	/**
	 * states that the scope path is in the downward part.
	 */
	DOWN;
}
