/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2017 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Léon Lim
Contributor(s): Denis Conan
 */
package mudebs.common.algorithms.routing;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import mudebs.common.MuDEBSException;

/**
 * This class defines a scope graph.
 * 
 * @author Léon Lim
 * @author Denis Conan
 * 
 */
public class ScopeGraph {
	/**
	 * the dimension of the graph. This is the identifier provided by the client
	 * application or the broker ---i.e., administrator.
	 */
	protected final URI dimension;

	/**
	 * the virtual scope &Tau;, which is the root of the graph.
	 */
	private ScopeTop top;

	/**
	 * the virtual scope &perp; (bottom), which represents the leaf of the
	 * graph.
	 */
	private ScopeBottom bot;

	/**
	 * constructor of the graph.
	 * 
	 * @param d
	 *            the URI of the dimension of the graph.
	 */
	public ScopeGraph(final URI d) {
		if (d == null) {
			throw new IllegalArgumentException("uri of the dimension is null");
		}
		dimension = d;
		bot = new ScopeBottom(d);
		top = new ScopeTop(d);
	}

	/**
	 * gets the virtual scope &Tau; (top) of the graph.
	 * 
	 * @return the scope &Tau; of the graph.
	 */
	public ScopeTop top() {
		return top;
	}

	/**
	 * gets the virtual scope &perp; (bottom) of the graph.
	 * 
	 * @return the scope &perp; of the graph..
	 */
	public ScopeBottom bot() {
		return bot;
	}

	/**
	 * gets the dimension of the graph.
	 * 
	 * @return the dimension.
	 */
	public URI dimension() {
		return dimension;
	}

	/**
	 * adds the edge <tt>(s,t)</tt> to the scope graph.
	 * 
	 * @param s
	 *            the starting scope of the edge to add.
	 * @param t
	 *            the ending scope of the edge to add.
	 * @throws MuDEBSException
	 *             the exception thrown when wrong dimensions or when the edge
	 *             is already in the graph.
	 */
	public void addEdge(final Scope s, final Scope t) throws MuDEBSException {
		if (!dimension.equals(s.dimension)) {
			throw new MuDEBSException(
					"scope '" + s + "' has dimension that is different from '"
							+ top.dimension);
		}
		if (!dimension.equals(t.dimension)) {
			throw new MuDEBSException("scope '" + t
					+ "' has dimension that is different from '" + dimension);
		}
		Scope sOfTheGraph = top.searchInSubScopes(s);
		Scope tOfTheGraph = top.searchInSubScopes(t);
		if (t instanceof ScopeTop) {
			if (sOfTheGraph == null) {
				s.addSuperScope(top);
				top.addSubScope(s);
			}
		} else {
			if (sOfTheGraph != null) {
				if (tOfTheGraph != null) {
					if (sOfTheGraph.superScopes().contains(tOfTheGraph)) {
						throw new MuDEBSException("edge" + " (" + s + "," + t
								+ ") " + "already exists in the scope graph");
					} else {
						tOfTheGraph.addSubScope(sOfTheGraph);
					}
				} else {
					t.addSubScope(sOfTheGraph);
					top.addSubScope(t);
				}
			} else {
				if (tOfTheGraph != null) {
					top.addSubScope(s);
					tOfTheGraph.addSubScope(s);
				} else {
					t.addSubScope(s);
					top.addSubScope(s);
					top.addSubScope(t);
				}
			}
		}
		top.addBottomToAll(bot);
	}

	/**
	 * removes the edge <tt>(s,t)</tt> from the scope graph.
	 * 
	 * TODO: what about removing nodes?
	 * 
	 * @param s
	 *            the starting scope of the edge to remove.
	 * @param t
	 *            the ending scope of the edge to remove.
	 * @throws MuDEBSException
	 *             the exception thrown when wrong dimensions.
	 */
	public void removeEdge(final Scope s, final Scope t)
			throws MuDEBSException {
		if (!dimension.equals(s.dimension)) {
			throw new MuDEBSException(
					"scope '" + s + "' has dimension that is different from"
							+ " '" + dimension);
		}
		if (!dimension.equals(t.dimension)) {
			throw new MuDEBSException(
					"scope '" + t + "' has dimension that is different from"
							+ " '" + dimension);
		}
		if (!dimension.equals(dimension)) {
			throw new MuDEBSException(
					"scope '" + this + "' has dimension that is different from"
							+ " '" + dimension);
		}
		if (top.searchInSubScopes(t) != null && top.searchInSubScopes(s) != null
				&& t.subScopes().contains(s) && s.superScopes().contains(t)) {
			t.removeSubScope(s);
			s.removeSuperScope(t);
		}
	}

	/**
	 * tests whether an edge exists in the scope graph.
	 * 
	 * @param s
	 *            the starting scope of the edge.
	 * @param t
	 *            the ending scope of the edge.
	 * @return <tt>true</tt> if the edge exists.
	 */
	public boolean existsEdge(final Scope s, final Scope t) {
		boolean result = false;
		if (top.searchInSubScopes(s) != null) {
			if (top.searchInSubScopes(t) != null) {
				if (s.superScopes().contains(t) && t.subScopes().contains(s)) {
					result = true;
				}
			}
		}
		return result;
	}

	/**
	 * tests whether a node exists in the scope graph.
	 * 
	 * @param toSearch
	 *            the scope to search for.
	 * @return <tt>true</tt> if the node exists.
	 */
	public boolean existsNode(final Scope toSearch) {
		return top.searchInSubScopes(toSearch) != null;
	}

	/**
	 * gets all the scopes of the scope graph in a list.
	 * 
	 * @return the list of scopes.
	 */
	public List<Scope> getScopes() {
		List<Scope> scopes = new ArrayList<Scope>();
		for (Scope sub : top.subScopes()) {
			if (!sub.equals(bot) && !sub.equals(top)) {
				scopes.add(sub);
			}
			scopes.addAll(sub.getAllSubScopes());
		}
		return scopes;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((dimension == null) ? 0 : dimension.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Scope)) {
			return false;
		}
		Scope other = (Scope) obj;
		if (dimension == null) {
			if (other.dimension != null) {
				return false;
			}
		} else if (!dimension.equals(other.dimension)) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ScopeGraph [dimension=" + dimension + ", toStringSubScopes()="
				+ toStringSubScopes() + "]";
	}

	/**
	 * returns the string version of the scope graph.
	 * 
	 * @return the string version of the scope graph.
	 */
	public String toStringSubScopes() {
		return top.toStringSubScopes();
	}
}
