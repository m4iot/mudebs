/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */
package mudebs.common.filters;

import org.w3c.dom.Document;

/**
 * This interface defines a muDEBS filter
 * 
 * @author Denis Conan
 * 
 */
public interface Filter {
	/**
	 * evaluates whether the publication matches the filter.
	 * 
	 * @param publication
	 *                the publication to evaluate.
	 * @return the boolean stating the matching.
	 */
	boolean evaluate(Document publication);
}
