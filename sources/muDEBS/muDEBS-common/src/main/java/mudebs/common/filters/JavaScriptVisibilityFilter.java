/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Léon Lim
 */
package mudebs.common.filters;



/**
 * This interface defines a muDEBS the JavaScript visibility filter.
 * 
 * @author Denis Conan
 * @author Léon Lim
 * 
 */
public interface JavaScriptVisibilityFilter extends Filter {
	/**
	 * the JavaScript filter that always returns <tt>true</tt>. It is for
	 * instance used when calling join scope to express that mapping up from
	 * bottom or to top is always true.
	 */
	public static String ALWAYS_TRUE = "function evaluate() {return true;}";
	/**
	 * the JavaScript filter that always returns <tt>false</tt>. It is for
	 * instance to express that mapping down to bottom or from top is always
	 * false.
	 */
	public static String ALWAYS_FALSE = "function evaluate() {return false;}";
	/**
	 * the ending scope.
	 */
}
