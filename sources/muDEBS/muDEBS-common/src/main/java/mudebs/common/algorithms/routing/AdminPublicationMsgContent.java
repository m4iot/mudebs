/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */
package mudebs.common.algorithms.routing;

import java.net.URI;
import java.util.List;

import mudebs.common.algorithms.ACK;
import mudebs.common.algorithms.AbstractMessageContent;

/**
 * This class defines the content of an admin publication exchanged between
 * brokers.
 * 
 * @author Denis Conan
 * 
 */
public class AdminPublicationMsgContent extends AbstractMessageContent {
	private List<URI> path;
	private ACK ack;
	private int seqNumberForAck;
	private String idAdv;
	private boolean privacyRequirement;
	private List<String> subsEnabledAC;
	private int sequenceNumber;
	private MultiScopingSpecification phiNotif;
	private String content;

	protected AdminPublicationMsgContent() {
	} // for use in OSGi Felix

	public AdminPublicationMsgContent(final List<URI> p, final ACK a,
			final int seqNumberAck, final String id, final boolean pr,
			final int seqNumber,
			final MultiScopingSpecification phiNotif, final String msg,
			final List<String> subsEnabledAC) {
		path = p;
		ack = a;
		seqNumberForAck = seqNumberAck;
		idAdv = id;
		privacyRequirement = pr;
		sequenceNumber = seqNumber;
		content = msg;
		this.phiNotif = phiNotif;
		this.subsEnabledAC = subsEnabledAC;
	}

	public List<URI> getPath() {
		return path;
	}

	public ACK getAck() {
		return ack;
	}

	public int getSeqNumberForAck() {
		return seqNumberForAck;
	}

	public String getIdAdv() {
		return idAdv;
	}

	public boolean getPrivacyRequirement() {
		return privacyRequirement;
	}

	public int getSequenceNumber() {
		return sequenceNumber;
	}

	public MultiScopingSpecification getPhiNotif() {
		return phiNotif;
	}

	public String getContent() {
		return content;
	}

	public List<String> getSubsEnabledAC() {
		return subsEnabledAC;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AdminPublicationMsgContent [path=" + path + ", ack=" + ack
				+ ", seqNumberForAck=" + seqNumberForAck + ", idAdv=" + idAdv
				+ ", privacyRequirement=" + privacyRequirement
				+ ", subsEnabledAC=" + subsEnabledAC + ", sequenceNumber="
				+ sequenceNumber + ", phi=" + phiNotif + ", content=" + content
				+ "]";
	}

	/**
	 * gets a short version of the message in a string.
	 * 
	 * @return a short description of the message.
	 */
	public String toStringBrief() {
		return "AdminPublicationMsgContent [path="
				+ path
				+ ", ack="
				+ ack
				+ ", seqNumberForAck="
				+ seqNumberForAck
				+ ", idAdv="
				+ idAdv
				+ ", privacyRequirement="
				+ privacyRequirement
				+ ", subsEnabledAC="
				+ subsEnabledAC
				+ ", sequenceNumber="
				+ sequenceNumber
				+ ", phi="
				+ phiNotif
				+ ", content="
				+ ((content.length() < 10) ? content : content.substring(0, 10)
						+ "...") + "]";
	}
}
