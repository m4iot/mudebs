/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Léon Lim
 */
package mudebs.common;

/**
 * This class contains the attributes that are constants of the broker.
 * 
 * @author Denis Conan
 * @author Léon Lim
 */
public final class Constants {
	/**
	 * number of connection retries.
	 */
	public final static int NB_CONNECTION_RETRIES = 10;
	/**
	 * waiting time between two connection retries.
	 */
	public final static int TIME_WAIT_CONNECTION_RETRIES = 2000;
	/**
	 * Index of the first message type.
	 */
	public final static int MESSAGE_TYPE_START_INDEX = 0;
	/**
	 * Index of the first message type of the routing algorithm.
	 */
	public final static int ROUTING_START_INDEX = MESSAGE_TYPE_START_INDEX;
	/**
	 * Index of the first message type of the overlay management algorithm.
	 */
	public final static int OVERLAY_MANAGEMENT_START_INDEX = 100;
	/**
	 * Index of the first message type of the scripting client algorithm.
	 */
	public final static int SCRIPTING_CLIENT_START_INDEX = 200;
	/**
	 * Index of the first message type of the scripting broker algorithm.
	 */
	public final static int SCRIPTING_BROKER_START_INDEX = 300;
	/**
	 * string option for providing the configuration for a logger.
	 */
	public final static String OPTION_LOGGER = "--log";
	/**
	 * string option for providing the URI of a client or a broker as a command
	 * line argument.
	 */
	public final static String OPTION_URI = "--uri";
	/**
	 * string option for providing the URI of a neighbouring broker as a command
	 * line argument.
	 */
	public final static String OPTION_NEIGHBOUR = "--neigh";
	/**
	 * string option for providing the URI of a broker as a command line
	 * argument.
	 */
	public final static String OPTION_BROKER = "--broker";
	/**
	 * string option for providing the name of the command (advertise, publish,
	 * subscribe, etc.) as a command line argument.
	 */
	public final static String OPTION_COMMAND = "--command";
	/**
	 * string option for indicating whether the command mode is blocking (local
	 * or global)..
	 */
	public final static String OPTION_ACK = "--ack";
	/**
	 * name of an (local) advertise command.
	 */
	public final static String COMMAND_ADVERTISE = "advertise";
	/**
	 * name of an global advertise command.
	 */
	public final static String COMMAND_ADVERTISE_GLOBAL = "advertise_global";
	/**
	 * name of a connect command.
	 */
	public final static String COMMAND_CONNECT = "connect";
	/**
	 * name of a disconnect/reconnect command.
	 */
	public final static String COMMAND_DISCONNECT_RECONNECT = "dis-/re-connect";
	/**
	 * name of a get scopes command.
	 */
	public final static String COMMAND_GET_SCOPES = "get-scopes";
	/**
	 * name of an publish command.
	 */
	public final static String COMMAND_PUBLISH = "publish";
	/**
	 * name of a (global) request command.
	 */
	public final static String COMMAND_REQUEST = "request";
	/**
	 * name of a local request command.
	 */
	public final static String COMMAND_REQUEST_LOCAL = "request_local";
	/**
	 * name of a (global) subscribe command.
	 */
	public final static String COMMAND_SUBSCRIBE = "subscribe";
	/**
	 * name of a local subscribe command.
	 */
	public final static String COMMAND_SUBSCRIBE_LOCAL = "subscribe_local";
	/**
	 * name of a terminate command for clients.
	 */
	public final static String COMMAND_TERMINATE = "terminate";
	/**
	 * name of a termination detection command.
	 */
	public final static String COMMAND_TERMINATION_DETECTION = "term_detection";
	/**
	 * name of a termination command, not the clients.
	 */
	public final static String COMMAND_TERMINATE_NOT_CLIENTS = "terminate_not_client";
	/**
	 * name of a termination command, also the clients.
	 */
	public final static String COMMAND_TERMINATE_ALL = "terminate_all";
	/**
	 * name of an unadvertise command.
	 */
	public final static String COMMAND_UNADVERTISE = "unadvertise";
	/**
	 * name of an unsubscribe command.
	 */
	public final static String COMMAND_UNSUBSCRIBE = "unsubscribe";
	/**
	 * name of an information request command.
	 */
	public final static String COMMAND_INFORMATION_REQUEST = "information";
	/**
	 * name of a join scope command.
	 */
	public final static String COMMAND_JOIN_SCOPE = "joinscope";
	/**
	 * name of a leave scope command.
	 */
	public final static String COMMAND_LEAVE_SCOPE = "leavescope";
	/**
	 * string option for providing the content of the command.
	 */
	public final static String OPTION_COMMAND_CONTENT = "--content";
	/**
	 * integer identifier of an advertisement.
	 */
	public final static String OPTION_ID = "--id";
	/**
	 * string option for providing the name of the file containing the content
	 * of the command.
	 */
	public final static String OPTION_COMMAND_FILE = "--file";
	/**
	 * string option for providing the name of the file containing the XACML
	 * policy.
	 */
	public final static String OPTION_COMMAND_POLICY = "--policy";
	/**
	 * string option for providing the ABAC information of an XACML request.
	 */
	public final static String OPTION_COMMAND_ABAC = "--abac";
	/**
	 * string option for specifying the information that is requested.
	 */
	public final static String OPTION_COMMAND_WHICH_INFO = "--whichinfo";
	/**
	 * string option for specifying all the information.
	 */
	public final static String OPTION_INFO_ALL = "all";
	/**
	 * string option for specifying the scope graphs.
	 */
	public final static String OPTION_INFO_SCOPE_GRAPHS = "scopegraphs";
	/**
	 * string option for specifying the scope lookup tables.
	 */
	public final static String OPTION_INFO_SLTS = "slts";
	/**
	 * string option for specifying the routing tables.
	 */
	public final static String OPTION_INFO_RTS = "rts";
	/**
	 * string option for specifying the scope bottom in command advertise.
	 */
	public final static String OPTION_COMMAND_SCOPEBOTTOM = "--scope-bottom";
	/**
	 * string option for specifying the scope top in command subscribe.
	 */
	public final static String OPTION_COMMAND_SCOPETOP = "--scope-top";
	/**
	 * string option for specifying the dimension in commands join or leave
	 * scope, and in commands advertise and subscribe.
	 */
	public final static String OPTION_COMMAND_DIMENSION = "--dimension";
	/**
	 * string option for specifying the scope in commands advertise and
	 * subscribe.
	 */
	public final static String OPTION_COMMAND_SCOPE = "--scope";
	/**
	 * string option for specifying the sub-scope in commands join or leave
	 * scope.
	 */
	public final static String OPTION_COMMAND_SUB_SCOPE = "--subscope";
	/**
	 * string option for specifying the super-scope in commands join or leave
	 * scope.
	 */
	public final static String OPTION_COMMAND_SUPER_SCOPE = "--superscope";
	/**
	 * string option for specifying the visibility filter from the sub-scope to
	 * the super-scope in command join scope.
	 */
	public final static String OPTION_COMMAND_MAP_UP_FILTER_FILE = "--mapupfile";
	/**
	 * string option for specifying the visibility filter from the super-scope
	 * to the sub-scope in command join scope.
	 */
	public final static String OPTION_COMMAND_MAP_DOWN_FILTER_FILE = "--mapdownfile";
	/**
	 * string option for specifying the explicit set of brokers that maintain the scope
	 */
	public final static String OPTION_COMMAND_EXPLICIT_BROKER_SET = "--brokerset";
	/**
	 * string command for specifying the logger to set.
	 */
	public final static String COMMAND_SETLOG = "setlog";
	/**
	 * string command for specifying the logger to set.
	 */
	public final static String OPTION_COMMAND_LOGGER_NAME = "--name";
	/**
	 * string option for specifying the level of logging.
	 */
	public final static String OPTION_COMMAND_LOGGER_LEVEL = "--level";
	/**
	 * string option for specifying the performance evaluation configuration
	 * file.
	 */
	public final static String OPTION_COMMAND_PERF_CONFIG_FILE = "--perfconfigfile";
	/**
	 * attribute name for analysing all data.
	 */
	public final static String ALL_STATS = "allStats";
	/**
	 * attribute name for analysing the number of identity messages sent.
	 */
	public final static String NB_IDENTITIES_SENT = "nbIdentitiesSent";
	/**
	 * attribute name for analysing the number of identity messages sent.
	 */
	public final static String NB_IDENTITIES_RECEIVED = "nbIdentitiesReceived";
	/**
	 * attribute name for analysing the number of advertisements received.
	 */
	public final static String NB_ADVERTISEMENTS_RECEIVED = "nbAdvertisementsReceived";
	/**
	 * attribute name for analysing the number of advertisement messages sent.
	 */
	public final static String NB_ADVERTISEMENTS_SENT = "nbAdvertisementsSent";
	/**
	 * attribute name for analysing the number of publication messages received.
	 */
	public final static String NB_PUBLICATIONS_RECEIVED = "nbPublicationsReceived";
	/**
	 * attribute name for analysing the number of publication messages sent.
	 */
	public final static String NB_PUBLICATIONS_SENT = "nbPublicationsSent";
	/**
	 * attribute name for analysing the number of replies messages received.
	 */
	public final static String NB_REPLIES_RECEIVED = "nbRepliesReceived";
	/**
	 * attribute name for analysing the number of subscription messages received
	 * from clients.
	 */
	public final static String NB_SUBSCRIPTIONS_RECEIVED = "nbSubscriptionsReceived";
	/**
	 * attribute name for analysing the number of subscription messages received
	 * from brokers.
	 */
	public final static String NB_ADMIN_SUBSCRIPTIONS_RECEIVED = "nbAdminSubscriptionsReceived";
	/**
	 * attribute name for analysing the number of subscription messages sent.
	 */
	public final static String NB_ADMIN_SUBSCRIPTIONS_SENT = "nbAdminSubscriptionsSent";
	/**
	 * attribute name for analysing the number of publication messages received.
	 */
	public final static String NB_ADMIN_PUBLICATIONS_RECEIVED = "nbAdminPublicationsReceived";
	/**
	 * attribute name for analysing the number of subscription messages sent.
	 */
	public final static String NB_ADMIN_PUBLICATION_SENT = "nbAdminPublicationsSent";
	/**
	 * attribute name for analysing the number of unadvertisement messages
	 * received.
	 */
	public final static String NB_UNADVERTISEMENTS_RECEIVED = "nbUnadvertisementsReceived";
	/**
	 * attribute name for analysing the number of unsubscription messages
	 * received from brokers.
	 */
	public final static String NB_ADMIN_UNSCRIPTIONS_RECEIVED = "nbAdminUnsubscriptionsReceived";
	/**
	 * attribute name for analysing the number of unadvertisement messages sent.
	 */
	public final static String NB_UNADVERTISEMENTS_SENT = "nbUnadvertisementsSent";
	/**
	 * attribute name for analysing the number of unsubscription messages
	 * received.
	 */
	public final static String NB_UNSCRIPTIONS_RECEIVED = "nbUnsubscriptionsReceived";
	/**
	 * attribute name for analysing the number of unsubscription messages sent.
	 */
	public final static String NB_UNSCRIPTIONS_SENT = "nbUnsubscriptionsSent";
	/**
	 * attribute name for analysing the number of join scope messages received
	 * from scripting brokers.
	 */
	public final static String NB_JOIN_SCOPE_RECEIVED = "nbJoinScopeReceived";
	/**
	 * attribute name for analysing the total number of publications delivered
	 * to clients.
	 */
	public final static String NB_PUBLICATIONS_DELIVERED = "nbPublicationsDelivered";
	/**
	 * attribute name for analysing the total number of brokers involved in
	 * routing publications from producers to consumers.
	 */
	public final static String NB_BROKERS_INVOLVED_IN_ROUTING = "nbBrokersInvolvedInRouting";
	/**
	 * attribute name for analysing the average number of broker involved in the
	 * routing of notifications.
	 */
	public final static String AVERAGE_NB_INVOLVED_BROKERS = "averageNbInvolvedBrokers";
	/**
	 * attribute name for analysing the routing table size for brokers.
	 */
	public final static String NB_BROKERS_ROUTING_TABLE_SIZE = "brokersRoutingTableSize";
	/**
	 * attribute name for anaylysing the average routing table size for brokers.
	 */
	public final static String AVERAGE_NB_BROKERS_ROUTING_TABLE_SIZE = "averageBrokersRoutingTableSize";
	/**
	 * attribute name for anaylysing the average routing table size for clients.
	 */
	public final static String AVERAGE_NB_CLIENTS_ROUTING_TABLE_SIZE = "averageClientsRoutingTableSize";
	/**
	 * attribute name for analysing the routing table size for clients.
	 */
	public final static String NB_CLIENTS_ROUTING_TABLE_SIZE = "clientsRoutingTableSize";
	/**
	 * attribute name for stating that there is scoping.
	 */
	public final static String SCOPING = "scoping";
	/**
	 * attribute name for analysing the execution time.
	 */
	public final static String EXECUTION_TIME = "executionTime";
	/**
	 * attribute name for analysing the average of execution time.
	 */
	public final static String AVERAGE_EXECUTION_TIME = "averageExecutionTime";
	/**
	 * attribute name for analysing the variance of execution time.
	 */
	public final static String VARIANCE_EXECUTION_TIME = "varianceExecutionTime";
	/**
	 * attribute name for analysing the standard deviation of execution time.
	 */
	public final static String STDV_EXECUTION_TIME = "standardDeviationExecutionTime";
}
