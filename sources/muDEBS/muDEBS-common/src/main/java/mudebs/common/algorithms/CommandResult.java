/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */
package mudebs.common.algorithms;


/**
 * This class is the root of the command result of blocking commands.
 * 
 * @author Denis Conan
 * 
 */
public class CommandResult {
	/**
	 * the status of the command.
	 */
	private CommandStatus status;
	/**
	 * the content of the result, which is a JSON string.
	 */
	private String result;
	
	/**
	 * the constructor.
	 * 
	 * @param s the status.
	 * @param r the result.
	 */
	public CommandResult(final CommandStatus s, final String r) {
		status = s;
		result = r;
	}

	/**
	 * @return the status
	 */
	public CommandStatus getStatus() {
		return status;
	}

	/**
	 * @return the result
	 */
	public String getResult() {
		return result;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CommandResult [status=" + status + ", result=" + result + "]";
	}
}
