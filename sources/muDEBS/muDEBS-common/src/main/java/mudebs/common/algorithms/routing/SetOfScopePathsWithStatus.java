/**
This file is part of the muDEBS platform.

Copyright (C) 2013-2017 Télécom SudParis

The muDEBS platform is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Léon Lim
Contributor(s): Denis Conan
 */
package mudebs.common.algorithms.routing;

import java.util.ArrayList;
import java.util.List;

import mudebs.common.MuDEBSException;

/**
 * This class defines a set of scope paths. All the scope paths must be
 * associated with the same dimension.
 * 
 * @author Léon Lim
 * @author Denis Conan
 *
 */
public class SetOfScopePathsWithStatus {
	/**
	 * the set of scope paths with status.
	 */
	List<ScopePathWithStatus> ssp;

	/**
	 * the constructor of the ScopePathSet.
	 */
	public SetOfScopePathsWithStatus() {
		ssp = new ArrayList<ScopePathWithStatus>();
	}

	/**
	 * creates a set of scope paths with status from a scope. The status is
	 * <tt>UP</tt>.
	 * 
	 * @param scope
	 *            the single scope of the path.
	 * @param lambda
	 *            the status.
	 * @return the set of scope paths with status.
	 */
	public static SetOfScopePathsWithStatus create(final Scope scope,
			final ScopePathStatus lambda) {
		if (scope == null) {
			throw new IllegalArgumentException("the scope is  null");
		}
		SetOfScopePathsWithStatus s = new SetOfScopePathsWithStatus();
		try {
			s.addScopePathWithStatus(ScopePathWithStatus.create(scope, lambda));
		} catch (MuDEBSException e) {
			// one scope path, then should never be thrown
			e.printStackTrace();
		}
		return s;
	}

	/**
	 * adds a scope path to the set of scope paths.
	 * 
	 * @param toAdd
	 *            the scope path to add.
	 * @throws MuDEBSException
	 *             the exception thrown: <tt>null</tt> value, scope path already
	 *             in the set.
	 */
	public void addScopePathWithStatus(final ScopePathWithStatus toAdd)
			throws MuDEBSException {
		if (toAdd == null) {
			throw new IllegalArgumentException("scope path to add is null");
		}
		if (ssp.contains(toAdd)) {
			throw new MuDEBSException(
					"scope path already exists in the set of scope paths");
		}
		ssp.add(toAdd);
	}

	/**
	 * adds a list of scope paths to the set of scope paths.
	 * 
	 * @param listToAdd
	 *            the list of scope paths to add.
	 * @throws MuDEBSException
	 *             the exception thrown: <tt>null</tt> value, scope path already
	 *             in the set.
	 */
	public void addScopePathWithStatus(final List<ScopePathWithStatus> listToAdd)
			throws MuDEBSException {
		if (listToAdd == null) {
			throw new IllegalArgumentException(
					"list of scope paths to add is null");
		}
		for (ScopePathWithStatus sp : listToAdd) {
			ssp.add(sp);
		}
	}

	/**
	 * get the set of scope paths with status.
	 * 
	 * @return the set
	 */
	public List<ScopePathWithStatus> getTheSet() {
		return ssp;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ssp == null) ? 0 : ssp.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SetOfScopePathsWithStatus other = (SetOfScopePathsWithStatus) obj;
		if (ssp == null) {
			if (other.ssp != null)
				return false;
		} else if (!ssp.equals(other.ssp))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SetOfScopePathsWithStatus [ssp=" + ssp + "]";
	}
}
