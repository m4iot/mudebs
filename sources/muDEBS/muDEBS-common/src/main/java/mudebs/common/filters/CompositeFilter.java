/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */
package mudebs.common.filters;

import java.util.List;

/**
 * This abstract class defines a muDEBS composite filter.
 * 
 * @author Denis Conan
 * 
 */
public abstract class CompositeFilter implements Filter {
	/**
	 * the list of filters composing the composite filter.
	 */
	protected List<Filter> filters;

	public List<Filter> getFilters() {
		return filters;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CompositeFilter [filters=" + filters + "]";
	}
}
