/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Léon Lim
Contributor(s): Denis Conan
 */
package mudebs.common.algorithms.routing;

import java.net.URI;
import java.util.Set;

import mudebs.common.algorithms.AbstractMessageContent;

/**
 * This class defines the content of a join scope message.
 * 
 * @author Léon Lim
 * @author Denis Conan
 * 
 */
public class JoinScopeMsgContent extends AbstractMessageContent {
	private URI sender;
	private int sequenceNumber;
	private URI dimension;
	private String subscope;
	private String superscope;
	private String mapupFilter;
	private String mapdownFilter;
	private Set<URI> explicitBrokerURISet;

	@SuppressWarnings("unused")
	private JoinScopeMsgContent() {
	} // for use in OSGi Felix

	public JoinScopeMsgContent(final URI sender, final int sequenceNumber,
			final URI dimension, final String subscope,
			final String superscope, final String upFilter,
			final String downFilter, final Set<URI> explicitBrokerURISet) {
		super();
		this.sender = sender;
		this.sequenceNumber = sequenceNumber;
		this.dimension = dimension;
		this.subscope = subscope;
		this.superscope = superscope;
		this.mapupFilter = upFilter;
		this.mapdownFilter = downFilter;
		this.explicitBrokerURISet = explicitBrokerURISet;
	}

	public URI getSender() {
		return sender;
	}

	public int getSequenceNumber() {
		return sequenceNumber;
	}

	public URI getDimension() {
		return dimension;
	}

	public String getSubScope() {
		return subscope;
	}

	public String getSuperScope() {
		return superscope;
	}

	public String getMapupFilter() {
		return mapupFilter;
	}

	public String getMapdownFilter() {
		return mapdownFilter;
	}

	public Set<URI> getExplicitBrokerURISet() {
		return explicitBrokerURISet;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "JoinScopeMsgContent [sender=" + sender + ", sequenceNumber="
				+ sequenceNumber + ", dimension=" + dimension + ", subscope="
				+ subscope + ", superscope=" + superscope + ", mapupFilter="
				+ mapupFilter + ", mapdownFilter=" + mapdownFilter
				+ ", explicitBrokerSet " + explicitBrokerURISet + "]";
	}

	/**
	 * gets a short version of the message in a string.
	 * 
	 * @return a short description of the message.
	 */
	public String toStringBrief() {
		return "JoinScopeMsgContent [sender="
				+ sender
				+ ", sequenceNumber="
				+ sequenceNumber
				+ ", dimension="
				+ dimension
				+ ", subscope="
				+ subscope
				+ ", superscope="
				+ superscope
				+ ", mapupFilter="
				+ ((mapupFilter.length() < 10) ? mapupFilter : mapupFilter
						.substring(0, 10) + "...")
				+ ", mapdownFilter="
				+ ((mapdownFilter.length() < 10) ? mapdownFilter
						: mapdownFilter.substring(0, 10) + "...")
				+ ", explicitBrokerSet" + explicitBrokerURISet + "]";
	}
}
