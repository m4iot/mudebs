/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Léon Lim
 */
package mudebs.common.algorithms.routing;

import java.net.URI;
import java.util.List;

import mudebs.common.algorithms.ACK;
import mudebs.common.algorithms.AbstractMessageContent;

/**
 * This class defines the content of an advertisement message forwarded by a
 * broker.
 * 
 * @author Denis Conan
 * 
 */
public class AdminAdvertisementMsgContent extends AbstractMessageContent {
	private List<URI> path;
	private ACK ack;
	private int seqNumber;
	private String identifier;
	private String routingFilter;
	private MultiScopingSpecification phiNotif;
	private String policyContent;

	@SuppressWarnings("unused")
	private AdminAdvertisementMsgContent() {
	} // for use in OSGi Felix

	public AdminAdvertisementMsgContent(final List<URI> p, final ACK a,
			final int seq, final String id, final String f,
			final MultiScopingSpecification phiNotif, final String policy) {
		path = p;
		ack = a;
		seqNumber = seq;
		identifier = id;
		routingFilter = f;
		this.phiNotif = phiNotif;
		policyContent = policy;
	}

	public List<URI> getPath() {
		return path;
	}

	public ACK getAck() {
		return ack;
	}

	public int getSeqNumber() {
		return seqNumber;
	}

	public String getIdentifier() {
		return identifier;
	}

	public String getRoutingFilter() {
		return routingFilter;
	}

	public String getPolicyContent() {
		return policyContent;
	}

	public MultiScopingSpecification getPhiNotif() {
		return phiNotif;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AdminAdvertisementMsgContent [path=" + path + ", ack=" + ack
				+ ", seqNumber=" + seqNumber + ", identifier=" + identifier
				+ ", routingFilter=" + routingFilter + ", phiNotif=" + phiNotif
				+ "]";
	}

	/**
	 * gets a short version of the message in a string.
	 * 
	 * @return a short description of the message.
	 */
	public String toStringBrief() {
		return "AdminAdvertisementMsgContent [path="
				+ path
				+ ", ack="
				+ ack
				+ ", seqNumber="
				+ seqNumber
				+ ", identifier="
				+ identifier
				+ ", routingFilter="
				+ ((routingFilter.length() < 10) ? routingFilter
						: routingFilter.substring(0, 10) + "...")
				+ ", phiNotif=" + phiNotif + "]";
	}
}
