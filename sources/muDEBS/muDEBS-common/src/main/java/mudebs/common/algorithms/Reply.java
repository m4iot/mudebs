/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2017 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */
package mudebs.common.algorithms;

import java.net.URI;

/**
 * This class is the root of the replies of blocking requests.
 * 
 * @author Denis Conan
 * 
 */
public class Reply {
	/**
	 * the status of the request.
	 */
	private CommandStatus status;
	/**
	 * the content of the result, which is a JSON string.
	 */
	private String result;
	/**
	 * the URI of the replier.
	 */
	private URI replier;

	/**
	 * the constructor.
	 * 
	 * @param s
	 *            the status.
	 * @param r
	 *            the result.
	 * @param u
	 *            the URI of the replier.
	 */
	public Reply(final CommandStatus s, final String r, final URI u) {
		status = s;
		result = r;
		replier = u;
	}

	/**
	 * @return the status.
	 */
	public CommandStatus getStatus() {
		return status;
	}

	/**
	 * @return the result.
	 */
	public String getResult() {
		return result;
	}

	/**
	 * @return the replier
	 */
	public URI getReplier() {
		return replier;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Reply [status=" + status + ", result=" + result + ", replier="
				+ replier + "]";
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((replier == null) ? 0 : replier.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Reply)) {
			return false;
		}
		Reply other = (Reply) obj;
		if (replier == null) {
			if (other.replier != null) {
				return false;
			}
		} else if (!replier.equals(other.replier)) {
			return false;
		}
		if (status != other.status) {
			return false;
		}
		return true;
	}
}
