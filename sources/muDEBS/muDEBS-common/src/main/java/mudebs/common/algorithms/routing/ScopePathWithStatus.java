/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2017 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Léon Lim
Contributor(s): Denis Conan
 */
package mudebs.common.algorithms.routing;

import java.lang.reflect.Type;
import java.net.URI;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;

import mudebs.common.MuDEBSException;

/**
 * This class defines a scope path with status, which is potentially a
 * visibility path.
 * 
 * 
 * @author Léon Lim
 * @author Denis Conan
 * 
 */
public class ScopePathWithStatus extends ScopePath {
	/**
	 * the status of the scope path.
	 */
	private ScopePathStatus lambda;

	/**
	 * the constructor of the scope path with status.
	 * 
	 * @param dimension
	 *            the dimension of the path.
	 * @param status
	 *            the status with which the scope path is associated.
	 */
	public ScopePathWithStatus(final URI dimension,
			final ScopePathStatus status) {
		super(dimension);
		lambda = status;
	}

	/**
	 * gets the status of the scope path.
	 * 
	 * @return the status
	 */
	public ScopePathStatus status() {
		return lambda;
	}

	/**
	 * sets the status of the scope path.
	 * 
	 * @param newStatus
	 *            the new status of the path.
	 */
	public void setStatus(final ScopePathStatus newStatus) {
		lambda = newStatus;
	}

	/**
	 * creates a scope path with status from a scope and a status.
	 * 
	 * @param scope
	 *            the scope.
	 * @param lambda
	 *            the status.
	 * 
	 * @return the scope path with status
	 */
	public static ScopePathWithStatus create(final Scope scope,
			final ScopePathStatus lambda) {
		if (scope == null) {
			throw new IllegalArgumentException("the scope is  null");
		}
		ScopePathWithStatus sp = new ScopePathWithStatus(scope.dimension,
				lambda);
		try {
			sp.addScope(scope);
		} catch (MuDEBSException e) {
			// one scope, then should never be thrown
			e.printStackTrace();
		}
		return sp;
	}

	/**
	 * add a list of scopes to the paths.
	 * 
	 * @param scopeList
	 *            the list of scopes to add.
	 * @return the same object after the addition.
	 * @throws MuDEBSException
	 *             exception thrown when the scope list either null or empty.
	 */
	public ScopePathWithStatus addScope(final List<Scope> scopeList)
			throws MuDEBSException {
		if (scopeList == null) {
			throw new IllegalArgumentException("scope list is null");
		}
		if (scopeList.isEmpty()) {
			throw new IllegalArgumentException("scope list is empty");
		}
		for (Scope scope : scopeList) {
			this.addScope(scope);
		}
		return this;
	}

	/**
	 * Add a scope to the path by verifying that the scope to add <tt>toAdd</tt>
	 * is an eligible next-hope scope.
	 * 
	 * @param toAdd
	 *            the scope to add.
	 */
	@Override
	public ScopePathWithStatus addScope(final Scope toAdd)
			throws MuDEBSException {
		if (toAdd == null) {
			throw new IllegalArgumentException("scope to add is null");
		}
		if (!toAdd.dimension.equals(dimension)) {
			throw new MuDEBSException(
					"dimension of the scope to add is different from the "
							+ "dimension of the scope path");
		}
		if (this.scopeSequence().contains(toAdd)) {
			throw new MuDEBSException(
					"scope " + toAdd + "already exist in the path");
		}
		if (this.scopeSequence().isEmpty()) {
			this.scopeSequence().add(toAdd);
		} else if (this.scopeSequence().contains(toAdd)) {
			throw new MuDEBSException(
					"scope to add already exists in the path");
		} else if (!toAdd.subScopes().contains(
				this.scopeSequence().get(this.scopeSequence().size() - 1))
				&& !toAdd.superScopes().contains(this.scopeSequence()
						.get(this.scopeSequence().size() - 1))) {
			throw new MuDEBSException(
					"scope to add is neither a super-scope nor a sub-scope "
							+ "of the last scope in the path");
		} else if (lambda == ScopePathStatus.DOWN) {
			if (!this.scopeSequence().get(this.scopeSequence().size() - 1)
					.subScopes().contains(toAdd)) {
				throw new MuDEBSException(
						"path has status " + ScopePathStatus.DOWN
								+ " but the scope to add is not a sub-scope "
								+ "of the last scope in path");
			} else {
				this.scopeSequence().add(toAdd);
			}
		} else if (lambda == ScopePathStatus.UP) {
			if (this.scopeSequence().get(this.scopeSequence().size() - 1)
					.subScopes().contains(toAdd)) {
				lambda = ScopePathStatus.DOWN;
			}
			this.scopeSequence().add(toAdd);
		}
		return this;
	}

	/**
	 * Add a scope to the path without verifying that the scope to add
	 * <tt>toAdd</tt> is an eligible next-hope scope.
	 * 
	 * @param toAdd
	 *            the scope to add.
	 */
	public void addScopeForGsonDeserialisation(final Scope toAdd) {
		this.scopeSequence().add(toAdd);
	}

	/**
	 * serialises the scope path with status to a gson object.
	 * 
	 * @param typeOfSrc the type of the source.
	 * @param context
	 *            the JSON serialization context.
	 * @return the JSON serialization element.
	 */
	public JsonElement serialize(final Type typeOfSrc,
			final JsonSerializationContext context) {
		final JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty("dimension", this.dimension.toString());
		jsonObject.addProperty("lambda", this.status().toString());
		final JsonArray jsonScopeSequence = new JsonArray();
		for (final Scope scope : this.scopeSequence()) {
			final JsonPrimitive jsonScope = new JsonPrimitive(scope.id);
			jsonScopeSequence.add(jsonScope);
		}
		jsonObject.add("scopeSequence", jsonScopeSequence);
		return jsonObject;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ScopePath)) {
			return false;
		}
		ScopePathWithStatus other = (ScopePathWithStatus) obj;
		if (dimension == null) {
			if (other.dimension != null) {
				return false;
			}
		} else if (!dimension.equals(other.dimension)) {
			return false;
		}
		if (lambda == null) {
			if (other.lambda != null) {
				return false;
			}
		} else if (!lambda.equals(other.lambda)) {
			return false;
		}
		if (this.scopeSequence().size() != other.scopeSequence().size()) {
			return false;
		} else {
			for (int i = 0; i < scopeSequence().size(); i++) {
				if (!scopeSequence().get(i)
						.equals(other.scopeSequence().get(i))) {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * returns a the scope path with status in a string.
	 * 
	 * This method is used in classes @see
	 * mudebs.common.algorithms.routing.TestScopePathWithStatus for the purpose
	 * of testing.
	 * 
	 * @return the String serialization of the scope path with its status.
	 */
	public String toStringPath() {
		if (this.scopeSequence().isEmpty()) {
			return "()";
		}
		if (this.scopeSequence().size() == 1) {
			return "((" + scopeSequence().get(0).id + "), " + lambda + ")";
		}
		String result = "((";
		for (int i = 0; i < scopeSequence().size() - 1; i++) {
			result = result + scopeSequence().get(i).identifier() + ", ";
		}
		result = result
				+ scopeSequence().get(scopeSequence().size() - 1).identifier()
				+ "), " + lambda + ")";
		return result;
	}
}
