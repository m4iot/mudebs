/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Léon Lim
Contributor(s): Denis Conan
 */
package mudebs.common;

/**
 * This class defines the attributes of the performance evaluation
 * configuration.
 * 
 * @author Léon Lim
 * @author Denis Conan
 */
public class PerfAttributes {
	/**
	 * states whether there is performance evaluation
	 */
	public static boolean performance_evaluation = false;
	/**
	 * the default directory in which the results are stored.
	 * 
	 * Be careful! The directory name is limited so that only one sub-directory
	 * is created.
	 */
	public static String RESULT_DIR = "./results";
	/**
	 * the configuration file name.
	 */
	public static String PREF_CONFIG_FILE = null;
	/**
	 * the file name for storing the results.
	 */
	public static String PERF_RESULT_FILE = null;
	/**
	 * the network size (number of brokers).
	 */
	public static int networkSize = 0;
	/**
	 * the boolean that states whether the performance evaluation configuration
	 * file if provided.
	 */
	public static boolean perfConfigFile = false;
	/**
	 * the boolean that states whether all data are analysed
	 */
	public static boolean allStats = false;
	/**
	 * the boolean that states whether the number of identity messages sent is
	 * analysed.
	 */
	public static boolean nbIdentitiesSent = false;
	/**
	 * the boolean that states whether the number of identity messages received
	 * is analysed.
	 */
	public static boolean nbIdentitiesReceived = false;
	/**
	 * the boolean that states whether the number of advertisements received is
	 * analysed.
	 */
	public static boolean nbAdvertisementsReceived = false;
	/**
	 * the boolean that states whether the number of advertisement messages sent
	 * is analysed.
	 */
	public static boolean nbAdvertisementsSent = false;
	/**
	 * the boolean that states whether the number of publication messages
	 * received is analysed.
	 */
	public static boolean nbPublicationsReceived = false;
	/**
	 * the boolean that states whether the number of publication messages
	 * delivered is analysed.
	 */
	public static boolean nbPublicationsDelivered = false;
	/**
	 * the boolean that states whether the number of publication messages sent
	 * is analysed.
	 */
	public static boolean nbPublicationsSent = false;
	/**
	 * the boolean that states whether the number of replies messages received
	 * is analysed.
	 */
	public static boolean nbRepliesReceived = false;
	/**
	 * the boolean that states whether the number of subscription messages
	 * received from clients is analysed.
	 */
	public static boolean nbSubscriptionsReceived = false;
	/**
	 * the boolean that states whether the number of subscription messages
	 * received from brokers is analysed.
	 */
	public static boolean nbAdminSubscriptionsReceived = false;
	/**
	 * the boolean that states whether the number of subscription messages sent
	 * is analysed.
	 */
	public static boolean nbAdminSubscriptionsSent = false;
	/**
	 * the boolean that states whether the number of unadvertisement messages
	 * received is analysed.
	 */
	public static boolean nbUnadvertisementsReceived = false;
	/**
	 * the boolean that states whether the number of unsubscription messages
	 * received from brokers.
	 */
	public static boolean nbAdminUnsubscriptionsReceived = false;
	/**
	 * the boolean that states whether the number of unadvertisement messages
	 * sent.
	 */
	public static boolean nbUnadvertisementsSent = false;
	/**
	 * the boolean that states whether the number of unsubscription messages
	 * received.
	 */
	public static boolean nbUnsubscriptionsReceived = false;
	/**
	 * the boolean that states whether the number of unsubscription messages
	 * sent.
	 */
	public static boolean nbUnsubscriptionsSent = false;
	/**
	 * the boolean that states whether the number of join scope messages
	 * received from scripting brokers is analysed.
	 */
	public static boolean nbJoinScopeReceived = false;
	/**
	 * the boolean that states whether the number of publication messages sent
	 * from scripting brokers is analysed.
	 */
	public static boolean nbAdminPublicationsSent = false;
	/**
	 * the boolean that states whether the number of publication messages
	 * received from scripting brokers is analysed.
	 */
	public static boolean nbAdminPublicationsReceived = false;
	/**
	 * the boolean that states whether the average number of brokers involved in
	 * the diffusion of a publication is analysed.
	 */
	public static boolean averageNbInvolvedBrokers = false;
	/**
	 * the boolean that states whether the routing table size for brokers is
	 * analysed
	 */
	public static boolean brokersRoutingTableSize = false;
	/**
	 * the boolean that states whether the routing table size for clients is
	 * analysed
	 */
	public static boolean clientsRoutingTableSize = false;
	/**
	 * the boolean that states whether the unsubscription of all subscriptions
	 * and advertisements starts.
	 */
	public static boolean unAllSubOrPubStarted = false;
	/**
	 * the boolean that states whether there is scoping.
	 */
	public static boolean scoping = false;
	/**
	 * the boolean that state whether the execution time for handling received
	 * notifications is analysed.
	 */
	public static boolean executionTime = false;
}
