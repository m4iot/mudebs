/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */
package mudebs.common.algorithms.routing;

import java.net.URI;
import java.util.List;

import mudebs.common.algorithms.AbstractMessageContent;
import mudebs.common.algorithms.OperationalMode;

/**
 * This class defines the content of a synchronous request.
 * 
 * @author Denis Conan
 * 
 */
public class AdminRequestMsgContent extends AbstractMessageContent {
	private List<URI> path;
	private OperationalMode local;
	private int sequenceNumber;
	private String filter;
	private MultiScopingSpecification phi;
	private ABACInformation abacInfo;

	@SuppressWarnings("unused")
	private AdminRequestMsgContent() {
	} // for use in OSGi Felix

	public AdminRequestMsgContent(final List<URI> p, final OperationalMode m,
			final int seq, final String f, final MultiScopingSpecification phi,
			final ABACInformation a) {
		path = p;
		local = m;
		sequenceNumber = seq;
		filter = f;
		this.phi = phi;
		abacInfo = a;
	}

	public List<URI> getPath() {
		return path;
	}

	public OperationalMode getLocal() {
		return local;
	}

	public int getSequenceNumber() {
		return sequenceNumber;
	}

	public String getFilter() {
		return filter;
	}

	public MultiScopingSpecification getPhi() {
		return phi;
	}

	public ABACInformation getABACInformation() {
		return abacInfo;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AdminRequestMsgContent [path=" + path + ", local=" + local
				+ ", sequenceNumber=" + sequenceNumber + ", filter=" + filter
				+ ",phi=" + phi + ", abacInfo=" + abacInfo + "]";
	}

	/**
	 * gets a short version of the message in a string.
	 * 
	 * @return a short description of the message.
	 */
	public String toStringBrief() {
		return "AdminRequestMsgContent [path="
				+ path
				+ ", local="
				+ local
				+ ", sequenceNumber="
				+ sequenceNumber
				+ ", filter="
				+ ((filter.length() < 10) ? filter : filter.substring(0, 10)
						+ "...") + ", phi=" + phi + ", abacInfo=" + abacInfo
				+ "]";
	}
}
