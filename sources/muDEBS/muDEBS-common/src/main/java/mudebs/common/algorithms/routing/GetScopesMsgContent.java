/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): 
 */
package mudebs.common.algorithms.routing;

import java.net.URI;

import mudebs.common.algorithms.AbstractMessageContent;

/**
 * This class defines the content of a message to ask for a set of scopes.
 * 
 * @author Denis Conan
 * 
 */
public class GetScopesMsgContent extends AbstractMessageContent {
	private URI sender;
	private int seqNumber;

	@SuppressWarnings("unused")
	private GetScopesMsgContent() {
	} // for use in OSGi Felix

	public GetScopesMsgContent(final URI sender, final int seqNum) {
		super();
		this.sender = sender;
		seqNumber = seqNum;
	}

	public URI getSender() {
		return sender;
	}

	public int getSequenceNumber() {
		return seqNumber;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "GetScopesMsgContent [sender=" + sender + "]";
	}

	/**
	 * gets a short version of the message in a string.
	 * 
	 * @return a short description of the message.
	 */
	public String toStringBrief() {
		return toString();
	}
}
