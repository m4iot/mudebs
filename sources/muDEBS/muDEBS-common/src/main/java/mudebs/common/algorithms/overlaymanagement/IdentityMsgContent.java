/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2017 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */
package mudebs.common.algorithms.overlaymanagement;

import java.net.URI;

import mudebs.common.algorithms.AbstractMessageContent;
import mudebs.common.algorithms.EntityType;

/**
 * This class defines the content of a message providing the identity of the
 * sender.
 * 
 * @author Denis Conan
 * 
 */
public class IdentityMsgContent extends AbstractMessageContent {
	/**
	 * the identity of the sender.
	 */
	private URI identity;
	/**
	 * the type of the sender.
	 */
	private EntityType type;

    @SuppressWarnings("unused")
	private IdentityMsgContent() {} // for use in OSGi in Felix
	
	/**
	 * Constructor of the message content.
	 * 
	 * @param s the URI of the sender.
	 * @param t the type of the sender.
	 */
	public IdentityMsgContent(final URI s, final EntityType t) {
		identity = s;
		type = t;
	}

	/**
	 * gets the identity.
	 * 
	 * @return the URI of the sender.
	 */
	public URI getIdentity() {
		return identity;
	}
	
	/**
	 * gets the type of the entity.
	 * 
	 * @return the type of the entity.
	 */
	public EntityType getEntityType() {
		return type;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "IdentityMsgContent [identity=" + identity + ", type=" + type
				+ "]";
	}

	/**
	 * gets a short version of the message in a string.
	 * 
	 * @return a short description of the message.
	 */
	public String toStringBrief() {
		return this.toString();
	}
}
