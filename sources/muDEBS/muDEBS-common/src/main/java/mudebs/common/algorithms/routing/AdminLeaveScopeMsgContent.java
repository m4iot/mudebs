/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Léon Lim
Contributor(s): Denis Conan
 */
package mudebs.common.algorithms.routing;

import java.net.URI;
import java.util.List;

import mudebs.common.algorithms.AbstractMessageContent;

/**
 * This class defines the content of an admin leave scope message.
 * 
 * @author Denis Conan
 * @author Léon Lim
 * 
 */
public class AdminLeaveScopeMsgContent extends AbstractMessageContent {
	private List<URI> path;
	private int sequenceNumber;
	private Scope startingScope;
	private Scope endingScope;

	@SuppressWarnings("unused")
	private AdminLeaveScopeMsgContent() {
	} // for use in OSGi Felix

	public AdminLeaveScopeMsgContent(final List<URI> p, final int sequenceNumber,
			final Scope statingScope, final Scope endingScope) {
		super();
		this.path = p;
		this.sequenceNumber = sequenceNumber;
		this.startingScope = statingScope;
		this.endingScope = endingScope;
	}

	public List<URI> getPath() {
		return path;
	}

	public int getSequenceNumber() {
		return sequenceNumber;
	}

	public Scope getStatingScope() {
		return startingScope;
	}

	public Scope getEndingScope() {
		return endingScope;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AdminJoinScopeMsgContent [path=" + path
				+ ", Starting scope=" + startingScope + ", Ending scope="
				+ endingScope + "]";
	}

	/**
	 * gets a short version of the message in a string.
	 * 
	 * @return a short description of the message.
	 */
	public String toStringBrief() {
		return toString();
	}
}
