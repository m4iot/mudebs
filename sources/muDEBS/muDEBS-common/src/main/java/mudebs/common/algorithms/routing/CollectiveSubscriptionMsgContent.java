/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */
package mudebs.common.algorithms.routing;

import java.net.URI;
import java.util.List;

import mudebs.common.algorithms.ACK;
import mudebs.common.algorithms.AbstractMessageContent;
import mudebs.common.algorithms.OperationalMode;

/**
 * This class defines the content of a collective subscription message sent by a
 * client.
 * 
 * @author Denis Conan
 * 
 */
public class CollectiveSubscriptionMsgContent extends AbstractMessageContent {
	private URI client;
	private int seqNumber;
	private OperationalMode opMode;
	private ACK ack;
	private List<Subscription> subscriptions;

	protected CollectiveSubscriptionMsgContent() {
	} // for use in OSGi Felix

	public CollectiveSubscriptionMsgContent(final URI client,
			final int seqNumber, final OperationalMode opMode, final ACK ack,
			final List<Subscription> subscriptions) {
		this.client = client;
		this.seqNumber = seqNumber;
		this.opMode = opMode;
		this.ack = ack;
		this.subscriptions = subscriptions;
	}

	public URI getClient() {
		return client;
	}

	public int getSequenceNumber() {
		return seqNumber;
	}

	public OperationalMode getOperationalMode() {
		return opMode;
	}

	public ACK getACK() {
		return ack;
	}

	public List<Subscription> getSubscriptions() {
		return subscriptions;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CollectiveSubscriptionMsgContent [client=" + client
				+ ", seqNumber=" + seqNumber + ", opMode=" + opMode + ", ack="
				+ ack + ", subscriptions=" + subscriptions + "]";
	}

	/**
	 * gets a short version of the message in a string.
	 * 
	 * @return a short description of the message.
	 */
	public String toStringBrief() {
		return "CollectiveSubscriptionMsgContent [client=" + client
				+ ", seqNumber=" + seqNumber + ", opMode=" + opMode + ", ack="
				+ ack + ", subscriptions= [Subscription["
				+ subscriptions.size() + " subscriptions]]";
	}
}
