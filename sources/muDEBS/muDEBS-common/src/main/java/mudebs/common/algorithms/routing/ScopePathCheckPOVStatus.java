/**
This file is part of the muDEBS platform.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS platform is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Léon Lim
 */

package mudebs.common.algorithms.routing;

/**
 * This class represents the point of view for check the scope path can be a
 * visibility, which can take take the value fromNotif and fromSub.
 * 
 * @author Léon Lim
 * 
 */
public enum ScopePathCheckPOVStatus {
	/**
	 * states that the verification is done according to publication point of
	 * view.
	 */
	FROMNOTIF,
	/**
	 * states that the verification is done according to subscription point of
	 * view.
	 */
	FROMSUB;
}
