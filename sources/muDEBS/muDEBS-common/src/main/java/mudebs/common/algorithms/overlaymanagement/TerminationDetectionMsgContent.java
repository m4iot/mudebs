/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2017 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */
package mudebs.common.algorithms.overlaymanagement;

import java.net.URI;
import java.util.HashMap;

import mudebs.common.algorithms.AbstractMessageContent;

/**
 * This class defines the content of a message of the termination detection
 * algorithm.
 * 
 * @author Denis Conan
 * 
 */
public class TerminationDetectionMsgContent extends AbstractMessageContent {
	/**
	 * the identity of the initiator.
	 */
	private URI initiator;
	/**
	 * the identity of the forwarder.
	 */
	private URI forwarder;
	/**
	 * the identifier of the wave for termination detection.
	 */
	private int waveNumber;
	/**
	 * the number of publication and request messages sent minus the number of
	 * publication and request messages received.
	 */
	private int nbMsgsInTransit;
	/**
	 * the color of the token: black = some local producers are still in action,
	 * white = no local producers seen.
	 */
	private ColorTerminationDetection color;
	/**
	 * the set of statistics.
	 */
	private HashMap<URI, HashMap<String, Number>> stats;

	@SuppressWarnings("unused")
	private TerminationDetectionMsgContent() {
	} // for use in OSGi in Felix

	/**
	 * Constructor of the message content.
	 * 
	 * @param initiator
	 *            the URI of the initiator.
	 * @param forwarder
	 *            the URI of the forwarder.
	 * @param wave
	 *            the wave number.
	 * @param nbMsgs
	 *            the number of messages in transit.
	 * @param color
	 *            the color of the token.
	 * @param stats
	 *            the statistics gathered.
	 */
	public TerminationDetectionMsgContent(final URI initiator,
			final URI forwarder, final int wave, final int nbMsgs,
			final ColorTerminationDetection color,
			final HashMap<URI, HashMap<String, Number>> stats) {
		this.initiator = initiator;
		this.forwarder = forwarder;
		waveNumber = wave;
		nbMsgsInTransit = nbMsgs;
		this.color = color;
		this.stats = stats;
	}

	/**
	 * gets the URI of the initiator.
	 * 
	 * @return the URI of the initiator.
	 */
	public URI getInitiator() {
		return initiator;
	}

	/**
	 * gets the URI of the forwarder.
	 * 
	 * @return the URI of the forwarder.
	 */
	public URI getForwarder() {
		return forwarder;
	}

	/**
	 * gets the wave number.
	 * 
	 * @return the number of the wave.
	 */
	public int getWaveNumber() {
		return waveNumber;
	}

	/**
	 * gets the number of messages in transit.
	 * 
	 * @return the number of messages stil in transit.
	 */
	public int getNbMsgsInTransit() {
		return nbMsgsInTransit;
	}

	/**
	 * gets the color.
	 * 
	 * @return the color of the token.
	 */
	public ColorTerminationDetection getColor() {
		return color;
	}

	/**
	 * gets the statistics.
	 * 
	 * @return the statistics that are collected while terminating.
	 */
	public HashMap<URI, HashMap<String, Number>> getStatistics() {
		return stats;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TerminationDetectionMsgContent [initiator="
				+ initiator
				+ ", forwarder="
				+ forwarder
				+ ", waveNumber="
				+ waveNumber
				+ ", nbMsgsInTransit="
				+ nbMsgsInTransit
				+ ", color="
				+ ((color == ColorTerminationDetection.BLACK) ? "black"
						: "white") + ", statistics=" + stats + "]";
	}

	/**
	 * gets a short version of the message in a string.
	 * 
	 * @return a short description of the message.
	 */
	public String toStringBrief() {
		return "TerminationDetectionMsgContent [initiator="
				+ initiator
				+ ", forwarder="
				+ forwarder
				+ ", waveNumber="
				+ waveNumber
				+ ", nbMsgsInTransit="
				+ nbMsgsInTransit
				+ ", color="
				+ ((color == ColorTerminationDetection.BLACK) ? "black"
						: "white") + "]";
	}
}
