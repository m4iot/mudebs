/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Léon Lim
Contributor(s): 
 */
package mudebs.common.algorithms.routing;

import java.lang.reflect.Type;
import java.net.URI;
import java.net.URISyntaxException;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * This class define the adaptor that serialises (deserialises) a scope path
 * with status to a gson object (a gson object representing a scope path with
 * status).
 * 
 * @author Léon Lim
 *
 */
public class ScopePathWithStatusAdaptor implements
		JsonSerializer<ScopePathWithStatus>,
		JsonDeserializer<ScopePathWithStatus> {
	@Override
	public JsonElement serialize(final ScopePathWithStatus sp,
			final Type typeOfSrc, final JsonSerializationContext context) {
		final JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty("dimension", sp.dimension.toString());
		jsonObject.addProperty("lambda", sp.status().toString());
		final JsonArray jsonScopeSequence = new JsonArray();
		for (final Scope scope : sp.scopeSequence()) {
			final JsonPrimitive jsonScope = new JsonPrimitive(scope.id);
			jsonScopeSequence.add(jsonScope);
		}
		jsonObject.add("scopeSequence", jsonScopeSequence);
		return jsonObject;
	}

	@Override
	public ScopePathWithStatus deserialize(JsonElement jsonScopePathWithStatus,
			Type typeOfSrc, JsonDeserializationContext context)
			throws JsonParseException {
		final JsonObject jsonObject = jsonScopePathWithStatus.getAsJsonObject();
		final JsonElement jsonDimension = jsonObject.get("dimension");
		final JsonElement jsonStatus = jsonObject.get("lambda");
		final JsonArray jsonScopeSequence = jsonObject.get("scopeSequence")
				.getAsJsonArray();
		ScopePathStatus status = null;
		final String stringStatus = jsonStatus.toString();
		if (stringStatus.replace("\"", "").equals("UP")) {
			status = ScopePathStatus.UP;
		} else {
			status = ScopePathStatus.DOWN;
		}
		URI dimension = null;
		try {
			dimension = new URI(jsonDimension.toString().replace("\"", ""));
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		final ScopePathWithStatus sp = new ScopePathWithStatus(dimension,
				status);
		for (JsonElement scopeString : jsonScopeSequence) {
			Scope scope = new Scope(dimension, scopeString.toString().replace("\"", ""));
			sp.addScopeForGsonDeserialisation(scope);
		}
		return sp;
	}
}
