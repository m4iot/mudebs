/**
 This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

 The muDEBS software is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 The muDEBS software platform is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

 Initial developer(s): Denis Conan
 Contributor(s):
 */

package mudebs.common.algorithms.routing;


/**
 * This class defines a subscription as an identifier, a routing filter, a
 * multiscoping specification, and ABAC information.
 * 
 * @author Denis Conan
 * 
 */
public class Subscription {
	/**
	 * the identifier.
	 */
	private String identifier;
	/**
	 * the routing filter.
	 */
	private String routingFilter;
	/**
	 * the multiscoping specification.
	 */
	private MultiScopingSpecification phi;
	/**
	 * the ABAC information.
	 */
	private ABACInformation abacInformation;

	/**
	 * constructs the objects.
	 * 
	 * @param identifier
	 *            the identifier.
	 * @param routingFilter
	 *            the routing filter.
	 * @param phi
	 *            the multiscoping specification.
	 * @param abacInformation
	 *            the ABAC information.
	 */
	public Subscription(final String identifier, final String routingFilter,
			final MultiScopingSpecification phi,
			final ABACInformation abacInformation) {
		this.identifier = identifier;
		this.routingFilter = routingFilter;
		this.phi = phi;
		this.abacInformation = abacInformation;
	}

	/**
	 * gets the identifier.
	 * 
	 * @return the identifier.
	 */
	public String getIdentifier() {
		return identifier;
	}

	/**
	 * gets routingFilter.
	 * 
	 * @return the routingFilter.
	 */
	public String getRoutingFilter() {
		return routingFilter;
	}

	/**
	 * gets the multiscoping specification.
	 * 
	 * @return the multiscoping specification.
	 */
	public MultiScopingSpecification getPhi() {
		return phi;
	}

	/**
	 * gets the ABAC information.
	 * 
	 * @return the ABAC information.
	 */
	public ABACInformation getAbacInformation() {
		return abacInformation;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((identifier == null) ? 0 : identifier.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Subscription)) {
			return false;
		}
		Subscription other = (Subscription) obj;
		if (identifier == null) {
			if (other.identifier != null) {
				return false;
			}
		} else if (!identifier.equals(other.identifier)) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Subscription [identifier=" + identifier + ", routingFilter="
				+ routingFilter + ", phi=" + phi + ", abacInformation="
				+ abacInformation + "]";
	}
}
