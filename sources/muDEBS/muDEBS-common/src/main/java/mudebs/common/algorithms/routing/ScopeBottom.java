/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Léon Lim
 */
package mudebs.common.algorithms.routing;

import java.net.URI;

/**
 * This class defines the virtual scope &perp; (bottom), which is the sub-scope
 * of all the scopes in a scope graph.
 * 
 * @author Denis Conan
 * @author Léon Lim
 */
public class ScopeBottom extends Scope {
	/**
	 * the identifier of the scope.
	 */
	public static String IDENTIFIER = "bot";

	/**
	 * construct a virtual scope Bottom with the identifier "bot".
	 * 
	 * @param dimension
	 *            the dimension of the virtual scope Bottom.
	 */
	public ScopeBottom(final URI dimension) {
		super(dimension, IDENTIFIER);
	}

	/**
	 * construct a virtual scope Bottom not attached to a particular dimension.
	 * This constructor can be used for instance when advertising.
	 */
	public ScopeBottom() {
		super(Scope.getDummyDimension(), IDENTIFIER);
	}
}
