/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */
package mudebs.common.algorithms.routing;

import java.net.URI;
import java.util.List;

import mudebs.common.algorithms.ACK;

/**
 * This class defines the content of a publication message with several
 * publications ---i.e., a collective publication.
 * 
 * @author Denis Conan
 * 
 */
public class CollectivePublicationMsgContent extends
		CollectiveAdminPublicationMsgContent {

	@SuppressWarnings("unused")
	private CollectivePublicationMsgContent() {
		super();
	} // for use in OSGi Felix

	public CollectivePublicationMsgContent(final List<URI> p, final ACK a,
			final int seqNumberAck, final List<Publication> pubs) {
		super(p, a, seqNumberAck, pubs);
	}

	public boolean getPrivacyRequirement() {
		throw new UnsupportedOperationException("Privacy requirement unknown");
	}

	public List<String> getSubsEnabledAC() {
		throw new UnsupportedOperationException("List of subscriptions"
				+ " with enabled access control unknown");
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CollectivePublicationMsgContent [path=" + getPath() + ", ack="
				+ getAck() + ", seqNumberForAck=" + getSeqNumberForAck()
				+ ", pubs=" + getPublications() + "]";
	}

	/**
	 * gets a short version of the message in a string.
	 * 
	 * @return a short description of the message.
	 */
	public String toStringBrief() {
		return "CollectivePublicationMsgContent [path=" + getPath()
				+ ", ack=" + getAck() + ", seqNumberForAck="
				+ getSeqNumberForAck() + ", pubs=[" + getPublications().size()
				+ "]]";
	}
}
