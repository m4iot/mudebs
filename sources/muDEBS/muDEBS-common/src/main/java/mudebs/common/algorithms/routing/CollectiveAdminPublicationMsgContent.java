/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */
package mudebs.common.algorithms.routing;

import java.net.URI;
import java.util.List;

import mudebs.common.algorithms.ACK;
import mudebs.common.algorithms.AbstractMessageContent;

/**
 * This class defines the content of an admin publication with several
 * publications ---i.e., a collective admin publication.
 * 
 * @author Denis Conan
 * 
 */
public class CollectiveAdminPublicationMsgContent extends
		AbstractMessageContent {
	private List<URI> path;
	private ACK ack;
	private int seqNumberForAck;
	private List<? extends AdminPublication> pubs;

	protected CollectiveAdminPublicationMsgContent() {
	} // for use in OSGi Felix

	public CollectiveAdminPublicationMsgContent(final List<URI> p, final ACK a,
			final int seqNumberAck, List<? extends AdminPublication> pubs) {
		path = p;
		ack = a;
		seqNumberForAck = seqNumberAck;
		this.pubs = pubs;
	}

	public List<URI> getPath() {
		return path;
	}

	public ACK getAck() {
		return ack;
	}

	public int getSeqNumberForAck() {
		return seqNumberForAck;
	}

	public List<? extends AdminPublication> getPublications() {
		return pubs;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CollectiveAdminPublicationMsgContent [path=" + path + ", ack="
				+ ack + ", seqNumberForAck=" + seqNumberForAck + ", pubs="
				+ pubs + "]";
	}

	/**
	 * gets a short version of the message in a string.
	 * 
	 * @return a short description of the message.
	 */
	public String toStringBrief() {
		return "CollectiveAdminPublicationMsgContent [path=" + path + ", ack="
				+ ack + ", seqNumberForAck=" + seqNumberForAck + ", pubs=["
				+ ", pubs=[" + pubs.size() + "]]";
	}
}
