/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */
package mudebs.common.algorithms.routing;

import java.net.URI;
import java.util.List;

import mudebs.common.algorithms.ACK;
import mudebs.common.algorithms.AbstractMessageContent;

/**
 * This class defines the content of a subscription message forwarded by a
 * broker.
 * 
 * @author Denis Conan
 * 
 */
public class AdminSubscriptionMsgContent extends AbstractMessageContent {
	private URI client;
	private ACK ack;
	private int seqNumber;
	private List<URI> path;
	private String identifier;
	private String routingFilter;
	private MultiScopingSpecification phiSub;
	private ABACInformation abacInfo;

	@SuppressWarnings("unused")
	private AdminSubscriptionMsgContent() {
	} // for use in OSGi Felix

	public AdminSubscriptionMsgContent(final URI c, final ACK a, final int seq,
			final List<URI> p, final String id, final String msg,
			MultiScopingSpecification phiSub, final ABACInformation abac) {
		client = c;
		ack = a;
		seqNumber = seq;
		path = p;
		identifier = id;
		routingFilter = msg;
		this.phiSub = phiSub;
		abacInfo = abac;
	}

	public URI getClient() {
		return client;
	}

	public ACK getAck() {
		return ack;
	}

	public int getSeqNumber() {
		return seqNumber;
	}

	public List<URI> getPath() {
		return path;
	}

	public String getIdentifier() {
		return identifier;
	}

	public String getRoutingFilter() {
		return routingFilter;
	}

	public MultiScopingSpecification getPhiSub() {
		return phiSub;
	}

	public ABACInformation getABACInformation() {
		return abacInfo;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AdminSubscriptionMsgContent [client=" + client + ", ack=" + ack
				+ ", seqNumber=" + seqNumber + ", path=" + path
				+ ", identifier=" + identifier + ", routingFilter="
				+ routingFilter + ", phiSub=" + phiSub + ", abacInfo=" + abacInfo
				+ "]";
	}

	/**
	 * gets a short version of the message in a string.
	 * 
	 * @return a short description of the message.
	 */
	public String toStringBrief() {
		return "AdminSubscriptionMsgContent [client="
				+ client
				+ ", ack="
				+ ack
				+ ", seqNumber="
				+ seqNumber
				+ ", path="
				+ path
				+ ", identifier="
				+ identifier
				+ ", routingFilter="
				+ ((routingFilter.length() < 10) ? routingFilter
						: routingFilter.substring(0, 10) + "...") + ", phiSub="
				+ phiSub + ", abacInfo=" + abacInfo + "]";
	}
}
