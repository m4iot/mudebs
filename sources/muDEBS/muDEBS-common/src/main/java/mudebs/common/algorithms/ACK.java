/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Christian Bac and Denis Conan
Contributor(s):
 */
package mudebs.common.algorithms;

/**
 * This enumeration defines the requirements for acknowledgement in case of
 * blocking command.
 * 
 * @author Denis Conan
 * 
 */
public enum ACK {
	/**
	 * This is a non-blocking call.
	 */
	NO,
	/**
	 * This is a blocking call. The caller asks for an acknowledgement as soon
	 * as the call is taking into account by the callee. In other words, no
	 * distributed algorithm is assumed before the callee acknowledges.
	 */
	LOCAL,
	/**
	 * This is a blocking call. The caller asks for an acknowledgement involving
	 * all the architectural entities that take part to the resolution of the
	 * call. In other words, the callee may implement a distributed algorithm
	 * such as a wave algorithm.
	 */
	GLOBAL;
}
