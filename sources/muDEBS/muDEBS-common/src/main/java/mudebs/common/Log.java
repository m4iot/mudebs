/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Léon Lim
 */
package mudebs.common;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 * This class contains the configuration of the logging facilities provided in
 * muDEBS.
 * 
 * To recapitulate, logging levels are: TRACE, DEBUG, INFO, WARN, ERROR, FATAL.
 * Specific logging levels for the performance evaluation are: NONE,
 * NB_ADV_RECEIVED, NB_ADV_SENT, NB_PUB_RECEIVED, NB_PUB_SENT, NB_REQ_RECEIVED,
 * NB_REP_SENT, NB_SUB_RECEIVED, NB_SUB_SENT,
 * 
 * @author Denis Conan
 * @author Léon Lim
 * 
 */
public final class Log {
	/**
	 * states whether logging is enabled or not.
	 */
	public static final boolean ON = true;
	/**
	 * name of logger for the communication part.
	 */
	public static final String LOGGER_NAME_COMM = "communication";
	/**
	 * logger object for the communication part.
	 */
	public static final Logger COMM = Logger.getLogger(LOGGER_NAME_COMM);
	/**
	 * name of logger for the configuration part.
	 */
	public static final String LOGGER_NAME_CONFIG = "configuration";
	/**
	 * logger object for the configuration part.
	 */
	public static final Logger CONFIG = Logger.getLogger(LOGGER_NAME_CONFIG);
	/**
	 * name of logger for the dispatching part.
	 */
	public static final String LOGGER_NAME_DISPATCH = "dispatching";
	/**
	 * logger object for the message dispatching part.
	 */
	public static final Logger DISPATCH = Logger
			.getLogger(LOGGER_NAME_DISPATCH);
	/**
	 * name of logger for the overlay-mngmt part.
	 */
	public static final String LOGGER_NAME_OVERLAY = "overlay";
	/**
	 * logger object for the overlay management part.
	 */
	public static final Logger OVERLAY = Logger.getLogger(LOGGER_NAME_OVERLAY);
	/**
	 * name of logger for the routing part.
	 */
	public static final String LOGGER_NAME_ROUTING = "routing";
	/**
	 * logger object for the routing part.
	 */
	public static final Logger ROUTING = Logger.getLogger(LOGGER_NAME_ROUTING);
	/**
	 * name of logger for the scoping part.
	 */
	public static final String LOGGER_NAME_SCOPING = "scoping";
	/**
	 * logger object for the scoping part.
	 */
	public static final Logger SCOPING = Logger.getLogger(LOGGER_NAME_SCOPING);
	/**
	 * name of logger for the scripting part.
	 */
	public static final String LOGGER_NAME_SCRIPTING = "scripting";
	/**
	 * logger object for the scripting part.
	 */
	public static final Logger SCRIPTING = Logger
			.getLogger("    muDEBS-scripting");
	/**
	 * name of the logger for performance evaluation part.
	 */
	public static final String LOGGER_NAME_PERFORMANCE = "performance";
	/**
	 * logger object for the performance evaluation part.
	 */
	public static final Logger PERFORMANCE = Logger.getLogger(LOGGER_NAME_PERFORMANCE);
	/**
	 * string value for the level <tt>FATAL</tt>.
	 */
	public final static String LEVEL_FATAL = "fatal";
	/**
	 * string value for the level <tt>FATAL</tt>.
	 */
	public final static String LEVEL_ERROR = "error";
	/**
	 * string value for the level <tt>ERROR</tt>.
	 */
	public final static String LEVEL_WARN = "warn";
	/**
	 * string value for the level <tt>WARN</tt>.
	 */
	public final static String LEVEL_INFO = "info";
	/**
	 * string value for the level <tt>DEBUG</tt>.
	 */
	public final static String LEVEL_DEBUG = "debug";
	/**
	 * string value for the level <tt>TRACE</tt>.
	 */
	public final static String LEVEL_TRACE = "trace";
	/**
	 * string value for the level <tt>PERF</tt>.
	 */
	public final static String LEVEL_PERF = "perf";

	/**
	 * static configuration, which can be changed by command line options.
	 */
	static {
		BasicConfigurator.configure();
		COMM.setLevel(Level.WARN);
		CONFIG.setLevel(Level.WARN);
		DISPATCH.setLevel(Level.WARN);
		OVERLAY.setLevel(Level.WARN);
		ROUTING.setLevel(Level.WARN);
		SCOPING.setLevel(Level.ERROR);
		SCRIPTING.setLevel(Level.WARN);
		PERFORMANCE.setLevel(PerfLevel.OFF);
	}

	/**
	 * configures a logger to a level.
	 * 
	 * @param loggerName
	 *            the name of the logger.
	 * @param levelName
	 *            the name of the level.
	 */
	public static void configureALogger(final String loggerName,
			final String levelName) {
		if (loggerName == null || levelName == null) {
			return;
		}
		if (loggerName.equalsIgnoreCase(LOGGER_NAME_COMM)) {
			if (levelName.equalsIgnoreCase(LEVEL_TRACE)) {
				COMM.setLevel(Level.TRACE);
			} else if (levelName.equalsIgnoreCase(LEVEL_DEBUG)) {
				COMM.setLevel(Level.DEBUG);
			} else if (levelName.equalsIgnoreCase(LEVEL_INFO)) {
				COMM.setLevel(Level.INFO);
			} else if (levelName.equalsIgnoreCase(LEVEL_WARN)) {
				COMM.setLevel(Level.WARN);
			} else if (levelName.equalsIgnoreCase(LEVEL_ERROR)) {
				COMM.setLevel(Level.ERROR);
			} else if (levelName.equalsIgnoreCase(LEVEL_FATAL)) {
				COMM.setLevel(Level.FATAL);
			}
		} else if (loggerName.equalsIgnoreCase(LOGGER_NAME_CONFIG)) {
			if (levelName.equalsIgnoreCase(LEVEL_TRACE)) {
				CONFIG.setLevel(Level.TRACE);
			} else if (levelName.equalsIgnoreCase(LEVEL_DEBUG)) {
				CONFIG.setLevel(Level.DEBUG);
			} else if (levelName.equalsIgnoreCase(LEVEL_INFO)) {
				CONFIG.setLevel(Level.INFO);
			} else if (levelName.equalsIgnoreCase(LEVEL_WARN)) {
				CONFIG.setLevel(Level.WARN);
			} else if (levelName.equalsIgnoreCase(LEVEL_ERROR)) {
				CONFIG.setLevel(Level.ERROR);
			} else if (levelName.equalsIgnoreCase(LEVEL_FATAL)) {
				CONFIG.setLevel(Level.FATAL);
			}
		} else if (loggerName.equalsIgnoreCase(LOGGER_NAME_DISPATCH)) {
			if (levelName.equalsIgnoreCase(LEVEL_TRACE)) {
				DISPATCH.setLevel(Level.TRACE);
			} else if (levelName.equalsIgnoreCase(LEVEL_DEBUG)) {
				DISPATCH.setLevel(Level.DEBUG);
			} else if (levelName.equalsIgnoreCase(LEVEL_INFO)) {
				DISPATCH.setLevel(Level.INFO);
			} else if (levelName.equalsIgnoreCase(LEVEL_WARN)) {
				DISPATCH.setLevel(Level.WARN);
			} else if (levelName.equalsIgnoreCase(LEVEL_ERROR)) {
				DISPATCH.setLevel(Level.ERROR);
			} else if (levelName.equalsIgnoreCase(LEVEL_FATAL)) {
				DISPATCH.setLevel(Level.FATAL);
			}
		} else if (loggerName.equalsIgnoreCase(LOGGER_NAME_OVERLAY)) {
			if (levelName.equalsIgnoreCase(LEVEL_TRACE)) {
				OVERLAY.setLevel(Level.TRACE);
			} else if (levelName.equalsIgnoreCase(LEVEL_DEBUG)) {
				OVERLAY.setLevel(Level.DEBUG);
			} else if (levelName.equalsIgnoreCase(LEVEL_INFO)) {
				OVERLAY.setLevel(Level.INFO);
			} else if (levelName.equalsIgnoreCase(LEVEL_WARN)) {
				OVERLAY.setLevel(Level.WARN);
			} else if (levelName.equalsIgnoreCase(LEVEL_ERROR)) {
				OVERLAY.setLevel(Level.ERROR);
			} else if (levelName.equalsIgnoreCase(LEVEL_FATAL)) {
				OVERLAY.setLevel(Level.FATAL);
			}
		} else if (loggerName.equalsIgnoreCase(LOGGER_NAME_ROUTING)) {
			if (levelName.equalsIgnoreCase(LEVEL_TRACE)) {
				ROUTING.setLevel(Level.TRACE);
			} else if (levelName.equalsIgnoreCase(LEVEL_DEBUG)) {
				ROUTING.setLevel(Level.DEBUG);
			} else if (levelName.equalsIgnoreCase(LEVEL_INFO)) {
				ROUTING.setLevel(Level.INFO);
			} else if (levelName.equalsIgnoreCase(LEVEL_WARN)) {
				ROUTING.setLevel(Level.WARN);
			} else if (levelName.equalsIgnoreCase(LEVEL_ERROR)) {
				ROUTING.setLevel(Level.ERROR);
			} else if (levelName.equalsIgnoreCase(LEVEL_FATAL)) {
				ROUTING.setLevel(Level.FATAL);
			}
		} else if (loggerName.equalsIgnoreCase(LOGGER_NAME_SCOPING)) {
			if (levelName.equalsIgnoreCase(LEVEL_TRACE)) {
				SCOPING.setLevel(Level.TRACE);
			} else if (levelName.equalsIgnoreCase(LEVEL_DEBUG)) {
				SCOPING.setLevel(Level.DEBUG);
			} else if (levelName.equalsIgnoreCase(LEVEL_INFO)) {
				SCOPING.setLevel(Level.INFO);
			} else if (levelName.equalsIgnoreCase(LEVEL_WARN)) {
				SCOPING.setLevel(Level.WARN);
			} else if (levelName.equalsIgnoreCase(LEVEL_ERROR)) {
				SCOPING.setLevel(Level.ERROR);
			} else if (levelName.equalsIgnoreCase(LEVEL_FATAL)) {
				SCOPING.setLevel(Level.FATAL);
			}
		} else if (loggerName.equalsIgnoreCase(LOGGER_NAME_SCRIPTING)) {
			if (levelName.equalsIgnoreCase(LEVEL_TRACE)) {
				SCRIPTING.setLevel(Level.TRACE);
			} else if (levelName.equalsIgnoreCase(LEVEL_DEBUG)) {
				SCRIPTING.setLevel(Level.DEBUG);
			} else if (levelName.equalsIgnoreCase(LEVEL_INFO)) {
				SCRIPTING.setLevel(Level.INFO);
			} else if (levelName.equalsIgnoreCase(LEVEL_WARN)) {
				SCRIPTING.setLevel(Level.WARN);
			} else if (levelName.equalsIgnoreCase(LEVEL_ERROR)) {
				SCRIPTING.setLevel(Level.ERROR);
			} else if (levelName.equalsIgnoreCase(LEVEL_FATAL)) {
				SCRIPTING.setLevel(Level.FATAL);
			}
		} else if (loggerName.equalsIgnoreCase(LOGGER_NAME_PERFORMANCE)) {
			if (levelName.equalsIgnoreCase(LEVEL_PERF)) {
				PERFORMANCE.setLevel(PerfLevel.PERF);
			} else{
				PERFORMANCE.setLevel(PerfLevel.OFF);
			}
		}
	}

	public static String printStackTrace(StackTraceElement[] stack) {
		StringBuffer result = new StringBuffer();
		for (int i = 0; i < stack.length; i++) {
			result.append(stack[i]).append("\n");
		}
		return result.toString();
	}
}
