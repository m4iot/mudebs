/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2017 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */
package mudebs.common.algorithms.overlaymanagement;

import java.net.URI;

import mudebs.common.algorithms.AbstractMessageContent;

/**
 * This class defines the content of a message of the termination algorithm.
 * 
 * @author Denis Conan
 * 
 */
public class TerminationMsgContent extends AbstractMessageContent {
	/**
	 * the identity of the forwarder.
	 */
	private URI forwarder;
	/**
	 * the boolean stating whether the message must be forwarded to clients.
	 */
	private boolean forwardToClients;

	@SuppressWarnings("unused")
	private TerminationMsgContent() {
	} // for use in OSGi in Felix

	/**
	 * Constructor of the message content.
	 * 
	 * @param forwarder
	 *            the URI of the forwarder.
	 * @param fToClients
	 *            the boolean stating whether the message must be forwarded to
	 *            clients.
	 */
	public TerminationMsgContent(final URI forwarder,
			final boolean fToClients) {
		this.forwarder = forwarder;
		this.forwardToClients = fToClients;
	}

	/**
	 * gets the URI of the forwarder.
	 * 
	 * @return the URI of the forwarder.
	 */
	public URI getForwarder() {
		return forwarder;
	}

	/**
	 * gets the boolean stating whether the message must be forwarded to
	 * clients.
	 * 
	 * @return the boolean stating whether the message must be forwarded to
	 *         clients.
	 */
	public boolean getForwardToClients() {
		return forwardToClients;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TerminationMsgContent [forwarder=" + forwarder
				+ ", forwardToClients=" + forwardToClients + "]";
	}

	/**
	 * gets a short version of the message in a string.
	 * 
	 * @return a short description of the message.
	 */
	public String toStringBrief() {
		return toString();
	}
}
