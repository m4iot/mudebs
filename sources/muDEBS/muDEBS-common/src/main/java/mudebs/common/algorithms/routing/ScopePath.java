/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2017 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Léon Lim
Contributor(s): Denis Conan
 */
package mudebs.common.algorithms.routing;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import mudebs.common.MuDEBSException;

/**
 * This class defines a scope path. A scope pat is a sequence of scopes such
 * that two consecutive scopes in the path are either identical or are related
 * by the relation super-scope.
 * 
 * @author Léon Lim
 * @author Denis Conan
 */
public class ScopePath {
	/**
	 * the dimension of the path.
	 */
	protected final URI dimension;

	/**
	 * the sequence of scopes.
	 */
	private List<Scope> scopeSequence;

	/**
	 * constructor of the scope path.
	 * 
	 * @param d
	 *            the uri of the dimension of the scope path.
	 */
	public ScopePath(final URI d) {
		if (d == null) {
			throw new IllegalArgumentException("uri of the dimension is null");
		}
		dimension = d;
		this.scopeSequence = new ArrayList<Scope>();
	}

	/**
	 * gets the sequence of scopes.
	 * 
	 * @return the list of scopes of the path.
	 */
	public List<Scope> scopeSequence() {
		return scopeSequence;
	}

	/**
	 * gets the dimension of the scope path.
	 * 
	 * @return the URI of the dimension.
	 */
	public URI dimension() {
		return dimension;
	}

	/**
	 * appends the scope <tt>toAdd</tt> to the end of the sequence of
	 * scopes.
	 * 
	 * @param toAdd
	 *            the scope to add.
	 * @return the object after the addition.
	 * @throws MuDEBSException
	 *             the exception thrown when there is no relation between
	 *             the scope to add and the last scope in the path.
	 */
	public ScopePath addScope(final Scope toAdd) throws MuDEBSException {
		if (toAdd == null) {
			throw new IllegalArgumentException("scope to add is null");
		}
		if (!toAdd.dimension.equals(dimension)) {
			throw new MuDEBSException(
					"dimension of the scope to add is different from the "
							+ "dimension of the scope path");
		}
		if (scopeSequence.isEmpty()) {
			scopeSequence.add(toAdd);
		} else if (scopeSequence.contains(toAdd)) {
			throw new MuDEBSException("scope to add already exists in the path");
		} else if (!toAdd.subScopes().contains(
				scopeSequence.get(scopeSequence.size() - 1))
				&& !toAdd.superScopes().contains(
						scopeSequence.get(scopeSequence.size() - 1))) {
			throw new MuDEBSException(
					"scope to add is neither a super-scope nor a sub-scope "
							+ "of the last scope in the path");
		} else {
			scopeSequence.add(toAdd);
		}
		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((dimension == null) ? 0 : dimension.hashCode());
		result = prime * result
				+ ((scopeSequence == null) ? 0 : scopeSequence.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ScopePath)) {
			return false;
		}
		ScopePath other = (ScopePath) obj;
		if (dimension == null) {
			if (other.dimension != null) {
				return false;
			}
		} else if (!dimension.equals(other.dimension)) {
			return false;
		}
		if (scopeSequence.size() != other.scopeSequence.size()) {
			return false;
		} else {
			for (int i = 0; i < scopeSequence.size(); i++) {
				if (!scopeSequence.get(i).equals(other.scopeSequence.get(i))) {
					return false;
				}
			}
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		if (this.scopeSequence.isEmpty()) {
			return "()";
		}
		if (this.scopeSequence.size() == 1) {
			return "(" + scopeSequence.get(0).identifier() + ")";
		}
		String result = "(";
		for (int i = 0; i < scopeSequence.size() - 1; i++) {
			result = result + scopeSequence.get(i).identifier() + ", ";
		}
		result = result
				+ scopeSequence.get(scopeSequence.size() - 1).identifier();
		return result + ")";
	}
}
