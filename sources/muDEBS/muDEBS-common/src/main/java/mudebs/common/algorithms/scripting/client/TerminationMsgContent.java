/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
*/
package mudebs.common.algorithms.scripting.client;

import java.net.URI;

import mudebs.common.algorithms.AbstractMessageContent;

/**
 * This class defines the content of a termination message.
 * 
 * @author Denis Conan
 *
 */
public class TerminationMsgContent extends AbstractMessageContent {
	private URI sender;
	
    @SuppressWarnings("unused")
	private TerminationMsgContent() {} // for use in OSGi Felix

	
	public TerminationMsgContent(final URI s) {
		sender = s;
	}

	public URI getSender() {
		return sender;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TerminationMsgContent [sender=" + sender + "]";
	}

	/**
	 * gets a short version of the message in a string.
	 * 
	 * @return a short description of the message.
	 */
	public String toStringBrief() {
		return this.toString();
	}
}
