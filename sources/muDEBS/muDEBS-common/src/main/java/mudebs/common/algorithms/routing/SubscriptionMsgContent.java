/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */
package mudebs.common.algorithms.routing;

import java.net.URI;

import mudebs.common.algorithms.ACK;
import mudebs.common.algorithms.AbstractMessageContent;
import mudebs.common.algorithms.OperationalMode;

/**
 * This class defines the content of a subscription message sent by a client.
 * 
 * @author Denis Conan
 * 
 */
public class SubscriptionMsgContent extends AbstractMessageContent {
	private URI client;
	private String filterId;
	private int seqNumber;
	private OperationalMode opMode;
	private ACK ack;
	private String routingFilter;
	private MultiScopingSpecification phiSub;
	private ABACInformation abacInfo;

	protected SubscriptionMsgContent() {
	} // for use in OSGi Felix

	public SubscriptionMsgContent(final URI client, final String id, final int seq,
			final OperationalMode mode, final ACK a, final String f,
			final MultiScopingSpecification phiSub,
			final ABACInformation abac) {
		this.client = client;
		filterId = id;
		seqNumber = seq;
		opMode = mode;
		ack = a;
		routingFilter = f;
		this.phiSub = phiSub;
		abacInfo = abac;
	}

	public URI getClient() {
		return client;
	}

	public String getFilterId() {
		return filterId;
	}

	public int getSeqNumber() {
		return seqNumber;
	}

	public OperationalMode getOperationalMode() {
		return opMode;
	}

	public ACK getAck() {
		return ack;
	}

	public String getRoutingFilter() {
		return routingFilter;
	}

	public MultiScopingSpecification getPhiSub() {
		return phiSub;
	}

	public ABACInformation getABACInformation() {
		return abacInfo;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SubscriptionMsgContent [sender=" + client + ", identifier="
				+ filterId + ", seqNumber=" + seqNumber + ", opMode=" + opMode
				+ ", ack=" + ack + ", routingFilter=" + routingFilter
				+ ", phiSub=" + phiSub + ", abacInfo=" + abacInfo + "]";
	}

	/**
	 * gets a short version of the message in a string.
	 * 
	 * @return a short description of the message.
	 */
	public String toStringBrief() {
		return "SubscriptionMsgContent [sender="
				+ client
				+ ", identifier="
				+ filterId
				+ ", seqNumber="
				+ seqNumber
				+ ", opMode="
				+ opMode
				+ ", ack="
				+ ack
				+ ", routingFilter="
				+ ((routingFilter.length() < 10) ? routingFilter
						: routingFilter.substring(0, 10) + "...") + ", phiSub="
				+ phiSub + ", abacInfo=" + abacInfo + "]";
	}
}
