/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2017 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform.  If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Léon Lim
 */
package mudebs.common.filters;

/**
 * This class defines a muDEBS XACML policy.
 * 
 * @author Denis Conan
 * @author Léon Lim
 */
public class XACMLPolicy {
	/**
	 * the XML String that represents an XACML policy .
	 */
	private String policy;

	public XACMLPolicy(String p) {
		policy = p;
	}

	/**
	 * gets the policy in XML String
	 * 
	 * @return the policy.
	 */
	public String getThePolicy() {
		return policy;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "XACMLPolicy [policy=" + policy + "]";
	}
}
