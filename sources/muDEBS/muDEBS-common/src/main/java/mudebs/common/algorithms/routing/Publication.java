/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */
package mudebs.common.algorithms.routing;

import java.util.List;

/**
 * This class defines the content of a publication.
 * 
 * @author Denis Conan
 * 
 */
public class Publication extends AdminPublication {
	public Publication(final String idAdv, final int seqNumber,
			final String content) {
		super(idAdv, false, seqNumber, null, content, null);
	}

	public boolean hasGotPrivacyRequirement() {
		throw new UnsupportedOperationException("Privacy requirement unknown");
	}

	public List<String> getSubsEnabledAC() {
		throw new UnsupportedOperationException("List of subscriptions"
				+ " with enabled access control unknown");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((getIdAdv() == null) ? 0 : getIdAdv().hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Publication)) {
			return false;
		}
		Publication other = (Publication) obj;
		if (getIdAdv() == null) {
			if (other.getIdAdv() != null) {
				return false;
			}
		} else if (!getIdAdv().equals(other.getIdAdv())) {
			return false;
		}
		return true;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Publication [idAdv=" + getIdAdv() + ", sequenceNumber="
				+ getSequenceNumber() + ", phiNotif=" + getPhiNotif()
				+ ", content=" + getContent() + "]";
	}

	/**
	 * gets a short version of the message in a string.
	 * 
	 * @return a short description of the message.
	 */
	public String toStringBrief() {
		return "Publication [idAdv="
				+ getIdAdv()
				+ ", sequenceNumber="
				+ getSequenceNumber()
				+ ", phiNotif="
				+ getPhiNotif()
				+ ", content="
				+ ((getContent().length() < 10) ? getContent() : getContent()
						.substring(0, 10) + "...") + "]";
	}
}
