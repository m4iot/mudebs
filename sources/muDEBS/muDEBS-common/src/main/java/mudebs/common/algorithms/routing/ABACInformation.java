/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */
package mudebs.common.algorithms.routing;

import java.util.ArrayList;

/**
 * This class defines the content of ABAC (Attribute-Based Access Control)
 * information.
 * 
 * @author Denis Conan
 * 
 */
public class ABACInformation {
	/**
	 * the list of attributes.
	 */
	private ArrayList<ABACAttribute> listOfAttributes;

	@SuppressWarnings("unused")
	private ABACInformation() {
		super();
	} // for use in OSGi Felix

	public ABACInformation(final ArrayList<ABACAttribute> l) {
		listOfAttributes = l;
	}

	public ArrayList<ABACAttribute> getListOfAttributes() {
		return listOfAttributes;
	}

	public void setListOfAttributes(ArrayList<ABACAttribute> listOfAttributes) {
		this.listOfAttributes = listOfAttributes;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ABACInformation [listOfAttributes="
				+ prettyPrintAttributes(listOfAttributes) + "]";
	}

	public String prettyPrintAttributes(ArrayList<ABACAttribute> list) {
		String result = "";
		if (list != null && !list.isEmpty()) {
			for (ABACAttribute att : list) {
				result += "(";
				result += att.getCategory();
				result += ",";
				result += att.getIdentifier();
				result += ",";
				result += att.getValue();
				result += ")";
				if (list.indexOf(att) != (list.size() - 1)) {
					result += ";";
				}
			}
		}
		return result;
	}
}
