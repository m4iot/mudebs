/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2017 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Léon Lim
Contributor(s): Denis Conan
 */
package mudebs.common;

import org.apache.log4j.Level;

/**
 * This class defines the level for the performance evaluation.
 * 
 * @author Léon Lim
 * @author Denis Conan
 */
public class PerfLevel extends Level {
	/**
	 * the serial version ID.
	 */
	private static final long serialVersionUID = -1L;
	/**
	 * the default value of the performance level.
	 */
	private static final int PERF_LEVEL_INT = TRACE_INT - 10;
	/**
	 * the default value for the level <tt>PERF</tt>.
	 */
	public final static String PERF_LEVEL_NAME = "perf";
	/**
	 * the performance log level.
	 */
	public static final Level PERF = new PerfLevel(PERF_LEVEL_INT,
			PERF_LEVEL_NAME, 7);

	/**
	 * the constructor.
	 * 
	 * @param level
	 *            the level.
	 * @param levelStr
	 *            the name of the level.
	 * @param syslogEquivalent
	 *            the corresponding {@link PerfLevel#PERF_LEVEL_INT}.
	 */
	public PerfLevel(int level, String levelStr, int syslogEquivalent) {
		super(level, levelStr, syslogEquivalent);
	}

	/**
	 * checks whether <code>sArg</code> is equal to <tt>PERF_LEVEL_NAME</tt>. If
	 * so, it returns {@link PerfLevel#PERF}. Else it calls
	 * {@link PerfLevel#toLevel(String, Level)} with {@link Level#DEBUG} as the
	 * defaultLevel.
	 * 
	 * @see Level#toLevel(java.lang.String)
	 * 
	 * @param sArg
	 *            the level.
	 * @return the corresponding {@link Level}.
	 */
	public static Level toLevel(String sArg) {
		if (sArg != null && sArg.toUpperCase().equals(PERF_LEVEL_NAME)) {
			return PERF;
		}
		return (Level) toLevel(sArg, Level.DEBUG);
	}

	/**
	 * checks whether <code>val</code> is equal to
	 * {@link PerfLevel#PERF_LEVEL_INT}. If so, it returns
	 * {@link PerfLevel#PERF}. Else it calls
	 * {@link PerfLevel#toLevel(int, Level)} with {@link Level#DEBUG} as the
	 * defaultLevel.
	 * 
	 * @see Level#toLevel(int)
	 * 
	 * @param val the integer of the initial level.
	 * @return the corresponding {@link Level}.
	 */
	public static Level toLevel(int val) {
		if (val == PERF_LEVEL_INT) {
			return PERF;
		}
		return (Level) toLevel(val, Level.DEBUG);
	}

	/**
	 * checks whether <code>val</code> is equal to
	 * {@link PerfLevel#PERF_LEVEL_INT}. If so, it returns
	 * {@link PerfLevel#PERF}. Else it calls
	 * {@link Level#toLevel(int, org.apache.log4j.Level)}.
	 * 
	 * @see Level#toLevel(int, org.apache.log4j.Level)
	 * 
	 * @param val the integer of the initial level.
	 * @param defaultLevel the default {@link Level} for the translation.
	 * @return the corresponding {@link Level}.
	 */
	public static Level toLevel(int val, Level defaultLevel) {
		if (val == PERF_LEVEL_INT) {
			return PERF;
		}
		return Level.toLevel(val, defaultLevel);
	}

	/**
	 * checks whether <code>sArg</code> is equal to <tt>PERF_LEVEL_NAME</tt>. If
	 * so, it returns {@link PerfLevel#PERF}. Else it calls
	 * {@link Level#toLevel(java.lang.String, org.apache.log4j.Level)}.
	 * 
	 * @see Level#toLevel(java.lang.String, org.apache.log4j.Level)
	 * 
	 * @param val the integer of the initial level.
	 * @param defaultLevel the default {@link Level} for the translation.
	 * @return the corresponding {@link Level}.
	 */
	public static Level toLevel(String val, Level defaultLevel) {
		if (val != null && val.toUpperCase().equals(PERF_LEVEL_NAME)) {
			return PERF;
		}
		return Level.toLevel(val, defaultLevel);
	}
}
