/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */
package mudebs.common.algorithms.routing;

import java.net.URI;

import mudebs.common.algorithms.ACK;
import mudebs.common.algorithms.AbstractMessageContent;
import mudebs.common.algorithms.OperationalMode;

/**
 * This class defines the content of an advertisement.
 * 
 * @author Denis Conan
 * 
 */
public class AdvertisementMsgContent extends AbstractMessageContent {
	private URI client;
	private String identifier;
	private int seqNumber;
	private OperationalMode opMode;
	private ACK ack;
	private String routingFilter;
	private MultiScopingSpecification phiNotif;
	private String policyContent;

	@SuppressWarnings("unused")
	private AdvertisementMsgContent() {
	} // for use in OSGi Felix

	public AdvertisementMsgContent(final URI s, final String id, final int seq,
			final OperationalMode mode, final ACK a, final String f,
			final MultiScopingSpecification phiNotif, final String p) {
		client = s;
		identifier = id;
		seqNumber = seq;
		opMode = mode;
		ack = a;
		routingFilter = f;
		this.phiNotif = phiNotif;
		policyContent = p;
	}

	public URI getClient() {
		return client;
	}

	public String getIdentifier() {
		return identifier;
	}

	public int getSeqNumber() {
		return seqNumber;
	}

	public OperationalMode getOperationalMode() {
		return opMode;
	}

	public ACK getAck() {
		return ack;
	}

	public String getRoutingFilter() {
		return routingFilter;
	}

	public String getPolicyContent() {
		return policyContent;
	}

	public MultiScopingSpecification getPhiNotif() {
		return phiNotif;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AdvertisementMsgContent [client=" + client + ", identifier="
				+ identifier + ", seqNumber=" + seqNumber + ", opMpde="
				+ opMode + ", ack=" + ack + ", routingFilter=" + routingFilter
				+ ", phiNotif=" + phiNotif + ", policyContent=" + policyContent + "]";
	}

	/**
	 * gets a short version of the message in a string.
	 * 
	 * @return a short description of the message.
	 */
	public String toStringBrief() {
		return "AdvertisementMsgContent [client="
				+ client
				+ ", identifier="
				+ identifier
				+ ", seqNumber="
				+ seqNumber
				+ ", opMode="
				+ opMode
				+ ", ack="
				+ ack
				+ ", routingFilter="
				+ ((routingFilter.length() < 10) ? routingFilter
						: routingFilter.substring(0, 10) + "...")
				+ ", phiNotif=" + phiNotif + ", policyContent=" + policyContent + "]";
	}
}
