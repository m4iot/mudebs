/**
This file is part of the muDEBS middleware.

Copyright (C) 2013-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */
package mudebs.common.algorithms.routing;

import java.net.URI;

import mudebs.common.algorithms.ACK;
import mudebs.common.algorithms.AbstractMessageContent;

/**
 * This class defines the content of an un-subscription.
 * 
 * @author Denis Conan
 * 
 */
public class UnsubscriptionMsgContent extends AbstractMessageContent {
	private URI client;
	private String identifier;
	private int seqNumber;
	private ACK ack;

	@SuppressWarnings("unused")
	private UnsubscriptionMsgContent() {
	} // for use in OSGi Felix

	public UnsubscriptionMsgContent(final URI s, final String id,
			final int seq, final ACK a) {
		client = s;
		identifier = id;
		seqNumber = seq;
		ack = a;
	}

	public URI getClient() {
		return client;
	}

	public String getIdentifier() {
		return identifier;
	}

	public int getSeqNumber() {
		return seqNumber;
	}

	public ACK getAck() {
		return ack;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UnsubscriptionMsgContent [client=" + client + ", identifier="
				+ identifier + ", seqNumber=" + seqNumber + ", ack=" + ack
				+ "]";
	}

	/**
	 * gets a short version of the message in a string.
	 * 
	 * @return a short description of the message.
	 */
	public String toStringBrief() {
		return this.toString();
	}
}
