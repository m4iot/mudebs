muDEBS platform

Copyright (C) 2013-2015
Contact: Denis.Conan[at]telecom-sudparis.eu

This product includes software developed at Institut Mines Télécom /
Télécom SudParis, except for the maven modules named xacml-unknown
(see the copyrights in the Java class files).

License: See file LICENSE.txt

================================================================================

Content of this file :
- Content of this directory
- Software prerequisites
- Shell variables to set
- Compilation and installation
- Execution
- Miscellaneous

NB: shell commands of this file are prepared to be executed
    in this working directory.

================================================================================

Content of this directory:
--------------------------
- muDEBS-amqp-gateway: Maven module of the gateway to topic-based DEBS
- muDEBS-broker: Maven module of the broker
- muDEBS-client: Maven module of the generic client
- muDEBS-common: Maven module of the common classes
- muDEBS-communication-javanio: Maven module of the Java NIO communication
- muDEBS-perf-client: Maven module of clients for performance evaluation
- muDEBS-scripting: Maven module of the entity to script the brokers/clients
- pom.xml: the POM of this Maven root module
- README.txt: this file
- scripts: the commands of muDEBS

Software prerequisites:
-----------------------
("opt" is for "optional" for Maven installation):
	1. Java Version 6.0
	   (http://www.oracle.com/technetwork/java/javase/overview/index.html)
	2. Maven Version 3.0.4
	   (http://maven.apache.org/)
opt	3. Eclipse Version 4.3 Kepler
	   (http://eclipse.org/)
opt	4. RabbitMQ Version rabbitmq_server-3.1.5
	   (http://www.rabbitmq.com/install-generic-unix.html)
	6. Unix shell
	   - The scripts have been tested on GNU/Linux operating systems,
	     more especially on Debian distribution, Version Jessie

Shell variables to set:
-----------------------
	1. JAVA_HOME to your Java SDK 6.0
$
export JAVA_HOME=<the root directory of your Java installation>
echo $JAVA_HOME
$
	2. RMQ_HOME to your rabbitmq_server-3.1.5
$
export RMQ_HOME=<the root directory of your RabbitMQ installation>
PATH=$PATH:${RMQ_HOME}/rabbitmq_server-3.1.5/sbin
export RABBITMQ_MNESIA_BASE=${RMQ_HOME}/rabbitmq_server-3.1.5/mnesia
export RABBITMQ_LOG_BASE=${RMQ_HOME}/rabbitmq_server-3.1.5/log
$

Compilation and installation:
-----------------------------
	To compile and install the modules, execute the following command.
	Use the options -Dmaven.test.skip=true and -Dmaven.javadoc.skip=true
	if you do not want to execute the JUnit tests or to generate the
	JavaDoc files, so that the compilation is much more rapid.
$
mvn install
$

Execution:
----------
	To execute the somewhat dummy example of muDEBS, execute the command:
$
(cd frameworks/muDEBS ; ./run.sh)
$

Miscellaneous:
--------------
