<!--
This file is part of the muDEBS platform.

The muDEBS platform is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the muDEBS platform.  If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Léon Lim
-->
<project xmlns="http://maven.apache.org/POM/4.0.0" 
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0
                      http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <groupId>muDEBS</groupId>
  <artifactId>root</artifactId>
  <name>The multiscale distributed event-based system muDEBS</name>
  <packaging>pom</packaging>
  <version>0.17.3</version>

  <description>
    The multiscale distributed event-based system muDEBS.
  </description>

  <licenses>
    <license>
      <name>LGPL</name>
      <url>http://www.gnu.org/licenses/lgpl</url>
    </license>
  </licenses>

  <organization>
    <name>Institut Mines-Télécom, Télécom Sudparis; UMR CNRS SAMOVAR/ACMES</name>
    <url>http://telecom-sudparis.eu/en_accueil.html</url>
  </organization>

  <inceptionYear>2013</inceptionYear>
  <developers>
    <developer>
      <id>conan</id>
      <name>Denis Conan</name>
      <email>Denis.Conan[at]telecom-sudparis.eu</email>
      <url>http://www-public.telecom-sudparis.eu/~conan</url>
      <organization>Institut Mines Télécom, Télécom SudParis</organization>
      <organizationUrl>http://www.telecom-sudparis.eu</organizationUrl>
      <roles>
        <role>Architect</role>
        <role>Developer</role>
      </roles>
    </developer>
    <developer>
      <id>lim</id>
      <name>Léon Lim</name>
      <email>Leon.Lim[at]telecom-sudparis.eu</email>
      <url>http://www-public.telecom-sudparis.eu/~lim_leon</url>
      <organization>Institut Mines Télécom, Télécom SudParis</organization>
      <organizationUrl>http://www.telecom-sudparis.eu</organizationUrl>
      <roles>
        <role>Architect</role>
        <role>Developer</role>
      </roles>
    </developer>
  </developers>
  <contributors>
    <contributor>
      <name>Christian Bac</name>
      <email>Christian.Bac[at]telecom-sudparis.eu</email>
      <organization>Institut Mines Télécom, Télécom SudParis</organization>
      <organizationUrl>http://www.telecom-sudparis.eu</organizationUrl>
    </contributor>
    <contributor>
      <name>Jing ZHU</name>
      <email>zjing.xd[at]gmail.com</email>
      <organization>ENSIIE student</organization>
    </contributor>
    <contributor>
      <name>Xuan LI</name>
      <email>lixuanvc[at]gmail.com</email>
      <organization>ENSIIE student</organization>
    </contributor>
  </contributors>

  <properties>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <deploy.dir>/tmp/muDEBS-deploy</deploy.dir>
  </properties>

  <repositories>
    <repository>
      <id>balana-repository</id>
      <name>Balana Repository</name>
      <url>http://dist.wso2.org/maven2</url>
      <layout>default</layout>
    </repository>
  </repositories>

  <dependencyManagement>
    <dependencies>
      <dependency>
        <groupId>commons-logging</groupId>
        <artifactId>commons-logging</artifactId>
        <version>1.1.1</version>
      </dependency>
      <!-- surefire -->
      <dependency>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-plugin</artifactId>
        <version>2.15</version>
      </dependency>
      <!-- junit -->
      <dependency>
        <groupId>junit</groupId>
        <artifactId>junit</artifactId>
        <version>4.10</version>
        <scope>test</scope>
      </dependency>
      <!-- javadoc -->
      <dependency>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-javadoc-plugin</artifactId>
        <version>2.9.1</version>
      </dependency>
      <!-- source packaging configuration -->
      <dependency>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-source-plugin</artifactId>
        <version>2.2.1</version>
      </dependency>
      <!-- site -->
      <dependency>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-site-plugin</artifactId>
        <version>3.3</version>
      </dependency>
    </dependencies>
  </dependencyManagement>

  <dependencies>
    <dependency>
      <groupId>org.wso2.balana</groupId>
      <artifactId>org.wso2.balana</artifactId>
      <version>1.0.0-wso2v7</version>
    </dependency>
    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>4.11</version>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>log4j</groupId>
      <artifactId>log4j</artifactId>
      <version>1.2.17</version>
    </dependency>
    <dependency>
      <groupId>com.google.code.gson</groupId>
      <artifactId>gson</artifactId>
      <version>2.3.1</version>
      <scope>compile</scope>
    </dependency>
  </dependencies>

  <build>
    <pluginManagement>
      <plugins>
        <!-- java compiler -->
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-compiler-plugin</artifactId>
          <version>2.3.2</version>
        </plugin>
        <!-- jar packaging -->
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-jar-plugin</artifactId>
          <version>2.4</version>
        </plugin>
        <!-- source packaging configuration -->
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-source-plugin</artifactId>
          <version>2.2.1</version>
        </plugin>
        <!-- javadoc packaging in jar configuration -->
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-javadoc-plugin</artifactId>
          <version>2.9.1</version>
        </plugin>
        <!-- surefire -->
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-surefire-plugin</artifactId>
          <version>2.15</version>
        </plugin>
      </plugins>
    </pluginManagement>

    <plugins>
      <!-- java compiler configuration -->
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-compiler-plugin</artifactId>
        <configuration>
          <source>1.8</source>
          <target>1.8</target>
          <encoding>UTF-8</encoding>
        </configuration>
      </plugin>
      <!-- source packaging configuration -->
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-source-plugin</artifactId>
        <executions>
          <execution>
            <phase>package</phase>
            <goals>
              <goal>jar</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
      <!-- javadoc packaging in jar configuration -->
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-javadoc-plugin</artifactId>
        <executions>
          <execution>
            <id>attach-javadocs</id>
            <phase>package</phase>
            <goals>
              <goal>jar</goal>
            </goals>
            <configuration>
              <docfilessubdirs>true</docfilessubdirs>
              <excludedocfilessubdir>.svn</excludedocfilessubdir>
              <detectOfflineLinks>false</detectOfflineLinks>
              <detectJavaApiLink>false</detectJavaApiLink>
            </configuration>
          </execution>
        </executions>
      </plugin>
      <!-- surefire configuration -->
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-plugin</artifactId>
        <configuration>
          <includes>
            <include>
              **/Test*.java
            </include>
          </includes>
        </configuration>
      </plugin>
    </plugins>
  </build>

  <!-- deployment: mvn deploy -->
  <distributionManagement>
    <repository>
      <id>mudebs</id>
      <name>muDEBS's release repository</name>
      <url>file:///${deploy.dir}/maven-repository/release</url>
    </repository>
    <snapshotRepository>
      <id>mudebs</id>
      <name>muDEBS's snapshot repository</name>
      <url>file:///${deploy.dir}/maven-repository/snapshot</url>
    </snapshotRepository>
    <site>
      <id>mudebs</id>
      <name>muDEBS's Maven site</name>
      <url>file:///${deploy.dir}/maven-repository/site</url>
    </site>
  </distributionManagement>

  <modules>
    <module>muDEBS-common</module>
    <module>muDEBS-communication-javanio</module>
    <module>muDEBS-broker</module>
    <module>muDEBS-client</module>
    <module>muDEBS-scripting</module>
    <module>muDEBS-amqp-gateway</module>
    <module>muDEBS-perf-client</module>
  </modules>
</project>
