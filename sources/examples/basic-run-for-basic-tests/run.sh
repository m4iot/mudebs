#!/bin/bash

MUDEBS_HOME_SCRIPTS="../../muDEBS/scripts"

echo 'Architecture of the test'
echo ''
echo ' ClientA     ClientB     ClientC'
echo '    |           |           |'
echo '    |           |           |'
echo '    |           |           |'
echo ' BrokerA --- BrokerB --- BrokerC'
echo '                \           /'
echo '                 \         /'
echo '                  \       /'
echo '       ClientF --- BrokerD --- ClientD'
echo '                      |'
echo '                      |'
echo '                      |'
echo '                   BrokerE --- ClientE'
echo '                      |'
echo '                      |'
echo '                      |'
echo '                   BrokerF --- ClientG'
echo '                      |'
echo '                      |'
echo '                      |'
echo '                   BrokerH --- ClientH'
echo ''
echo ''

part0=no
part1=no
part2=no
part3=no
part4=no
part5=no
part6=no
part7=no
part8=no
ARGS="$@"
while [ $# -gt 0 ] ; do
	case "$1" in
		0) part0=yes; echo 'Part ZERO is going to be executed' ;;
		1) part1=yes; echo 'Part ONE is going to be executed' ;;
		2) part2=yes; echo 'Part TWO is going to be executed' ;;
		3) part3=yes; echo 'Part THREE is going to be executed' ;;
		4) part4=yes; echo 'Part FOUR is going to be executed' ;;
		5) part5=yes; echo 'Part FIVE is going to be executed' ;;
		6) part6=yes; echo 'Part SIX is going to be executed' ;;
		7) part7=yes; echo 'Part SEVEN is going to be executed' ;;
		8) part8=yes; echo 'Part EIGHT is going to be executed' ;;
	esac
	shift
done

if [ $part0 = no -a $part1 = no -a $part2 = no -a $part3 = no -a $part4 = no -a $part5 = no -a $part6 = no -a $part7 = no -a $part8 = no ]
then
    echo 'usage: run.sh [parts to execute, being from 1 to 8]...'
    echo 'example: run.sh 0 1 2 3 4 5 6 7 8'
    echo '         0 = loggers'
    echo '         1 = un/advertising, un/subscribing, and publishing'
    echo '         2 = global advertisements and local subscriptions'
    echo '         3 = local acknowledgements'
    echo '         4 = global acknowledgements'
    echo '         5 = synchronous requests with multiple responses'
    echo '         6 = attribute-based access control with XACML'
    echo '         7 = scoping'
    echo '         8 = with performance measurements'
    exit -1
fi

echo
echo "Hit return when you want to start the brokers"
read x
echo "Starting BrokerA"
${MUDEBS_HOME_SCRIPTS}/startbroker --uri mudebs://localhost:2000/BrokerA --log PERFORMANCE.PERF --perfconfigfile ./filters_and_properties/performance.config #--log ROUTING.INFO
#sleep 1
echo "Starting BrokerB"
${MUDEBS_HOME_SCRIPTS}/startbroker --uri mudebs://localhost:2001/BrokerB --log PERFORMANCE.PERF --perfconfigfile ./filters_and_properties/performance.config #--log ROUTING.INFO
#sleep 1
echo "Starting BrokerC"
${MUDEBS_HOME_SCRIPTS}/startbroker --uri mudebs://localhost:2002/BrokerC --log PERFORMANCE.PERF --perfconfigfile ./filters_and_properties/performance.config #--log ROUTING.INFO
echo "Starting BrokerD"
${MUDEBS_HOME_SCRIPTS}/startbroker --uri mudebs://localhost:2003/BrokerD --log PERFORMANCE.PERF --perfconfigfile ./filters_and_properties/performance.config #--log ROUTING.INFO
echo "Starting BrokerE"
${MUDEBS_HOME_SCRIPTS}/startbroker --uri mudebs://localhost:2004/BrokerE --log PERFORMANCE.PERF --perfconfigfile ./filters_and_properties/performance.config #--log ROUTING.INFO
echo "Starting BrokerF"
${MUDEBS_HOME_SCRIPTS}/startbroker --uri mudebs://localhost:2005/BrokerF --log PERFORMANCE.PERF --perfconfigfile ./filters_and_properties/performance.config #--log ROUTING.INFO

echo "Broker B connects to BrokerA"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2001/BrokerB --command connect --neigh mudebs://localhost:2000/BrokerA
echo "Broker C connects to BrokerB"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2002/BrokerC --command connect --neigh mudebs://localhost:2001/BrokerB
echo "Broker D connects to BrokerB"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2003/BrokerD --command connect --neigh mudebs://localhost:2001/BrokerB
echo "Broker D connects to BrokerC"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2003/BrokerD --command connect --neigh mudebs://localhost:2002/BrokerC
echo "Broker E connects to BrokerD"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2004/BrokerE --command connect --neigh mudebs://localhost:2003/BrokerD
echo "Broker F connects to BrokerE"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2005/BrokerF --command connect --neigh mudebs://localhost:2004/BrokerE

echo
echo "Starting ClientA connected to BrokerA"
${MUDEBS_HOME_SCRIPTS}/startclient --uri mudebs://localhost:2100/ClientA --broker mudebs://localhost:2000/BrokerA #--log SCRIPTING.TRACE
echo "Starting ClientB connected to BrokerB"
${MUDEBS_HOME_SCRIPTS}/startclient --uri mudebs://localhost:2101/ClientB --broker mudebs://localhost:2001/BrokerB #--log SCRIPTING.TRACE
echo "Starting ClientC connected to BrokerC"
${MUDEBS_HOME_SCRIPTS}/startclient --uri mudebs://localhost:2102/ClientC --broker mudebs://localhost:2002/BrokerC #--log SCRIPTING.TRACE
echo "Starting ClientD connected to BrokerD"
${MUDEBS_HOME_SCRIPTS}/startclient --uri mudebs://localhost:2103/ClientD --broker mudebs://localhost:2003/BrokerD #--log SCRIPTING.TRACE
echo "Starting ClientE connected to BrokerE"
${MUDEBS_HOME_SCRIPTS}/startclient --uri mudebs://localhost:2104/ClientE --broker mudebs://localhost:2004/BrokerE #--log SCRIPTING.TRACE
echo "Starting ClientF connected to BrokerD"
${MUDEBS_HOME_SCRIPTS}/startclient --uri mudebs://localhost:2105/ClientF --broker mudebs://localhost:2003/BrokerD #--log SCRIPTING.TRACE
echo "Starting ClientG connected to BrokerF"
${MUDEBS_HOME_SCRIPTS}/startclient --uri mudebs://localhost:2106/ClientG --broker mudebs://localhost:2005/BrokerF #--log SCRIPTING.TRACE
echo "Starting ClientH with local BrokerH"
${MUDEBS_HOME_SCRIPTS}/startclientwithlocalbroker --uri mudebs://localhost:2107/ClientH --broker mudebs://localhost:2007/BrokerH --log PERFORMANCE.PERF --perfconfigfile ./filters_and_properties/performance.config #--log SCRIPTING.TRACE
echo "Broker H connects to BrokerF"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2007/BrokerH --command connect --neigh mudebs://localhost:2005/BrokerF

sleep 7

echo
echo "Hit return to start sending advertisements, publications, subscriptions, etc."
read x

if [ $part0 = yes ]
then

echo
echo
echo
echo PART ZERO
echo
echo
echo
echo BrokerA, setting logger, no logger name
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2000/BrokerA --command setlog --level trace
sleep 1
echo
echo BrokerA, setting logger, no logging level
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2000/BrokerA --command setlog --name routing
sleep 1
echo
echo Setting logger routing to trace for BrokerA
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2000/BrokerA --command setlog --name routing --level trace
sleep 1
echo
echo Setting logger routing to warn for BrokerA
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2000/BrokerA --command setlog --name routing --level warn
sleep 1

fi

if [ $part1 = yes ]
then

echo
echo
echo
echo PART ONE
echo
echo
echo
echo ClientA subscribing to filter \'first\' in \'./filters_and_properties/subscription.mudebs\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2100/ClientA --command subscribe --id 'first' --file './filters_and_properties/subscription.mudebs'
sleep 1
echo
echo ClientA advertising filter \'first\' in \'./filters_and_properties/advertisement.mudebs\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2100/ClientA --command advertise --id 'first' --file './filters_and_properties/advertisement.mudebs'
sleep 1
echo
echo ClientA publishing \'apublicationfromA\' with adv. filter \'first\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2100/ClientA --command publish --id 'first' --content '<foo>apublicationfromA</foo>'
sleep 1
echo
echo ClientB advertising filter \'first\' in \'./filters_and_properties/advertisement.mudebs\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2101/ClientB --command advertise --id 'first' --file './filters_and_properties/advertisement.mudebs'
sleep 1
echo
echo ClientB publishing \'apublicationfromB\' with adv. filter \'first\'
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2101/ClientB --command publish --id 'first' --content '<foo>apublicationfromB</foo>'
echo
sleep 1
echo
echo ClientC advertising filter \'first\' in \'./filters_and_properties/advertisement.mudebs\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2102/ClientC --command advertise --id 'first' --file './filters_and_properties/advertisement.mudebs'
sleep 1
echo
echo ClientC making an error in advertising filter \'first\' already registered
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2102/ClientC --command advertise --id 'first' --file './filters_and_properties/advertisement.mudebs'
sleep 1
echo
echo ClientC publishing \'apublicationfromC\' with adv. filter \'first\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2102/ClientC --command publish --id 'first' --content '<foo>apublicationfromC</foo>'
sleep 1
echo
echo ClientD making an error in advertising a filter with no id.
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2103/ClientD --command advertise --file './filters_and_properties/advertisement.mudebs'
sleep 1
echo
echo ClientD trying to publish with no success '(unknown filter)'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2103/ClientD --command publish --id 'first' --content '<foo>apublicationfromD</foo>'
sleep 1
echo
echo ClientE advertising filter \'first\' in \'./filters_and_properties/advertisement.mudebs\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2104/ClientE --command advertise --id 'first' --file './filters_and_properties/advertisement.mudebs'
sleep 1
echo
echo ClientE publishing \'apublicationfromE\' with adv. filter \'first\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2104/ClientE --command publish --id 'first' --content '<foo>apublicationfromE</foo>'
sleep 1
echo
echo ClientE unadvertising filter \'first\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2104/ClientE --command unadvertise --id 'first'
sleep 1
echo
echo ClientE trying to publish with unregistered adv. filter \'first\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2104/ClientE --command publish --id 'first' --content '<foo>apublicationfromE</foo>'
sleep 1
echo
echo ClientC subscribing to filter \'first\' in \'./filters_and_properties/subscription.mudebs\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2102/ClientC --command subscribe --id 'first' --file './filters_and_properties/subscription.mudebs'
sleep 1
echo
echo ClientB publishing 'apublicationfromB' with adv. filter \'first\'
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2101/ClientB --command publish --id 'first' --content '<foo>apublicationfromB</foo>'
sleep 1
# # echo
# # echo ClientC disconnecting from current broker and reconnectiong to new broker BrokerE
# # echo
# # ${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2102/ClientC --command dis-/re-connect --broker mudebs://localhost:2004/BrokerE
# # sleep 1
# # echo
# # echo ClientC publishing 'anotherpublicationbutfromE' with adv. filter \'filter\'
# # echo
# # ${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2102/ClientC --command publish --id 'first' --content '<foo>anotherpublicationbutfromE</foo>'
# # sleep 1
# # echo
# # echo ClientB publishing 'apublicationfromB' with adv. filter \'filter\'
# # ${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2101/ClientB --command publish --id 'first' --content '<foo>apublicationfromB</foo>'
# # echo
# # sleep 1
echo
echo ClientA unsubscribing the filter 'first'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2100/ClientA --command unsubscribe --id 'first'
sleep 1
echo
echo ClientC publishing, but ClientA does not receive because of unsubscription
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2102/ClientC --command publish --id 'first' --content '<foo>anotherpublicationfromC</foo>'
sleep 1
echo
echo ClientA subscribing the filter 'first' and 'second'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2100/ClientA --command subscribe --id 'first' --file './filters_and_properties/subscription.mudebs' --id 'second' --file './filters_and_properties/subscription.mudebs'
sleep 1
echo
echo ClientC publishing
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2102/ClientC --command publish --id 'first' --content '<foo>anotherpublicationfromC</foo>'
sleep 1
echo
echo ClientA advertising filter \'second\' in \'./filters_and_properties/advertisement.mudebs\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2100/ClientA --command advertise --id 'second' --file './filters_and_properties/advertisement.mudebs'
sleep 1
echo
echo ClientA publishing two publications
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2100/ClientA --command publish --id 'first' --content '<foo>apublicationfromA-first</foo>' --id 'second' --content '<foo>apublicationfromA-second</foo>'
sleep 1
echo
echo ClientH subscribing to filter \'first\' in \'./filters_and_properties/subscription.mudebs\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2107/ClientH --command subscribe --id 'first' --file './filters_and_properties/subscription.mudebs'
sleep 1
echo
echo ClientH advertising filter \'first\' in \'./filters_and_properties/advertisement.mudebs\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2107/ClientH --command advertise --id 'first' --file './filters_and_properties/advertisement.mudebs'
sleep 1
echo
echo ClientH publishing \'apublicationfromH\' with adv. filter \'first\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2107/ClientH --command publish --id 'first' --content '<foo>apublicationfromH</foo>'
sleep 1
echo
echo ClientC publishing
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2102/ClientC --command publish --id 'first' --content '<foo>anotherpublicationfromC</foo>'
sleep 1

fi

if [ $part2 = yes ]
then

echo
echo
echo PART TWO
echo
echo
echo
echo
sleep 1
echo ClientG subscribing '(local)' to filter \'second\' in \'./filters_and_properties/subscription.mudebs\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2106/ClientG --command subscribe_local --id 'second' --file './filters_and_properties/subscription.mudebs'
sleep 1
echo
echo ClientF advertising '(global)' filter \'second\' in \'./filters_and_properties/advertisement.mudebs\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2105/ClientF --command advertise_global --id 'second' --file './filters_and_properties/advertisement.mudebs'
sleep 1
echo
echo ClientF publishing \'apublicationfromF\' with adv. filter \'second\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2105/ClientF --command publish --id 'second' --content '<foo>apublicationfromF</foo>'
sleep 1
echo
echo ClientF unadvertising filter \'second\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2105/ClientF --command unadvertise --id 'second'
sleep 1
echo
echo ClientF trying to publish with adv. \'second\' after unadvertise
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2105/ClientF --command publish --id 'second' --content '<foo>anotherpublicationfromF</foo>'
sleep 1
echo
echo ClientG unsubscribing the filter \'second\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2106/ClientG --command unsubscribe --id 'second'
sleep 1

fi

if [ $part3 = yes ]
then

echo
echo
echo PART THREE
echo
echo
echo
echo
sleep 1
echo ClientC advertising \(local\) filter \'third\' with local ack
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2102/ClientC --command advertise --ack 'local' --id 'third' --file './filters_and_properties/advertisement.mudebs'
sleep 1
echo
echo ClientA subscribing \(global\) to filter \'third\' with local ack
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2100/ClientA --command subscribe --ack 'local' --id 'third' --file './filters_and_properties/subscription.mudebs'
sleep 1
echo
echo ClientC publishing with adv. filter \'third\' with local ack
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2102/ClientC --command publish --ack local --id 'third' --content '<foo>apublicationfromC</foo>'
sleep 1
echo
echo ClientC unadvertising filter \'third\' with local ack
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2102/ClientC --command unadvertise --ack local --id 'third'
sleep 1
echo
echo ClientA unsubscribing the filter \'third\' with local ack
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2100/ClientA --command unsubscribe --ack local --id 'third'
sleep 1

fi

if [ $part4 = yes ]
then

echo
echo
echo
echo PART FOUR
echo
echo
echo
echo
sleep 1
echo ClientF advertising '(global)' filter \'fourth\' with global ack
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2105/ClientF --command advertise_global --ack global --id 'fourth' --file './filters_and_properties/advertisement.mudebs'
sleep 1
echo
echo ClientA subscribing to filter \'fourth\' with global ack
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2100/ClientA --command subscribe --ack global --id 'fourth' --file './filters_and_properties/subscription.mudebs'
sleep 1
echo
echo ClientF publishing with adv. filter \'fourth\' with global ack
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2105/ClientF --command publish --ack global --id 'fourth' --content '<foo>apublicationfromF</foo>'
sleep 1
echo
echo ClientF unadvertising '(global)' filter \'fourth\' with global ack
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2105/ClientF --command unadvertise --ack global --id 'fourth'
sleep 1
echo
echo ClientA unadvertising all the filters
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2100/ClientA --command unadvertise
sleep 1
echo
echo ClientB unadvertising all the filters
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2101/ClientB --command unadvertise
sleep 1
echo
echo ClientC unadvertising all the filters
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2102/ClientC --command unadvertise
sleep 1
echo
echo ClientA unsubscribing the filter \'fourth\' with global ack
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2100/ClientA --command unsubscribe --ack global --id 'fourth'
sleep 1
echo
echo ClientF requesting filter in \'./filters_and_properties/subscription.mudebs\', no adv.
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2105/ClientF --command request --file './filters_and_properties/subscription.mudebs'
sleep 1

fi

if [ $part5 = yes ]
then

echo
echo
echo
echo PART FIVE
echo
echo
echo
echo
sleep 1
echo ClientA advertising filter \'fifth\' in \'./filters_and_properties/advertisement.mudebs\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2100/ClientA --command advertise --id 'fifth' --file './filters_and_properties/advertisement.mudebs'
sleep 1
echo
echo ClientB advertising '(global)' filter \'fifth\' in \'./filters_and_properties/advertisement.mudebs\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2101/ClientB --command advertise_global --id 'fifth' --file './filters_and_properties/advertisement.mudebs'
sleep 1
echo
echo ClientF requesting filter in \'./filters_and_properties/subscription.mudebs\', no content
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2105/ClientF --command request --file './filters_and_properties/subscription.mudebs'
sleep 1
echo
echo ClientF requesting '(local)' filter in \'./filters_and_properties/subscription.mudebs\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2105/ClientF --command request_local --file './filters_and_properties/subscription.mudebs'
sleep 1
echo
echo ClientA publishing \'apublicationfromA\' with adv. filter \'fifth\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2100/ClientA --command publish --id 'fifth' --content '<foo>apublicationfromA</foo>'
sleep 1
echo
echo ClientB publishing \'apublicationfromB\' with adv. filter \'fifth\'
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2101/ClientB --command publish --id 'fifth' --content '<foo>apublicationfromB</foo>'
echo
sleep 1
echo
echo ClientF requesting filter in \'./filters_and_properties/subscription.mudebs\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2105/ClientF --command request --file './filters_and_properties/subscription.mudebs'
sleep 1
echo
echo ClientF requesting '(local)' filter in \'./filters_and_properties/subscription.mudebs\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2105/ClientF --command request_local --file './filters_and_properties/subscription.mudebs'
sleep 1
echo
echo
echo ClientA unadvertising all the filters
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2100/ClientA --command unadvertise
sleep 1
echo
echo ClientB unadvertising all the filters
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2101/ClientB --command unadvertise
sleep 1
echo
echo
echo ClientC unsubscribing all the filters
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2102/ClientC --command unsubscribe
sleep 1

fi

if [ $part6 = yes ]
then

echo
echo
echo
echo PART SIX
echo
echo
echo
sleep 1
echo ClientA subscribing to filter \'sixth\' in \'./filters_and_properties/subscription.mudebs\' with ABAC info \'category1,id1,INCOME:category2,id2,PersonalUse:category3,id3,abcd:category4,id4,access\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2100/ClientA --command subscribe --id 'sixth' --file './filters_and_properties/subscription.mudebs' --abac 'category1,id1,INCOME:category2,id2,PersonalUse:category3,id3,abcd:category4,id4,access'
sleep 1
echo
echo ClientA advertising filter \'sixth\' in \'./filters_and_properties/advertisement.mudebs\' with XAMCL policy in \'./filters_and_properties/policysimple.xml\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2100/ClientA --command advertise --id 'sixth' --file './filters_and_properties/advertisement.mudebs' --policy './filters_and_properties/policysimple.xml'
sleep 1
echo
echo ClientA publishing \'apublicationfromA\' with adv. filter \'sixth\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2100/ClientA --command publish --id 'sixth' --content '<foo>apublicationfromA</foo>'
sleep 1
echo
echo ClientB advertising filter \'sixth\' in \'./filters_and_properties/advertisement.mudebs\' with policy in \'./filters_and_properties/policysimple.xml\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2101/ClientB --command advertise --id 'sixth' --file './filters_and_properties/advertisement.mudebs' --policy './filters_and_properties/policysimple.xml'
sleep 1
echo
echo ClientB publishing \'apublicationfromB\' with adv. filter \'sixth\'
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2101/ClientB --command publish --id 'sixth' --content '<foo>apublicationfromB</foo>'
echo
sleep 1
echo
echo ClientC advertising filter \'sixth\' in \'./filters_and_properties/advertisement.mudebs\' with no policy
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2102/ClientC --command advertise --id 'sixth' --file './filters_and_properties/advertisement.mudebs'
sleep 1
echo
echo ClientC publishing \'apublicationfromC\' with adv. filter \'sixth\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2102/ClientC --command publish --id 'sixth' --content '<foo>apublicationfromC</foo>'
sleep 1
echo
echo ClientG subscribing to filter \'sixth\' in \'./filters_and_properties/subscription.mudebs\' with ABAC info \'category1,id1,WRONG:category2,id2,PersonalUse:category3,id3,abcd:category4,id4,access\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2106/ClientG --command subscribe --id 'sixth' --file './filters_and_properties/subscription.mudebs' --abac 'category1,id1,WRONG:category2,id2,PersonalUse:category3,id3,abcd:category4,id4,access'
sleep 1
echo
echo ClientB publishing \'anotherpublicationfromB\' with adv. filter \'sixth\'
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2101/ClientB --command publish --id 'sixth' --content '<foo>anotherpublicationfromB</foo>'
echo
sleep 1
echo
echo ClientC publishing \'anotherpublicationfromC\' with adv. filter \'sixth\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2102/ClientC --command publish --id 'sixth' --content '<foo>anotherpublicationfromC</foo>'
sleep 1
echo
echo ClientF advertising '(global)' filter \'sixth\' in \'./filters_and_properties/advertisement.mudebs\' with policy in \'./filters_and_properties/policysimple.xml\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2105/ClientF --command advertise_global --id 'sixth' --file './filters_and_properties/advertisement.mudebs' --policy './filters_and_properties/policysimple.xml'
sleep 1
echo
echo ClientF subscribing '(local)' to filter \'sixth\' in \'./filters_and_properties/subscription.mudebs\' with ABAC info \'category1,id1,INCOME:category2,id2,PersonalUse:category3,id3,abcd:category4,id4,access\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2105/ClientF --command subscribe_local --id 'sixth' --file './filters_and_properties/subscription.mudebs' --abac 'category1,id1,INCOME:category2,id2,PersonalUse:category3,id3,abcd:category4,id4,access'
sleep 1
echo
echo ClientF publishing \'apublicationfromF\' with adv. filter \'sixth\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2105/ClientF --command publish --id 'sixth' --content '<foo>apublicationfromF</foo>'
sleep 1
echo
echo ClientE subscribing '(local)' to filter \'sixth\' in \'./filters_and_properties/subscription.mudebs\' with ABAC info \'category1,id1,INCOME:category2,id2,PersonalUse:category3,id3,abcd:category4,id4,access\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2104/ClientE --command subscribe_local --id 'sixth' --file './filters_and_properties/subscription.mudebs' --abac 'category1,id1,INCOME:category2,id2,PersonalUse:category3,id3,abcd:category4,id4,access'
sleep 1
echo
echo ClientE advertising filter \'sixth\' in \'./filters_and_properties/advertisement.mudebs\' with XAMCL policy in \'./filters_and_properties/policysimple.xml\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2104/ClientE --command advertise --id 'sixth' --file './filters_and_properties/advertisement.mudebs' --policy './filters_and_properties/policysimple.xml'
sleep 1
echo
echo ClientE publishing \'apublicationfromE\' with adv. filter \'sixth\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2104/ClientE --command publish --id 'sixth' --content '<foo>apublicationfromE</foo>'
sleep 1
echo
echo ClientA subscribing to filter \'sixth-bis\' in \'./filters_and_properties/subscription.mudebs\' with no ABAC info
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2100/ClientA --command subscribe --id 'sixth-bis' --file './filters_and_properties/subscription.mudebs'
sleep 1
echo
echo ClientB subscribing to filter \'sixth\' in \'./filters_and_properties/subscription.mudebs\' with no ABAC info
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2101/ClientB --command subscribe --id 'sixth' --file './filters_and_properties/subscription.mudebs'
sleep 1
echo
echo ClientA publishing \'apublicationfromA\' with adv. filter \'sixth\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2100/ClientA --command publish --id 'sixth' --content '<foo>apublicationfromA</foo>'
sleep 1
echo
echo ClientG subscribing to filter \'sixth-bis\' in \'./filters_and_properties/subscription.mudebs\' with ABAC info \'category1,id1,INCOME:category2,id2,PersonalUse:category3,id3,abcd:category4,id4,access\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2106/ClientG --command subscribe --id 'sixth-bis' --file './filters_and_properties/subscription.mudebs' --abac 'category1,id1,INCOME:category2,id2,PersonalUse:category3,id3,abcd:category4,id4,access'
sleep 1
echo
echo ClientF publishing \'anotherpublicationfromF\' with adv. filter \'sixth\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2105/ClientF --command publish --id 'sixth' --content '<foo>anotherpublicationfromF</foo>'
sleep 1
echo
echo ClientF requesting filter in \'./filters_and_properties/subscription.mudebs\', with abac info \'category1,id1,INCOME:category2,id2,PersonalUse:category3,id3,abcd:category4,id4,access\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2105/ClientF --command request --file './filters_and_properties/subscription.mudebs' --abac 'category1,id1,INCOME:category2,id2,PersonalUse:category3,id3,abcd:category4,id4,access'
sleep 1
echo
echo ClientF requesting filter in \'./filters_and_properties/subscription.mudebs\', with wrong abac info \'category1,id1,INCOME:category2,id2,PersonalUse:category3,id3,abcd:category4,id4,WRONG\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2105/ClientF --command request --file './filters_and_properties/subscription.mudebs' --abac 'category1,id1,INCOME:category2,id2,PersonalUse:category3,id3,abcd:category4,id4,WRONG'
sleep 1
echo
echo ClientF requesting filter in \'./filters_and_properties/subscription.mudebs\', no abac info
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2105/ClientF --command request --file './filters_and_properties/subscription.mudebs'
sleep 1

fi

if [ $part7 = yes ]
then

echo
echo
echo
echo PART SEVEN
echo
echo
echo
sleep 1

echo
echo Information of BrokerA, but wrong usage
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2000/BrokerA --command information
sleep 1
echo
echo Information of BrokerA = scope graph
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2000/BrokerA --command information --whichinfo scopegraphs
sleep 1
echo
echo Information of BrokerA = scope lookup tables
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2000/BrokerA --command information --whichinfo slts
sleep 1
echo
echo Information of BrokerA = routing tables
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2000/BrokerA --command information --whichinfo rts
sleep 1
echo
echo On BrokerA, join scope \(s,top\)
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2000/BrokerA --command joinscope --dimension mudebs://localhost:Dim1 --subscope s --superscope top
sleep 1
echo
echo On BrokerA, wrong join scope with top as sub-scope
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2000/BrokerA --command joinscope --dimension mudebs://localhost:Dim1 --subscope top --superscope s --mapupfile ./filters_and_properties/mapupfilter.mudebs --mapdownfile ./filters_and_properties/mapdownfilter.mudebs
sleep 1
echo
echo On BrokerA, wrong join scope with bottom as super-scope
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2000/BrokerA --command joinscope --dimension mudebs://localhost:Dim1 --subscope s --superscope bot --mapupfile ./filters_and_properties/mapupfilter.mudebs --mapdownfile ./filters_and_properties/mapdownfilter.mudebs
sleep 1
echo
echo On BrokerA, wrong join scope \(s,t\) with missing mapup filter
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2000/BrokerA --command joinscope --dimension mudebs://localhost:Dim1 --subscope s --superscope t --mapdownfile ./filters_and_properties/mapdownfilter.mudebs
sleep 1
echo
echo On BrokerA, wrong join scope \(s,t\) with missing mapdown filter
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2000/BrokerA --command joinscope --dimension mudebs://localhost:Dim1 --subscope s --superscope t --mapupfile ./filters_and_properties/mapupfilter.mudebs
sleep 1
echo
echo Information of BrokerA = all
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2000/BrokerA --command information --whichinfo all
sleep 1
echo
echo Information of BrokerB = all
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2001/BrokerB --command information --whichinfo all
sleep 1
echo
echo ClientC making an error in advertising with scoping '(no scope id)'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2102/ClientC --command advertise --ack 'local' --id 'first' --file './filters_and_properties/advertisement.mudebs' --dimension mudebs://localhost:Dim1
sleep 1
echo
echo ClientC making an error in advertising with scoping '(no dimension)'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2102/ClientC --command advertise --ack 'local' --id 'first' --file './filters_and_properties/advertisement.mudebs' --scope s
sleep 1
echo
echo On BrokerA, join scope \(s2,top\) on Dim2
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2000/BrokerA --command joinscope --dimension mudebs://localhost:Dim2 --subscope s2 --superscope top
sleep 1
echo
echo ClientC advertising \'first\' with scope s, s2 and bot
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2102/ClientC --command advertise --ack 'local' --id 'first' --file './filters_and_properties/advertisement.mudebs' --dimension mudebs://localhost:Dim1 --scope s --dimension mudebs://localhost:Dim2 --scope s2 --scope-bottom
sleep 1
echo
echo ClientC advertising \'second\' with scope s and s2
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2102/ClientC --command advertise --ack 'local' --id 'second' --file './filters_and_properties/advertisement.mudebs' --dimension mudebs://localhost:Dim1 --scope s --dimension mudebs://localhost:Dim2 --scope s2
sleep 1
echo ClientA subscribing with scope s with unknown dimension
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2100/ClientA --command subscribe --ack 'local' --id 'first' --file './filters_and_properties/subscription.mudebs' --dimension mudebs://localhost:unknown --scope s
sleep 1
echo ClientA subscribing with scope s and s2
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2100/ClientA --command subscribe --ack 'local' --id 'third' --file './filters_and_properties/subscription.mudebs' --dimension mudebs://localhost:Dim1 --scope s --dimension mudebs://localhost:Dim2 --scope s2
sleep 1
echo ClientA subscribing with scope s, s2 and top
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2100/ClientA --command subscribe --ack 'local' --id 'second' --file './filters_and_properties/subscription.mudebs' --dimension mudebs://localhost:Dim1 --scope s --dimension mudebs://localhost:Dim2 --scope s2 --scope-top
sleep 1
echo ClientA getting the scopes of its access broker
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2100/ClientA --command get-scopes
sleep 1
echo
echo On BrokerA, leave scope \(s,t\)
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2000/BrokerA --command leavescope --dimension mudebs://localhost:Dim1 --subscope s --superscope t
sleep 1

fi

if [ $part8 = yes ]
then

echo
echo
echo
echo PART EIGHT
echo
echo
echo
sleep 1

echo "Starting BrokerAForTerm"
${MUDEBS_HOME_SCRIPTS}/startbroker --uri mudebs://localhost:3000/BrokerAForTerm --log PERFORMANCE.PERF --perfconfigfile ./filters_and_properties/performance.config #--log ROUTING.TRACE
#sleep 1
echo "Starting BrokerBForTerm"
${MUDEBS_HOME_SCRIPTS}/startbroker --uri mudebs://localhost:3001/BrokerBForTerm --log PERFORMANCE.PERF --perfconfigfile ./filters_and_properties/performance.config #--log ROUTING.TRACE
#sleep 1
echo "BrokerBForTerm connects to BrokerAForTerm"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3001/BrokerBForTerm --command connect --neigh mudebs://localhost:3000/BrokerAForTerm
echo "Starting BrokerCForTerm"
${MUDEBS_HOME_SCRIPTS}/startbroker --uri mudebs://localhost:3002/BrokerCForTerm --log PERFORMANCE.PERF --perfconfigfile ./filters_and_properties/performance.config #--log ROUTING.TRACE
echo "BrokerCForTerm connects to BrokerBForTerm"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3002/BrokerCForTerm --command connect --neigh mudebs://localhost:3001/BrokerBForTerm
echo "Starting BrokerDForTerm"
${MUDEBS_HOME_SCRIPTS}/startbroker --uri mudebs://localhost:3003/BrokerDForTerm --log PERFORMANCE.PERF --perfconfigfile ./filters_and_properties/performance.config #--log ROUTING.TRACE
echo "BrokerDForTerm connects to BrokerBForTerm"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3003/BrokerDForTerm --command connect --neigh mudebs://localhost:3001/BrokerBForTerm
echo "BrokerDForTerm connects to BrokerCForterm"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3003/BrokerDForTerm --command connect --neigh mudebs://localhost:3002/BrokerCForTerm
echo "Starting BrokerEForTerm"
${MUDEBS_HOME_SCRIPTS}/startbroker --uri mudebs://localhost:3004/BrokerEForTerm --log PERFORMANCE.PERF --perfconfigfile ./filters_and_properties/performance.config #--log ROUTING.TRACE
echo "BrokerEForTerm connects to BrokerDForTerm"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3004/BrokerEForTerm --command connect --neigh mudebs://localhost:3003/BrokerDForTerm
echo "Starting BrokerFForTerm"
${MUDEBS_HOME_SCRIPTS}/startbroker --uri mudebs://localhost:3005/BrokerFForTerm --log PERFORMANCE.PERF --perfconfigfile ./filters_and_properties/performance.config #--log ROUTING.TRACE
echo "BrokerFForTerm connects to BrokerEForTerm"
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3005/BrokerFForTerm --command connect --neigh mudebs://localhost:3004/BrokerEForTerm

echo
echo "Starting ClientAForTerm connected to BrokerAForTerm"
${MUDEBS_HOME_SCRIPTS}/startclient --uri mudebs://localhost:3100/ClientAForTerm --broker mudebs://localhost:3000/BrokerAForTerm #--log SCRIPTING.TRACE
echo "Starting ClientBForTerm connected to BrokerBForTerm"
${MUDEBS_HOME_SCRIPTS}/startclient --uri mudebs://localhost:3101/ClientBForTerm --broker mudebs://localhost:3001/BrokerBForTerm #--log SCRIPTING.TRACE
echo "Starting ClientCForTerm connected to BrokerCForTerm"
${MUDEBS_HOME_SCRIPTS}/startclient --uri mudebs://localhost:3102/ClientCForTerm --broker mudebs://localhost:3002/BrokerCForTerm #--log SCRIPTING.TRACE
echo "Starting ClientDForTerm connected to BrokerDForTerm"
${MUDEBS_HOME_SCRIPTS}/startclient --uri mudebs://localhost:3103/ClientDForTerm --broker mudebs://localhost:3003/BrokerDForTerm #--log SCRIPTING.TRACE
echo "Starting ClientEForTerm connected to BrokerEForTerm"
${MUDEBS_HOME_SCRIPTS}/startclient --uri mudebs://localhost:3104/ClientEForTerm --broker mudebs://localhost:3004/BrokerEForTerm #--log SCRIPTING.TRACE
echo "Starting ClientFForTerm connected to BrokerDForTerm"
${MUDEBS_HOME_SCRIPTS}/startclient --uri mudebs://localhost:3105/ClientFForTerm --broker mudebs://localhost:3003/BrokerDForTerm #--log SCRIPTING.TRACE
echo "Starting ClientGForTerm connected to BrokerFForTerm"
${MUDEBS_HOME_SCRIPTS}/startclient --uri mudebs://localhost:3106/ClientGForTerm --broker mudebs://localhost:3005/BrokerFForTerm #--log SCRIPTING.TRACE
sleep 7

# echo
# echo Setting logger overlay to debug for BrokerAForTerm
# ${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3000/BrokerAForTerm --command setlog --name overlay --level debug
# echo Setting logger overlay to debug for BrokerBForTerm
# ${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3001/BrokerBForTerm --command setlog --name overlay --level debug
# echo Setting logger overlay to debug for BrokerCForTerm
# ${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3002/BrokerCForTerm --command setlog --name overlay --level debug
# echo Setting logger overlay to debug for BrokerDForTerm
# ${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3003/BrokerCForTerm --command setlog --name overlay --level debug
# echo Setting logger overlay to debug for BrokerEForTerm
# ${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3004/BrokerEForTerm --command setlog --name overlay --level debug
# echo Setting logger overlay to debug for BrokerFForTerm
# ${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3005/BrokerFForTerm --command setlog --name overlay --level debug
# sleep 4

echo
echo ClientAForTerm subscribing to filter \'first\' in \'./filters_and_properties/subscription.mudebs\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3100/ClientAForTerm --command subscribe --id 'first' --file './filters_and_properties/subscription.mudebs'
sleep 1
echo
echo ClientAForTerm advertising filter \'first\' in \'./filters_and_properties/advertisement.mudebs\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3100/ClientAForTerm --command advertise --id 'first' --file './filters_and_properties/advertisement.mudebs'
sleep 1
echo
echo On BrokerAForTerm, termination detection
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3000/BrokerAForTerm --command term_detection
sleep 1
echo
echo ClientAForTerm publishing \'apublicationfromA\' with adv. filter \'first\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3100/ClientAForTerm --command publish --id 'first' --content '<foo>apublicationfromA</foo>'
sleep 1
echo
echo ClientBForTerm advertising filter \'first\' in \'./filters_and_properties/advertisement.mudebs\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3101/ClientBForTerm --command advertise --id 'first' --file './filters_and_properties/advertisement.mudebs'
sleep 1
echo
echo On BrokerFForTerm, termination detection
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3005/BrokerFForTerm --command term_detection
sleep 1
echo
echo ClientBForTerm publishing \'apublicationfromB\' with adv. filter \'first\'
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3101/ClientBForTerm --command publish --id 'first' --content '<foo>apublicationfromB</foo>'
sleep 1
echo
echo ClientCForTerm advertising filter \'first\' in \'./filters_and_properties/advertisement.mudebs\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3102/ClientCForTerm --command advertise --id 'first' --file './filters_and_properties/advertisement.mudebs'
sleep 1
echo
echo ClientCForTerm publishing \'apublicationfromC\' with adv. filter \'first\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3102/ClientCForTerm --command publish --id 'first' --content '<foo>apublicationfromC</foo>'
sleep 1
echo
echo ClientEForTerm advertising filter \'first\' in \'./filters_and_properties/advertisement.mudebs\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3104/ClientEForTerm --command advertise --id 'first' --file './filters_and_properties/advertisement.mudebs'
sleep 1
echo
echo ClientEForTerm publishing \'apublicationfromE\' with adv. filter \'first\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3104/ClientEForTerm --command publish --id 'first' --content '<foo>apublicationfromE</foo>'
sleep 1
echo
echo ClientEForTerm unadvertising filter \'first\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3104/ClientEForTerm --command unadvertise --id 'first'
sleep 1
echo
echo ClientCForTerm subscribing to filter \'first\' in \'./filters_and_properties/subscription.mudebs\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3102/ClientCForTerm --command subscribe --id 'first' --file './filters_and_properties/subscription.mudebs'
sleep 1
echo
echo ClientBForTerm publishing 'apublicationfromB' with adv. filter \'first\'
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3101/ClientBForTerm --command publish --id 'first' --content '<foo>apublicationfromB</foo>'
sleep 1
echo
echo ClientAForTerm unsubscribing the filter 'first'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3100/ClientAForTerm --command unsubscribe --id 'first'
sleep 1
echo
echo ClientAForTerm unadvertising all the filters
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3100/ClientAForTerm --command unadvertise
sleep 1
echo
echo ClientBForTerm unadvertising all the filters
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3101/ClientBForTerm --command unadvertise
sleep 1
echo
echo ClientCForTerm unadvertising all the filters
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3102/ClientCForTerm --command unadvertise
sleep 1
echo
echo ClientDForTerm unadvertising all the filters
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:3103/ClientCForTerm --command unadvertise
sleep 1
echo
echo On BrokerAForTerm, termination detection
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3000/BrokerAForTerm --command term_detection
sleep 2
echo
echo On BrokerAForTerm, terminate
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:3000/BrokerAForTerm --command terminate_all
sleep 7

fi

echo
echo "Hit return when you want to stop the demonstration"
read x
echo "Stopping all the clients"
${MUDEBS_HOME_SCRIPTS}/stopclient ClientA
${MUDEBS_HOME_SCRIPTS}/stopclient ClientB
${MUDEBS_HOME_SCRIPTS}/stopclient ClientC
${MUDEBS_HOME_SCRIPTS}/stopclient ClientD
${MUDEBS_HOME_SCRIPTS}/stopclient ClientE
${MUDEBS_HOME_SCRIPTS}/stopclient ClientF
${MUDEBS_HOME_SCRIPTS}/stopclient ClientG
${MUDEBS_HOME_SCRIPTS}/stopclient ClientH

echo "Stopping all the brokers"
${MUDEBS_HOME_SCRIPTS}/stopbroker BrokerA
${MUDEBS_HOME_SCRIPTS}/stopbroker BrokerB
${MUDEBS_HOME_SCRIPTS}/stopbroker BrokerC
${MUDEBS_HOME_SCRIPTS}/stopbroker BrokerD
${MUDEBS_HOME_SCRIPTS}/stopbroker BrokerE
${MUDEBS_HOME_SCRIPTS}/stopbroker BrokerF

if [ $part8 = yes ]
then

ps aux | grep java

${MUDEBS_HOME_SCRIPTS}/stopclient ClientAForTerm
${MUDEBS_HOME_SCRIPTS}/stopclient ClientBForTerm
${MUDEBS_HOME_SCRIPTS}/stopclient ClientCForTerm
${MUDEBS_HOME_SCRIPTS}/stopclient ClientDForTerm
${MUDEBS_HOME_SCRIPTS}/stopclient ClientEForTerm
${MUDEBS_HOME_SCRIPTS}/stopclient ClientFForTerm
${MUDEBS_HOME_SCRIPTS}/stopclient ClientGForTerm

${MUDEBS_HOME_SCRIPTS}/stopbroker BrokerAForTerm
${MUDEBS_HOME_SCRIPTS}/stopbroker BrokerBForTerm
${MUDEBS_HOME_SCRIPTS}/stopbroker BrokerCForTerm
${MUDEBS_HOME_SCRIPTS}/stopbroker BrokerDForTerm
${MUDEBS_HOME_SCRIPTS}/stopbroker BrokerEForTerm
${MUDEBS_HOME_SCRIPTS}/stopbroker BrokerFForTerm

fi
