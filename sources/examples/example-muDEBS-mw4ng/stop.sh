#!/bin/bash

MUDEBS_HOME_SCRIPTS="$(cd ../../../sources/muDEBS/scripts/ && pwd)"

echo "Stopping all the clients"
${MUDEBS_HOME_SCRIPTS}/stopclient W
${MUDEBS_HOME_SCRIPTS}/stopclient X
${MUDEBS_HOME_SCRIPTS}/stopclient Y
${MUDEBS_HOME_SCRIPTS}/stopclient Z
echo "Stopping all the brokers"
${MUDEBS_HOME_SCRIPTS}/stopbroker B1
${MUDEBS_HOME_SCRIPTS}/stopbroker B2
${MUDEBS_HOME_SCRIPTS}/stopbroker B3
${MUDEBS_HOME_SCRIPTS}/stopbroker B4
${MUDEBS_HOME_SCRIPTS}/stopbroker B5
