#!/bin/bash

MUDEBS_HOME_SCRIPTS="$(cd ../../../sources/muDEBS/scripts/ && pwd)"

echo 'Architecture of the example'
echo ''
echo '  W        B2            Z'
echo '   \      /|\           /'
echo '    \    / | \         /'
echo '     \  /  |  \       /'
echo '      B3   |   B1---B5'
echo '     /     |  /       \'
echo '    /      | /         \'
echo '   /       |/           \'
echo '  X        B4            Y'
echo ''

echo
echo "Hit return when you want to start the brokers and clients"
read x
echo "Starting B1"
${MUDEBS_HOME_SCRIPTS}/startbroker --uri mudebs://localhost:2001/B1 #--log ROUTING.DEBUG
echo "Starting B2 connected to B1"
${MUDEBS_HOME_SCRIPTS}/startbroker --uri mudebs://localhost:2002/B2 --neigh mudebs://localhost:2001/B1 #--log ROUTING.DEBUG
echo "Starting B3 connected to B2"
${MUDEBS_HOME_SCRIPTS}/startbroker --uri mudebs://localhost:2003/B3 --neigh mudebs://localhost:2002/B2 #--log ROUTING.DEBUG
echo "Starting B4 connected to B1 and B2"
${MUDEBS_HOME_SCRIPTS}/startbroker --uri mudebs://localhost:2004/B4 --neigh mudebs://localhost:2001/B1 --neigh mudebs://localhost:2002/B2 #--log ROUTING.DEBUG
echo "Starting B5 connected to B1"
${MUDEBS_HOME_SCRIPTS}/startbroker --uri mudebs://localhost:2005/B5 --neigh mudebs://localhost:2001/B1 #--log ROUTING.DEBUG

echo "Starting W connected to B3"
${MUDEBS_HOME_SCRIPTS}/startclient --uri mudebs://localhost:2101/W --broker mudebs://localhost:2003/B3 #--log SCRIPTING.TRACE
echo "Starting X connected to B3"
${MUDEBS_HOME_SCRIPTS}/startclient --uri mudebs://localhost:2102/X --broker mudebs://localhost:2003/B3 #--log SCRIPTING.TRACE
echo "Starting Y connected to B5"
${MUDEBS_HOME_SCRIPTS}/startclient --uri mudebs://localhost:2103/Y --broker mudebs://localhost:2005/B5 #--log SCRIPTING.TRACE
echo "Starting Z connected to B5"
${MUDEBS_HOME_SCRIPTS}/startclient --uri mudebs://localhost:2104/Z --broker mudebs://localhost:2005/B5 #--log SCRIPTING.TRACE

sleep 7

echo
echo "Hit return to start join scope calls"
read x

echo
echo On B1, join scope \(es,top\)
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2001/B1 --command joinscope --dimension mudebs://localhost:membership --subscope es --superscope top
sleep 1

echo
echo On B2, join scope \(us,es\)
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2002/B2 --command joinscope --dimension mudebs://localhost:membership --subscope us --superscope es --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
sleep 1

echo
echo On B3, join scope \(ls,us\)
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2003/B3 --command joinscope --dimension mudebs://localhost:membership --subscope ls --superscope us --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
sleep 1
echo
echo On B1, join scope \(fs,es\)
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2001/B1 --command joinscope --dimension mudebs://localhost:membership --subscope fs --superscope es --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
sleep 1
echo
echo On B5, join scope \(is,fs\)
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2005/B5 --command joinscope --dimension mudebs://localhost:membership --subscope is --superscope fs --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
sleep 1
echo
echo On B5, join scope \(ir,top\)
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2005/B5 --command joinscope --dimension mudebs://localhost:membership --subscope ir --superscope top
sleep 1
echo
echo On B5, join scope \(is,ir\)
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2005/B5 --command joinscope --dimension mudebs://localhost:membership --subscope is --superscope ir --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
sleep 1

echo
echo On B2, join scope \(eu,top\)
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2002/B2 --command joinscope --dimension mudebs://localhost:admin_area --subscope eu --superscope top
sleep 1
echo
echo On B2, join scope \(fr,eu\)
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2002/B2 --command joinscope --dimension mudebs://localhost:admin_area --subscope fr --superscope eu --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
sleep 1
echo
echo On B3, join scope \(bo,fr\)
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2003/B3 --command joinscope --dimension mudebs://localhost:admin_area --subscope bo --superscope fr --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
sleep 1
echo
echo On B3, join scope \(ch,bo\)
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2003/B3 --command joinscope --dimension mudebs://localhost:admin_area --subscope ch --superscope bo --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
sleep 1
echo
echo On B1, join scope \(uk,eu\)
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2001/B1 --command joinscope --dimension mudebs://localhost:admin_area --subscope uk --superscope eu --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
sleep 1
echo
echo On B5, join scope \(lo,uk\)
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2005/B5 --command joinscope --dimension mudebs://localhost:admin_area --subscope lo --superscope uk --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
sleep 1

echo
echo On B4, join scope \(pi,top\)
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2004/B4 --command joinscope --dimension mudebs://localhost:network_range --subscope pi --superscope top
sleep 1
echo
echo On B5, join scope \(tm,pi\)
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2005/B5 --command joinscope --dimension mudebs://localhost:network_range --subscope tm --superscope pi --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
sleep 1
echo
echo On B2, join scope \(ti,pi\)
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2002/B2 --command joinscope --dimension mudebs://localhost:network_range --subscope ti --superscope pi --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
sleep 1
echo
echo On B3, join scope \(bw,ti\)
echo
${MUDEBS_HOME_SCRIPTS}/broker --uri mudebs://localhost:2003/B3 --command joinscope --dimension mudebs://localhost:network_range --subscope bw --superscope ti --mapupfile ./ressources/mapupfilter.mudebs --mapdownfile ./ressources/mapdownfilter.mudebs
sleep 1

echo
echo "Hit return to start advertising and subscribing"
read x

echo
echo W advertising filter \'first\' with scopes \'ls\' and \'ch\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2101/W --command advertise --ack local --id 'first' --file ./ressources/advertisement.mudebs --dimension mudebs://localhost:membership --scope ls --dimension mudebs://localhost:admin_area --scope ch
sleep 1
echo
echo X advertising filter \'second\' with scopes \'ls\', \'ch\' and bot
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2102/X --command advertise --ack local --id 'second' --file ./ressources/advertisement.mudebs --dimension mudebs://localhost:membership --scope ls --dimension mudebs://localhost:admin_area --scope ch --scope-bottom
echo
sleep 1
echo Y subscribing filter \'third\' with scopes \'is\', \'lo\' and \'tm\'
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2103/Y --command subscribe --ack local --id 'third' --file ./ressources/subscription.mudebs --dimension mudebs://localhost:membership --scope is --dimension mudebs://localhost:admin_area --scope lo --dimension mudebs://localhost:network_range --scope tm
echo
sleep 1
echo Z subscribing filter \'fourth\' with scopes \'is\', \'tm\' and top
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2104/Z --command subscribe --ack local --id 'fourth' --file ./ressources/subscription.mudebs --dimension mudebs://localhost:membership --scope is --dimension mudebs://localhost:network_range --scope tm --scope-top
sleep 1

echo
echo "Hit return to start publishing"
read x
echo
echo W publishing with adv. filter \'first\' with local ack
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2101/W --command publish --ack local --id 'first' --content '<foo>helloWorldIAmW</foo>'
sleep 1
echo
echo X publishing with adv. filter \'second\' with local ack
echo
${MUDEBS_HOME_SCRIPTS}/client --uri mudebs://localhost:2102/X --command publish --ack local --id 'second' --content '<foo>helloWorldIAmX</foo>'
sleep 1

echo "Stopping all the clients"
${MUDEBS_HOME_SCRIPTS}/stopclient W
${MUDEBS_HOME_SCRIPTS}/stopclient X
${MUDEBS_HOME_SCRIPTS}/stopclient Y
${MUDEBS_HOME_SCRIPTS}/stopclient Z
echo "Stopping all the brokers"
${MUDEBS_HOME_SCRIPTS}/stopbroker B1
${MUDEBS_HOME_SCRIPTS}/stopbroker B2
${MUDEBS_HOME_SCRIPTS}/stopbroker B3
${MUDEBS_HOME_SCRIPTS}/stopbroker B4
${MUDEBS_HOME_SCRIPTS}/stopbroker B5
